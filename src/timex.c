/*
    ZEsarUX  ZX Second-Emulator And Released for UniX 
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>

#include "cpu.h"
#include "timex.h"
#include "debug.h"
#include "contend.h"
#include "mem128.h"
#include "menu.h"
#include "screen.h"
#include "tbblue.h"



z80_bit timex_video_emulation={0};
z80_byte timex_port_ff=0;




//Para poder hacer 512x192 en una parte de la pantalla solamente
int timex_ugly_hack_enabled=0;
int timex_ugly_hack_last_hires=0; //a partir de que coordenada se hace cambio



//Si real, es 512x192 sin escalado, pero no permite efectos de linea
//Si no, es 512x192 escalado a 256x192 y permite efectos (cambios de modo) en el mismo frame
z80_bit timex_mode_512192_real={1};


/*
With the Timex hi res display, the border color is the same as the paper color in the second CLUT. Bits 3-5 of port FF set the ink, paper, and border values to the following ULAplus palette registers:
BITS INK PAPER BORDER
000 24 31 31 001 25 30 30
010 26 29 29
011 27 28 28
100 28 27 27
101 29 26 26
110 30 25 25
111 31 24 24

*/


//Retorna color tinta (sin brillo) para modo timex 6
int get_timex_ink_mode6_color(void)
{
        z80_byte col6=(timex_port_ff>>3)&7;
        return col6;
}


//Retorna color paper (sin brillo) para modo timex 6
int get_timex_paper_mode6_color(void)
{
        z80_byte col6=7-get_timex_ink_mode6_color();
        return col6;
}


//Retorna color border (con brillo)
int get_timex_border_mode6_color(void)
{

        return (7-get_timex_ink_mode6_color() )+8;
}



int timex_si_modo_512(void)
{
    //Si es modo timex 512x192, llamar a otra funcion
                z80_byte timex_video_mode=timex_port_ff&7;
                if (timex_video_emulation.v) {
                        if (timex_video_mode==4 || timex_video_mode==6) {
				return 1;
                        }
                }

	return 0;
}



int timex_si_modo_512_y_zoom_par(void)
{
	if (timex_si_modo_512() ) {
		if ( (zoom_x&1)==0) return 1;
	}


	return 0;

}

int timex_si_modo_shadow(void)
{
     z80_byte timex_video_mode=timex_port_ff&7;

        if (timex_video_emulation.v) {
                //Modos de video Timex
                /*
000 - Video data at address 16384 and 8x8 color attributes at address 22528 (like on ordinary Spectrum);

001 - Video data at address 24576 and 8x8 color attributes at address 30720;

010 - Multicolor mode: video data at address 16384 and 8x1 color attributes at address 24576;

110 - Extended resolution: without color attributes, even columns of video data are taken from address 16384, and odd columns of video data are taken fr
om address 24576
                */

                if (timex_video_mode==1) return 1;

        }

        return 0;

}


int timex_si_modo_8x1(void)
{
     z80_byte timex_video_mode=timex_port_ff&7;

        if (timex_video_emulation.v) {
                //Modos de video Timex
                /*
000 - Video data at address 16384 and 8x8 color attributes at address 22528 (like on ordinary Spectrum);

001 - Video data at address 24576 and 8x8 color attributes at address 30720;

010 - Multicolor mode: video data at address 16384 and 8x1 color attributes at address 24576;

110 - Extended resolution: without color attributes, even columns of video data are taken from address 16384, and odd columns of video data are taken fr
om address 24576
                */

                if (timex_video_mode==2) return 1;

        }

        return 0;

}


void set_timex_port_ff(z80_byte value)
{
	if (timex_video_emulation.v) {

		if ( (timex_port_ff&7)!=(value&7)) {
	                char mensaje[200];

			if ((value&7)==0) sprintf (mensaje,"Setting Timex Video Mode 0 (standard screen 0)");
			else if ((value&7)==1) sprintf (mensaje,"Setting Timex Video Mode 1 (standard screen 1)");
			else if ((value&7)==2) sprintf (mensaje,"Setting Timex Video Mode 2 (hires colour 8x1)");
			else if ((value&7)==6) {
				if ( (zoom_x&1)==0 && timex_mode_512192_real.v) {
					sprintf (mensaje,"Setting Timex Video Mode 6 (512x192 monochrome)");
				}

				else {
					sprintf (mensaje,"Setting Timex Video Mode 6 (512x192 monochrome)");
                                }
			}
                        else sprintf (mensaje,"Setting Unknown Timex Video Mode %d",value);

                	screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,mensaje);
		}

        	if ((value&7)==6) {
                        //Indicar que se ha puesto modo timex en alguna parte del frame
	                //timex_ugly_hack_last_hires=t_estados/screen_testados_linea;
        	        //printf ("estableciendo modo timex en y: %d\n",timex_ugly_hack_last_hires);
        	}


		z80_byte last_timex_port_ff=timex_port_ff;
		timex_port_ff=value;
		//Color del border en modo timex hi-res sale de aqui
		//Aunque con esto avisamos que el color del border en modo 512x192 se puede haber modificado
		modificado_border.v=1;
		if (last_timex_port_ff!=timex_port_ff) clear_putpixel_cache(); //porque se puede cambiar de modo, borrar la putpixel cache

			//Sincronizar los 5 bits bajos a registro tbblue			

			/*
			(W) 0x69 (105) => DISPLAY CONTROL 1 REGISTER

			Bit	Function
			7	Enable the Layer 2 (alias for Layer 2 Access Port ($123B) bit 1)
			6	Enable ULA shadow (bank 7) display (alias for Memory Paging Control ($7FFD) bit 3)
			5-0	alias for Timex Sinclair Video Mode Control ($xxFF) bits 5:0

			*/
			tbblue_registers[105] &= (128+64);
			tbblue_registers[105] |= (value&63);


	}

}
