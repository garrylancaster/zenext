/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "screen.h"
#include "debug.h"
#include "mem128.h"
#include "operaciones.h"
#include "charset.h"
#include "menu.h"
#include "audio.h"
#include "contend.h"
#include "ula.h"
#include "ulaplus.h"
#include "timex.h"
#include "timer.h"
#include "tbblue.h"
#include "settings.h"

//Incluimos estos dos para la funcion de fade out
#ifdef COMPILE_XWINDOWS
	#include "scrxwindows.h"
#endif


//Tabla que indica cada direccion de pantalla para cada coordenada
z80_int *screen_addr_table;

//ultima posicion y para funcion screen_print
int screen_print_y=0;

//si se muestran determinados mensajes en splash, como los de cambio de modo de video
//no confundir con el mensaje de bienvenida
z80_bit screen_show_splash_texts={1};

//mostrar uso de cpu en footer
z80_bit screen_show_cpu_usage={1};

//mostrar temperatura de cpu en footer
z80_bit screen_show_cpu_temp={1};

//mostrar fps en footer
z80_bit screen_show_fps={1};

int screen_reduce_offset_x=0;
int screen_reduce_offset_y=0;


z80_bit ocr_settings_not_look_23606={0};

//Rutinas de pantalla
void (*scr_refresca_pantalla) (void);
void (*scr_refresca_pantalla_solo_driver) (void);
void (*scr_set_fullscreen) (void);
void (*scr_reset_fullscreen) (void);
int ventana_fullscreen=0;
int (*scr_init_pantalla) (void);
void (*scr_end_pantalla) (void);
z80_byte (*scr_lee_puerto) (z80_byte puerto_h,z80_byte puerto_l);
void (*scr_actualiza_tablas_teclado) (void);

void (*scr_putpixel_zoom) (int x,int y,unsigned int color);
void (*scr_putpixel_zoom_rainbow)(int x,int y,unsigned int color);

void (*scr_putpixel) (int x,int y,unsigned int color);
void (*scr_putpixel_final_rgb) (int x,int y,unsigned int color_rgb);
void (*scr_putpixel_final) (int x,int y,unsigned int color);

int (*scr_get_menu_width) (void);
int (*scr_get_menu_height) (void);




//Rutina que muestra los mensajes de registros de cpu, propio para cada driver
void (*scr_debug_registers)(void);

//Rutina que muestra los mensajes de "debug_printf", propio para cada driver
void (*scr_messages_debug)(char *mensaje);

//Rutina para imprimir un caracter del menu
void (*scr_putchar_menu) (int x,int y, z80_byte caracter,int tinta,int papel);

//Rutina para imprimir un caracter en el pie de la pantalla
void (*scr_putchar_footer) (int x,int y, z80_byte caracter,int tinta,int papel);


int scr_tiene_colores=0;

//nombre del driver: aa, null, xwindows, etc. inicializado por cada driver en init
//renombrado a _new_ para evitar posible error de segfault
char scr_new_driver_name[100];

void scr_tsconf_putpixel_zx_mode(int x,int y,unsigned color);
void scr_refresca_border_tsconf_cont(void);
void screen_tsconf_refresca_rainbow(void);




//Para frameskip manual
//valor indicado en menu para frameskip
int frameskip=0;
//conteo actual para ver si toca refrescar frame (cuando vale 0)
int frameskip_counter=0;

//Si se muestra puntero raton
z80_bit mouse_pointer_shown={1};


//Intento de hacer las rutinas de refresco de pantalla mas rapidas
//Se guarda minima y dibujada y maxima y, para solo refrescar esa zona que se haya tocado con putpixel
//probado de momento con X11 y consume mas cpu aun
int putpixel_max_y=-1;
int putpixel_min_y=99999;



total_palette_colours total_palette_colours_array[TOTAL_PALETAS_COLORES]={
	{"Speccy","16 colour standard",0,SPECCY_TOTAL_PALETTE_COLOURS},
	//{"SpeccyReal","Real 16/48/+ palette",SPECCY_1648_REAL_PALETTE_FIRST_COLOR,SPECCY_1648_REAL_PALETTE_COLOURS},
	{"Gigascreen","256 gigascreen",SPECCY_TOTAL_PALETTE_COLOURS+SPECCY_GREY_SCANLINE_TOTAL_PALETTE_COLOURS,GIGASCREEN_TOTAL_PALETTE_COLOURS},
	{"Z88","Z88 4 colour",Z88_PXCOLON,Z88_TOTAL_PALETTE_COLOURS},
	{"ULAPlus","ULAPlus GRB palette",ULAPLUS_INDEX_FIRST_COLOR,ULAPLUS_TOTAL_PALETTE_COLOURS},
	{"Spectra","Spectra palette",SPECTRA_INDEX_FIRST_COLOR,SPECTRA_TOTAL_PALETTE_COLOURS},
	{"CPC","CPC palette",CPC_INDEX_FIRST_COLOR,CPC_TOTAL_PALETTE_COLOURS},
	{"Prism","Prism 12 bit palette",PRISM_INDEX_FIRST_COLOR,PRISM_TOTAL_PALETTE_COLOURS},
	{"Sam Coupe","Sam 128 colour palette",SAM_INDEX_FIRST_COLOR,SAM_TOTAL_PALETTE_COLOURS},
	{"TBBlue RGB9","TBBlue 512 colour palette",RGB9_INDEX_FIRST_COLOR,RGB9_TOTAL_PALETTE_COLOURS},
	{"TSConf","TSConf 15 bit palette",TSCONF_INDEX_FIRST_COLOR,TSCONF_TOTAL_PALETTE_COLOURS},
	{"VDP9918A","16 colour standard",VDP_9918_INDEX_FIRST_COLOR,VDP_9918_TOTAL_PALETTE_COLOURS},
};

z80_bit scr_refresca_sin_colores={0};

void scr_set_driver_name(char *nombre)
{
	strcpy(scr_new_driver_name,nombre);
}

//colores usados para el fondo cuando hay menu/overlay activo
//int spectrum_colortable_oscuro[EMULATOR_TOTAL_PALETTE_COLOURS];

//colores usados para grises, red, green, etc
//int spectrum_colortable_grises[EMULATOR_TOTAL_PALETTE_COLOURS];

//y puntero que indica una tabla o la otra
int *spectrum_colortable;


//Colores normales (los primeros sin oscuro), sean ya colores o gama de grises
//los 16 colores (de 16..31) son mas oscuros usados en modos interlaced
//los 4 colores siguientes son los usados en Z88
//int *spectrum_colortable_normal;
int spectrum_colortable_normal[EMULATOR_TOTAL_PALETTE_COLOURS];

//Colores solo en blanco y negro para cuando se abre el menu y el emulador esta con multitask off
//int spectrum_colortable_new_blanco_y_negro[EMULATOR_TOTAL_PALETTE_COLOURS];

//Tabla con los colores reales del Spectrum. Formato RGB
int spectrum_colortable_original_new[16] =
{

0x000000,  //negro
0x0000C0,  //azul
0xC00000,  //rojo
0xC000C0,  //magenta
0x00C000,  //verde
0x00C0C0,  //cyan
0xC0C000,  //amarillo
0xC0C0C0,  //blanco

0x000000,
0x0000FF,
0xFF0000,
0xFF00FF,
0x00FF00,
0x00FFFF,
0xFFFF00,
0xFFFFFF

};

//Tabla con los colores exactamente iguales para Spectrum 16/48/+, segun calculos de Richard Atkinson
int spectrum_colortable_1648_real[16] =
{
0x060800,
0x0D13A7,
0xBD0707,
0xC312AF,
0x07BA0C,
0x0DC6B4,
0xBCB914,
0xC2C4BC,
0x060800,
0x161CB0,
0xCE1818,
0xDC2CC8,
0x28DC2D,
0x36EFDE,
0xEEEB46,
0xFDFFF7
};

int *screen_return_spectrum_palette(void)
{
	return spectrum_colortable_original_new;
}



z80_bit spectrum_1648_use_real_palette={0};


//Tabla con colores para tema de GUI Solarized. 
/*
SOLARIZED HEX     16/8 TERMCOL  XTERM/HEX   L*A*B      RGB         HSB
--------- ------- ---- -------  ----------- ---------- ----------- -----------
base03    #002b36  8/4 brblack  234 #1c1c1c 15 -12 -12   0  43  54 193 100  21
base02    #073642  0/4 black    235 #262626 20 -12 -12   7  54  66 192  90  26
base01    #586e75 10/7 brgreen  240 #585858 45 -07 -07  88 110 117 194  25  46
base00    #657b83 11/7 bryellow 241 #626262 50 -07 -07 101 123 131 195  23  51
base0     #839496 12/6 brblue   244 #808080 60 -06 -03 131 148 150 186  13  59
base1     #93a1a1 14/4 brcyan   245 #8a8a8a 65 -05 -02 147 161 161 180   9  63
base2     #eee8d5  7/7 white    254 #e4e4e4 92 -00  10 238 232 213  44  11  93
base3     #fdf6e3 15/7 brwhite  230 #ffffd7 97  00  10 253 246 227  44  10  99
yellow    #b58900  3/3 yellow   136 #af8700 60  10  65 181 137   0  45 100  71
orange    #cb4b16  9/3 brred    166 #d75f00 50  50  55 203  75  22  18  89  80
red       #dc322f  1/1 red      160 #d70000 50  65  45 220  50  47   1  79  86
magenta   #d33682  5/5 magenta  125 #af005f 50  65 -05 211  54 130 331  74  83
violet    #6c71c4 13/5 brmagenta 61 #5f5faf 50  15 -45 108 113 196 237  45  77
blue      #268bd2  4/4 blue      33 #0087ff 55 -10 -45  38 139 210 205  82  82
cyan      #2aa198  6/6 cyan      37 #00afaf 60 -35 -05  42 161 152 175  74  63
green     #859900  2/2 green     64 #5f8700 60 -20  65 133 153   0  68 100  60
*/
const int solarized_colortable_original[16]={
0x002b36, //base03
0x073642, //base02
0x586e75, //base01
0x657b83, //base00
0x839496, //base0
0x93a1a1, //base1   (5)
0xeee8d5, //base2
0xfdf6e3, //base3
0xb58900, //yellow
0xcb4b16, //orange
0xdc322f, //red     (10)
0xd33682, //magenta
0x6c71c4, //violet
0x268bd2, //blue
0x2aa198, //cyan
0x859900, //green    (15)
};




//Modo de grises activo
//0: colores normales
//1: componente Blue
//2: componente Green
//4: componente Red
//Se pueden sumar para diferentes valores
int screen_gray_mode=0;

z80_bit inverse_video;


//Indica que el driver de video (por el momento, solo xwindows y fbdev) debe repintar la pantalla
//teniendo en cuenta si hay menu activo, y por tanto evitar pintar zonas donde hay texto del menu
//esta variable la pone a 1 el driver de xwindows y fbdev
int screen_refresh_menu=0;


//si esta activado real video
z80_bit rainbow_enabled;

//Si hay autodetecccion de modo rainbow
z80_bit autodetect_rainbow;


//Valores usados en real video
//normalmente a 8
int screen_invisible_borde_superior;
//normalmente a 56.
int screen_borde_superior;

//estos dos anteriores se suman aqui. es 64 en 48k, y 63 en 128k. por tanto, uno de los dos valores vale 1 menos
int screen_indice_inicio_pant;

//suma del anterior+192
int screen_indice_fin_pant;

//normalmente a 56
int screen_total_borde_inferior;

//normalmente a 48
int screen_total_borde_izquierdo;
//lo mismo en t_estados
int screen_testados_total_borde_izquierdo;

//normalmente a 48
int screen_total_borde_derecho;

//normalmente a 96
int screen_invisible_borde_derecho;

//lo mismo pero en testados
//int screen_invisible_testados_borde_derecho;


//Total de scanlines. usualmente 312 o 311
int screen_scanlines;

//Total de t_estados por linea
int screen_testados_linea;

//Total de t_estados de un frame entero
int screen_testados_total;


//donde empieza borde derecho, en testados
int screen_testados_indice_borde_derecho;


int temp_borrar=3;

//Estos valores se inicializaran la primera vez en set_machine
int get_total_ancho_rainbow_cached;

int temp_borrar2=0;

int get_total_alto_rainbow_cached;


//buffer border de linea actual
//#define BORDER_ARRAY_LENGTH 228+24 -> en screen.h
//24 de screen_total_borde_izquierdo/2
//Un scanline empieza con display, border derecho, retrace horizontal y borde izquierdo. y ahi se inicia una nueva scanline
//A la hora de dibujar en nuestra rutina consideramos: borde izq-display-borde derecho-retrace
//Pero el border en cambio si que lo guardamos teniendo en cuenta esto,
//sentencias out guardan valor de border comenzando en posicion 24
//z80_byte buffer_border[BORDER_ARRAY_LENGTH];

//Array de border que guarda colores teniendo en cuenta toda la pantalla (para cada t-estado posible)
//+xx para que haya un margen por debajo por si la funcion de lectura de border en screen.c se va de array
z80_byte fullbuffer_border[MAX_FULLBORDER_ARRAY_LENGTH+MAX_STATES_LINE];

//pixeles y atributos
z80_byte scanline_buffer[SCANLINEBUFFER_ONE_ARRAY_LENGTH*MAX_CPU_TURBO_SPEED];

//ultima posicion leida en buffer_atributos
int last_x_atributo;



//frames que hemos saltado
int framedrop_total=0;
//frames que se iban a dibujar, saltados o no... cuando se llega a 50, se resetea, y se muestra por pantalla los drop
int frames_total=0;

//ha llegado la interrupcion de final de frame antes de redibujar la pantalla. Normalmente no dibujar ese frame para ganar velocidad
int framescreen_saltar;

//Se hace auto frameskip
z80_bit autoframeskip={1};

//Ultimo FPS leido. Para mostrar en consola o debug menu
int ultimo_fps=0;

//vofile
FILE *ptr_vofile;
char *vofilename;

z80_bit vofile_inserted;

//fps del archivo final=50/vofile_fps
int vofile_fps=10;

int vofile_frame_actual;


//Para deteccion de realvideo segun veces que cambia el border
//Veces que ha cambiado el color del border en un mismo frame
int detect_rainbow_border_changes_in_frame=0;
//Numero de frames seguidos en que el border se ha cambiado cada frame mas o igual de DETECT_RAINBOW_BORDER_MAX_IN_FRAMES veces
int detect_rainbow_border_total_frames=0;


//Colores usados en pantalla panic, en logo de extended desktop etc. Son los colores del rainbow de spectrum
//rojo, amarillo, verde, cyan,negro
int screen_colores_rainbow[]={2+8,6+8,4+8,5+8,0};

int screen_colores_rainbow_nobrillo[]={2,6,4,5,0};

//Retorna 1 si el driver grafico es completo
int si_complete_video_driver(void)
{
        if (!strcmp(scr_new_driver_name,"xwindows")) return 1;
        if (!strcmp(scr_new_driver_name,"sdl")) return 1;
        if (!strcmp(scr_new_driver_name,"fbdev")) return 1;
        if (!strcmp(scr_new_driver_name,"cocoa")) return 1;
        return 0;
}


//Retorna 1 si el driver grafico permite menu normal
int si_normal_menu_video_driver(void)
{

	//printf ("video driver: %s\n",scr_new_driver_name);

	if (si_complete_video_driver() ) return 1;

        return 0;
}

//establece valor de screen_indice_inicio_pant y screen_indice_fin_pant
void screen_set_video_params_indices(void)
{
	screen_indice_inicio_pant=screen_invisible_borde_superior+screen_borde_superior;

		screen_indice_fin_pant=screen_indice_inicio_pant+192;

	screen_scanlines=screen_indice_fin_pant+screen_total_borde_inferior;
	//screen_testados_linea=(screen_invisible_borde_izquierdo+screen_total_borde_izquierdo+256+screen_total_borde_derecho)/2;

	screen_testados_total=screen_testados_linea*screen_scanlines;

	//TODO. hacer esto de manera mas elegante



	screen_testados_total_borde_izquierdo=screen_total_borde_izquierdo/2;

	screen_testados_indice_borde_derecho=screen_testados_total_borde_izquierdo+128;

	//printf ("t_estados_linea: %d\n",screen_testados_linea);
	//printf ("scanlines: %d\n",screen_scanlines);
	//printf ("t_estados_total: %d\n",screen_testados_total);
	//sleep(2);

}


//el formato del buffer del video rainbow es:
//1 byte por pixel, cada pixel tiene el valor de color 0..15.
//Valores 16..255 no tienen sentido, de momento
//Valores mas alla de 255 son usados en ulaplus. ver tablas exactas
z80_int *rainbow_buffer=NULL;


//cache de putpixel. Solo usado en modos rainbow (segun recuerdo)
z80_int *putpixel_cache=NULL;


//funcion con debug. usada en el macro con debug

void store_value_rainbow_debug(z80_int **p, z80_int valor)
{
	z80_int *puntero_buf_rainbow;
	puntero_buf_rainbow=*p;

        int ancho,alto,tamanyo;

	ancho=screen_get_emulated_display_width_no_zoom();
	alto=screen_get_emulated_display_height_no_zoom();


        tamanyo=ancho*alto*2;

        //Asignamos mas bytes dado que la ultima linea de pantalla genera datos (de borde izquierdo) mas alla de donde corresponde
        tamanyo=tamanyo+ancho;

	tamanyo*=MAX_CPU_TURBO_SPEED;


	if (puntero_buf_rainbow==NULL) {
		printf ("puntero_buf_rainbow NULL\n");
		return ;
	}

	if (puntero_buf_rainbow>(rainbow_buffer+tamanyo)) {
		//printf ("puntero_buf_rainbow mayor limite final: %d (max %d)\n",puntero_buf_rainbow-rainbow_buffer,tamanyo);
		return;
	}

	if (puntero_buf_rainbow<rainbow_buffer) {
                //printf ("puntero_buf_rainbow menor que inicial: -%d\n",rainbow_buffer-puntero_buf_rainbow);
                return;
        }


	*puntero_buf_rainbow=valor;
	(*p)++;

}





void recalcular_get_total_ancho_rainbow(void)
{
		get_total_ancho_rainbow_cached=2*TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v+512;
}

//sin contar la parte invisible
void recalcular_get_total_alto_rainbow(void)
{
        debug_printf (VERBOSE_INFO,"Recalculate get_total_alto_rainbow");
		get_total_alto_rainbow_cached=(TBBLUE_TOP_BORDER_NO_ZOOM+TBBLUE_BOTTOM_BORDER_NO_ZOOM)*border_enabled.v+384;
}

//esas dos funciones, get_total_ancho_rainbow y get_total_alto_rainbow ahora son macros definidos en screen.h


void init_rainbow(void)
{
	int ancho,alto,tamanyo;

        ancho=screen_get_emulated_display_width_no_zoom();
        alto=screen_get_emulated_display_height_no_zoom();


	tamanyo=ancho*alto*2; //buffer de 16 bits (*2 bytes)

	//Asignamos mas bytes dado que la ultima linea de pantalla genera datos (de borde izquierdo) mas alla de donde corresponde
	tamanyo=tamanyo+ancho;

	tamanyo*=MAX_CPU_TURBO_SPEED;

	debug_printf (VERBOSE_INFO,"Initializing two rainbow video buffer of size: %d x %d , %d bytes each",ancho,alto,tamanyo);


        if (rainbow_buffer) free(rainbow_buffer);

	rainbow_buffer=malloc(tamanyo);
	if (rainbow_buffer==NULL) {
		cpu_panic("Error allocating rainbow video buffer");
	}
}

void init_cache_putpixel(void)
{

#ifdef PUTPIXELCACHE
	debug_printf (VERBOSE_INFO,"Initializing putpixel_cache");
	if (putpixel_cache!=NULL) {
		debug_printf (VERBOSE_INFO,"Freeing previous putpixel_cache");
		free(putpixel_cache);
	}

        int ancho,alto,tamanyo;


        //ancho=screen_get_emulated_display_width_no_zoom();
        //alto=screen_get_emulated_display_height_no_zoom();
	//Incluir en tamanyo el footer
        //ancho=screen_get_window_size_width_no_zoom_border_en();
        //alto=screen_get_window_size_height_no_zoom_border_en();
        ancho=screen_get_window_size_width_no_zoom();
        alto=screen_get_window_size_height_no_zoom();


	//El tamanyo depende del footer. Pero no del border (siempre se incluye con border)


        tamanyo=ancho*alto;
	//para poder hacer putpixel cache con zoom y*2 y interlaced, doble de tamanyo
	tamanyo *=2;

	//para poder hacer putpixel cache con modo timex 512x192
	tamanyo *=2;


	putpixel_cache=malloc(tamanyo*2); //*2 porque es z80_int


	debug_printf (VERBOSE_INFO,"Initializing putpixel_cache of size: %d bytes",tamanyo);

	if (putpixel_cache==NULL) {
		cpu_panic("Error allocating putpixel_cache video buffer");
	}

	clear_putpixel_cache();
#else
	debug_printf (VERBOSE_INFO,"Putpixel cache disabled on compilation time");
#endif

}


//#define put_putpixel_cache(x,y) putpixel_cache[x]=y


//rutina para comparar un caracter
//entrada:
//p: direccion de pantalla en sp


//salida:
//caracter que coincide
//0 si no hay coincidencia

//inverse si o no

//sprite origen a intervalos de "step"
z80_byte compare_char_tabla_step(z80_byte *origen,z80_byte *inverse,z80_byte *tabla_leemos,int step) {

        z80_byte *copia_origen;
	z80_byte *tabla_comparar;


        z80_byte caracter=32;

        for (;caracter<128;caracter++) {
                //printf ("%d\n",caracter);
                //tabla_leemos apunta siempre al primer byte de la tabla del caracter que leemos
                tabla_comparar=tabla_leemos;
                copia_origen=origen;

                //tabla_comparar : puntero sobre la tabla de caracteres
                //copia_origen: puntero sobre la pantalla

                //
                int numero_byte=0;
                for (numero_byte=0; (numero_byte<8) && (*copia_origen == *tabla_comparar) ;numero_byte++,copia_origen+=step,tabla_comparar++) {
                }

                if (numero_byte == 8) {
                        *inverse=0;
                        return caracter;
                }


                //probar con texto inverso

                numero_byte=0;
                for (numero_byte=0; (numero_byte<8) && ((*copia_origen ^ *tabla_comparar) == 255) ;numero_byte++,copia_origen+=step,tabla_comparar++) {
                }

                if (numero_byte == 8) {
                        *inverse=1;
                        return caracter;
                }


                tabla_leemos +=8;
        }



        return 0;
}

//comparar sprite de origen (con direccionamiento de spectrum, cada linea a intervalo 256) con la tabla de caracteres de la ROM
z80_byte compare_char_tabla(z80_byte *origen,z80_byte *inverse,z80_byte *tabla_leemos) {
	return compare_char_tabla_step(origen,inverse,tabla_leemos,256);
}



//devuelve 255 si no coincide
z80_byte compare_char_tabla_rainbow(z80_byte *origen,z80_byte *inverse,z80_byte *tabla_leemos) {

        z80_byte *tabla_comparar;


        z80_byte caracter=0;



        for (;caracter<64;caracter++) {
                //printf ("%d\n",caracter);
                //tabla_leemos apunta siempre al primer byte de la tabla del caracter que leemos
                tabla_comparar=tabla_leemos;
                //copia_origen=origen;

                //tabla_comparar : puntero sobre la tabla de caracteres
                //copia_origen: puntero sobre la pantalla

                //
                int numero_byte=0;
                for (numero_byte=0; (numero_byte<8) && (origen[numero_byte] == *tabla_comparar) ;numero_byte++,tabla_comparar++) {
                }

                if (numero_byte == 8) {
                        *inverse=0;
                        return caracter;
                }


                //probar con texto inverso
               	numero_byte=0;
                for (numero_byte=0; (numero_byte<8) && ((origen[numero_byte] ^ *tabla_comparar) == 255 ) ;numero_byte++,tabla_comparar++) {
       	        }

               	if (numero_byte == 8) {
			*inverse=1;
                        return caracter;
       	        }


                tabla_leemos +=8;
        }



        return 255;
}



z80_byte compare_char_step(z80_byte *origen,z80_byte *inverse,int step)
{
        z80_byte *tabla_leemos;
	z80_byte caracter;

	//Tenemos que buscar en toda la tabla de caracteres. Primero en tabla conocida y luego en la que apunta a 23606/7
	//Tabla conocida es la del spectrum, pero tambien vale para ZX81
	tabla_leemos=char_set_spectrum;

	caracter=compare_char_tabla_step(origen,inverse,tabla_leemos,step);
	if (caracter!=0) return caracter;
	
	
	//si no consultamos a 23606/7, retornar 0
	if (ocr_settings_not_look_23606.v) return 0;

	return caracter;
}

z80_byte compare_char(z80_byte *origen,z80_byte *inverse)
{
	return compare_char_step(origen,inverse,256);
}

z80_int devuelve_direccion_pantalla_no_table(z80_byte x,z80_byte y)
{

        z80_byte linea,high,low;

        linea=y/8;

        low=x+ ((linea & 7 )<< 5);
        high= (linea  & 24 )+ (y%8);



        return low+high*256;
}

void init_screen_addr_table(void)
{

	int x,y;
	int index=0;
	z80_int direccion;

	screen_addr_table=malloc(6144*2);
	if (screen_addr_table==NULL) {
		cpu_panic ("Error allocating sprite table");
	}


	for (y=0;y<192;y++) {
                for (x=0;x<32;x++) {
                direccion=devuelve_direccion_pantalla_no_table(x,y);
		screen_addr_table[index++]=direccion;
		}
	}

}



int scr_si_color_oscuro(void)
{

	//desactivamos por completo el cambio a color oscuro. Y como consecuencia tambien, el cambio a blanco y negro cuando esta menu abierto y multitask off
	return 0;

	if (menu_overlay_activo) {
                if (menu_abierto==1) {
			return 1;
                }

		else {
			//si no estamos en menu, hacerlo solo cuando este splash   //o guessing tape
			if (menu_splash_text_active.v) return 1;
			//if (tape_guessing_parameters) return 1;
		}
	}

	return 0;
}


void scr_refresca_border_comun_spectrumzx8081(unsigned int color)
{
//      printf ("Refresco border\n");

        int x,y;


	//Top border cambia en spectrum y zx8081 y ace
	int topborder=TOP_BORDER;

        //parte superior
        for (y=0;y<topborder;y++) {
                for (x=0;x<ANCHO_PANTALLA*zoom_x+LEFT_BORDER*2;x++) {
                                scr_putpixel(x,y,color);
                }
        }

        //parte inferior
        for (y=0;y<BOTTOM_BORDER;y++) {
                for (x=0;x<ANCHO_PANTALLA*zoom_x+LEFT_BORDER*2;x++) {
                                scr_putpixel(x,topborder+y+ALTO_PANTALLA*zoom_y,color);


                }
        }


        //laterales
        for (y=0;y<ALTO_PANTALLA*zoom_y;y++) {
                for (x=0;x<LEFT_BORDER;x++) {
                        scr_putpixel(x,topborder+y,color);
                        scr_putpixel(LEFT_BORDER+ANCHO_PANTALLA*zoom_x+x,topborder+y,color);
                }

        }


}

void scr_refresca_border(void)
{
	int color;

	color=out_254 & 7;

	if (scr_refresca_sin_colores.v) color=7;

	scr_refresca_border_comun_spectrumzx8081(color);
}


void screen_tbblue_refresca_pantalla(void)
{

                //modo clasico. sin rainbow
                if (rainbow_enabled.v==0) {
                        screen_tbblue_refresca_no_rainbow();
                }

                else {
                        //modo rainbow - real video
                        //en spectrum normal era: scr_refresca_pantalla_rainbow_comun();
//scr_refresca_pantalla_rainbow_comun(); //Se puede usar esta funcion comun a todos

			screen_tbblue_refresca_rainbow();
                }

}

void clear_putpixel_cache(void)
{

#ifdef PUTPIXELCACHE

    if (putpixel_cache==NULL) return;

	debug_printf (VERBOSE_INFO,"Clearing putpixel cache");




	int tamanyo_y;

	tamanyo_y=screen_get_window_size_height_no_zoom_border_en();

	int tamanyo_x;

	tamanyo_x=screen_get_window_size_width_no_zoom_border_en();

	if (timex_si_modo_512() ) tamanyo_x *=2;


	//printf ("Clearing putpixel cache %d X %d\n",tamanyo_x,tamanyo_y);
	/*int x,y;
	int indice=0;
	
	for (y=0;y<tamanyo_y;y++) {
		for (x=0;x<tamanyo_x;x++) {
			//cambiar toda la cache
			//ponemos cualquier valor que no pueda existir, para invalidarla
			putpixel_cache[indice]=65535;

			indice++;
		}
	}*/

	//Alternativa con memset mas rapido
	int longitud=tamanyo_y*tamanyo_x*2; //*2 porque es un z80_int
	memset(putpixel_cache,255,longitud);

	//printf ("clear putpixel cache get_total_ancho_rainbow=%d get_total_alto_rainbow=%d \n",get_total_ancho_rainbow(),get_total_alto_rainbow() );
#endif

}

//putpixel escalandolo al zoom necesario y teniendo en cuenta toda la pantalla entera (rainbow)
//y con cache
//por tanto, (0,0) = arriba izquierda del border
void scr_putpixel_zoom_rainbow_mas_de_uno(int x,int y,unsigned int color)
{

#ifdef PUTPIXELCACHE
	int indice_cache;

	indice_cache=(get_total_ancho_rainbow()*y)+x;

	if (putpixel_cache[indice_cache]==color) return;

	//printf ("not in cache: x %d y %d\n",x,y);
	//put_putpixel_cache(indice_cache,color);
	putpixel_cache[indice_cache]=color;
#endif

        int zx,zy;
        int xzoom=x*zoom_x;
        int yzoom=y*zoom_y;


        //Escalado a zoom indicado
        for (zx=0;zx<zoom_x;zx++) {
        	for (zy=0;zy<zoom_y;zy++) {
                        scr_putpixel(xzoom+zx,yzoom+zy,color);
                }
        }

}


//putpixel escalandolo con zoom 1 - sin escalado
//y con cache
//por tanto, (0,0) = arriba izquierda del border
void scr_putpixel_zoom_rainbow_uno(int x,int y,unsigned int color)
{

#ifdef PUTPIXELCACHE
        int indice_cache;

        indice_cache=(get_total_ancho_rainbow()*y)+x;

        if (putpixel_cache[indice_cache]==color) return;

        //printf ("not in cache: x %d y %d\n",x,y);
        //put_putpixel_cache(indice_cache,color);
        putpixel_cache[indice_cache]=color;
#endif

	scr_putpixel(x,y,color);
}


//putpixel escalandolo al zoom necesario y teniendo en cuenta el border
//por tanto, (0,0) = dentro de pantalla
void scr_putpixel_zoom_mas_de_uno(int x,int y,unsigned int color)
{

#ifdef PUTPIXELCACHE
	int indice_cache;

					indice_cache=(get_total_ancho_rainbow()*(TBBLUE_TOP_BORDER_NO_ZOOM*border_enabled.v+y)) + TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v+x;

	if (putpixel_cache[indice_cache]==color) return;

	//printf ("scr_putpixel_zoom not in cache: x %d y %d indice_cache=%d \n",x,y,indice_cache);
	//put_putpixel_cache(indice_cache,color);
	putpixel_cache[indice_cache]=color;
#endif

        int zx,zy;
	int offsetx,offsety;
		offsetx=TBBLUE_LEFT_BORDER*border_enabled.v;
                offsety=TBBLUE_TOP_BORDER*border_enabled.v;
        int xzoom=x*zoom_x;
        int yzoom=y*zoom_y;



	//Escalado a zoom indicado
        for (zx=0;zx<zoom_x;zx++) {
        	for (zy=0;zy<zoom_y;zy++) {
                	scr_putpixel(offsetx+xzoom+zx,offsety+yzoom+zy,color);
		}
	}
}

//putpixel escalandolo a zoom 1 -> no zoom
//por tanto, (0,0) = dentro de pantalla
void scr_putpixel_zoom_uno(int x,int y,unsigned int color)
{

#ifdef PUTPIXELCACHE
        int indice_cache;

            indice_cache=(get_total_ancho_rainbow()*(TBBLUE_TOP_BORDER_NO_ZOOM*border_enabled.v+y)) + TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v+x;

        if (putpixel_cache[indice_cache]==color) return;

        //printf ("scr_putpixel_zoom color %d not in cache: x %d y %d indice_cache=%d contenido=%d\n",color,x,y,indice_cache,putpixel_cache[indice_cache]);
        //put_putpixel_cache(indice_cache,color);
        putpixel_cache[indice_cache]=color;
#endif

	        int offsetx,offsety;

			                offsetx=TBBLUE_LEFT_BORDER*border_enabled.v;
			                offsety=TBBLUE_TOP_BORDER*border_enabled.v;


	scr_putpixel(offsetx+x,offsety+y,color);
}


void set_putpixel_zoom(void)
{
	if (zoom_x==1 && zoom_y==1) {
		scr_putpixel_zoom=scr_putpixel_zoom_uno;
		scr_putpixel_zoom_rainbow=scr_putpixel_zoom_rainbow_uno;
		debug_printf (VERBOSE_INFO,"Setting putpixel functions to zoom 1");
	}

	else {
		scr_putpixel_zoom=scr_putpixel_zoom_mas_de_uno;
		scr_putpixel_zoom_rainbow=scr_putpixel_zoom_rainbow_mas_de_uno;
		debug_printf (VERBOSE_INFO,"Setting putpixel functions to variable zoom");
	}
}

int ancho_layer_menu_machine=0;
int alto_layer_menu_machine=0;


z80_int *buffer_layer_machine=NULL;
z80_int *buffer_layer_menu=NULL;
int tamanyo_memoria_buffer_layer_menu=0;


//Especie de semaforo que indica:
//Pantalla esta siendo actualizada
//o
//Se esta reasignando layers de menu machine
//No se pueden dar las dos condiciones a la vez, pues si esta por debajo redibujando y reasignamos layers, petara todo
int sem_screen_refresh_reallocate_layers=0;


int running_realloc=0;

void scr_reallocate_layers_menu(int ancho,int alto)
{

	debug_printf (VERBOSE_DEBUG,"Allocating memory for menu layers %d X %d",ancho,alto);
	//debug_exec_show_backtrace();

	if (!menu_overlay_activo) {
		//No estrictamente necesario, pero evitamos usos de buffer_layer_menu o machine (especialmente desde thread de redibujo de cocoa) mientras se reasignan layers
		debug_printf (VERBOSE_DEBUG,"Returning reallocate layers as there are no active menu");
		return;
	}

	//Si el tamanyo anterior es igual que ahora, no tiene sentido tocarlo
	if (ancho_layer_menu_machine==ancho && alto_layer_menu_machine==alto) {
		debug_printf (VERBOSE_DEBUG,"Returning reallocate layers as the current size is the same as the new (%d X %d)",ancho,alto);
		return;
	}


	if (running_realloc) {
		debug_printf (VERBOSE_DEBUG,"Another realloc already running. sem_screen_refresh_reallocate_layers: %d width %d height %d",sem_screen_refresh_reallocate_layers,ancho,alto);
		return;
	}

  if (running_realloc) debug_printf (VERBOSE_DEBUG,"Reallocate layers, screen currently reallocating... wait");

	while (running_realloc) {
		//printf ("screen currently reallocating... wait\n");
		usleep(100);
	}	

	running_realloc=1;

	//No se puede reasignar layers si esta por debajo refrescando pantalla. Esperar a que finalice
	if (sem_screen_refresh_reallocate_layers) debug_printf (VERBOSE_DEBUG,"Reallocate layers, screen currently redrawing... wait");
	while (sem_screen_refresh_reallocate_layers) {
		//printf ("screen currently redrawing... wait\n");
		usleep(100);
	}

	sem_screen_refresh_reallocate_layers=1;




	ancho_layer_menu_machine=ancho;
	alto_layer_menu_machine=alto;	

	//printf ("antes buffer_layer_machine %p buffer_layer_menu %p\n",buffer_layer_machine,buffer_layer_menu);
	
	//Liberar si conviene
	if (buffer_layer_machine!=NULL) {
		//printf ("liberando buffer_layer_machine\n");
		free (buffer_layer_machine);
		buffer_layer_machine=NULL;
	}

	//printf ("despues si liberar buffer_layer_machine\n");
	
	if (buffer_layer_menu!=NULL) {
		//printf ("Liberando buffer_layer_menu\n");
		free(buffer_layer_menu);
		buffer_layer_menu=NULL;
		tamanyo_memoria_buffer_layer_menu=0;
	}


	//printf ("despues si liberar buffer_layer_menu\n");

	//Asignar
	int numero_elementos=ancho_layer_menu_machine*alto_layer_menu_machine;
	int size_layers=numero_elementos*sizeof(z80_int);

	//printf ("Asignando layer tamanyo %d\n",size_layers);

	buffer_layer_machine=malloc(size_layers);
	buffer_layer_menu=malloc(size_layers);
	tamanyo_memoria_buffer_layer_menu=numero_elementos;


	//printf ("despues buffer_layer_machine %p buffer_layer_menu %p\n",buffer_layer_machine,buffer_layer_menu);

	if (buffer_layer_machine==NULL || buffer_layer_menu==NULL) {
		//printf ("Cannot allocate memory for menu layers\n");
		cpu_panic("Cannot allocate memory for menu layers");	
	}


	//Inicializar layers. Esto puede dar problemas si se llama aqui sin tener el driver de video inicializado del todo
	//por esto hay que tener cuidado en que cuando se llama aqui, esta todo correcto
	//Si esto da problemas, quiza quitar el scr_clear_layer_menu y hacerlo mas tarde
	//o quiza scr_clear_layer_menu no deberia llamar a scr_redraw_machine_layer(); (y llamar a ahi desde otro sitio)

	scr_clear_layer_menu();


	sem_screen_refresh_reallocate_layers=0;	

	running_realloc=0;

}

void scr_init_layers_menu(void)
{
	int ancho,alto;

	ancho=screen_get_window_size_width_zoom_border_en();

  alto=screen_get_window_size_height_zoom_border_en();

	scr_reallocate_layers_menu(ancho,alto);

	//printf("alto: %d\n",alto);

}

void scr_putpixel_layer_menu_no_zoom(int x,int y,int color)
{
	int xzoom=x;
	int yzoom=y;

      
	int xdestino=xzoom;
	int ydestino=yzoom;
	//scr_putpixel(xzoom+zx,yzoom+zy,color);
	if (buffer_layer_menu==NULL) {
		//printf ("scr_putpixel_layer_menu NULL\n"); //?????
	}
	else {
		//Proteger que no se salga de rango
		int offset=ydestino*ancho_layer_menu_machine+xdestino;

		if (offset<tamanyo_memoria_buffer_layer_menu) {
		
			buffer_layer_menu[offset]=color;

			//Y hacer mix
			screen_putpixel_mix_layers(xdestino,ydestino); 														
		}

		else {
			//printf ("fuera de rango %d %d\n",xdestino,ydestino);
		}
	}

												  
                
        
}

void scr_putpixel_layer_menu(int x,int y,int color)
{
	int xzoom=x*zoom_x;
	int yzoom=y*zoom_y;

	int zx,zy;


	//Escalado a zoom indicado
	for (zx=0;zx<zoom_x;zx++) {
		for (zy=0;zy<zoom_y;zy++) {
			int xdestino=xzoom+zx;
			int ydestino=yzoom+zy;
			//scr_putpixel(xzoom+zx,yzoom+zy,color);
			if (buffer_layer_menu==NULL) {
				//printf ("scr_putpixel_layer_menu NULL\n"); //?????
			}
			else {
				//Proteger que no se salga de rango
				int offset=ydestino*ancho_layer_menu_machine+xdestino;

				if (offset<tamanyo_memoria_buffer_layer_menu) {
				
					buffer_layer_menu[offset]=color;

					//Y hacer mix
					screen_putpixel_mix_layers(xdestino,ydestino); 
				}
				else {
					//printf ("fuera de rango %d %d\n",xdestino,ydestino);
				}

			}

											
		}
	}
}

void scr_redraw_machine_layer(void)
{

	debug_printf (VERBOSE_DEBUG,"Redraw machine layer");


	if (scr_putpixel==NULL) return;	

		if (buffer_layer_machine==NULL) return;
		if (!si_complete_video_driver() ) return;	

	int x,y;
	//int posicion=0;

	int ancho_layer=ancho_layer_menu_machine;
	int alto_layer=alto_layer_menu_machine;

	int ancho_ventana=screen_get_window_size_width_zoom_border_en();
  int alto_ventana=screen_get_window_size_height_zoom_border_en();	

	//Si son tamaños distintos, no hacer nada
	if (ancho_ventana!=ancho_layer || alto_ventana!=alto_layer) {
		//printf ("Window size does not match menu layers size\n");
		return;
	}

  
	//Obtener el tamaño menor
	/*
	Por que hacemos esto?
	porque vamos a recorrer el layer de maquina, entero, y redibujar cada pixel en pantalla
	Dado que puede haber diferencias de tamaños entre ambos (al redimensionar ventanas) nos limitamos
	a la zona mas pequeña
	*/
	int ancho,alto;
	if (ancho_layer<ancho_ventana) ancho=ancho_layer;
	else ancho=ancho_ventana;

	if (alto_layer<alto_ventana) alto=alto_layer;
	else alto=alto_ventana;

	for (y=0;y<alto;y++) {
		for (x=0;x<ancho;x++) {
			//printf ("x %d y %d p %p\n",x,y,scr_putpixel_final);
			int posicion=ancho_layer_menu_machine*y+x;
			z80_int color=buffer_layer_machine[posicion];
			scr_putpixel_final(x,y,color);
		}
	}


}

unsigned int screen_get_color_from_rgb(unsigned char red,unsigned char green,unsigned char blue)
{
	return (red<<16)|(green<<8)|blue;
}

void screen_reduce_color_rgb(int percent,unsigned int *red,unsigned int *green,unsigned int *blue)
{
	*red=((*red)*percent)/100;
	*green=((*green)*percent)/100;
	*blue=((*blue)*percent)/100;
}

void screen_get_rgb_components(unsigned int color_rgb,unsigned int *red,unsigned int *green,unsigned int *blue)
{
	*blue=color_rgb & 0xFF;
	color_rgb >>=8;

	*green=color_rgb & 0xFF;
	color_rgb >>=8;

	*red=color_rgb & 0xFF;

}

/*
0=Menu por encima de maquina, si no es transparente
1=Menu por encima de maquina, si no es transparente. Y Color Blanco con brillo es transparente
2=Mix de los dos colores, con control de transparecnai


Otro setting=Maquina bajar brillo, se combina con los anteriores
*/
int screen_menu_mix_method=0; //Por defecto, no mezclar
int screen_menu_mix_transparency=10; //Dice la opacidad de la capa de menu.  Si 100, transparente total. Si 0, opaco total

//Si reducimos brillo de la maquina al abrir el menu, solo vale para metodos 0  y 1
z80_bit screen_menu_reduce_bright_machine={0};

//Color en blanco de y negro de maquina con menu abierto cuando multitask esta off
z80_bit screen_machine_bw_no_multitask={0};

char *screen_menu_mix_methods_strings[]={
	"Over","Chroma","Mix"
};

unsigned int screen_convert_rgb_to_bw(unsigned int color_rgb)
{
					//blanco y negro
				if (!menu_multitarea && menu_abierto && screen_machine_bw_no_multitask.v) {
unsigned int red_machine,green_machine,blue_machine;

					screen_get_rgb_components(color_rgb,&red_machine,&green_machine,&blue_machine);	
					int color_gris=rgb_to_grey(red_machine,green_machine,blue_machine);
					red_machine=green_machine=blue_machine=color_gris;
					color_rgb=screen_get_color_from_rgb(red_machine,green_machine,blue_machine);
				}


	return color_rgb;
}

//Mezclar dos pixeles de layer menu y layer maquina
void screen_putpixel_mix_layers(int x,int y)
{
        //Obtener los dos pixeles
        z80_int color_menu=buffer_layer_menu[y*ancho_layer_menu_machine+x];
        z80_int color_machine=buffer_layer_machine[y*ancho_layer_menu_machine+x];


				unsigned int color_rgb;

				unsigned int color_rgb_menu,color_rgb_maquina;

				unsigned int red_menu,green_menu,blue_menu;
				unsigned int red_machine,green_machine,blue_machine;

				unsigned char red_final,green_final,blue_final;

				z80_int color_indexado;

				int metodo_mix=screen_menu_mix_method & 3;


				switch (metodo_mix) {


					case 1:
        		//Si es transparente menu, o color 15, poner machine
        		if (color_menu==SCREEN_LAYER_TRANSPARENT_MENU || color_menu==ESTILO_GUI_PAPEL_NORMAL) {
							color_indexado=color_machine;
							color_rgb=spectrum_colortable[color_indexado];

							color_rgb=screen_convert_rgb_to_bw(color_rgb);

							if (screen_menu_reduce_bright_machine.v) {
								screen_get_rgb_components(color_rgb,&red_machine,&green_machine,&blue_machine);	
								screen_reduce_color_rgb(50,&red_machine,&green_machine,&blue_machine);	
								color_rgb=screen_get_color_from_rgb(red_machine,green_machine,blue_machine);						
							}							
						}
        		else {
							color_indexado=color_menu;
							color_rgb=spectrum_colortable[color_indexado];
						}

											
					break;

					case 2:

						//Mezclar los dos con control de opacidad, siempre que color_menu no sea transparente
						if (color_menu==SCREEN_LAYER_TRANSPARENT_MENU) {
							color_rgb=spectrum_colortable[color_machine];
							color_rgb=screen_convert_rgb_to_bw(color_rgb);
						}							

						else {
							color_rgb_menu=spectrum_colortable[color_menu];
							

							color_rgb_maquina=spectrum_colortable[color_machine];
							color_rgb_maquina=screen_convert_rgb_to_bw(color_rgb_maquina);

							screen_get_rgb_components(color_rgb_menu,&red_menu,&green_menu,&blue_menu);
							screen_get_rgb_components(color_rgb_maquina,&red_machine,&green_machine,&blue_machine);


							//Mezclarlos			

							screen_reduce_color_rgb(100-screen_menu_mix_transparency,&red_menu,&green_menu,&blue_menu);


							int machine_transparency=screen_menu_mix_transparency;
							screen_reduce_color_rgb(machine_transparency,&red_machine,&green_machine,&blue_machine);

							red_final=red_menu+red_machine;
							green_final=green_menu+green_machine;
							blue_final=blue_menu+blue_machine;

							color_rgb=screen_get_color_from_rgb(red_final,green_final,blue_final);
						}

					break;

					default:
				
        		//Si es transparente menu, poner machine
        		if (color_menu==SCREEN_LAYER_TRANSPARENT_MENU) {
							color_indexado=color_machine;
							color_rgb=spectrum_colortable[color_indexado];

							color_rgb=screen_convert_rgb_to_bw(color_rgb);

							if (screen_menu_reduce_bright_machine.v) {
								screen_get_rgb_components(color_rgb,&red_machine,&green_machine,&blue_machine);	
								screen_reduce_color_rgb(50,&red_machine,&green_machine,&blue_machine);	
								color_rgb=screen_get_color_from_rgb(red_machine,green_machine,blue_machine);						
							}

						}

        		else {
							color_indexado=color_menu;
							color_rgb=spectrum_colortable[color_indexado];
						}


					break;					

				}

				//blanco y negro
				//color_rgb=screen_convert_rgb_to_bw(color_rgb);
				/*if (!menu_multitarea) {
					screen_get_rgb_components(color_rgb,&red_machine,&green_machine,&blue_machine);	
					int color_gris=rgb_to_grey(red_machine,green_machine,blue_machine);
					red_machine=green_machine=blue_machine=color_gris;
					color_rgb=screen_get_color_from_rgb(red_machine,green_machine,blue_machine);
				}*/

				scr_putpixel_final_rgb(x,y,color_rgb);
}



void scr_clear_layer_menu(void)
{
		if (buffer_layer_menu==NULL) return;
		if (!si_complete_video_driver() ) return;

		debug_printf (VERBOSE_DEBUG,"Clearing layer menu");
		//sleep(1);


		int i;
		int size=ancho_layer_menu_machine*alto_layer_menu_machine;
		//printf ("Clearing layer size %d. buffer_layer_menu %p realloc layers %d\n",size,buffer_layer_menu,sem_screen_refresh_reallocate_layers);
		//size/=16;

		//z80_int *initial_p;



		//initial_p=buffer_layer_menu;
		for (i=0;i<size;i++) {
			//if (initial_p!=buffer_layer_menu) {
			//if (buffer_layer_menu==NULL) {
			//if (sem_screen_refresh_reallocate_layers) {
			//	printf ("---i %d %p realloc layers %d\n",i,buffer_layer_menu,sem_screen_refresh_reallocate_layers);
			//	sleep(5);
			//}
			buffer_layer_menu[i]=SCREEN_LAYER_TRANSPARENT_MENU; //color transparente	
		}

		//printf ("After Clearing layer size %d. buffer_layer_menu %p\n",size,buffer_layer_menu);

		//printf ("Before clear putpixel cache\n");
		clear_putpixel_cache();
		//printf ("After clear putpixel cache\n");

		//printf ("End clearing layer menu\n");

}


//Hacer un putpixel en la coordenada indicada pero haciendo tan gordo el pixel como diga zoom_level
//Y sin lanzar zoom_x ni zoom_y
//Usado en help keyboard
void scr_putpixel_gui_no_zoom(int x,int y,int color,int zoom_level)
{ 
	//Hacer zoom de ese pixel si conviene
	int incx,incy;
	for (incy=0;incy<zoom_level;incy++) {
		for (incx=0;incx<zoom_level;incx++) {
			//printf("putpixel %d,%d\n",x+incx,y+incy);
			scr_putpixel_layer_menu_no_zoom(x+incx,y+incy,color);
			
		}
	}
}

//Hacer un putpixel en la coordenada indicada pero haciendo tan gordo el pixel como diga zoom_level
void scr_putpixel_gui_zoom(int x,int y,int color,int zoom_level)
{ 
	//Hacer zoom de ese pixel si conviene
	int incx,incy;
	for (incy=0;incy<zoom_level;incy++) {
		for (incx=0;incx<zoom_level;incx++) {
			//printf("putpixel %d,%d\n",x+incx,y+incy);
			scr_putpixel_layer_menu(x+incx,y+incy,color);
			//if (rainbow_enabled.v==1) scr_putpixel_zoom_rainbow(x+incx,y+incy,color);

			//else scr_putpixel_zoom(x+incx,y+incy,color);
		}
	}
}

void scr_return_margenxy_rainbow(int *margenx_izq,int *margeny_arr)
{

        *margenx_izq=screen_total_borde_izquierdo*border_enabled.v;
        *margeny_arr=screen_borde_superior*border_enabled.v;

		*margenx_izq=TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v;
		*margeny_arr=TBBLUE_TOP_BORDER_NO_ZOOM*border_enabled.v;
}

//Retorna 0 si ese pixel no se debe mostrar debido a tamaño de caracter < 8
int scr_putchar_menu_comun_zoom_reduce_charwidth(int bit)
{

	//Reducciones segun cada tamaño de letra
	int saltar_pixeles_size7;
	int saltar_pixeles_size6[2];
	int saltar_pixeles_size5[3];


	//Escalados por defecto
	//Saltar primer pixel en caso tamaño 7
	saltar_pixeles_size7=0;

	//Saltar primer pixel y ultimo pixel en caso tamaño 6
	saltar_pixeles_size6[0]=0;	
	saltar_pixeles_size6[1]=7;	
	
	//Saltar primer pixel y ultimos pixeles en caso tamaño 5
	saltar_pixeles_size5[0]=0;	
	saltar_pixeles_size5[1]=6;
	saltar_pixeles_size5[2]=7;	

	//Segun tipo de letra
	if (estilo_gui_activo==ESTILO_GUI_MSX)	{
		saltar_pixeles_size7=7;

		saltar_pixeles_size6[0]=7;	
		saltar_pixeles_size6[1]=6;		

		saltar_pixeles_size5[0]=7;	
		saltar_pixeles_size5[1]=6;			
		saltar_pixeles_size5[2]=5;			
	}

	if (estilo_gui_activo==ESTILO_GUI_Z88)	{
		saltar_pixeles_size7=0;

		saltar_pixeles_size6[0]=0;	
		saltar_pixeles_size6[1]=1;	
		
		saltar_pixeles_size5[0]=0;	
		saltar_pixeles_size5[1]=1;
		saltar_pixeles_size5[2]=2;			
	}	

	if (estilo_gui_activo==ESTILO_GUI_SAM) {
		saltar_pixeles_size7=0;

		saltar_pixeles_size6[0]=0;	
		saltar_pixeles_size6[1]=1;	
		
		saltar_pixeles_size5[0]=0;	
		saltar_pixeles_size5[1]=1;
		saltar_pixeles_size5[2]=7;			
	}


	//Los demas se ajustan bien al escalado por defecto


	if (menu_char_width==8) {
		return 1;
	}

	//Si 7, saltar un pixel
	else if (menu_char_width==7) {
		if (bit==saltar_pixeles_size7) {
			return 0;
		}
	}

	//Si 6, saltar dos pixeles
	else if (menu_char_width==6) {
		if (bit==saltar_pixeles_size6[0] || bit==saltar_pixeles_size6[1]) {
			return 0;
		}
	}

	//Si 5, saltar tres pixeles
	else if (menu_char_width==5) {
		if (bit==saltar_pixeles_size5[0] || bit==saltar_pixeles_size5[1] || bit==saltar_pixeles_size5[2]) {
			return 0;
		}
	}	


	//Por defecto
	return 1;
}


//Muestra un caracter en pantalla, usado en menu
//entrada: caracter
//x,y: coordenadas en x-0..31 e y 0..23 
//inverse si o no
//ink, paper
//y valor de zoom
void scr_putchar_menu_comun_zoom(z80_byte caracter,int x,int y,z80_bit inverse,int tinta,int papel,int zoom_level)
{

	int color;
  z80_byte bit;
  z80_byte line;
  z80_byte byte_leido;

  //printf ("tinta %d papel %d\n",tinta,papel);

  //margenes de zona interior de pantalla. Para modo rainbow
  int margenx_izq;
  int margeny_arr;

	z80_byte *puntero;
	puntero=&char_set[(caracter-32)*8];


	scr_return_margenxy_rainbow(&margenx_izq,&margeny_arr);

	//temp prueba
	//margenx_izq=margeny_arr=0;

	//Caso de pentagon y en footer
	if (pentagon_timing.v && y>=31) margeny_arr=56*border_enabled.v;
	
  y=y*8;

  for (line=0;line<8;line++,y++) {
		byte_leido=*puntero++;
		if (inverse.v==1) byte_leido = byte_leido ^255;

		int px=0; //Coordenada x del pixel final
		for (bit=0;bit<8;bit++) {
			if (byte_leido & 128 ) color=tinta;
			else color=papel;

   

			byte_leido=(byte_leido&127)<<1;

			//este scr_putpixel_zoom_rainbow tiene en cuenta los timings de la maquina (borde superior, por ejemplo)

			int xfinal,yfinal;

			//xfinal=(((x*menu_char_width)+bit)*zoom_level);

			xfinal=(((x*menu_char_width)+px)*zoom_level);
			yfinal=y*zoom_level;


			//No hay que sumar ya los margenes
			/*if (rainbow_enabled.v==1) {
				xfinal +=margenx_izq;

				yfinal +=margeny_arr;
			}*/




			//Hacer zoom de ese pixel si conviene

		
			//Ancho de caracter 8, 7 y 6 pixeles
			if (scr_putchar_menu_comun_zoom_reduce_charwidth(bit)) {
				scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
				px++;
			}


			/*
			if (menu_char_width==8) {
				scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
				px++;
			}

			//Si 7, saltar primer pixel a la izquierda
			else if (menu_char_width==7) {
				if (bit!=0) {
					scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
					px++;
				}
			}

			//Si 6, saltar dos pixeles: primero izquierda y primero derecha
			else if (menu_char_width==6) {
				if (bit!=0 && bit!=7) {
					scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
					px++;
				}
			}

			//Si 5, saltar tres pixeles: primero izquierda y centro y primero derecha
			else if (menu_char_width==5) {
				if (bit!=0 && bit!=6 && bit!=7) {
					scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
					px++;
				}
			}
			*/


    }
  }
}


void scr_putchar_footer_comun_zoom(z80_byte caracter,int x,int y,z80_bit inverse,int tinta,int papel)
{
	        int color;
        z80_byte bit;
        z80_byte line;
        z80_byte byte_leido;

        //printf ("tinta %d papel %d\n",tinta,papel);

        //margenes de zona interior de pantalla. Para modo rainbow
        int margenx_izq;
        int margeny_arr;

	int zoom_level=1;

	z80_byte *puntero;
	puntero=&char_set[(caracter-32)*8];

        scr_return_margenxy_rainbow(&margenx_izq,&margeny_arr);

        //Caso de pentagon y en footer
        if (pentagon_timing.v && y>=31) margeny_arr=56*border_enabled.v;

        y=y*8;

        for (line=0;line<8;line++,y++) {
          byte_leido=*puntero++;
          if (inverse.v==1) byte_leido = byte_leido ^255;
          for (bit=0;bit<8;bit++) {
                if (byte_leido & 128 ) color=tinta;
                else color=papel;




                byte_leido=(byte_leido&127)<<1;

                //este scr_putpixel_zoom_rainbow tiene en cuenta los timings de la maquina (borde superior, por ejemplo)

                int xfinal,yfinal;

                if (rainbow_enabled.v==1) {
                        //xfinal=(((x*8)+bit)*zoom_level);
                        xfinal=(((x*8)+bit)*zoom_level);
                        xfinal +=margenx_izq;

                        yfinal=y*zoom_level;
                        yfinal +=margeny_arr;
                }

                else {
                        //xfinal=((x*8)+bit)*zoom_level;
                        xfinal=((x*8)+bit)*zoom_level;
                        yfinal=y*zoom_level;
                }


 //Hacer zoom de ese pixel si conviene


                //Ancho de caracter 8, 7 y 6 pixeles
                /*if (menu_char_width==8) scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);

                //Si 7, saltar primer pixel a la izquierda
                else if (menu_char_width==7) {
                        if (bit!=0) scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
                }

                //Si 6, saltar dos pixeles: primero izquierda y primero derecha
                else if (menu_char_width==6) {
                        if (bit!=0 && bit!=7) scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
                }

                //Si 5, saltar tres pixeles: primero izquierda y centro y primero derecha
                else if (menu_char_width==5) {
                        if (bit!=0 && bit!=6 && bit!=7) scr_putpixel_gui_zoom(xfinal,yfinal,color,zoom_level);
                }*/

	                                if (rainbow_enabled.v==1) scr_putpixel_zoom_rainbow(xfinal,yfinal,color);

                                else scr_putpixel_zoom(xfinal,yfinal,color);							

                /*int incx,incy;
                for (incy=0;incy<zoom_level;incy++) {
                        for (incx=0;incx<zoom_level;incx++) {
                                if (rainbow_enabled.v==1) scr_putpixel_zoom_rainbow(xfinal+incx,yfinal+incy,color);

                                else scr_putpixel_zoom(xfinal+incx,yfinal+incy,color);
                        }
                }*/


           }
        }
}								



//Muestra un caracter en footer
//Se utiliza solo al dibujar en zx81/81 y ace, y spectrum (simulado zx81) pero no en menu
//entrada: puntero=direccion a tabla del caracter
//x,y: coordenadas en x-0..31 e y 0..23 del zx81
//inverse si o no
//ink, paper
//si emula fast mode o no
//y valor de zoom
void old_scr_putchar_footer_comun_zoom(z80_byte caracter,int x,int y,z80_bit inverse,int tinta,int papel)
{

        int color;
        z80_byte bit;
        z80_byte line;
        z80_byte byte_leido;

        //printf ("tinta %d papel %d\n",tinta,papel);

        //margenes de zona interior de pantalla. Para modo rainbow
        int margenx_izq;
        int margeny_arr;


	z80_byte *puntero;
	puntero=&char_set[(caracter-32)*8];

	scr_return_margenxy_rainbow(&margenx_izq,&margeny_arr);

	//Caso de pentagon y en footer
	//if (pentagon_timing.v && y>=31) margeny_arr=56*border_enabled.v;
	
        y=y*8;

        for (line=0;line<8;line++,y++) {
          byte_leido=*puntero++;
          if (inverse.v==1) byte_leido = byte_leido ^255;
          for (bit=0;bit<8;bit++) {
                if (byte_leido & 128 ) color=tinta;
                else color=papel;

                //simular modo fast para zx81
	

                byte_leido=(byte_leido&127)<<1;

		//este scr_putpixel_zoom_rainbow tiene en cuenta los timings de la maquina (borde superior, por ejemplo)

		int xfinal,yfinal;

		xfinal=(((x*8)+bit));
		yfinal=y;

		//if (rainbow_enabled.v==1) {
			xfinal +=margenx_izq;

			yfinal +=margeny_arr;
		//}


		//Hacer zoom de ese pixel si conviene		
		scr_putpixel_gui_zoom(xfinal,yfinal,color,1);

		//footer va en capa de machine

		//printf ("%d %d\n",xfinal,yfinal);
		//scr_putpixel_zoom(xfinal,yfinal,color);


           }
        }
}



//Devuelve bit pixel, en coordenadas 0..255,0..191. En pantalla rainbow para zx8081
int scr_get_pixel_rainbow(int x,int y)
{

	z80_byte byte_leido;

	z80_int *puntero_buf_rainbow;

        puntero_buf_rainbow=&rainbow_buffer[ y*get_total_ancho_rainbow()+x ];

	byte_leido=(*puntero_buf_rainbow)&15;
	if (byte_leido==0) return 1;
	else return 0;

}



//Devuelve pixel a 1 o 0, en coordenadas 0..255,0..191. En pantalla de spectrum
int scr_get_pixel(int x,int y)
{

	z80_int direccion;
	z80_byte byte_leido;
	z80_byte bit;
	z80_byte mascara;

       z80_byte *screen=get_base_mem_pantalla();
       direccion=screen_addr_table[(y<<5)]+x/8;
       byte_leido=screen[direccion];


	bit=x%8;
	mascara=128;
	if (bit) mascara=mascara>>bit;
	if ((byte_leido & mascara)==0) return 0;
	else return 1;

}


//Devuelve suma de pixeles a 1 en un cuadrado de 4x4, en coordenadas 0..255,0..191. En pantalla de spectrum
int scr_get_4pixel(int x,int y)
{

	int result=0;
	int dx,dy;

        for (dx=0;dx<4;dx++) {
                for (dy=0;dy<4;dy++) {
			result +=scr_get_pixel(x+dx,y+dy);
		}
	}

	return result;

}


//Devuelve suma de pixeles de colores en un cuadrado de 4x4, en coordenadas 0..255,0..191. En rainbow para zx8081
int scr_get_4pixel_rainbow(int x,int y)
{

	int result=0;
	int dx,dy;

        for (dx=0;dx<4;dx++) {
                for (dy=0;dy<4;dy++) {
                        result +=scr_get_pixel_rainbow(x+dx,y+dy);
                }
        }
        return result;


}



int calcula_offset_screen (int x,int y)
{

        unsigned char high,low;

        low=x+ ((y & 7 )<< 5);
        high= y  & 24;



        return low+high*256;



}





//Retorna 0 si no hay que refrescar esa zona
//Pese a que en cada driver de video, cuando refresca pantalla, luego llama a overlay menu
//Pero en xwindows, se suele producir un refresco por parte del servidor X que provoc
//parpadeo entre la pantalla de spectrum y el menu
//por tanto, es preferible que si esa zona de pantalla de spectrum esta ocupada por algun texto del menu, no repintar para no borrar texto del menu
//Esto incluye tambien el texto de splash del inicio
//No incluiria cualquier otra funcion de overlay diferente del menu o el splash
int scr_ver_si_refrescar_por_menu_activo(int x,int fila)
{


	//Esta funcion ya no tiene sentido. Escribir siempre 
	return 1;

	x /=menu_gui_zoom;
	fila /=menu_gui_zoom;


	if (x>31 || fila>23) return 1;



	//Ver en casos en que puede que haya menu activo y hay que hacer overlay
  if (screen_refresh_menu==1) {
		if (menu_overlay_activo==1) {
                                        //hay menu activo. no refrescar esa coordenada si hay texto del menu
			int pos=fila*32+x;

			if (overlay_usado_screen_array[pos]) {
                                        //if (overlay_screen_array[pos].caracter!=0) {
                                                //no hay que repintar en esa zona
				return 0;
			}

			

		}
	}
	return 1;

}

//putpixel escalandolo a zoom 1 -> no zoom
//por tanto, (0,0) = dentro de pantalla
void scr_putpixel_zoom_timex_mode6(int x,int y,unsigned int color)
{

#ifdef PUTPIXELCACHE
/*
        int indice_cache;

	//printf ("--%d\n",get_total_ancho_rainbow() );

        //indice_cache=(get_total_ancho_rainbow()*(screen_borde_superior*border_enabled.v+y)) + screen_total_borde_izquierdo*border_enabled.v+x;

	// multiplicar por 2 dado que es 512 de ancho
        //indice_cache=(get_total_ancho_rainbow()*2*(screen_borde_superior*border_enabled.v+y)) + screen_total_borde_izquierdo*border_enabled.v+x;

#define ANCHO_TIMEX 512
	indice_cache=ANCHO_TIMEX*y+x;

        if (putpixel_cache[indice_cache]==color) return;

        putpixel_cache[indice_cache]=color;
*/
#endif

                int offsetx,offsety;

	//Aqui se llama ya haciendo 512x192. En caso de zoom 4, pues tenemos que dividir entre dos


        offsetx=LEFT_BORDER*border_enabled.v;
        offsety=TOP_BORDER*border_enabled.v;


	int zx,zy;
        int xzoom=x*zoom_x/2;
        int yzoom=y*zoom_y;

        //Escalado a zoom indicado
        for (zx=0;zx<zoom_x;zx++) {
                for (zy=0;zy<zoom_y;zy++) {
                        scr_putpixel(offsetx+xzoom+zx,offsety+yzoom+zy,color);
                }
        }


}


void scr_refresca_pantalla_timex_512x192(void)
{
        int x,y,bit;
        z80_int direccion;
        z80_byte byte_leido;
        int fila;
        //int zx,zy;

        int col6;
        int tin6, pap6;





       z80_byte *screen=get_base_mem_pantalla();

        //printf ("dpy=%x ventana=%x gc=%x image=%x\n",dpy,ventana,gc,image);
        int x_hi;


				tin6=get_timex_ink_mode6_color();


                //Obtenemos color
                pap6=get_timex_paper_mode6_color();

				//printf ("antes tin6: %d pap6: %d\n",tin6,pap6);


				//Poner brillo1
				tin6 +=8;
				pap6 +=8;

				if (ulaplus_presente.v && ulaplus_enabled.v) {
					//Colores en ulaplus en este modo son:
					/*
BITS INK PAPER BORDER
000  24 31 31
001  25 30 30
010  26 29 29
011  27 28 28
100  28 27 27
101  29 26 26
110  30 25 25
111  31 24 24
					*/

					tin6 +=16;
					pap6 +=16;

					//printf ("tin6: %d pap 6: %d\n",tin6,pap6);

					tin6=ulaplus_palette_table[tin6]+ULAPLUS_INDEX_FIRST_COLOR;
					pap6=ulaplus_palette_table[pap6]+ULAPLUS_INDEX_FIRST_COLOR;
					//printf ("P tin6: %d pap 6: %d\n",tin6,pap6);

				}

				//Si tbblue
					z80_byte attribute_temp=(pap6&7)*8  + (tin6&7) + 64;
					z80_int tinta_temp=tin6;
					z80_int papel_temp=pap6;
					get_pixel_color_tbblue(attribute_temp,&tinta_temp,&papel_temp);

					tin6=tinta_temp;
					pap6=papel_temp;
					tin6=RGB9_INDEX_FIRST_COLOR+tbblue_get_palette_active_ula(tin6);
					pap6=RGB9_INDEX_FIRST_COLOR+tbblue_get_palette_active_ula(pap6);
					//printf ("attr: %d tin6: %d pap6: %d\n",attribute_temp,tin6,pap6);

		z80_int incremento_offset=0;


	//Refrescar border si conviene
	if (border_enabled.v) {
                        //ver si hay que refrescar border
                        if (modificado_border.v)
                        {
                                //printf ("refrescamos border\n");
                                scr_refresca_border_comun_spectrumzx8081(pap6);
                                modificado_border.v=0;
                        }

        }



        for (y=0;y<192;y++) {
                direccion=screen_addr_table[(y<<5)];


                fila=y/8;
                for (x=0,x_hi=0;x<64;x++,x_hi +=8) {


                        //Ver en casos en que puede que haya menu activo y hay que hacer overlay
			//if (1==1) {
                        if (scr_ver_si_refrescar_por_menu_activo(x/2,fila)) {

                                byte_leido=screen[direccion+incremento_offset];


                                for (bit=0;bit<8;bit++) {
					if (byte_leido&128) col6=tin6;
					else col6=pap6;


					//printf ("color: %d\n",col6);

                                        //scr_putpixel(offsetx+x_hi+bit,offsety+y,col6);
					//printf ("x: %d y: %d\n",x_hi+bit,y*2);


					scr_putpixel_zoom_timex_mode6(x_hi+bit,y,col6);

                                        byte_leido=byte_leido<<1;
                                }
                        }

			incremento_offset ^=8192;


                        if (incremento_offset==0) direccion++;
			//printf ("direccion:%d\n",direccion);
                }

        }

}


void screen_generic_putpixel_indexcolour(z80_int *destino,int x,int y,int ancho,int color)
{
	int offset=y*ancho+x;

	destino[offset]=color;
}

int screen_generic_getpixel_indexcolour(z80_int *destino,int x,int y,int ancho)
{
        int offset=y*ancho+x;

        return destino[offset];
}

void screen_put_asciibitmap_generic(char **origen,z80_int *destino,int x,int y,int ancho_orig, int alto_orig, int ancho_destino, void (*putpixel) (z80_int *destino,int x,int y,int ancho_destino,int color), int zoom,int inverso)
{
	int fila,columna;

	for (fila=0;fila<alto_orig;fila++) {
		//int offset_fila=fila*ancho_orig;
		char *texto;
		
		texto=origen[fila];
		for (columna=0;columna<ancho_orig;columna++) {
			char caracter=texto[columna];

			if (caracter!=' ') {
				int color_pixel=return_color_zesarux_ascii(caracter);

				if (inverso) {
					//Se supone que el color esta entre 0 y 15 pero por si acaso
					if (color_pixel>=0 && color_pixel<=15) {
						color_pixel=15-color_pixel;
					}
				}

				int zx,zy;
				for (zx=0;zx<zoom;zx++) {
					for (zy=0;zy<zoom;zy++) {
						putpixel(destino,x+columna*zoom+zx,y+fila*zoom+zy,ancho_destino,color_pixel);
					}
				}
			}
		}
	}
}

int scalled_rainbow_ancho=0;
int scalled_rainbow_alto=0;


void scr_refresca_pantalla_rainbow_unalinea_timex(int y)
{

	//printf ("timex modo 512x192 linea y: %d\n",y);
	//return;

	        int x,bit;
        z80_int direccion;
        z80_byte byte_leido;
        int fila;
        //int zx,zy;

        int col6;
        int tin6, pap6;


       z80_byte *screen=get_base_mem_pantalla();

        //printf ("dpy=%x ventana=%x gc=%x image=%x\n",dpy,ventana,gc,image);
        int x_hi;


                                tin6=get_timex_ink_mode6_color();


                                //Obtenemos color
                                pap6=get_timex_paper_mode6_color();


                                //Poner brillo1
                                tin6 +=8;
                                pap6 +=8;

                                if (ulaplus_presente.v && ulaplus_enabled.v) {
                                        //Colores en ulaplus en este modo son:
                                        /*
BITS INK PAPER BORDER
000 24 31 31
001 25 30 30
010 26 29 29
011 27 28 28
100 28 27 27
101 29 26 26
110 30 25 25
111 31 24 24
                                        */

                                        tin6 +=16;
                                        pap6 +=16;


					tin6=ulaplus_palette_table[tin6]+ULAPLUS_INDEX_FIRST_COLOR;
                                        pap6=ulaplus_palette_table[pap6]+ULAPLUS_INDEX_FIRST_COLOR;
                                }

                z80_int incremento_offset=0;


        
                direccion=screen_addr_table[(y<<5)];


                fila=y/8;
                for (x=0,x_hi=0;x<64;x++,x_hi +=8) {


                        //Ver en casos en que puede que haya menu activo y hay que hacer overlay
                        //if (1==1) {
                        if (scr_ver_si_refrescar_por_menu_activo(x/2,fila)) {

                                byte_leido=screen[direccion+incremento_offset];


                                for (bit=0;bit<8;bit++) {
                                        if (byte_leido&128) col6=tin6;
                                        else col6=pap6;


                                        //printf ("color: %d\n",col6);

                                        //scr_putpixel(offsetx+x_hi+bit,offsety+y,col6);
                                        //printf ("x: %d y: %d\n",x_hi+bit,y*2);


                                                scr_putpixel_zoom_timex_mode6(x_hi+bit,y,col6);

                                        byte_leido=byte_leido<<1;
                                }
                        }

                        incremento_offset ^=8192;


                        if (incremento_offset==0) direccion++;
                        //printf ("direccion:%d\n",direccion);
                }

       

}


//Refresco pantalla con rainbow
void scr_refresca_pantalla_rainbow_comun(void)
{

	//Si es modo timex 512x192, llamar a otra funcion
	if (timex_si_modo_512_y_zoom_par() ) {
		//Si zoom x par
					if (timex_mode_512192_real.v) {
						scr_refresca_pantalla_timex_512x192();
						return;
					}
	}


	//aqui no tiene sentido (o si?) el modo simular video zx80/81 en spectrum
	int ancho,alto;

	ancho=get_total_ancho_rainbow();
	alto=get_total_alto_rainbow();

	int x,y,bit;

	//margenes de zona interior de pantalla. Para overlay menu
	int margenx_izq=screen_total_borde_izquierdo*border_enabled.v;
	int margenx_der=screen_total_borde_izquierdo*border_enabled.v+256;
	int margeny_arr=screen_borde_superior*border_enabled.v;
	int margeny_aba=screen_borde_superior*border_enabled.v+192;

	//para overlay menu tambien
	//int fila;
	//int columna;

	z80_int color_pixel;
	z80_int *puntero;
        z80_int *even_puntero;

	even_puntero=rainbow_buffer;
	int dibujar;


	for (y=0;y<alto;y++) {
		//Truco para tener a partir de una posicion y modo timex 512x192

		int altoborder=screen_borde_superior;
		int linea_cambio_timex=timex_ugly_hack_last_hires-screen_invisible_borde_superior;
	        puntero = even_puntero;
/*
		if (timex_mode_512192_real.v==0 && timex_video_emulation.v && timex_ugly_hack_enabled && timex_ugly_hack_last_hires>0 && 
			y>=linea_cambio_timex && y<192+altoborder && ((zoom_x&1)==0) ) {
			
			scr_refresca_pantalla_rainbow_unalinea_timex(y-altoborder);
			puntero +=ancho;
		}
		else*/ {
		for (x=0;x<ancho;x+=8) {
			dibujar=1;

			//Ver si esa zona esta ocupada por texto de menu u overlay

			if (y>=margeny_arr && y<margeny_aba && x>=margenx_izq && x<margenx_der) {
				if (!scr_ver_si_refrescar_por_menu_activo( (x-margenx_izq)/8, (y-margeny_arr)/8) )
					dibujar=0;
			}


			if (dibujar==1) {
					for (bit=0;bit<8;bit++) {
						color_pixel=*puntero++;
						scr_putpixel_zoom_rainbow(x+bit,y,color_pixel);
					}
			}
			else puntero+=8;

		}
		}

                if (y & 1) even_puntero = puntero;
	}

	//timex_ugly_hack_last_hires=0;



}



//Refresco pantalla sin rainbow
void scr_refresca_pantalla_comun(void)
{
	int x,y,bit;
        z80_int direccion,dir_atributo;
        z80_byte byte_leido;
        int color=0;
        int fila;
        //int zx,zy;

        z80_byte attribute,ink,paper,bright,flash,aux;



       z80_byte *screen=get_base_mem_pantalla();

        //printf ("dpy=%x ventana=%x gc=%x image=%x\n",dpy,ventana,gc,image);
	z80_byte x_hi;

        for (y=0;y<192;y++) {
                //direccion=16384 | devuelve_direccion_pantalla(0,y);

                //direccion=16384 | screen_addr_table[(y<<5)];
                direccion=screen_addr_table[(y<<5)];


                fila=y/8;
                dir_atributo=6144+(fila*32);
                for (x=0,x_hi=0;x<32;x++,x_hi +=8) {


			//Ver en casos en que puede que haya menu activo y hay que hacer overlay
			if (scr_ver_si_refrescar_por_menu_activo(x,fila)) {

                	        byte_leido=screen[direccion];
	                        attribute=screen[dir_atributo];

				//Prueba de un modo de video inventado en que el color de la tinta sale de los 4 bits de la zona de pixeles
				//int ink1,ink2;

				if (scr_refresca_sin_colores.v) {
					attribute=56;
					//ink1=(byte_leido >>4)&0xF;
					//ink2=(byte_leido    )&0xF;					
				}


        	                ink=attribute &7;
                	        paper=(attribute>>3) &7;
	                        bright=(attribute) &64;
        	                flash=(attribute)&128;
                	        if (flash) {
                        	        //intercambiar si conviene
	                                if (estado_parpadeo.v) {
        	                                aux=paper;
                	                        paper=ink;
	                                        ink=aux;
        	                        }
                	        }

				if (bright) {
					ink +=8;
					paper +=8;
				}

                        	for (bit=0;bit<8;bit++) {

					color= ( byte_leido & 128 ? ink : paper );
					//if (scr_refresca_sin_colores.v) {
					//	if (bit<=3) color= ( byte_leido & 128 ? ink1 : paper );
					//	else color= ( byte_leido & 128 ? ink2 : paper );
					//}
					scr_putpixel_zoom(x_hi+bit,y,color);

	                                byte_leido=byte_leido<<1;
        	                }
			}

			//temp
			//else {
			//	printf ("no refrescamos zona x %d fila %d\n",x,fila);
			//}


                        direccion++;
			dir_atributo++;
                }

        }

}




void load_screen(char *scrfile)
{

	{
		debug_printf (VERBOSE_INFO,"Loading Screen File");
		FILE *ptr_scrfile;
		ptr_scrfile=fopen(scrfile,"rb");
                if (!ptr_scrfile) {
			debug_printf (VERBOSE_ERR,"Unable to open Screen file");
		}

		else {

			z80_byte leido;
			int i;
			for (i=0;i<6912;i++) {
				fread(&leido,1,1,ptr_scrfile);
				poke_byte_no_time(16384+i,leido);
			}



			fclose(ptr_scrfile);

		}

	}
}

void save_screen_scr(char *scrfile)
{

                                debug_printf (VERBOSE_INFO,"Saving Screen File");

FILE *ptr_scrfile;
                                  ptr_scrfile=fopen(scrfile,"wb");
                                  if (!ptr_scrfile)
                                {
                                      debug_printf (VERBOSE_ERR,"Unable to open Screen file");
                                  }
                                else {


					z80_byte escrito;
	        	                int i;
        	        	        for (i=0;i<6912;i++) {
						escrito=peek_byte_no_time(16384+i);
						fwrite(&escrito,1,1,ptr_scrfile);
		                        }



	                               fclose(ptr_scrfile);

                                }
}

//Grabar pantalla segun si extension scr, pbm o bmp
void save_screen(char *screen_save_file)
{
	if (!util_compare_file_extension(screen_save_file,"scr")) {
		save_screen_scr(screen_save_file);
	}

	else if (!util_compare_file_extension(screen_save_file,"pbm")) {

		//Asignar buffer temporal
		int longitud=6144;
		z80_byte *buf_temp=malloc(longitud);
		if (buf_temp==NULL) {
				debug_printf(VERBOSE_ERR,"Error allocating temporary buffer");
		}

		//Convertir pantalla a sprite ahi
		z80_byte *origen;
		origen=get_base_mem_pantalla();
		util_convert_scr_sprite(origen,buf_temp);

		util_write_pbm_file(screen_save_file,256,192,8,buf_temp);

		free(buf_temp);


	}

	else if (!util_compare_file_extension(screen_save_file,"bmp")) {

		util_write_screen_bmp(screen_save_file);

	}		

	else {
		debug_printf(VERBOSE_ERR,"Unsuported file type");
		return;
	} 

}



z80_int screen_return_border_ulaplus_color(void)
{
	//En modos lineal (radastan, 5, 7, 9) color del border depende de radaspalbank y sale de ulaplus
	//En resto ulaplus, sale de colores (8-15) de ulaplus
	int offset=8;

	/*
		printf ("%d %d %06X %06X\n",ulaplus_palette_table[screen_border_last_color]+ULAPLUS_INDEX_FIRST_COLOR,ulaplus_palette_table[screen_border_last_color+8]+ULAPLUS_INDEX_FIRST_COLOR,
		spectrum_colortable[ulaplus_palette_table[screen_border_last_color]+ULAPLUS_INDEX_FIRST_COLOR],spectrum_colortable[ulaplus_palette_table[screen_border_last_color+8]+ULAPLUS_INDEX_FIRST_COLOR]
		);
	*/

	return ulaplus_palette_table[screen_border_last_color+offset]+ULAPLUS_INDEX_FIRST_COLOR;
}

void screen_incremento_border_si_ulaplus(void)
{
		//no necesario
		return;
                        //Modos ulaplus (cualquiera) el color del border es del puerto 254, indexado a la tabla de paper
                        if (ulaplus_presente.v && ulaplus_enabled.v) {
                                //screen_border_last_color=screen_border_last_color+ULAPLUS_INDEX_FIRST_COLOR+8;
                                screen_border_last_color=ulaplus_palette_table[screen_border_last_color+8]+ULAPLUS_INDEX_FIRST_COLOR;
                        }

}

//ultimo color leido por rutina de screen_store_scanline_rainbow_border_comun
z80_int screen_border_last_color;



unsigned int screen_store_scanline_border_si_incremento_real(unsigned int color_border)
{
	return color_border;
}


void screen_store_scanline_rainbow_border_comun(z80_int *puntero_buf_rainbow,int xinicial)
{


	int ancho_pantalla=256;

	int t_estados_por_pixel=2;

	int indice_border=t_scanline*screen_testados_linea;
	int inicio_retrace_horiz=indice_border+(ancho_pantalla+screen_total_borde_derecho)/t_estados_por_pixel;
	int final_retrace_horiz=inicio_retrace_horiz+screen_invisible_borde_derecho/t_estados_por_pixel;
	//printf ("indice border: %d inicio_retrace_horiz: %d final_retrace_horiz: %d\n",indice_border,inicio_retrace_horiz,final_retrace_horiz);

	//X inicial de nuestro bucle. Siempre empieza en la zona de display-> al acabar borde izquierdo
	int x=screen_total_borde_izquierdo;

	z80_byte border_leido;

	//Para modo interlace
	int y=t_scanline_draw;

	z80_int color_border;

	color_border=screen_border_last_color;

	color_border=screen_store_scanline_border_si_incremento_real(color_border);

	if (ulaplus_presente.v && ulaplus_enabled.v) {
		//color_border=ulaplus_palette_table[screen_border_last_color+8]+ULAPLUS_INDEX_FIRST_COLOR;
		color_border=screen_return_border_ulaplus_color();
	}

		//En tbblue, color border depends on several machine settings, has also own Timex mode handling
		color_border=tbblue_get_border_color(color_border);


	//Hay que recorrer el array del border para la linea actual
	int final_border_linea=indice_border+screen_testados_linea;
	for (;indice_border<final_border_linea;indice_border++) {
		//obtenemos si hay cambio de border. En tbblue puede que no esté activado
			border_leido=255; 

		if (border_leido!=255) {

			screen_border_last_color=border_leido;
			color_border=screen_border_last_color;

			color_border=screen_store_scanline_border_si_incremento_real(color_border);

			//if (indice_border!=0) printf ("cambio color en indice_border=%d color=%d\n",indice_border,last_color);

			if (ulaplus_presente.v && ulaplus_enabled.v) {
				//color_border=ulaplus_palette_table[screen_border_last_color+8]+ULAPLUS_INDEX_FIRST_COLOR;
				color_border=screen_return_border_ulaplus_color();
			}

					//En tbblue, color border depends on several machine settings, has also own Timex mode handling
					color_border=tbblue_get_border_color(color_border);

		}

		int ancho_rainbow=get_total_ancho_rainbow();

		//Si estamos en x a partir del parametro inicial y Si no estamos en zona de retrace horizontal, dibujar border e incrementar posicion
		if (x>=xinicial) {

			//si nos pasamos de border izquierdo
			if ( (indice_border<inicio_retrace_horiz || indice_border>=final_retrace_horiz) ) {
				//Por cada t_estado van 2 pixeles normalmente
					int jj;
					for (jj=0;jj<t_estados_por_pixel;jj++) {
						store_value_rainbow(puntero_buf_rainbow,color_border);
							puntero_buf_rainbow[ancho_rainbow]=color_border; //pixel de abajo a la derecha
							puntero_buf_rainbow[ancho_rainbow-1]=color_border; //pixel de abajo 
							store_value_rainbow(puntero_buf_rainbow,color_border); //pixel de derecha y incrementamos
							

					}
			}

			//Se llega a siguiente linea
			if (indice_border==inicio_retrace_horiz) {
				y++;
				//En caso de tbblue hay que saltar una linea mas en buffer rainbow, ya que hacemos doble de alto
				if ((y & 1) == 0) puntero_buf_rainbow +=ancho_rainbow;
			}
		}

		//Por cada t_estado van 2 pixeles
		x+=t_estados_por_pixel;

	}


}

//Guardar en buffer rainbow linea actual de borde superior o inferior
void screen_store_scanline_rainbow_border_comun_supinf(void)
{

	int scanline_copia=t_scanline_draw-screen_invisible_borde_superior;

	z80_int *puntero_buf_rainbow;

	int x=screen_total_borde_izquierdo;

	//printf ("%d\n",scanline_copia*get_total_ancho_rainbow());
	//esto podria ser un contador y no hace falta que lo recalculemos cada vez. TODO
	puntero_buf_rainbow=&rainbow_buffer[scanline_copia*get_total_ancho_rainbow()+x];

	//Empezamos desde x en zona display, o sea, justo despues del ancho del borde izquierdo
	screen_store_scanline_rainbow_border_comun(puntero_buf_rainbow,x );


}

//Guardar en buffer rainbow la linea actual. Para Spectrum. solo display
//Tener en cuenta que si border esta desactivado, la primera linea del buffer sera de display,
//en cambio, si border esta activado, la primera linea del buffer sera de border
void screen_store_scanline_rainbow_solo_display(void)
{
		screen_store_scanline_rainbow_solo_display_tbblue();
		return;
}

//Guardar en buffer rainbow linea actual de borde superior o inferior
void screen_store_scanline_rainbow_border_tbblue_supinf(void)
{

	int scanline_copia=t_scanline_draw-screen_invisible_borde_superior;

	z80_int *puntero_buf_rainbow;

	int x=screen_total_borde_izquierdo;

	//printf ("%d\n",scanline_copia*get_total_ancho_rainbow());
	//esto podria ser un contador y no hace falta que lo recalculemos cada vez. TODO
	//int offset=
	puntero_buf_rainbow=&rainbow_buffer[scanline_copia*get_total_ancho_rainbow()+x*2];

	//Empezamos desde x en zona display, o sea, justo despues del ancho del borde izquierdo
	screen_store_scanline_rainbow_border_comun(puntero_buf_rainbow,x );


}





void screen_store_scanline_rainbow_solo_border_tbblue(void)
{


	int ancho_pantalla=256;


        //zona de border superior o inferior. Dibujar desde posicion x donde acaba el ancho izquierdo de borde, linea horizontal
	//hasta derecha del todo, y luego trozo de ancho izquiero del borde de linea siguiente
        if ( (t_scanline_draw>=screen_invisible_borde_superior && t_scanline_draw<screen_indice_inicio_pant) ||
             (t_scanline_draw>=screen_indice_fin_pant && t_scanline_draw<screen_indice_fin_pant+screen_total_borde_inferior)
	   ) {

		screen_store_scanline_rainbow_border_tbblue_supinf();
		//printf ("borde superior o inferior\n");
        }



        //zona de border + pantalla + border
	//Dibujar desde borde derecho hasta borde izquierdo de linea siguiente
        else if (t_scanline_draw>=screen_indice_inicio_pant && t_scanline_draw<screen_indice_fin_pant) {

	        //linea que se debe leer
	        //int scanline_copia=t_scanline_draw-screen_indice_inicio_pant;

        	z80_int *puntero_buf_rainbow;
	        //esto podria ser un contador y no hace falta que lo recalculemos cada vez. TODO
        	int y;

	        y=t_scanline_draw-screen_invisible_borde_superior;

		//nos situamos en borde derecho
		//y se dibujara desde el borde derecho hasta el izquierdo de la siguiente linea
		int offset_derecha=(screen_total_borde_izquierdo+ancho_pantalla)*2; //*2 porque es doble de ancho
		puntero_buf_rainbow=&rainbow_buffer[ y*get_total_ancho_rainbow()+offset_derecha ];


	        screen_store_scanline_rainbow_border_comun(puntero_buf_rainbow,screen_total_borde_izquierdo+ancho_pantalla);

        }




	//primera linea de border. Realmente empieza una linea atras y acaba la primera linea de borde
	//con el borde izquierdo de la primera linea visible
	//Esto solo sirve para dibujar primera linea de border (de ancho izquierdo solamente)

	else if ( t_scanline_draw==screen_invisible_borde_superior-1 ) {
		z80_int *puntero_buf_rainbow;

		puntero_buf_rainbow=&rainbow_buffer[0];

		int xinicial=screen_total_borde_izquierdo+ancho_pantalla+screen_total_borde_derecho+screen_invisible_borde_derecho;
		//printf ("primera linea de borde: %d empezamos en xinicial: %d \n",t_scanline_draw,xinicial);


		//si se ha cambiado el border en la zona superior invisible de border, actualizarlo
		//Esto sucede en aquaplane
		//Quiza habria que buscar en el array de border, en toda la zona inicial que corresponde a la parte no visible de border,
		//el ultimo valor enviado. Pero esto seria muy lento. Basta con leer ultimo valor enviado (esto es aproximado,
		//el valor que tenemos en out_254 es el del final de esta linea actual, que no tiene por que coincidir con el valor de la linea anterior,
		//aunque seria un caso muy raro)

		//screen_border_last_color=out_254 & 7;
		screen_border_last_color=get_border_colour_from_out();


		screen_store_scanline_rainbow_border_comun(puntero_buf_rainbow,xinicial);

	}




}



/*

  Guardar en buffer rainbow la linea actual-solo border. Para Spectrum
  Cada linea en t-estados empieza en la posicion X donde se dibuja la pantalla de "pixels" propiamente, o sea, dentro del border:


    Aqui
      |
      |
      |
      v

  ------------------------
  ------------------------
  ----                ----
  ----                ----
  ----                ----
  ----                ----
  ----                ----
  ----                ----
  ----                ----
  ------------------------
  ------------------------

Por tanto, si estamos en zona inferior o superior del borde, se dibuja partiendo de la posicion X de ancho de border, se llena toda la linea
horizontal, y se dibuja la parte izquierda de borde (de ancho X) de la linea siguiente.
Si estamos en zona central (borde+display+borde) se dibuja desde borde derecho hasta el izquierdo de la siguiente linea
Hay que tener en cuenta que la rutina de dibujar de borde, screen_store_scanline_rainbow_border_comun, recorre siempre todo el array del border
de la linea actual del border (incluso en la zona central) pero no dibuja en pantalla hasta que se alcanza la posicion que se le dice como parametro


*/
void screen_store_scanline_rainbow_solo_border(void)
{
	if (border_enabled.v==0) return;

	screen_store_scanline_rainbow_solo_border_tbblue();
}




void siguiente_frame_pantalla(void)
{
	frames_total++;
        if (frames_total==50) {

                              //contador framedrop
                                if (framedrop_total!=0) {
					//si no hay frameskip forzado
                                        if (!frameskip && ultimo_fps!=50) debug_printf(VERBOSE_INFO,"FPS: %d",ultimo_fps);
                                }


				ultimo_fps=50-framedrop_total;

                                framedrop_total=0;
                                frames_total=0;
        }


	//Gestion de autoactivado de realvideo cuando hay cambios de border
	if (rainbow_enabled.v==0 && autodetect_rainbow.v) {
		//Si el numero de cambios de border en un frame pasa el minimo
		//printf ("numero de cambios: %d\n",detect_rainbow_border_changes_in_frame);
		if (detect_rainbow_border_changes_in_frame>=DETECT_RAINBOW_BORDER_MAX_IN_FRAMES) {

			//printf ("total frames: %d\n",detect_rainbow_border_total_frames);
			//Conteo de frames, incrementar
			if (detect_rainbow_border_total_frames==DETECT_RAINBOW_BORDER_TOTAL_FRAMES) {
				//Activar realvideo
				debug_printf (VERBOSE_INFO,"Enabling realvideo due to repeated border changes. Minimum border changes in frame: %d. Total frames repeated: %d",DETECT_RAINBOW_BORDER_MAX_IN_FRAMES,detect_rainbow_border_total_frames);
				enable_rainbow();
				//Reseteamos contadores, por si se desactiva y vuelve a activar posteriormente
				detect_rainbow_border_changes_in_frame=0;
				detect_rainbow_border_total_frames=0;
			}
			else detect_rainbow_border_total_frames++;
		}

		else {
			//Si no, resetear total frames
			//printf ("no pasa el minimo de cambios. resetear\n");
			detect_rainbow_border_total_frames=0;
		}


		//Nuevo frame. Numero de cambios en frame a 0
		detect_rainbow_border_changes_in_frame=0;
	}

}

char last_message_helper_aofile_vofile_file_format[1024]="";
char last_message_helper_aofile_vofile_util[1088]="";
char last_message_helper_aofile_vofile_bytes_minute_audio[1024]="";
char last_message_helper_aofile_vofile_bytes_minute_video[1024]="";
z80_byte *vofile_buffer;

void print_helper_aofile_vofile(void)
{

         int ancho,alto;


        ancho=screen_get_emulated_display_width_no_zoom_border_en();
        alto=screen_get_emulated_display_height_no_zoom_border_en();


#define AOFILE_TYPE_RAW 0
#define AOFILE_TYPE_WAV 1
//extern int aofile_type;

        char buffer_texto_video[500];
        char buffer_texto_audio[500];


				int audio_bytes_per_second,video_bytes_per_second; //bytes por segundo

				audio_bytes_per_second=FRECUENCIA_SONIDO*2; //*2 porque es stereo en wav
				video_bytes_per_second=ancho*3*alto*(50/vofile_fps);//*3 porque son 24 bits

        sprintf(buffer_texto_video,"-demuxer rawvideo -rawvideo fps=%d:w=%d:h=%d:format=bgr24",50/vofile_fps,ancho,alto);

	if (aofile_type==AOFILE_TYPE_RAW) {
		audio_bytes_per_second /=2; //porque es mono en rwa
        	sprintf(buffer_texto_audio,"-audiofile %s -audio-demuxer rawaudio -rawaudio channels=1:rate=%d:samplesize=1",aofilename,FRECUENCIA_SONIDO);
	}

	if (aofile_type==AOFILE_TYPE_WAV) {
		sprintf(buffer_texto_audio,"-audiofile %s",aofilename);
	}




	if (aofile_inserted.v==1 && vofile_inserted.v==0) {

		if (aofile_type==AOFILE_TYPE_RAW) {
			sprintf(last_message_helper_aofile_vofile_util,"You can convert it with: sox  -t .raw -r %d -b 8 -e unsigned -c 1 %s outputfile.wav",FRECUENCIA_SONIDO,aofilename);
		}

		//Si es wav, texto de conversion vacio
		else {
			last_message_helper_aofile_vofile_util[0]=0;
		}


	}

	if (aofile_inserted.v==0 && vofile_inserted.v==1) {
		sprintf(last_message_helper_aofile_vofile_util,"You can play it with : mplayer %s %s",buffer_texto_video,vofilename);
	}

	if (aofile_inserted.v==1 && vofile_inserted.v==1) {
		sprintf(last_message_helper_aofile_vofile_util,"You can play both audio & video files with : mplayer %s %s %s",buffer_texto_video,buffer_texto_audio,vofilename);
	}

	sprintf(last_message_helper_aofile_vofile_bytes_minute_audio,"Every minute of file uses %d KB",audio_bytes_per_second*60/1024);
	sprintf(last_message_helper_aofile_vofile_bytes_minute_video,"Every minute of file uses %d KB",video_bytes_per_second*60/1024);

	debug_printf(VERBOSE_INFO,"%s",last_message_helper_aofile_vofile_util);

}

void init_vofile(void)
{

                //debug_printf (VERBOSE_INFO,"Initializing Audio Output File");

                ptr_vofile=fopen(vofilename,"wb");
                //printf ("ptr_vofile: %p\n",ptr_vofile);

                if (!ptr_vofile)
                {
                        debug_printf(VERBOSE_ERR,"Unable to create vofile %s",vofilename);
                        vofilename=NULL;
                        vofile_inserted.v=0;
                        return;
                }

         int ancho,alto,tamanyo;
        //ancho=LEFT_BORDER_NO_ZOOM+ANCHO_PANTALLA+RIGHT_BORDER_NO_ZOOM;
        //alto=TOP_BORDER_NO_ZOOM+ALTO_PANTALLA+BOTTOM_BORDER_NO_ZOOM;
	//Si se esta con vofile activo y se cambia de maquina, el buffer tiene que ser suficientemente grande para que quepa,
	//y este buffer se asigna solo al principio. Sino petaria con segmentation fault seguramente

        //ancho=screen_get_emulated_display_width_no_zoom();
        //alto=screen_get_emulated_display_height_no_zoom();
	ancho=720;
	alto=576;
	//esto es mucho mas de lo que necesita


        tamanyo=ancho*alto;

        vofile_buffer=malloc(tamanyo*3);
        if (vofile_buffer==NULL) {
                cpu_panic("Error allocating video output buffer");
        }

	//Hay que activar realvideo dado que el video se genera en base a esto
	enable_rainbow();


	vofile_frame_actual=0;

        vofile_inserted.v=1;

        ancho=screen_get_emulated_display_width_no_zoom_border_en();
        alto=screen_get_emulated_display_height_no_zoom_border_en();


        sprintf(last_message_helper_aofile_vofile_file_format,"Writing video output file, format raw, %d FPS, %d X %d, bgr24",50/vofile_fps,ancho,alto);
        debug_printf(VERBOSE_INFO,"%s",last_message_helper_aofile_vofile_file_format);
	print_helper_aofile_vofile();
}




unsigned char buffer_rgb[3];


/*

Paleta antigua para vofile no usada ya. Usamos misma paleta activa de color

// Paletas VGA en 6 bit, Paleta archivo raw 8 bit, multiplicar por 4
#define BRI0      (42+5)*4
#define BRI1      (16)*4

// Tabla para los colores reales

unsigned char tabla_colores[]={
//      RED       GREEN     BLUE                 G R B
    	0,	  0,        0,			// 0 En SP: 0 0 0 Black
    	0,        0,        BRI0,	      	// 1        0 0 1 Blue
	BRI0,     0,	    0,         		// 2        0 1 0 Red
	BRI0,	  0,	    BRI0,      		// 3        0 1 1 Magenta
	0,	  BRI0,	    0,			// 4        1 0 0 Green
	0,	  BRI0,	    BRI0,		// 5        1 0 1 Cyan
	BRI0,	  BRI0,	    0,			// 6        1 1 0 Yellow
	BRI0,	  BRI0,	    BRI0,		// 7        1 1 1 White


//With brightness

	0,	  0,        0,			// 0        0 0 0 Black
    	0,        0,        BRI0+BRI1, 		// 1        0 0 1 Blue
	BRI0+BRI1,0,	    0,         		// 2        0 1 0 Red
	BRI0+BRI1,0,	    BRI0+BRI1, 		// 3        0 1 1 Magenta
	0,	  BRI0+BRI1,0,			// 4        1 0 0 Green
	0,	  BRI0+BRI1,BRI0+BRI1,		// 5        1 0 1 Cyan
	BRI0+BRI1,BRI0+BRI1,0,			// 6        1 1 0 Yellow
	BRI0+BRI1,BRI0+BRI1,BRI0+BRI1,		// 7        1 1 1 White

};

*/

void convertir_paleta(z80_int valor)
{

	unsigned char valor_r,valor_g,valor_b;

	//colores originales
	//int color=spectrum_colortable_original[valor];

	//colores de tabla activa
	int color=spectrum_colortable[valor];


	valor_r=(color & 0xFF0000) >> 16;
	valor_g=(color & 0x00FF00) >> 8;
	valor_b= color & 0x0000FF;


	buffer_rgb[0]=valor_b;
	buffer_rgb[1]=valor_g;
	buffer_rgb[2]=valor_r;

}


void convertir_color_spectrum_paleta_to_rgb(z80_int valor,int *r,int *g,int *b)
{

	//unsigned char valor_r,valor_g,valor_b;

	//colores de tabla activa
	int color=spectrum_colortable[valor];


	*r=(color & 0xFF0000) >> 16;
	*g=(color & 0x00FF00) >> 8;
	*b= color & 0x0000FF;



}


void vofile_send_frame(z80_int *buffer)
{

        if (vofile_inserted.v==0) return;

	vofile_frame_actual++;
	//printf ("actual %d tope %d\n",vofile_frame_actual,vofile_fps);
	if (vofile_frame_actual!=vofile_fps) return;
	vofile_frame_actual=0;

        int escritos;

         int ancho,alto,tamanyo;

        ancho=screen_get_emulated_display_width_no_zoom_border_en();
        alto=screen_get_emulated_display_height_no_zoom_border_en();


        tamanyo=ancho*alto;

	int origen_buffer=0;
	z80_byte *destino_buffer;
	destino_buffer=vofile_buffer;
	//z80_byte byte_leido;
	z80_int color_leido;


	//printf ("tamanyo: %d vofile_buffer: %p\n",tamanyo,vofile_buffer);

	for (;origen_buffer<tamanyo;origen_buffer++) {
		//byte_leido=*buffer++;
		//convertir_paleta(byte_leido);
		color_leido=*buffer++;
		convertir_paleta(color_leido);
	 	*destino_buffer++=buffer_rgb[0];
	 	*destino_buffer++=buffer_rgb[1];
	 	*destino_buffer++=buffer_rgb[2];
	}


	escritos=fwrite(vofile_buffer,1,tamanyo*3, ptr_vofile);
        if (escritos!=tamanyo*3) {

                        debug_printf(VERBOSE_ERR,"Unable to write to vofile %s",vofilename);
                        vofilename=NULL;
                        vofile_inserted.v=0;

                //debug_printf(VERBOSE_ERR,"Bytes escritos: %d\n",escritos);
                //cpu_panic("Error writing vofile\n");
        }


}

void close_vofile(void)
{

        if (vofile_inserted.v==0) {
                debug_printf (VERBOSE_INFO,"Closing vofile. But already closed");
                return;
        }

        vofile_inserted.v=0;


	debug_printf (VERBOSE_INFO,"Closing vofile type RAW");
	fclose(ptr_vofile);
}

//Resetea algunos parametros de drivers de video, ya seteados a 0 al arrancar
//se llama aqui al cambiar el driver de video en caliente
void screen_reset_scr_driver_params(void)
{
	scr_tiene_colores=0;



	screen_refresh_menu=0;

	scr_messages_debug=NULL;

	esc_key_message="ESC";
}

void screen_set_colour_normal(int index, int colour)
{

	spectrum_colortable_normal[index]=colour;



}

void screen_init_colour_table_siguiente(void)
{

                	int i,r,g,b,valorgris;


		if (screen_gray_mode!=0) {


//Modo de grises activo
//0: colores normales
//1: componente Blue
//2: componente Green
//4: componente Red
//Se pueden sumar para diferentes valores

#define GRAY_MODE_CONST 30
#define GRAY_MODE_CONST_BRILLO 20


	        for (i=0;i<16;i++) {
				valorgris=(i&7)*GRAY_MODE_CONST;

				if (i>=8) valorgris +=GRAY_MODE_CONST_BRILLO;

				VALOR_GRIS_A_R_G_B

				screen_set_colour_normal(i,(r<<16)|(g<<8)|b);

	        }

			//El color 8 es negro, con brillo 1. Pero negro igual
			screen_set_colour_normal(8,0);

			//spectrum_colortable_normal=spectrum_colortable_grises;

			//trama de grises para ulaplus
			//z80_byte color;
			int color32;
                        for (i=0;i<256;i++) {
		                int r,g,b;
		                int valorgris=i;
                		VALOR_GRIS_A_R_G_B

		                color32=(r<<16)|(g<<8)|b;

                                screen_set_colour_normal(ULAPLUS_INDEX_FIRST_COLOR+i, color32);

                        }

												//trama de grises para rgb9
												//z80_byte color;

									for (i=0;i<512;i++) {
									int r,g,b;
									int valorgris=i;
									valorgris=i/2;
									VALOR_GRIS_A_R_G_B

									color32=(r<<16)|(g<<8)|b;

										screen_set_colour_normal(RGB9_INDEX_FIRST_COLOR+i, color32);

									}
			//Colores HEATMAP
			for (i=0;i<256;i++) {
				valorgris=i;
				VALOR_GRIS_A_R_G_B
				screen_set_colour_normal(HEATMAP_INDEX_FIRST_COLOR+i,(r<<16)|(g<<8)|b);
			}

				//Colores Solarized. No los pasamos a grises estos
				for (i=0;i<SOLARIZED_TOTAL_PALETTE_COLOURS;i++) {
					screen_set_colour_normal(SOLARIZED_INDEX_FIRST_COLOR+i,solarized_colortable_original[i]);
				}			


		}

		else {

			//si no gris
			//spectrum_colortable_normal=(int *)spectrum_colortable_original;
			int i;
			int color32;
			int *paleta;
			paleta=screen_return_spectrum_palette();
			for (i=0;i<16;i++) {
				color32=paleta[i];
				//debug_printf(VERBOSE_DEBUG,"Initializing Standard Spectrum Color. Index: %i  Value: %06XH",i,spectrum_colortable_original[i]);
				//screen_set_colour_normal(i,spectrum_colortable_original[i]);
				debug_printf(VERBOSE_PARANOID,"Initializing Standard Spectrum Color. Index: %i  Value: %06XH",i,color32);
				screen_set_colour_normal(i,color32);
			}



			//Colores reales de spectrum 16/48/+
			/*
			for (i=0;i<16;i++) {
                                debug_printf(VERBOSE_DEBUG,"Initializing Standard Spectrum 16/48/+ Real Color. Index: %i  Value: %06XH",i,spectrum_colortable_1648_real[i]);
                                screen_set_colour_normal(SPECCY_1648_REAL_PALETTE_FIRST_COLOR+i,spectrum_colortable_1648_real[i]);
                        }
			*/

			//colores ulaplus
			//ulaplus_rgb_table
			//ULAPLUS_INDEX_FIRST_COLOR

			for (i=0;i<256;i++) {
				color32=ulaplus_rgb_table[i];
				debug_printf(VERBOSE_PARANOID,"Initializing ULAPlus Color. Index: %i  Value: %06XH",i,color32);
				screen_set_colour_normal(ULAPLUS_INDEX_FIRST_COLOR+i, color32);
			}

				//Colores RGB9
				for (i=0;i<512;i++) {
					debug_printf (VERBOSE_PARANOID,"RGB9 color: %02XH 32 bit: %06XH",i,get_rgb9_color(i));
					screen_set_colour_normal(RGB9_INDEX_FIRST_COLOR+i,get_rgb9_color(i));
				}
				//Colores HEATMAP
				for (i=0;i<256;i++) {
					int colorheat=i<<16;
					debug_printf (VERBOSE_PARANOID,"Heatmap color: %02XH 32 bit: %06XH",i,colorheat);
					screen_set_colour_normal(HEATMAP_INDEX_FIRST_COLOR+i,colorheat);
				}

				//Colores Solarized
				for (i=0;i<SOLARIZED_TOTAL_PALETTE_COLOURS;i++) {
					screen_set_colour_normal(SOLARIZED_INDEX_FIRST_COLOR+i,solarized_colortable_original[i]);
				}

					

		}


		//Si video inverso
		if (inverse_video.v==1) {
        	        for (i=0;i<EMULATOR_TOTAL_PALETTE_COLOURS;i++) {
                	        b=spectrum_colortable_normal[i] & 0xFF;
                        	g=(spectrum_colortable_normal[i] >> 8 ) & 0xFF;
	                        r=(spectrum_colortable_normal[i] >> 16 ) & 0xFF;

        	                r=r^255;
                	        g=g^255;
                        	b=b^255;

				screen_set_colour_normal(i,(r<<16)|(g<<8)|b);
			}
		}


                //inicializar tabla de colores oscuro
		/*
                for (i=0;i<EMULATOR_TOTAL_PALETTE_COLOURS;i++) {
                        b=spectrum_colortable_normal[i] & 0xFF;
                        g=(spectrum_colortable_normal[i] >> 8 ) & 0xFF;
                        r=(spectrum_colortable_normal[i] >> 16 ) & 0xFF;

                        r=r/2;
                        g=g/2;
                        b=b/2;

                        spectrum_colortable_oscuro[i]=(r<<16)|(g<<8)|b;
                }
		*/

		//Establecemos tabla actual
                spectrum_colortable=spectrum_colortable_normal;



}

void screen_init_colour_table(void)
{
	/*
	Primero generamos tabla de colores grises. Esa tabla se usa cuando se abre el menu con multitask off, donde los colores se ponen en gris
	TODO: hacerlos en gris y tambien oscuros
	Para ello, se genera tabla con forzado a gris, lo copio a tabla de grises, y luego se genera colores normales
	*/

	debug_printf (VERBOSE_INFO,"Creating colour tables for %d colours",EMULATOR_TOTAL_PALETTE_COLOURS);
	if (EMULATOR_TOTAL_PALETTE_COLOURS>65535) cpu_panic("More than 65536 colours to allocate. This is fatal!");

	int antes_screen_gray_mode=screen_gray_mode;
	screen_gray_mode=7;
	screen_init_colour_table_siguiente();



	//Copiamos de tabla normal, que seran grises, a tabla grises y ademas oscuros
	/*
	int i,r,g,b;
	for (i=0;i<EMULATOR_TOTAL_PALETTE_COLOURS;i++) {

                        b=spectrum_colortable_normal[i] & 0xFF;
                        g=(spectrum_colortable_normal[i] >> 8 ) & 0xFF;
                        r=(spectrum_colortable_normal[i] >> 16 ) & 0xFF;

                        r=r/2;
                        g=g/2;
                        b=b/2;

                        spectrum_colortable_new_blanco_y_negro[i]=(r<<16)|(g<<8)|b;

	}
	*/


	screen_gray_mode=antes_screen_gray_mode;
	screen_init_colour_table_siguiente();

}


int screen_force_refresh=0;

//Retorna 1 si se tiene que refrescar pantalla. Aplica frameskip y autoframeskip
int screen_if_refresh(void)
{

	//Forzado puntual de refresco de pantalla, para que no haga frameskip. Usado por ejemplo en debug cpu
	//Cuando se hace una vez, luego se resetea a 0
	if (screen_force_refresh) {
		screen_force_refresh=0;
		return 1;
	}

	//Si esta en top speed, solo 1 frame
	if (timer_condicion_top_speed() ) {
			if ((top_speed_real_frames%50)!=0) return 0;
		return 1;
	}


	if ( (framescreen_saltar==0 || autoframeskip.v==0) && frameskip_counter==0) {
		return 1;
	}


	return 0;
}



void cpu_loop_refresca_pantalla_return(void)
{
        //Calcular tiempo usado en refrescar pantalla
        core_cpu_timer_refresca_pantalla_difftime=timer_stats_diference_time(&core_cpu_timer_refresca_pantalla_antes,&core_cpu_timer_refresca_pantalla_despues);

        //media de tiempo
        core_cpu_timer_refresca_pantalla_media=(core_cpu_timer_refresca_pantalla_media+core_cpu_timer_refresca_pantalla_difftime)/2;


		TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_cpu_timer_refresca_pantalla);
}
	

void cpu_loop_refresca_pantalla(void)
{

	//Calcular tiempo usado en refrescar pantalla
	timer_stats_current_time(&core_cpu_timer_refresca_pantalla_antes);

	TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_cpu_timer_refresca_pantalla);

	//Para calcular el tiempo entre frames. Idealmente 20 ms
	//Diferencia tiempo
	core_cpu_timer_each_frame_difftime=timer_stats_diference_time(&core_cpu_timer_each_frame_antes,&core_cpu_timer_each_frame_despues);
	//Media de tiempo
	core_cpu_timer_each_frame_media=(core_cpu_timer_each_frame_media+core_cpu_timer_each_frame_difftime)/2;
	//Siguiente tiempo
	timer_stats_current_time(&core_cpu_timer_each_frame_antes);

	//Si esta en top speed, solo 1 frame
	if (timer_condicion_top_speed() ) {

		if (screen_if_refresh() ) {
			//printf ("top_speed_real_frames:%d\n",top_speed_real_frames);
	
			top_speed_real_frames=1;
			debug_printf (VERBOSE_DEBUG,"Refreshing screen on top speed");
			scr_refresca_pantalla();
			frameskip_counter=frameskip;

		}
		cpu_loop_refresca_pantalla_return();
		return;
	}

		//printf ("saltar: %d counter %d\n",framescreen_saltar,frameskip_counter);

				//Si se ha llegado antes a final de frame, y no hay frameskip manual
				//Si no hay autoframeskip, el primer parentesis siempre se cumple
				//Si hay autoframeskip, y se ha tardado mucho en llegar a final de frame (framescreen_saltar>0) , el primer parentesis no se cumple y por tanto no se redibuja pantalla

                                //if ( (framescreen_saltar==0 || autoframeskip.v==0) && frameskip_counter==0) {
				if (screen_if_refresh() ) {
					//printf ("refrescando\n");
                                        scr_refresca_pantalla();
                                        frameskip_counter=frameskip;
                                }


				//Si no se ha llegado a final de frame antes, o hay frameskip manual
                                else {
					//printf ("-no refrescando\n");
                                       if (frameskip_counter) frameskip_counter--;
                                        else debug_printf(VERBOSE_DEBUG,"Framedrop %d",framedrop_total);


                                        framedrop_total++;

                                }

	cpu_loop_refresca_pantalla_return();
}




//Escribe texto en pantalla empezando por en x,y, gestionando salto de linea
//de momento solo se usa en panic para xwindows y fbdev
//ultima posicion y queda guardada en screen_print_y
void screen_print(int x,int y,int tinta,int papel,char *mensaje)
{
	while (*mensaje) {
		scr_putchar_menu(x++,y,*mensaje++,tinta,papel);
		if (x==32) {
			x=0;
			y++;
		}
	}
	screen_print_y=y;
}


void screen_set_parameters_slow_machines(void)
{

	if (no_cambio_parametros_maquinas_lentas.v==1) {
		debug_printf (VERBOSE_INFO,"Parameter nochangeslowparameters enabled. Do not change any frameskip or realvideo parameters");
		return;
	}

	//Parametros por defecto en Raspberry.
#ifdef EMULATE_RASPBERRY

	//Real beeper desactivado pues consume mas cpu (un 7 o 8 % mas en pc)
	if (beeper_real_enabled) {
		beeper_real_enabled=0;
		debug_printf (VERBOSE_INFO,"It is a raspberry system. Disabling Real Beeper");
	}

        if (frameskip<1) {
                frameskip=1;
                debug_printf (VERBOSE_INFO,"It is a raspberry system. Setting frameskip to: %d",frameskip);
                return;
        }

	return;

#endif



}


//Activar rainbow y el estabilizador de imagen de zx8081
void enable_rainbow(void) {

	debug_printf (VERBOSE_INFO,"Enabling RealVideo");

	//si hay un cambio
	if (rainbow_enabled.v==0) {
        	rainbow_enabled.v=1;
		screen_set_parameters_slow_machines();
	}



		/*Modos rainbow usan putpixel cache. Vaciarla por lo que pudiera haber antes
		//Si no se vaciase, si por ejemplo estamos con un programa en basic tipo:
		// 1 border 2: border 3: border 4: pause 1: cls
		//Si cambiamos de realvideo on , a off, y luego a on, al hacer on, que pasara:
		1: vemos franjas de border bien, con realvideo on, y usando putpixel cache
		2: no vemos colores, real video esta a off, y probablemente border 7 entero (lo normal). En modo no real video no usa putpixel cache
		3: volvemos a modo realvideo. Border estaba todo blanco. Como putpixel cache estaba antes con las franjas de colores,
		ahora las franjas estan mas o menos en el mismo sitio, y la cache dice que no hay que redibujarlas. Total: se ve todo el border 7
		*/
		clear_putpixel_cache();

}

//Desactivar rainbow
void disable_rainbow(void) {
	debug_printf (VERBOSE_INFO,"Disabling RealVideo");

	//Vofile necesita de rainbow para funcionar. no dejar desactivarlo si esta esto activo
	if (vofile_inserted.v==1) {
		debug_printf (VERBOSE_ERR,"Video out to file needs realvideo to work. You can not disable realvideo with video out enabled");
		return;
	}

	//si hay un cambio
	if (rainbow_enabled.v==1) {
	        rainbow_enabled.v=0;
		screen_set_parameters_slow_machines();
        }

        modificado_border.v=1;

	//Desactivar estos cuatro. Asi siempre que realvideo sea 0, ninguno de estos tres estara activo
	disable_ulaplus();
}



void enable_border(void)
{
	border_enabled.v=1;
	modificado_border.v=1;
    
	//Recalcular algunos valores cacheados
    recalcular_get_total_ancho_rainbow();
    recalcular_get_total_alto_rainbow();

	//Siempre que se redimensiona tamanyo ventana (sin contar zoom) o se reinicia driver video hay que reiniciar cache putpixel
	init_cache_putpixel();
}

void disable_border(void)
{
    border_enabled.v=0;
	modificado_border.v=1;
    
	//Recalcular algunos valores cacheados
    recalcular_get_total_ancho_rainbow();
    recalcular_get_total_alto_rainbow();

	//Siempre que se redimensiona tamanyo ventana (sin contar zoom) o se reinicia driver video hay que reiniciar cache putpixel
	init_cache_putpixel();
}



void set_t_scanline_draw_zero(void)
{
        t_scanline_draw=0;

}

void t_scanline_next_line(void)
{

        t_scanline_draw++;

        t_scanline++;


}


//Tamanyo pantalla emulada sin contar border
int screen_get_emulated_display_width_no_zoom(void)
{
        return TBBLUE_DISPLAY_WIDTH+TBBLUE_LEFT_BORDER_NO_ZOOM*2;
}

//Tamanyo pantalla emulada sin contar border
int screen_get_emulated_display_height_no_zoom(void)
{
        return TBBLUE_DISPLAY_HEIGHT+TBBLUE_TOP_BORDER_NO_ZOOM+TBBLUE_BOTTOM_BORDER_NO_ZOOM;
}


//Tamanyo pantalla emulada contando border
int screen_get_emulated_display_width_no_zoom_border_en(void)
{
	return TBBLUE_DISPLAY_WIDTH+(TBBLUE_LEFT_BORDER_NO_ZOOM*2)*border_enabled.v;
}

//Tamanyo pantalla emulada contando pantalla + borde inferior - usado en scr*putchar_footer
//Como la funcion screen_get_emulated_display_height_no_zoom_border_en pero sin contar borde superior
int screen_get_emulated_display_height_no_zoom_bottomborder_en(void)
{
        return TBBLUE_DISPLAY_HEIGHT+(TBBLUE_BOTTOM_BORDER_NO_ZOOM)*border_enabled.v;
}



//Tamanyo pantalla emulada contando border
int screen_get_emulated_display_height_no_zoom_border_en(void)
{
	return TBBLUE_DISPLAY_HEIGHT+(TBBLUE_TOP_BORDER_NO_ZOOM+TBBLUE_BOTTOM_BORDER_NO_ZOOM)*border_enabled.v;
}



//Tamanyo pantalla emulada sin contar border, y multiplicando por zoom
int screen_get_emulated_display_width_zoom(void)
{
	return screen_get_emulated_display_width_no_zoom()*zoom_x;
}

//Tamanyo pantalla emulada sin contar border, y multiplicando por zoom
int screen_get_emulated_display_height_zoom(void)
{
	return screen_get_emulated_display_height_no_zoom()*zoom_y;
}


//Tamanyo pantalla emulada contando border y multiplicando por zoom
int screen_get_emulated_display_width_zoom_border_en(void)
{
        return screen_get_emulated_display_width_no_zoom_border_en()*zoom_x;
}

//Tamanyo pantalla emulada contando border y multiplicando por zoom
int screen_get_emulated_display_height_zoom_border_en(void)
{
        return screen_get_emulated_display_height_no_zoom_border_en()*zoom_y;
}



//Tamanyo ventana contando border y multiplicando por zoom
//Suele ser el mismo tamanyo de la pantalla emulada pero sumando el margen inferior
int screen_get_window_size_height_zoom_border_en(void)
{
	return screen_get_emulated_display_height_zoom_border_en()+WINDOW_FOOTER_SIZE*zoom_y;
}

//Tamanyo ventana contando border y multiplicando por zoom
//Suele ser el mismo tamanyo de la pantalla emulada pero sumando el margen inferior
int screen_get_window_size_width_zoom_border_en(void)
{
        return screen_get_emulated_display_width_zoom_border_en()+0;
}


int screen_get_window_size_height_no_zoom_border_en(void)
{
        return screen_get_emulated_display_height_no_zoom_border_en()+WINDOW_FOOTER_SIZE;
}


int screen_get_window_size_width_no_zoom_border_en(void)
{
        return screen_get_emulated_display_width_no_zoom_border_en()+0;
}

int screen_get_window_size_height_no_zoom(void)
{
	return screen_get_emulated_display_height_no_zoom()+WINDOW_FOOTER_SIZE;
}

int screen_get_window_size_width_no_zoom(void)
{
        return screen_get_emulated_display_width_no_zoom()+0;
}


//refresco de pantalla, 2 veces, para que cuando haya modo interlaced o gigascreen y multitask on, se dibujen los dos frames, el par y el impar
void all_interlace_scr_refresca_pantalla(void)
{
    scr_refresca_pantalla();

	//Modo timex real necesita esto
	if (timex_video_emulation.v && (timex_mode_512192_real.v || timex_ugly_hack_enabled)) clear_putpixel_cache();
}



//devuelve sprite caracter de posicion rainbow
//0,0 -> inicio rainbow
//El sprite de 8x8 en posicion x,y es guardado en direccion *caracter
void screen_get_sprite_char(int x,int y,z80_byte *caracter)
{
    z80_int *origen_rainbow;

        origen_rainbow=&rainbow_buffer[ y*get_total_ancho_rainbow()+x ];
        //origen_rainbow +=screen_total_borde_izquierdo*border_enabled.v;


        //construimos bytes de origen a comparar

        int bit,j;
        z80_byte acumulado;
        z80_int leido;
        for (j=0;j<8;j++) {

                acumulado=0;

                for (bit=0;bit<8;bit++) {
                        acumulado=acumulado*2;
                        leido=*origen_rainbow;
			//Si color 0, bit a 1. Sino, bit a 0
                        if ( leido==0 ) acumulado |=1;

                        origen_rainbow++;
                }

                *caracter=acumulado;
                caracter++;

                //siguiente byte origen
                origen_rainbow=origen_rainbow+get_total_ancho_rainbow()-8;


        }
}



void screen_reset_putpixel_maxmin_y(void)
{
	putpixel_max_y=-1;
	putpixel_min_y=99999;
}







void disable_timex_video(void)
{
	debug_printf (VERBOSE_DEBUG,"Disabling Timex Video");
	timex_video_emulation.v=0;
}

void enable_timex_video(void)
{
	//Si no esta activo y si maquina es spectrum
	if (timex_video_emulation.v==0) {
		debug_printf (VERBOSE_DEBUG,"Enabling Timex Video");

		//necesita real video
		enable_rainbow();

		timex_video_emulation.v=1;
	}
}


//Da un valor de color segun:
//Puntero a inicio de linea
//bits por pixel: 1,2,4,8
//Coordenada x en pixel
//Si modo cpc, los colores para modos de mas de 1 bpp salen diferente de lo habitual
z80_byte scr_get_colour_byte(z80_byte *inicio_linea,int bpp, int x, int si_cpc)
{
	int pixeles_por_byte;
	z80_byte mascara;
	//Para silenciar al compilador
	pixeles_por_byte=0;
	mascara=0;

	switch (bpp) {
		case 1:
			pixeles_por_byte=8;
			mascara=1;
		break;

		case 2:
			pixeles_por_byte=4;
			mascara=3;
		break;

		case 4:
			pixeles_por_byte=2;
			mascara=15;
		break;

		case 8:
			pixeles_por_byte=1;
			mascara=255;
		break;

		default:
			cpu_panic("Invalid value bpp on scr_get_colour_byte");
		break;
	}

	//Resto
	int resto=x%pixeles_por_byte;

	//Situarnos en el byte correspondiente
	x=x/pixeles_por_byte;


	//Obtener byte
	z80_byte valor_leido=*(inicio_linea+x);

	//Rotar dentro del byte tanto como sea necesario de resto
	//1bpp:  C0 C1 C2 C3 C4 C5 C6 C7
	//2bpp:  C0 C0 C1 C1 C2 C2 C3 C3
	//4bpp:  C0 C0 C0 C0 C1 C1 C1 C1
	//8bpp:  C0 C0 C0 C0 C0 C0 C0 C0

	//Para CPC
	if (si_cpc && bpp>1) {
		switch (bpp) {
			case 2:
				if (resto==0) valor_leido=(valor_leido&128)>>7 | ((valor_leido&8)>>2);
				if (resto==1) valor_leido=(valor_leido&64)>>6 | ((valor_leido&4)>>1);
				if (resto==2) valor_leido=(valor_leido&32)>>5 | ((valor_leido&2));
				if (resto==3) valor_leido=(valor_leido&16)>>4 | ((valor_leido&1)<<1   );
			break;

			case 4:
				if (resto==0) valor_leido=(valor_leido&128)>>7 | (valor_leido&8)>>2 | (valor_leido&32)>>3 | (valor_leido&2)<<2;
				if (resto==1) valor_leido=(valor_leido&64)>>6  | (valor_leido&4)>>1 | (valor_leido&16)>>2 | (valor_leido&1)<<3;
			break;

		}
	}

	else {

		for (;resto<pixeles_por_byte-1;resto++) {
			//printf ("resto: %d valor_leido: %d\n",resto,valor_leido);
			valor_leido = valor_leido >> bpp;
		}
	}

	//printf ("resultado: %d\n",valor_leido&mascara);
	//sleep(2);

	//return (valor_leido&1);
	return (valor_leido&mascara);

}


//Guardamos funcion de overlay y lo desactivamos, y finalizamos pantalla
void screen_end_pantalla_save_overlay(void (**previous_function)(void),int *menu_antes ) {
	*previous_function=menu_overlay_function;	
	*menu_antes=menu_overlay_activo;

	menu_overlay_activo=0;			
	scr_end_pantalla();
}

//Restauramos funcion de overlay y lo activamos
void screen_restart_pantalla_restore_overlay(void (*previous_function)(void),int menu_antes)
{
	menu_overlay_function=previous_function;
	menu_overlay_activo=menu_antes;


	//Si hay menu activo, reallocate layers, ya que probablemente habra cambiado el tamaño (activar border, footer, etc)
	if (menu_overlay_activo) {
		scr_init_layers_menu();
	}
}

void screen_set_window_zoom(int z)
{

	if (z>9 || z<1) {
		debug_printf (VERBOSE_ERR,"Invalid zoom value %d",z);
		return;
	}

	//printf ("funcion anterior: %p\n",menu_overlay_function);

	//Guardar funcion de texto overlay activo, para desactivarlo temporalmente. No queremos que se salte a realloc_layers simultaneamente,
	//mientras se hace putpixel desde otro sitio -> provocaria escribir pixel en layer que se esta reasignando
  	void (*previous_function)(void);
	int menu_antes;

	screen_end_pantalla_save_overlay(&previous_function,&menu_antes);

	//printf ("funcion leida: %p\n",previous_function);

	//printf ("despues end pantalla\n");

	zoom_x=zoom_y=z;
	modificado_border.v=1;

	screen_init_pantalla_and_others_and_realjoystick();
	set_putpixel_zoom();


	menu_init_footer();

	//printf ("despues init footer\n");

	screen_restart_pantalla_restore_overlay(previous_function,menu_antes);


	//menu_overlay_function=previous_function;
	//menu_overlay_activo=1;

	//printf ("---------final cambio zooom\n");
}






//En DESUSO:
//Retorna color RGB en formato 32 bits para un color rgb en formato 8 bit de RRRGGGBB. 
//Actualmente de momento solo usado en TBBLUE
//NO es mismo formato que tabla de ulaplus. Ulaplus tiene formato GGGRRRBB
/*
int get_rgb8_color (z80_byte color)
{
	//Minitablas de conversion de 3 bits a 8 bits
	z80_byte color_3_to_8[8]={
	0,36,73,109,146,182,219,255
	};

	int color32;
	z80_byte r,g,b;
	z80_byte r8,g8,b8;


		r=(color>>5)&7;
		g=(color>>2)&7;

		//componente b es un tanto esoterico
		//The missing lowest blue bit is set to OR of the other two blue bits (Bb becomes 000 for 00, and Bb1 for anything else)
		b=(color&3);
		b=(b<<1);
		if (b) b=b|1;

		//Pasamos cada componente de 3 bits a su correspondiente de 8 bits
		r8=color_3_to_8[r];
		g8=color_3_to_8[g];
		b8=color_3_to_8[b];

		color32=(r8<<16)|(g8<<8)|b8;
		return color32;


}
*/


//Retorna color RGB en formato 32 bits para un color rgb en formato 9 bit de RRRGGGBBB. 
//Actualmente de momento solo usado en TBBLUE

int get_rgb9_color (z80_int color)
{
	//Minitablas de conversion de 3 bits a 8 bits
	z80_byte color_3_to_8[8]={
	0,36,73,109,146,182,219,255
	};

	int color32;
	z80_byte r,g,b;
	z80_byte r8,g8,b8;
	//formato en color:
	//876543210
	//RRRGGGBBB

		r=(color>>6)&7;
		g=(color>>3)&7;
		b=color&7;

		//Pasamos cada componente de 3 bits a su correspondiente de 8 bits
		r8=color_3_to_8[r];
		g8=color_3_to_8[g];
		b8=color_3_to_8[b];

		color32=(r8<<16)|(g8<<8)|b8;
		return color32;


}


z80_bit zxuno_tbblue_disparada_raster={0};

z80_byte get_zxuno_tbblue_rasterctrl(void)
{
	return tbblue_registers[34];
}

void set_zxuno_tbblue_rasterctrl(z80_byte valor)
{
	tbblue_registers[34]=valor;
}

z80_byte get_zxuno_tbblue_rasterline(void)
{
	return tbblue_registers[35];
}

void zxuno_tbblue_handle_raster_interrupts()
{


/*
TBBUE y ZXUNO gestionan la interrupcion raster de la misma manera


ZXUNO

$0C RASTERLINE Lectura/Escritura	Almacena los 8 bits menos significativos de la línea de pantalla en la que se desea provocar
un disparo de una interrupción enmascarable. Un valor 0 para este registro (con LINE8 también igual a 0) establece que la
interrupción ráster se disparará, si está habilitada, justo al comenzar el borde derecho de la línea anterior a la primera
 línea de pantalla en la que comienza la zona de "paper". Dicho en otras palabras: el conteo de líneas de esta interrupción
  asume que una línea de pantalla se compone de: borde derecho + intervalo de blanking horizontal + borde izquierdo + zona
	 de paper. Si se asume de esta forma, el disparo de la interrupción se haría al comienzo de la línea seleccionada.
Un valor para RASTERLINE igual a 192 (con LINE8 igual a 0) dispara la interrupción ráster al comienzo del borde inferior.
 Los números de línea para el fin del borde inferior y comienzo del borde superior dependen de los timings empleados.
  El mayor valor posible en la práctiva para RASTERLINE corresponde a una interrupción ráster disparada en
	 la última línea del borde superior (ver RASTERCTRL)

$0D	RASTERCTRL	Lectura/Escritura	Registro de control y estado de la interrupción ráster. Se definen los siguientes bits.
INT	0	0	0	0	DISVINT	ENARINT	LINE8
INT: este bit sólo está disponible en lectura. Vale 1 durante 32 ciclos de reloj a partir del momento en que se dispara la interrupción ráster. Este bit está disponible aunque el procesador tenga las interrupciones deshabilitadas. No está disponible si el bit ENARINT vale 0.
DISVINT: a 1 para deshabilitar las interrupciones enmascarables por retrazo vertical (las originales de la ULA). Tras un reset, este bit vale 0.
ENARINT: a 1 para habilitar las interrupciones enmascarables por línea ráster. Tras un reset, este bit vale 0.
LINE8: guarda el bit 8 del valor de RASTERLINE, para poder definir cualquier valor entre 0 y 511, aunque en la práctica, el mayor valor está limitado por el número de líneas generadas por la ULA (311 en modo 48K, 310 en modo 128K, 319 en modo Pentagon). Si se establece un número de línea superior al límite, la interrupción ráster no se producirá.


TBBLUE:

	(R/W) 34 => Raster line interrupt control
	  bit 7 = (R) INT flag, 1=During INT (even if the processor has interrupt disabled)
	  bits 6-3 = Reserved, must be 0
	  bit 2 = If 1 disables original ULA interrupt (Reset to 0 after a reset)
	  bit 1 = If 1 enables Raster line interrupt (Reset to 0 after a reset)
	  bit 0 = MSB of Raster line interrupt value (Reset to 0 after a reset)

	(R/W) 35 => Raster line interrupt value LSB
	  bits 7-0 = Raster line value LSB (0-255)(Reset to 0 after a reset)

*/

					if (iff1.v==1 && (get_zxuno_tbblue_rasterctrl() & 2) ) {
						//interrupciones raster habilitadas
						//printf ("interrupciones raster habilitadas en %d\n",zxuno_ports[0x0c] + (256 * (zxuno_ports[0x0d]&1) ));


						//Ver si estamos entre estado 128 y 128+32
						int estados_en_linea=t_estados % screen_testados_linea;

						if (estados_en_linea>=128 && estados_en_linea<128+32) {
							//Si no se ha disparado la interrupcion
							if (zxuno_tbblue_disparada_raster.v==0) {
								//Comprobar la linea definida
								//El contador de lineas considera que la línea 0 es la primera línea de paper, la linea 192 por tanto es la primera línea de borde inferior.
								// El último valor del contador es 311 si estamos en un 48K, 310 si estamos en 128K, o 319 si estamos en Pentagon, y coincidiría con la última línea del borde superior.
								//se dispara justo al comenzar el borde derecho de la línea anterior a aquella que has seleccionado
								int linea_raster=get_zxuno_tbblue_rasterline() + (256 * (get_zxuno_tbblue_rasterctrl()&1) );

								int disparada_raster=0;


								//se dispara en linea antes... ?
								/*if (linea_raster>0) linea_raster--;
								else {
									linea_raster=screen_scanlines-1;
								}*/


								//es zona de vsync y borde superior
								//Aqui el contador raster tiene valor (192+56 en adelante)
								//contador de scanlines del core, entre 0 y screen_indice_inicio_pant ,
								if (t_scanline<screen_indice_inicio_pant) {
									if (t_scanline==linea_raster-192-screen_total_borde_inferior) disparada_raster=1;
								}

								//Esto es zona de paper o borde inferior
								//Aqui el contador raster tiene valor 0 .. <(192+56)
								//contador de scanlines del core, entre screen_indice_inicio_pant y screen_testados_total
								else {
									if (t_scanline-screen_indice_inicio_pant==linea_raster) disparada_raster=1;
								}

								if (disparada_raster) {
									//Disparar interrupcion
									zxuno_tbblue_disparada_raster.v=1;
									interrupcion_maskable_generada.v=1;

									//printf ("Generando interrupcion raster en scanline %d, raster: %d , estados en linea: %d, t_estados %d\n",
									//	t_scanline,linea_raster+1,estados_en_linea,t_estados);

									//Activar bit INT
									z80_byte valor=get_zxuno_tbblue_rasterctrl();
									valor |=128;
									set_zxuno_tbblue_rasterctrl(valor);
								}

								else {
									//Resetear bit INT
									//zxuno_ports[0x0d] &=(255-128);
								}
							}
						}

						//Cualquier otra zona de t_estados, meter a 0
						else {
							zxuno_tbblue_disparada_raster.v=0;
							//Resetear bit INT
							z80_byte valor=get_zxuno_tbblue_rasterctrl();
							valor &=(255-128);
							set_zxuno_tbblue_rasterctrl(valor);
						}

					}



}



int generic_footertext_operating_counter=0;

void generic_footertext_print_operating_aux(char *s)
{

        if (generic_footertext_operating_counter) {
        			
								menu_footer_activity(s);
        }
}

void old_delete_generic_footertext_print_operating_aux(char *s)
{

        if (generic_footertext_operating_counter) {
        			//01234567
        	char string_aux[]="        "; //2 espacios, 6 caracteres y 0 final
        	int longitud=strlen(s);
        	if (longitud>6) longitud=6;

        	int indice_string=0;

        	string_aux[indice_string++]=' ';

        	//printf ("texto: %s\n",s);
        	for (;longitud;indice_string++,longitud--,s++) {
        		//printf ("[%d] [%d] [%c] [%c]\n",indice_string,longitud,string_aux[indice_string],*s);
        		string_aux[indice_string]=*s;
        	}

        	string_aux[indice_string++]=' ';
        	string_aux[indice_string]=0;

                //		      					       01234567
                //menu_putstring_footer(WINDOW_FOOTER_ELEMENT_X_GENERICTEXT,1,string_aux,WINDOW_FOOTER_PAPER,WINDOW_FOOTER_INK);
								
								menu_footer_activity(string_aux);
        }
}

void generic_footertext_print_operating(char *s)
{
	//printf ("footer %s\n",s);

        //Si ya esta activo, no volver a escribirlo. Porque ademas el menu_putstring_footer consumiria mucha cpu
        if (!generic_footertext_operating_counter) {  
        	//Borrar si habia alguno otro diferente
					//printf ("delete footer\n");
        	delete_generic_footertext();
        	  
		generic_footertext_operating_counter=2;
                generic_footertext_print_operating_aux(s);

        }

	generic_footertext_operating_counter=2;
}


void delete_generic_footertext(void)
{
	menu_delete_footer_activity();
}


int rgb_to_grey(int r,int g,int b)
{
/* luminosity method
The luminosity method is a more sophisticated version of the average method. It also averages the values, but it forms a weighted average to account for human perception. We’re more sensitive to green than other colors, so green is weighted most heavily. The formula for luminosity is 0.21 R + 0.72 G + 0.07 B.

https://www.johndcook.com/blog/2009/08/24/algorithms-convert-color-grayscale/
*/

	r=(r*21)/100;
	g=(g*72)/100;
	b=(b*7)/100;

	return r+g+b;

}


//Retorna Y posicion de pixeles para un scanline determinado
//Dice si salta linea siguiente
//Si y es negativo quiere decir que no esta visible (en zona de vsync por ejemplo)
int screen_get_y_coordinate_tstates(void)
{
	int y;
	
	y=t_scanline_draw-screen_invisible_borde_superior;


	return y;

}
//Retorna X posicion de pixeles para un scanline determinado
//Dice si salta linea siguiente
//Si x es negativo quiere decir que no esta visible (en zona de hsync por ejemplo)
int screen_get_x_coordinate_tstates(int *si_salta_linea)
{
        int estados_en_linea=t_estados % screen_testados_linea;

/*


Estos en pixeles
//normalmente a 48
int screen_total_borde_izquierdo;

//normalmente a 48
int screen_total_borde_derecho;

//normalmente a 96
int screen_invisible_borde_derecho;


*/

//printf ("screen_total_borde_derecho: %d\n",screen_total_borde_derecho);
//printf ("screen_total_borde_izquierdo: %d\n",screen_total_borde_izquierdo);
//printf ("screen_invisible_borde_derecho: %d\n",screen_invisible_borde_derecho);
//printf ("\n");

	*si_salta_linea=0; //por defecto
	int x;


	//Por tanto pasamos los t-estados actuales a turbo x1
	estados_en_linea /=cpu_turbo_speed;

	//Lo siguiente en t-estados
	int inicio_borde_derecho;

	//inicio_borde_derecho=128;
	inicio_borde_derecho=screen_testados_linea/cpu_turbo_speed-screen_total_borde_izquierdo/2-screen_total_borde_derecho/2-screen_invisible_borde_derecho/2;

	//printf ("%d\n",inicio_borde_derecho);
	int inicio_borde_derecho_invisible=inicio_borde_derecho+screen_total_borde_derecho/2;
	int inicio_borde_izquierdo=inicio_borde_derecho_invisible+screen_invisible_borde_derecho/2;

	//Si estoy mas alla del border izquierdo, avisar de salto de coordenada
	if (estados_en_linea>=inicio_borde_izquierdo) {
		*si_salta_linea=1;
		//Ajustar a la izquierda
		estados_en_linea -=inicio_borde_izquierdo;

		//coordenada final
		int x=estados_en_linea*2;
		return x;
	}

	//Si estoy en la zona de parte derecha invisible
	if (estados_en_linea>=inicio_borde_derecho_invisible && estados_en_linea<inicio_borde_izquierdo) {
		//Zona invisible
		return -1;
	}

	//Zona display o de border derecho
	x=estados_en_linea*2;
	x +=screen_total_borde_izquierdo; //Sumarle el ancho de pixeles de borde izquierdo
	return x;
}


//Cambia la paleta de color a Modo blanco y negro cuando se abre menu y multitarea esta a off
/*
void screen_change_bw_menu_multitask(void)
{

	if (menu_multitarea==0 && menu_abierto) {
		if (screen_bw_no_multitask_menu.v) spectrum_colortable=spectrum_colortable_new_blanco_y_negro;
	}

}
*/

/*
Nadie deberia llamar a scr_init_pantalla() directamente. Hay que usar esta funcion, por la razon de que:
Al cambiar por ejemplo footer, se cierra y se abre driver de video. Si no hay resize/removimiento de ventana , lo que sucede es que
al abrir la ventana se genera una ventana con fondo negro. Dado que la putpixel cache no habria cambiado, no se refresca,
y por tanto se queda en negro. Esto pasa al cambiar otros parametros tambien de la ventana, como al cambiar zoom por ejemplo
*/

/*
Nota: antes de usar esta funcion, sucedia por ejemplo que, al desactivar/activar footer, se hacia scr_end_pantalla y scr_init_pantalla,
generalmente el driver xwindows, genera un evento de scrxwindows_resize, que a su vez, generaba un clear_putpixel_cache, por lo que todo iba bien
Pero a veces, en concreto en mi pc (quiza dependa de la version de Xorg) no genera dicho evento, con lo que no se hacia clear_putpixel_cache,
dejando la ventana en negro como se comenta antes
*/

int screen_init_pantalla_and_others(void)
{
	int retorno=scr_init_pantalla();
  
	//Siempre que se redimensiona tamanyo ventana (sin contar zoom) o se reinicia driver video hay que reiniciar cache putpixel
  init_cache_putpixel();

	//printf ("screen_init_pantalla_and_others\n");
	//menu_init_footer();

	return retorno;
}


void screen_init_pantalla_and_others_and_realjoystick(void)
{



	screen_init_pantalla_and_others();

	/*
	Al iniciar driver video, en el caso de SDL por ejemplo, apunta a las funciones de realjoystick sdl. Si no inicializamos dicho joystick,
	sucedera que al hacer el poll de joystick, usara un joystick no inicializado y petara 

	TODO: hacer que el init de sdl de video, tambien inicialice el joystick (en el caso que no usemos driver linux nativo)
	*/

	//Ya no hace falta

    //realjoystick_reopen_driver();
}


const char *s_spectrum_video_mode_standard="256x192";
const char *s_spectrum_video_mode_timex_standard_one="256x192 Timex Screen 1";
const char *s_spectrum_video_mode_timex_hicol="256x192 Timex 8x1 Color";
const char *s_spectrum_video_mode_timex_hires="512x192 Timex monochrome";

const char *s_spectrum_video_mode_tbblue_lores="128x96 256 colours";

char *get_spectrum_ula_string_video_mode(void)
{

	//Por defecto
	const char *string_mode=s_spectrum_video_mode_standard;

	if (timex_video_emulation.v) {

			if ((timex_port_ff&7)==1) string_mode=s_spectrum_video_mode_timex_standard_one;
			else if ((timex_port_ff&7)==2) string_mode=s_spectrum_video_mode_timex_hicol;
			else if ((timex_port_ff&7)==6) string_mode=s_spectrum_video_mode_timex_hires;

	}

		if (tbblue_registers[21]&128) string_mode=s_spectrum_video_mode_tbblue_lores;

	return (char *)string_mode;

}


//Convertir paleta EGA de 16 colores a Spectrum
int screen_ega_to_spectrum_colour(int ega_col)
{
//https://en.wikipedia.org/wiki/Enhanced_Graphics_Adapter
	//Ega:      0 black, 1 blue, 2 green, 3 cyan,    4 red,   5 magenta, 6 brown,  7 white, bright black, bright blue, bright green, bright cyan, bright red, bright magenta, bright yellow, white, 
	//Spectrum: 0 black, 1 blue, 2 red,   3 magenta, 4 green, 5 cyan,    6 yellow, 7 white, + brillos

	int lookup_table[]={0,1,4,5,2,3,6,7};

	int brillo=0;

	if (ega_col>7) {
		brillo=1;
		ega_col -=8;
	}
	//Por si acaso

	ega_col &=7;

	int color_final=lookup_table[ega_col]+8*brillo;
	return color_final;
}




void screen_render_bmpfile(z80_byte *mem,int indice_paleta_color,zxvision_window *ventana)
{

						//putpixel del archivo bmp
			

/*
Name	Size	Offset	Description
Header	
 	Signature	2 bytes	0000h	'BM'
FileSize	4 bytes	0002h	File size in bytes
reserved	4 bytes	0006h	unused (=0)
DataOffset	4 bytes	000Ah	Offset from beginning of file to the beginning of the bitmap data

 	Size	4 bytes	000Eh	Size of InfoHeader =40 
Width	4 bytes	0012h	Horizontal width of bitmap in pixels
Height	4 bytes	0016h	Vertical height of bitmap in pixels
*/		


	//ancho y alto de la cabecera. maximo 16 bit
						int ancho=mem[18] + 256 * mem[19];
						int alto=mem[22] + 256 * mem[23];

						//printf ("ancho: %d alto: %d\n",ancho,alto);


						//118 bytes de cabecera ignorar
						//Cuantos bytes de cabecera ignorar?

/*
						Name	Size	Offset	Description
Header	
 	Signature	2 bytes	0000h	'BM'
FileSize	4 bytes	0002h	File size in bytes
reserved	4 bytes	0006h	unused (=0)
DataOffset	4 bytes	000Ah	Offset from beginning of file to the beginning of the bitmap data
*/
						//Pillamos el offset como valor de 16 bits para simplificar

						int offset_bmp=mem[10] + 256 * mem[11];
						//printf ("offset pixeles: %d\n",offset_bmp);

								int ancho_calculo=ancho;
								//Nota: parece que el ancho tiene que ser par, para poder calcular el offset
								//if ( (ancho_calculo % 2 ) !=0) ancho_calculo++;						

						int x,y;
						for (y=0;y<alto;y++) {
							for (x=0;x<ancho;x++) {
								//lineas empiezan por la del final en un bmp
								//1 byte por pixel, color indexado
			


								int offset_final=(alto-1-y)*ancho_calculo + x + offset_bmp;

								


								//printf ("offset_final_ %d\n",offset_final);
								z80_byte byte_leido=mem[offset_final];
								z80_int color_final=indice_paleta_color+byte_leido;
								//zxvision_putpixel(ventana,x,y,color_final);
								zxvision_putpixel_no_zoom(ventana,x,y,color_final);
							}
						}
						

}

