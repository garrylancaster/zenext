/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "cpu.h"
#include "operaciones.h"
#include "debug.h"
#include "audio.h"
#include "ay38912.h"
#include "mem128.h"
#include "menu.h"
#include "screen.h"
#include "compileoptions.h"
#include "contend.h"
#include "joystick.h"
#include "ula.h"
#include "utils.h"
#include "printers.h"
#include "disassemble.h"
#include "ulaplus.h"
#include "mmc.h"
#include "divmmc.h"
#include "diviface.h"
#include "timex.h"
#include "timer.h"
#include "tbblue.h"
#include "multiface.h"
#include "ds1307.h"
#include "settings.h"
#include "datagear.h"


void (*poke_byte)(z80_int dir,z80_byte valor);
void (*poke_byte_no_time)(z80_int dir,z80_byte valor);
z80_byte (*peek_byte)(z80_int dir);
z80_byte (*peek_byte_no_time)(z80_int dir);
z80_byte (*lee_puerto)(z80_byte puerto_h,z80_byte puerto_l);
void (*out_port)(z80_int puerto,z80_byte value);

void (*push_valor)(z80_int valor,z80_byte tipo); 

z80_byte lee_puerto_teclado(z80_byte puerto_h);
z80_byte lee_puerto_spectrum_no_time(z80_byte puerto_h,z80_byte puerto_l);

void set_value_beeper (int v);


//Tablas para instrucciones adc, sbc y otras
z80_byte overflow_add_table[] = { 0, 0, 0, FLAG_PV, FLAG_PV, 0, 0, 0 };

z80_byte halfcarry_add_table[] ={ 0, FLAG_H, FLAG_H, FLAG_H, 0, 0, 0, FLAG_H };
z80_byte halfcarry_sub_table[] = { 0, 0, FLAG_H, 0, FLAG_H, 0, FLAG_H, FLAG_H };

z80_byte overflow_sub_table[] = { 0, FLAG_PV, 0, 0, 0, 0, FLAG_PV, 0 };

z80_byte parity_table[256];
z80_byte sz53_table[256];
z80_byte sz53p_table[256];



//La mayoria de veces se llama aqui desde set_flags_carry_, dado que casi todas las operaciones que tocan el carry tocan el halfcarry
//hay algunas operaciones, como inc8, que tocan el halfcarry pero no el carry
void set_flags_halfcarry_suma(z80_byte antes,z80_byte result)
{
        antes=antes & 0xF;
        result=result & 0xF;

        if (result<antes) Z80_FLAGS |=FLAG_H;
        else Z80_FLAGS &=(255-FLAG_H);
}

//La mayoria de veces se llama aqui desde set_flags_carry_, dado que casi todas las operaciones que tocan el carry tocan el halfcarry
//hay algunas operaciones, como inc8, que tocan el halfcarry pero no el carry
void set_flags_halfcarry_resta(z80_byte antes,z80_byte result)
{
        antes=antes & 0xF;
        result=result & 0xF;

        if (result>antes) Z80_FLAGS |=FLAG_H;
        else Z80_FLAGS &=(255-FLAG_H);
}


//Well as stated in chapter 3 if the result of an operation in two's complement produces a result that's signed incorrectly then there's an overflow
//o So overflow flag = carry-out flag XOR carry from bit 6 into bit 7.
/*
void set_flags_overflow_inc(z80_byte antes,z80_byte result)
{

	if (result==128) Z80_FLAGS |=FLAG_PV;
	else Z80_FLAGS &=(255-FLAG_PV);

}
*/

/*
void set_flags_overflow_dec(z80_byte antes,z80_byte result)
//Well as stated in chapter 3 if the result of an operation in two's complement produces a result that's signed incorrectly then there's an overflow
//o So overflow flag = carry-out flag XOR carry from bit 6 into bit 7.
{
	if (result==127) Z80_FLAGS |=FLAG_PV;
	else Z80_FLAGS &=(255-FLAG_PV);
}
*/

void set_flags_overflow_suma(z80_byte antes,z80_byte result)

//Siempre llamar a esta funcion despues de haber actualizado el Flag de Carry, pues lo utiliza

//Well as stated in chapter 3 if the result of an operation in two's complement produces a result that's signed incorrectly then there's an overflow
//o So overflow flag = carry-out flag XOR carry from bit 6 into bit 7.
{
	//127+127=254 ->overflow    01111111 01111111 = 11111110    NC   67=y    xor=1
	//-100-100=-200 -> overflow 10011100 10011100 = 00111000     C    67=n   xor=1

	//-2+127=125 -> no overflow 11111110 01111111 = 11111101     C    67=y   xor=0
	//127-2=125 -> no overlow   01111111 11111110 = 11111101     C    67=y   xor=0

	//10-100=-90 -> no overflow 00001010 10011100 = 10100110    NC    67=n   xor=0

	z80_byte overflow67;

        if ( (result & 127) < (antes & 127) ) overflow67=FLAG_C;
        else overflow67=0;

	if ( (Z80_FLAGS & FLAG_C ) ^ overflow67) Z80_FLAGS |=FLAG_PV;
	else Z80_FLAGS &=(255-FLAG_PV);

}

void set_flags_overflow_resta(z80_byte antes,z80_byte result)

//Siempre llamar a esta funcion despues de haber actualizado el Flag de Carry, pues lo utiliza

//Well as stated in chapter 3 if the result of an operation in two's complement produces a result that's signed incorrectly then there's an overflow
//o So overflow flag = carry-out flag XOR carry from bit 6 into bit 7.
{
        //127+127=254 ->overflow    01111111 01111111 = 11111110    NC   67=y    xor=1
        //-100-100=-200 -> overflow 10011100 10011100 = 100111000    C    67=n   xor=1

        //-2+127=125 -> no overflow 11111110 01111111 = 11111101     C    67=y   xor=0
        //127-2=125 -> no overlow   01111111 11111110 = 11111101     C    67=y   xor=0

        //10-100=-90 -> no overflow 00001010 10011100 = 10100110    NC    67=n   xor=0

        z80_byte overflow67;


        if ( (result & 127) > (antes & 127) ) overflow67=FLAG_C;
        else overflow67=0;

	if ( (Z80_FLAGS & FLAG_C ) ^ overflow67) Z80_FLAGS |=FLAG_PV;
	else Z80_FLAGS &=(255-FLAG_PV);

}


void set_flags_overflow_suma_16(z80_int antes,z80_int result)

//Siempre llamar a esta funcion despues de haber actualizado el Flag de Carry, pues lo utiliza

//Well as stated in chapter 3 if the result of an operation in two's complement produces a result that's signed incorrectly then there's an overflow
//o So overflow flag = carry-out flag XOR carry from bit 6 into bit 7.
{
        //127+127=254 ->overflow    01111111 01111111 = 11111110    NC   67=y    xor=1
        //-100-100=-200 -> overflow 10011100 10011100 = 00111000    C    67=n   xor=1

        //-2+127=125 -> no overflow 11111110 01111111 = 11111101     C    67=y   xor=0
        //127-2=125 -> no overlow   01111111 11111110 = 11111101     C    67=y   xor=0

        //10-100=-90 -> no overflow 00001010 10011100 = 10100110    NC    67=n   xor=0

        z80_byte overflow67;


        if ( (result & 32767) < (antes & 32767) ) overflow67=FLAG_C;
        else overflow67=0;

	if ( (Z80_FLAGS & FLAG_C ) ^ overflow67) Z80_FLAGS |=FLAG_PV;
	else Z80_FLAGS &=(255-FLAG_PV);


}

void set_flags_overflow_resta_16(z80_int antes,z80_int result)

//Siempre llamar a esta funcion despues de haber actualizado el Flag de Carry, pues lo utiliza

//Well as stated in chapter 3 if the result of an operation in two's complement produces a result that's signed incorrectly then there's an overflow
//o So overflow flag = carry-out flag XOR carry from bit 6 into bit 7.
{
        //127+127=254 ->overflow    01111111 01111111 = 11111110    NC   67=y    xor=1
        //-100-100=-200 -> overflow 10011100 10011100 = 00111000     C    67=n   xor=1

        //-2+127=125 -> no overflow 11111110 01111111 = 11111101     C    67=y   xor=0
        //127-2=125 -> no overlow   01111111 11111110 = 11111101     C    67=y   xor=0

        //10-100=-90 -> no overflow 00001010 10011100 = 10100110    NC    67=n   xor=0

        z80_byte overflow67;


        if ( (result & 32767) > (antes & 32767) ) overflow67=FLAG_C;
        else overflow67=0;

	if ( (Z80_FLAGS & FLAG_C ) ^ overflow67) Z80_FLAGS |=FLAG_PV;
	else Z80_FLAGS &=(255-FLAG_PV);

}



//activa flags segun instruccion in r,(c)
void set_flags_in_reg(z80_byte value)
{
	Z80_FLAGS=( Z80_FLAGS & FLAG_C) | sz53p_table[value];
}

void set_flags_parity(z80_byte value)
{
	Z80_FLAGS=(Z80_FLAGS & (255-FLAG_PV) ) | parity_table[value];
}



//Parity set if even number of bits set
//Paridad si numero par de bits
z80_byte get_flags_parity(z80_byte value)
{
	z80_byte result;

        result=FLAG_PV;
        z80_byte mascara=1;
        z80_byte bit;

        for (bit=0;bit<8;bit++) {
                if ( (value) & mascara) result = result ^ FLAG_PV;
                mascara = mascara << 1;
        }

	return result;
}



//Inicializar algunas tablas para acelerar cpu
void init_cpu_tables(void)
{
	z80_byte value=0;

	int contador=0;

	debug_printf (VERBOSE_INFO,"Initializing cpu flags tables");

#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x80 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x01 ? 1 : 0)


	//Tabla paridad, sz53
	for (contador=0;contador<256;contador++,value++) {
		parity_table[value]=get_flags_parity(value);
		debug_printf (VERBOSE_PARANOID,"Parity table: value: %3d (" BYTETOBINARYPATTERN ") parity: %d",value,BYTETOBINARY(value),parity_table[value]);


		sz53_table[value]=value & ( FLAG_3|FLAG_5|FLAG_S );

		if (value==0) sz53_table[value] |=FLAG_Z;

		debug_printf (VERBOSE_PARANOID,"SZ53 table: value: %3d (" BYTETOBINARYPATTERN ") flags: (" BYTETOBINARYPATTERN ") ",value,BYTETOBINARY(value),BYTETOBINARY(sz53_table[value])) ;


		sz53p_table[value]=sz53_table[value] | parity_table[value];
		debug_printf (VERBOSE_PARANOID,"SZ53P table: value: %3d (" BYTETOBINARYPATTERN ") flags: (" BYTETOBINARYPATTERN ") ",value,BYTETOBINARY(value),BYTETOBINARY(sz53p_table[value])) ;

	}
}

void neg(void)
{
/*
        z80_byte result,antes;

        antes=0;

        result=antes-reg_a;
        reg_a=result;

        set_flags_carry_resta(antes,result);
        set_flags_overflow_resta(antes,result);
        Z80_FLAGS=(Z80_FLAGS & (255-FLAG_3-FLAG_5-FLAG_Z-FLAG_S)) | FLAG_N | sz53_table[result];

*/

        z80_byte tempneg=reg_a;
        reg_a=0;
	sub_a_reg(tempneg);
}



//Comun al generar interrupcion en im0/1
void cpu_common_jump_im01(void)
{
	//if (im_mode==0 || im_mode==1) {
	reg_pc=56;
	t_estados += 7;


	//Im modo 0 la interrupción es un t-estado más rápido que con Im modo 1, en cpu mostek

	if (im_mode==0 && z80_cpu_current_type==Z80_TYPE_MOSTEK) t_estados--;
}


//Rutinas de cpu core vacias para que, al parsear breakpoints del config file, donde aun no hay inicializada maquina,
//funciones como opcode1=XX , peek(x), etc no peten porque utilizan funciones peek. Inicializar también las de puerto por si acaso

z80_byte peek_byte_vacio(z80_int dir GCC_UNUSED)
{
	return 0;
}

void poke_byte_vacio(z80_int dir GCC_UNUSED,z80_byte valor GCC_UNUSED)
{

}


z80_byte lee_puerto_vacio(z80_byte puerto_h GCC_UNUSED,z80_byte puerto_l GCC_UNUSED)
{
	return 0;
}


void out_port_vacio(z80_int puerto GCC_UNUSED,z80_byte value GCC_UNUSED)
{

}

z80_int peek_word(z80_int dir)
{
        z80_byte h,l;

        l=peek_byte(dir);
        h=peek_byte(dir+1);

        return (h<<8) | l;
}

z80_int peek_word_no_time(z80_int dir)
{
        z80_byte h,l;

        l=peek_byte_no_time(dir);
        h=peek_byte_no_time(dir+1);

        return (h<<8) | l;
}

z80_int lee_word_pc(void)
{
        z80_int valor;

        valor=peek_word(reg_pc);
        reg_pc +=2;
        return valor;

	//este reg_pc+=2 es incorrecto, no funciona
	//return peek_word(reg_pc+=2);
}

void poke_word(z80_int dir,z80_int valor)
{
        poke_byte(dir,valor & 0xFF);
        poke_byte(dir+1, (valor >> 8) & 0xFF );
}



z80_byte peek_byte_desp(z80_int dir,z80_byte desp)
{


//RETURN (dir+desp)

        z80_int desp16,puntero;

        desp16=desp8_to_16(desp);

        puntero=dir + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif

        return peek_byte(puntero);

}

void poke_byte_desp(z80_int dir,z80_byte desp,z80_byte valor)
{
        z80_int desp16,puntero;

        desp16=desp8_to_16(desp);

        puntero=dir + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif

	poke_byte(puntero,valor);
}




z80_int sbc_16bit(z80_int reg, z80_int value)
{

	z80_int result;
        z80_byte h;
        int result_32bit;

        set_memptr(reg+1);



        result_32bit=reg-value-( Z80_FLAGS & FLAG_C );

        z80_int lookup =      ( (  (reg) & 0x8800 ) >> 11 ) | \
                            ( (  (value) & 0x8800 ) >> 10 ) | \
                            ( ( result_32bit & 0x8800 ) >>  9 );  \


        result=result_32bit & 65535;
        h=value_16_to_8h(result);
        set_undocumented_flags_bits(h);


        if (result_32bit & 0x10000) Z80_FLAGS |=FLAG_C;
        else Z80_FLAGS &=(255-FLAG_C);

        set_flags_zero_sign_16(result);

	Z80_FLAGS=(Z80_FLAGS & (255-FLAG_H-FLAG_PV)) | halfcarry_sub_table[lookup&0x07] | overflow_sub_table[lookup >> 4] | FLAG_N;

	return result;


}





z80_int add_16bit(z80_int reg, z80_int value)
{
        z80_int result,antes;
        z80_byte h;

	set_memptr(reg+1);


        antes=reg;
        result=reg;

        result +=value;

	z80_int lookup = ( (       (reg) & 0x0800 ) >> 11 ) | \
                            ( (  (value) & 0x0800 ) >> 10 ) | \
                            ( (   result & 0x0800 ) >>  9 );  \


        h=value_16_to_8h(result);
        set_undocumented_flags_bits(h);

        set_flags_carry_16_suma(antes,result);


	Z80_FLAGS=(Z80_FLAGS & (255-FLAG_H-FLAG_N)) | halfcarry_add_table[lookup];


        return result;
}

z80_int adc_16bit(z80_int reg, z80_int value)
{

        z80_int result;
        z80_byte h;
	int result_32bit;

	set_memptr(reg+1);


        result_32bit=reg+value+( Z80_FLAGS & FLAG_C );

	z80_int lookup =      ( (  (reg) & 0x8800 ) >> 11 ) | \
                            ( (  (value) & 0x8800 ) >> 10 ) | \
                            ( ( result_32bit & 0x8800 ) >>  9 );  \


	result=result_32bit & 65535;
        h=value_16_to_8h(result);
        set_undocumented_flags_bits(h);


        if (result_32bit & 0x10000) Z80_FLAGS |=FLAG_C;
        else Z80_FLAGS &=(255-FLAG_C);

        set_flags_zero_sign_16(result);

        Z80_FLAGS=(Z80_FLAGS & (255-FLAG_H-FLAG_PV-FLAG_N)) | halfcarry_add_table[lookup&0x07] | overflow_add_table[lookup >> 4];


        return result;


}

z80_int desp8_to_16(z80_byte desp)
{

	z80_int desp16;

        if (desp>127) {
		desp=256-desp;
                desp16=-desp;
	}
        else
                desp16=desp;

	return desp16;

}



z80_int pop_valor()
{
	z80_int valor;

        valor=peek_word(reg_sp);
        reg_sp +=2;
        return valor;

}

//Tener en cuenta los valores de tipo para los strings:
/*enum push_value_type {
	PUSH_VALUE_TYPE_DEFAULT=0,
	PUSH_VALUE_TYPE_CALL,
	PUSH_VALUE_TYPE_RST,
	PUSH_VALUE_TYPE_PUSH,
	PUSH_VALUE_TYPE_MASKABLE_INTERRUPT,
        PUSH_VALUE_TYPE_NON_MASKABLE_INTERRUPT
};
*/

char *push_value_types_strings[TOTAL_PUSH_VALUE_TYPES]={
	"default",
	"call",
	"rst",
	"push",
	"maskable_interrupt",
	"non_maskable_interrupt"
};



//En la funcion por defecto no usamos el tipo
//El tipo realmente sera un valor en el rango del enum de push_value_type
//lo pongo como z80_byte y no como enum porque luego al activar extended_stack, las funciones de nested
//requieren que ese parametro sea z80_byte
void push_valor_default(z80_int valor,z80_byte tipo GCC_UNUSED) 
{ 
        reg_sp -=2; 
        poke_word(reg_sp,valor); 
} 



z80_byte rlc_valor_comun(z80_byte value)
{
        if (value & 128) Z80_FLAGS |=FLAG_C;
        else Z80_FLAGS &=(255-FLAG_C);

        value=value << 1;
        value |= ( Z80_FLAGS & FLAG_C );

        set_undocumented_flags_bits(value);
	Z80_FLAGS &=(255-FLAG_N-FLAG_H);

        return value;

}

z80_byte rlc_valor(z80_byte value)
{
	value=rlc_valor_comun(value);
	set_flags_zero_sign(value);
	set_flags_parity(value);
	return value;
}


void rlc_reg(z80_byte *reg)
{
        z80_byte value=*reg;

        *reg=rlc_valor(value);

}

void rlca(void)
{
        //RLCA no afecta a S, Z ni P
        reg_a=rlc_valor_comun(reg_a);
}



//rutina comun a rl y rla
z80_byte rl_valor_comun(z80_byte value)
{
        z80_bit flag_C_antes;
        flag_C_antes.v=( Z80_FLAGS & FLAG_C );

        if (value & 128) Z80_FLAGS |=FLAG_C;
        else Z80_FLAGS &=(255-FLAG_C);

        value=value << 1;
        value |= flag_C_antes.v;

        set_undocumented_flags_bits(value);

        Z80_FLAGS &=(255-FLAG_N-FLAG_H);

        return value;

}

z80_byte rl_valor(z80_byte value)
{
	value=rl_valor_comun(value);
	set_flags_zero_sign(value);
	set_flags_parity(value);
        return value;

}

void rl_reg(z80_byte *reg)
{
        z80_byte value=*reg;

        *reg=rl_valor(value);

}


void rla(void)
{
	//RLA no afecta a S, Z ni P
        reg_a=rl_valor_comun(reg_a);
}



z80_byte rr_valor_comun(z80_byte value)
{
        z80_bit flag_C_antes;
        flag_C_antes.v=( Z80_FLAGS & FLAG_C );

        if (value & 1) Z80_FLAGS |=FLAG_C;
        else Z80_FLAGS &=(255-FLAG_C);

        value=value >> 1;
        value |= (flag_C_antes.v*128);

        set_undocumented_flags_bits(value);
	Z80_FLAGS &=(255-FLAG_N-FLAG_H);

        return value;

}

z80_byte rr_valor(z80_byte value)
{
	value=rr_valor_comun(value);
        set_flags_zero_sign(value);
	set_flags_parity(value);
	return value;
}


void rr_reg(z80_byte *reg)
{
        z80_byte value=*reg;

        *reg=rr_valor(value);

}


void rra(void)
{
        //RRA no afecta a S, Z ni P
        reg_a=rr_valor_comun(reg_a);

}

z80_byte rrc_valor_comun(z80_byte value)
{
        if (value & 1) Z80_FLAGS |=FLAG_C;
        else Z80_FLAGS &=(255-FLAG_C);

        value=value >> 1;
        value |= (( Z80_FLAGS & FLAG_C )*128);

        set_undocumented_flags_bits(value);
	Z80_FLAGS &=(255-FLAG_N-FLAG_H);

        return value;

}

z80_byte rrc_valor(z80_byte value)
{
	value=rrc_valor_comun(value);
	set_flags_zero_sign(value);
	set_flags_parity(value);
	return value;
}

void rrc_reg(z80_byte *reg)
{
        z80_byte value=*reg;

        *reg=rrc_valor(value);

}



void rrca(void)
{
        //RRCA no afecta a S, Z ni P
        reg_a=rrc_valor_comun(reg_a);
}





z80_byte sla_valor(z80_byte value)
{

	Z80_FLAGS=0;

        if (value & 128) Z80_FLAGS |=FLAG_C;

        value=value << 1;

	Z80_FLAGS |=sz53p_table[value];

        return value;

}

void sla_reg(z80_byte *reg)
{

        z80_byte value=*reg;

        *reg=sla_valor(value);

}

z80_byte sra_valor(z80_byte value)
{
        z80_byte value7=value & 128;

	Z80_FLAGS=0;

        if (value & 1) Z80_FLAGS |=FLAG_C;

        value=value >> 1;
        value |= value7;

	Z80_FLAGS |=sz53p_table[value];

        return value;

}

void sra_reg(z80_byte *reg)
{
        z80_byte value=*reg;

        *reg=sra_valor(value);

}

z80_byte srl_valor(z80_byte value)
{

	Z80_FLAGS=0;

        if (value & 1) Z80_FLAGS |=FLAG_C;

        value=value >> 1;

	Z80_FLAGS |=sz53p_table[value];

        return value;

}

void srl_reg(z80_byte *reg)
{
        z80_byte value=*reg;

        *reg=srl_valor(value);

}

z80_byte sls_valor(z80_byte value)
{

	Z80_FLAGS=0;

        if (value & 128) Z80_FLAGS |=FLAG_C;

        value=(value << 1) | 1;

	Z80_FLAGS |=sz53p_table[value];

        return value;

}

void sls_reg(z80_byte *reg)
{

        z80_byte value=*reg;

        *reg=sls_valor(value);

}


void add_a_reg(z80_byte value)
{

        z80_byte result,antes;

        result=reg_a;
        antes=reg_a;

        result +=value;
        reg_a=result;


        set_flags_carry_suma(antes,result);
        set_flags_overflow_suma(antes,result);
        Z80_FLAGS=(Z80_FLAGS & (255-FLAG_N-FLAG_Z-FLAG_S-FLAG_5-FLAG_3)) | sz53_table[result];


}


void adc_a_reg(z80_byte value)
{

	//printf ("flag_C antes: %d ",( Z80_FLAGS & FLAG_C ));

        z80_byte result,lookup;
	z80_int result_16bit;


        result_16bit=reg_a;
        result_16bit=result_16bit+value+( Z80_FLAGS & FLAG_C );

	lookup = ( (       reg_a & 0x88 ) >> 3 ) |
                     ( ( (value) & 0x88 ) >> 2 ) |
                ( ( result_16bit & 0x88 ) >> 1 );


	result=value_16_to_8l(result_16bit);
        reg_a=result;


	Z80_FLAGS = ( result_16bit & 0x100 ? FLAG_C : 0 );

	Z80_FLAGS=Z80_FLAGS | halfcarry_add_table[lookup & 0x07] | overflow_add_table[lookup >> 4] | sz53_table[result];

	//printf ("antes: %d value: %d result: %d flag_c: %d flag_pv: %d\n", antes, value, result, ( Z80_FLAGS & FLAG_C ),flag_PV.v);

}


//Devuelve el resultado de A-value sin alterar A
z80_byte sub_value(z80_byte value)
{
        z80_byte result,antes;

        result=reg_a;
        antes=reg_a;

        result -=value;

        set_flags_carry_resta(antes,result);
        set_flags_overflow_resta(antes,result);
        Z80_FLAGS=(Z80_FLAGS & (255-FLAG_Z-FLAG_S-FLAG_5-FLAG_3)) | FLAG_N | sz53_table[result];

	return result;


}

void sub_a_reg(z80_byte value)
{
	reg_a=sub_value(value);

}

void sbc_a_reg(z80_byte value)
{
        z80_byte result,lookup;
        z80_int result_16bit;

        result_16bit=reg_a;
	result_16bit=result_16bit-value-( Z80_FLAGS & FLAG_C );


        lookup = ( (       reg_a & 0x88 ) >> 3 ) |
                     ( ( (value) & 0x88 ) >> 2 ) |
                ( ( result_16bit & 0x88 ) >> 1 );

	result=value_16_to_8l(result_16bit);
        reg_a=result;


	Z80_FLAGS = ( result_16bit & 0x100 ? FLAG_C : 0 );

        Z80_FLAGS=Z80_FLAGS | halfcarry_sub_table[lookup & 0x07] | overflow_sub_table[lookup >> 4] | FLAG_N | sz53_table[result];


}




void rl_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
	set_memptr(puntero);
#endif

        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = rl_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}

void rr_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = rr_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}



void rlc_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
	z80_byte valor_leido;
	z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = rlc_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}

void rrc_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = rrc_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}

void sla_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;


	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = sla_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}

void sra_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = sra_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}

void srl_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = srl_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}

void sls_ixiy_desp_reg(z80_byte desp,z80_byte *registro)
{
        z80_byte valor_leido;
        z80_int desp16,puntero;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = sls_valor(valor_leido);
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
}


void res_bit_ixiy_desp_reg(z80_byte numerobit, z80_byte desp, z80_byte *registro)
{

	z80_byte valor_and,valor_leido;
	z80_int desp16,puntero;

	//printf ("res bit=%d desp=%d reg=%x   \n",numerobit,desp,registro);

	valor_and=1;

	if (numerobit) valor_and = valor_and << numerobit;

	//cambiar 0 por 1
	valor_and = valor_and ^ 0xFF;

	desp16=desp8_to_16(desp);
	puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


	valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
	valor_leido = valor_leido & valor_and;
	poke_byte(puntero,valor_leido);


	if (registro!=0) *registro=valor_leido;
	//printf (" valor_and = %d valor_final = %d \n",valor_and,valor_leido);

}

void set_bit_ixiy_desp_reg(z80_byte numerobit, z80_byte desp, z80_byte *registro)
{

        z80_byte valor_or,valor_leido;
        z80_int desp16,puntero;

        //printf ("set bit=%d desp=%d reg=%x   \n",numerobit,desp,registro);

        valor_or=1;

        if (numerobit) valor_or = valor_or << numerobit;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );
        valor_leido = valor_leido | valor_or;
        poke_byte(puntero,valor_leido);

        if (registro!=0) *registro=valor_leido;
        //printf (" valor_set = %d valor_final = %d \n",valor_or,valor_leido);

}

//void bit_bit_ixiy_desp_reg(z80_byte numerobit, z80_byte desp, z80_byte *registro)
void bit_bit_ixiy_desp_reg(z80_byte numerobit, z80_byte desp)
{

        z80_byte valor_and,valor_leido;
        z80_int desp16,puntero;

        //printf ("bit bit=%d desp=%d reg=%x   \n",numerobit,desp,registro);

        valor_and=1;

        if (numerobit) valor_and = valor_and << numerobit;

	desp16=desp8_to_16(desp);
        puntero=*registro_ixiy + desp16;

#ifdef EMULATE_MEMPTR
        set_memptr(puntero);
#endif


        valor_leido=peek_byte(puntero);
	contend_read_no_mreq( puntero, 1 );

//                                        ;Tambien hay que poner el flag P/V con el mismo valor que
//                                        ;coge el flag Z, y el flag S debe tener el valor del bit 7


	Z80_FLAGS=(Z80_FLAGS & (255-FLAG_N-FLAG_Z-FLAG_PV-FLAG_S)) | FLAG_H;

	if (!(valor_leido & valor_and) ) {
		Z80_FLAGS |=FLAG_Z|FLAG_PV;
	}

        if (numerobit==7 && (valor_leido & 128) ) Z80_FLAGS |=FLAG_S;

        //En teoria el BIT no tiene este comportamiento de poder asignar el resultado a un registro
        //if (registro!=0) *registro=valor_leido;

        //printf (" valor_bit = %d valor_final = %d \n",valor_or,valor_leido);

        set_undocumented_flags_bits(value_16_to_8h(puntero));

}




z80_byte *devuelve_reg_offset(z80_byte valor)
{

	//printf ("devuelve_reg de: %d\n",valor);
	switch (valor) {
		case 0:
			return &reg_b;
		;;
		case 1:
			return &reg_c;
		;;
		case 2:
			return &reg_d;
		;;
		case 3:
			return &reg_e;
		;;
		case 4:
			return &reg_h;
		;;
		case 5:
			return &reg_l;
		;;
		case 6:
			return 0;
		;;
		case 7:
			return &reg_a;
		;;

		default:
			cpu_panic("Critical Error devuelve_reg_offset valor>7");
			//aqui no deberia llegar nunca
			return 0;
		break;
	}

}



void rl_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

	if (registro==0) {
		//(HL)
		valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
		valor_leido=rl_valor(valor_leido);
        	poke_byte(HL,valor_leido);
	}

	else rl_reg(registro);

}

void rr_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=rr_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else rr_reg(registro);
}



void rlc_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=rlc_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else rlc_reg(registro);
}

void rrc_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=rrc_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else rrc_reg(registro);
}

void sla_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=sla_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else sla_reg(registro);
}

void sra_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=sra_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else sra_reg(registro);
}

void srl_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=srl_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else srl_reg(registro);

}

void sls_cb_reg(z80_byte *registro)
{
        z80_byte valor_leido;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido=sls_valor(valor_leido);
                poke_byte(HL,valor_leido);
        }

        else sls_reg(registro);
}


void res_bit_cb_reg(z80_byte numerobit, z80_byte *registro)
{

	z80_byte valor_and,valor_leido;

	//printf ("res bit=%d reg=%x  \n",numerobit,registro);

	valor_and=1;

	if (numerobit) valor_and = valor_and << numerobit;

	//cambiar 0 por 1
	valor_and = valor_and ^ 0xFF;

	if (registro==0) {
		//(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
		valor_leido=valor_leido & valor_and;
                poke_byte(HL,valor_leido);
	}
	else {
		valor_leido = (*registro) & valor_and;
		*registro = valor_leido;
	}

	//printf (" valor_and = %d valor_final = %d \n",valor_and,valor_leido);

}

void set_bit_cb_reg(z80_byte numerobit, z80_byte *registro)
{

        z80_byte valor_or,valor_leido;

	//printf ("set bit=%d reg=%x  \n",numerobit,registro);

        valor_or=1;

        if (numerobit) valor_or = valor_or << numerobit;

        if (registro==0) {
                //(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
                valor_leido = valor_leido | valor_or;
                poke_byte(HL,valor_leido);
        }
        else {

                valor_leido = (*registro) | valor_or;
                *registro = valor_leido;
        }

        //printf (" valor_set = %d valor_final = %d \n",valor_or,valor_leido);

}

void bit_bit_cb_reg(z80_byte numerobit, z80_byte *registro)
{

        z80_byte valor_or,valor_leido;

	//printf ("bit bit=%d reg=%x ",numerobit,registro);

        valor_or=1;

        if (numerobit) valor_or = valor_or << numerobit;

	if (registro==0) {
		//(HL)
                valor_leido=peek_byte(HL);
		contend_read_no_mreq( HL, 1 );
		set_undocumented_flags_bits_memptr();

	}
	else {
		valor_leido=*registro;
	        if (numerobit==5 && (valor_leido & 32 )) Z80_FLAGS |=FLAG_5;
	        else Z80_FLAGS &=(255-FLAG_5);

        	if (numerobit==3 && (valor_leido & 8 )) Z80_FLAGS |=FLAG_3;
	        else Z80_FLAGS &=(255-FLAG_3);

	}

//                                        ;Tambien hay que poner el flag P/V con el mismo valor que
//                                        ;coge el flag Z, y el flag S debe tener el valor del bit 7


	Z80_FLAGS=(Z80_FLAGS & (255-FLAG_N-FLAG_Z-FLAG_PV-FLAG_S)) | FLAG_H;

	if (!(valor_leido & valor_or) ) {
		Z80_FLAGS |=FLAG_Z|FLAG_PV;
	}

	if (numerobit==7 && (valor_leido & 128) ) Z80_FLAGS |=FLAG_S;

        //printf (" valor_bit = %d valor_final = %d \n",valor_or,valor_leido);


}


z80_byte lee_puerto_spectrum(z80_byte puerto_h,z80_byte puerto_l)
{
  z80_int port=value_8_to_16(puerto_h,puerto_l);
  ula_contend_port_early( port );
  ula_contend_port_late( port );
  z80_byte valor = lee_puerto_spectrum_no_time( puerto_h, puerto_l );

  t_estados++;

  return valor;

}


z80_byte idle_bus_port_atribute(void)
{

	//Retorna el byte que lee la ULA
	//de momento solo retornar el ultimo atributo, teniendo en cuenta que el rainbow debe estar habilitado
	//TODO: retornar tambien byte de pixeles
	//Si no esta habilitado rainbow, retornara algun valor aleatorio fijo

	//TODO: mirar que usa fuse en funcion spectrum_unattached_port


	//Si no esta habilitado rainbow y hay que detectar rainbow, habilitar rainbow, siempre que no este en ROM
	if (rainbow_enabled.v==0) {
		if (autodetect_rainbow.v) {
			if (reg_pc>=16384) {
				debug_printf(VERBOSE_INFO,"Autoenabling realvideo so the program seems to need it (Idle bus port reading on Spectrum)");
				enable_rainbow();
			}
		}

	}

//printf ("last ula attribute: %d t_states: %d\n",last_ula_attribute,t_estados);
	//Si estamos en zona de border, retornar 255
	/*
	Due to the resistors which decouple the two dedicated buses inside the Spectrum, when the Z80 reads from an unattached port
	(such as 0xFF) it actually reads the data currently present on the ULA bus, which may happen to be a byte being transferred
	by the ULA from video memory for the screen rendering. If the ULA is building the border, then its data bus is idle (0xFF),
	otherwise we are perfectly able to predict whether it is reading a bitmap byte, an attribute or again it's idle.
	This "nice" effect goes under the name of floating bus. Unfortunately some programs do rely on the exact behaviour of the
	floating bus, so we can't simply forget about it; notable examples are Arkanoid (first edition), Cobra, Sidewize, Duet and DigiSynth.
	*/
	int t_estados_en_linea=(t_estados % screen_testados_linea);
	if (t_estados_en_linea>=128) return 255;

	switch( t_estados_en_linea % 8 ) {
		/* Attribute bytes */
		case 5:
		case 3:
			return last_ula_attribute;
		break;

		/* Screen data */
		case 4:
		case 2:
		  //printf ("pixel: %d\n",last_ula_pixel);
			return last_ula_pixel;
		break;

		default:
		/* Idle bus */
			return 0xff;
		break;

	}


}


z80_byte idle_bus_port(z80_int puerto)
{
	debug_printf(VERBOSE_DEBUG,"Idle bus port reading: %x",puerto);
	z80_byte valor_idle=idle_bus_port_atribute();
	debug_printf(VERBOSE_DEBUG,"last ula attribute: %d t_states: %d\n",valor_idle,t_estados);
	return valor_idle;
}

z80_byte teclado_and_todas(z80_byte valor)
{
     int ceros=0;
                ceros +=util_return_ceros_byte(puerto_65278|224); //Valor de fila teclado poniendo los otros bits a 1
                ceros +=util_return_ceros_byte(puerto_65022|224);
                ceros +=util_return_ceros_byte(puerto_64510|224);
                ceros +=util_return_ceros_byte(puerto_63486|224);
                ceros +=util_return_ceros_byte(puerto_61438|224);
                ceros +=util_return_ceros_byte(puerto_57342|224);
                ceros +=util_return_ceros_byte(puerto_49150|224);
                ceros +=util_return_ceros_byte(puerto_32766|224);

                if (ceros>2) {
                    valor &= puerto_65278 & puerto_65022 & puerto_64510 & puerto_63486 & puerto_61438 & puerto_57342 & puerto_49150 & puerto_32766;
                }
    return valor;
}

z80_byte teclado_return_valor_fila(z80_byte fila)
{
    switch (fila) {
        case 0:
            return puerto_65278;
        break;

        case 1:
            return puerto_65022;
        break;

        case 2:
            return puerto_64510;
        break;

        case 3:
            return puerto_63486;
        break;

        case 4:
            return puerto_61438;
        break;

        case 5:
            return puerto_57342;
        break;

        case 6:
            return puerto_49150;
        break;

        case 7:
            return puerto_32766;
        break;

        default:
            //no deberia suceder
            return 255;
        break;


    }
}

/*
Lo que ocurre exactamente es lo siguiente: cuando se pulsan dos teclas que pertenecen a distintas filas, 
pero que pertenecen a la misma columna (como la A y la G en el ejemplo), las filas de ambas teclas adquieren 
el potencial de 0 voltios, así que aunque nosotros hayamos seleccionado un fila para leer, en realidad se 
están seleccionando dos filas para leer. Si en la fila que no pretendíamos leer hay más de una tecla pulsada (la I), 
ésta obviamente aparecerá en la línea de salida.
*/


//Para una fila que vamos a leer, comparamos si con el resto de filas, coinciden teclas en misma columna
//si es asi, se agregara esa fila para leer en el puerto final

//Para saber si se pulsan teclas de misma columna, hacer xor a nivel de bit

z80_byte teclado_matrix_que_filas(z80_byte fila,z80_byte puerto_h)
{
    int i;

    z80_byte valor_fila_leida=teclado_return_valor_fila(fila);
    z80_byte mascara_fila=254; //11111110 realmente movemos el 0 a la izquierda

    for (i=0;i<8;i++) {
        if (i!=fila) { //No comparar fila con ella misma
            int bit;
            int mascara_bit=1;
            for (bit=0;bit<5;bit++) {
                //Si se ha pulsado esa tecla en la fila que estamos observando
                if ((valor_fila_leida&mascara_bit)==0) {
                    //Ver si se ha pulsado tambien tecla en la fila con la que comparamos
                    if ((teclado_return_valor_fila(i)&mascara_bit)==0) {
                        //Leeremos fila adicional
                        puerto_h &=mascara_fila;
                    }
                }
                mascara_bit=mascara_bit<<1;
            }
        }
        mascara_fila=(mascara_fila<<1)|1; //Bit que resetearemos para leer fila adicional si conviene
    }

    return puerto_h;
}

//Que puerto o puertos se leeran finalmente aplicando bug de matrix error
z80_byte teclado_matrix_puerto_final(z80_byte puerto_h)
{
    int mascara=1;
    int i;

    z80_byte final_puerto_h=puerto_h;

    if (keyboard_matrix_error.v) {

        //char puerto_binario[9];
        
        //util_byte_to_binary(puerto_h,puerto_binario);
        //printf ("Port to read: %02XH (%s) ",puerto_h,puerto_binario);

        for (i=0;i<8;i++) {
          if ((puerto_h&mascara)==0) final_puerto_h=teclado_matrix_que_filas(i,puerto_h);
          mascara=mascara<<1;
        }

        //util_byte_to_binary(final_puerto_h,puerto_binario);
        //printf ("Port finally read: %02XH (%s)\n",final_puerto_h,puerto_binario);

    }



    return final_puerto_h;

}

z80_byte lee_puerto_teclado(z80_byte puerto_h)
{

			z80_byte acumulado;

                //puerto teclado

                //si estamos en el menu, no devolver tecla
                if (zxvision_key_not_sent_emulated_mach() ) return 255;


		//Si esta spool file activo, generar siguiente tecla
		if (input_file_keyboard_is_playing() ) {
			if (input_file_keyboard_turbo.v==0) {
				input_file_keyboard_get_key();
			}

			else {
				//en modo turbo enviamos cualquier tecla pero la rom realmente lee de la direccion 23560
				ascii_to_keyboard_port(' ');
			}
		}



                        acumulado=255;

            puerto_h=teclado_matrix_puerto_final(puerto_h);

                        //A zero in one of the five lowest bits means that the corresponding key is pressed. 
                        //If more than one address line is made low, the result is the logical AND of all single inputs, 
                        //so a zero in a bit means that at least one of the appropriate keys is pressed. 
                        //For example, only if each of the five lowest bits of the result from reading from Port 00FE 
                        //(for instance by XOR A/IN A,(FE)) is one, no key is pressed

            if ((puerto_h & 1) == 0)   {
				acumulado &=puerto_65278;

                //acumulado=teclado_matrix_error(puerto_65278,acumulado);

				//Si hay alguna tecla del joystick cursor pulsada, enviar tambien shift
//z80_byte puerto_65278=255; //    db    255  ; V    C    X    Z    Sh    ;0
				if (joystick_emulation==JOYSTICK_CURSOR_WITH_SHIFT && puerto_especial_joystick!=0) {
					acumulado &=(255-1);
				}
			}


            if ((puerto_h & 2) == 0)   {
				acumulado &=puerto_65022;
                //acumulado=teclado_matrix_error(puerto_65022,acumulado);

                                //OPQASPACE Joystick
                                if (joystick_emulation==JOYSTICK_OPQA_SPACE) {
                                        if ((puerto_especial_joystick&4)) acumulado &=(255-1);
                                }

			}


            if ((puerto_h & 4) == 0)   {
				acumulado &=puerto_64510;
                //acumulado=teclado_matrix_error(puerto_64510,acumulado);

                                //OPQASPACE Joystick
                                if (joystick_emulation==JOYSTICK_OPQA_SPACE) {
                                        if ((puerto_especial_joystick&8)) acumulado &=(255-1);
                                }

			}




//z80_byte puerto_especial_joystick=0; //Fire Up Down Left Right

			//Para cursor, sinclair joystick
//z80_byte puerto_63486=255; //    db              255  ; 5    4    3    2    1     ;3


            if ((puerto_h & 8) == 0)   {
				acumulado &=puerto_63486;
                //acumulado=teclado_matrix_error(puerto_63486,acumulado);
				//sinclair 2 joystick
				if (joystick_emulation==JOYSTICK_SINCLAIR_2) {
					if ((puerto_especial_joystick&1)) acumulado &=(255-2);
					if ((puerto_especial_joystick&2)) acumulado &=(255-1);
					if ((puerto_especial_joystick&4)) acumulado &=(255-4);
					if ((puerto_especial_joystick&8)) acumulado &=(255-8);
					if ((puerto_especial_joystick&16)) acumulado &=(255-16);
				}

				//cursor joystick 5 iz 8 der 6 abajo 7 arriba 0 fire
				if (joystick_emulation==JOYSTICK_CURSOR || joystick_emulation==JOYSTICK_CURSOR_WITH_SHIFT) {
					if ((puerto_especial_joystick&2)) acumulado &=(255-16);
				}

		                //gunstick
                                if (gunstick_emulation==GUNSTICK_SINCLAIR_2) {
                                        if (mouse_left!=0) {

                                                acumulado &=(255-1);

                                                if (gunstick_view_white()) acumulado &=(255-4);


                                        }
                                }

			}

//z80_byte puerto_61438=255; //    db              255  ; 6    7    8    9    0     ;4


            if ((puerto_h & 16) == 0)  {
				acumulado &=puerto_61438;
                //acumulado=teclado_matrix_error(puerto_61438,acumulado);

				//sinclair 1 joystick
				if (joystick_emulation==JOYSTICK_SINCLAIR_1) {
					if ((puerto_especial_joystick&1)) acumulado &=(255-8);
					if ((puerto_especial_joystick&2)) acumulado &=(255-16);
					if ((puerto_especial_joystick&4)) acumulado &=(255-4);
					if ((puerto_especial_joystick&8)) acumulado &=(255-2);
					if ((puerto_especial_joystick&16)) acumulado &=(255-1);
				}
				//cursor joystick 5 iz 8 der 6 abajo 7 arriba 0 fire
                                if (joystick_emulation==JOYSTICK_CURSOR  || joystick_emulation==JOYSTICK_CURSOR_WITH_SHIFT) {
                                        if ((puerto_especial_joystick&1)) acumulado &=(255-4);
                                        if ((puerto_especial_joystick&4)) acumulado &=(255-16);
                                        if ((puerto_especial_joystick&8)) acumulado &=(255-8);
                                        if ((puerto_especial_joystick&16)) acumulado &=(255-1);
                                }


				//gunstick
				if (gunstick_emulation==GUNSTICK_SINCLAIR_1) {
					if (mouse_left!=0) {

						acumulado &=(255-1);

						if (gunstick_view_white()) acumulado &=(255-4);


					}
				}
			}


            if ((puerto_h & 32) == 0)  {
				acumulado &=puerto_57342;
                //acumulado=teclado_matrix_error(puerto_57342,acumulado);

                                //OPQASPACE Joystick
                                if (joystick_emulation==JOYSTICK_OPQA_SPACE) {
                                        if ((puerto_especial_joystick&1)) acumulado &=(255-1);
                                        if ((puerto_especial_joystick&2)) acumulado &=(255-2);
                                }


			}

            if ((puerto_h & 64) == 0)  {
                acumulado &=puerto_49150;
                //acumulado=teclado_matrix_error(puerto_49150,acumulado);
            }


            if ((puerto_h & 128) == 0) {
				acumulado &=puerto_32766;

                //acumulado=teclado_matrix_error(puerto_32766,acumulado);

				//OPQASPACE Joystick
				if (joystick_emulation==JOYSTICK_OPQA_SPACE) {
					if ((puerto_especial_joystick&16)) acumulado &=(255-1);
				}

			}


            return acumulado;


}


//Devuelve valor para el puerto de kempston (joystick o gunstick kempston)
z80_byte get_kempston_value(void)
{

                        z80_byte acumulado=0;

                        //si estamos con menu abierto, no retornar nada
                        if (zxvision_key_not_sent_emulated_mach() ) return 0;

                        if (joystick_emulation==JOYSTICK_KEMPSTON) {
                                //mapeo de ese puerto especial es igual que kempston
                                acumulado=puerto_especial_joystick;
                        }

                       //gunstick
                       if (gunstick_emulation==GUNSTICK_KEMPSTON) {

                                if (mouse_left!=0) {

                                    acumulado |=16;

                                    if (gunstick_view_white()) acumulado |=4;

                                }
                        }

                        return acumulado;
}

z80_byte lee_puerto_spectrum_ula(z80_byte puerto_h)
{

                z80_byte valor;
                valor=lee_puerto_teclado(puerto_h);
                //teclado issue 2 o 3

                //issue 3
                if (keyboard_issue2.v==0) valor=(valor & (255-64));

                //issue 2
                else valor=valor|64;


                return valor;


}


//Devuelve valor puerto para maquinas Spectrum
z80_byte lee_puerto_spectrum_no_time(z80_byte puerto_h,z80_byte puerto_l)
{

	debug_fired_in=1;
	//extern z80_byte in_port_ay(z80_int puerto);
	//65533 o 49149
	//FFFDh (65533), BFFDh (49149)

	z80_int puerto=value_8_to_16(puerto_h,puerto_l);


	if (puerto_l==0xFD) {
		if (puerto_h==0xFF) {
			activa_ay_chip_si_conviene();
			if (ay_chip_present.v==1) return in_port_ay(puerto_h);
		}
		if (puerto_h==0x2F) return 255;
		if (puerto_h==0x3F) return 255;

	}


		if (multiface_enabled.v) {
			/*
			xx9F ........             Multiface I In
	xx1F ........             Multiface I Out
	xxBF ........             Multiface 128 In
	xx9F ........             Multiface 128 In v2 (Disciple) (uh/what?)
	xx3F ........             Multiface 128 Out
	xx3F ........             Multiface III Button
	xx3F ........             Multiface III In
	xxBF ........             Multiface III Out
	7F3F                      Multiface III P7FFD (uh?)
	1F3F                      Multiface III P1FFD (uh?)
	FF3F                      British Micro Grafpad Pen up/
			*/

			if (puerto_l==0x1f && multiface_type==MULTIFACE_TYPE_ONE) {
				multiface_unmap_memory();
			}

			if (puerto_l==0x3f) {
				switch(multiface_type)
				{
					case MULTIFACE_TYPE_128:
						multiface_unmap_memory();
						return 0xff;
					break;

					case MULTIFACE_TYPE_THREE:
						multiface_map_memory();
						if (puerto_h==0x7f) return puerto_32765;
						if (puerto_h==0x1f) return puerto_8189;
						return 0xff;
					break;
				}

			}

			if (puerto_l==0x9f && multiface_type==MULTIFACE_TYPE_ONE) {
				multiface_map_memory();
			}

			if (puerto_l==0xbf) {
				switch(multiface_type)
				{
					case MULTIFACE_TYPE_128:
						if (!multiface_lockout) {
							multiface_map_memory();
							return (puerto_32765&8)<<4;
						}
						return 0xff;
					break;

					case MULTIFACE_TYPE_THREE:
						multiface_unmap_memory();
						return 0xff;
					break;
				}
			}


		}




	//Fuller audio box.
	if ((puerto_l==0x3f && puerto_h==0)) {

		activa_ay_chip_si_conviene();
		if (ay_chip_present.v==1) {

			return in_port_ay(0xFF);

		}

	}


	//Fuller Joystick
	//The Fuller Audio Box included a joystick interface. Results were obtained by reading from port 0x7f in the form F---RLDU, with active bits low.
        if (puerto_l==0x7f) {
                if (joystick_emulation==JOYSTICK_FULLER) {
			z80_byte valor_joystick=255;
			//si estamos con menu abierto, no retornar nada
			if (zxvision_key_not_sent_emulated_mach() ) return valor_joystick;


			if ((puerto_especial_joystick&1)) valor_joystick &=(255-8);
			if ((puerto_especial_joystick&2)) valor_joystick &=(255-4);
			if ((puerto_especial_joystick&4)) valor_joystick &=(255-2);
			if ((puerto_especial_joystick&8)) valor_joystick &=(255-1);
			if ((puerto_especial_joystick&16)) valor_joystick &=(255-128);

                        return valor_joystick;
                }

        }

	//zx printer
	if (puerto_l==0xFB) {
                if (zxprinter_enabled.v==1) {
			return zxprinter_get_port();
                }


        }

	//ulaplus
	if (ulaplus_presente.v && puerto_l==0x3B && puerto_h==0xFF) {
		return ulaplus_return_port_ff3b();
	}


	//Puerto DF parece leerse desde juego Bestial Warrior en el menu, aunque dentro del juego no se lee
	//Quiza hace referencia a la Stack Light Rifle

	//Dejarlo comentado dado que entra en conflicto con kempston mouse

	/*

	if ( puerto_l==0xdf) {
		//gunstick.
//Stack Light Rifle (Stack Computer Services Ltd) (1983)
//connects to expansion port, accessed by reading from Port DFh:

//  bit1       = Trigger Button (0=Pressed, 1=Released)
//  bit4       = Light Sensor   (0=Light, 1=No Light)
//  other bits = Must be "1"    (the programs use compare FDh to test if bit1=0)

               if (gunstick_emulation==GUNSTICK_PORT_DF) {

                        z80_byte acumulado=255;
                        if (mouse_left!=0) {

                            acumulado &=(255-2);

                            if (gunstick_view_white()) acumulado &=(255-16);


                        }

			printf ("gunstick acumulado: %d\n",acumulado);

                        return acumulado;
                }

		//puerto kempston tambien es DF en kempston mouse
		//else return idle_bus_port(puerto_l+256*puerto_h);

	}

	*/

	//kempston mouse. Solo con menu cerrado
	if ( !menu_abierto && kempston_mouse_emulation.v  &&  (puerto_l&32) == 0  &&  ( (puerto_h&7)==3 || (puerto_h&7)==7 || (puerto_h&2)==2 ) ) {
		//printf ("kempston mouse. port 0x%x%x\n",puerto_h,puerto_l);

//IN 64479 - return X axis (0-255)
//IN 65503 - return Y axis (0-255)
//IN 64223 - return button status
//D0 - right button
//D1 - left button
//D2-D7 - not used

//From diagram ports

//X-Axis  = port 64479 xxxxx011 xx0xxxxx
//Y-Axis  = port 65503 xxxxx111 xx0xxxxx
//BUTTONS = port 64223 xxxxxx10 xx0xxxxx

		z80_byte acumulado=0;


		if ((puerto_h&7)==3) {
			//X-Axis
			acumulado=kempston_mouse_x*kempston_mouse_factor_sensibilidad;
		}

                if ((puerto_h&7)==7) {
                        //Y-Axis
			acumulado=kempston_mouse_y*kempston_mouse_factor_sensibilidad;
                }

                if ((puerto_h&3)==2) {
                        //Buttons
			acumulado=255;

			//left button
			if (mouse_left) acumulado &=(255-2);
                        //right button
                        if (mouse_right) acumulado &=(255-1);
                        
                        //en Next, bits altos se usan para wheel, como no los emulamos, a 0
                        //https://specnext.dev/wiki/Kempston_Mouse_Buttons
                        acumulado &=0x0F;
                }

		//printf ("devolvemos valor: %d\n",acumulado);
		return acumulado;

	}

	//If you read from a port that activates both the keyboard and a joystick port (e.g. Kempston), the joystick takes priority.

        //kempston joystick en Inves
	//En Inves, Si A5=0 solamente
        //kempston joystick para maquinas no Inves
        //Si A5=A6=A7=0 y A0=1, kempston joystick
        if ( (puerto_l & (1+32+64+128)) == 1) {

                if (joystick_emulation==JOYSTICK_KEMPSTON || gunstick_emulation==GUNSTICK_KEMPSTON) {
			return get_kempston_value();
                }
	}

	{
		//Puertos divmmc sin tener que habilitar divmmc paging.
		//if (divmmc_mmc_ports_enabled.v && mmc_enabled.v==1 && puerto_l == 0xEB) return mmc_read();


		//Las puertas que se pueden leer son: 24D5, 24DD y 24DF.
		//if (puerto==0x24D5) return tbblue_read_port_24d5();
		//if (puerto==0x24DD) return tbblue_config2;
		//if (puerto==0x24DF) return tbblue_port_24df;
		if (puerto==TBBLUE_REGISTER_PORT) return tbblue_get_register_port();
		if (puerto==TBBLUE_VALUE_PORT) return tbblue_get_value_port();
		if (puerto==TBBLUE_SPRITE_INDEX_PORT)	return tbblue_get_port_sprite_index();
		if (puerto==TBBLUE_LAYER2_PORT)	return tbblue_get_port_layer2_value();

		if (puerto==DS1307_PORT_CLOCK) return ds1307_get_port_clock();
		if (puerto==DS1307_PORT_DATA) return ds1307_get_port_data();

		//Puertos DIVMMC/DIVIDE. El de Paginacion
		//Este puerto solo se puede leer en TBBLUE y es necesario para que NextOS funcione bien
		//if (puerto_l==0xe3 && diviface_enabled.v) return diviface_read_control_register();
		if (puerto_l==0xe3) return diviface_read_control_register();

		//TODO puerto UART de tbbue. De momento retornamos 0, en la demo de Pogie espera que ese valor (el bit 1 concretamente)
		//sea 0 antes de empezar
		// http://devnext.referata.com/wiki/UART_TX
		// https://www.specnext.com/the-next-on-the-network/
		//if (puerto==TBBLUE_UART_RX_PORT) return 0;


		if (puerto==TBBLUE_UART_RX_PORT) return tbblue_uartbridge_readdata();

		//puerto estado es el de escritura pero en lectura
		if (puerto==TBBLUE_UART_TX_PORT) return tbblue_uartbridge_readstatus();

		//TODO puerto segundo joystick. De momento retornar 0
		if (puerto==TBBLUE_SECOND_KEMPSTON_PORT) return 0;


	}

	if (datagear_dma_emulation.v && (puerto_l==DATAGEAR_DMA_FIRST_PORT || puerto_l==DATAGEAR_DMA_SECOND_PORT) ) {
			//printf ("Reading Datagear DMA Port %04XH\n",puerto);
			//TODO
			return 0;
	}

	//Puertos DIVMMC
	if (divmmc_mmc_ports_enabled.v && (puerto_l==0xe7 || puerto_l==0xeb) ) {
		//printf ("Puerto DIVMMC Read: 0x%02x\n",puerto_l);

	        //Si en ZXUNO y DIVEN desactivado.
		//Aunque cuando se toca el bit DIVEN de ZX-Uno se sincroniza divmmc_enable,
		//pero por si acaso... por si se activa manualmente desde menu del emulador
		//el divmmc pero zxuno espera que este deshabilitado, como en la bios
	        //if (MACHINE_IS_ZXUNO_DIVEN_DISABLED) return 255;

                if (puerto_l==0xeb) {
                        z80_byte valor_leido=mmc_read();
                        //printf ("Valor leido: %d\n",valor_leido);
                        return valor_leido;
                }

                return 255;

        }


        //Puerto ULA, cualquier puerto par. En un Spectrum normal, esto va al final
	//En un Inves, deberia ir al principio, pues el inves hace un AND con el valor de los perifericos que retornan valor en el puerto leido
        if ( (puerto_l & 1)==0 ) {

		return lee_puerto_spectrum_ula(puerto_h);

        }


        //Puerto Timex Video. 8 bit bajo a ff
        if (timex_video_emulation.v && puerto_l==0xFF) {
		return timex_port_ff;
        }
	z80_byte valor_idle_bus_port=idle_bus_port(puerto_l+256*puerto_h);

	//ZEsarUX ZXI ports
    if (hardware_debug_port.v) {
	   if (puerto==ZESARUX_ZXI_PORT_REGISTER) return zesarux_zxi_read_last_register();
	   if (puerto==ZESARUX_ZXI_PORT_DATA)     return zesarux_zxi_read_register_value();
    }

	return valor_idle_bus_port;

}











void cpi_cpd_common(void)
{

	z80_byte antes,result;

	antes=reg_a;
        result=reg_a-peek_byte(HL);

        contend_read_no_mreq( HL, 1 ); contend_read_no_mreq( HL, 1 );
        contend_read_no_mreq( HL, 1 ); contend_read_no_mreq( HL, 1 );
        contend_read_no_mreq( HL, 1 );



        set_undocumented_flags_bits(result);
        set_flags_zero_sign(result);
	set_flags_halfcarry_resta(antes,result);
        Z80_FLAGS |=FLAG_N;


        BC--;

        if (!BC) Z80_FLAGS &=(255-FLAG_PV);
        else Z80_FLAGS |=FLAG_PV;

	if ((Z80_FLAGS & FLAG_H)) result--;

        if (result & 8 ) Z80_FLAGS |=FLAG_3;
        else Z80_FLAGS &=(255-FLAG_3);

        if (result & 2 ) Z80_FLAGS |=FLAG_5;
        else Z80_FLAGS &=(255-FLAG_5);


}


//Extracted from Fuse emulator
void set_value_beeper (int v)
{
  if (reg_pc>1200 && reg_pc<1350 && output_beep_filter_on_rom_save.v) {
		//Estamos en save.
		//printf ("valor beeper: %d\n",v);
		value_beeper=( v ? AMPLITUD_TAPE*2 : -AMPLITUD_TAPE*2);

		//Si activamos modo alteracion beeper. Ideal para que se escuche mas alto y poder enviar a inves
		/*
		En audacity, despues de exportar con valor 122 de beeper, aplicar reduccion de ruido:
		db 3, sensibilidad 0, suavidad 150 hz, ataque 0.15
		Tambien se puede aplicar reduccion de agudos -5
		Luego reproducir con volumen del pc al maximo
		*/

		if (output_beep_filter_alter_volume.v) {
			//value_beeper=( v ? 122 : -122);
			value_beeper=( v ? output_beep_filter_volume : -output_beep_filter_volume);
		}

	

  }

  else {
	value_beeper = v;
  }



}

z80_byte get_border_colour_from_out(void)
{
z80_byte color_border;

                                                color_border=out_254 & 7;


	return color_border;
}




void out_port_spectrum_border(z80_byte value)
{
	//printf ("out port 254 desde reg_pc=%d. puerto: %04XH value: %02XH\n",reg_pc,puerto,value);

	//Guardamos temporalmente el valor anterior para compararlo con el actual
	//en el metodo de autodeteccion de border real video
	z80_byte anterior_out_254=out_254;

            modificado_border.v=1;

		out_254_original_value=value;

		//printf ("valor mic y ear: %d\n",value&(8+16));
		//printf ("pc: %d\n",reg_pc);
                        out_254=value;
                        set_value_beeper(value & 0x18);

                if (rainbow_enabled.v) {
                        //printf ("%d\n",t_estados);
                        int i;

			i=t_estados;
                        //printf ("t_estados %d screen_testados_linea %d bord: %d\n",t_estados,screen_testados_linea,i);

			//Este i>=0 no haria falta en teoria
			//pero ocurre a veces que justo al activar rainbow, t_estados_linea_actual tiene un valor descontrolado
                        if (i>=0 && i<CURRENT_FULLBORDER_ARRAY_LENGTH) {

				//Ajuste valor de indice a multiple de 4 (lo normal en la ULA)
				//O sea, quitamos los 2 bits inferiores
				//Excepto en pentagon
				if (pentagon_timing.v==0) i=i&(0xFFFFFFFF-3);

				{
					int actualiza_fullbuffer_border=1;
					//No si esta desactivado en tbblue
					if (tbblue_store_scanlines_border.v==0) {
						actualiza_fullbuffer_border=0;
					}

					if (actualiza_fullbuffer_border) {
						fullbuffer_border[i]=get_border_colour_from_out();
					}
				}
				//printf ("cambio border i=%d color: %d\n",i,out_254 & 7);
			}
                        //else printf ("Valor de border scanline fuera de rango: %d\n",i);
                }


		else {
			//Realvideo desactivado. Si esta autodeteccion de realvideo, hacer lo siguiente
			if (autodetect_rainbow.v) {
				//if (reg_pc>=16384) {
					//Ver si hay cambio de border
					if ( (anterior_out_254&7) != (out_254&7) ) {
						//printf ("cambio de border\n");
						detect_rainbow_border_changes_in_frame++;
					}
				//}
			}
		}

		set_value_beeper_on_array(value_beeper);

}


void out_port_spectrum_no_time(z80_int puerto,z80_byte value)
{

	debug_fired_out=1;
        //Los OUTS los capturan los diferentes interfaces que haya conectados, por tanto no hacer return en ninguno, para que se vayan comprobando
        //uno despues de otro
	z80_byte puerto_l=puerto&255;

        //super wonder boy usa puerto 1fe
        //paperboy usa puertos xxfe
	//Puerto 254 realmente es cualquier puerto par
       	if ( (puerto & 1 )==0 ) {
		out_port_spectrum_border(value);
        }


//ay chip
/*
El circuito de sonido contiene dieciseis registros; para seleccionarlos, primero se escribe
el numero de registro en la puerta de escritura de direcciones, FFFDh (65533), y despues
lee el valor del registro (en la misma direccion) o se escribe en la direccion de escritura
de registros de datos, BFFDh (49149). Una vez seleccionado un registro, se puede realizar
cualquier numero de operaciones de lectura o escritura de datos. S~1o habr~ que volver
escribir en la puerta de escritura de direcciones cuando se necesite seleccio~ar otro registro.
La frecuencia de reloj basica de este circuito es 1.7734 MHz (con precision del 0.01~~o).
*/
	//el comportamiento de los puertos ay es con mascaras AND... esto se ve asi en juegos como chase hq
/*
Peripheral: 128K AY Register.
Port: 11-- ---- ---- --0-

Peripheral: 128K AY (Data).
Port: 10-- ---- ---- --0-

49154=1100000000000010

*/

	//Puertos Chip AY
	if ( ((puerto & 49154) == 49152) || ((puerto & 49154) == 32768) ) {
		z80_int puerto_final=puerto;

		if ((puerto & 49154) == 49152) puerto_final=65533;
		else if ((puerto & 49154) == 32768) puerto_final=49149;

		activa_ay_chip_si_conviene();
		if (ay_chip_present.v==1) out_port_ay(puerto_final,value);

	}


	//Puertos especiales de TBBLUE y de paginacion 128kb
	{
		//if (puerto==0x24D9 || puerto==0x24DB || puerto==0x24DD || puerto==0x24DF) tbblue_out_port(puerto,value);
		if (puerto==TBBLUE_REGISTER_PORT) tbblue_set_register_port(value);
		if (puerto==TBBLUE_VALUE_PORT) tbblue_set_value_port(value);

		//Puerto tipicamente 32765
		// the hardware will respond only to those port addresses with
		//bit 1 reset, bit 14 set and bit 15 reset (as opposed to just bits 1 and 15 reset on the 128K/+2).
        if ( (puerto & 49154) == 16384 ) tbblue_out_port_32765(value);			


		//Puerto tipicamente 8189
			// the hardware will respond to all port addresses with bit 1 reset, bit 12 set and bits 13, 14 and 15 reset).
		if ( (puerto & 61442 )== 4096) tbblue_out_port_8189(value);

		if (puerto==TBBLUE_SPRITE_INDEX_PORT)	tbblue_out_port_sprite_index(value);
		if (puerto==TBBLUE_LAYER2_PORT) tbblue_out_port_layer2_value(value);

		//if (puerto_l==TBBLUE_SPRITE_PALETTE_PORT)	tbblue_out_sprite_palette(value);
		if (puerto_l==TBBLUE_SPRITE_PATTERN_PORT) tbblue_out_sprite_pattern(value);

		//Mantenemos puerto 0x55 port compatibilidad temporalmente. Se eliminara al subir nueva beta o la estable
		if (puerto_l==0x55) tbblue_out_sprite_pattern(value);

		if (puerto_l==TBBLUE_SPRITE_SPRITE_PORT) tbblue_out_sprite_sprite(value);

		if (puerto==DS1307_PORT_CLOCK) ds1307_write_port_clock(value);
		if (puerto==DS1307_PORT_DATA) ds1307_write_port_data(value);

		if (puerto==TBBLUE_UART_TX_PORT) tbblue_uartbridge_writedata(value);				


	}

	if (datagear_dma_emulation.v && (puerto_l==DATAGEAR_DMA_FIRST_PORT || puerto_l==DATAGEAR_DMA_SECOND_PORT) ) {
			//printf ("Writing Datagear DMA port %04XH with value %02XH\n",puerto,value);
			datagear_write_value(value);
	}

	//Fuller audio box
	//The sound board works on port numbers 0x3f and 0x5f. Port 0x3f is used to select the active AY register and to
	//receive data from the AY-3-8912, while port 0x5f is used for sending data
	//Interfiere con MMC
	if ( ( puerto==0x3f || puerto==0x5f) ) {
		activa_ay_chip_si_conviene();
		if (ay_chip_present.v==1) {
			z80_int puerto_final=puerto;
			//printf("out Puerto sonido chip AY puerto=%d valor=%d\n",puerto,value);
                	if (puerto==0x3f) puerto_final=65533;
	                else if (puerto==0x5f) puerto_final=49149;
        	        //printf("out Puerto cambiado sonido chip AY puerto=%d valor=%d\n",puerto,value);

                	out_port_ay(puerto_final,value);

		}
	}

	//zx printer
	if ((puerto&0xFF)==0xFB) {
		if (zxprinter_enabled.v==1) {
			zxprinter_write_port(value);

		}
	}



  //UlaPlus
  //if (ulaplus_presente.v && (puerto&0xFF)==0x3b) {
	if (ulaplus_presente.v && (puerto==0xbf3b || puerto==0xff3b) ) {
		ulaplus_write_port(puerto,value);
	}


        //Puertos DIVMMC. El de MMC
        if (divmmc_mmc_ports_enabled.v && (puerto_l==0xe7 || puerto_l==0xeb) ) {
		//printf ("Puerto DIVMMC Write: 0x%02x valor: 0x%02X\n",puerto_l,value);
        	//Si en ZXUNO y DIVEN desactivado
                //Aunque cuando se toca el bit DIVEN de ZX-Uno se sincroniza divmmc_enable,
                //pero por si acaso... por si se activa manualmente desde menu del emulador
                //el divmmc pero zxuno espera que este deshabilitado, como en la bios
	        //if (MACHINE_IS_ZXUNO_DIVEN_DISABLED) return;
		//if (!MACHINE_IS_ZXUNO_DIVEN_DISABLED) {

	                if (puerto_l==0xe7) {
				//Parece que F6 es la tarjeta 0 en divmmc
				if (value==0xF6) value=0xFE;
				mmc_cs(value);
			}
        	        if (puerto_l==0xeb) mmc_write(value);
		//}
        }

	//Puertos DIVMMC/DIVIDE. El de Paginacion
	if (diviface_enabled.v && puerto_l==0xe3) {
	        //Si en ZXUNO y DIVEN desactivado
                //Aunque cuando se toca el bit DIVEN de ZX-Uno se sincroniza divmmc_enable,
                //pero por si acaso... por si se activa manualmente desde menu del emulador
                //el divmmc pero zxuno espera que este deshabilitado, como en la bios
        	//if (MACHINE_IS_ZXUNO_DIVEN_DISABLED) return;

			//printf ("Puerto control paginacion DIVMMC Write: 0x%02x valor: 0x%02X\n",puerto_l,value);
			//printf ("antes control register: %02XH paginacion automatica: %d\n",diviface_control_register,diviface_paginacion_automatica_activa.v);
			diviface_write_control_register(value);
			//printf ("despues control register: %02XH paginacion automatica: %d\n",diviface_control_register,diviface_paginacion_automatica_activa.v);
	}


	//Puerto Timex Video. 8 bit bajo a ff
	if (timex_video_emulation.v && puerto_l==0xFF) {
		set_timex_port_ff(value);
	}

	//DAC Audio
	if (audiodac_enabled.v && puerto_l==audiodac_types[audiodac_selected_type].port) {
		//Parche para evitar bug de envio de sonido en esxdos. que activa modo turbo en zxbadaloc y coincide con puerto DF de Specdrum
		//printf ("PC %04XH\n",reg_pc);
		int sonido=1;

		if (puerto_l==0xDF && reg_pc<0x2000) sonido=0;

		if (sonido) {
			audiodac_send_sample_value(value);
			//audiodac_last_value_data=value;
			//silence_detection_counter=0;
		}
	}


	//No estoy seguro que esto también haga falta en el caso de OUT
	//Parece que si lo habilito aqui tambien, hacer un "save" desde multiface no funciona, se cuelga
	if (multiface_enabled.v) {
		if (puerto_l==0x3f && multiface_type==MULTIFACE_TYPE_128) {
			//printf ("Setting multiface lockout to 1\n");
			multiface_lockout=1;
		}
	}

//ZEsarUX ZXI ports
    if (hardware_debug_port.v) {
	   if (puerto==ZESARUX_ZXI_PORT_REGISTER) zesarux_zxi_write_last_register(value);
	   if (puerto==ZESARUX_ZXI_PORT_DATA)     zesarux_zxi_write_register_value(value);
    }


	//Prueba pentevo
	if (puerto_l==0xAF) {
		//printf ("Out port Pentevo %04XH value %02XH\n",puerto,value);
	}


	//debug_printf (VERBOSE_DEBUG,"Out Port %x unknown written with value %x, PC after=0x%x",puerto,value,reg_pc);
}





void out_port_spectrum(z80_int puerto,z80_byte value)
{
  ula_contend_port_early( puerto );
  out_port_spectrum_no_time(puerto,value);
  ula_contend_port_late( puerto ); t_estados++;
}

