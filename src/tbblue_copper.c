/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"

z80_byte tbblue_copper_memory[TBBLUE_COPPER_MEMORY];
z80_int tbblue_copper_pc=0;

//Obtiene posicion de escritura del copper
z80_int tbblue_copper_get_write_position(void)
{
	z80_int posicion;
	posicion=tbblue_registers[97] | ((tbblue_registers[98]&7)<<8);
	return posicion;
}

//Establece posicion de escritura del copper
void tbblue_copper_set_write_position(z80_int posicion)
{
	tbblue_registers[97]=posicion&0xFF;

	z80_byte msb=(posicion>>8)&7; //3 bits bajos

	z80_byte reg98=tbblue_registers[98];
	reg98 &=(255-7);
	reg98 |=msb;
	tbblue_registers[98]=reg98;
}

void tbblue_copper_increment_write_position(void)
{
	z80_int posicion=tbblue_copper_get_write_position();

	posicion++;
	tbblue_copper_set_write_position(posicion);
}


//Escribe dato copper en posicion de escritura
void tbblue_copper_write_data(z80_byte value)
{
	z80_int posicion=tbblue_copper_get_write_position();

	posicion &=(TBBLUE_COPPER_MEMORY-1);


	//printf ("Writing copper data index %d data %02XH\n",posicion,value);

	tbblue_copper_memory[posicion]=value;

	posicion++;
	tbblue_copper_set_write_position(posicion);

}

//Escribe dato copper en posicion de escritura, 16 bits
void tbblue_copper_write_data_16b(z80_byte value1, z80_byte value2)
{
	z80_int posicion=tbblue_copper_get_write_position();

	posicion &=(TBBLUE_COPPER_MEMORY-1);

	//After a write to an odd address, the entire 16-bits are written to Copper memory at once.
	if (posicion&1) {	
		tbblue_copper_memory[posicion-1]=value1;
		tbblue_copper_memory[posicion]=value2;
		//printf ("Writing copper 16b data index %d data %02X%02XH\n",posicion-1,value1,value2);
	}

	posicion++;
	tbblue_copper_set_write_position(posicion);

}

//Devuelve el byte donde apunta indice
z80_byte tbblue_copper_get_byte(z80_int posicion)
{
	posicion &=(TBBLUE_COPPER_MEMORY-1);
	return tbblue_copper_memory[posicion];
}

//Devuelve el valor de copper
z80_int tbblue_copper_get_pc(void)
{
	return tbblue_copper_pc & (TBBLUE_COPPER_MEMORY-1);
}

//Devuelve el byte donde apunta pc
z80_byte tbblue_copper_get_byte_pc(void)
{
	return tbblue_copper_get_byte(tbblue_copper_pc);

}

void tbblue_copper_get_wait_opcode_parameters(z80_int *line, z80_int *horiz)
{
	z80_byte byte_a=tbblue_copper_get_byte(tbblue_copper_pc);
	z80_byte byte_b=tbblue_copper_get_byte(tbblue_copper_pc+1);

	*line=byte_b|((byte_a&1)<<8);
	*horiz=((byte_a>>1)&63);
}

void tbblue_copper_reset_pc(void)
{
	tbblue_copper_pc=0;
}

void tbblue_copper_set_stop(void)
{
	tbblue_registers[98] &=63;
}

void tbblue_copper_next_opcode(void)
{
	//Incrementar en 2. 
	tbblue_copper_pc +=2;

  /*
		modos
		01 = Copper start, execute the list, then stop at last adress
       10 = Copper start, execute the list, then loop the list from start
       11 = Copper start, execute the list and restart the list at each frame
	*/

	//Si ha ido a posicion 0
	if (tbblue_copper_pc==TBBLUE_COPPER_MEMORY) {
		z80_byte copper_control_bits=tbblue_copper_get_control_bits();
			switch (copper_control_bits) {
						case TBBLUE_RCCH_COPPER_STOP:
							//Se supone que nunca se estara ejecutando cuando el mode sea stop
							tbblue_copper_set_stop();
						break;

						case TBBLUE_RCCH_COPPER_RUN_LOOP:
								//loop
								tbblue_copper_pc=0;
								//printf ("Reset copper on mode TBBLUE_RCCH_COPPER_RUN_LOOP\n");
						break;

						case TBBLUE_RCCH_COPPER_RUN_LOOP_RESET:
								//loop
								tbblue_copper_pc=0;
								//printf ("Reset copper on mode TBBLUE_RCCH_COPPER_RUN_LOOP_RESET\n");
						break;

						case TBBLUE_RCCH_COPPER_RUN_VBI:
								//loop??
								tbblue_copper_pc=0;
								//printf ("Reset copper on mode RUN_VBI\n");
						break;
			}
	}

}


//z80_bit tbblue_copper_ejecutando_halt={0};

//Ejecuta opcodes del copper // hasta que se encuentra un wait
void tbblue_copper_run_opcodes(void)
{

	z80_byte byte_leido=tbblue_copper_get_byte_pc();
	z80_byte byte_leido2=tbblue_copper_get_byte(tbblue_copper_pc+1);

	//Asumimos que no
	//tbblue_copper_ejecutando_halt.v=0;

	//if (tbblue_copper_get_pc()==0x24) printf ("%02XH %02XH\n",byte_leido,byte_leido2);

    //Special case of "value 0 to port 0" works as "no operation" (duration 1 CLOCK)
	/*
	Dado que el registro 0 es de solo lectura, no pasa nada si escribe en el: al leerlo se obtiene un valor calculado y no el del array
    if (byte_leido==0 && byte_leido2==0) {
      //printf("NOOP at %04XH\n",tbblue_copper_pc);
	  tbblue_copper_next_opcode();
      return;
    }
	*/

    //Special case of "WAIT 63,511" works as "halt" instruction
	/*
    if (byte_leido==255 && byte_leido2==255) {
	  //printf("HALT at %04XH\n",tbblue_copper_pc);
	  tbblue_copper_ejecutando_halt.v=1;

      return;
    }
	*/

		if ( (byte_leido&128)==0) {
			//Es un move
			z80_byte indice_registro=byte_leido&127;
			//tbblue_copper_pc++;
			
			//tbblue_copper_pc++;
			//printf ("Executing MOVE register %02XH value %02XH\n",indice_registro,valor_registro);
			tbblue_set_value_port_position(indice_registro,byte_leido2);

			tbblue_copper_next_opcode();

		}
		else {
			//Es un wait
			//Si se cumple, saltar siguiente posicion
			//z80_int linea, horiz;
			//tbblue_copper_get_wait_opcode_parameters(&linea,&horiz);
			if (tbblue_copper_wait_cond_fired () ) {
                                                        //printf ("Wait condition positive at copper_pc %02XH scanline %d raster %d\n",tbblue_copper_pc,t_scanline,tbblue_get_current_raster_position() );
                                                        tbblue_copper_next_opcode();
                                                        //printf ("Wait condition positive, after incrementing copper_pc %02XH\n",tbblue_copper_pc);
			}
			//printf ("Waiting until scanline %d horiz %d\n",linea,horiz);
			
		}
	
}

z80_byte tbblue_copper_get_control_bits(void)
{
	//z80_byte control=(tbblue_registers[98]>>6)&3;
	z80_byte control=(tbblue_registers[98])&(128+64);
	/*

# define(`__RCCH_COPPER_STOP', 0x00)
# define(`__RCCH_COPPER_RUN_LOOP_RESET', 0x40)
# define(`__RCCH_COPPER_RUN_LOOP', 0x80)
# define(`__RCCH_COPPER_RUN_VBI', 0xc0)

	*/
	return control;
}



/*int tbblue_copper_is_opcode_wait(void)
{
	z80_byte byte_leido=tbblue_copper_get_byte_pc();
	if ( (byte_leido&128) ) return 1;
	return 0;
}*/

//Si scanline y posicion actual corresponde con instruccion wait
int tbblue_copper_wait_cond_fired(void)
{
	//int scanline_actual=t_scanline;


	int current_horizontal=tbblue_get_current_raster_horiz_position();

	//Obtener parametros de instruccion wait
	z80_int linea, horiz;
	tbblue_copper_get_wait_opcode_parameters(&linea,&horiz);

	int current_raster=tbblue_get_current_raster_position();

	//511, 63
	//if (tbblue_copper_get_pc()==0x24) 
	// printf ("Waiting until raster %d horiz %d. current %d on copper_pc=%04X\n",linea,horiz,current_raster,tbblue_copper_get_pc() );

	//comparar vertical
	if (current_raster==linea) {
		//comparar horizontal
		//printf ("Comparing current %d to %d\n",current_horizontal,horiz);
		if (current_horizontal>=horiz) {
			//printf ("Fired wait condition %d,%d at %d,%d (t-states %d)\n",linea,horiz,current_raster,current_horizontal,
			//		t_estados % screen_testados_linea);
			return 1;
		}
	}

	return 0;
}



void tbblue_copper_handle_next_opcode(void)
{

	//Si esta activo copper
    z80_byte copper_control_bits=tbblue_copper_get_control_bits();
    if (copper_control_bits != TBBLUE_RCCH_COPPER_STOP) {
        //printf ("running copper %d\n",tbblue_copper_pc);
        tbblue_copper_run_opcodes();
	}
}                                           


/*
void tbblue_if_copper_halt(void)
{
	//Si esta activo copper
    z80_byte copper_control_bits=tbblue_copper_get_control_bits();
    if (copper_control_bits != TBBLUE_RCCH_COPPER_STOP) {
        //printf ("running copper %d\n",tbblue_copper_pc);
		if (tbblue_copper_ejecutando_halt.v) {
			//liberar el halt
			//printf ("copper was on halt (copper_pc=%04XH). Go to next opcode\n",tbblue_copper_get_pc() );
			tbblue_copper_next_opcode();
			//printf ("copper was on halt (copper_pc after=%04XH)\n",tbblue_copper_get_pc() );
		}
	}	
}
*/					
 

void tbblue_copper_handle_vsync(void)
{
	z80_byte copper_control_bits=tbblue_copper_get_control_bits();
    if (copper_control_bits==TBBLUE_RCCH_COPPER_RUN_VBI) {
    	tbblue_copper_reset_pc();
        //printf ("Reset copper on control bit 3 on vsync\n");
    }
                                                   
}


void tbblue_copper_write_control_hi_byte(z80_byte value)
{
	/*

# define(`__RCCH_COPPER_STOP', 0x00)
# define(`__RCCH_COPPER_RUN_LOOP_RESET', 0x40)
# define(`__RCCH_COPPER_RUN_LOOP', 0x80)
# define(`__RCCH_COPPER_RUN_VBI', 0xc0)

# STOP causes the copper to stop executing instructions
# and hold the instruction pointer at its current position.
#
# RUN_LOOP_RESET causes the copper to reset its instruction
# pointer to 0 and run in LOOP mode (see next).
#
# RUN_LOOP causes the copper to restart with the instruction
# pointer at its current position.  Once the end of the instruction
# list is reached, the copper loops back to the beginning.
#
# RUN_VBI causes the copper to reset its instruction
# pointer to 0 and run in VBI mode.  On vsync interrupt,
# the copper restarts the instruction list from the beginning.

# Note that modes RUN_LOOP_RESET and RUN_VBI will only reset
# the instruction pointer to zero if the mode actually changes
# to RUN_LOOP_RESET or RUN_VBI.  Writing the same mode in a
# second write will not cause the instruction pointer to zero.

# It is possible to write values into the copper's instruction
# space while it is running and since the copper constantly
# refetches a wait instruction it is executing, you can cause
# the wait instruction to end prematurely by changing it to
# something else.

*/

	z80_byte action=value&(128+64);

	switch (action) {
		//Estos dos casos, resetean el puntero de instruccion
		case TBBLUE_RCCH_COPPER_RUN_LOOP_RESET:
			//printf ("Reset copper PC when writing TBBLUE_RCCH_COPPER_RUN_LOOP_RESET to control hi byte\n");
			tbblue_copper_reset_pc();
		break;

		case TBBLUE_RCCH_COPPER_RUN_VBI:
			//printf ("Reset copper PC when writing TBBLUE_RCCH_COPPER_RUN_VBI to control hi byte\n");
			tbblue_copper_reset_pc();
		break;

	}

}

