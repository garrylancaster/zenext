/*
    ZEsarUX  ZX Second-Emulator And Released for UniX 
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TIMEX_H
#define TIMEX_H

#include "cpu.h"

extern z80_byte timex_port_ff;
extern z80_bit timex_video_emulation;


extern int get_timex_ink_mode6_color(void);
extern int get_timex_border_mode6_color(void);

extern int timex_si_modo_512(void);

extern z80_bit timex_mode_512192_real;

extern int timex_si_modo_512_y_zoom_par(void);

extern int timex_si_modo_shadow(void);
extern int timex_si_modo_8x1(void);

extern int get_timex_paper_mode6_color(void);

extern int timex_ugly_hack_enabled;
extern int timex_ugly_hack_last_hires;

extern void set_timex_port_ff(z80_byte value);


#endif
