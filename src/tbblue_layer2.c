/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"


/* Informacion relacionada con Layer2. Puede cambiar en el futuro, hay que ir revisando info en web de Next

Registros internos implicados:

(R/W) 0x12 (18) => Layer 2 RAM page
 bits 7-6 = Reserved, must be 0
 bits 5-0 = SRAM page (point to page 8 after a Reset)

(R/W) 0x13 (19) => Layer 2 RAM shadow page
 bits 7-6 = Reserved, must be 0
 bits 5-0 = SRAM page (point to page 11 after a Reset)

(R/W) 0x14 (20) => Global transparency color
  bits 7-0 = Transparency color value (Reset to 0xE3, after a reset)
  (Note this value is 8-bit only, so the transparency is compared only by the MSB bits of the final colour)



(R/W) 0x16 (22) => Layer2 Offset X
  bits 7-0 = X Offset (0-255)(Reset to 0 after a reset)

(R/W) 0x17 (23) => Layer2 Offset Y
  bits 7-0 = Y Offset (0-191)(Reset to 0 after a reset)




Posiblemente registro 20 aplica a cuando el layer2 esta por detras de pantalla de spectrum, y dice el color de pantalla de spectrum
que actua como transparente
Cuando layer2 esta encima de pantalla spectrum, el color transparente parece que es el mismo que sprites: TBBLUE_TRANSPARENT_COLOR 0xE3

Formato layer2: 256x192, linear, 8bpp, RRRGGGBB (mismos colores que sprites), ocupa 48kb

Se accede en modo escritura en 0000-3fffh mediante puerto:

Banking in Layer2 is out 4667 ($123B)
bit 0 = write enable, which changes writes from 0-3fff to write to layer2,
bit 1 = Layer2 ON or OFF set=ON,
bit 2 = ????
bit 3 = Use register 19 instead of 18 to tell sram page
bit 4 puts layer 2 behind the normal spectrum screen
bit 6 and 7 are to say which 16K section is paged in,
$03 = 00000011b Layer2 on and writable and top third paged in at $0000,
$43 = 01000011b Layer2 on and writable and middle third paged in at $0000,
$C3 = 11000011b Layer2 on and writable and bottom third paged in at $0000,  ?? sera 100000011b??? TODO
$02 = 00000010b Layer2 on and nothing paged in. etc

Parece que se mapea la pagina de sram indicada en registro 19

*/


/*

IMPORTANT!!

Trying some old layer2 demos that doesn't set register 19 is dangerous.
To avoid problems, first do:
out 9275, 19
out 9531,32
To set layer2 to the extra ram:
0x080000 – 0x0FFFFF (512K) => Extra RAM

Then load the demo program and will work

*/

z80_byte tbblue_port_123b;
z80_byte tbblue_port_123b_layer2_offset;

//valor inicial para tbblue_port_123b en caso de fast boot mode
int tbblue_initial_123b_port=-1;

//Diferentes layers a componer la imagen final
/*
(R/W) 0x15 (21) => Sprite and Layers system
  bit 7 - LoRes mode, 128 x 96 x 256 colours (1 = enabled)
  bits 6-5 = Reserved, must be 0
  bits 4-2 = set layers priorities:
     Reset default is 000, sprites over the Layer 2, over the ULA graphics
     000 - S L U
     001 - L S U
     010 - S U L
     011 - L U S
     100 - U S L
     101 - U L S
 */


int tbblue_write_on_layer2(void)
{
	if (tbblue_port_123b &1) return 1;
	return 0;
}

int tbblue_is_active_layer2(void)
{
	if (tbblue_port_123b & 2) return 1;
	return 0;
}

int tbblue_get_offset_start_layer2_reg(z80_byte register_value)
{
	//since core3.0 the NextRegs 0x12 and 0x13 are 7bit.
	int offset=register_value&127;
	//due to 7bit the value can leak outside of 2MiB
	// in HW the reads outside of SRAM module are "unspecified result", writes are ignored (!)	

	offset*=16384;

	//Y empezar en 0x040000 – 0x05FFFF (128K) => ZX Spectrum RAM
	/*
	recordemos
	    0x040000 – 0x05FFFF (128K) => ZX Spectrum RAM			(16 paginas) 
    0x060000 – 0x07FFFF (128K) => Extra RAM				(16 paginas)

    0x080000 – 0x0FFFFF (512K) => 1st Extra IC RAM (if present)		(64 paginas)
    0x100000 – 0x17FFFF (512K) => 2nd Extra IC RAM (if present)		(64 paginas)
    0x180000 – 0xFFFFFF (512K) => 3rd Extra IC RAM (if present)		(64 paginas)

    0x200000 (2 MB)

    	Con and 63, maximo layer 2 son 64 paginas
    	64*16384=1048576  -> 1 mb total
    	empezando en 0x040000 + 1048576 = 0x140000 y no nos salimos de rango (estariamos en el 2nd Extra IC RAM)
    	Dado que siempre asigno 2 mb para tbblue, no hay problema

    	*/



	offset +=0x040000;

	return offset;

}

int tbblue_get_offset_start_layer2(void)
{
	if (tbblue_port_123b & 8 ) return tbblue_get_offset_start_layer2_reg(tbblue_registers[19]);
	else return tbblue_get_offset_start_layer2_reg(tbblue_registers[18]);

}


z80_byte tbblue_get_port_layer2_value(void)
{
	return tbblue_port_123b;
}

void tbblue_out_port_layer2_value(z80_byte value)
{
        int update_mmus = 0;

        if (value & 0x10)
        {
                // Writing bit 4=1 changes the 16K bank offset.
                tbblue_port_123b_layer2_offset = value & 0x7;
                update_mmus = tbblue_port_123b & 0x05;
        }
        else
        {
                update_mmus = (tbblue_port_123b ^ value) & 0xcd;
	        tbblue_port_123b = value;
        }

        // Update NextReg 0x69 (Display Control 1).
        // Bit 7 of the nextreg mirrors bit 1 of the port value.
        tbblue_registers[0x69] &= 0x7f;
        if (value & 0x02)
        {
                tbblue_registers[0x69] |= 0x80;
        }

        if (update_mmus)
        {
                tbblue_touch_mmu012345();
        }
}
