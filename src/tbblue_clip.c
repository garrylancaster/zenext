/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"


// shifts and masks how the clip-window index is stored in tbblue_registers[28]
#define TBBLUE_CLIP_WINDOW_LAYER2_INDEX_SHIFT   0
#define TBBLUE_CLIP_WINDOW_LAYER2_INDEX_MASK    (3<<TBBLUE_CLIP_WINDOW_LAYER2_INDEX_SHIFT)

#define TBBLUE_CLIP_WINDOW_SPRITES_INDEX_SHIFT  2
#define TBBLUE_CLIP_WINDOW_SPRITES_INDEX_MASK   (3<<TBBLUE_CLIP_WINDOW_SPRITES_INDEX_SHIFT)

#define TBBLUE_CLIP_WINDOW_ULA_INDEX_SHIFT      4
#define TBBLUE_CLIP_WINDOW_ULA_INDEX_MASK       (3<<TBBLUE_CLIP_WINDOW_ULA_INDEX_SHIFT)

#define TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_SHIFT  6
#define TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_MASK   (3<<TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_SHIFT)


/* 
Clip window registers

(R/W) 0x18 (24) => Clip Window Layer 2
  bits 7-0 = Coords of the clip window
  1st write - X1 position
  2nd write - X2 position
  3rd write - Y1 position
  4rd write - Y2 position
  Reads do not advance the clip position
  The values are 0,255,0,191 after a Reset

(R/W) 0x19 (25) => Clip Window Sprites
  bits 7-0 = Cood. of the clip window
  1st write - X1 position
  2nd write - X2 position
  3rd write - Y1 position
  4rd write - Y2 position
  The values are 0,255,0,191 after a Reset
  Reads do not advance the clip position
  When the clip window is enabled for sprites in "over border" mode,
  the X coords are internally doubled and the clip window origin is
  moved to the sprite origin inside the border.

(R/W) 0x1A (26) => Clip Window ULA/LoRes
  bits 7-0 = Coord. of the clip window
  1st write = X1 position
  2nd write = X2 position
  3rd write = Y1 position
  4rd write = Y2 position
  The values are 0,255,0,191 after a Reset
  Reads do not advance the clip position

(R/W) 0x1B (27) => Clip Window Tilemap
  bits 7-0 = Coord. of the clip window
  1st write = X1 position
  2nd write = X2 position
  3rd write = Y1 position
  4rd write = Y2 position
  The values are 0,159,0,255 after a Reset, Reads do not advance the clip position, The X coords are internally doubled (in 40x32 mode, quadrupled in 80x32)

0x1C (28) => Clip Window control
(R) (may change)
  bits 7:6 = Tilemap clip index
  bits 5:4 = ULA/Lores clip index
  bits 3:2 = Sprite clip index
  bits 1:0 = Layer 2 clip index
(W) (may change)
  bits 7:4 = Reserved, must be 0
  bit 3 = Reset the tilemap clip index
  bit 2 = Reset the ULA/LoRes clip index
  bit 1 = Reset the sprite clip index
  bit 0 = Reset the Layer 2 clip index
*/

z80_byte clip_windows[4][4];                    // memory array to store actual clip windows

void tbblue_inc_clip_window_index(const z80_byte index_mask) {
    const z80_byte inc_one = (index_mask<<1) ^ index_mask;   // extract bottom bit of mask (+garbage in upper bits)
    const z80_byte inc_index = (tbblue_registers[28] + inc_one) & index_mask;
    tbblue_registers[28] &= ~index_mask;        // clear old index value
    tbblue_registers[28] |= inc_index;          // set new index value
}

z80_byte tbblue_get_clip_window_layer2_index(void) {
    return (tbblue_registers[28] & TBBLUE_CLIP_WINDOW_LAYER2_INDEX_MASK)>>TBBLUE_CLIP_WINDOW_LAYER2_INDEX_SHIFT;
}

z80_byte tbblue_get_clip_window_sprites_index(void) {
    return (tbblue_registers[28] & TBBLUE_CLIP_WINDOW_SPRITES_INDEX_MASK)>>TBBLUE_CLIP_WINDOW_SPRITES_INDEX_SHIFT;
}

z80_byte tbblue_get_clip_window_ula_index(void) {
    return (tbblue_registers[28] & TBBLUE_CLIP_WINDOW_ULA_INDEX_MASK)>>TBBLUE_CLIP_WINDOW_ULA_INDEX_SHIFT;
}

z80_byte tbblue_get_clip_window_tilemap_index(void) {
    return (tbblue_registers[28] & TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_MASK)>>TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_SHIFT;
}

void tbblue_inc_clip_window_layer2_index(void) {
    tbblue_inc_clip_window_index(TBBLUE_CLIP_WINDOW_LAYER2_INDEX_MASK);
}

void tbblue_reset_clip_window_layer2_index(void) {
    tbblue_registers[28] &= ~TBBLUE_CLIP_WINDOW_LAYER2_INDEX_MASK;
}

void tbblue_inc_clip_window_sprites_index(void) {
    tbblue_inc_clip_window_index(TBBLUE_CLIP_WINDOW_SPRITES_INDEX_MASK);
}

void tbblue_reset_clip_window_sprites_index(void) {
    tbblue_registers[28] &= ~(TBBLUE_CLIP_WINDOW_SPRITES_INDEX_MASK);
}

void tbblue_inc_clip_window_ula_index(void) {
    tbblue_inc_clip_window_index(TBBLUE_CLIP_WINDOW_ULA_INDEX_MASK);
}

void tbblue_reset_clip_window_ula_index(void) {
    tbblue_registers[28] &= ~(TBBLUE_CLIP_WINDOW_ULA_INDEX_MASK);
}

void tbblue_inc_clip_window_tilemap_index(void) {
    tbblue_inc_clip_window_index(TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_MASK);
}

void tbblue_reset_clip_window_tilemap_index(void) {
    tbblue_registers[28] &= ~(TBBLUE_CLIP_WINDOW_TILEMAP_INDEX_MASK);
}
