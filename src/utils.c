/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#define _GNU_SOURCE
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>

#if defined(__APPLE__)
        #include <sys/syslimits.h>
#endif


#ifdef MINGW
//Para usar GetLogicalDrives
#include <winbase.h>
#endif

#include "compileoptions.h"

#include "audio.h"
#include "autoselectoptions.h"
#include "ay38912.h"
#include "debug.h"
#include "diviface.h"
#include "divmmc.h"
#include "joystick.h"
#include "mem128.h"
#include "menu_system.h"
#include "menu_items.h"
#include "mmc.h"
#include "multiface.h"
#include "network.h"
#include "operaciones.h"
#include "realjoystick.h"
#include "screen.h"
#include "settings.h"
#include "tbblue.h"
#include "timex.h"
#include "ula.h"
#include "ulaplus.h"
#include "utils.h"

//Archivo usado para entrada de teclas
FILE *ptr_input_file_keyboard;
//Nombre archivo
char input_file_keyboard_name_buffer[PATH_MAX];
//Puntero que apunta al nombre
char *input_file_keyboard_name=NULL;
//Si esta insertado
z80_bit input_file_keyboard_inserted;
//Si esta en play (y no pausado)
z80_bit input_file_keyboard_playing;

//Pausa en valores de 1/50 segundos
int input_file_keyboard_delay=5;
//Contador actual
int input_file_keyboard_delay_counter=0;
//Pendiente de leer siguiente tecla
z80_bit input_file_keyboard_pending_next;
//Si lo siguiente es pausa o tecla
z80_bit input_file_keyboard_is_pause;
//Si hay que enviar pausa despues de cada pulsacion o no
z80_bit input_file_keyboard_send_pause;

//modo turbo de spool file
z80_bit input_file_keyboard_turbo={0};

//Si se guarda la configuracion al salir del programa
z80_bit save_configuration_file_on_exit={0};

//ultima tecla leida
unsigned char input_file_keyboard_last_key;

void parse_customfile_options(void);


char *customconfigfile=NULL;

int write_rom_nested_id_poke_byte;
int write_rom_nested_id_poke_byte_no_time;


//Utilidades externas
char external_tool_sox[PATH_MAX]="/usr/bin/sox";
//char external_tool_unzip[PATH_MAX]="/usr/bin/unzip";
char external_tool_gunzip[PATH_MAX]="/bin/gunzip";
char external_tool_tar[PATH_MAX]="/bin/tar";
char external_tool_unrar[PATH_MAX]="/usr/bin/unrar";


//tablas de conversion de teclado
struct x_tabla_teclado tabla_teclado_numeros[]={
//0
                                        {&puerto_61438, 1},
                                        {&puerto_63486, 1},
                                        {&puerto_63486, 2},
                                        {&puerto_63486, 4},
//4
                                        {&puerto_63486, 8},
                                        {&puerto_63486, 16},
                                        {&puerto_61438, 16},
                                        {&puerto_61438, 8},
//8
                                        {&puerto_61438, 4},
                                        {&puerto_61438, 2}
};

struct x_tabla_teclado tabla_teclado_letras[]={
        {&puerto_65022,1},   //A
        {&puerto_32766,16},
        {&puerto_65278,8},
        {&puerto_65022,4},   //D

                                        {&puerto_64510, 4},
                                        {&puerto_65022, 8},
                                        {&puerto_65022, 16},
                                        {&puerto_49150, 16},
                                        {&puerto_57342, 4},
                                        {&puerto_49150, 8},
                                        {&puerto_49150, 4},
                                        {&puerto_49150, 2},
                                        {&puerto_32766, 4},
//M
                                        {&puerto_32766, 8},
                                        {&puerto_57342, 2},
                                        {&puerto_57342, 1},
                                        {&puerto_64510, 1},
                                        {&puerto_64510, 8},
                                        {&puerto_65022, 2},
                                        {&puerto_64510, 16},
//U
                                        {&puerto_57342, 8},
                                        {&puerto_65278, 16},
                                        {&puerto_64510, 2},
                                        {&puerto_65278, 4},
                                        {&puerto_57342, 16},
                                        {&puerto_65278, 2}


};

//Para guardar geometria de ventanas
int total_config_window_geometry=0;
saved_config_window_geometry saved_config_window_geometry_array[MAX_CONFIG_WINDOW_GEOMETRY];

z80_bit debug_parse_config_file;


#ifdef EMULATE_CPU_STATS
//Arrays de contadores de numero de veces instrucciones usadas
unsigned int stats_codsinpr[256];
unsigned int stats_codpred[256];
unsigned int stats_codprcb[256];
unsigned int stats_codprdd[256];
unsigned int stats_codprfd[256];
unsigned int stats_codprddcb[256];
unsigned int stats_codprfdcb[256];
//codprddfd.c  codprddfd.o  codpred.c    codpred.o

void util_stats_increment_counter(unsigned int *stats_array,int index)
{
	stats_array[index]++;
}

unsigned int util_stats_get_counter(unsigned int *stats_array,int index)
{
	return stats_array[index];
}

void util_stats_set_counter(unsigned int *stats_array,int index,unsigned int value)
{
	stats_array[index]=value;
}


//Buscar dentro del indice el de mayor valor
int util_stats_find_max_counter(unsigned int *stats_array)
{
	int index_max=0;
	unsigned int value_max=0;

	int i;
	unsigned int value;

	for (i=0;i<256;i++) {
		value=util_stats_get_counter(stats_array,i);
		if (value>value_max) {
			value_max=value;
			index_max=i;
		}
	}

	return index_max;
}


void util_stats_init(void)
{

        debug_printf (VERBOSE_INFO,"Initializing CPU Statistics Counter Array");

        int i;

        for (i=0;i<256;i++) {
                util_stats_set_counter(stats_codsinpr,i,0);
                util_stats_set_counter(stats_codpred,i,0);
                util_stats_set_counter(stats_codprcb,i,0);
                util_stats_set_counter(stats_codprdd,i,0);
                util_stats_set_counter(stats_codprfd,i,0);
                util_stats_set_counter(stats_codprddcb,i,0);
                util_stats_set_counter(stats_codprfdcb,i,0);
        }
}

//Retornar la suma de todos los contadores
unsigned int util_stats_sum_all_counters(void)
{

	unsigned int total=0;
	int i;

        for (i=0;i<256;i++) {
                total +=util_stats_get_counter(stats_codsinpr,i);
                total +=util_stats_get_counter(stats_codpred,i);
                total +=util_stats_get_counter(stats_codprcb,i);
                total +=util_stats_get_counter(stats_codprdd,i);
                total +=util_stats_get_counter(stats_codprfd,i);
                total +=util_stats_get_counter(stats_codprddcb,i);
                total +=util_stats_get_counter(stats_codprfdcb,i);
        }


	return total;

}


#endif








//Obtiene la extension de filename y la guarda en extension. Extension sin el punto
//filename debe ser nombre de archivo, sin incluir directorio
void util_get_file_extension(char *filename,char *extension)
{

        //obtener extension del nombre
        //buscar ultimo punto
        //parar si se encuentra / o \ de division de carpeta

        char caracter_carpeta='/';
#ifdef MINGW
        caracter_carpeta='\\';
#endif


        int j;
        j=strlen(filename);
        if (j==0) extension[0]=0;
        else {
                for (;j>=0 && filename[j]!='.' && filename[j]!=caracter_carpeta;j--);

                if (filename[j]==caracter_carpeta) {
                        extension[0]=0; //no hay extension
                }

                else {
                        if (j>=0) strcpy(extension,&filename[j+1]);
                        else extension[0]=0;
                }
        }

        debug_printf (VERBOSE_DEBUG,"Filename: [%s] Extension: [%s]",filename,extension);
}

//Obtiene el nombre de filename sin extension y la guarda en filename_without_extension.
//filename debe ser nombre de archivo, sin incluir directorio
//TODO: busca hasta el primer punto. quiza deberia tener en cuenta el punto desde el final... o no..
void util_get_file_without_extension(char *filename,char *filename_without_extension)
{

	int i;

	for (i=0;filename[i]!=0 && filename[i]!='.';i++) {
		filename_without_extension[i]=filename[i];
	}

	filename_without_extension[i]=0;

        //printf ("Filename: %s without Extension: %s\n",filename,filename_without_extension);
}



//Obtiene el nombre y extension (quitamos el directorio)
void util_get_file_no_directory(char *filename,char *file_no_dir)
{
	//buscamos / o barra invertida windows

        int j;
        j=strlen(filename);
        if (j==0) file_no_dir[0]=0;
	else {
		for (;j>=0 && filename[j]!='/' && filename[j]!='\\' ;j--);

		strcpy(file_no_dir,&filename[j+1]);
	}
}

//Compara la extension indicada con la del archivo, sin distinguir mayusculas
//Devuelve valor de strcasecmp -> 0 igual, otros->diferente
//Extension sin el punto
int util_compare_file_extension(char *filename,char *extension_compare)
{
	char extension[NAME_MAX];

	util_get_file_extension(filename,extension);

	return strcasecmp(extension,extension_compare);
}



//Retorna el directorio de una ruta completa, ignorando barras repetidas del final
void util_get_dir(char *ruta,char *directorio)
{
        int i;

        if (ruta==NULL) {
                debug_printf (VERBOSE_DEBUG,"ruta NULL");
                directorio[0]=0;
                return;
        }

	//buscar barra final / (o \ de windows)
        for (i=strlen(ruta)-1;i>=0;i--) {
                if (ruta[i]=='/' || ruta[i]=='\\') break;
        }

        //Ubicarse en el primer caracter no /
        for (;i>=0;i--) {
                if (ruta[i]!='/' && ruta[i]!='\\') break;
        }

        if (i>=0) {

                //TODO. porque esto no va con strcpy??? Da segmentation fault en mac, en cintas de revenge.p
                int j;
                for (j=0;j<=i;j++) {
                        directorio[j]=ruta[j];
                }
                //sleep(1);
                //printf ("despues de strncpy\n");
                directorio[i+1]='/';
                directorio[i+2]=0;
                //printf ("directorio final: %s\n",directorio);
        }

        else {
                //printf ("directorio vacio\n");
                //directorio vacio
                directorio[0]=0;
        }

}

//Compone path completo a un archivo teniendo el directorio y nombre
//tiene en cuenta si directorio es vacio
//lo mete en fullpath
void util_get_complete_path(char *dir,char *name,char *fullpath)
{
	if (dir[0]==0) {
		//directorio nulo
		sprintf (fullpath,"%s",name);
	}
	else {
		sprintf (fullpath,"%s/%s",dir,name);
	}
}

void util_press_menu_symshift(int pressrelease)
{
        if (pressrelease) menu_symshift.v=1;
        else menu_symshift.v=0;
}


//En spectrum, desactiva symbol shift. En zx80/81, desactiva mayusculas
void clear_symshift(void)
{
        puerto_32766 |=2;

        //Desactivamos tecla symshift para menu
        menu_symshift.v=0;

}


//En spectrum, activa symbol shift. En zx80/81, activa mayusculas
void set_symshift(void)
{
//puerto_65278   db    255  ; V    C    X    Z    Sh    ;0
//puerto_32766    db              255  ; B    N    M    Simb Space ;7

        puerto_32766 &=255-2;

        //Activamos tecla symshift para menu
        menu_symshift.v=1;

}



//Abre un archivo en solo lectura buscandolo en las rutas:
//1) ruta actual
//2) ../Resources/
//3) INSTALLPREFIX/
//normalmente usado para cargar roms
//modifica puntero FILE adecuadamente
void old_open_sharedfile(char *archivo,FILE **f)
{
        char buffer_nombre[1024];
	strcpy(buffer_nombre,archivo);

        //ruta actual
        debug_printf(VERBOSE_INFO,"Looking for file %s at current dir",buffer_nombre);
        *f=fopen(buffer_nombre,"rb");

        //sino, en ../Resources
        if (!(*f)) {
                sprintf(buffer_nombre,"../Resources/%s",archivo);
                debug_printf(VERBOSE_INFO,"Looking for file %s",buffer_nombre);
                *f=fopen(buffer_nombre,"rb");

                //sino, en INSTALLPREFIX/share/zesarux
                if (!(*f)) {
                        sprintf(buffer_nombre,"%s/%s/%s",INSTALL_PREFIX,"/share/zesarux/",archivo);
                        debug_printf(VERBOSE_INFO,"Looking for file %s",buffer_nombre);
                        *f=fopen(buffer_nombre,"rb");
                }
        }


}


//Busca un archivo buscandolo en las rutas:
//1) ruta actual
//2) ../Resources/
//3) INSTALLPREFIX/
//normalmente usado para cargar roms
//Retorna ruta archivo en ruta_final (siempre que no sea NULL), valor retorno no 0. Si no existe, valor retorno 0
int find_sharedfile(char *archivo,char *ruta_final)
{
        char buffer_nombre[PATH_MAX];
	strcpy(buffer_nombre,archivo);

	int existe;

        //ruta actual
        debug_printf(VERBOSE_INFO,"Looking for file %s at current dir",buffer_nombre);
        existe=si_existe_archivo(buffer_nombre);

        //sino, en ../Resources
        if (!existe) {
                sprintf(buffer_nombre,"../Resources/%s",archivo);
                debug_printf(VERBOSE_INFO,"Looking for file %s",buffer_nombre);
                existe=si_existe_archivo(buffer_nombre);

                //sino, en INSTALLPREFIX/share/zesarux
                if (!existe) {
                        sprintf(buffer_nombre,"%s/%s/%s",INSTALL_PREFIX,"/share/zesarux/",archivo);
                        debug_printf(VERBOSE_INFO,"Looking for file %s",buffer_nombre);
                        existe=si_existe_archivo(buffer_nombre);
                }
        }

	if (existe) {
		if (ruta_final!=NULL) strcpy(ruta_final,buffer_nombre);
		debug_printf(VERBOSE_INFO,"Found on path %s",buffer_nombre);
	}

	return existe;
}


//Abre un archivo en solo lectura buscandolo en las rutas:
//1) ruta actual
//2) ../Resources/
//3) INSTALLPREFIX/
//normalmente usado para cargar roms
//modifica puntero FILE adecuadamente
void open_sharedfile(char *archivo,FILE **f)
{
        char buffer_nombre[PATH_MAX];

	int existe=find_sharedfile(archivo,buffer_nombre);
	if (existe) {
		*f=fopen(buffer_nombre,"rb");
	}

	else *f=NULL;

}


//Devuelve puntero a archivo si ha podido encontrar el archivo y abrirlo en escritura.
//NULL
void open_sharedfile_write_open(char *archivo,FILE **f)
{
	//Asumimos NULL
	*f=NULL;

	if (!si_existe_archivo(archivo)) return;

	*f=fopen(archivo,"wb");

}

//Abre un archivo en escritura buscandolo en las rutas:
//1) ruta actual
//2) ../Resources/
//3) INSTALLPREFIX/
//normalmente usado para cargar roms
//modifica puntero FILE adecuadamente
//Primero mira que el archivo exista y luego intenta abrirlo para escritura
void open_sharedfile_write(char *archivo,FILE **f)
{
        char buffer_nombre[1024];

        //ruta actual
        debug_printf(VERBOSE_INFO,"Looking for file %s at current dir",archivo);
        open_sharedfile_write_open(archivo,f);

        //sino, en ../Resources
        if (!(*f)) {
                sprintf(buffer_nombre,"../Resources/%s",archivo);
                debug_printf(VERBOSE_INFO,"Looking for file %s",buffer_nombre);
                open_sharedfile_write_open(buffer_nombre,f);

                //sino, en INSTALLPREFIX/share/zesarux
                if (!(*f)) {
                        sprintf(buffer_nombre,"%s/%s/%s",INSTALL_PREFIX,"/share/zesarux/",archivo);
                        debug_printf(VERBOSE_INFO,"Looking for file %s",buffer_nombre);
                        open_sharedfile_write_open(buffer_nombre,f);
                }
        }

	//printf ("file: %d\n",*f);

}

void get_compile_info(char *s)
{
        sprintf (s,"%s",
        "Compilation date: " COMPILATION_DATE "\n"
	"\n"
	"Compilation system: " COMPILATION_SYSTEM "\n"
	"\n"
        "Configure command: " CONFIGURE_OPTIONS "\n"
	"\n"
        "Compile variables: " COMPILE_VARIABLES "\n"
	"\n"
        "Compile INITIALCFLAGS: " COMPILE_INITIALCFLAGS "\n"
        "Compile INITIALLDFLAGS: " COMPILE_INITIALLDFLAGS "\n"
        "Compile FINALCFLAGS: " COMPILE_FINALCFLAGS "\n"
        "Compile FINALLDFLAGS: " COMPILE_FINALLDFLAGS "\n"
	"\n"
        "Install PREFIX: " INSTALL_PREFIX "\n"
        );

	if (strlen(s)>=MAX_COMPILE_INFO_LENGTH) cpu_panic("Error. MAX_COMPILE_INFO_LENGTH reached");
}

void show_compile_info(void)
{
        char buffer[MAX_COMPILE_INFO_LENGTH];
        get_compile_info(buffer);
        printf ("%s",buffer);
}


//Segun tecla de entrada, genera puerto de teclado, lo pone o lo borra
void ascii_to_keyboard_port_set_clear(unsigned tecla,int pressrelease)
{

	//printf ("ascii_to_keyboard_port_set_clear: tecla: %d pressrelease: %d\n",tecla,pressrelease);

                                if (tecla>='A' && tecla<='Z') {
                                        	//mayus. para Spectrum
						if (pressrelease) {
	                                        	puerto_65278 &=255-1;
						}
						else {
							puerto_65278 |=1;
						}

       	                                tecla=tecla+('a'-'A');
                                }


        //printf ("Tecla buena: %d  \n",c);
	switch(tecla) {

                                case 27:
                                        //printf ("Alt\n");
                                break;

				case 32:
                                	if (pressrelease) {
						puerto_32766 &=255-1;
					}
	                                else {
						puerto_32766 |=1;
					}

        	                break;


	                        case 13:
	                        case 10:
        	                        if (pressrelease) {
						puerto_49150 &=255-1;
					}

                	                else {
						puerto_49150 |=1;
					}


                        	break;

                                default:
					convert_numeros_letras_puerto_teclado(tecla,pressrelease);
                                break;
	}


	//simbolos para ZX Spectrum
	{
		switch (tecla) {
                                case '!':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_63486 &=255-1;
					}
					else {
						clear_symshift();
						puerto_63486 |=1;
					}
                                break;

                                //Enviar caps shift con 128
                                case 128:
                                        if (pressrelease) {
						//mayus
						puerto_65278 &=255-1;
                                        }
                                        else {
						//mayus
						puerto_65278 |=1;
                                        }
				break;

                                //Enviar symbol shift con 129
                                case 129:
                                        if (pressrelease) {
						set_symshift();
                                        }
                                        else {
						clear_symshift();
                                        }
				break;

				//Enviar Shift + 1 (edit).
				/*
				En curses va bien
				Es stdout habria que hacer el | , luego el texto y enter, todo a la vez, en la misma linea,
				porque si se hace | y enter, se edita la linea y al dar enter se entra la linea sin modificar
				*/
				case '|':
                                        if (pressrelease) {
						//mayus
						puerto_65278 &=255-1;
                                                puerto_63486 &=255-1;
                                        }
                                        else {
						//mayus
						puerto_65278 |=1;
                                                puerto_63486 |=1;
                                        }
				break;


                                //Symbol + 2
                                case '@':
                                        if (pressrelease) {
						set_symshift();
	                                        puerto_63486 &=255-2;
					}
					else {
						clear_symshift();
	                                        puerto_63486 |=2;
					}
                                break;

                                 //Symbol + 3
                                case '#':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_63486 &=255-4;
					}
					else {
	                                        clear_symshift();
        	                                puerto_63486 |=4;
					}

                                break;

                                case '$':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_63486 &=255-8;
					}
					else {
	                                        clear_symshift();
        	                                puerto_63486 |=8;
					}
                                break;


                                case '%':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_63486 &=255-16;
					}
					else {
	                                        clear_symshift();
        	                                puerto_63486 |=16;
					}
                                break;

                                case '&':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_61438 &=255-16;
					}
					else {
	                                        clear_symshift();
        	                                puerto_61438 |=16;
					}
                                break;

                                //Comilla simple
                                case '\'':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_61438 &=255-8;
					}
					else {
	                                        clear_symshift();
        	                                puerto_61438 |=8;
					}
                                break;

                                case '(':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_61438 &=255-4;
					}
					else {
	                                        clear_symshift();
        	                                puerto_61438 |=4;
					}
                                break;

                                case ')':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_61438 &=255-2;
					}
					else {
	                                        clear_symshift();
        	                                puerto_61438 |=2;
					}
                                break;

                                case '_':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_61438 &=255-1;
					}
					else {
	                                        clear_symshift();
        	                                puerto_61438 |=1;
					}
                                break;

                                case '*':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_32766 &=255-16;
					}
					else {
	                                        clear_symshift();
        	                                puerto_32766 |=16;
					}
                                break;

                                case '?':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_65278 &=255-8;
					}
					else {
	                                        clear_symshift();
        	                                puerto_65278 |=8;
					}
                                break;

                                //Esto equivale al simbolo de "flecha" arriba - exponente
                                case '^':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_49150 &=255-16;
					}
					else {
	                                        clear_symshift();
        	                                puerto_49150 |=16;
					}
                                break;

                                case '-':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_49150 &=255-8;
					}
					else {
	                                        clear_symshift();
        	                                puerto_49150 |=8;
					}
                                break;


                                case '+':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_49150 &=255-4;
					}
					else {
	                                        clear_symshift();
        	                                puerto_49150 |=4;
					}
                                break;

                                case '=':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_49150 &=255-2;
					}
					else {
	                                        clear_symshift();
        	                                puerto_49150 |=2;
					}
                                break;

                                case ';':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_57342 &=255-2;
					}
					else {
	                                        clear_symshift();
        	                                puerto_57342 |=2;
					}
                                break;

                                //Comilla doble
                                case '"':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_57342 &=255-1;
					}
					else {
	                                        clear_symshift();
        	                                puerto_57342 |=1;
					}
                                break;

                                case '<':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_64510 &=255-8;
					}
					else {
	                                        clear_symshift();
        	                                puerto_64510 |=8;
					}
                                break;

                                case '>':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_64510 &=255-16;
					}
					else {
	                                        clear_symshift();
        	                                puerto_64510 |=16;
					}
                                break;

                                case '/':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_65278 &=255-16;
					}
					else {
	                                        clear_symshift();
        	                                puerto_65278 |=16;
					}
                                break;

                                case ':':
					if (pressrelease) {
	                                        set_symshift();
        	                                puerto_65278 &=255-2;
					}
					else {
	                                        clear_symshift();
        	                                puerto_65278 |=2;
					}
                                break;

                                case '.':
					if (pressrelease) {
						puerto_32766 &=255-2-4;
					}
					else {
						puerto_32766 |=2+4;
					}

                                break;

                                case ',':
					if (pressrelease) {
	                                        puerto_32766 &=255-2-8;
					}
					else {
	                                        puerto_32766 |=2+8;
					}
				break;


		}
	}
}

//Segun tecla de entrada, genera puerto de teclado, lo activa
void ascii_to_keyboard_port(unsigned tecla)
{
	ascii_to_keyboard_port_set_clear(tecla,1);
}

int input_file_keyboard_is_playing(void)
{
        if (input_file_keyboard_inserted.v && input_file_keyboard_playing.v) return 1;
        else return 0;
}

void insert_input_file_keyboard(void)
{
	input_file_keyboard_inserted.v=1;
        input_file_keyboard_playing.v=0;
}

void eject_input_file_keyboard(void)
{
        input_file_keyboard_inserted.v=0;
        input_file_keyboard_playing.v=0;

        //Si modo turbo, quitar
        if (input_file_keyboard_turbo.v) {
                reset_peek_byte_function_spoolturbo();
                input_file_keyboard_turbo.v=0;
        }
}

int input_file_keyboard_init(void)
{
	ptr_input_file_keyboard=fopen(input_file_keyboard_name,"rb");

	if (!ptr_input_file_keyboard) {
		debug_printf(VERBOSE_ERR,"Unable to open input file %s",input_file_keyboard_name);
		eject_input_file_keyboard();
		return 1;
	}

	insert_input_file_keyboard();

	return 0;

}

void input_file_keyboard_close(void)
{
	eject_input_file_keyboard();
	fclose(ptr_input_file_keyboard);
}


void reset_keyboard_ports(void)
{
        //inicializar todas las teclas a nada - 255
        puerto_65278=255;
        puerto_65022=255;
        puerto_64510=255;
        puerto_63486=255;
        puerto_61438=255;
        puerto_57342=255;
        puerto_49150=255;
        puerto_32766=255;
        puerto_especial1=255;
        puerto_especial2=255;
        puerto_especial3=255;
        puerto_especial4=255;

        puerto_especial_joystick=0;

        menu_symshift.v=0;
        menu_capshift.v=0;
        menu_backspace.v=0;
        menu_tab.v=0;
}


void input_file_keyboard_get_key(void)
{
                        if (input_file_keyboard_pending_next.v==1) {
                                input_file_keyboard_pending_next.v=0;
                                //leer siguiente tecla o enviar pausa (nada)
				if (input_file_keyboard_send_pause.v==1) {
					if (input_file_keyboard_is_pause.v==1) {
						reset_keyboard_ports();
						return;
					}
				}

                                int leidos=fread(&input_file_keyboard_last_key,1,1,ptr_input_file_keyboard);
				if (leidos==0) {
					debug_printf (VERBOSE_INFO,"Read 0 bytes of Input File Keyboard. End of file");
					eject_input_file_keyboard();
					reset_keyboard_ports();
					return;
				}

                        }


                        //Puertos a 0
                        reset_keyboard_ports();
                        //solo habilitar tecla indicada
			if (input_file_keyboard_send_pause.v==0) ascii_to_keyboard_port(input_file_keyboard_last_key);

			//Se envia pausa. Ver si ahora es pausa o tecla
			else {
				if (input_file_keyboard_is_pause.v==0) ascii_to_keyboard_port(input_file_keyboard_last_key);
			}


}


//Devuelve tamanyo minimo de ROM para el tipo de maquina indicado
int get_rom_size(int machine)
{
	if (machine==19) return 8192;
	else cpu_panic("Unknown machine on get_rom_size");

	return 0;
}



//1 si ok
//0 si error
int configfile_read_aux(char *configfile,char *mem)
{

        //Avisar si tamanyo grande
        if (get_file_size(configfile) > (long int)MAX_SIZE_CONFIG_FILE) cpu_panic("Configuration file is larger than maximum size allowed");

	FILE *ptr_configfile;
        ptr_configfile=fopen(configfile,"rb");


        if (!ptr_configfile) {
                printf("Unable to open configuration file %s\n",configfile);
                return 0;
        }

        int leidos=fread(mem,1,MAX_SIZE_CONFIG_FILE,ptr_configfile);

        //Poner un codigo 0 al final
        mem[leidos]=0;


        fclose(ptr_configfile);

        return 1;

}

//Devuelve 1 si ok
//0 si error
int util_get_configfile_name(char *configfile)
{
  if (customconfigfile!=NULL) {
        sprintf(configfile,"%s",customconfigfile);
        return 1;
  }

  #ifndef MINGW
  	char *directorio_home;
  	directorio_home=getenv("HOME");
  	if ( directorio_home==NULL) {
                  //printf("Unable to find $HOME environment variable to open configuration file\n");
  		return 0;
  	}

  	sprintf(configfile,"%s/%s",directorio_home,DEFAULT_ZESARUX_CONFIG_FILE);

  #else
  	char *homedrive;
  	char *homepath;
  	homedrive=getenv("HOMEDRIVE");
  	homepath=getenv("HOMEPATH");

  	if (homedrive==NULL || homepath==NULL) {
  		//printf("Unable to find HOMEDRIVE or HOMEPATH environment variables to open configuration file\n");
                  return 0;
          }

  	sprintf(configfile,"%s\\%s\\%s",homedrive,homepath,DEFAULT_ZESARUX_CONFIG_FILE);

  #endif

  return 1;

}


//Devuelve 1 si ok
//0 si error
int util_create_sample_configfile(int additional)
{

char configfile[PATH_MAX];
	FILE *ptr_configfile;

if (util_get_configfile_name(configfile)==0)  {
  printf("Unable to find $HOME, or HOMEDRIVE or HOMEPATH environment variables to open configuration file\n");
  return 0;
}

  ptr_configfile=fopen(configfile,"wb");
              if (!ptr_configfile) {
                      printf("Unable to create sample configuration file %s\n",configfile);
                      return 0;
              }

  char *sample_config=
    ";ZEsarUX configuration file\n"
    ";\n"
    ";Lines beginning with ; or # are ignored\n"
    ";You can specify here the same options passed on command line, for example:\n"
    ";--verbose 2\n"
    ";Options can be written in quotation marks, for example:\n"
    ";--joystickemulated \"OPQA Space\"\n"
    ";Options can be written on the same line or different lines, like:\n"
    ";--verbose 2 --machine 128k\n"
    ";Or:\n"
    ";--verbose 2\n"
    ";--machine 128k\n"
    ";Or even like this:\n"
    ";--verbose\n"
    ";2\n"
    ";--machine\n"
    ";128k\n"
    "\n"
    ";Run zesarux with --help or --experthelp to see all the options\n"
    "\n"
    ;

  fwrite(sample_config, 1, strlen(sample_config), ptr_configfile);

  if (additional)  {
	char *sample_config_additional="--saveconf-on-exit"
	    "\n"
	    "\n"
	;

	fwrite(sample_config_additional, 1, strlen(sample_config_additional), ptr_configfile);
   }


              fclose(ptr_configfile);

              return 1;

}

/*int util_write_config_add_string(char *config_settings,char *string_add)
{
    int longitud=strlen(string_add)+2; //Agregar texto y salto linea
    sprintf (config_settings,"%s \n",string_add);

    return longitud;
}
*/

#define MAX_CONFIG_SETTING (PATH_MAX*2)

int util_write_config_add_string (char *config_settings, const char * format , ...)
{

	//tamaño del buffer bastante mas grande que el valor constante definido
    char string_add[MAX_CONFIG_SETTING+100];


    va_list args;
    va_start (args, format);
    vsprintf (string_add,format, args);
    va_end (args);

    int longitud=strlen(string_add)+2; //Agregar texto y salto linea
    sprintf (config_settings,"%s \n",string_add);

    return longitud;

}


//Retorna un string con valor X,+X o -X segun button_type
void util_write_config_aux_realjoystick(int button_type, int button, char *texto)
{
  		if (button_type==0) {
  			sprintf(texto,"%d",button);
  		}
  		else if (button_type<0) {
  			sprintf(texto,"-%d",button);
  		}

  		else {
  			sprintf(texto,"+%d",button);
  		}
}

void util_copy_path_delete_last_slash(char *origen, char *destino)
{
    //Quitar barras del final si las hay
    strcpy(destino,origen);
    int i=strlen(destino);
    i--;
    while (i>0 &&
        (destino[i]=='/' || destino[i]=='\\')
        ) {
            destino[i]=0;
            i--;
    }

}

//void util_save_config_normalize_path


//1 si ok
//0 si error
int util_write_configfile(void)
{
  //Sobreescribimos archivos con settings actuales
  debug_printf (VERBOSE_INFO,"Writing configuration file");

  //Creamos archivo de configuracion de ejemplo y agregamos settings aparte

  if (util_create_sample_configfile(0)==0) {
    debug_printf(VERBOSE_ERR,"Cannot write configuration file");
    return 0;
  }

  //Agregamos contenido
  char config_settings[20000]; //Esto es mas que suficiente

  char buffer_temp[MAX_CONFIG_SETTING]; //Para cadenas temporales
  //char buffer_temp2[MAX_CONFIG_SETTING]; //Para cadenas temporales

  int indice_string=0;

  int i;



//Macro para no repetir tantas veces lo mismo
#define ADD_STRING_CONFIG indice_string +=util_write_config_add_string(&config_settings[indice_string]

  //sprintf (buffer_temp,"--zoom %d",zoom_x);
  if (save_configuration_file_on_exit.v)      ADD_STRING_CONFIG,"--saveconf-on-exit");
                                              ADD_STRING_CONFIG,"--zoom %d",zoom_x);
  if (frameskip)                              ADD_STRING_CONFIG,"--frameskip %d",frameskip);


//Solo la agrego si no es 8, que esto es experimental
  if (menu_char_width!=8)		      ADD_STRING_CONFIG,"--menucharwidth %d",menu_char_width);


  if (autoframeskip.v==0)                     ADD_STRING_CONFIG,"--disable-autoframeskip");
  if (no_cambio_parametros_maquinas_lentas.v) ADD_STRING_CONFIG,"--nochangeslowparameters");
  if (ventana_fullscreen)                     ADD_STRING_CONFIG,"--fullscreen");
  if (verbose_level)                          ADD_STRING_CONFIG,"--verbose %d",verbose_level);

  if (debug_unnamed_console_enabled.v==0)     ADD_STRING_CONFIG,"--disable-debug-console-win");

  if (debug_always_show_messages_in_console.v) ADD_STRING_CONFIG,"--verbose-always-console");
  if (windows_no_disable_console.v)           ADD_STRING_CONFIG,"--nodisableconsole");
  if (porcentaje_velocidad_emulador!=100)     ADD_STRING_CONFIG,"--cpuspeed %d",porcentaje_velocidad_emulador);

  if (cpu_random_r_register.v)                ADD_STRING_CONFIG,"--random-r-register");


  //no uso esto de momento if (tbblue_initial_123b_port>=0)            ADD_STRING_CONFIG,"--tbblue-123b-port %d",tbblue_initial_123b_port);

                                              ADD_STRING_CONFIG,"--ao %s",audio_new_driver_name);
                                              ADD_STRING_CONFIG,"--vo %s",scr_new_driver_name);
  if (ay_speech_enabled.v==0)                 ADD_STRING_CONFIG,"--disableayspeech");
  if (ay_envelopes_enabled.v==0)              ADD_STRING_CONFIG,"--disableenvelopes");
  if (audio_noreset_audiobuffer_full.v)       ADD_STRING_CONFIG,"--noreset-audiobuffer-full");

                                              ADD_STRING_CONFIG,"--pcspeaker-wait-time %d",audiopcspeaker_tiempo_espera);

  if (border_enabled.v==0)                    ADD_STRING_CONFIG,"--disableborder");
  if (mouse_pointer_shown.v==0)               ADD_STRING_CONFIG,"--hidemousepointer");
  if (mouse_menu_disabled.v)                  ADD_STRING_CONFIG,"--disablemenumouse");

  if (kempston_mouse_emulation.v)             ADD_STRING_CONFIG,"--enablekempstonmouse");

                                              ADD_STRING_CONFIG,"--kempstonmouse-sens %d",kempston_mouse_factor_sensibilidad);


  if (core_spectrum_uses_reduced.v)           ADD_STRING_CONFIG,"--spectrum-reduced-core");
  else                                        ADD_STRING_CONFIG,"--no-spectrum-reduced-core");


  if (menu_footer==0)                         ADD_STRING_CONFIG,"--disablefooter");
  if (menu_multitarea==0)                     ADD_STRING_CONFIG,"--disablemultitaskmenu");


  //if (screen_bw_no_multitask_menu.v==0)       ADD_STRING_CONFIG,"--disablebw-no-multitask");

  get_machine_config_name_by_number(buffer_temp,current_machine_type);

  if (buffer_temp[0]!=0) {
                                              ADD_STRING_CONFIG,"--machine %s",buffer_temp);
  }


  //Archivos recientes de smartload. Recorremos desde abajo hasta arriba
	for (i=MAX_LAST_FILESUSED-1;i>=0;i--) {
		if (last_files_used_array[i][0]!=0) ADD_STRING_CONFIG,"--addlastfile \"%s\"",last_files_used_array[i]);
	}





  if (binary_file_load[0]!=0) {
        //printf ("dir binary_file_load: %s\n",binary_file_load);
	util_get_dir(binary_file_load,buffer_temp);
        //printf ("dir binary_file_load final: %s\n",buffer_temp);
 	ADD_STRING_CONFIG,"--loadbinarypath \"%s\"",buffer_temp);
  }

  if (binary_file_save[0]!=0) {
        util_get_dir(binary_file_save,buffer_temp);
 	ADD_STRING_CONFIG,"--savebinarypath \"%s\"",buffer_temp);
  }


  if (recreated_zx_keyboard_support.v)	      ADD_STRING_CONFIG,"--recreatedzx");



  if (emulator_tmpdir_set_by_user[0]!=0) {
	//Quitar barra del final si la hay
	util_copy_path_delete_last_slash(emulator_tmpdir_set_by_user,buffer_temp);
 	ADD_STRING_CONFIG,"--tempdir \"%s\"",buffer_temp);
  }

                                              ADD_STRING_CONFIG,"--tool-sox-path \"%s\"",external_tool_sox);
                                              //ADD_STRING_CONFIG,"--tool-unzip-path \"%s\"",external_tool_unzip);
                                              ADD_STRING_CONFIG,"--tool-gunzip-path \"%s\"",external_tool_gunzip);
                                              ADD_STRING_CONFIG,"--tool-tar-path \"%s\"",external_tool_tar);
                                              ADD_STRING_CONFIG,"--tool-unrar-path \"%s\"",external_tool_unrar);
  if (mmc_file_name[0]!=0)                    ADD_STRING_CONFIG,"--mmc-file \"%s\"",mmc_file_name);
  if (mmc_enabled.v)                          ADD_STRING_CONFIG,"--enable-mmc");
  if (divmmc_mmc_ports_enabled.v)             ADD_STRING_CONFIG,"--enable-divmmc-ports");

  if (mmc_write_protection.v)		      ADD_STRING_CONFIG,"--mmc-write-protection");

  //Los settings de  mmc paging no guardarlo si maquina es tbblue, pues acaba activando el divmmc paging en tbblue y entonces esto provoca que no arranque la tbblue rom
    if (divmmc_diviface_enabled.v)            ADD_STRING_CONFIG,"--enable-divmmc-paging");
    if (divmmc_mmc_ports_enabled.v && divmmc_diviface_enabled.v)
                                              ADD_STRING_CONFIG,"--enable-divmmc");


                                              ADD_STRING_CONFIG,"--showfiredbreakpoint %d",debug_show_fired_breakpoints_type);


  if (debug_breakpoints_enabled.v)            ADD_STRING_CONFIG,"--enable-breakpoints");

  if (debug_shows_invalid_opcode.v)           ADD_STRING_CONFIG,"--show-invalid-opcode");


  if (debug_breakpoints_cond_behaviour.v==0)  ADD_STRING_CONFIG,"--brkp-always");


  if (debug_settings_show_screen.v)           ADD_STRING_CONFIG,"--show-display-debug");


  if (menu_debug_registers_if_showscan.v)     ADD_STRING_CONFIG,"--show-electron-debug");




  for (i=0;i<MAX_BREAKPOINTS_CONDITIONS;i++) {



			if (debug_breakpoints_conditions_array_tokens[i][0].tipo!=TPT_FIN) {

				//nuevo parser de breakpoints
				char buffer_temp[MAX_BREAKPOINT_CONDITION_LENGTH];
				exp_par_tokens_to_exp(debug_breakpoints_conditions_array_tokens[i],buffer_temp,MAX_PARSER_TOKENS_NUM);
                                ADD_STRING_CONFIG,"--set-breakpoint %d \"%s\"",i+1,buffer_temp);
			}


	}

  for (i=0;i<MAX_BREAKPOINTS_CONDITIONS;i++) {
    if (debug_breakpoints_actions_array[i][0]!=0)
                                              ADD_STRING_CONFIG,"--set-breakpointaction %d \"%s\"",i+1,debug_breakpoints_actions_array[i]);
  }



  for (i=0;i<DEBUG_MAX_WATCHES;i++) {

			if (debug_watches_array[i][0].tipo!=TPT_FIN) {

				char buffer_temp[MAX_BREAKPOINT_CONDITION_LENGTH];
				exp_par_tokens_to_exp(debug_watches_array[i],buffer_temp,MAX_PARSER_TOKENS_NUM);
                                ADD_STRING_CONFIG,"--set-watch %d \"%s\"",i+1,buffer_temp);
			}

	}



  for (i=0;i<65536;i++) {
		if (mem_breakpoint_array[i]!=0)
                                              ADD_STRING_CONFIG,"--set-mem-breakpoint %04XH %d",i,mem_breakpoint_array[i]);
	}

  if (hardware_debug_port.v)                  ADD_STRING_CONFIG,"--hardware-debug-ports");
  if (zesarux_zxi_hardware_debug_file[0]!=0)  ADD_STRING_CONFIG,"--hardware-debug-ports-byte-file \"%s\"",zesarux_zxi_hardware_debug_file);


  if (screen_show_splash_texts.v==0)          ADD_STRING_CONFIG,"--nosplash");
  if (screen_show_cpu_usage.v==0)             ADD_STRING_CONFIG,"--no-cpu-usage");
  if (screen_show_cpu_temp.v==0)              ADD_STRING_CONFIG,"--no-cpu-temp");
  if (screen_show_fps.v==0)                   ADD_STRING_CONFIG,"--no-fps");
  if (opcion_no_welcome_message.v)                     ADD_STRING_CONFIG,"--nowelcomemessage");

  if (menu_hide_vertical_percentaje_bar.v) ADD_STRING_CONFIG,"--hide-menu-percentage-bar");


  if (menu_hide_minimize_button.v)           ADD_STRING_CONFIG,"--hide-menu-minimize-button");
  if (menu_hide_close_button.v)              ADD_STRING_CONFIG,"--hide-menu-close-button");


  if (menu_invert_mouse_scroll.v)             ADD_STRING_CONFIG,"--invert-menu-mouse-scroll");

  if (menu_allow_background_windows)          ADD_STRING_CONFIG,"--allow-background-windows");

  if (always_force_overlay_visible_when_menu_closed) ADD_STRING_CONFIG,"--allow-background-windows-closed-menu");

                                              ADD_STRING_CONFIG,"--menu-mix-method \"%s\"",screen_menu_mix_methods_strings[screen_menu_mix_method]);


                                                ADD_STRING_CONFIG,"--menu-transparency-perc %d",screen_menu_mix_transparency);



  if (screen_menu_reduce_bright_machine.v)   ADD_STRING_CONFIG,"--menu-darken-when-open");
  if (screen_machine_bw_no_multitask.v)      ADD_STRING_CONFIG,"--menu-bw-multitask");



  if (rainbow_enabled.v)                      ADD_STRING_CONFIG,"--realvideo");

  if (autodetect_rainbow.v==0)                ADD_STRING_CONFIG,"--no-detect-realvideo");

  if (tbblue_store_scanlines.v)               ADD_STRING_CONFIG,"--tbblue-legacy-hicolor");
  if (tbblue_store_scanlines_border.v)        ADD_STRING_CONFIG,"--tbblue-legacy-border");

  if (ulaplus_presente.v)                     ADD_STRING_CONFIG,"--enableulaplus");
  if (timex_video_emulation.v)                ADD_STRING_CONFIG,"--enabletimexvideo");
  if (timex_mode_512192_real.v==0)	      ADD_STRING_CONFIG,"--disablerealtimex512");


  if (beeper_enabled.v==0)                    ADD_STRING_CONFIG,"--disablebeeper");
  if (beeper_real_enabled==0)                 ADD_STRING_CONFIG,"--disablerealbeeper");
  if (ay_retorna_numero_chips()>1)            ADD_STRING_CONFIG,"--totalaychips %d",ay_retorna_numero_chips() );

  if (ay3_stereo_mode>0)                      ADD_STRING_CONFIG,"--ay-stereo-mode %d",ay3_stereo_mode);

                                              ADD_STRING_CONFIG,"--ay-stereo-channel A %d",ay3_custom_stereo_A);
                                              ADD_STRING_CONFIG,"--ay-stereo-channel B %d",ay3_custom_stereo_B);
                                              ADD_STRING_CONFIG,"--ay-stereo-channel C %d",ay3_custom_stereo_C);

  if (audiodac_enabled.v)                     ADD_STRING_CONFIG,"--enableaudiodac");
                                              ADD_STRING_CONFIG,"--audiodactype \"%s\"",audiodac_types[audiodac_selected_type].name);

  if (audiovolume!=100)                       ADD_STRING_CONFIG,"--audiovolume %d",audiovolume);
  if (ay_player_exit_emulator_when_finish.v)  ADD_STRING_CONFIG,"--ayplayer-end-exit");
  if (ay_player_repeat_file.v==0)             ADD_STRING_CONFIG,"--ayplayer-end-no-repeat");
  if (ay_player_limit_infinite_tracks!=0)     ADD_STRING_CONFIG,"--ayplayer-inf-length %d",ay_player_limit_infinite_tracks/50);
  if (ay_player_limit_any_track!=0)           ADD_STRING_CONFIG,"--ayplayer-any-length %d",ay_player_limit_any_track/50);


  if (audio_midi_output_initialized)          ADD_STRING_CONFIG,"--enable-midi");
                                              ADD_STRING_CONFIG,"--midi-client %d",audio_midi_client);
                                              ADD_STRING_CONFIG,"--midi-port %d",audio_midi_port);

                                              ADD_STRING_CONFIG,"--midi-raw-device %s",audio_raw_midi_device_out);
  if (audio_midi_raw_mode==0)                 ADD_STRING_CONFIG,"--midi-no-raw-mode");


  if (midi_output_record_noisetone.v)         ADD_STRING_CONFIG,"--midi-allow-tone-noise");



  if (menu_limit_menu_open.v)                 ADD_STRING_CONFIG,"--limitopenmenu");

  if (menu_filesel_hide_dirs.v)         ADD_STRING_CONFIG,"--hide-dirs");

  if (menu_filesel_show_previews.v==0)         ADD_STRING_CONFIG,"--no-file-previews");

  if (menu_desactivado.v)                     ADD_STRING_CONFIG,"--disablemenu");

  if (menu_desactivado_andexit.v)              ADD_STRING_CONFIG,"--disablemenuandexit");


  if (menu_desactivado_file_utilities.v)      ADD_STRING_CONFIG,"--disablemenufileutils");


  if (menu_force_writing_inverse_color.v)     ADD_STRING_CONFIG,"--forcevisiblehotkeys");
  if (force_confirm_yes.v)                    ADD_STRING_CONFIG,"--forceconfirmyes");
                                              ADD_STRING_CONFIG,"--gui-style \"%s\"",definiciones_estilos_gui[estilo_gui_activo].nombre_estilo);


  for (i=0;i<total_config_window_geometry;i++) {
       ADD_STRING_CONFIG,"--windowgeometry %s %d %d %d %d", saved_config_window_geometry_array[i].nombre,
       saved_config_window_geometry_array[i].x,saved_config_window_geometry_array[i].y,
        saved_config_window_geometry_array[i].ancho,saved_config_window_geometry_array[i].alto);
  }

  if (menu_reopen_background_windows_on_start.v) ADD_STRING_CONFIG,"--enable-restore-windows");


  if (menu_allow_background_windows && menu_reopen_background_windows_on_start.v) {
        //Guardar lista de ventanas activas
	//Empezamos una a una, desde la de mas abajo
	zxvision_window *ventana;

	ventana=zxvision_current_window;

	if (ventana!=NULL) {

                ventana=zxvision_find_first_window_below_this(ventana);

                while (ventana!=NULL) {

                        if (ventana->geometry_name[0]!=0 && ventana->can_be_backgrounded) {
                                ADD_STRING_CONFIG,"--restorewindow \"%s\"",ventana->geometry_name);
                        }


                        ventana=ventana->next_window;

                }
        }

  }



  for (i=0;i<MAX_F_FUNCTIONS_KEYS;i++) {
    enum defined_f_function_ids accion=defined_f_functions_keys_array[i];
    if (accion!=F_FUNCION_DEFAULT) {
                                              ADD_STRING_CONFIG,"--def-f-function F%d \"%s\"",i+1,defined_f_functions_array[accion].texto_funcion);
    }
  }


  if (input_file_keyboard_name!=NULL && input_file_keyboard_inserted.v)         ADD_STRING_CONFIG,"--keyboardspoolfile \"%s\"",input_file_keyboard_name);

  if (input_file_keyboard_playing.v)           ADD_STRING_CONFIG,"--keyboardspoolfile-play");


                                              ADD_STRING_CONFIG,"--joystickemulated \"%s\"",joystick_texto[joystick_emulation]);

  if (remote_protocol_enabled.v)              ADD_STRING_CONFIG,"--enable-remoteprotocol");
                                              ADD_STRING_CONFIG,"--remoteprotocol-port %d",remote_protocol_port);

  if (realjoystick_disabled.v==1)              ADD_STRING_CONFIG,"--disablerealjoystick");


  if (no_native_linux_realjoystick.v)          ADD_STRING_CONFIG,"--no-native-linux-realjoy");



                          ADD_STRING_CONFIG,"--realjoystickpath %s",string_dev_joystick);

                        ADD_STRING_CONFIG,"--realjoystick-calibrate %d",realjoystick_autocalibrate_value);



  //real joystick buttons to events. Siempre este antes que el de events/buttons to keys
  for (i=0;i<MAX_EVENTS_JOYSTICK;i++) {
  	if (realjoystick_events_array[i].asignado.v) {
  		char texto_button[20];
  		int button_type;
  		button_type=realjoystick_events_array[i].button_type;

  		util_write_config_aux_realjoystick(button_type, realjoystick_events_array[i].button, texto_button);

  		ADD_STRING_CONFIG,"--joystickevent %s %s",texto_button,realjoystick_event_names[i]);
  	}
  }





  //real joystick buttons to keys
  for (i=0;i<MAX_KEYS_JOYSTICK;i++) {
  	if (realjoystick_keys_array[i].asignado.v) {
  		char texto_button[20];
  		int button_type;
  		z80_byte caracter;
  		caracter=realjoystick_keys_array[i].caracter;
  		button_type=realjoystick_keys_array[i].button_type;

		util_write_config_aux_realjoystick(button_type, realjoystick_keys_array[i].button, texto_button);

  		ADD_STRING_CONFIG,"--joystickkeybt %s %d",texto_button,caracter);
  	}
  }




  //joystickkeyev no lo estoy autoguardando, esto es mas indicado para archivos .config
  if (realjoystick_clear_keys_on_smartload.v) ADD_STRING_CONFIG,"--clearkeylistonsmart");



  if (quickexit.v)                            ADD_STRING_CONFIG,"--quickexit");

  //Guardar si hay algo que Guardar
  if (indice_string) {
    char configfile[PATH_MAX];
  	 FILE *ptr_configfile;

     if (util_get_configfile_name(configfile)==0)  {
       debug_printf(VERBOSE_ERR,"Unable to find $HOME, or HOMEDRIVE or HOMEPATH environment variables to open configuration file");
       return 0;
     }

     ptr_configfile=fopen(configfile,"a+");
     if (!ptr_configfile) {
                        debug_printf(VERBOSE_ERR,"Cannot write configuration file %s",configfile);
                        return 0;
      }

    fwrite(config_settings, 1, strlen(config_settings), ptr_configfile);


      fclose(ptr_configfile);
    }
    return 1;

}

//Leer archivo de configuracion en buffer
//Devuelve 0 si no existe
//Detecta que no exceda el limite
int configfile_read(char *mem,int limite)
{



	char configfile[PATH_MAX];

  if (util_get_configfile_name(configfile)==0)  {
    printf("Unable to find $HOME, or HOMEDRIVE or HOMEPATH environment variables to open configuration file\n");
    return 0;
  }



	//Ver si archivo existe

	if (!si_existe_archivo(configfile)) {
		printf("Configuration file %s not found\nCreating a new one\n",configfile);

    if (util_create_sample_configfile(1)==0) return 0;

	}

        int tamanyo_archivo=get_file_size(configfile);

        if (tamanyo_archivo>limite) {
             cpu_panic("Configuration file larger than maximum allowed size. Exiting");
        }

	return configfile_read_aux(configfile,mem);

}

//Retorna puntero a caracter leido que hace de final (sea final de linea o de archivo)
char *configfile_end_line(char *m)
{

	while ( (*m!='\n') && (*m!=0) ) {
		m++;
	}

	return m;
}

//Retorna puntero a caracter leido que hace de final (sea final de linea o de archivo)
char *configfile_next_field(char *m,int comillas_iniciales)
{

	//Hasta final de linea, o final de texto, o espacio (y no comilla previa), o comillas
       while (
		(*m!='\n') &&
		(*m!='\r') &&
		(*m!=0) &&
		(*m!='"')
	 ) {

		if (comillas_iniciales==0 && *m==' ') break;

                //Dejamos que se puedan escapar unas " si van precedidas de barra invertida
                //if ( (*m)=='\\') {
                //        if (*(m+1)=='"') m++;
                //}

                m++;
        }

        return m;
}


void configfile_parse_lines(char *mem,char *p_argv[],int *p_argc)
{
	char caracter;
	int argumentos=*p_argc;

	//para compatibilidad con rutinas de leer parametros, argv[0] no lo usamos
	p_argv[0]="";
	argumentos++;

	do {
		caracter=*mem;

		//Fin
		if (caracter==0) {
			*p_argc=argumentos;
			return;
		}

		//Comentarios
		if (caracter==';' || caracter=='#') {
			mem=configfile_end_line(mem);
			if (*mem) mem++;
		}

		//Estamos en salto de linea o espacio o retorno de carro, saltamos
		else if (caracter=='\n' || caracter==' ' || caracter=='\r' ) mem++;

		else {

			int comillas_iniciales=0;

			//Parametro correcto
			//Ver si comillas iniciales
			if (caracter=='"') {
				comillas_iniciales=1;
				mem++;
				//printf ("comillas iniciales en %s\n",mem);
			}


			p_argv[argumentos]=mem;
			argumentos++;
                        if (argumentos>MAX_PARAMETERS_CONFIG_FILE) {
                                cpu_panic("Maximum parameters in config file reached");
                        }

			mem=configfile_next_field(mem,comillas_iniciales);

			//Poner codigo 0 al final
			if (*mem) {
				*mem=0;
				mem++;
			}
		}

	} while (1);

}


char *configfile_argv[MAX_PARAMETERS_CONFIG_FILE];
int configfile_argc=0;

//Convertir archivo de configuracion leido en argv

//Ignorar lineas que empiecen con ; o #
//Separar lineas y espacios en diferentes parametros; tener en cuenta texto que haya entre comillas, no separar por espacios
//Cada final de parametro tendra codigo 0
void configfile_parse(void)
{
	char *mem_config=malloc(MAX_SIZE_CONFIG_FILE+1);
        //printf ("mem_config init: %p\n",mem_config);

        if (mem_config==NULL) {
		cpu_panic("Unable to allocate memory for configuration file");
	}

	if (configfile_read(mem_config,MAX_SIZE_CONFIG_FILE)==0) {
		//No hay archivo de configuracion. Parametros vacios
		configfile_argv[0]="";
		configfile_argc=1;
                //free(mem_config);
		return;
	}


	configfile_parse_lines(mem_config,&configfile_argv[0],&configfile_argc);

	//Mostrar cada parametro. Ignoramos el primero (numero 0) pues es vacio
	if (debug_parse_config_file.v) {
		int i;
		for (i=1;i<configfile_argc;i++) {
			printf ("Debug: Configfile, parameter %d = [%s]\n",i,configfile_argv[i]);
		}
	}

        //TODO: Este free da segmentation fault. Por que??? Revisar los otros free de leer archivos de config que estan comentados tambien
        //printf ("mem_config in free: %p\n",mem_config);
        //free(mem_config);
	return;
}

char *custom_config_mem_pointer=NULL;

//Parsear archivo indicado de configuracion
void configfile_parse_custom_file(char *archivo)
{

	//Ver si hay que asignar memoria
	//Debe quedar esta memoria asignada al salir de este procedimiento, pues lo usa en parse_customfile_options
	if (custom_config_mem_pointer==NULL) {
		debug_printf (VERBOSE_DEBUG,"Allocating memory to read custom config file");
		custom_config_mem_pointer=malloc(MAX_SIZE_CONFIG_FILE+1);
	}

	else {
		debug_printf (VERBOSE_DEBUG,"No need to allocate memory to read custom config file, we allocated it before");
	}


        if (custom_config_mem_pointer==NULL) {
                cpu_panic("Unable to allocate memory for configuration file");
        }

	//Valor inicial
	configfile_argc=0;


        if (configfile_read_aux(archivo,custom_config_mem_pointer)==0) {
                //No hay archivo de configuracion. Parametros vacios
                configfile_argv[0]="";
                configfile_argc=1;
                //free(custom_config_mem_pointer);
                return;
        }


        configfile_parse_lines(custom_config_mem_pointer,&configfile_argv[0],&configfile_argc);

        //Mostrar cada parametro. Ignoramos el primero (numero 0) pues es vacio
        if (debug_parse_config_file.v) {
                int i;
                for (i=1;i<configfile_argc;i++) {
                        printf ("Debug: Custom Configfile, parameter %d = [%s]\n",i,configfile_argv[i]);
                }
        }

        //free(custom_config_mem_pointer);
        return;
}


//Parsear archivo de configuracion indicado, usado en insertar cinta, snap, etc
void parse_custom_file_config(char *archivo)
{

	configfile_parse_custom_file(archivo);
	argc=configfile_argc;
	argv=configfile_argv;
	puntero_parametro=0;

	parse_customfile_options();
}


tecla_redefinida lista_teclas_redefinidas[MAX_TECLAS_REDEFINIDAS];


void clear_lista_teclas_redefinidas(void)
{
	int i;
	for (i=0;i<MAX_TECLAS_REDEFINIDAS;i++) {
		lista_teclas_redefinidas[i].tecla_original=0;
	}
	debug_printf (VERBOSE_DEBUG,"Cleared redefined keys table");
}

//Poder redefinir una tecla. Retorna 0 si no hay redefinicion. Retorna 1 si lo hay
z80_byte util_redefinir_tecla(z80_byte tecla)
{

	int i;
	z80_byte tecla_original,tecla_redefinida;
	for (i=0;i<MAX_TECLAS_REDEFINIDAS;i++) {
		tecla_original=lista_teclas_redefinidas[i].tecla_original;
		tecla_redefinida=lista_teclas_redefinidas[i].tecla_redefinida;
		if (tecla_original) {
			if (tecla_original==tecla) return tecla_redefinida;
		}
	}

	return 0;


}

//Agregar una tecla a la lista de redefinicion. Retorna 0 si ok, 1 si error (lista llena por ejemplo)
int util_add_redefinir_tecla(z80_byte p_tecla_original, z80_byte p_tecla_redefinida)
{

	int i;
        z80_byte tecla_original;
	//z80_byte tecla_redefinida;

	for (i=0;i<MAX_TECLAS_REDEFINIDAS;i++) {
		tecla_original=lista_teclas_redefinidas[i].tecla_original;
                //tecla_redefinida=lista_teclas_redefinidas[i].tecla_redefinida;
		if (tecla_original==0) {
			lista_teclas_redefinidas[i].tecla_original=p_tecla_original;
			lista_teclas_redefinidas[i].tecla_redefinida=p_tecla_redefinida;
			debug_printf (VERBOSE_DEBUG,"Added key %d to be %d",p_tecla_original,p_tecla_redefinida);
			return 0;
		}
	}

	debug_printf (VERBOSE_ERR,"Maximum redefined keys (%d)",MAX_TECLAS_REDEFINIDAS);
	return 1;
}


void convert_numeros_letras_puerto_teclado_continue(z80_byte tecla,int pressrelease)
{


       //Si teclado recreated, y menu cerrado
       /*if (!menu_abierto && recreated_zx_keyboard_support.v) {

               enum util_teclas tecla_final;
               int pressrelease_final;
               //printf ("Entrada a convertir tecla recreated desde convert_numeros_letras_puerto_teclado_continue\n");
               recreated_zx_spectrum_keyboard_convert(tecla, &tecla_final, &pressrelease_final);
               if (tecla_final) {
               		//printf ("redefinicion de tecla. antes: %d despues: %d\n",tecla,tecla_final);
                       tecla=tecla_final;
                       pressrelease=pressrelease_final;

			if (tecla==UTIL_KEY_CAPS_SHIFT) {
				//printf ("Pulsada caps shift (%d)\n",UTIL_KEY_CAPS_SHIFT);
				util_set_reset_key(tecla,pressrelease);
				return;
			}

                       //Si sigue estando entre a-z y 0-9 enviar tal cual. Si no, llamar a util_set_reset_key
                       if (
                       	(tecla>='a' && tecla<='z') ||
                       	(tecla>='0' && tecla<='9')
                       	)
                       {
                       	//Nada. Prefiero escribir la condicion asi que no poner un negado
                       	//printf("Tecla final es entre az y 09. generar puerto spectrum\n");
                       }


                       else {
                       	//util_set_reset_key(tecla,pressrelease);
                       	//printf ("Enviar a util_set_reset_key_convert_recreated_yesno sin convertir\n");
                       	util_set_reset_key_convert_recreated_yesno(tecla,pressrelease,0);
                       	return;
                       }
               }
       }*/

       convert_numeros_letras_puerto_teclado_continue_after_recreated(tecla,pressrelease);
}



void convert_numeros_letras_puerto_teclado_continue_after_recreated(z80_byte tecla,int pressrelease)
{

	//Redefinicion de teclas
	z80_byte tecla_redefinida;
	tecla_redefinida=util_redefinir_tecla(tecla);
	if (tecla_redefinida) tecla=tecla_redefinida;

  if (tecla>='a' && tecla<='z') {
      int indice=tecla-'a';

      z80_byte *puerto;
      puerto=tabla_teclado_letras[indice].puerto;

      z80_byte mascara;
      mascara=tabla_teclado_letras[indice].mascara;

                                        //printf ("asignamos tecla mediante array indice=%d puerto=%x mascara=%d - ",indice,puerto,mascara);

      if (pressrelease) *puerto &=255-mascara;
      else *puerto |=mascara;
  }

  if (tecla>='0' && tecla<='9') {
  int indice=tecla-'0';

  z80_byte *puerto;
  puerto=tabla_teclado_numeros[indice].puerto;

  z80_byte mascara;
  mascara=tabla_teclado_numeros[indice].mascara;


  if (pressrelease) *puerto &=255-mascara;
  else *puerto |=mascara;
  }
}


//Retorna 1 si existe archivo
//0 si no
int si_existe_archivo(char *nombre)
{


                //Ver si archivo existe y preguntar
                struct stat buf_stat;

                if (stat(nombre, &buf_stat)==0) {

			return 1;

                }

	return 0;

}


//Retorna tamanyo archivo
long int get_file_size(char *nombre)
{
        struct stat buf_stat;

                if (stat(nombre, &buf_stat)!=0) {
                        debug_printf(VERBOSE_INFO,"Unable to get status of file %s",nombre);
			return 0;
                }

                else {
			//printf ("file size: %ld\n",buf_stat.st_size);
			return buf_stat.st_size;
                }
}





//Retorna numero lineas archivo.
int get_file_lines(char *filename)
{

	int leidos;
        int total_lineas=0;

        //Leemos primero todo el archivo en memoria
        long int tamanyo_archivo=get_file_size(filename);

        z80_byte *buffer_memoria;
        z80_byte *buffer_memoria_copia;

        buffer_memoria=malloc(tamanyo_archivo);
        if (buffer_memoria==NULL) cpu_panic("Can not allocate memory for counting file lines");
        buffer_memoria_copia=buffer_memoria;

        FILE *ptr_archivo;
        ptr_archivo=fopen(filename,"rb");
        if (!ptr_archivo) {
                debug_printf (VERBOSE_DEBUG,"Can not open %s",filename);
                return 0;
        }

        leidos=fread(buffer_memoria,1,tamanyo_archivo,ptr_archivo);
        fclose(ptr_archivo);

        /*
        Nota: leyendo de fread todo de golpe, o byte a byte, suele tardar lo mismo, EXCEPTO, en el caso que el archivo este en cache
        del sistema operativo, parece que leer byte a byte no tira de cache. En cambio, leyendo todo de golpe, si que usa la cache
        Ejemplo:
        Leer archivo de 371 MB. lineas total: 14606365
        25 segundos aprox en leer y contar lineas, usando fread byte a byte
        Si uso fread leyendo todo de golpe, tarda menos de 1 segundo
        */

        z80_byte byte_leido;

        while (leidos>0) {
                byte_leido=*buffer_memoria_copia;
                buffer_memoria_copia++;
                leidos--;

                if (byte_leido=='\n') total_lineas++;

                //if (leidos<5) printf ("leyendo de memoria\n");
        }

        if (tamanyo_archivo>0) {
                //printf ("liberando memoria\n");
                free(buffer_memoria);
        }

        //printf ("lineas total: %d\n",total_lineas);

	return total_lineas;

}



//Retorna -1 si hay algun error
//retorna bytes leidos si ok
int lee_archivo(char *nombre,char *buffer,int max_longitud)
{

	int leidos;

                FILE *ptr_archivo;
                ptr_archivo=fopen(nombre,"rb");
                if (!ptr_archivo) {
                        debug_printf (VERBOSE_DEBUG,"Can not open %s",nombre);
                        return -1;
                }

                leidos=fread(buffer,1,max_longitud,ptr_archivo);
                fclose(ptr_archivo);

		return leidos;

}


//Decir si el raton funciona sobre el menu
int si_menu_mouse_activado(void)
{
  //if (kempston_mouse_emulation.v) return 0;
  if (gunstick_emulation) return 0;
  if (mouse_menu_disabled.v) return 0;

  return 1;
}


//Numero de veces que se ha pulsado la tecla de abrir el menu
int util_if_open_just_menu_times=0;

//Contador que se incrementa cada vez desde timer.c (50 veces por segundo)
unsigned int util_if_open_just_menu_counter=0;

//Contador inicial de la primera pulsacion de tecla de abrir menu
unsigned int util_if_open_just_menu_initial_counter=0;

//Accion de abrir menu (F5, boton joystick) que ademas controla si el boton esta limitado para que abra el menu solo cuando se pulsa 3 veces seguidas, por ejemplo, en 1 segundo
int util_if_open_just_menu(void)
{
	if (menu_limit_menu_open.v==0) return 1;

	//esta limitado el uso de F5, hay que pulsar 3 veces en el ultimo segundo
	if (util_if_open_just_menu_times==0) {
		util_if_open_just_menu_initial_counter=util_if_open_just_menu_counter;
	}

	util_if_open_just_menu_times++;
	debug_printf (VERBOSE_DEBUG,"Pressed open menu key: %d times",util_if_open_just_menu_times);

	//Si llega a 3 veces, ver si la diferencia del contador es menor o igual que 50
	if (util_if_open_just_menu_times==3) {
		util_if_open_just_menu_times=0;
		int diferencia=util_if_open_just_menu_counter-util_if_open_just_menu_initial_counter;
		debug_printf (VERBOSE_DEBUG,"Time elapsed between the third keypress and the first one (in 1/50s): %d",diferencia);
		if (diferencia<=50) return 1;
	}

	return 0;
}


//Si esta emulacion de kempston mouse activa, no se puede usar el raton para el menu
void util_set_reset_mouse(enum util_mouse_buttons boton,int pressrelease)
{

  switch(boton)
  {
    case UTIL_MOUSE_LEFT_BUTTON:
      if (pressrelease) {

        //printf ("Leido press release mouse\n");
        if (si_menu_mouse_activado() ) {
          //Si no esta menu abierto, hace accion de abrir menu, siempre que no este kempston
          if (menu_abierto==0) {
                  if (kempston_mouse_emulation.v==0) {
                          menu_fire_event_open_menu();
                          menu_was_open_by_left_mouse_button.v=1;
                  }
          }
          else {
            //Si esta menu abierto, es como enviar enter, pero cuando no esta la ventana en background
            if (zxvision_keys_event_not_send_to_machine) {
                    //y si se pulsa dentro de ventana
                    if (si_menu_mouse_en_ventana() ) {
                    //util_set_reset_key(UTIL_KEY_ENTER,1);  //Ya no enviamos enter al pulsar boton mouse
                    }
            }
          }
        }

        mouse_left=1;
        //printf ("left button pressed\n");

      }
      else {

        if (si_menu_mouse_activado()) {

          if (menu_abierto==1) {
            //Si esta menu abierto, es como enviar enter
            //util_set_reset_key(UTIL_KEY_ENTER,0); //Ya no enviamos enter al pulsar boton mouse
          }
        }

        mouse_left=0;
        //printf ("reseteamos mouse_pressed_close_window desde utils ventana\n");
        //puerto_especial1 |=1;
        mouse_pressed_close_window=0;
        mouse_pressed_background_window=0;
        mouse_pressed_hotkey_window=0;
        mouse_pressed_hotkey_window_key=0;

      }
    break;

    case UTIL_MOUSE_RIGHT_BUTTON:
      if (pressrelease) {
        mouse_right=1;
        //Si esta menu abierto, hace como ESC

        if (si_menu_mouse_activado() ) {
          //Si no esta menu abierto, hace accion de abrir menu, siempre que no este kempston
          if (menu_abierto==0) {
                  if (kempston_mouse_emulation.v==0) menu_fire_event_open_menu();
          }
          else {
            //Si esta menu abierto, es como enviar ESC
            if (zxvision_keys_event_not_send_to_machine) {
                    //y si se pulsa dentro de ventana
                    //no ver dentro ventana
                    //if (si_menu_mouse_en_ventana() ) {
                    util_set_reset_key(UTIL_KEY_ESC,1);
                    //}
            }
          }
        }

      }
      else {
        mouse_right=0;
        //Si esta menu abierto, hace como ESC
        if (si_menu_mouse_activado()) {
          if (menu_abierto) util_set_reset_key(UTIL_KEY_ESC,0);
        }
      }
    break;

  }


}


void util_set_reset_key_convert_recreated_yesno(enum util_teclas tecla,int pressrelease,int convertrecreated)
{


	//Para poder evitar que se vuelva a convertir cuando se llama desde convert_numeros_letras_puerto_teclado_continue
	if (convertrecreated) {
		//Si teclado recreated, y menu cerrado
		if (!menu_abierto && recreated_zx_keyboard_support.v) {

			enum util_teclas tecla_final;
			int pressrelease_final;

			//Si es mayusculas
			if (tecla==UTIL_KEY_SHIFT_L) {
				//printf ("Pulsada shift L real\n");
				if (pressrelease) recreated_zx_keyboard_pressed_caps.v=1;
				else recreated_zx_keyboard_pressed_caps.v=0;
				return;
			}

			//printf ("Entrada a convertir tecla recreated desde util_set_reset_key\n");
			recreated_zx_spectrum_keyboard_convert(tecla, &tecla_final, &pressrelease_final);
			if (tecla_final) {
				tecla=tecla_final;
				pressrelease=pressrelease_final;
			}
		}
	}

	util_set_reset_key_continue(tecla,pressrelease);
}




void util_set_reset_key(enum util_teclas tecla,int pressrelease)
{
       util_set_reset_key_convert_recreated_yesno(tecla,pressrelease,1);
}

void convert_numeros_letras_puerto_teclado(z80_byte tecla,int pressrelease)
{

        convert_numeros_letras_puerto_teclado_continue(tecla,pressrelease);
}






//Retorna no 0 si hay redefinicion de tecla F (no es default)
int util_set_reset_key_continue_f_functions(enum util_teclas tecla,int pressrelease)
{

  //printf ("tecla: %d pressrelease: %d menu_abierto: %d\n",tecla,pressrelease,menu_abierto);

  //Aunque este el menu abierto, procesar estas teclas. Asi esto permite que la tecla de OSD aparezca
  //desde el menu
  //if (menu_abierto) return 0;
  //Ver si la tecla F esta asignada
  //enum defined_f_function_ids defined_f_functions_keys_array[MAX_F_FUNCTIONS_KEYS]

  int indice;

  switch (tecla) {
    case UTIL_KEY_F1:
      indice=0;
    break;

    case UTIL_KEY_F2:
      indice=1;
    break;

    case UTIL_KEY_F3:
      indice=2;
    break;

    case UTIL_KEY_F4:
      indice=3;
    break;

    case UTIL_KEY_F5:
      indice=4;
      //printf ("F5!!\n");
    break;

    case UTIL_KEY_F6:
      indice=5;
    break;

    case UTIL_KEY_F7:
      indice=6;
    break;

    case UTIL_KEY_F8:
      indice=7;
    break;

    case UTIL_KEY_F9:
      indice=8;
    break;

    case UTIL_KEY_F10:
      indice=9;
    break;

    case UTIL_KEY_F11:
      indice=10;
    break;

    case UTIL_KEY_F12:
      indice=11;
    break;

    case UTIL_KEY_F13:
      indice=12;
    break;

    case UTIL_KEY_F14:
      indice=13;
    break;

    case UTIL_KEY_F15:
      indice=14;
    break;

    default:
      return 0;
    break;
  }

  enum defined_f_function_ids accion=defined_f_functions_keys_array[indice];

  debug_printf (VERBOSE_DEBUG,"Key: F%d Action: %s",indice+1,defined_f_functions_array[accion].texto_funcion);

  //Abrir menu si funcion no es defecto y no es background window
  //if (accion!=F_FUNCION_DEFAULT && accion!=F_FUNCION_BACKGROUND_WINDOW) {
  if (accion!=F_FUNCION_DEFAULT) {
    if (pressrelease) {
      //Activar funcion f en menu
      menu_button_f_function.v=1;
      menu_button_f_function_index=indice;
      menu_abierto=1;


    }
    return 1;
  }

  return 0;
}


//Aqui solo se activan/desactivan bits de puerto_especial para teclas F
void util_set_reset_key_continue_tecla_f(enum util_teclas tecla,int pressrelease)
{

switch(tecla)
{

  //F1 pulsado
  case UTIL_KEY_F1:

          if (pressrelease) {
                  puerto_especial2 &=255-1;
          }
          else {
                  puerto_especial2 |=1;
          }
  break;

  //F2 pulsado
  case UTIL_KEY_F2:

          if (pressrelease) {
                  puerto_especial2 &=255-2;
          }
          else {
                  puerto_especial2 |=2;
          }
  break;


  case UTIL_KEY_F3:

          if (pressrelease) {
                  puerto_especial2 &=255-4;
          }
          else {
                  puerto_especial2 |=4;
          }
  break;


       case UTIL_KEY_F4:
           if (pressrelease) {
             puerto_especial2 &=255-8;
           }

           else {
             puerto_especial2 |=8;
           }
      break;


  case UTIL_KEY_F5:

          if (pressrelease) {
                  puerto_especial2 &=255-16;
          }
          else {
                  puerto_especial2 |=16;
          }
  break;


  case UTIL_KEY_F6:

          if (pressrelease) {
                  puerto_especial3 &=255-1;
          }
          else {
                  puerto_especial3 |=1;
          }
  break;


  case UTIL_KEY_F7:

          if (pressrelease) {
                  puerto_especial3 &=255-2;
          }
          else {
                  puerto_especial3 |=2;
          }
  break;


  case UTIL_KEY_F8:
        if (pressrelease) {
               puerto_especial3 &=255-4;
        }
        else {
              puerto_especial3 |=4;
        }

  break;


  case UTIL_KEY_F9:

          if (pressrelease) {
              puerto_especial3 &=255-8;
          }
          else {
              puerto_especial3 |=8;
          }

  break;


  case UTIL_KEY_F10:

          if (pressrelease) {
                  puerto_especial3 &=255-16;
          }
          else {
                  puerto_especial3 |=16;
          }
  break;


  case UTIL_KEY_F11:

          if (pressrelease) {
                  puerto_especial4 &=255-1;
          }
          else {
                  puerto_especial4 |=1;
          }
  break;


  case UTIL_KEY_F12:

          if (pressrelease) {
                  puerto_especial4 &=255-2;
          }
          else {
                  puerto_especial4 |=2;
          }
  break;


  case UTIL_KEY_F13:

          if (pressrelease) {
                  puerto_especial4 &=255-4;
          }
          else {
                  puerto_especial4 |=4;
          }
  break;


  case UTIL_KEY_F14:

          if (pressrelease) {
                  puerto_especial4 &=255-8;
          }
          else {
                  puerto_especial4 |=8;
          }
  break;


  case UTIL_KEY_F15:

          if (pressrelease) {
                  puerto_especial4 &=255-16;
          }
          else {
                  puerto_especial4 |=16;
          }
  break;

  default:
    //Para que no se queje el compilador
  break;

}

//printf ("puerto especial 4 en set: %d\n",puerto_especial4);

}

void util_set_reset_key_continue(enum util_teclas tecla,int pressrelease)
{

  //Activar bits de puertos_especiales para teclas F
  //Hay que hacerlo asi para que el menu se vea notificado de pulsacion o no pulsacion de esas teclas, estén o no asignadas a funciones
  //Sirve por ejemplo para que si tenemos mapeado F13 a hard reset, al pulsar F13, cuando se llama a menu_espera_no_tecla, que funcione
  util_set_reset_key_continue_tecla_f(tecla,pressrelease);

  //Ver si hay teclas F redefinidas
  if (util_set_reset_key_continue_f_functions(tecla,pressrelease)) return;

  util_set_reset_key_continue_after_zeng(tecla,pressrelease);
}

void util_set_reset_key_continue_after_zeng(enum util_teclas tecla,int pressrelease)
{

        //temp reasignacion
        //if (tecla==UTIL_KEY_ALT_R) tecla=UTIL_KEY_ENTER;

	switch (tecla) {
                        case UTIL_KEY_SPACE:
                                if (pressrelease) {
                                        puerto_32766 &=255-1;
                                }
                                else {
                                        puerto_32766 |=1;
                                }
                        break;

                        case UTIL_KEY_ENTER:
                                if (pressrelease) {
                                        puerto_49150 &=255-1;
                                }

                                else {
                                        puerto_49150 |=1;
                                }


                        break;

                        case UTIL_KEY_SHIFT_L:
                        case UTIL_KEY_CAPS_SHIFT:

                                if (pressrelease) menu_capshift.v=1;
                                else menu_capshift.v=0;

                                if (pressrelease) {
                                        puerto_65278  &=255-1;
                                }
                                else  {
                                        puerto_65278 |=1;
                                }
                        break;

                        case UTIL_KEY_SHIFT_R:
                                if (pressrelease) menu_capshift.v=1;
                                else menu_capshift.v=0;

                                if (pressrelease) {
                                        puerto_65278  &=255-1;
                                }
                                else  {
                                        puerto_65278 |=1;
                                }
                        break;

                        case UTIL_KEY_ALT_L:

                                util_press_menu_symshift(pressrelease);
//puerto_32766    db              255  ; B    N    M    Simb Space ;7

                                        if (pressrelease) {
                                                puerto_32766  &=255-2;
                                        }


                                        else  {
                                                puerto_32766 |=2;
                                        }
                        break;

                        case UTIL_KEY_ALT_R:
                                util_press_menu_symshift(pressrelease);

//puerto_32766    db              255  ; B    N    M    Simb Space ;7

                                        if (pressrelease) {
                                                puerto_32766  &=255-2;
                                        }


                                        else  {
                                                puerto_32766 |=2;
                                        }
                        break;


                        case UTIL_KEY_CONTROL_R:
                        case UTIL_KEY_CONTROL_L:

                                util_press_menu_symshift(pressrelease);

//puerto_32766    db              255  ; B    N    M    Simb Space ;7

                                        if (pressrelease) {
                                                puerto_32766  &=255-2;
                                        }


                                        else  {
                                                puerto_32766 |=2;

                                        }
                        break;

                        //Teclas que generan doble pulsacion
                        case UTIL_KEY_BACKSPACE:
                                if (pressrelease) menu_backspace.v=1;
                                else menu_backspace.v=0;

	                                if (pressrelease) {
        	                                puerto_65278 &=255-1;
                	                        puerto_61438 &=255-1;
	                                }
        	                        else {
                	                        puerto_65278 |=1;
                        	                puerto_61438 |=1;
	                                }
                        break;


                        case UTIL_KEY_HOME:
                                if (pressrelease) {
                                        joystick_set_fire(1);
                                }
                                else {
                                        joystick_release_fire(1);
                                }
                        break;

                        case UTIL_KEY_LEFT:
                                if (pressrelease) {
                                        //puerto_65278 &=255-1;
                                        //puerto_63486 &=255-16;
                                        joystick_set_left(1);
                                }
                                else {
                                        //puerto_65278 |=1;
                                        //puerto_63486 |=16;
                                        joystick_release_left(1);
                                }
                        break;
                        case UTIL_KEY_RIGHT:
                                if (pressrelease) {
                                        //puerto_65278 &=255-1;
                                        //puerto_61438 &=255-4;
                                        joystick_set_right(1);
                                }
                                else {
                                        //puerto_65278 |=1;
                                        //puerto_61438 |=4;
                                        joystick_release_right(1);
                                }
                        break;

                        case UTIL_KEY_DOWN:
                                if (pressrelease) {
                                        //puerto_65278 &=255-1;
                                        //puerto_61438 &=255-16;
                                        joystick_set_down(1);

                                }
                                else {
                                        //puerto_65278 |=1;
                                        //puerto_61438 |=16;
                                        joystick_release_down(1);

                                }
                        break;

                        case UTIL_KEY_UP:
                                if (pressrelease) {
                                        //puerto_65278 &=255-1;
                                        //puerto_61438 &=255-8;
                                        joystick_set_up(1);
                                }
                                else {
                                        //puerto_65278 |=1;
                                        //puerto_61438 |=8;
                                        joystick_release_up(1);
                                }
                        break;


                        //las 5 son botones de joystick que vienen desde ZENG (entrade de comando por ZRCP) exclusivamente.
                        //Se diferencian de las anteriores en que no vuelven a generar evento ZENG de nuevo
                        case UTIL_KEY_JOY_FIRE:
                                if (pressrelease) {
                                        joystick_set_fire(0);
                                }
                                else {
                                        joystick_release_fire(0);
                                }
                        break;

                        case UTIL_KEY_JOY_LEFT:
                                if (pressrelease) {
                                        joystick_set_left(0);
                                }
                                else {
                                        joystick_release_left(0);
                                }
                        break;
                        case UTIL_KEY_JOY_RIGHT:
                                if (pressrelease) {
                                        joystick_set_right(0);
                                }
                                else {
                                        joystick_release_right(0);
                                }
                        break;

                        case UTIL_KEY_JOY_DOWN:
                                if (pressrelease) {
                                        joystick_set_down(0);


                                }
                                else {
                                        joystick_release_down(0);

                                }
                        break;

                        case UTIL_KEY_JOY_UP:
                                if (pressrelease) {
                                        joystick_set_up(0);
                                }
                                else {
                                        joystick_release_up(0);
                                }
                        break;





                        case UTIL_KEY_TAB:
                                if (pressrelease) {
                                        menu_tab.v=1;
                                        //printf ("Pulsado TAB\n");
                                }
                                else {
                                        menu_tab.v=0;
                                        //printf ("Liberado TAB\n");
                                }

	                                if (pressrelease) {
                                        	puerto_65278 &=255-1;
                                	        puerto_32766 &=255-2;
        	                        }



	                                else {
                                	        puerto_65278 |=1;
                        	                puerto_32766 |=2;
	                                }
                        break;

                        case UTIL_KEY_CAPS_LOCK:

	                                if (pressrelease) {
        	                                puerto_65278 &=255-1;
                	                        puerto_63486 &=255-2;
	                                }
        	                        else {
                	                        puerto_65278 |=1;
                        	                puerto_63486 |=2;
	                                }
                        break;

                        case UTIL_KEY_COMMA:
                                util_press_menu_symshift(pressrelease);
	                                if (pressrelease) {
        	                                puerto_32766 &=255-2-8;

                	                }
                        	        else {
                                	        puerto_32766 |=2+8;

	                                }
                        break;


                        //Punto
                        case UTIL_KEY_PERIOD:
                                util_press_menu_symshift(pressrelease);
                                        if (pressrelease) {
                                                puerto_32766 &=255-2-4;

                                        }
                                        else {
                                                puerto_32766 |=2+4;

                                        }
                        break;

                        case UTIL_KEY_MINUS:

                                if (pressrelease) {
                                        set_symshift();
                                        puerto_49150 &=255-8;
                                }
                                else {
                                        clear_symshift();
                                        puerto_49150 |=8;
                                }
                        break;
                       case UTIL_KEY_PLUS:
                                if (pressrelease) {
                                        set_symshift();
                                        puerto_49150 &=255-4;
                                }
                                else {
                                        clear_symshift();
                                        puerto_49150 |=4;
                                }
                        break;

                        case UTIL_KEY_SLASH:
                                if (pressrelease) {
                                        set_symshift();
                                        puerto_65278 &=255-16;
                                }
                                else {
                                        clear_symshift();
                                        puerto_65278 |=16;
                                }
                        break;

                        case UTIL_KEY_ASTERISK:
                                if (pressrelease) {
                                        set_symshift();
                                        puerto_32766 &=255-16;
                                }
                                else {
                                        clear_symshift();
                                        puerto_32766 |=16;
                                }
                        break;

                        case UTIL_KEY_F5:

                                if (pressrelease) {
					                    if (util_if_open_just_menu() )  {
                                            //printf("Disparar evento abrir menu\n");
                                            menu_fire_event_open_menu();
                                        }
                                }
                                else {
                                }
                        break;

                        //z80_byte puerto_especial3=255; //  F10 F9 F8 F7 F6

                        //F6 pulsado. De momento no hace nada
                        case UTIL_KEY_F6:

                                if (pressrelease) {

                                }
                                else {

                                }
                        break;

                        //F7 pulsado. Uso del simulador de joystick si esta habilitado
                        case UTIL_KEY_F7:

                                if (pressrelease) {
					if (simulador_joystick) {
						simulador_joystick_forzado=1;
					}
                                }
                                else {

                                }
                        break;


                        case UTIL_KEY_F8:
                        break;


                        case UTIL_KEY_F9:
                        break;

                        //F10 pulsado
                        case UTIL_KEY_F10:

                                if (pressrelease) {
                                }
                                else {
                                }
                        break;


                        //z80_byte puerto_especial3=255; //  F10 F9 F8 F7 F6


                        //z80_byte puerto_especial4=255; //  F15 F14 F13 F12 F11

                        //F11 pulsado. De momento no hace nada
                        case UTIL_KEY_F11:

                                if (pressrelease) {
                                }
                                else {
                                }
                        break;

                        //F12 pulsado. De momento no hace nada
                        case UTIL_KEY_F12:

                                if (pressrelease) {
                                }
                                else {
                                }
                        break;

                        //F13 pulsado. De momento no hace nada
                        case UTIL_KEY_F13:

                                if (pressrelease) {
                                }
                                else {
                                }
                        break;

                        //F14 pulsado. De momento no hace nada
                        case UTIL_KEY_F14:

                                if (pressrelease) {
                                }
                                else {
                                }
                        break;

                        //F15 pulsado. De momento no hace nada
                        case UTIL_KEY_F15:

                                if (pressrelease) {
                                }
                                else {
                                }
                        break;




                        case UTIL_KEY_ESC:
//A15 (#7) | RSH    SQR     ESC     INDEX   CAPS    .       /       £
                          //ESC para Spectrum es BREAK, siempre que no este text to speech habilitado
                          if (pressrelease) {
                            puerto_65278 &=255-1;
                            puerto_32766 &=255-1;
                                puerto_especial1 &=255-1;
                          }
                          else {
                            puerto_65278 |=1;
                            puerto_32766 |=1;
                                puerto_especial1 |=1;
                          }
			break;

                        //PgUP
                        case UTIL_KEY_PAGE_UP:

                                if (pressrelease) {
                                        puerto_especial1 &=255-2;
                                }
                                else {
                                        puerto_especial1 |=2;
                                }
                        break;

                        //PgDn
                        case UTIL_KEY_PAGE_DOWN:

                                if (pressrelease) {
                                        puerto_especial1 &=255-4;
                                }
                                else {
                                        puerto_especial1 |=4;
                                }
                        break;

			case UTIL_KEY_NONE:
			break;

			default:
				//Caso entre a-z y 0-9
			if (
                        (tecla>='a' && tecla<='z') ||
                        (tecla>='0' && tecla<='9')
			)
				{
					convert_numeros_letras_puerto_teclado_continue_after_recreated(tecla,pressrelease);
				}
			break;




	}
}



//Retorna numero parseado. Si acaba en H, se supone que es hexadecimal
//Si acaba en
//Si empieza por ' o "" es un caracter (1 solo caracter, no un string)
//Valor de retorno unsigned de 32 bits
//Da tipo valor segun:
/*
enum token_parser_formato {
	TPF_DECIMAL,
	TPF_HEXADECIMAL,
	TPF_BINARIO,
	TPF_ASCII
}; */

unsigned int parse_string_to_number_get_type(char *texto,enum token_parser_formato *tipo_valor)
{
	int value;

        //Asumimos decimal
        *tipo_valor=TPF_DECIMAL;

	int l=strlen(texto);
	if (l==0) return 0;


	//Si empieza por ' o ""
	if (texto[0]=='\'' || texto[0]=='"') {
                *tipo_valor=TPF_ASCII;
                return texto[1];
        }

	//sufijo. Buscar ultimo caracter antes de final de cadena o espacio. Asi podemos parsear cosas como "20H 32 34", y se interpretara solo el 20H
        int posicion_sufijo=0;
        for (;texto[posicion_sufijo]!=0 && texto[posicion_sufijo]!=' ';posicion_sufijo++);
        posicion_sufijo--;

  //int posicion_sufijo=l-1;


	char sufijo=texto[posicion_sufijo];
	if (sufijo=='H' || sufijo=='h') {
		//hexadecimal
		//quitamos sufijo y parseamos
		texto[posicion_sufijo]=0;
		value=strtol(texto, NULL, 16);
		//volvemos a dejar sufijo tal cual
		texto[posicion_sufijo]=sufijo;

                *tipo_valor=TPF_HEXADECIMAL;
		return value;
	}

        if (sufijo=='%') {
		//binario
		//quitamos sufijo y parseamos
		texto[posicion_sufijo]=0;
		value=strtol(texto, NULL, 2);
		//volvemos a dejar sufijo tal cual
		texto[posicion_sufijo]=sufijo;

                *tipo_valor=TPF_BINARIO;
		return value;
	}

	//decimal
        return atoi(texto);

}


//Retorna numero parseado. Si acaba en H, se supone que es hexadecimal
//Si acaba en
//Si empieza por ' o "" es un caracter (1 solo caracter, no un string)
//Valor de retorno unsigned de 32 bits
unsigned int parse_string_to_number(char *texto)
{
        enum token_parser_formato tipo_valor;
        return parse_string_to_number_get_type(texto,&tipo_valor);
}

/*int ascii_to_hexa(char c)
{
  if (c>='0' && c<='9') return c-'0';
  if (c>='a' && c<='f') return c-'a'+10;
  if (c>='A' && c<='F') return c-'A'+10;
  return 0;
}*/

//Retorna numero grande parseado. Si acaba en H, se supone que es hexadecimal
//Si empieza por ' o "" es un caracter (1 solo caracter, no un string)
/*//Valor de retorno unsigned de 64 bits
long long int parse_string_to_long_number(char *texto)
{
	long long int value;

  while (*texto && *texto!=' ') {
    printf("t: [%c]",*texto);
    value=value*16;
    value +=ascii_to_hexa(*texto);
    texto++;
  }

  return value;


}*/


//carpeta temporal usada por el emulador
char emulator_tmpdir[PATH_MAX]="";

//carpeta temporal que ha especificado el usuario
char emulator_tmpdir_set_by_user[PATH_MAX]="";

//Retorna directorio temporal para el usuario y lo crea
//Formato: /tmp/zesarux-<uid>
//donde <uid> es el uid del usuario
char *get_tmpdir_base(void)
{

	//Si lo ha cambiado el usuario
	if (emulator_tmpdir_set_by_user[0]!=0) strcpy(emulator_tmpdir,emulator_tmpdir_set_by_user);


	if (emulator_tmpdir[0]==0) {

#ifndef MINGW
		int uid=getuid();
		sprintf (emulator_tmpdir,"/tmp/zesarux-%d",uid);
#else
		//Obtener ruta temporal raiz c:\windows
		char windows_temp[PATH_MAX];

		char *env_tmp,*env_temp,*env_userprofile,*env_systemroot;
        	env_tmp=getenv("TMP");
        	env_temp=getenv("TEMP");
		env_userprofile=getenv("USERPROFILE");
		env_systemroot=getenv("SystemRoot");


		//Ir probando uno a uno
		if (env_tmp!=NULL) sprintf (windows_temp,"%s",env_tmp);
		else if (env_temp!=NULL) sprintf (windows_temp,"%s",env_temp);
		else if (env_userprofile!=NULL) sprintf (windows_temp,"%s",env_userprofile);
		else if (env_systemroot!=NULL) sprintf (windows_temp,"%s",env_systemroot);
		else {
			//como ultima instancia, c:\windows\temp
			sprintf (windows_temp,"%s","C:\\windows\\temp\\");
		}

		debug_printf (VERBOSE_DEBUG,"Windows Temporary dir: %s",windows_temp);

		//Obtener un uid unico para usuario
		char template_dir[PATH_MAX];
		sprintf (template_dir,"%s\\zesarux-XXXXXX",windows_temp);

		char *dir;
		dir=mkdtemp(template_dir);

                if (dir==NULL) {
                        debug_printf (VERBOSE_DEBUG,"Error getting temporary directory: %s",strerror(errno) );
                }
                else {
                        sprintf(emulator_tmpdir,"%s",dir);
			debug_printf (VERBOSE_DEBUG,"ZEsarUX Temporary dir: %s",emulator_tmpdir);
                }

#endif


		debug_printf (VERBOSE_INFO,"Creating new temporary directory %s",emulator_tmpdir);


#ifndef MINGW
	      mkdir(emulator_tmpdir,S_IRWXU);
#else
	      mkdir(emulator_tmpdir);
#endif


	}

	return emulator_tmpdir;
}


//Retorna 0 si ok
//1 si error al abrir archivo
//Tiene en cuenta zonas de memoria
int save_binary_file(char *filename,int valor_leido_direccion,int valor_leido_longitud)
{


	menu_debug_set_memory_zone_attr();

        char zone_name[MACHINE_MAX_MEMORY_ZONE_NAME_LENGHT+1];
	menu_get_current_memory_zone_name_number(zone_name);

        if (valor_leido_longitud==0) valor_leido_longitud=menu_debug_memory_zone_size;


	debug_printf(VERBOSE_INFO,"Saving %s file at %d address at zone %s with %d bytes",filename,valor_leido_direccion,zone_name,valor_leido_longitud);

	FILE *ptr_binaryfile_save;
	ptr_binaryfile_save=fopen(filename,"wb");
	if (!ptr_binaryfile_save) {

		debug_printf (VERBOSE_ERR,"Unable to open Binary file %s",filename);
		return 1;
	}

	else {

		int escritos=1;
		z80_byte byte_leido;
		while (valor_leido_longitud>0 && escritos>0) {

			byte_leido=menu_debug_get_mapped_byte(valor_leido_direccion);

			escritos=fwrite(&byte_leido,1,1,ptr_binaryfile_save);
			valor_leido_direccion++;
			valor_leido_longitud--;
		}


		fclose(ptr_binaryfile_save);
	}

	return 0;

}

//Retorna 0 si ok
//1 si archivo no encontrado
//2 si error leyendo
//Tiene en cuenta zonas de memoria
int load_binary_file(char *filename,int valor_leido_direccion,int valor_leido_longitud)
{
	int returncode=0;

	if (!si_existe_archivo(filename)) return 1;

	if (valor_leido_longitud==0) valor_leido_longitud=4194304; //4 MB max


	menu_debug_set_memory_zone_attr();


	char zone_name[MACHINE_MAX_MEMORY_ZONE_NAME_LENGHT+1];
	menu_get_current_memory_zone_name_number(zone_name);

	debug_printf(VERBOSE_INFO,"Loading %s file at %d address at zone %s with maximum %d bytes",filename,valor_leido_direccion,zone_name,valor_leido_longitud);


	FILE *ptr_binaryfile_load;
	ptr_binaryfile_load=fopen(filename,"rb");
	if (!ptr_binaryfile_load) {

		debug_printf (VERBOSE_ERR,"Unable to open Binary file %s",filename);
		returncode=2;

	}

	else {

		int leidos=1;
		z80_byte byte_leido;
		while (valor_leido_longitud>0 && leidos>0) {
			leidos=fread(&byte_leido,1,1,ptr_binaryfile_load);
			if (leidos>0) {

					menu_debug_write_mapped_byte(valor_leido_direccion,byte_leido);

					valor_leido_direccion++;
					valor_leido_longitud--;
			}
		}


		fclose(ptr_binaryfile_load);

	}

    return returncode;

}






//extern char *mostrar_footer_game_name;

//Opciones para configuracion custom de archivo cinta/snap
void parse_customfile_options(void)
{

	//Si se ha leido algun parametro de --joystickkeybt o --joystickkeyev
	//Cuando se lee el primero, se inicializa la tabla a 0
	int leido_config_joystick_a_key=0;

        while (!siguiente_parametro()) {

		debug_printf (VERBOSE_DEBUG,"Parsing setting %s",argv[puntero_parametro]);

                if (!strcmp(argv[puntero_parametro],"--realvideo")) {
                                enable_rainbow();
                }

		else if (!strcmp(argv[puntero_parametro],"--programname")) {
			siguiente_parametro_argumento();
			mostrar_footer_game_name=argv[puntero_parametro];
		}

		//Para que aparezca --programsettingsinfo tiene que haber --programname
		else if (!strcmp(argv[puntero_parametro],"--programsettingsinfo")) {
                        siguiente_parametro_argumento();
                        mostrar_footer_second_message=argv[puntero_parametro];
                }


		else if (!strcmp(argv[puntero_parametro],"--disableborder")) {
			debug_printf(VERBOSE_INFO,"End Screen");
	//Guardar funcion de texto overlay activo, para desactivarlo temporalmente. No queremos que se salte a realloc_layers simultaneamente,
	//mientras se hace putpixel desde otro sitio -> provocaria escribir pixel en layer que se esta reasignando
  void (*previous_function)(void);
  int menu_antes;

	screen_end_pantalla_save_overlay(&previous_function,&menu_antes);


			disable_border();
			screen_init_pantalla_and_others_and_realjoystick();
                        screen_restart_pantalla_restore_overlay(previous_function,menu_antes);
			debug_printf(VERBOSE_INFO,"Creating Screen");
		}

                else if (!strcmp(argv[puntero_parametro],"--enableborder")) {
                        debug_printf(VERBOSE_INFO,"End Screen");

	//Guardar funcion de texto overlay activo, para desactivarlo temporalmente. No queremos que se salte a realloc_layers simultaneamente,
	//mientras se hace putpixel desde otro sitio -> provocaria escribir pixel en layer que se esta reasignando
  void (*previous_function)(void);
  int menu_antes;

	screen_end_pantalla_save_overlay(&previous_function,&menu_antes);

                        enable_border();
			screen_init_pantalla_and_others_and_realjoystick();
                        screen_restart_pantalla_restore_overlay(previous_function,menu_antes);
                        debug_printf(VERBOSE_INFO,"Creating Screen");
                }

		else if (!strcmp(argv[puntero_parametro],"--enableulaplus")) {
			enable_ulaplus();
		}

		else if (!strcmp(argv[puntero_parametro],"--disableulaplus")) {
			disable_ulaplus();
		}
		else if (!strcmp(argv[puntero_parametro],"--enabletimexvideo")) {
                        enable_timex_video();
		}

		else if (!strcmp(argv[puntero_parametro],"--disabletimexvideo")) {
                        disable_timex_video();
		}

		else if (!strcmp(argv[puntero_parametro],"--redefinekey")) {
				z80_byte tecla_original, tecla_redefinida;
                                siguiente_parametro_argumento();
                                tecla_original=parse_string_to_number(argv[puntero_parametro]);

                                siguiente_parametro_argumento();
                                tecla_redefinida=parse_string_to_number(argv[puntero_parametro]);

                                if (util_add_redefinir_tecla(tecla_original,tecla_redefinida)) {
					//Error volver sin leer mas parametros
                                        return;
                                }
		}

		else if (!strcmp(argv[puntero_parametro],"--clearredefinekey")) {
			clear_lista_teclas_redefinidas();
		}



		else if (!strcmp(argv[puntero_parametro],"--joystickemulated")) {
			siguiente_parametro_argumento();
			if (realjoystick_set_type(argv[puntero_parametro])) {
			//Error. Volver sin leer mas parametros
				return;
			}

		}

		else if (!strcmp(argv[puntero_parametro],"--joystickevent")) {
                                char *text_button;
                                char *text_event;

                                //obtener boton
                                siguiente_parametro_argumento();
                                text_button=argv[puntero_parametro];

                                //obtener evento
                                siguiente_parametro_argumento();
                                text_event=argv[puntero_parametro];

                                //Y definir el evento
                                if (realjoystick_set_button_event(text_button,text_event)) {
					//Error. Volver sin leer mas parametros
                                        return;
                                }


		}

		else if (!strcmp(argv[puntero_parametro],"--joystickkeybt")) {

				if (leido_config_joystick_a_key==0) {
					//Vaciar tabla
					realjoystick_clear_keys_array();
					//Puntero a 0
					joystickkey_definidas=0;
					//Decir que para las siguientes no se borra
					leido_config_joystick_a_key=1;
				}

                                char *text_button;
                                char *text_key;

                                //obtener boton
                                siguiente_parametro_argumento();
                                text_button=argv[puntero_parametro];

                                //obtener tecla
                                siguiente_parametro_argumento();
                                text_key=argv[puntero_parametro];

                                //Y definir el evento
                                if (realjoystick_set_button_key(text_button,text_key)) {
					//Error. Volver sin leer mas parametros
                                        return;
                                }

		}

                else if (!strcmp(argv[puntero_parametro],"--joystickkeyev")) {

                                if (leido_config_joystick_a_key==0) {
                                        //Vaciar tabla
                                        realjoystick_clear_keys_array();
                                        //Puntero a 0
                                        joystickkey_definidas=0;
                                        //Decir que para las siguientes no se borra
                                        leido_config_joystick_a_key=1;
                                }

                                char *text_event;
                                char *text_key;

                                //obtener boton
                                siguiente_parametro_argumento();
                                text_event=argv[puntero_parametro];

                                //obtener tecla
                                siguiente_parametro_argumento();
                                text_key=argv[puntero_parametro];

                                //Y definir el evento
                                if (realjoystick_set_event_key(text_event,text_key)) {
					//Error. Volver sin leer mas parametros
                                        return;
                                }

                }

                else if (!strcmp(argv[puntero_parametro],"--cleareventlist")) {
			realjoystick_clear_events_array();
		}


		else if (!strcmp(argv[puntero_parametro],"--frameskip")) {
                                siguiente_parametro_argumento();
                                int valor=atoi(argv[puntero_parametro]);
                                if (valor<0 || valor>50) {
                                        debug_printf (VERBOSE_ERR,"Invalid frameskip value");
                                        return;
                                }
                                frameskip=valor;
                }

		else if (!strcmp(argv[puntero_parametro],"--aychip")) {
                                ay_chip_present.v=1;
		}



		//
		//Opcion no reconocida. Error
		//
                else {
                        debug_printf (VERBOSE_ERR,"Setting %s not allowed on custom config file",argv[puntero_parametro]);
                        return;
                }
        }


}

void customconfig_help(void)
{
	printf ("Custom config help:\n"
		"\n"
		"Custom config files are configuration files for every program or game you want.\n"
		"When you load a tape/snapshot, the emulator will search for a file, identical name of the file you load,\n"
		"but adding extension .config, and it will apply every parameter contained in the file.\n"
		"\n"
		"So, if you load for example: game.tap, it will be searched a file game.tap.config\n"
		"This config file is a text file you can edit; the format is like the " DEFAULT_ZESARUX_CONFIG_FILE " main configuration file,\n"
		"but not every command line parameter allowed on main configuration file is allowed on a custom config file.\n"
		"There are some .config files included in this emulator, you can view them to have some examples."
		"\n"
		"The following are the allowed parameters on a custom config file, we don't describe the parameters that are the same as on command line:\n"
		"\n"

	"--machine id \n"
	"--programname text          Set the name of the tape/snapshot, shown on footer\n"
	"--programsettingsinfo text  Set the text for settings information, shown on footer\n"
	"--frameskip n\n"
	"--disableborder\n"
	"--enableborder              Enable border\n"
	"--aychip                    Enable AY-Chip\n"
	"--realvideo\n"
	"--enableulaplus             Enable ULAplus video modes\n"
	"--disableulaplus            Disable ULAplus video modes\n"
	"--enabletimexvideo          Enable Timex video modes\n"
	"--disabletimexvideo         Disable Timex video modes\n"
	"--redefinekey src dest\n"
	"--clearredefinekey          Clear redefine key table\n"
	"--joystickemulated type\n"
	"--joystickevent but evt\n"
	"--joystickkeybt but key\n"
	"--joystickkeyev evt key\n"
	"--text-keyboard-add text\n"
        "--text-keyboard-length n\n"
        "--text-keyboard-finalspc\n"
	"--cleareventlist\n"

	"\n"
	);
}

//Nombres cortos de maquinas y sus id
struct s_machines_short_names_id {
        char machine_name[32];
        int machine_id;
};

//Finaliza con machine_id -1
struct s_machines_short_names_id machines_short_names_id[]={
   {"16k",0},
   {"48k",1},
   {"48ks",20},
   {"Inves",2},
   {"TK90X",3},
   {"TK90XS",4},
   {"TK95",5},
   {"128k",6},
   {"128ks",7},
   {"P2",8},
   {"P2F",9},
   {"P2S",10},

   {"P2A40",11},
   {"P2A41",12},
   {"P2AS",13},
   {"ZXUNO",14},
   {"Chloe140",15},
   {"Chloe280",16},
   {"TS2068",17},
   {"Prism",18},
   {"TBBlue",19},

   {"Pentagon",21},
   {"Chrome",MACHINE_ID_CHROME},
   {"BaseConf",MACHINE_ID_BASECONF},
   {"TSConf",MACHINE_ID_TSCONF},
   {"P340",MACHINE_ID_SPECTRUM_P3_40},
   {"P341",MACHINE_ID_SPECTRUM_P3_41},
   {"P3S",MACHINE_ID_SPECTRUM_P3_SPA},
   {"ZX80",120},
   {"ZX81",121},
   {"ACE",122},

   {"Z88",130},
   {"CPC464",MACHINE_ID_CPC_464},
   {"CPC4128",MACHINE_ID_CPC_4128},
   {"SAM",150},
   {"QL",160},
   {"MK14",MACHINE_ID_MK14_STANDARD},

   {"MSX1",MACHINE_ID_MSX1},
   {"COLECO",MACHINE_ID_COLECO},
   {"SVI318",MACHINE_ID_SVI_318},
   {"SVI328",MACHINE_ID_SVI_328},

   //Fin
   {"",-1}
};


//Devuelve -1 si desconocida
int get_machine_id_by_name(char *machine_name)
{

        int i=0;

        while (machines_short_names_id[i].machine_id>=0) {

                if (!strcasecmp(machines_short_names_id[i].machine_name,machine_name)) {
                        return machines_short_names_id[i].machine_id;
                }

                i++;
        }

        //no encontrado
        debug_printf (VERBOSE_ERR,"Unknown machine %s",machine_name);
        return -1;

}

//Devuelve 0 si ok
int set_machine_type_by_name(char *machine_name)
{

  int maquina=get_machine_id_by_name(machine_name);
  if (maquina==-1) return 1;

  current_machine_type=maquina;


	return 0;
}

//Esta es la funcion inversa de la anterior. Devuelve "" si no se sabe numero de maquina
void get_machine_config_name_by_number(char *machine_name,int machine_number)
{

        int i=0;

        while (machines_short_names_id[i].machine_id>=0) {

                if (machine_number==machines_short_names_id[i].machine_id) {
                        strcpy(machine_name,machines_short_names_id[i].machine_name);
                        return;
                }

                i++;
        }

        //no encontrado
        machine_name[0]=0;
}



//Alternativa a scandir para sistemas Mingw, que no implementan dicha funcion
int scandir_mingw(const char *dir, struct dirent ***namelist,
              int (*filter)(const struct dirent *),
              int (*compar)(const struct dirent **, const struct dirent **))
{

	#define MAX_ARCHIVOS_SCANDIR_MINGW 20000
        int archivos=0;

        //Puntero a cada archivo leido
        struct dirent *memoria_archivos;

        //Array de punteros.
        struct dirent **memoria_punteros;

        //Asignamos memoria
        memoria_punteros=malloc(sizeof(struct dirent *)*MAX_ARCHIVOS_SCANDIR_MINGW);


        if (memoria_punteros==NULL) {
                cpu_panic("Error allocating memory when reading directory");
        }

        *namelist=memoria_punteros;

        //int indice_puntero;


       struct dirent *dp;
       DIR *dfd;

       if ((dfd = opendir(dir)) == NULL) {
           debug_printf(VERBOSE_ERR,"Can't open directory %s", dir);
           return -1;
       }

       while ((dp = readdir(dfd)) != NULL) {

                debug_printf(VERBOSE_DEBUG,"scandir_mingw: file: %s",dp->d_name);

		if (filter(dp)) {

		        //Asignar memoria para ese fichero
		        memoria_archivos=malloc(sizeof(struct dirent));

		      if (memoria_archivos==NULL) {
                		cpu_panic("Error allocating memory when reading directory");
		        }

		        //Meter puntero
		        memoria_punteros[archivos]=memoria_archivos;

		        //Meter datos
		        memcpy(memoria_archivos,dp,sizeof( struct dirent ));
		        archivos++;

		        if (archivos>=MAX_ARCHIVOS_SCANDIR_MINGW) {
                		debug_printf(VERBOSE_ERR,"Error. Maximum files in directory reached: %d",MAX_ARCHIVOS_SCANDIR_MINGW);
		                return archivos;
		        }

		}


       }
       closedir(dfd);

	//lanzar qsort
	int (*funcion_compar)(const void *, const void *);

	funcion_compar=( int (*)(const void *, const void *)  )compar;

	qsort(memoria_punteros,archivos,sizeof( struct dirent *), funcion_compar);

        return archivos;
}


//Devuelve la letra en mayusculas
char letra_mayuscula(char c)
{
	if (c>='a' && c<='z') c=c-('a'-'A');
	return c;
}

//Devuelve la letra en minusculas
char letra_minuscula(char c)
{
        if (c>='A' && c<='Z') c=c+('a'-'A');
        return c;
}

//Convierte una string en minusculas
void string_a_minusculas(char *origen, char *destino)
{
	char letra;

	for (;*origen;origen++,destino++) {
		letra=*origen;
		letra=letra_minuscula(letra);
		*destino=letra;
	}

	*destino=0;
}

//Dice si ruta es absoluta. 1 si es absoluta. 0 si no
int si_ruta_absoluta(char *ruta)
{

#ifdef MINGW
	//En Windows
	//Si empieza por '\' es absoluta
        if (ruta[0]=='\\') return 1;

	//Si empieza por letra unidad: es absoluta
	if (strlen(ruta)>2) {
		//Solo mirar los :
		if (ruta[1]==':') return 1;
	}

        return 0;

#else
	//En Unix
	//Si empieza por '/' es absoluta
	if (ruta[0]=='/') return 1;
	else return 0;
#endif

}

//Retorna tipo de archivo segun valor d_type
//0: desconocido
//1: archivo normal (o symbolic link)
//2: directorio


int get_file_type_from_stat(struct stat *f)
{
  if (f->st_mode & S_IFDIR) return 2;
  else return 1;
}


//0 desconocido o inexistente
//1 normal
//2 directorio
int get_file_type_from_name(char *nombre)
{
  struct stat buf_stat;

          if (stat(nombre, &buf_stat)!=0) {
                  debug_printf(VERBOSE_INFO,"Unable to get status of file %s",nombre);
return 0;
          }

          else {
//printf ("file size: %ld\n",buf_stat.st_size);
return get_file_type_from_stat(&buf_stat);
          }
}

int file_is_directory(char *nombre)
{
    if (get_file_type_from_name(nombre)==2) return 1;
    else return 0;
}

//Retorna fecha de un archivo en valores de punteros
//Devuelve 1 si error
//anyo tal cual: 2017, etc
int get_file_date_from_stat(struct stat *buf_stat,int *hora,int *minuto,int *segundo,int *dia,int *mes,int *anyo)
{



          struct tm *foo;

#if defined(__APPLE__)
          struct timespec *d;
          d=&buf_stat->st_mtimespec;
          //foo = gmtime((const time_t *)d);
          foo = localtime((const time_t *)d);
#else
          time_t *d;
          d=&buf_stat->st_mtime;
          //foo = gmtime((const time_t *)d);
          foo = localtime((const time_t *)d);
#endif

//printf("Year: %d\n", foo->tm_year);
//printf("Month: %d\n", foo->tm_mon);
//printf("Day: %d\n", foo->tm_mday);
//printf("Hour: %d\n", foo->tm_hour);
//printf("Minute: %d\n", foo->tm_min);
//printf("Second: %d\n", foo->tm_sec);

*hora=foo->tm_hour;
*minuto=foo->tm_min;
*segundo=foo->tm_sec;

*dia=foo->tm_mday;
*mes=foo->tm_mon+1;
*anyo=foo->tm_year+1900;

          return 0;


}

//Retorna fecha de un archivo en valores de punteros
//Devuelve 1 si error
//anyo tal cual: 2017, etc
int get_file_date_from_name(char *nombre,int *hora,int *minuto,int *segundo,int *dia,int *mes,int *anyo)
{
    struct stat buf_stat;

    if (stat(nombre, &buf_stat)!=0) {
        debug_printf(VERBOSE_INFO,"Unable to get status of file %s",nombre);
        return 1;
    }


    get_file_date_from_stat(&buf_stat,hora,minuto,segundo,dia,mes,anyo);


    return 0;


}




//Retorna tipo de archivo segun valor d_type
//Funcion nueva que usa st_mode en vez de d_type. d_type no valia para Windows ni para Haiku
//0: desconocido
//1: archivo normal (o symbolic link)
//2: directorio
//Entrada: d_type, nombre archivo. requisito es que el archivo se encuentre en directorio actual


int get_file_type(char *nombre)
{
/*
       lowing macro constants for the value returned in d_type:

       DT_BLK      This is a block device.

       DT_CHR      This is a character device.

       DT_DIR      This is a directory.

       DT_FIFO     This is a named pipe (FIFO).

       DT_LNK      This is a symbolic link.

       DT_REG      This is a regular file.

       DT_SOCK     This is a UNIX domain socket.

       DT_UNKNOWN  The file type is unknown.

*/



    //TODO: d_type no lo estamos usando. Se podria eliminar su uso



    struct stat buf_stat;

    if (stat(nombre, &buf_stat)==0) {
		debug_printf (VERBOSE_DEBUG,"Name: %s st_mode: %d constants: S_IFDIR: %d",nombre,buf_stat.st_mode,S_IFDIR);

		if (buf_stat.st_mode & S_IFDIR) return 2;
		else return 1;
	}


	//desconocido
    return 0;



}


void convert_relative_to_absolute(char *relative_path,char *final_path)
{
	//Directorio actual
	char directorio_actual[PATH_MAX];
	getcwd(directorio_actual,PATH_MAX);

	//Cambiamos a lo que dice el path relativo
	menu_filesel_chdir(relative_path);

	//Y en que directorio acabamos?
	getcwd(final_path,PATH_MAX);

	//Volver a directorio inicial
	menu_filesel_chdir(directorio_actual);

	//printf ("Convert relative to absolute path: relative: %s absolute: %s\n",relative_path,final_path);

	debug_printf (VERBOSE_DEBUG,"Convert relative to absolute path: relative: %s absolute: %s",relative_path,final_path);

}

int util_read_file_lines(char *memoria,char **lineas,int max_lineas)
{

	int linea=0;
	//char letra;

	while (linea<max_lineas && *memoria!=0) {
		lineas[linea++]=memoria;

		while (*memoria!='\n' && *memoria!='\r' && *memoria!=0) {
			//printf ("1 %d %c ",linea,*memoria);
			memoria++;
		}

		//meter un 0
		if (*memoria!=0) {
			*memoria=0;
			memoria++;
		}

		//saltar saltos de lineas sobrantes
		while ( (*memoria=='\n' || *memoria=='\r') && *memoria!=0) {
			//printf ("2 %d %c ",linea,*memoria);
			memoria++;
		}

		//printf (" memoria %p\n",memoria);
	}

	return linea;

}

int util_read_line_fields(char *memoria,char **campos)
{
	int campo_actual=0;
	while (*memoria!=0) {
		campos[campo_actual++]=memoria;

		while (*memoria!=' ' && *memoria!=0) {
			memoria++;
		}

                //meter un 0
                if (*memoria!=0) {
                        *memoria=0;
                        memoria++;
                }

		//saltar espacios adicionales
		while ( *memoria==' ' && *memoria!=0) {
			memoria++;
		}
	}

	return campo_actual;
}


int util_parse_pok_mem(char *memoria,struct s_pokfile **tabla_pokes)
{
/*
'N' means: this is the Next trainer,
'Y' means: this is the last line of the file (the rest of the file is ignored).
After the 'N' follows the name/description of this specific trainer. This string
may be up to 30 characters. There is no space between the 'N' and the string.
Emulator authors can use these strings to set up the selection entries.

The following lines, up to the next 'N' or 'Z' hold the POKEs to be applied for
this specific trainer. Again, the first character determines the content.
'M' means: this is not the last POKE (More)
'Z' means: this is the last POKE
The rest of a POKE line is built from

  bbb aaaaa vvv ooo

All values are decimal, separation is done by one or more spaces.

The field 'bbb' (128K memory bank) is built from
  bit 0-2 : bank value
        3 : ignore bank (1=yes, always set for 48K games)



Ejemplo:
NImmunity
M  8 44278  58   0
Z  8 44285  58   0
Y
*/


/*
Estructura:

int indice_accion: numero de orden de la accion poke. 0 para la primera, 1 para la segunda, etc
Un poke de inmunidad que sean dos pokes, tendran este numero igual (y si es el primero, sera 0)
char texto[40]: lo que hace ese poke
z80_byte banco;
z80_int direccion;
z80_byte valor;
z80_byte valor_orig;

*/

	//int indice_accion=0;

	//char *lineas[MAX_LINEAS_POK_FILE];

        char **lineas;

        lineas=malloc(sizeof(char *) * MAX_LINEAS_POK_FILE);

        if (lineas==NULL) cpu_panic("Can not allocate memory for pok file reading");

	char ultimo_nombre_poke[MAX_LENGTH_LINE_POKE_FILE+1]="";

	int lineas_leidas=util_read_file_lines(memoria,lineas,MAX_LINEAS_POK_FILE);

	int i,j;

	int total_pokes=0;

	int final=0;

	int destino=0;

	for (i=0;i<lineas_leidas && final==0;i++) {
		debug_printf (VERBOSE_DEBUG,"line: %d text: %s",i,lineas[i]);
		char *campos[10];

		//Si linea pasa de 1024, limitar
		if (strlen(lineas[i]) >=1024 ) {
			debug_printf (VERBOSE_DEBUG,"Line %d too large. Truncating",i);
			lineas[i][1023]=0;
		}

		//copiar linea actual antes de trocear para recuperar luego nombre de poke en N si conviene

		char ultima_linea[1024];
		sprintf (ultima_linea,"%s",lineas[i]);

		int total_campos=util_read_line_fields(lineas[i],campos);
		for (j=0;j<total_campos;j++) {
			//printf ("campo %d : [%s] ",j,campos[j]);
			//Si campo excede maximo, truncar
			if ( strlen(campos[j]) > MAX_LENGTH_LINE_POKE_FILE) campos[j][MAX_LENGTH_LINE_POKE_FILE]=0;
		}

		//Casos
		switch (campos[0][0]) {
			case 'N':
				//Texto de accion puede ser de dos palabras
				strncpy(ultimo_nombre_poke,&ultima_linea[1],MAX_LENGTH_LINE_POKE_FILE);
                                ultimo_nombre_poke[MAX_LENGTH_LINE_POKE_FILE] = 0;
			break;

			case 'M':
			case 'Z':
				//printf ("i: %d puntero %p\n",i,&tabla_pokes[i]->indice_accion);
				tabla_pokes[destino]->indice_accion=total_pokes;
				sprintf (tabla_pokes[destino]->texto,"%s",ultimo_nombre_poke);
				if (total_campos>=4) {
					tabla_pokes[destino]->banco=atoi(campos[1]);
					tabla_pokes[destino]->direccion=atoi(campos[2]);
					tabla_pokes[destino]->valor=atoi(campos[3]);
				}

				if (total_campos>=5) {
					tabla_pokes[destino]->valor_orig=atoi(campos[4]);
				}

				//Si era final de poke de accion, incrementar
				if (campos[0][0]=='Z') total_pokes++;

				destino++;

			break;


			case 'Y':
				final=1;
			break;

			default:
				//printf ("error format\n");
				return -1;
			break;
		}

		if (!final) {

		}


		//printf ("\n");
	}

        free(lineas);

	//return lineas_leidas;
	return destino;

}


//Retorna numero elementos del array
int util_parse_pok_file(char *file,struct s_pokfile **tabla_pokes)
{
	char *mem;

        int max_size=(MAX_LINEAS_POK_FILE*MAX_LENGTH_LINE_POKE_FILE)-1; //1 para el 0 del final


        if (get_file_size(file) > max_size) {
                debug_printf (VERBOSE_ERR,"File too large");
                return 0;
        }

	mem=malloc(max_size);


                FILE *ptr_pok;
                ptr_pok=fopen(file,"rb");

                if (!ptr_pok) {
                        debug_printf (VERBOSE_ERR,"Unable to open file %s",file);
                        return 0;
                }


                int leidos=fread(mem,1,max_size,ptr_pok);

		//Fin de texto
		mem[leidos]=0;

		fclose(ptr_pok);


	int total=util_parse_pok_mem(mem,tabla_pokes);



	free(mem);

	return total;


}


//Usado en pok file
int util_poke(z80_byte banco,z80_int direccion,z80_byte valor)
{
		//ram_mem_table
		//Si banco >7, hacer poke normal
		if (banco>7) {
			debug_printf (VERBOSE_DEBUG,"util_spectrum_poke. pokeing address %d with value %d",direccion,valor);
			poke_byte_no_time(direccion,valor);
		}
		else {
			//Poke con banco de memoria
			direccion = direccion & 16383;
			z80_byte *puntero;
			puntero=ram_mem_table[banco]+direccion;
			debug_printf (VERBOSE_DEBUG,"util_spectrum_poke. pokeing bank %d address %d with value %d",banco,direccion,valor);
			*puntero=valor;
		}

	return 0;


}



//Busca archivo "archivo" en directorio, sin distinguir mayusculas/minusculas
//Si encuentra, lo mete en nombreencontrado, y devuelve no 0
//Si no encuentra, devuelve 0
int util_busca_archivo_nocase(char *archivo,char *directorio,char *nombreencontrado)
{

	debug_printf (VERBOSE_DEBUG,"Searching file %s in directory %s",archivo,directorio);

	//Saltar todas las / iniciales del archivo
	while ( (*archivo=='/' || *archivo=='\\') && (*archivo!=0) ) {
		archivo++;
	}


	struct dirent *busca_archivo_dirent;
	DIR *busca_archivo_dir=NULL;



	busca_archivo_dir = opendir(directorio);


	if (busca_archivo_dir==NULL) {
		return 0;
        }

        do {

                busca_archivo_dirent = readdir(busca_archivo_dir);

                if (busca_archivo_dirent == NULL) {
                        closedir(busca_archivo_dir);
			return 0;
                }

		if (!strcasecmp(archivo,busca_archivo_dirent->d_name)) {
			//Encontrado
			sprintf (nombreencontrado,"%s",busca_archivo_dirent->d_name);
			debug_printf (VERBOSE_DEBUG,"Found file %s",nombreencontrado);
			return 1;
		}


        } while(1);

	//Aqui no llega nunca
	return 0;


}


//Funciones Spool Turbo para modelos Spectrum
void peek_byte_spoolturbo_check_key(z80_int dir)
{
	//si dir=23560, enviar tecla de spool file
	z80_int lastk=23560;

        if (input_file_keyboard_is_playing() && dir==lastk) {
	                        z80_byte input_file_keyboard_last_key;

                                int leidos=fread(&input_file_keyboard_last_key,1,1,ptr_input_file_keyboard);
                                if (leidos==0) {
                                        debug_printf (VERBOSE_INFO,"Read 0 bytes of Input File Keyboard. End of file");
                                        eject_input_file_keyboard();
                                        reset_keyboard_ports();
                                }

                                //conversion de salto de linea
                                if (input_file_keyboard_last_key==10) input_file_keyboard_last_key=13;

                                poke_byte_no_time(lastk,input_file_keyboard_last_key);


        }
}

//Punteros a las funciones originales
z80_byte (*peek_byte_no_time_no_spoolturbo)(z80_int dir);
z80_byte (*peek_byte_no_spoolturbo)(z80_int dir);

z80_byte peek_byte_spoolturbo(z80_int dir)
{

	peek_byte_spoolturbo_check_key(dir);

	return peek_byte_no_spoolturbo(dir);
}

z80_byte peek_byte_no_time_spoolturbo(z80_int dir)
{

        peek_byte_spoolturbo_check_key(dir);

        return peek_byte_no_time_no_spoolturbo(dir);
}




void set_peek_byte_function_spoolturbo(void)
{

        debug_printf(VERBOSE_INFO,"Enabling spoolturbo on peek_byte");


		//Cambiar valores de repeticion de teclas
		poke_byte_no_time(23561,1);
		poke_byte_no_time(23562,1);


        peek_byte_no_time_no_spoolturbo=peek_byte_no_time;
        peek_byte_no_time=peek_byte_no_time_spoolturbo;

        peek_byte_no_spoolturbo=peek_byte;
        peek_byte=peek_byte_spoolturbo;

}

void reset_peek_byte_function_spoolturbo(void)
{
	debug_printf(VERBOSE_INFO,"Resetting spoolturbo on peek_byte");

	//Restaurar valores de repeticion de teclas
	poke_byte_no_time(23561,35);
	poke_byte_no_time(23562,5);

        peek_byte_no_time=peek_byte_no_time_no_spoolturbo;
        peek_byte=peek_byte_no_spoolturbo;
}


//Funciones de Escritura en ROM
//Permitido en Spectrum 48k, Spectrum 16k, ZX80, ZX81, Jupiter Ace
//Punteros a las funciones originales
//void (*poke_byte_no_time_no_writerom)(z80_int dir,z80_byte value);
//void (*poke_byte_no_writerom)(z80_int dir,z80_byte value);


void poke_byte_writerom_exec(z80_int dir,z80_byte value)
{
		if (dir<16384) memoria_spectrum[dir]=value;

}

z80_byte poke_byte_writerom(z80_int dir,z80_byte value)
{

	poke_byte_writerom_exec(dir,value);

        //poke_byte_no_writerom(dir,value);
	debug_nested_poke_byte_call_previous(write_rom_nested_id_poke_byte,dir,value);

	//Para que no se queje el compilador
	return 0;

}

z80_byte poke_byte_no_time_writerom(z80_int dir,z80_byte value)
{

	poke_byte_writerom_exec(dir,value);

        //poke_byte_no_time_no_writerom(dir,value);
	debug_nested_poke_byte_no_time_call_previous(write_rom_nested_id_poke_byte_no_time,dir,value);

	//Para que no se queje el compilador
	return 0;
}




void set_poke_byte_function_writerom(void)
{

        debug_printf(VERBOSE_INFO,"Enabling Write on ROM on poke_byte");

        //poke_byte_no_time_no_writerom=poke_byte_no_time;
        //poke_byte_no_time=poke_byte_no_time_writerom;

        //poke_byte_no_writerom=poke_byte;
        //poke_byte=poke_byte_writerom;

        write_rom_nested_id_poke_byte=debug_nested_poke_byte_add(poke_byte_writerom,"Write rom poke_byte");
        write_rom_nested_id_poke_byte_no_time=debug_nested_poke_byte_no_time_add(poke_byte_no_time_writerom,"Write rom poke_byte_no_time");

}

void reset_poke_byte_function_writerom(void)
{
        debug_printf(VERBOSE_INFO,"Resetting Write on ROM on poke_byte");
        //poke_byte_no_time=poke_byte_no_time_no_writerom;
        //poke_byte=poke_byte_no_writerom;

        debug_nested_poke_byte_del(write_rom_nested_id_poke_byte);
        debug_nested_poke_byte_no_time_del(write_rom_nested_id_poke_byte_no_time);

}

//Retorna nombre de cinta spectrum (10 bytes)
void util_tape_get_name_header(z80_byte *tape,char *texto)
{
	int i;
	z80_byte caracter;

	for (i=0;i<10;i++) {
		caracter=*tape++;
		if (caracter<32 || caracter>126) caracter='.';

		*texto++=caracter;
	}

	*texto=0;
}


void util_tape_get_info_tapeblock(z80_byte *tape,z80_byte flag,z80_int longitud,char *texto)
{
	char buffer_nombre[11];

	z80_byte first_byte=tape[0];
		if (flag==0 && first_byte<=3 && longitud==19) {
			//Posible cabecera
			util_tape_get_name_header(&tape[1],buffer_nombre);

                        z80_int cabecera_longitud;
                        z80_int cabecera_inicio;

                        cabecera_longitud=value_8_to_16(tape[12],tape[11]);
                        cabecera_inicio=value_8_to_16(tape[14],tape[13]);



			//Evitar el uso del caracter ":" para evitar generar nombres (en el expansor de archivos) con ":" que pueden dar problemas en windows
			switch (first_byte) {
				case 0:
					sprintf(texto,"Program %s",buffer_nombre);
				break;

				case 1:
					sprintf(texto,"Num array %s",buffer_nombre);
				break;

				case 2:
					sprintf(texto,"Char array %s",buffer_nombre);
				break;

				case 3:
                                        if (cabecera_longitud==6912 && cabecera_inicio==16384) sprintf(texto,"Screen$ %s",buffer_nombre);
                                                          //01234567890123456789012345678901
                                                          //Code: 1234567890 [16384,49152]
					else sprintf(texto,"Code %s [%d,%d]",buffer_nombre,cabecera_inicio,cabecera_longitud);
				break;

			}
		}

		else if (flag==0 && first_byte==3 && longitud==36) {
			//Bloque de codigo fuente SPED
			util_tape_get_name_header(&tape[1],buffer_nombre);

                                     //01234567890123456789012345678901
                                     //SPED: 1234567890 (12/12/2099)
                        z80_byte sped_day=tape[18];
                        z80_byte sped_month=tape[19];
                        z80_byte sped_year=tape[20];

			sprintf(texto,"SPED %s (%d-%d-%d)",buffer_nombre,sped_day,sped_month,sped_year+1980);

		}


		else sprintf(texto,"Flag %d Length %d",flag,longitud-2); //Saltar los 2 bytes de flag y checksum
}

//Retorna texto descriptivo de informacion de cinta en texto. Cinta tipo tap
//Retorna longitud del bloque
int util_tape_tap_get_info(z80_byte *tape,char *texto)
{
	int longitud=value_8_to_16(tape[1],tape[0]);
	//Si longitud<2, es error
	int flag=tape[2];
	tape+=3;

	//char buffer_nombre[11];

	if (longitud<2) strcpy(texto,"Corrupt tape");
	else {
		util_tape_get_info_tapeblock(tape,flag,longitud,texto);


	}


	return longitud+2;
}


//Peek byte multiple para Z80 y Motorola
//Esta deberia ser la rutina preferida para llamarse desde menu y otros sitios
z80_byte peek_byte_z80_moto(unsigned int address)
{
  address=adjust_address_space_cpu(address);

  return peek_byte_no_time(address);
}

void poke_byte_z80_moto(unsigned int address,z80_byte valor)
{
  address=adjust_address_space_cpu(address);
  poke_byte_no_time(address,valor);
}

unsigned int get_pc_register(void)
{
  return reg_pc;
}

unsigned int adjust_address_space_cpu(unsigned int direccion)
{
  return direccion;
}


void convert_signed_unsigned(char *origen, unsigned char *destino,int longitud)
{
        int i;
        z80_byte leido;

        for (i=0;i<longitud;i++) {
                leido=*origen;
                leido=128+leido;

                *destino=leido;
                destino++;

                origen++;
        }

}

//Retorna 0 si ok
/*
Al descomprimir bloques de datos de RZX suele decir:
gunzip: invalid compressed data--crc error
Aunque descomprime bien. Sera porque en caso del RZX le agrego una cabecera manualmente
*/
int uncompress_gz(char *origen,char *destino)
{

  debug_printf(VERBOSE_INFO,"Uncompressing %s to %s",origen,destino);

//Descomprimir
char uncompress_command[PATH_MAX+18];
char uncompress_program[PATH_MAX];


sprintf (uncompress_program,"%s",external_tool_gunzip);
sprintf (uncompress_command,"cat %s | %s -c  > \"%s\" ",origen,external_tool_gunzip,destino);

if (system (uncompress_command)==-1) return 1;

return 0;
}

char *mingw_strcasestr(const char *arg1, const char *arg2)
{
   const char *a, *b;

   for(;*arg1;arg1++) {

     a = arg1;
     b = arg2;

     while((*a | 32) == (*b | 32)) {
	a++;
	b++;
       if(!*b)
         return (char *)arg1;
     }

   }

   return NULL;
}

char *util_strcasestr(char *string, char *string_a_buscar)
{

        //Debe coincidir desde el principio de string
#ifdef MINGW
        char *coincide=mingw_strcasestr(string,string_a_buscar);
#else
        char *coincide=strcasestr(string,string_a_buscar);
#endif

        return coincide;

}


void util_sprintf_address_hex(menu_z80_moto_int p,char *string_address)
{
    sprintf (string_address,"%04XH",p);
}



//Separar comando con codigos 0 y rellenar array de parametros
int util_parse_commands_argvc(char *texto, char *parm_argv[], int maximo)
{

        int args=0;

        while (*texto) {
                //Inicio parametro
                parm_argv[args++]=texto;
                if (args==maximo) {
                        debug_printf(VERBOSE_DEBUG,"Max parameters reached (%d)",maximo);
                        return args;
                }

                //Ir hasta espacio o final
                while (*texto && *texto!=' ') {
                        texto++;
                }

                if ( (*texto)==0) return args;

                *texto=0; //Separar cadena
                texto++;
        }

        return args;
}


//Separar comando con codigos 0 y rellenar array de parametros, parecido a
//util_parse_commands_argvc pero tienendo en cuenta si hay comillas
int util_parse_commands_argvc_comillas(char *texto, char *parm_argv[], int maximo)
{

        int args=0;

        //Si se han leido en algun momento al parsear un parametro
        int comillas_algun_momento=0;


        while (*texto) {
                //Inicio parametro
                parm_argv[args++]=texto;
                if (args==maximo) {
                        debug_printf(VERBOSE_DEBUG,"Max parameters reached (%d)",maximo);
                        return args;
                }

                //Ir hasta espacio o final

                comillas_algun_momento=0;

                //Variable que va conmutando segun se lee
                int comillas_leidas=0;

                int antes_escape=0;

                //TODO: al escapar comillas , incluye el caracter de escape tambien
                //esto sucede porque estamos escribiendo en el mismo sitio que leemos
                //deberia tener una cadena origen y una destino distintas

                //TODO: controlar parametros como : 123"456 -> en este caso resulta: 23"45

                while (*texto && (*texto!=' ' || comillas_leidas)  ) {
                        //Si son comillas y no escapadas
                        if ((*texto)=='"' && !antes_escape) {
                                //printf ("Leemos comillas\n");
                                comillas_leidas ^=1;

                                comillas_algun_momento=1;
                        }

                        if ( (*texto)=='\\') antes_escape=1;
                        else antes_escape=0;

                        texto++;
                }

                if (comillas_algun_momento) {
                        //Se ha leido en algun momento. Quitar comillas iniciales y finales
                        //Lo que hacemos es cambiar el inicio de parametro a +1

                        //printf ("Ajustar parametro porque esta entre comillas\n");

                        char *inicio_parametro;
                        inicio_parametro=parm_argv[args-1];

                        inicio_parametro++;

                        parm_argv[args-1]=inicio_parametro;

                        //Y quitar comillas del final
                        *(texto-1)=0;

                        //printf ("Parametro ajustado sin comillas: [%s]\n",parm_argv[args-1]);

                }

                if ( (*texto)==0) return args;



                *texto=0; //Separar cadena
                texto++;
        }

        return args;
}

//Retorna 0 si ok. No 0 si error. Ancho expresado en pixeles. Alto expresado en pixeles
//Source es en crudo bytes monocromos. ppb sera 8 siempre
int util_write_pbm_file(char *archivo, int ancho, int alto, int ppb, z80_byte *source)
{

  FILE *ptr_destino;
  ptr_destino=fopen(archivo,"wb");

  if (ptr_destino==NULL) {
    debug_printf (VERBOSE_ERR,"Error writing pbm file");
    return 1;
  }


    //Escribir cabecera pbm
  	char pbm_header[20]; //Suficiente

  	sprintf (pbm_header,"P4\n%d %0d\n",ancho,alto);

  	fwrite(pbm_header,1,strlen(pbm_header),ptr_destino);


  int x,y;
  for (y=0;y<alto;y++) {
    for (x=0;x<ancho/ppb;x++) {
      fwrite(source,1,1,ptr_destino);
      source++;
    }
  }

  fclose(ptr_destino);

  return 0;
}


//Retorna 0 si ok. No 0 si error. Ancho expresado en pixeles. Alto expresado en pixeles
//Source es en crudo bytes monocromos. ppb sera 8 siempre
int util_write_sprite_c_file(char *archivo, int ancho, int alto, int ppb, z80_byte *source)
{

  FILE *ptr_destino;
  ptr_destino=fopen(archivo,"wb");

  if (ptr_destino==NULL) {
    debug_printf (VERBOSE_ERR,"Error writing pbm file");
    return 1;
  }


    //Escribir cabecera pbm
        char *c_header="//Created by ZEsarUX emulator\n\nunsigned char mysprite[]={\n";
        char *c_end_header="};\n";

  	fwrite(c_header,1,strlen(c_header),ptr_destino);

          char buffer_linea[30]; // "0xFF," (5 caracteres. Damos mas por si acaso extendemos)


  int x,y;
  for (y=0;y<alto;y++) {
    for (x=0;x<ancho/ppb;x++) {
            sprintf (buffer_linea,"0x%02X,",*source);
        fwrite(buffer_linea,1,strlen(buffer_linea),ptr_destino);
      source++;
    }

    //Meter salto de linea cada cambio de posicion y
    char nl='\n';
    fwrite(&nl,1,1,ptr_destino);

  }

        //final de archivo
fwrite(c_end_header,1,strlen(c_end_header),ptr_destino);
  fclose(ptr_destino);

  return 0;
}


void util_truncate_file(char *filename)
{

	debug_printf(VERBOSE_INFO,"Truncating file %s",filename);

	FILE *ptr_destino;
	ptr_destino=fopen(filename,"wb");

  	if (ptr_destino==NULL) {
    		debug_printf (VERBOSE_ERR,"Error truncating file");
    		return;
    	}

    	fclose(ptr_destino);

}


//Retorna tamanyo de zona y actualiza puntero a memoria indicada
//Si es 0, no existe
//Attrib: bit 0: read, bit 1: write

unsigned int machine_get_memory_zone_attrib(int zone, int *readwrite)
{

  //Por defecto
  int size=0;

  //Zona 0, ram speccy
  switch (zone) {
    case 0:


      *readwrite=3; //1 read, 2 write

        size=2*1024*1024; //Retornamos siempre zona memoria 2 MB

    break;

    //Zona rom
    case 1:


      *readwrite=1; //1 read, 2 write
        size=8192;
    break;

    //diviface rom
    case 2:
      if (diviface_enabled.v) {
        *readwrite=1;
        size=DIVIFACE_FIRMWARE_KB*1024;
      }
    break;

    //diviface ram
    case 3:
      if (diviface_enabled.v) {
        *readwrite=3;
        size=(get_diviface_total_ram())*1024;
      }
    break;

    //Multiface rom
    case 12:
    	if (multiface_enabled.v) {
                *readwrite=1;
    		size=8192;
    	}
    break;

    //Multiface ram
    case 13:
    	if (multiface_enabled.v) {
                *readwrite=3;
    		size=8192;
    	}
    break;

    //tbblue sprites
    case 14:
        *readwrite=3;
        size=TBBLUE_SPRITE_ARRAY_PATTERN_SIZE;
    break;


	//memory zone by file. 16
    case MEMORY_ZONE_NUM_FILE_ZONE:
      if (memory_zone_by_file_size>0) {
        *readwrite=3;
        size=memory_zone_by_file_size;
      }
    break;

    //tbblue copper
    case MEMORY_ZONE_NUM_TBBLUE_COPPER:
        *readwrite=3;
        size=TBBLUE_COPPER_MEMORY;
    break;

        case MEMORY_ZONE_DEBUG:
                if (memory_zone_current_size) {
                        size=memory_zone_current_size;
                }
        break;


  }

  return size;

}


z80_byte *machine_get_memory_zone_pointer(int zone, int address)
{

  z80_byte *p=NULL;

  //Zona 0, ram speccy
  switch (zone) {
    case 0:
        p=&memoria_spectrum[address];

    break;


    //Zona 1, rom speccy
    case 1:
        p=&tbblue_fpga_rom[address];
    break;


    //diviface rom
    case 2:
      if (diviface_enabled.v) {
        p=&diviface_memory_pointer[address];
      }
    break;

    //diviface ram
    case 3:
      if (diviface_enabled.v) {
        p=&diviface_ram_memory_pointer[address];
      }
    break;
/*
 * TODO:
    //Multiface rom
    case 12:
    	if (multiface_enabled.v) {
    		p=&multiface_memory_pointer[address];
    	}
    break;

    //Multiface ram
    case 13:
    	if (multiface_enabled.v) {
    		p=&multiface_memory_pointer[address+8192];
    	}
    break;
*/

    //tbblue sprites
    case 14:
        p=tbsprite_new_patterns;
        p=p+address;
    break;

	//memory zone by file. 16
	case MEMORY_ZONE_NUM_FILE_ZONE:
		if (memory_zone_by_file_size>0) {
			p=&memory_zone_by_file_pointer[address];
		}
	break;

    //tbblue copper
    case MEMORY_ZONE_NUM_TBBLUE_COPPER:
        p=tbblue_copper_memory;
        p=p+address;
    break;

    case MEMORY_ZONE_DEBUG:
                if (memory_zone_current_size) {
                        p=&memory_zone_debug_ptr[address];
                }
        break;

  }

  return p;

}

subzone_info subzone_info_tbblue[]={
        {0x000000,0x00FFFF,"ZX Spectrum ROM"},
        {0x010000,0x011FFF,"DivMMC ROM"},
        {0x012000,0x013FFF,"Unused"},
        {0x014000,0x015FFF,"Multiface ROM"},
        {0x016000,0x017FFF,"Multiface RAM"},
        {0x018000,0x01BFFF,"AltROM 0"},
        {0x01C000,0x01FFFF,"AltROM 1"},
        {0x020000,0x03FFFF,"divMMC RAM"},
        {0x040000,0x05FFFF,"ZX Spectrum RAM"},
        {0x060000,0x07FFFF,"Extra RAM"},
        {0x080000,0x0FFFFF,"1st Extra IC RAM"},
        {0x100000,0x17FFFF,"2nd Extra IC RAM"},
        {0x180000,0xFFFFFF,"3rd Extra IC RAM"},
  {0,0,""}
};

//Busca la subzona de memoria en la tabla indicada, retorna indice
int machine_seach_memory_subzone_name(subzone_info *tabla,int address)
{
        int i;

        for (i=0;tabla[i].nombre[0]!=0;i++) if (address>=tabla[i].inicio && address<=tabla[i].fin) break;

        return i;
}


subzone_info *machine_get_memory_subzone_array(int zone, int machine_id)
{


  switch (machine_id) {
          case MACHINE_ID_TBBLUE:
                if (zone==0) {
                        return subzone_info_tbblue;

                }
          break;
  }

        return NULL;

}


//Maximo texto: 32 de longitud
void machine_get_memory_subzone_name(int zone, int machine_id, int address, char *name)
{

  //Por defecto
  strcpy(name,"");

        subzone_info *puntero;
        puntero=machine_get_memory_subzone_array(zone,machine_id);
        if (puntero==NULL) return;

        int indice=machine_seach_memory_subzone_name(puntero,address);
        strcpy(name,puntero[indice].nombre);

}




//Maximo texto: 15 de longitud

void machine_get_memory_zone_name(int zone, char *name)
{

  //Por defecto
  strcpy(name,"Unknown zone");

  //Zona 0, ram speccy
  switch (zone) {
    case 0:
		 //123456789012345
      strcpy(name,"Machine RAM");


    break;


    case 1:
		 //123456789012345
      strcpy(name,"Machine ROM");


    break;

    case 2:
      if (diviface_enabled.v) {
		   //123456789012345
        strcpy(name,"Diviface eprom");
      }
    break;

    case 3:
      if (diviface_enabled.v) {
		   //123456789012345
        strcpy(name,"Diviface ram");
      }
    break;

    //Multiface rom
    case 12:
    	if (multiface_enabled.v) {
    			   //123456789012345
		strcpy(name,"Multiface rom");
        }
    break;


    //Multiface ram
    case 13:
    	if (multiface_enabled.v) {
    			   //123456789012345
		strcpy(name,"Multiface ram");
        }
    break;

    case 14:
          		   //123456789012345
		strcpy(name,"TBBlue patterns");
    break;

	//memory zone by file. 16
	case MEMORY_ZONE_NUM_FILE_ZONE:
		if (memory_zone_by_file_size>0) {
          		   //123456789012345
		strcpy(name,"File zone");
		}
	break;

    //tbblue copper. 17
    case MEMORY_ZONE_NUM_TBBLUE_COPPER:
          		   //123456789012345
		strcpy(name,"TBBlue copper");
    break;


        case MEMORY_ZONE_DEBUG:
                if (memory_zone_current_size) {
                        strcpy(name,"Debug");
                }
        break;

  }


}



int machine_get_next_available_memory_zone(int zone)
{
  //Dado una zona actual, busca primera disponible. Si llega al final, retorna -1
  //char nombre[1024];
  int readwrite;

  do {
    //printf ("Zone: %d\n",zone);
    if (zone>=MACHINE_MAX_MEMORY_ZONES) return -1;
    unsigned int size=machine_get_memory_zone_attrib(zone,&readwrite);
    if (size>0) return zone;
    zone++;
  } while (1);

}

void util_delete(char *filename)
{
	unlink(filename);
}



//Extraido de http://rosettacode.org/wiki/CRC-32#C
z80_long_int util_crc32_calculation(z80_long_int crc, z80_byte *buf, size_t len)
{
        static z80_long_int table[256];
        static int have_table = 0;
        z80_long_int rem;
        z80_byte octet;
        int i, j;
        z80_byte *p, *q;

        /* This check is not thread safe; there is no mutex. */
        if (have_table == 0) {
                /* Calculate CRC table. */
                for (i = 0; i < 256; i++) {
                        rem = i;  /* remainder from polynomial division */
                        for (j = 0; j < 8; j++) {
                                if (rem & 1) {
                                        rem >>= 1;
                                        rem ^= 0xedb88320;
                                } else
                                        rem >>= 1;
                        }
                        table[i] = rem;
                }
                have_table = 1;
        }

        crc = ~crc;
        q = buf + len;
        for (p = buf; p < q; p++) {
                octet = *p;  /* Cast to unsigned octet. */
                crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
        }
        return ~crc;
}


//Retorna cuantos bits estan a 0 en un byte
int util_return_ceros_byte(z80_byte valor)
{
	int i;
	int ceros=0;
	for (i=0;i<8;i++) {
		if ((valor&1)==0) ceros++;

		valor=valor>>1;
	}

	return ceros;
}


void util_byte_to_binary(z80_byte value,char *texto)
{
	int i;

	for (i=0;i<8;i++) {
		if (value&128) *texto='1';
		else *texto='0';

		texto++;

		value=value<<1;
	}

	*texto=0; //fin cadena
}

void util_save_file(z80_byte *origin, long int tamanyo_origen, char *destination_file)
{


        FILE *ptr_destination_file;
        ptr_destination_file=fopen(destination_file,"wb");

                if (!ptr_destination_file) {
                        debug_printf (VERBOSE_ERR,"Can not open %s",destination_file);
                        return;
        }

        //Leer byte a byte... Si, es poco eficiente
        while (tamanyo_origen) {
        	fwrite(origin,1,1,ptr_destination_file);
                origin++;
        	tamanyo_origen--;
	}
        fclose(ptr_destination_file);

}


void util_copy_file(char *source_file, char *destination_file)
{

	long int tamanyo_origen=get_file_size(source_file);

	FILE *ptr_source_file;
        ptr_source_file=fopen(source_file,"rb");
        if (!ptr_source_file) {
                        debug_printf (VERBOSE_ERR,"Can not open %s",source_file);
                        return;
        }

        FILE *ptr_destination_file;
        ptr_destination_file=fopen(destination_file,"wb");

                if (!ptr_destination_file) {
                        debug_printf (VERBOSE_ERR,"Can not open %s",destination_file);
                        return;
        }

        z80_byte byte_buffer;

        //Leer byte a byte... Si, es poco eficiente
        while (tamanyo_origen) {
        	fread(&byte_buffer,1,1,ptr_source_file);
        	fwrite(&byte_buffer,1,1,ptr_destination_file);
        	tamanyo_origen--;
	}
        fclose(ptr_source_file);
        fclose(ptr_destination_file);


}


//Cambiar en cadena s, caracter orig por dest
void util_string_replace_char(char *s,char orig,char dest)
{
	while (*s) {
		if ( (*s)==orig) *s=dest;
		s++;
	}
}


//Agrega una cadena de texto a otra con salto de linea al final. Retorna longitud texto agregado contando salto de linea
int util_add_string_newline(char *destination,char *text_to_add)
{
	int longitud_texto=strlen(text_to_add)+1; //Agregar salto de linea
	sprintf (destination,"%s\n",text_to_add);
 	return longitud_texto;

}


//Agrega al texto "original" la cadena "string_to_add", comprobando que no se exceda el limite de longitud de original
//limite hay que pasarlo como el total del buffer (incluyendo el 0 final)
//Retorna 1 si no cabe
int util_concat_string(char *original,char *string_to_add,int limite)
{
        int longitud_original=strlen(original);

        int longitud_to_add=strlen(string_to_add);

        int longitud_final=longitud_original+longitud_to_add+1; //el +1 del byte 0 final

        if (longitud_final>limite) return 1;

        char *offset_add;
        offset_add=&original[longitud_original];
        strcpy(offset_add,string_to_add);

        return 0;
}

//De una cadena de bytes, lo muestra como hexadecimal, sin espacios. Retorna cadena acabada en 0
//Completa con espacios hasta longitud
void util_binary_to_hex(z80_byte *origen, char *destino, int longitud_max, int longitud)
{
	int i;

	for (i=0;i<longitud_max && i<longitud;i++) {
		sprintf(destino,"%02X",*origen);

		origen++;
		destino+=2;
	}

	for (;i<longitud_max;i++) {
		*destino=' ';
		destino++;
		*destino=' ';
		destino++;
	}

	*destino=0;
}



//De una cadena de bytes, lo muestra como ascii, sin espacios. si hay caracter no imprimible, muestra . . . Retorna cadena acabada en 0
//Completa con espacios
void util_binary_to_ascii(z80_byte *origen, char *destino, int longitud_max, int longitud)
{
	int i;
	z80_byte caracter;

	for (i=0;i<longitud_max && i<longitud;i++) {
		caracter=*origen;
		if (caracter<32 || caracter>126) caracter='.';
		*destino=caracter;

		origen++;
		destino++;
	}

	for (;i<longitud_max;i++) {
                *destino=' ';
        }

        *destino=0;
}

//Imprime numero binario de 32 bits acabado con prefijo "%"
//longitud_max maximo de la longitud de la cadena incluyendo "%"
//No imprimir ceros al inicio
void util_ascii_to_binary(int valor_origen,char *destino,int longitud_max)
{

        //Si es cero tal cual
        if (valor_origen==0 && longitud_max>=2) {
                strcpy(destino,"0%");
                return;
        }

        //Usamos sin signo por si acaso
        unsigned int valor=valor_origen;
        const unsigned int mascara=(unsigned int)2147483648;

        longitud_max--; //hemos de considerar prefijo final

        int nbit;
        int primer_uno=0;

        for (nbit=0;nbit<32 && longitud_max>0;nbit++) {
                //printf ("nbit %d valor %u\n",nbit,valor);
                if (valor & mascara) {
                        //printf ("primer uno en nbit %d valor %d\n",nbit,valor);
                        *destino='1';
                        primer_uno=1;
                }

                else {
                        if (primer_uno) *destino='0';
                }

                if (primer_uno) {
                        destino++;
                        longitud_max--;
                }

                valor=valor<<1;
        }



        *destino='%';
        destino++;

        *destino=0;

}




/*void util_file_save(char *filename,z80_byte *puntero, long int tamanyo);
{
	           FILE *ptr_filesave;
                                  ptr_filesave=fopen(filename,"wb");
                                  if (!ptr_filesave)
                                {
                                      debug_printf (VERBOSE_ERR,"Unable to open file %s",filename);

                                  }
                                else {

                                        fwrite(puntero,1,tamanyo,ptr_filesave);

                                  fclose(ptr_filesave);


                                }

}*/

void util_file_append(char *filename,z80_byte *puntero, int tamanyo)
{
                   FILE *ptr_filesave;
                                  ptr_filesave=fopen(filename,"ab");
                                  if (!ptr_filesave)
                                {
                                      debug_printf (VERBOSE_ERR,"Unable to open file %s",filename);

                                  }
                                else {

                                        fwrite(puntero,1,tamanyo,ptr_filesave);

                                  fclose(ptr_filesave);


                                }

}

/*
Funcion usada en compresion de datos
Dado un puntero de entrada, y la longitud del bloque, dice cuantas veces aparecer el primer byte, y que byte es
Retorna: numero de repeticiones (minimo 1) y el byte que es (modifica contenido puntero *byte_repetido)
*/

int util_get_byte_repetitions(z80_byte *memoria,int longitud,z80_byte *byte_repetido)
{
	int repeticiones=1;

	//Primer byte
	z80_byte byte_anterior;
	byte_anterior=*memoria;
	//printf ("byte anterior: %d\n",byte_anterior);

	memoria++;
	longitud--;

	while (longitud>0 && (*memoria)==byte_anterior) {
		//printf ("longitud: %d memoria: %p\n",longitud,memoria);
		repeticiones++;
		memoria++;
		longitud--;
	}

	*byte_repetido=byte_anterior;
	return repeticiones;

}

void util_write_repeated_byte(z80_byte *memoria,z80_byte byte_a_repetir,int longitud)
{
	while (longitud>0) {
		*memoria=byte_a_repetir;

		longitud--;
		memoria++;
	}
}


/*
Funcion de compresion de datos
Dado un puntero de entrada, la longitud del bloque, y el byte "magico" de repeticion (normalmente DD), y el puntero de salida, retorna esos datos comprimidos, mediante:
siendo XX el byte magico, cuando hay un bloque con repeticion (minimo 5 repeticiones) se retorna como:
XX XX YY ZZ, siendo YY el byte a repetir y ZZ el numero de repeticiones (0 veces significa 256 veces)
Si no hay repeticiones, se retornan los bytes tal cual
*casos a considerar:
**si en entrada hay byte XX repetido al menos 2 veces, se trata como repeticion, sin tener que llegar al minimo de 5
**si el bloque de salida ocupa mas que el de entrada (que hubiesen muchos XX XX en la entrada) , el proceso que llama aqui lo debe considerar, para dejar el bloque sin comprimir
**si se repite mas de 256 veces, trocear en varios

Retorna: longitud del bloque destino


Considerar caso DD 00 00 00 00 00 ...  -> genera DD    DD DD 00 5
Habria que ver al generar repeticion, si anterior era DD, hay que convertir el DD anterior en DD DD DD 01
Debe generar:
DD DD DD 01  DD DD 00 05

O sea, tenemos el DD anterior, agregamos DD DD 01

con zxuno.flash es uno de estos casos


Peor de los casos:

A)
DD DD 00  DD DD 00  ....  6 bytes
DD DD DD 02     DD DD DD 02   8 bytes

B)
DD 00 00 00 00 00   DD 00 00 00 00 00  ... 12 bytes
DD DD DD 01  DD DD 00 5    DD DD DD 01  DD DD 00 5   .... 16 bytes

consideramos lo peor que sea el doble

*/

int util_compress_data_repetitions(z80_byte *origen,z80_byte *destino,int longitud,z80_byte magic_byte)
{

	int longitud_destino=0;

	int antes_es_magic_aislado=0; //Si el de antes era un byte magic (normalmente DD) asilado

	while (longitud) {
		z80_byte byte_repetido;
		int repeticiones=util_get_byte_repetitions(origen,longitud,&byte_repetido);
		//printf ("Remaining size: %d Byte: %02X Repetitions: %d\n",longitud,byte_repetido,repeticiones);
		//if (longitud<0) exit(1);

		origen +=repeticiones;
		longitud -=repeticiones;

		//Hay repeticiones
		if (repeticiones>=5
			||
		(byte_repetido==magic_byte && repeticiones>1)
		) {
			//Escribir magic byte dos veces, byte a repetir, y numero repeticiones
			//Si repeticiones > 256, trocear
			while (repeticiones>0) {
				if (antes_es_magic_aislado) {
					destino[0]=magic_byte;
					destino[1]=magic_byte;
					destino[2]=1;

					destino +=3;
					longitud_destino +=3;
				}

				destino[0]=magic_byte;
				destino[1]=magic_byte;
				destino[2]=byte_repetido;
				z80_byte brep;
				if (repeticiones>255) brep=0;
				else brep=repeticiones;

				destino[3]=brep;

				//printf ("%d %02X %02X %02X %02X\n",longitud,magic_byte,magic_byte,byte_repetido,brep);

				destino +=4;
				longitud_destino +=4;

				repeticiones -=256;
			}

			antes_es_magic_aislado=0;
		}

		else {
			//No hay repeticiones
			if (repeticiones==1 && byte_repetido==magic_byte) antes_es_magic_aislado=1;
			else antes_es_magic_aislado=0;


			util_write_repeated_byte(destino,byte_repetido,repeticiones);
			//printf ("%d %02X(%d)\n",longitud,byte_repetido,repeticiones);

			destino +=repeticiones;
			longitud_destino +=repeticiones;



		}
	}

	return longitud_destino;

}



/*
Funcion de descompresion de datos
siendo XX el byte magico, cuando hay un bloque con repeticion (minimo 5 repeticiones) se retorna como:
XX XX YY ZZ, siendo YY el byte a repetir y ZZ el numero de repeticiones (0 veces significa 256 veces)

Retorna: longitud del bloque destino
*/

int util_uncompress_data_repetitions(z80_byte *origen,z80_byte *destino,int longitud,z80_byte magic_byte)
{
        int longitud_destino=0;

        while (longitud) {
		//Si primer y segundo byte son el byte magic
		int repeticion=0;
		if (longitud>=4) {
			if (origen[0]==magic_byte && origen[1]==magic_byte) {
				repeticion=1;

				z80_byte byte_a_repetir=origen[2];

				int longitud_repeticion;
				longitud_repeticion=origen[3];

				if (longitud_repeticion==0) longitud_repeticion=256;

				util_write_repeated_byte(destino,byte_a_repetir,longitud_repeticion);

				origen+=4;
				longitud-=4;

				destino+=longitud_repeticion;
				longitud_destino+=longitud_repeticion;
			}
		}

		if (!repeticion) {
			*destino=*origen;

			origen++;
			longitud--;

			destino++;
			longitud_destino++;
		}
	}

	return longitud_destino;
}



//Obtener coordenada x,y de una direccion de pantalla dada
//x entre 0..255 (aunque sera multiple de 8)
//y entre 0..191
void util_spectrumscreen_get_xy(z80_int dir,int *xdest,int *ydest)
{
        //De momento para ir rapido, buscamos direccion en array de scanline
        //screen_addr_table

        //dir -=16384;  //TODO: quiza en vez de hacer una resta, hacer un AND para quitar ese bit. Quiza incluso quitar todos los bits 15,14,13
        //asi quitaria offsets 32768, 16384 y 8192

        dir &=(65535-32768-16384-8192);


        int indice=0;
        int x,y;
        for (y=0;y<192;y++) {
                for (x=0;x<32;x++) {

                        if (dir==screen_addr_table[indice]) {
                                *xdest=x*8;
                                *ydest=y;
                                return;
                        }

                        indice++;
                }
        }

}


//Convierte una pantalla de tipo spectrum (con sus particulares direcciones) a un sprite
void util_convert_scr_sprite(z80_byte *origen,z80_byte *destino)
{
	int x,y;
	z80_byte *linea_origen;

	for (y=0;y<192;y++) {
		int offset_origen=screen_addr_table[y*32] & 8191;
		linea_origen=&origen[offset_origen];
		for (x=0;x<32;x++) {
			*destino=*linea_origen;
			destino++;
			linea_origen++;
		}
	}
}

//Devolver valor sin signo
int util_get_absolute(int valor)
{
        if (valor<0) valor=-valor;

        return valor;
}


//Devolver signo de valor
int util_get_sign(int valor)
{
	if (valor<0) return -1;

	return +1;
}

void util_save_game_config(char *filename)
{
  //Sobreescribimos archivos con settings actuales
  debug_printf (VERBOSE_INFO,"Writing configuration file");

  //Agregamos contenido
  char config_settings[20000]; //Esto es mas que suficiente

  char buffer_temp[MAX_CONFIG_SETTING]; //Para cadenas temporales
  //char buffer_temp2[MAX_CONFIG_SETTING]; //Para cadenas temporales

  int indice_string=0;

  int i;


  //Al menos generamos una linea en blanco inicial
                                              ADD_STRING_CONFIG,";Autogenerated autoconfig file");



  //Y empezamos a meter opciones

  get_machine_config_name_by_number(buffer_temp,current_machine_type);
  if (buffer_temp[0]!=0) {
                                              ADD_STRING_CONFIG,"--machine %s",buffer_temp);
  }

  if (frameskip)                              ADD_STRING_CONFIG,"--frameskip %d",frameskip);


  if (border_enabled.v==0)                    ADD_STRING_CONFIG,"--disableborder");


  if (rainbow_enabled.v)                      ADD_STRING_CONFIG,"--realvideo");




//Estas solo si es Spectrum
        if (ulaplus_presente.v)                     ADD_STRING_CONFIG,"--enableulaplus");
        if (timex_video_emulation.v)                ADD_STRING_CONFIG,"--enabletimexvideo");



					ADD_STRING_CONFIG,"--clearredefinekey");

                                        //Esto conviene meterlo en el .config, asi el usuario
                                        //sabe que sus botones a eventos se inicializan en el juego
                                        //y se establecen un poco mas abajo
                                        //en caso que no quisiera, que lo quite

					ADD_STRING_CONFIG,"--cleareventlist");


						ADD_STRING_CONFIG,"--joystickemulated \"%s\"",joystick_texto[joystick_emulation]);


  //real joystick buttons to events. Siempre este antes que el de events/buttons to keys
  for (i=0;i<MAX_EVENTS_JOYSTICK;i++) {
        if (realjoystick_events_array[i].asignado.v) {
                char texto_button[20];
                int button_type;
                button_type=realjoystick_events_array[i].button_type;

                util_write_config_aux_realjoystick(button_type, realjoystick_events_array[i].button, texto_button);

                ADD_STRING_CONFIG,"--joystickevent %s %s",texto_button,realjoystick_event_names[i]);
        }
  }


  //real joystick buttons to keys
  for (i=0;i<MAX_KEYS_JOYSTICK;i++) {
        if (realjoystick_keys_array[i].asignado.v) {
                char texto_button[20];
                int button_type;
                z80_byte caracter;
                caracter=realjoystick_keys_array[i].caracter;
                button_type=realjoystick_keys_array[i].button_type;

                //Si hay evento en vez de numero de boton, meter --joystickkeyev

                //printf ("Buscando evento para boton %d tipo %d\n",realjoystick_keys_array[i].button,button_type);
                int buscar_evento_index=realjoystick_buscar_evento_en_tabla(realjoystick_keys_array[i].button,button_type);
                if (buscar_evento_index>=0) {
                        //printf ("Encontrado evento %d para boton %d tipo %d\n",buscar_evento_index,realjoystick_keys_array[i].button,button_type);
                    ADD_STRING_CONFIG,"--joystickkeyev %s %d",realjoystick_event_names[buscar_evento_index],caracter);
                }

                else {
                        util_write_config_aux_realjoystick(button_type, realjoystick_keys_array[i].button, texto_button);
                        ADD_STRING_CONFIG,"--joystickkeybt %s %d",texto_button,caracter);
                }
        }
  }



         FILE *ptr_configfile;

     ptr_configfile=fopen(filename,"wb");
     if (!ptr_configfile) {
                        debug_printf(VERBOSE_ERR,"Cannot write configuration file %s",filename);
                        return;
      }

    fwrite(config_settings, 1, strlen(config_settings), ptr_configfile);


      fclose(ptr_configfile);


}


int util_find_right_most_space(char *texto)
{
        int i=strlen(texto)-1;

        for (;i>=0;i--) {
                if (texto[i]!=' ') return i;
        }

        return i;
}

void util_clear_final_spaces(char *orig,char *destination)
{
        //Quitar espacios al final del texto
        //Primero localizar primer espacio de la derecha

        int indice=0;

        int espacio_derecha=util_find_right_most_space(orig);

        if (espacio_derecha>=0) {
            for (indice=0;indice<=espacio_derecha && orig[indice];indice++) {
                    destination[indice]=orig[indice];
            }
        }

        destination[indice]=0;
}


int util_is_digit(char c)
{
        if (c>='0' && c<='9') return 1;
        return 0;
}


int util_get_available_drives(char *texto)
{
#ifdef MINGW
	int bitmask_unidades=GetLogicalDrives(); //Mascara de bits. bit inferior si unidad A disponible, etc
#else
	int bitmask_unidades=0;
#endif

	int unidades_detectadas=0;

	char letra_actual='A';

	for (;letra_actual<='Z';letra_actual++) {
		//Ver bit inferior
		if (bitmask_unidades&1) {
			//printf ("letra actual: %d unidades_detectadas: %d\n",letra_actual,unidades_detectadas);
			texto[unidades_detectadas]=letra_actual;
			unidades_detectadas++;
		}

		bitmask_unidades >>=1;
	}

	//y final de texto
	texto[unidades_detectadas]=0;


	return unidades_detectadas;
}

int get_cpu_frequency(void)
{
        int cpu_hz=screen_testados_total*50;

        return cpu_hz;
}


//Retorna 0 si no encontrado
int util_find_window_geometry(char *nombre,int *x,int *y,int *ancho,int *alto)
{
        int i;

        for (i=0;i<total_config_window_geometry;i++) {
                if (!strcasecmp(nombre,saved_config_window_geometry_array[i].nombre)) {
                        *x=saved_config_window_geometry_array[i].x;
                        *y=saved_config_window_geometry_array[i].y;
                        *ancho=saved_config_window_geometry_array[i].ancho;
                        *alto=saved_config_window_geometry_array[i].alto;
                        debug_printf (VERBOSE_DEBUG,"Returning window geometry %s from index %d, %d,%d %dX%d",
                        nombre,i,*y,*y,*ancho,*alto);
                        return 1;
                }
        }

        //Si no se encuentra, meter geometria por defecto
        *x=menu_origin_x();
        *y=0;
        *ancho=ZXVISION_MAX_ANCHO_VENTANA;
        *alto=ZXVISION_MAX_ALTO_VENTANA;
        debug_printf (VERBOSE_DEBUG,"Returning default window geometry for %s",nombre);
        return 0;
}

//Retorna 0 si error. Lo agrega si no existe. Si existe, lo modifica
int util_add_window_geometry(char *nombre,int x,int y,int ancho,int alto)
{

        int destino=total_config_window_geometry;
        int sustituir=0;

        int i;

        //Buscar si se encuentra, sustituir
        for (i=0;i<total_config_window_geometry;i++) {
                if (!strcasecmp(nombre,saved_config_window_geometry_array[i].nombre)) {
                        destino=i;
                        sustituir=1;
                        break;
                }
        }

        if (!sustituir) {
                if (total_config_window_geometry==MAX_CONFIG_WINDOW_GEOMETRY) {
                        debug_printf (VERBOSE_ERR,"Maximum window geometry config reached (%d)",MAX_CONFIG_WINDOW_GEOMETRY);
                        return 0;
                }
        }

        debug_printf (VERBOSE_DEBUG,"Storing window geometry at %d index array, name %s, %d,%d %dX%d",
                destino,nombre,x,y,ancho,alto);

        strcpy(saved_config_window_geometry_array[destino].nombre,nombre);
        saved_config_window_geometry_array[destino].x=x;
        saved_config_window_geometry_array[destino].y=y;
        saved_config_window_geometry_array[destino].ancho=ancho;
        saved_config_window_geometry_array[destino].alto=alto;

        if (!sustituir) total_config_window_geometry++;

        return 1;

}

//Misma funcion que la anterior pero indicando solo puntero a ventana
//El nombre lo obtiene de la estructura de zxvision_window
void util_add_window_geometry_compact(zxvision_window *ventana)
{

        char *nombre;

        nombre=ventana->geometry_name;
        if (nombre[0]==0) {
                //printf ("Trying to save window geometry but name is blank\n");
                return;
        }

        util_add_window_geometry(nombre,ventana->x,ventana->y,ventana->visible_width,ventana->visible_height);
}

void util_clear_all_windows_geometry(void)
{
        total_config_window_geometry=0;
}

//Agregar una letra en el string en la posicion indicada
void util_str_add_char(char *texto,int posicion,char letra)
{


        if (posicion<0) return; //error

        //Primero hacer hueco, desplazar todo a la derecha
        int longitud=strlen(texto);



        //Si se intenta meter mas alla de la posicion del 0 final
        if (posicion>longitud) {
                //printf ("intentando escribir mas alla del 0 final\n");
                posicion=longitud;
        }

        int i;


        //Meter un 0 despues del 0 ultimo
        texto[longitud+1]=0;

        for (i=longitud;i>posicion;i--) {
                texto[i]=texto[i-1];
        }

        texto[posicion]=letra;
}

//Quitar una letra en el string en la posicion indicada
void util_str_del_char(char *texto,int posicion)
{

         if (posicion<0) return; //error

        //desplazar todo a la izquierda
        int longitud=strlen(texto);

        if (longitud==0) return; //cadena vacia


        //Si se intenta borrar mas alla de la longitud
        if (posicion>=longitud) {
                //printf ("intentando borrar mas alla del final\n");
                posicion=longitud-1;
        }

        int i;


        for (i=posicion;i<longitud;i++) {
                texto[i]=texto[i+1];
        }

}


char util_printable_char(char c)
{
        if (c<32 || c>126) return '?';
        else return c;
}

//funcion para leer una linea desde origen, hasta codigo 10 NL
//se puede limitar max en destino
//retorna puntero a byte despues de salto linea
char *util_read_line(char *origen,char *destino,int size_orig,int max_size_dest,int *leidos)
{
	max_size_dest --;
	*leidos=0;
	for (;*origen && size_orig>0 && max_size_dest>0;origen++,size_orig--,(*leidos)--) {
		//ignorar cr
		if ( *origen=='\r' ) continue;
		if ( *origen=='\n' ) {
			origen++;
			break;
		}
		*destino=*origen;
		destino++;
		max_size_dest--;

	}
	*destino=0;
	return origen;
}

//Retorna el codigo http o <0 si otros errores
int util_download_file(char *hostname,char *url,char *archivo,int use_ssl,int estimated_maximum_size)
{
  int http_code;
	char *mem;
	char *orig_mem;
	char *mem_after_headers;
	int total_leidos;
	int retorno;
        char redirect_url[NETWORK_MAX_URL];



        retorno=zsock_http(hostname,url,&http_code,&mem,&total_leidos,&mem_after_headers,1,"",use_ssl,redirect_url,estimated_maximum_size);


        if (http_code==302 && redirect_url[0]!=0) {
                debug_printf (VERBOSE_DEBUG,"util_download_file: detected redirect to %s",redirect_url);
                //TODO: solo gestiono 1 redirect

                //obtener protocolo
                use_ssl=util_url_is_https(redirect_url);

                //obtener host
                char nuevo_host[NETWORK_MAX_URL];
                util_get_host_url(redirect_url,nuevo_host);

                //obtener nueva url (sin host)
                char nueva_url[NETWORK_MAX_URL];
                util_get_url_no_host(redirect_url,nueva_url);

                //liberar memoria anterior
                if (mem!=NULL) free(mem);

                debug_printf (VERBOSE_DEBUG,"util_download_file: querying again host %s (SSL=%d) url %s",nuevo_host,use_ssl,nueva_url);

                //El redirect sucede con las url a archive.org

                retorno=zsock_http(nuevo_host,nueva_url,&http_code,&mem,&total_leidos,&mem_after_headers,1,"",use_ssl,redirect_url,estimated_maximum_size);

	}


	orig_mem=mem;

	if (mem_after_headers!=NULL && http_code==200) {
		//temp limite
		//mem_after_headers[10000]=0;
		//menu_generic_message("Games",mem_after_headers);
		//char texto_final[30000];

		//int indice_destino=0;

		int dif_header=mem_after_headers-mem;
		total_leidos -=dif_header;
		mem=mem_after_headers;
		//grabar a disco
		//todo usar funcion de utils comun, existe?

		FILE *ptr_destino;
                ptr_destino=fopen(archivo,"wb");

                if (ptr_destino==NULL) {
                        debug_printf (VERBOSE_ERR,"Error writing output file");
                        return -1;
                }



  	        fwrite(mem_after_headers,1,total_leidos,ptr_destino);


                fclose(ptr_destino);
                free(orig_mem);
        }

        //Fin resultado http correcto
        else {
                if (retorno<0) {
                        //printf ("Error: %d %s\n",retorno,z_sock_get_error(retorno));
                        return retorno;
                }
                else {
                        //No hacemos VERBOSE_ERR porque sera responsabilidad del que llame a la funcion util_download_file
                        //de mostrar mensaje en ventana
                        debug_printf(VERBOSE_DEBUG,"Error downloading file. Return code: %d",http_code);
                }
        }

        return http_code;
}

void util_normalize_name(char *texto)
{
	while (*texto) {
		char c=*texto;
		if (c=='(' || c==')') {
			*texto='_';
		}
		texto++;
	}
}

void util_normalize_query_http(char *orig,char *dest)
{

	while (*orig) {
		char c=*orig;
                if (c<=32 || c>=127 || c=='/') {
                        sprintf (dest,"%%%02x",c);
                        dest+=3;
                }

                else {
                        *dest=c;
			dest++;
		}


		orig++;
	}

	*dest=0;


}




int on_extract_entry(const char *filename, void *arg) {
    static int i = 0;
    int n = *(int *)arg;
    debug_printf (VERBOSE_INFO,"Internal zip decompressor: Extracted: %s (%d of %d)", filename, ++i, n);

    return 0;
}

int util_url_is_https(char *url)
{

        char *encontrado;

        encontrado=strstr(url,"https://");
        if (encontrado==url) return 1;
        else return 0;
}

void util_get_host_url(char *url, char *host)
{
        //buscar primero si hay ://

        char *encontrado;
        //char *inicio_host;

        encontrado=strstr(url,"://");
        if (encontrado!=NULL) {
                encontrado +=3; //saltar 3 caracteres ://
        }
        else encontrado=url;

        //Copiar hasta la primera / o final de string
        int i=0;

        for (i=0;(*encontrado)!=0 && (*encontrado)!='/';i++,host++,encontrado++) {
                *host=*encontrado;
        }

        *host=0;

}

void util_get_url_no_host(char *url, char *url_no_host)
{
        //buscar primero si hay ://

        char *encontrado;
        //char *inicio_host;

        encontrado=strstr(url,"://");
        if (encontrado!=NULL) {
                encontrado +=3; //saltar 3 caracteres ://
        }
        else encontrado=url;

        //Buscar hasta la primera / o final de string
        for (;(*encontrado)!=0 && (*encontrado)!='/';encontrado++);

        //copiar desde ahi hasta final de string
        for (;(*encontrado)!=0;encontrado++,url_no_host++) {
                *url_no_host=*encontrado;
        }

        *url_no_host=0;

}


//Retorna ? si caracter esta fuera de rango. Si no , retorna caracter
char util_return_valid_ascii_char(char c)
{
        if (c>=32 && c<=126) return c;
        else return '?';
}


//Retorna checksum de bloque igual que hace spectrum. Podemos indicar valor inicial distinto de 0 para empezar a calcular ya
//con byte de flag calculado
z80_byte get_memory_checksum_spectrum(z80_byte crc,z80_byte *origen,int longitud)
{
        //z80_byte crc=0;

        while (longitud>0) {
                crc ^=*origen;
                origen++;
                longitud--;
        }

        return crc;
}

//Guardar valor de 16 bits en direccion de memoria
void util_store_value_little_endian(z80_byte *destination,z80_int value)
{
        destination[0]=value_16_to_8l(value);
        destination[1]=value_16_to_8h(value);
}


//Recuperar valor de 16 bits de direccion de memoria
z80_int util_get_value_little_endian(z80_byte *origin)
{
        return value_8_to_16(origin[1],origin[0]);
}



void util_get_home_dir(char *homedir)
{

        //asumimos vacio
        homedir[0]=0;

  #ifndef MINGW
        char *directorio_home;
        directorio_home=getenv("HOME");
        if ( directorio_home==NULL) {
                  //printf("Unable to find $HOME environment variable to open configuration file\n");
                return;
        }

        sprintf(homedir,"%s/",directorio_home);

  #else
        char *homedrive;
        char *homepath;
        homedrive=getenv("HOMEDRIVE");
        homepath=getenv("HOMEPATH");

        if (homedrive==NULL || homepath==NULL) {
                //printf("Unable to find HOMEDRIVE or HOMEPATH environment variables to open configuration file\n");
                  return;
          }

        sprintf(homedir,"%s\\%s\\",homedrive,homepath);

  #endif


}

 #define UTIL_BMP_HEADER_SIZE (14+40)

int util_bmp_get_file_size(int ancho,int alto)
{
        //File size
        //cabecera + ancho*alto*3 (*3 porque es 24 bit de color)
        int file_size=(ancho*alto*3)+UTIL_BMP_HEADER_SIZE;

        return file_size;
}

//Asignar memoria para un archivo bmp de 24 bits y meter algunos valores en cabecera
z80_byte *util_bmp_new(int ancho,int alto)
{

        //http://www.ece.ualberta.ca/~elliott/ee552/studentAppNotes/2003_w/misc/bmp_file_format/bmp_file_format.htm


        int memoria_necesaria=(ancho*alto*3)+UTIL_BMP_HEADER_SIZE;

        z80_byte *puntero;

        puntero=malloc(memoria_necesaria);

        if (puntero==NULL) cpu_panic("Can not allocate memory for bmp file");

        puntero[0]='B';
        puntero[1]='M';

        //File size
        //cabecera + ancho*alto*3 (*3 porque es 24 bit de color)
        int file_size=util_bmp_get_file_size(ancho,alto);
        puntero[2]=file_size & 0xFF;
        file_size >>=8;

        puntero[3]=file_size & 0xFF;
        file_size >>=8;

        puntero[4]=file_size & 0xFF;
        file_size >>=8;

        puntero[5]=file_size & 0xFF;
        file_size >>=8;

        //Unused
        puntero[6]=puntero[7]=puntero[8]=puntero[9];

        //Data offset
        puntero[10]=value_16_to_8l(UTIL_BMP_HEADER_SIZE);
        puntero[11]=value_16_to_8h(UTIL_BMP_HEADER_SIZE);
        puntero[12]=puntero[13]=0;

        //Size of info header = 40
        puntero[14]=40;
        puntero[15]=puntero[16]=puntero[17]=0;

        //Ancho
        puntero[18]=value_16_to_8l(ancho);
        puntero[19]=value_16_to_8h(ancho);
        puntero[20]=puntero[21]=0;

        //Alto
        puntero[22]=value_16_to_8l(alto);
        puntero[23]=value_16_to_8h(alto);
        puntero[24]=puntero[25]=0;

        //Planes = 1
        puntero[26]=1;
        puntero[27]=0;

        //Bits per pixel -> 24
        puntero[28]=24;
        puntero[29]=0;

        //Compression -> 0 no compression
        puntero[30]=puntero[31]=puntero[32]=puntero[33]=0;

        //Image size. If compression -> 0
        puntero[34]=puntero[35]=puntero[36]=puntero[37]=0;

        //horizontal resolution: Pixels/meter. 2835. valor de referencia de otro bmp
        puntero[38]=value_16_to_8l(2835);
        puntero[39]=value_16_to_8h(2835);
        puntero[40]=puntero[41]=0;


        //vertical resolution: Pixels/meter
        puntero[42]=value_16_to_8l(2835);
        puntero[43]=value_16_to_8h(2835);
        puntero[44]=puntero[45]=0;

        //Number of actually used colors. For a 8-bit / pixel bitmap this will be 100h or 256.
        //-> 1 ^24
        puntero[46]=puntero[47]=puntero[48]=0;
        puntero[49]=1;

        //Number of important colors  0 = all
        puntero[50]=puntero[51]=puntero[52]=puntero[53]=0;

        return puntero;
}

void util_bmp_putpixel(z80_byte *puntero,int x,int y,int r,int g,int b)
{
        int ancho=value_8_to_16(puntero[19],puntero[18]);
        int alto=value_8_to_16(puntero[23],puntero[22]);

        //Se dibuja de abajo a arriba
        int yfinal=(alto-1)-y;

        int tamanyo_linea=ancho*3;
        int offset_x=x*3;
//TODO: determinados anchos de imagen parece que el offset de linea no siempre es este
        int offset=(yfinal*tamanyo_linea)+offset_x+UTIL_BMP_HEADER_SIZE;

        //Each 3-byte triplet in the bitmap array represents the relative intensities of blue, green, and red, respectively, for a pixel.

        puntero[offset++]=b;
        puntero[offset++]=g;
        puntero[offset++]=r;

}





void util_write_screen_bmp(char *archivo)
{

        enable_rainbow();

        int ancho,alto;


        ancho=screen_get_emulated_display_width_no_zoom_border_en();
        alto=screen_get_emulated_display_height_no_zoom_border_en();


        FILE *ptr_scrfile;
        ptr_scrfile=fopen(archivo,"wb");
        if (!ptr_scrfile) {
                debug_printf (VERBOSE_ERR,"Unable to open Screen file");
        }

        else {


                z80_byte *puntero_bitmap;

                puntero_bitmap=util_bmp_new(ancho,alto);


//pixeles...



                //Derivado de vofile_send_frame


                z80_int *original_buffer;

                original_buffer=rainbow_buffer;


                z80_int color_leido;

                int r,g,b;

                int x,y;


                for (y=0;y<alto;y++) {
                        for (x=0;x<ancho;x++) {
                                color_leido=*original_buffer;
                                convertir_color_spectrum_paleta_to_rgb(color_leido,&r,&g,&b);

                                util_bmp_putpixel(puntero_bitmap,x,y,r,g,b);

                                original_buffer++;
                        }
                }



                int file_size=util_bmp_get_file_size(ancho,alto);

                fwrite(puntero_bitmap,1,file_size,ptr_scrfile);




                fclose(ptr_scrfile);

                free(puntero_bitmap);
        }




}

//Cargar 256 colores de paleta de un archivo bmp (de memoria) en un indice de nuestra paleta interna
void util_bmp_load_palette(z80_byte *mem,int indice_inicio_color)
{

		//Cargar la paleta bmp. A partir del offset 36h??
/*
ColorTable	4 * NumColors bytes	0036h	present only if Info.BitsPerPixel less than 8
colors should be ordered by importance
 		Red	1 byte	 	Red intensity
Green	1 byte	 	Green intensity
Blue	1 byte	 	Blue intensity
reserved	1 byte	 	unused (=0)

*/

        		int i;
		int indice_paleta; //=0x36+4;

		indice_paleta=122;  //obtenido mediante restar el inicio de los pixeles (1146) - 1024 (1024 es lo que ocupa la tabla de colores)
		//Orden BGR0
		for (i=0;i<256;i++) {
			int red=mem[indice_paleta+2];
			int green=mem[indice_paleta+1];
			int blue=mem[indice_paleta];

			int color=(red<<16) | (green<<8) | blue;


			screen_set_colour_normal(indice_inicio_color+i,color);

			indice_paleta +=4;
		}
}



void util_rotate_file(char *filename,int archivos)
{

	//Borrar el ultimo
	char buffer_last_file[PATH_MAX];

	sprintf(buffer_last_file,"%s.%d",filename,archivos);

	debug_printf (VERBOSE_DEBUG,"Erasing oldest file %s",buffer_last_file);

	util_delete(buffer_last_file);

	//Y renombrar, el penultimo al ultimo, el antepenultimo al penultimo, etc
	//con 10 archivos:
	//ren file.9 file.10
	//ren file.8 file.9
	//ren file.7 file.8
	//...
	//ren file.1 file.2
	//ren file file.1 esto a mano
	int i;

	for (i=archivos-1;i>=0;i--) {
		char buffer_file_orig[PATH_MAX];
		char buffer_file_dest[PATH_MAX];

		//Caso especial ultimo (seria el .0)
		if (i==0) {
			strcpy(buffer_file_orig,filename);
		}
		else {
			sprintf(buffer_file_orig,"%s.%d",filename,i);
		}

		sprintf(buffer_file_dest,"%s.%d",filename,i+1);

		debug_printf (VERBOSE_DEBUG,"Renaming log file %s -> %s",buffer_file_orig,buffer_file_dest);
		rename(buffer_file_orig,buffer_file_dest);
	}


}



//Convertir una string de origen a caracteres charset, sustituyendo graficos en cirilicos
int util_convert_utf_charset(char *origen,z80_byte *final,int longitud_texto)
{
        int longitud_final=0;

        int era_utf=0;

        z80_byte letra;

        while (longitud_texto>0) {

                letra=*origen;
                origen++;

            if (era_utf) {
                letra=menu_escribe_texto_convert_utf(era_utf,letra);
                era_utf=0;

                //Caracter final utf
                *final=letra;

            }


            //Si no, ver si entra un prefijo utf
            else {
                //printf ("letra: %02XH\n",letra);
                //Prefijo utf
                            if (menu_es_prefijo_utf(letra)) {
                                    era_utf=letra;
                    //printf ("activado utf\n");
                            }

                else {
                    //Caracter normal
                *final=letra;

                }
            }




        //if (x>=32) {
        //  printf ("Escribiendo caracter [%c] en x: %d\n",letra,x);
        //}


        if (!era_utf) {
                                final++;

                longitud_final++;

        }


        longitud_texto--;

        }


        return longitud_final;
}

const char *spectrum_colour_names[16]={
        "Black",
        "Blue",
        "Red",
        "Magenta",
        "Green",
        "Cyan",
        "Yellow",
        "White",
        "Black",
        "BrightBlue",
        "BrightRed",
        "BrightMagenta",
        "BrightGreen",
        "BrightCyan",
        "BrightYellow",
        "BrightWhite",
};


//Guardar en la posicion indicada el valor de 32 bits, little endian
void util_write_long_value(z80_byte *destino,unsigned int valor)
{
    destino[0]=valor         & 0xFF;
    destino[1]=(valor >> 8)  & 0xFF;
    destino[2]=(valor >> 16) & 0xFF;
    destino[3]=(valor >> 24) & 0xFF;
}

//Lee de la posicion indicada el valor de 32 bits, little endian
unsigned int util_read_long_value(z80_byte *origen)
{
    return (origen[0])|(origen[1]<<8)|(origen[2]<<16)|(origen[3]<<24);
}
