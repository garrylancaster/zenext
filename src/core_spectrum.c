/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <time.h>
#include <sys/time.h>
#include <errno.h>


#include "cpu.h"
#include "debug.h"
#include "audio.h"
#include "screen.h"
#include "ay38912.h"
#include "operaciones.h"
#include "timer.h"
#include "menu.h"
#include "compileoptions.h"
#include "contend.h"
#include "ula.h"
#include "utils.h"
#include "realjoystick.h"
#include "diviface.h"
#include "timex.h"
#include "tbblue.h"
#include "mem128.h"

#include "settings.h"
#include "datagear.h"

#include "ds1307.h"

z80_byte byte_leido_core_spectrum;


int duracion_ultimo_opcode=0;


int disparada_int_pentagon=0;

int pentagon_inicio_interrupt=160;

//int tempcontafifty=0;

int si_siguiente_sonido(void)
{


	return 1;
}


/*
void t_scanline_next_border(void)
{
        //resetear buffer border

	int i;
        //en principio inicializamos el primer valor con el ultimo out del border
        buffer_border[0]=out_254 & 7;

	//Siguientes valores inicializamos a 255
        for (i=1;i<BORDER_ARRAY_LENGTH;i++) buffer_border[i]=255;


}
*/

void t_scanline_next_fullborder(void)
{
        //resetear buffer border

		//No si esta desactivado en tbblue
		if (tbblue_store_scanlines_border.v==0) return;

        //a 255
        //for (i=0;i<CURRENT_FULLBORDER_ARRAY_LENGTH;i++) fullbuffer_border[i]=255;
		//mas rapido con memset
		memset(fullbuffer_border,255,CURRENT_FULLBORDER_ARRAY_LENGTH);

}

extern z80_byte pref237_opcode_leido;
void interrupcion_si_despues_lda_ir(void)
{

	//NMOS
	//printf ("leido %d en interrupt\n",byte_leido_core_spectrum);
	if (byte_leido_core_spectrum==237) {
		//printf ("leido 237 en interrupt, siguiente=%d\n",pref237_opcode_leido);
		if (pref237_opcode_leido==87 || pref237_opcode_leido==95) {
			//printf ("Poner PV a 0 despues de LD a,i o LD a,r\n");
			Z80_FLAGS &=(255-FLAG_PV);
		}
	}
}






void core_spectrum_store_rainbow_current_atributes(void)
{

	//No hacer esto en tbblue
	if (tbblue_store_scanlines.v==0) return;

	//Si no vamos a refrescar pantalla, no tiene sentido almacenar nada en el buffer
/*
Sin saltar frame aqui, tenemos por ejemplo
./zesarux --realvideo --frameskip 4  --vo null --exit-after 10
15 % cpu
tiempo de proceso en 10 segundos: user	0m1.498s

Saltando frame aqui,
12% cpu
tiempo de proceso en 10 segundos: user	0m1.239s
*/
                                if (!screen_if_refresh()) {
                                        //if ((t_estados/screen_testados_linea)>310) printf ("-Not storing rainbow buffer as framescreen_saltar is %d or manual frameskip\n",framescreen_saltar);

					//printf ("-Not storing rainbow buffer as framescreen_saltar is %d or manual frameskip\n",framescreen_saltar);
					return;
                                }

				//ULA dibujo de pantalla
				//last_x_atributo guarda ultima posicion (+1) antes de ejecutar el opcode
				//lo que se pretende hacer aqui es guardar los atributos donde esta leyendo la ula actualmente,
				//y desde esta lectura a la siguiente, dado que cada opcode del z80 puede tardar mas de 4 ciclos (en 4 ciclos se generan 8 pixeles)
				//Esto no es exactamente perfecto,
				//lo ideal es que cada vez que avanzase el contador de t_estados se guardase el atributo que se va a leer
				//como esto es muy costoso, hacemos que se guardan los atributos leidos desde el opcode anterior hasta este
				if (rainbow_enabled.v) {
					if (t_scanline_draw>=screen_indice_inicio_pant && t_scanline_draw<screen_indice_fin_pant) {
						int atributo_pos=(t_estados % screen_testados_linea)/4;
							z80_byte *screen_atributo=get_base_mem_pantalla_attributes();
							z80_byte *screen_pixel=get_base_mem_pantalla();

							//printf ("atributos: %p pixeles: %p\n",screen_atributo,screen_pixel);

							int dir_atributo;

							if (timex_si_modo_8x1()) {
								dir_atributo=screen_addr_table[(t_scanline_draw-screen_indice_inicio_pant)*32];
							}

							else {
								dir_atributo=(t_scanline_draw-screen_indice_inicio_pant)/8;
								dir_atributo *=32;
							}


                                                        int dir_pixel=screen_addr_table[(t_scanline_draw-screen_indice_inicio_pant)*32];

						//si hay cambio de linea, empezamos con el 0
						//puede parecer que al cambiar de linea nos perdemos el ultimo atributo de pantalla hasta el primero de la linea
						//siguiente, pero eso es imposible, dado que eso sucede desde el ciclo 128 hasta el 228 (zona de borde)
						// y no hay ningun opcode que tarde 100 ciclos
						if (last_x_atributo>atributo_pos) last_x_atributo=0;
							dir_atributo += last_x_atributo;
							dir_pixel +=last_x_atributo;

						//Puntero al buffer de atributos
						z80_byte *puntero_buffer;
						puntero_buffer=&scanline_buffer[last_x_atributo*2];

						for (;last_x_atributo<=atributo_pos;last_x_atributo++) {
							//printf ("last_x_atributo: %d atributo_pos: %d\n",last_x_atributo,atributo_pos);
							last_ula_pixel=screen_pixel[dir_pixel];
							last_ula_attribute=screen_atributo[dir_atributo];

							//realmente en este array guardamos tambien atributo cuando estamos en la zona de border,
							//nos da igual, lo hacemos por no complicarlo
							//debemos tambien suponer que atributo_pos nunca sera mayor o igual que ATRIBUTOS_ARRAY_LENGTH
							//esto debe ser asi dado que atributo_pos=(t_estados % screen_testados_linea)/4;

							*puntero_buffer++=last_ula_pixel;
							*puntero_buffer++=last_ula_attribute;
							dir_atributo++;
							dir_pixel++;
						}

						//Siguiente posicion se queda en last_x_atributo

						//printf ("fin lectura attr linea %d\n",t_scanline_draw);
					}

					//Para el bus idle le decimos que estamos en zona de border superior o inferior y por tanto retornamos 255
					else {
						last_ula_attribute=255;
						last_ula_pixel=255;
					}
				}
}


void core_spectrum_fin_frame_pantalla(void)
{
	//Siguiente frame de pantalla
				timer_get_elapsed_core_frame_post();



					tbblue_copper_handle_vsync();


				//tsconf_last_frame_y=-1;

				if (rainbow_enabled.v==1) t_scanline_next_fullborder();

		        t_scanline=0;

		                //printf ("final scan lines. total: %d\n",screen_scanlines);
                		        //printf ("reset no inves\n");
					set_t_scanline_draw_zero();



                                //Parche para maquinas que no generan 312 lineas, porque si enviamos menos sonido se escuchara un click al final
                                //Es necesario que cada frame de pantalla contenga 312 bytes de sonido
                                //Igualmente en la rutina de envio_audio se vuelve a comprobar que todo el sonido a enviar
                                //este completo; esto es necesario para Z88


                int linea_estados=t_estados/screen_testados_linea;

                while (linea_estados<312) {
					audio_send_stereo_sample(audio_valor_enviar_sonido_izquierdo,audio_valor_enviar_sonido_derecho);
					//audio_send_mono_sample(audio_valor_enviar_sonido_izquierdo);
                                        linea_estados++;
                }



                t_estados -=screen_testados_total;

				audio_tone_generator_last=-audio_tone_generator_last;


				//Final de instrucciones ejecutadas en un frame de pantalla
				if (iff1.v==1) {
					interrupcion_maskable_generada.v=1;

					//En Timex, ver bit 6 de puerto FF
					//En ZXuno, ver bit disvint

	                    if (get_zxuno_tbblue_rasterctrl() & 4) {
        	                //interrupciones normales deshabilitadas
                	        //printf ("interrupciones normales deshabilitadas\n");
							//Pero siempre que no se haya disparado una maskable generada por raster

							if (zxuno_tbblue_disparada_raster.v==0) {
								//printf ("interrupciones normales deshabilitadas y no raster disparada\n");
								interrupcion_maskable_generada.v=0;
							}
                        }



					//Si la anterior instruccion ha tardado 32 ciclos o mas
					if (duracion_ultimo_opcode>=cpu_duracion_pulso_interrupcion) {
						debug_printf (VERBOSE_PARANOID,"Losing last interrupt because last opcode lasts 32 t-states or more");
						interrupcion_maskable_generada.v=0;
					}


				}

				//Final de frame. Permitir de nuevo interrupciones pentagon
				disparada_int_pentagon=0;


				cpu_loop_refresca_pantalla();

				vofile_send_frame(rainbow_buffer);


				siguiente_frame_pantalla();


				if (debug_registers) scr_debug_registers();

	  	                contador_parpadeo--;
                        	//printf ("Parpadeo: %d estado: %d\n",contador_parpadeo,estado_parpadeo.v);
	                        if (!contador_parpadeo) {
        	                        contador_parpadeo=16;
                	                estado_parpadeo.v ^=1;
	                        }


				if (!interrupcion_timer_generada.v) {
					//Llegado a final de frame pero aun no ha llegado interrupcion de timer. Esperemos...
					//printf ("no demasiado\n");
					esperando_tiempo_final_t_estados.v=1;
				}

				else {
					//Llegado a final de frame y ya ha llegado interrupcion de timer. No esperamos.... Hemos tardado demasiado
					//printf ("demasiado\n");
					esperando_tiempo_final_t_estados.v=0;
				}




}

#define MAX_SILENT_SCANLINES 600
int last_beeper_scanline = 0;
int last_beeper = 0;
int last_dac_scanline = 0;
int last_dac = 0;

void core_spectrum_fin_scanline(void)
{
//printf ("%d\n",t_estados);
			//if (t_estados>69000) printf ("t_scanline casi final: %d\n",t_scanline);

			if (si_siguiente_sonido() ) {

				//audio_valor_enviar_sonido=0;

				audio_valor_enviar_sonido_izquierdo=audio_valor_enviar_sonido_derecho=0;

				audio_valor_enviar_sonido_izquierdo +=da_output_ay_izquierdo();
				audio_valor_enviar_sonido_derecho +=da_output_ay_derecho();

                                if (beeper_enabled.v)
                                {
                                        if (value_beeper != last_beeper)
                                        {
                                                last_beeper = value_beeper;
                                                last_beeper_scanline = 0;
                                        }

                                        if (last_beeper_scanline < MAX_SILENT_SCANLINES)
                                        {
                                                last_beeper_scanline++;

                                                if (beeper_real_enabled==0) {
                                                        audio_valor_enviar_sonido_izquierdo += value_beeper;
                                                        audio_valor_enviar_sonido_derecho += value_beeper;
                                                }

                                                else {
                                                        char suma_beeper=get_value_beeper_sum_array();
                                                        audio_valor_enviar_sonido_izquierdo += suma_beeper;
                                                        audio_valor_enviar_sonido_derecho += suma_beeper;
                                                        beeper_new_line();
                                                }
                                        }
                                }

				if (audiodac_enabled.v)
                                {
                                        if (audiodac_last_value_data != last_dac)
                                        {
                                                last_dac = audiodac_last_value_data;
                                                last_dac_scanline = 0;
                                        }

                                        if (last_dac_scanline < MAX_SILENT_SCANLINES)
                                        {
                                                audiodac_mix();
                                                last_dac_scanline++;
                                        }
				}

				//Ajustar volumen
				if (audiovolume!=100) {
					audio_valor_enviar_sonido_izquierdo=audio_adjust_volume(audio_valor_enviar_sonido_izquierdo);
					audio_valor_enviar_sonido_derecho=audio_adjust_volume(audio_valor_enviar_sonido_derecho);
				}

				//if (audio_valor_enviar_sonido>127 || audio_valor_enviar_sonido<-128) printf ("Error audio value: %d\n",audio_valor_enviar_sonido);

				if (audio_tone_generator) {
					audio_send_mono_sample(audio_tone_generator_get() );
				}

				else {
					audio_send_stereo_sample(audio_valor_enviar_sonido_izquierdo,audio_valor_enviar_sonido_derecho);
				}



				ay_chip_siguiente_ciclo();

			}

			//final de linea

			//copiamos contenido linea y border a buffer rainbow
			if (rainbow_enabled.v==1) {
				if (!screen_if_refresh()) {
					//if ((t_estados/screen_testados_linea)>319) printf ("-Not storing rainbow buffer as framescreen_saltar is %d or manual frameskip\n",framescreen_saltar);
				}

				else {
					//if ((t_estados/screen_testados_linea)>319) printf ("storing rainbow buffer\n");
					TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_store_scanline_rainbow);
					screen_store_scanline_rainbow_solo_border();
					screen_store_scanline_rainbow_solo_display();
					TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_store_scanline_rainbow);
				}

				//t_scanline_next_border();

			}

			TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_t_scanline_next_line);
			t_scanline_next_line();
			TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_t_scanline_next_line);


			//se supone que hemos ejecutado todas las instrucciones posibles de toda la pantalla. refrescar pantalla y
			//esperar para ver si se ha generado una interrupcion 1/50

            if (t_estados>=screen_testados_total) {
				TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_fin_frame_pantalla);
				core_spectrum_fin_frame_pantalla();

				TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_fin_frame_pantalla);
			}
			//Fin bloque final de pantalla



}

void core_spectrum_handle_interrupts(void)
{
		debug_fired_interrupt=1;

			//printf ("Generada interrupcion Z80\n");



			//if (interrupcion_non_maskable_generada.v) printf ("generada nmi\n");

                        //ver si esta en HALT
                        if (z80_ejecutando_halt.v) {
                                        z80_ejecutando_halt.v=0;
                                        reg_pc++;
                        }

						//ver si estaba en halt el copper

			if (1==1) {
					if (1==1) {
					//else {




					//justo despues de EI no debe generar interrupcion
					//e interrupcion nmi tiene prioridad
						if (interrupcion_maskable_generada.v && byte_leido_core_spectrum!=251) {

						//printf ("Lanzada interrupcion spectrum normal\n");

						debug_anota_retorno_step_maskable();
						//Tratar interrupciones maskable
						interrupcion_maskable_generada.v=0;

                                                // HACK: This emulates the NMOS bug in LD A,I/R where P/V does not
                                                //       necessarily get the correct value of IFF2. This is apparently
                                                //       needed by some demos, but Z80N does not have this behaviour
                                                //       and NextZXOS assumes P/V is always correct.
                                                //       Perhaps ZX81 JBRACING, as mentioned in Changelog?
						//interrupcion_si_despues_lda_ir();



						push_valor(reg_pc,PUSH_VALUE_TYPE_MASKABLE_INTERRUPT);

						reg_r++;




						//desactivar interrupciones al generar una
						iff1.v=iff2.v=0;
						//Modelos spectrum

						if (im_mode==0 || im_mode==1) {
							cpu_common_jump_im01();
						}
						else {
						//IM 2.

							z80_int temp_i;
							z80_byte dir_l,dir_h;


                            temp_i=reg_i*256+255;
							dir_l=peek_byte(temp_i++);
							dir_h=peek_byte(temp_i);
							reg_pc=value_8_to_16(dir_h,dir_l);
							t_estados += 7;

							//Para mejorar demos ula128 y scroll2017
							//Pero esto hace empeorar la demo ulatest3.tap
							if (ula_im2_slow.v) t_estados++;
						}

					}
				}


			}
}


void core_spectrum_handle_interrupts_pentagon(void)
{
		if (!disparada_int_pentagon) {

				int linea=t_estados/screen_testados_linea;
				if (linea==319) {
					//en el Spectrum la INT comienza en el scanline 248, 0T
					//Pero en Pentagon la interrupción debe dispararse en el scanline 239 (contando desde 0), y 320 pixel clocks (o 160 T estados) tras comenzar dicho scanline
					//A los 160 estados
					int t_est_linea=t_estados % screen_testados_linea;
					if (t_est_linea>=pentagon_inicio_interrupt) {
						//printf ("Int Pentagon\n");
						//printf ("scanline %d t_estados %d\n",t_estados/screen_testados_linea,t_estados);

						disparada_int_pentagon=1;
						if (iff1.v==1) {
							//printf ("Generated pentagon interrupt\n");
							//printf ("scanline %d t_estados %d\n",t_estados/screen_testados_linea,t_estados);
							interrupcion_maskable_generada.v=1;


							//Si la anterior instruccion ha tardado 32 ciclos o mas
							if (duracion_ultimo_opcode>=cpu_duracion_pulso_interrupcion) {
								debug_printf (VERBOSE_PARANOID,"Losing last interrupt because last opcode lasts 32 t-states or more");
								interrupcion_maskable_generada.v=0;
							}
						}
					}

				}
			}
}

void core_spectrum_ciclo_fetch(void)
{

	TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_store_rainbow_current_atributes);
	core_spectrum_store_rainbow_current_atributes();
	TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_store_rainbow_current_atributes);




				if (nmi_pending_pre_opcode) {
						//Dado que esto se activa despues de lanzar nmi y antes de leer opcode, aqui saltara cuando PC=66H
						debug_printf (VERBOSE_DEBUG,"Handling nmi mapping pre opcode fetch at %04XH",reg_pc);
						nmi_handle_pending_prepost_fetch();
				}


				int t_estados_antes_opcode=t_estados;
				core_refetch=0;

				//Modo normal
				if (diviface_enabled.v==0) {

        	                        contend_read( reg_pc, 4 );
					byte_leido_core_spectrum=FETCH_OPCODE();



				}


				//Modo con diviface activado
				else {
					diviface_pre_opcode_fetch();
					contend_read( reg_pc, 4 );
					byte_leido_core_spectrum=FETCH_OPCODE();
					diviface_post_opcode_fetch();
				}




#ifdef EMULATE_CPU_STATS
				util_stats_increment_counter(stats_codsinpr,byte_leido_core_spectrum);
#endif

                reg_pc++;

				//Nota: agregar estos dos if de nmi_pending_pre_opcode y nmi_pending_post_opcode
				//supone un 0.2 % de uso mas en mi iMac: pasa de usar 5.4% cpu a 5.6% cpu en --vo null y --ao null
				//Es muy poco...
				if (nmi_pending_post_opcode) {
					//Dado que esto se activa despues de lanzar nmi y leer opcode, aqui saltara cuando PC=67H
					debug_printf (VERBOSE_DEBUG,"Handling nmi mapping post opcode fetch at %04XH",reg_pc);
					nmi_handle_pending_prepost_fetch();
				}

				reg_r++;



#ifdef EMULATE_SCF_CCF_UNDOC_FLAGS
				//Guardar antes F
				scf_ccf_undoc_flags_before=Z80_FLAGS;
#endif

	            codsinpr[byte_leido_core_spectrum]  () ;


#ifdef EMULATE_SCF_CCF_UNDOC_FLAGS
				//Para saber si se ha modificado
				scf_ccf_undoc_flags_after_changed=(Z80_FLAGS  == scf_ccf_undoc_flags_before ? 0 : 1);
#endif

				//Ultima duracion, si es que ultimo opcode no genera fetch de nuevo del opcode
				if (!core_refetch) duracion_ultimo_opcode=t_estados-t_estados_antes_opcode;
				else duracion_ultimo_opcode +=t_estados-t_estados_antes_opcode;





				//Soporte interrupciones raster zxuno
				zxuno_tbblue_handle_raster_interrupts();

				//Soporte Datagear/TBBlue DMA
				if (datagear_dma_emulation.v && datagear_dma_is_disabled.v==0) datagear_handle_dma();

				//Soporte TBBlue copper y otras...
					//Si esta activo copper
					tbblue_copper_handle_next_opcode();


					if (tbblue_use_rtc_traps) {
						//Reloj RTC
						if (reg_pc==0x27a9 || reg_pc==0x27aa) {
						/*
							27A9 C9     RET
							27AA 37     SCF
							27AB C9     RET
						*/
							if (
								peek_byte_no_time(reg_pc)==0xC9 &&
								peek_byte_no_time(reg_pc+1)==0x37 &&
								peek_byte_no_time(reg_pc+2)==0xC9
							)
							tbblue_trap_return_rtc();
						}

					}



}

//bucle principal de ejecucion de la cpu de spectrum
void cpu_core_loop_spectrum(void)
{
		debug_get_t_stados_parcial_pre();

		timer_check_interrupt();

                if (interrupcion_non_maskable_generada.v) {
                        debug_anota_retorno_step_nmi();
                        //printf ("generada nmi\n");
                        interrupcion_non_maskable_generada.v=0;


                        //NMI wait 14 estados
                        t_estados += 14;

                        if (tbblue_registers[0xc0] & 0x08)
                        {
                                tbblue_registers[0xc2] = reg_pc & 0xff;
                                tbblue_registers[0xc3] = reg_pc >> 8;
                                reg_sp -= 2;
                        }
                        else
                        {
                                push_valor(reg_pc,PUSH_VALUE_TYPE_NON_MASKABLE_INTERRUPT);
                        }

                        reg_r++;
                        iff1.v=0;
                        //printf ("Calling NMI with pc=0x%x\n",reg_pc);

                        //Otros 6 estados
                        t_estados += 6;

                        //Total NMI: NMI WAIT 14 estados + NMI CALL 12 estados
                        reg_pc= 0x66;

                                                                        //printf ("generada nmi pc=%04XH\n",reg_pc);

                        //temp

                        t_estados -=15;

                        if (diviface_enabled.v) {
                                //diviface_paginacion_manual_activa.v=0;
                                diviface_write_control_register(diviface_read_control_register() & 0x7f);
                                diviface_set_automatic(0);
                        }

                        generate_nmi_prepare_fetch();


                }


		{
int t_estados_before = t_estados;
			if (esperando_tiempo_final_t_estados.v==0) {
				TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_ciclo_fetch);
				core_spectrum_ciclo_fetch();
				TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_ciclo_fetch);
            }
t_estados_log += (t_estados-t_estados_before);

        }



		//A final de cada scanline
		if ( (t_estados/screen_testados_linea)>t_scanline  ) {
			TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_fin_scanline);
			core_spectrum_fin_scanline();
			TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_fin_scanline);
		}


		//Ya hemos leido duracion ultimo opcode. Resetearla a 0 si no hay que hacer refetch
		if (!core_refetch) duracion_ultimo_opcode=0;



		if (esperando_tiempo_final_t_estados.v) {
			timer_pause_waiting_end_frame();
		}



		//Interrupcion de 1/50s. mapa teclas activas y joystick
        if (interrupcion_fifty_generada.v) {
			interrupcion_fifty_generada.v=0;

            //y de momento actualizamos tablas de teclado segun tecla leida
			//printf ("Actualizamos tablas teclado %d ", temp_veces_actualiza_teclas++);
			TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_scr_actualiza_tablas_teclado);
			scr_actualiza_tablas_teclado();
			TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_scr_actualiza_tablas_teclado);


			//lectura de joystick
			TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_realjoystick_main);
			realjoystick_main();
			TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_realjoystick_main);



		}


		//Interrupcion de procesador y marca final de frame
		if (interrupcion_timer_generada.v) {
			//printf ("Generada interrupcion timer\n");
			interrupcion_timer_generada.v=0;
			esperando_tiempo_final_t_estados.v=0;

			//Para calcular lo que se tarda en ejecutar todo un frame
			timer_get_elapsed_core_frame_pre();


        }


		//Interrupcion de cpu. gestion im0/1/2. Esto se hace al final de cada frame en spectrum o al cambio de bit6 de R en zx80/81
		if (interrupcion_maskable_generada.v || interrupcion_non_maskable_generada.v) {
			TIMESENSOR_ENTRY_PRE(TIMESENSOR_ID_core_spectrum_handle_interrupts);
			core_spectrum_handle_interrupts();
			TIMESENSOR_ENTRY_POST(TIMESENSOR_ID_core_spectrum_handle_interrupts);
        }
		//Fin gestion interrupciones




		debug_get_t_stados_parcial_post();
}
