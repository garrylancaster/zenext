/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <stdio.h>
#ifndef MINGW
	#include <unistd.h>
#endif
#include <string.h>

#include <time.h>
#include <sys/time.h>
#include <errno.h>

#include <signal.h>

#ifdef MINGW
	//Para llamar a FreeConsole
	#include <windows.h>
#endif


#if defined(__APPLE__)
	//Para _NSGetExecutablePath
	#include <mach-o/dyld.h>
#endif


#include "audio.h"
#include "ay38912.h"
#include "compileoptions.h"
#include "contend.h"
#include "cpu.h"
#include "datagear.h"
#include "debug.h"
#include "diviface.h"
#include "divmmc.h"
#include "ds1307.h"
#include "joystick.h"
#include "mem128.h"
#include "menu_items.h"
#include "mmc.h"
#include "multiface.h"
#include "network.h"
#include "operaciones.h"
#include "printers.h"
#include "realjoystick.h"
#include "remote.h"
#include "screen.h"
#include "scrnull.h"
#include "settings.h"
#include "tbblue.h"
#include "timer.h"
#include "timex.h"
#include "ula.h"
#include "ulaplus.h"
#include "utils.h"


#ifdef USE_COCOA
#include "scrcocoa.h"
#endif


#ifdef COMPILE_XWINDOWS
#include "scrxwindows.h"
#endif

#ifdef COMPILE_SDL

	#ifdef COMPILE_SDL2
		#include "scrsdl2.h"
	#else
		#include "scrsdl.h"
	#endif
#endif


#ifdef COMPILE_FBDEV
#include "scrfbdev.h"
#endif


#ifdef COMPILE_DSP
#include "audiodsp.h"
#endif

#ifdef COMPILE_PCSPEAKER
#include "audiopcspeaker.h"
#endif

#ifdef COMPILE_SDL
#include "audiosdl.h"
#endif


#ifdef COMPILE_ALSA
#include "audioalsa.h"
#endif

#ifdef COMPILE_PULSE
#include "audiopulse.h"
#endif


#ifdef COMPILE_COREAUDIO
#include "audiocoreaudio.h"
#endif


#include "audionull.h"

#ifdef USE_PTHREADS
#include <pthread.h>

pthread_t thread_main_loop;

#endif



#include "autoselectoptions.h"



char *romfilename;

//Maquina actual
z80_byte current_machine_type;

//Ultima maquina seleccionada desde post_set_machine
z80_byte last_machine_type=255;


//Tipos de CPU Z80 activa
enum z80_cpu_types z80_cpu_current_type=Z80_TYPE_GENERIC;


char *z80_cpu_types_strings[TOTAL_Z80_CPU_TYPES]={
	"Generic",
	"Mostek",
	"CMOS"
};


char *scrfile;

int zoom_x=2,zoom_y=2;
int zoom_x_original,zoom_y_original;

struct timeval z80_interrupts_timer_antes, z80_interrupts_timer_ahora;

struct timeval zesarux_start_time;

long z80_timer_difftime, z80_timer_seconds, z80_timer_useconds;

//Indica si se tiene que probar dispositivos. Solo se hace cuando no se especifica uno concreto por linea de comandos
z80_bit try_fallback_video;
z80_bit try_fallback_audio;

//punteros a funciones de inicio para hacer fallback de video, funciones de set
driver_struct scr_driver_array[MAX_SCR_INIT];
int num_scr_driver_array=0;

//punteros a funciones de inicio para hacer fallback de audio, funciones de set
driver_struct audio_driver_array[MAX_SCR_INIT];
int num_audio_driver_array=0;

//Los inicializamos a cadena vacia... No poner NULL, dado que hay varios strcmp que se comparan contra esto
char *driver_screen="";
char *driver_audio="";


//porcentaje de velocidad de la cpu
int porcentaje_velocidad_emulador=100;

int anterior_porcentaje_velocidad_emulador=100;


int argc;
char **argv;
int puntero_parametro;



z80_bit border_enabled;

//contador que indica cuantos frames de pantalla entero se han enviado, para contar cuando enviamos el sonido
int contador_frames_veces_buffer_audio=0;

int final_nombre;


//T-estados totales del frame
int t_estados=0;
int t_estados_log=0;

//t-estados parcial. Usados solo en debug
unsigned int debug_t_estados_parcial=0;

//Scan line actual. Este siempre indica la linea actual dentro del frame total. No alterable por vsync
int t_scanline=0;

int t_scanline_draw=0;
int t_scanline_draw_timeout=0;


z80_bit test_config_and_exit={0};


//SDL : Si se lee teclado mediante scancodes raw en vez de usar localizacion de teclado
z80_bit sdl_raw_keyboard_read={0};

//Si se usa core de spectrum reducido o no
#ifdef USE_REDUCED_CORE_SPECTRUM
z80_bit core_spectrum_uses_reduced={1};
#else
z80_bit core_spectrum_uses_reduced={0};
#endif


void add_scr_init_array(char *name,int (*funcion_init) () , int (*funcion_set) () )
{
	if (num_scr_driver_array==MAX_SCR_INIT) {
                cpu_panic("Error. Maximum number of screen drivers");
	}


	strcpy(scr_driver_array[num_scr_driver_array].driver_name,name);
	scr_driver_array[num_scr_driver_array].funcion_init=funcion_init;
	scr_driver_array[num_scr_driver_array].funcion_set=funcion_set;

	num_scr_driver_array++;
}


void add_audio_init_array(char *name,int (*funcion_init) () , int (*funcion_set) () )
{
        if (num_audio_driver_array==MAX_AUDIO_INIT) {
                cpu_panic("Error. Maximum number of audio drivers");
        }


        strcpy(audio_driver_array[num_audio_driver_array].driver_name,name);
        audio_driver_array[num_audio_driver_array].funcion_init=funcion_init;
        audio_driver_array[num_audio_driver_array].funcion_set=funcion_set;

        num_audio_driver_array++;
}


void do_fallback_video(void)
{
	debug_printf(VERBOSE_INFO,"Guessing video driver");

	int i;

	for (i=0;i<num_scr_driver_array;i++) {

		screen_reset_scr_driver_params();

		int (*funcion_init) ();
		int (*funcion_set) ();

		funcion_init=scr_driver_array[i].funcion_init;
		funcion_set=scr_driver_array[i].funcion_set;
		if ( (funcion_init()) ==0) {
			debug_printf(VERBOSE_DEBUG,"Ok video driver i:%d %s",i,scr_new_driver_name);
			funcion_set();
			return;
		}

		debug_printf(VERBOSE_INFO,"Fallback to next video driver");
	}


	printf ("No valid video driver found\n");
	exit(1);
}

void do_fallback_audio(void)
{
        debug_printf(VERBOSE_INFO,"Guessing audio driver");

        int i;

        for (i=0;i<num_audio_driver_array;i++) {
                int (*funcion_init) ();
                int (*funcion_set) ();

                funcion_init=audio_driver_array[i].funcion_init;
                funcion_set=audio_driver_array[i].funcion_set;
                if ( (funcion_init()) ==0) {
                        debug_printf (VERBOSE_DEBUG,"Ok audio driver i:%d %s",i,audio_new_driver_name);
                        funcion_set();
                        return;
                }

		debug_printf(VERBOSE_INFO,"Fallback to next audio driver");
        }


        printf ("No valid audio driver found\n");
        exit(1);
}


int siguiente_parametro(void)
{
	argc--;
	if (argc==0) {
		//printf ("Error sintaxis\n");
		return 1;
	}
	puntero_parametro++;
	return 0;
}

void siguiente_parametro_argumento(void)
{
	if (siguiente_parametro()) {
		printf ("Syntax error. Parameter %s missing value\n",argv[puntero_parametro]);
		exit(1);
	}
}

z80_registro registro_hl;

z80_registro registro_de;

z80_registro registro_bc;

z80_byte reg_h_shadow,reg_l_shadow;
z80_byte reg_b_shadow,reg_c_shadow;
z80_byte reg_d_shadow,reg_e_shadow;
z80_byte reg_a_shadow;


z80_byte reg_i;
z80_byte reg_r,reg_r_bit7;

z80_int reg_pc;
z80_byte reg_a;
z80_int reg_sp;
z80_int reg_ix;
z80_int reg_iy;

//Nueva gestion de flags
z80_byte Z80_FLAGS;
z80_byte Z80_FLAGS_SHADOW;

//MEMPTR. Solo se usara si se ha activado en el configure
z80_int memptr;

//Solo se usa si se habilita EMULATE_SCF_CCF_UNDOC_FLAGS
z80_byte scf_ccf_undoc_flags_before;
int scf_ccf_undoc_flags_after_changed;

//Optimizacion de velocidad de LDIR,LDDR:
z80_bit cpu_ldir_lddr_hack_optimized={0};

//A 0 si interrupts disabled
//A 1 si interrupts enabled
//z80_bit interrupts;
z80_bit iff1;
/*
These flip flops are simultaneously set or reset by the EI and DI instructions. IFF1 determines whether interrupts are allowed, but its value cannot be read. The value of IFF2 is copied to the P/V flag by LD A,I and LD A,R. When an NMI occurs, IFF1 is reset, thereby disallowing further [maskable] interrupts, but IFF2 is left unchanged. This enables the NMI service routine to check whether the interrupted program had enabled or disabled maskable interrupts. So, Spectrum snapshot software can only read IFF2, but most emulators will emulate both, and then the one that matters most is IFF1.
*/

z80_bit iff2;

z80_byte im_mode=0;
z80_bit cpu_step_mode;

//border se ha modificado
z80_bit modificado_border;

//Si se genera valor random para cada cold boot en el registro R
z80_bit cpu_random_r_register={0};

//Inves. Poke ROM. valor por defecto
//z80_byte valor_poke_rom=255;


//Decir si hay que volver a hacer fetch en el core, esto pasa con instrucciones FD FD FD ... por ejemplo
int core_refetch=0;


//Decir que ha llegado a final de frame pantalla y tiene que revisar si enviar y recibir snapshots de ZRCP y ZENG
z80_bit core_end_frame_check_zrcp_zeng_snap={0};

//en spectrum, 32. en pentagon, 36
int cpu_duracion_pulso_interrupcion=32;


//Inves. Ultimo valor hecho poke a RAM baja (0...16383) desde menu
z80_byte last_inves_low_ram_poke_menu=255;

z80_bit inves_ula_bright_error={1};

//Inves. Contador ula delay. mas o menos exagerado
//maximo 1: a cada atributo
//z80_byte inves_ula_delay_factor=3;


//Tablas teclado
z80_byte puerto_65278=255; //    db    255  ; V    C    X    Z    Sh    ;0
z80_byte puerto_65022=255; //    db    255  ; G    F    D    S    A     ;1
z80_byte puerto_64510=255; //    db              255  ; T    R    E    W    Q     ;2
z80_byte puerto_63486=255; //    db              255  ; 5    4    3    2    1     ;3
z80_byte puerto_61438=255; //    db              255  ; 6    7    8    9    0     ;4
z80_byte puerto_57342=255; //    db              255  ; Y    U    I    O    P     ;5
z80_byte puerto_49150=255; //    db              255  ; H                J         K      L    Enter ;6
z80_byte puerto_32766=255; //    db              255  ; B    N    M    Simb Space ;7

//puertos especiales no presentes en spectrum
z80_byte puerto_especial1=255; //   ;  .  .  PgDn  PgUp ESC ;
z80_byte puerto_especial2=255; //   F5 F4 F3 F2 F1
z80_byte puerto_especial3=255; //  F10 F9 F8 F7 F6
z80_byte puerto_especial4=255; //  F15 F14 F13 F12 F11

z80_bit chloe_keyboard={0};



//se ha llegado al final de instrucciones en un frame de pantalla y esperamos
//a que se genere una interrupcion de 1/50s
z80_bit esperando_tiempo_final_t_estados;

//se ha generado interrupcion maskable de la cpu
//en spectrum pasa con el timer
z80_bit interrupcion_maskable_generada;


//se ha generado interrupcion non maskable de la cpu.
z80_bit interrupcion_non_maskable_generada;


//se ha generado interrupcion de timer 1/50s
z80_bit interrupcion_timer_generada;


z80_bit z80_ejecutando_halt;


z80_byte *memoria_spectrum;




//Si no cambiamos parametros de frameskip y otros cuando es maquina lenta (raspberry, cocoa mac os x, etc)
z80_bit no_cambio_parametros_maquinas_lentas={0};



//fin valores para stdout


z80_bit windows_no_disable_console={0};


//Si salir rapido del emulador
z80_bit quickexit={0};


//Si soporte azerty para xwindows
z80_bit azerty_keyboard={0};


z80_int ramtop_ace;

//Permitir escritura en ROM
z80_bit allow_write_rom={0};

//Modo turbo. 1=normal. 2=7 Mhz, 3=14 Mhz, etc
int cpu_turbo_speed=1;


char dump_ram_file[PATH_MAX]="";


//Si se sale del emulador despues de X segundos. A 0 para desactivarlo
int exit_emulator_after_seconds=0;

int exit_emulator_after_seconds_counter=0;


char last_version_string[255]="";


z80_bit do_no_show_changelog_when_update={0};

int cpu_turbo_speed_antes=1;


z80_bit set_machine_empties_audio_buffer={1};

//Texto indicado en el parametro
char parameter_disablebetawarning[100]="";


//parametros de estadisticas
int total_minutes_use=0;


//Aqui solo se llama posteriormente a haber inicializado la maquina, nunca antes
void cpu_set_turbo_speed(void)
{


	debug_printf (VERBOSE_INFO,"Changing turbo mode from %dX to %dX",cpu_turbo_speed_antes,cpu_turbo_speed);

	//Ajustes previos de t_estados. En estos ajustes, solo las variables t_estados, antes_t_estados se usan. Las t_estados_en_linea, t_estados_percx son para debug
	//printf ("Turbo was %d, setting turbo %d\n",cpu_turbo_speed_antes,cpu_turbo_speed);

	//int t_estados_en_linea=t_estados % screen_testados_linea;

	//int t_estados_percx=(t_estados_en_linea*100)/screen_testados_linea;

	//printf ("Before changing turbo, t-scanline: %d, t-states: %d, t-states in line: %d, percentaje column: %d%%\n",t_scanline,t_estados,t_estados_en_linea,t_estados_percx);

	int antes_t_estados=t_estados / cpu_turbo_speed_antes;

	//printf ("Before changing turbo, t-states at turbo 1X: %d\n",antes_t_estados);


	z80_bit antes_debug_breakpoints_enabled;
	antes_debug_breakpoints_enabled.v=debug_breakpoints_enabled.v;

	z80_bit antes_mutiface_enabled;
	antes_mutiface_enabled.v=multiface_enabled.v;

	z80_bit antes_cpu_code_coverage_enabled;
	antes_cpu_code_coverage_enabled.v=cpu_code_coverage_enabled.v;

	z80_bit antes_cpu_history_enabled;
	antes_cpu_history_enabled.v=cpu_history_enabled.v;

	z80_bit antes_extended_stack_enabled;
	antes_extended_stack_enabled.v=extended_stack_enabled.v;





	if (cpu_turbo_speed>MAX_CPU_TURBO_SPEED) {
		debug_printf (VERBOSE_INFO,"Turbo mode higher than maximum. Setting to %d",MAX_CPU_TURBO_SPEED);
		cpu_turbo_speed=MAX_CPU_TURBO_SPEED;
	}




	//Variable turbo se sobreescribe al llamar a set_machine_params. Guardar y restaurar luego
	int speed=cpu_turbo_speed;

	set_machine_empties_audio_buffer.v=0; //para que no vacie buffer de sonido y asi sonido no se oye extraño
	set_machine_params();

	cpu_turbo_speed=speed;

	screen_testados_linea *=cpu_turbo_speed;
        screen_set_video_params_indices();
        inicializa_tabla_contend_cached_change_cpu_speed();

        //Recalcular algunos valores cacheados
        recalcular_get_total_ancho_rainbow();
        recalcular_get_total_alto_rainbow();




	//Ajustes posteriores de t_estados
	//Ajustar t_estados para que se quede en mismo "sitio"
	t_estados=antes_t_estados * cpu_turbo_speed;


	//t_estados_en_linea=t_estados % screen_testados_linea;

	//t_estados_percx=(t_estados_en_linea*100)/screen_testados_linea;

	//printf ("After changing turbo, t-states: %d, t-states in line: %d, percentaje column: %d%%\n",t_estados,t_estados_en_linea,t_estados_percx);
	//printf ("Calculated t-scanline according to t-states: %d\n",t_estados / screen_testados_linea);



        init_rainbow();
        init_cache_putpixel();

	//Si estaba modo debug cpu, reactivar
	if (antes_debug_breakpoints_enabled.v) {
		debug_printf(VERBOSE_INFO,"Re-enabling breakpoints because they were enabled before changing turbo mode");
		debug_breakpoints_enabled.v=1;
		breakpoints_enable();
	}

	if (antes_mutiface_enabled.v) multiface_enable();



	if (antes_cpu_code_coverage_enabled.v) set_cpu_core_code_coverage_enable();


	if (antes_cpu_history_enabled.v) set_cpu_core_history_enable();


	if (antes_extended_stack_enabled.v) set_extended_stack();



	cpu_turbo_speed_antes=cpu_turbo_speed;



	/*
	Calculos de tiempo en ejecutar esta funcion de cambio de velocidad de cpu, desde metodo antiguo hasta optimizado actual:
--Metodo clasico de obtener tablas contend:

	Con O0:
cpu: X01 tiempo: 1611 us
cpu: X08 tiempo: 4117 us

	Con O2:
cpu: X01 tiempo: 880 us
cpu: X08 tiempo: 1031 us



	--Con rutina contend con memset:
	Con O0:
cpu: X08 tiempo: 863 us

	Con O2:
cpu: X08 tiempo: 539 us

	-- Con tabla cacheada al cambiar speed:
	Con O0:
cpu: X01 tiempo: 964 us
cpu: X08 tiempo: 883 us

	Con O2:
cpu: X01 tiempo: 547 us
cpu: X08 tiempo: 438 us
	*/


}



//Establecer la mayoria de registros a valores indefinidos (a 255), que asi se establecen al arrancar una maquina
//al pulsar el boton de reset aqui no se llama
void cold_start_cpu_registers(void)
{

	//Probar RANDOMIZE USR 46578 y debe aparecer pantalla con colores y al pulsar Espacio, franjas en el borde
	//Si esto se ejecuta despues de un reset, no sucede
	//Ver http://foro.speccy.org/viewtopic.php?f=11&t=2319
	//y http://www.worldofspectrum.org/forums/discussion/comment/539714#Comment_539714

        /*
        AF=BC=DE=HL=IX=IY=SP=FFFFH
        AF'=BC'=DE'=HL'=FFFFH
        IR=0000H
        */

        reg_a=0xff;
        Z80_FLAGS=0xff;
        BC=HL=DE=0xffff;
        reg_ix=reg_iy=reg_sp=0xffff;

        reg_h_shadow=reg_l_shadow=reg_b_shadow=reg_c_shadow=reg_d_shadow=reg_e_shadow=reg_a_shadow=Z80_FLAGS_SHADOW=0xff;
        reg_i=0;
        reg_r=reg_r_bit7=0;

	if (cpu_random_r_register.v) {
		reg_r=value_16_to_8l(randomize_noise[0]) & 127;
		debug_printf (VERBOSE_DEBUG,"R Register set to random value: %02XH",reg_r);
	}

	out_254_original_value=out_254=0xff;

	modificado_border.v=1;
		tbblue_hard_reset();
}


//Para maquinas Z88 y zxuno y prism
void hard_reset_cpu(void)
{
        divmmc_diviface_enable();
        diviface_allow_automatic_paging.v=0;
    	tbblue_hard_reset();
	reset_cpu();
}

void soft_reset_cpu(void)
{
        divmmc_diviface_enable();
        diviface_allow_automatic_paging.v=0;
    	tbblue_soft_reset();
		reset_cpu();
}


void reset_cpu(void)
{

	debug_printf (VERBOSE_INFO,"Reset cpu");

	reg_pc=0;
	reg_i=0;

	//mapear rom 0 en modos 128k y paginas RAM normales
	puerto_32765=0;
	puerto_8189=0;

	zesarux_zxi_last_register=0;
	zesarux_zxi_registers_array[0]=0;

	zesarux_zxi_registers_array[4]=0;
	zesarux_zxi_registers_array[5]=0;


	interrupcion_maskable_generada.v=0;
	interrupcion_non_maskable_generada.v=0;
  interrupcion_timer_generada.v=0;
	iff1.v=iff2.v=0;
  im_mode=0;


	//Algunos otros registros
	reg_a=0xff;
	Z80_FLAGS=0xff;
	reg_sp=0xffff;

	datagear_reset();

	diviface_write_control_register(diviface_read_control_register() & 0x7f);
        diviface_set_automatic(0);


        z80_ejecutando_halt.v=0;


        esperando_tiempo_final_t_estados.v=0;



	t_estados=0;
	t_scanline=0;
	t_scanline_draw=0;

	init_chip_ay();

#ifdef EMULATE_CPU_STATS
util_stats_init();
#endif


		if (ulaplus_presente.v) ulaplus_set_mode(0);
		if (ulaplus_presente.v) ulaplus_set_extended_mode(0);


	//Resetear modo timex
	timex_port_ff=0;

	//Resetear puerto eff7 pentagon
	puerto_eff7=0;


		//tbblue_read_port_24d5_index=0;
		ds1307_reset();


        if (mmc_enabled.v) mmc_reset();

	ay_player_playing.v=0;

	if (multiface_enabled.v) {
		multiface_lockout=0;
		multiface_unmap_memory();
	}

	//Inicializar zona memoria de debug
	debug_memory_zone_debug_reset();

}


void cpu_help(void)
{
	printf ("Usage:\n"

		"--zoom n           Total Zoom Factor\n"
		"--vo driver        Video output driver. Valid drivers: ");

#ifdef USE_COCOA
	printf ("cocoa ");
#endif

#ifdef COMPILE_XWINDOWS
	printf ("xwindows ");
#endif

#ifdef COMPILE_SDL
        printf ("sdl ");
#endif


#ifdef COMPILE_FBDEV
	printf ("fbdev ");
#endif

	printf ("null\n");


	printf ("--vofile file      Also output video to raw file\n");
	printf ("--vofilefps n      FPS of the output video [1|2|5|10|25|50] (default:5)\n");



	printf ("--ao driver        Audio output driver. Valid drivers: ");

#ifdef COMPILE_PULSE
        printf ("pulse ");
#endif

#ifdef COMPILE_ALSA
        printf ("alsa ");
#endif

#ifdef COMPILE_SDL
        printf ("sdl ");
#endif

#ifdef COMPILE_DSP
        printf ("dsp ");
#endif

#ifdef COMPILE_PCSPEAKER
        printf ("pcspeaker ");
#endif

#ifdef COMPILE_COREAUDIO
        printf ("coreaudio ");
#endif


        printf ("null\n");




#ifdef USE_SNDFILE
	printf ("--aofile file      Also output sound to wav or raw file\n");
#else
	printf ("--aofile file      Also output sound to raw file\n");
#endif

	printf ("--version          Get emulator version and exit. Must be the first command line setting\n");

	printf ("\n");


		printf ("\n"
		"--noconfigfile     Do not load configuration file. This parameter must be the first and it's ignored if written on config file\n"
		"--configfile f     Use the specified config file. This parameter must be the first and it's ignored if written on config file\n"
		"--experthelp       Show expert options\n"
		"\n"
		"Any command line setting shown here or on experthelp can be written on a configuration file,\n"
		"this configuration file is on your home directory with name: " DEFAULT_ZESARUX_CONFIG_FILE "\n"

		"\n"

#ifdef MINGW
		"Note: On Windows command prompt, all emulator messages are supressed (except the initial copyright message).\n"
		"To avoid this, you must specify at least one parameter on command line.\n\n"
#endif

              );

}

void cpu_help_expert(void)
{

	printf ("Expert options, in sections: \n"
		"\n"
		"\n"
		"Debugging\n"
		"---------\n"
		"\n"
		"--verbose n                 Verbose level n (0=only errors, 1=warning and errors, 2=info, warning and errors, 3=debug, 4=lots of messages)\n"
        "--disable-debug-console-win Disable debug console window\n"
		"--verbose-always-console    Always show messages in console (using simple printf) additionally to the default video driver, interesting in some cases as curses, aa or caca video drivers\n"
		"--debugregisters            Debug CPU Registers on text console\n"
	    "--showcompileinfo           Show compilation information\n"
		"--debugconfigfile           Debug parsing of configuration file (and .config files). This parameter must be the first and it's ignored if written on config file\n"
		"--testconfig                Test configuration and exit without starting emulator\n"







		"--romfile file             Select custom ROM file\n"
		"--loadbinary file addr len Load binary file \"file\" at address \"addr\" with length \"len\". Set ln to 0 to load the entire file in memory\n"
		"--loadbinarypath path      Select initial Load Binary path\n"
		"--savebinarypath path      Select initial Save Binary path\n"
		"--keyboardspoolfile file   Insert spool file for keyboard presses\n"
		"--keyboardspoolfile-play   Play spool file right after starting the emulated machine\n"
#ifdef USE_PTHREADS
		"--enable-remoteprotocol    Enable remote protocol\n"
		"--remoteprotocol-port n    Set remote protocol port (default: 10000)\n"
#endif


		"--showfiredbreakpoint n    Tells to show the breakpoint condition when it is fired. Possible values: \n"
		"                           0: always shows the condition\n"
		"                           1: only shows conditions that are not like PC=XXXX\n"
		"                           2: never shows conditions\n"

#ifdef MINGW
		"--nodisableconsole         Do not disable text output on this console. On Windows, text output is disabled unless you specify "
		"at least one parameter on command line, or this parameter on command line or on configuration file. \n"
#endif

	    "--enable-breakpoints       Enable breakpoints handling.\n"



	    "--brkp-always              Fire a breakpoint when it is fired always, not only when the condition changes from false to true\n"


		"--show-display-debug       Shows emulated screen on every key action on debug registers menu\n"


		"--show-electron-debug      Shows TV electron position when debugging, using a coloured line\n"


	    "--show-invalid-opcode      If running invalid cpu opcodes will generate a warning message\n"


);

printf(
		"--set-breakpoint n s       Set breakpoint with string s at position n. n must be between 1 and %d. string s must be written in \"\" if has spaces. Used normally with --enable-breakpoints\n",MAX_BREAKPOINTS_CONDITIONS
);

printf(
		"--set-breakpointaction n s Set breakpoint action with string s at position n. n must be between 1 and %d. string s must be written in \"\" if has spaces. Used normally with --enable-breakpoints\n",MAX_BREAKPOINTS_CONDITIONS
);

printf(
		"--set-watch n s            Set watch with string s at position n. n must be between 1 and %d. string s must be written in \"\" if has spaces\n",DEBUG_MAX_WATCHES
);

printf (
	  "--set-mem-breakpoint a n   Set memory breakpoint at address a for type n\n"
	  "--hardware-debug-ports     These ports are used to interact with ZEsarUX, for example showing a ASCII character on console, read ZEsarUX version, etc. "
		"Read file extras/docs/zesarux_zxi_registers.txt for more information\n"
	  "--hardware-debug-ports-byte-file f  Sets the file used on register HARDWARE_DEBUG_BYTE_FILE\n"

	  "--dump-ram-to-file f       Dump memory from 4000h to ffffh to a file, when exiting emulator\n"
		"\n"
		"\n"
		"CPU Core\n"
		"--------\n"
		"\n"


		"--cpuspeed n               Set CPU speed in percentage\n"

		"--random-r-register        Generate random value for R register on every cold start, instead of the normal 0 value. Useful to avoid same R register in the start of games, when they use that register as a random value\n"

		"\n"

		"\n"
		"\n"
		"Audio Features\n"
		"--------------\n"
		"\n"

		"--disableayspeech          Disable AY Speech sounds\n"
		"--disableenvelopes         Disable AY Envelopes\n"
		"--disablebeeper            Disable Beeper\n"
    "--disablerealbeeper        Disable real Beeper sound\n"
		"--totalaychips  n          Number of ay chips. Default 1\n"
		"--ay-stereo-mode n         Mode of AY stereo emulated: 0=Mono, 1=ACB, 2=ABC, 3=BAC, 4=Custom. Default Mono\n"
		"--ay-stereo-channel X n    Position of AY channel X (A, B or C) in case of Custom Stereo Mode. 0=Left, 1=Center, 2=Right\n"

		"--enableaudiodac           Enable DAC emulation. By default Specdrum\n"
		"--audiodactype type        Select one of audiodac types: "
);
		audiodac_print_types();

printf (
		"\n"
		"--audiovolume n            Sets the audio output volume to percentage n\n"

		"--ayplayer-end-exit        Exit emulator when end playing current ay file\n"
		"--ayplayer-end-no-repeat   Do not repeat playing from the beginning when end playing current ay file\n"
		"--ayplayer-inf-length n    Limit to n seconds to ay tracks with infinite length\n"
		"--ayplayer-any-length n    Limit to n seconds to all ay tracks\n"
		"--enable-midi              Enable midi output\n"
		"--midi-client n            Set midi client value to n. Needed only on Linux with Alsa audio driver\n"
		"--midi-port n              Set midi port value to n. Needed on Windows and Linux with Alsa audio driver\n"
		"--midi-raw-device s        Set midi raw device to s. Needed on Linux with Alsa audio driver\n"
		"--midi-allow-tone-noise    Allow tone+noise channels on midi\n"
		"--midi-no-raw-mode         Do not use midi in raw mode. Raw mode is required on Linux to emulate AY midi registers\n"


		"\n"
		"\n"
		"Audio Driver Settings\n"
		"---------------------\n"
		"\n"

		"--noreset-audiobuffer-full Do not reset audio buffer when it's full. By default it does reset the buffer when full, it helps reducing latency\n"

#ifdef COMPILE_PCSPEAKER
		"--pcspeaker-wait-time      Wait time between every audio byte sent. Values between 0 and 64 microseconds\n"
#endif



#ifdef COMPILE_ALSA
		"--alsaperiodsize n         Alsa audio periodsize multiplier (2 or 4). Default 2. Lower values reduce latency but can increase cpu usage\n"


#ifdef USE_PTHREADS

		"--fifoalsabuffersize n     Alsa fifo buffer size multiplier (4 to 10). Default 4. Lower values reduce latency but can increase cpu usage\n"

#endif
#endif



#ifdef COMPILE_PULSE
#ifdef USE_PTHREADS
                "--pulseperiodsize n        Pulse audio periodsize multiplier (1 to 4). Default 2. Lower values reduce latency but can increase cpu usage\n"
                "--fifopulsebuffersize n    Pulse fifo buffer size multiplier (4 to 10). Default 10. Lower values reduce latency but can increase cpu usage\n"
#endif
#endif

#ifdef COMPILE_COREAUDIO
								"--fifocorebuffersize n     Coreaudio fifo buffer size multiplier (2 to 10). Default 2. Lower values reduce latency but can increase cpu usage\n"
#endif

#ifdef COMPILE_SDL
		);


		//Cerramos el printf para que quede mas claro que hacemos un printf con un parametro
		printf (
		"--sdlsamplesize n          SDL audio sample size (128 to 2048). Default %d. Lower values reduce latency but can increase cpu usage\n",DEFAULT_AUDIOSDL_SAMPLES);
		printf (
		"--fifosdlbuffersize n      SDL fifo buffer size multiplier (2 to 10). Default 2. Lower values reduce latency but can increase cpu usage\n"
		"--sdlrawkeyboard           SDL read keyboard in raw mode, needed for ZX Recreated to work well\n");


		printf (
#endif



		"\n"
		"\n"
		"Display Settings\n"
		"----------------\n"
		"\n"

		"--realvideo                Enable real video display - for Spectrum (rainbow and other advanced effects) and ZX80/81 (non standard & hi-res modes)\n"
		"--no-detect-realvideo      Disable real video autodetection\n"

		"--tbblue-legacy-hicolor    Allow legacy hi-color effects on pixel/attribute display zone\n"
		"--tbblue-legacy-border     Allow legacy border effects on tbblue machine\n"

		"--enableulaplus            Enable ULAplus video modes\n"
		"--enabletimexvideo         Enable Timex video modes\n"
		"--disablerealtimex512      Disable real Timex mode 512x192. In this case, it's scalled to 256x192 but allows scanline effects\n"
		"--no-ocr-alternatechars    Disable looking for an alternate character set other than the ROM default on OCR functions\n"
		"--scr file                 Load Screen File at startup\n"



		"\n"
		"\n"
		"Video Driver Settings\n"
		"---------------------\n"
		"\n"


#ifdef USE_XEXT
	        "--disableshm               Disable X11 Shared Memory\n"
#endif

		"--nochangeslowparameters   Do not change any performance parameters (frameskip, realvideo, etc) "
		"on slow machines like raspberry, etc\n"



#ifdef COMPILE_FBDEV
                "--no-use-ttyfbdev          Do not use a tty on fbdev driver. It disables keyboard\n"
                "--no-use-ttyrawfbdev       Do not use keyboard on raw mode for fbdev driver\n"
                "--use-all-res-fbdev        Use all virtual resolution on fbdev driver. Experimental feature\n"
                "--decimal-full-scale-fbdev Use non integer zoom to fill the display with full screen mode on fbdev driver\n"
#ifdef EMULATE_RASPBERRY
                "--fbdev-margin-width n     Increment fbdev width size on n pixels on Raspberry full screen\n"
                "--fbdev-margin-height n    Increment fbdev width height on n pixels on Raspberry full screen\n"
#endif

#endif


		"\n"
		"\n"
		"GUI Settings\n"
		"---------------------\n"
		"\n"


		//"--invespokerom n           Inves rom poke value\n"

		"--zoomx n                  Horizontal Zoom Factor\n"
		"--zoomy n                  Vertical Zoom Factor\n"

		"--frameskip n              Set frameskip (0=none, 1=25 FPS, 2=16 FPS, etc)\n"
		"--disable-autoframeskip    Disable autoframeskip\n"


		"--fullscreen               Enable full screen\n"
		"--disableborder            Disable Border\n"
		"--hidemousepointer         Hide Mouse Pointer. Not all video drivers support this\n"
		"--disablemenumouse         Disable mouse on emulator menu\n"


		"--disablefooter            Disable window footer\n"
		"--disablemultitaskmenu     Disable multitasking menu\n"
		//"--disablebw-no-multitask   Disable changing to black & white colours on the emulator machine when menu open and multitask is off\n"
		"--nosplash                 Disable all splash texts\n"
#ifndef MINGW
		"--no-cpu-usage             Do not show host CPU usage on footer\n"
#endif

		"--no-cpu-temp              Do not show host CPU temperature on footer\n"
		"--no-fps                   Do not show FPS on footer\n"



		"--hide-menu-percentage-bar  Hides vertical percentaje bar on the right of text windows and file selector\n"
		"--hide-menu-minimize-button Hides minimize button on the title window\n"
		"--hide-menu-close-button    Hides close button on the title window\n"
		"--invert-menu-mouse-scroll  Inverts mouse scroll movement\n"
		"--allow-background-windows  Allow putting windows in background\n"
        "--allow-background-windows-closed-menu  Allow these background windows even with menu closed\n"
		);

	printf (
		"--menu-mix-method s         How to mix menu and the layer below. s should be one of:");

		int i;
		for (i=0;i<MAX_MENU_MIX_METHODS;i++) {
			printf ("%s ",screen_menu_mix_methods_strings[i]);
		}

		printf ("\n");



printf (

		"--menu-transparency-perc n Transparency percentage to apply to menu\n"
		"--menu-darken-when-open    Darken layer below menu when menu open\n"
		"--menu-bw-multitask        Grayscale layer below menu when menu opened and multitask is disabled\n"



		"--nowelcomemessage         Disable welcome message\n"
		"--red                      Force display mode with red colour\n"
		"--green                    Force display mode with green colour\n"
		"--blue                     Force display mode with blue colour\n"
		"  Note: You can combine colours, for example, --red --green for Yellow display, or --red --green --blue for Gray display\n"
		"--inversevideo             Inverse display colours\n"
		"--realpalette              Use real Spectrum colour palette according to info by Richard Atkinson\n"
		"--disabletooltips          Disable tooltips on menu\n"
		"--no-first-aid s           Disable first aid message s. Do not throw any error if invalid\n"
		"--disable-all-first-aid    Disable all first aid messages\n"
		"--forcevisiblehotkeys      Force always show hotkeys. By default it will only be shown after a timeout or wrong key pressed\n"
		"--forceconfirmyes          Force confirmation dialogs yes/no always to yes\n"
		"--gui-style s              Set GUI style. Available: ");

		estilo_gui_retorna_nombres();


printf (
		"\n"
		"--hide-dirs                Do not show directories on file selector menus\n"
		"--no-file-previews         Do not show file previews on file selector menus\n"
		"--limitopenmenu            Limit the action to open menu (F5 by default, joystick button). To open it, you must press the key 3 times in one second\n"
		"--setmachinebyname         Select machine by name instead of manufacturer\n"
		"--disablemenu              Disable menu\n"
		"--disablemenuandexit       Disable menu. Any event that opens the menu will exit the emulator\n"
		"--disablemenufileutils     Disable File Utilities menu\n"


		"--text-keyboard-add text   Add a string to the Adventure Text OSD Keyboard. The first addition erases the default text keyboard.\n"
		" You can use hotkeys by using double character ~~ just before the letter, for example:\n"
		" --text-keyboard-add ~~north   --text-keyboard-add e~~xamine\n");

printf (
		"--text-keyboard-length n   Define the duration for every key press on the Adventure Text OSD Keyboard, in 1/50 seconds (default %d). Minimum 10, maximum 100\n"
		"The half of this value, the key will be pressed, the other half, released. Example: --text-keyboard-length 50 to last 1 second\n",
		DEFAULT_ADV_KEYBOARD_KEY_LENGTH);

printf (
		"--text-keyboard-finalspc   Sends a space after every word on the Adventure Text OSD Keyboard\n"
		//"--text-keyboard-clear      Clear all entries of the Adventure Text Keyboard\n"
		"\n"
		"\n"
		"Hardware Settings\n"
		"-----------------\n"
		"\n"

		"--printerbitmapfile f      Sends printer output to image file. Supported formats: pbm, txt\n"
		"--printertextfile f        Sends printer output to text file using OCR method. Printer output is saved to a text file using OCR method to guess text.\n"
		"--redefinekey src dest     Redefine key scr to be key dest. You can write maximum 10 redefined keys\n"
        "                           Key must be ascii character numbers or a character included in escaped quotes, like: 97 (for 'a') or \\'q\\'\n"
        "                           (the escaped quotes are used only in command line; on configuration file, they are normal quotes '')\n"

        "--recreatedzx              Enable support for Recreated ZX Spectrum Keyboard\n"

		"--keymap n                 Which kind of physical keyboard you have. Default 0 (English) or 1 (Spanish)\n"


		);


		printf (
	  "--def-f-function key action  Define F key to do an action. action can be: ");


			for (i=0;i<MAX_F_FUNCTIONS;i++) {
				printf ("%s ",defined_f_functions_array[i].texto_funcion);
			}



		printf (
		"\n"
		"--enablekempstonmouse      Enable kempston mouse emulation\n"
		"--kempstonmouse-sens n     Set kempston mouse sensitivity (1-%d)\n",MAX_KMOUSE_SENSITIVITY);

		printf (
		"--spectrum-reduced-core    Use Spectrum reduced core. It uses less cpu, ideal for slow devices like Raspberry Pi One and Zero\n"
		"                           The following features will NOT be available or will NOT be properly emulated when using this core:\n"
		"                           Debug t-states, Char detection, +3 Disk, Save to tape, Divide, Divmmc, RZX, Raster interrupts, TBBlue Copper, Audio DAC, Video out to file\n"
		"--no-spectrum-reduced-core Do not use Spectrum reduced core\n"
                "\n"
                "Memory Settings\n"
                "-----------------\n"
                "\n"

		"--acemem n                 Emulate 3, 19 or 35 kb of memory on Jupiter Ace\n"

		"--128kmem n                Set more than 128k RAM for 128k machines. Allowed values: 128, 256, 512"



		"\n"

		"\n"
		"\n"
		"Storage Settings\n"
		"----------------\n"
		"\n"


		"--mmc-file f               Set mmc image file\n"
		"--enable-mmc               Enable MMC emulation. Usually requires --mmc-file\n"
		"--mmc-write-protection     Enable MMC write protection\n"
		"--mmc-no-persistent-writes Disable MMC persistent writes\n"

		"--enable-divmmc-ports      Enable DIVMMC emulation ports only, but not paging. Usually requires --enable-mmc\n"
		"--enable-divmmc-paging     Enable DIVMMC paging only\n"
		"--enable-divmmc            Enable DIVMMC emulation: ports & paging. Usually requires --enable-mmc\n"



		"\n"
                "\n"
                "External Tools\n"
                "--------------\n"
                "\n"


                "--tool-sox-path p          Set external tool sox path. Path can not include spaces\n"
                "--tool-gunzip-path p       Set external tool gunzip path. Path can not include spaces\n"
                "--tool-tar-path p          Set external tool tar path. Path can not include spaces\n"
                "--tool-unrar-path p        Set external tool unrar path. Path can not include spaces\n"


		"\n"
		"\n"
		"Joystick\n"
		"--------\n"
		"\n"


		"--joystickemulated type    Type of joystick emulated. Type can be one of: ");

	joystick_print_types();
		printf (" (default: %s).\n",joystick_texto[joystick_emulation]);
		printf ("  Note: if a joystick type has spaces in its name, you must write it between \"\"\n");


	printf(
		"--disablerealjoystick      Disable real joystick emulation\n"
		"--realjoystickpath f       Change default real joystick device path\n"
		"--realjoystick-calibrate n Parameter to autocalibrate joystick axis. Axis values read from joystick less than n and greater than -n are considered as 0. Default: 16384. Not used on native linux real joystick\n"

#ifdef USE_LINUXREALJOYSTICK
		"--no-native-linux-realjoy  Do not use native linux real joystick support. Instead use the video driver joystick support (currently only SDL)\n"
#endif
                "--joystickevent but evt    Set a joystick button or axis to an event (changes joystick to event table)\n"
                "                           If it's a button (not axis), must be specified with its number, without sign, for example: 2\n"
                "                           If it's axis, must be specified with its number and sign, for example: +2 or -2\n"
                "                           Event must be one of: ");

                realjoystick_print_event_keys();

	printf ("\n"
		"--joystickkeybt but key    Define a key pressed when a joystick button pressed (changes joystick to key table)\n"
		"                           If it's a button (not axis), must be specified with its number, without sign, for example: 2\n"
        "                           If it's axis, must be specified with its number and sign, for example: +2 or -2\n"
        "                           Key must be an ascii character number or a character included in escaped quotes, like: 13 (for enter) or \\'q\\'\n"
		"                           (the escaped quotes are used only in command line; on configuration file, they are normal quotes '')\n"
		"                           Note: to simulate Caps shift, use key value 128, and to simulate Symbol shift, use key value 129\n"



                "--joystickkeyev evt key    Define a key pressed when a joystick event is generated (changes joystick to key table)\n"
                "                           Event must be one of: ");

                realjoystick_print_event_keys();


	printf ("\n"
		"                           Key must be an ascii character number or a character included in escaped quotes, like: 13 (for enter) or \\'q\\' \n"
		"                           (the escaped quotes are used only in command line; on configuration file, they are normal quotes '')\n"
		"                           Note: to simulate Caps shift, use key value 128, and to simulate Symbol shift, use key value 129\n"

        	"\n"
		"  Note: As you may see, --joystickkeyev is not dependent on the real joystick type you use, because it sets an event to a key, "
		"and --joystickkeybt and --joystickevent are dependent on the real joystick type, because they set a button/axis number to "
		"an event or key, and button/axis number changes depending on the joystick (the exception here is the axis up/down/left/right "
		"which are the same for all joysticks: up: -1, down: +1, left: -1, right: +1)\n"

		"\n"
		"--clearkeylistonsmart      Clear all joystick (events and buttons) to keys table every smart loading.\n"
		"                           Joystick to events table is never cleared using this setting\n"
		"--cleareventlist           Clears joystick to events table\n"
		"--enablejoysticksimulator  Enable real joystick simulator. Only useful on development\n"


		"\n"
		"\n"
		"Network\n"
		"-------\n"
		"\n"

		"--zeng-remote-hostname s   ZENG last remote hostname\n"
		"--zeng-remote-port n       ZENG last remote port\n"
		"--zeng-iam-master          Tells this machine is a ZENG master\n"



		"\n"
		"\n"
		"Statistics\n"
		"----------\n"
		"\n"

		"--total-minutes-use n          Total minutes of use of ZEsarUX\n"
		"--stats-send-already-asked     Do not ask to send statistics\n"
		"--stats-send-enabled           Enable send statistics\n"
		"--stats-uuid s                 UUID to send statistics\n"
		"--stats-disable-check-updates  Disable checking of available ZEsarUX updates\n"
		"--stats-disable-check-yesterday-users  Disable checking ZEsarUX yesterday users\n"

		"--stats-last-avail-version s   ZEsarUX last available version to download\n"


		"--stats-speccy-queries n       Total queries on the speccy online browser\n"







		"\n"
		"\n"
		"Miscellaneous\n"
		"-------------\n"
		"\n"



		"--saveconf-on-exit         Always save configuration when exiting emulator\n"
		"--helpcustomconfig         Show help for autoconfig files\n"
		"--quickexit                Exit emulator quickly: no yes/no confirmation\n"
		"--exit-after n             Exit emulator after n seconds\n"
		"--last-version s           String which identifies last version run. Usually doesnt need to change it, used to show the start popup of the new version changes\n"
		"--no-show-changelog        Do not show changelog when updating version\n"
		"--disablebetawarning text  Do not pause beta warning message on boot for version named as that parameter text\n"
		"--windowgeometry s x y w h Set window geometry. Parameters: window name (s), x coord, y coord, width (w), height (h)\n"
		"--enable-restore-windows   Allow restore windows on start\n"
        "--restorewindow s          Restore window s on start\n"


		"--clear-all-windowgeometry Clear all windows geometry thay may be loaded from the configuration file\n"



		"--tbblue-autoconfigure-sd-already-asked     Do not ask to autoconfigure tbblue initial SD image\n"

		//Esto no hace falta que lo vea un usuario, solo lo uso yo para probar partes del emulador
		"--tonegenerator n          Enable tone generator. Possible values: 1: generate max, 2: generate min, 3: generate min/max at 50 Hz\n"


		"\n\n"

	);

}

#ifdef USE_COCOA
int set_scrdriver_cocoa(void)
{
                        scr_refresca_pantalla=scrcocoa_refresca_pantalla;
												scr_refresca_pantalla_solo_driver=scrcocoa_refresca_pantalla_solo_driver;
                        scr_init_pantalla=scrcocoa_init;
                        scr_end_pantalla=scrcocoa_end;
                        scr_lee_puerto=scrcocoa_lee_puerto;
                        scr_actualiza_tablas_teclado=scrcocoa_actualiza_tablas_teclado;
        return 0;
}

#endif


#ifdef COMPILE_XWINDOWS
int set_scrdriver_xwindows(void)
{
                        scr_refresca_pantalla=scrxwindows_refresca_pantalla;
												scr_refresca_pantalla_solo_driver=scrxwindows_refresca_pantalla_solo_driver;
                        scr_init_pantalla=scrxwindows_init;
                        scr_end_pantalla=scrxwindows_end;
                        scr_lee_puerto=scrxwindows_lee_puerto;
                        scr_actualiza_tablas_teclado=scrxwindows_actualiza_tablas_teclado;
                        //scr_debug_registers=scrxwindows_debug_registers;
			//scr_messages_debug=scrxwindows_messages_debug;
	return 0;
}

#endif

#ifdef COMPILE_SDL
int set_scrdriver_sdl(void)
{
                        scr_refresca_pantalla=scrsdl_refresca_pantalla;
												scr_refresca_pantalla_solo_driver=scrsdl_refresca_pantalla_solo_driver;
                        scr_init_pantalla=scrsdl_init;
                        scr_end_pantalla=scrsdl_end;
                        scr_lee_puerto=scrsdl_lee_puerto;
                        scr_actualiza_tablas_teclado=scrsdl_actualiza_tablas_teclado;
        return 0;
}

#endif



#ifdef COMPILE_FBDEV
int set_scrdriver_fbdev(void)
{
                        scr_refresca_pantalla=scrfbdev_refresca_pantalla;
												scr_refresca_pantalla_solo_driver=scrfbdev_refresca_pantalla_solo_driver;
                        scr_init_pantalla=scrfbdev_init;
                        scr_end_pantalla=scrfbdev_end;
                        scr_lee_puerto=scrfbdev_lee_puerto;

			//la rutina de tabla teclado la establecemos en el init... pues se cambia segun si modo raw o no
                        //scr_actualiza_tablas_teclado=scrfbdev_actualiza_tablas_teclado;
                        //scr_debug_registers=scrfbdev_debug_registers;
                        //scr_messages_debug=scrfbdev_messages_debug;
        return 0;
}

#endif



int set_scrdriver_null(void) {
scr_refresca_pantalla=scrnull_refresca_pantalla;
scr_refresca_pantalla_solo_driver=scrnull_refresca_pantalla_solo_driver;
scr_init_pantalla=scrnull_init;
scr_end_pantalla=scrnull_end;
scr_lee_puerto=scrnull_lee_puerto;
scr_actualiza_tablas_teclado=scrnull_actualiza_tablas_teclado;
//scr_debug_registers=scrnull_debug_registers;
//			scr_messages_debug=scrnull_messages_debug;
	return 0;
}


#ifdef COMPILE_DSP
int set_audiodriver_dsp(void) {
                        audio_init=audiodsp_init;
                        audio_send_frame=audiodsp_send_frame;
			audio_thread_finish=audiodsp_thread_finish;
			audio_end=audiodsp_end;
			audio_get_buffer_info=audiodsp_get_buffer_info;
			return 0;

                }
#endif


#ifdef COMPILE_PCSPEAKER
int set_audiodriver_pcspeaker(void) {
                        audio_init=audiopcspeaker_init;
                        audio_send_frame=audiopcspeaker_send_frame;
			audio_thread_finish=audiopcspeaker_thread_finish;
			audio_end=audiopcspeaker_end;
			audio_get_buffer_info=audiopcspeaker_get_buffer_info;
			return 0;

                }
#endif



#ifdef COMPILE_SDL
int set_audiodriver_sdl(void) {
                        audio_init=audiosdl_init;
                        audio_send_frame=audiosdl_send_frame;
                        audio_thread_finish=audiosdl_thread_finish;
                        audio_end=audiosdl_end;
												audio_get_buffer_info=audiosdl_get_buffer_info;
                        return 0;

                }
#endif


#ifdef COMPILE_ALSA
int set_audiodriver_alsa(void) {
                        audio_init=audioalsa_init;
                        audio_send_frame=audioalsa_send_frame;
			audio_thread_finish=audioalsa_thread_finish;
			audio_end=audioalsa_end;
			audio_get_buffer_info=audioalsa_get_buffer_info;
			return 0;

                }
#endif

#ifdef COMPILE_PULSE
int set_audiodriver_pulse(void) {
                        audio_init=audiopulse_init;
                        audio_send_frame=audiopulse_send_frame;
                        audio_thread_finish=audiopulse_thread_finish;
			audio_end=audiopulse_end;
			audio_get_buffer_info=audiopulse_get_buffer_info;
                        return 0;

                }
#endif


#ifdef COMPILE_COREAUDIO
int set_audiodriver_coreaudio(void) {
                        audio_init=audiocoreaudio_init;
                        audio_send_frame=audiocoreaudio_send_frame;
                        audio_thread_finish=audiocoreaudio_thread_finish;
			audio_end=audiocoreaudio_end;
			audio_get_buffer_info=audiocoreaudio_get_buffer_info;
                        return 0;

                }
#endif

//Randomize Usando segundos del reloj
//Usado en AY Chip y Random ram
void init_randomize_noise_value(void)
{

                int chips=ay_retorna_numero_chips();

                int i;

                for (i=0;i<chips;i++) {

	                gettimeofday(&z80_interrupts_timer_antes, NULL);
        	        randomize_noise[i]=z80_interrupts_timer_antes.tv_sec & 0xFFFF;
                	//printf ("randomize vale: %d\n",randomize_noise);

		}
}


void random_ram(z80_byte *puntero,int longitud)
{

	//Cada vez que se inicializa una maquina, reasignar valor randomize
	init_randomize_noise_value();

	z80_byte valor, valor_h, valor_l;

	for (;longitud;longitud--,puntero++) {


		ay_randomize(0);

                valor_h=value_16_to_8h(randomize_noise[0]);
                valor_l=value_16_to_8l(randomize_noise[0]);
		valor=valor_h ^ valor_l;

		*puntero=valor;
	}

}


void avisar_opcion_obsoleta(char *texto){
	//Solo avisa si no hay guardado de configuracion
	//Porque si hay guardado, al guardarlo ya lo grabara como toca
	if (save_configuration_file_on_exit.v==0) debug_printf (VERBOSE_ERR,"%s",texto);
}

//Patron de llenado para inves: FF,00,FF,00, etc...
void random_ram_inves(z80_byte *puntero,int longitud)
{

        z80_byte valor=255;

        for (;longitud;longitud--,puntero++) {

                //printf ("random:%d\n",valor);
                *puntero=valor;

		valor = valor ^255;
        }

	//asumimos que el ultimo valor enviado desde menu sera el por defecto (255)
	last_inves_low_ram_poke_menu=255;

}



struct s_machine_names machine_names[]={

//char *machine_names[]={
                                            {"ZX Spectrum 16k",              	0},
                                            {"ZX Spectrum 48k", 			1},
                                            {"Inves Spectrum+",			2},
                                            {"Microdigital TK90X",		3},
                                            {"Microdigital TK90X (Spanish)",	4},
                                            {"Microdigital TK95",		5},
                                            {"ZX Spectrum+ 128k",			6},
                                            {"ZX Spectrum+ 128k (Spanish)",		7},
                                            {"ZX Spectrum +2",			8},
                                            {"ZX Spectrum +2 (French)",		9},
                                            {"ZX Spectrum +2 (Spanish)",		10},
                                            {"ZX Spectrum +2A (ROM v4.0)",		11},
                                            {"ZX Spectrum +2A (ROM v4.1)",		12},
                                            {"ZX Spectrum +2A (Spanish)",		13},
					    {"ZX-Uno",         			14},
					    {"Chloe 140SE",    			15},
					    {"Chloe 280SE",    			16},
			   		    {"Timex TS2068",   			17},
					    {"Prism 512",       			18},
					    {"TBBLue",   			19},
					    {"ZX Spectrum+ 48k (Spanish)",		20},
					    {"Pentagon",		21},
							{"Chrome", MACHINE_ID_CHROME},
							{"ZX-Evolution TS-Conf", MACHINE_ID_TSCONF},
							{"ZX-Evolution BaseConf", MACHINE_ID_BASECONF},


                                            {"ZX Spectrum +3 (ROM v4.0)",		MACHINE_ID_SPECTRUM_P3_40},
                                            {"ZX Spectrum +3 (ROM v4.1)",		MACHINE_ID_SPECTRUM_P3_41},
                                            {"ZX Spectrum +3 (Spanish)",		MACHINE_ID_SPECTRUM_P3_SPA},

{"MSX1",MACHINE_ID_MSX1},
{"ColecoVision",MACHINE_ID_COLECO},
{"Spectravideo 318",MACHINE_ID_SVI_318},
{"Spectravideo 328",MACHINE_ID_SVI_328},

                                            {"ZX80",  				120},
                                            {"ZX81",  				121},
					    {"Jupiter Ace",  			122},
					    {"Z88",  				130},
				            {"CPC 464",  			MACHINE_ID_CPC_464},
							{"CPC 4128",  			MACHINE_ID_CPC_4128},
					    {"Sam Coupe", 			150},
					    {"QL",				160},
							{"MK14", MACHINE_ID_MK14_STANDARD},

						//Indicador de final
					{"",0}

};




char *get_machine_name(z80_byte machine)
{
	int i;

	for (i=0;i<99999;i++) {
		if (machine_names[i].nombre_maquina[0]==0) {
			char mensaje[200];
			sprintf (mensaje,"No machine name found for machine id: %d",machine);
			cpu_panic(mensaje);
		}

		if (machine_names[i].id==machine) return machine_names[i].nombre_maquina;
	}

	//Aunque aqui no se llega nunca, para que no se queje el compilador
	return NULL;
}

//int maquina_anterior_cambio_cpu_speed=-1;

//ajustar timer de final de cada frame de pantalla. En vez de lanzar un frame cada 20 ms, hacerlo mas o menos rapido
void set_emulator_speed(void)
{

	//Calcular velocidad. caso normal que porcentaje=100, valor queda timer_sleep_machine=original_timer_sleep_machine*100/100 = original_timer_sleep_machine
	timer_sleep_machine=original_timer_sleep_machine*100/porcentaje_velocidad_emulador;
	if (timer_sleep_machine==0) timer_sleep_machine=1;

	//Si ha cambiado velocidad, reiniciar driver audio con frecuencia adecuada
	if (anterior_porcentaje_velocidad_emulador!=porcentaje_velocidad_emulador) {
		if (audio_end!=NULL) audio_end();
		frecuencia_sonido_variable=FRECUENCIA_CONSTANTE_NORMAL_SONIDO*porcentaje_velocidad_emulador/100;
		if (audio_init!=NULL) {
			if (audio_init()) {
				//Error
				fallback_audio_null();
			}
		}
	}

	anterior_porcentaje_velocidad_emulador=porcentaje_velocidad_emulador;

	debug_printf (VERBOSE_INFO,"Setting timer_sleep_machine to %d us",timer_sleep_machine);

	/*
	Lo anterior cubre los casos:
	-cambio de maquina de spectrum a z88: se recalcula timer_sleep_machine, sin tener que reiniciar driver audio
	-cambio de porcentaje cpu: se recalcula timer_sleep_machine y se reinicia driver audio si hay cambio porcentaje velocidad
	*/

}

/*
void set_emulator_speed(void)
{
	//ajuste mediante t estados por linea. requiere desactivar realvideo
	//ademas la tabla de memoria contended se saldra de rango...
	screen_testados_linea=screen_testados_linea*porcentaje_velocidad_emulador/100;

	printf ("t estados por linea: %d\n",screen_testados_linea);

}
*/


//ajusta max_cpu_cycles segun porcentaje cpu
//desactivado de momento
void old_set_emulator_speed(void)
{

	return ;

	//Esto se hacia antes cuando la cpu no estaba sincronizada y solo habia el contador de maximo de instrucciones
	//ahora habria que cuadrar muchos contadores en base a la velocidad: total_testates, testates de border, etc....

	if (porcentaje_velocidad_emulador==100) return;

	//controlar valores. por ejemplo, un porcentaje de 1, da un floating point exception en alguna parte del codigo...
	if (porcentaje_velocidad_emulador<10 || porcentaje_velocidad_emulador>1000) {
		debug_printf (VERBOSE_ERR,"Invalid value for cpu speed: %d",porcentaje_velocidad_emulador);
		return;
	}

	debug_printf (VERBOSE_INFO,"Setting cpu speed to: %d%%",porcentaje_velocidad_emulador);

	//lo hacemos multiple de 312
	//max_cpu_cycles=max_cpu_cycles/312;

	//aplicamos porcentaje
	//max_cpu_cycles=max_cpu_cycles*porcentaje_velocidad_emulador;
	//max_cpu_cycles=max_cpu_cycles/100;

	//y lo volvemos a dejar *312
	//max_cpu_cycles=max_cpu_cycles*312;

}


//asignar memoria de maquina, liberando memoria antes si conviene
void malloc_machine(int tamanyo)
{
	if (memoria_spectrum!=NULL) {
		debug_printf(VERBOSE_INFO,"Freeing previous Machine memory");
		free(memoria_spectrum);
	}

	debug_printf(VERBOSE_INFO,"Allocating %d bytes for Machine memory",tamanyo);
	memoria_spectrum=malloc(tamanyo);

	if (memoria_spectrum==NULL) {
		cpu_panic ("Error. Cannot allocate Machine memory");
	}


}

void malloc_mem_machine(void) {

        // 2048K SRAM + 8K BOOTROM + 8K "out of range"
        malloc_machine( TBBLUE_TOTAL_MEMORY_USED*1024);
        random_ram(memoria_spectrum,TBBLUE_TOTAL_MEMORY_USED*1024);

        tbblue_init_memory_tables();
}



void set_machine_params(void)
{

/*
0=Sinclair 16k
1=Sinclair 48k
2=Inves Spectrum+
3=tk90x
4=tk90xs
5=tk95
6=Sinclair 128k
7=Sinclair 128k Español
8=Amstrad +2
9=Amstrad +2 - Frances
10=Amstrad +2 - Espa�ol
11=Amstrad +2A (ROM v4.0)
12=Amstrad +2A (ROM v4.1)
13=Amstrad +2A - Espa�ol
14=ZX-Uno
15=Chloe 140SE
16=Chloe 280SE
17=Timex TS2068
18=Prism
19=TBBlue
20=Spectrum + Spanish
21=Pentagon
22=Chrome
23=ZX-Evolution TS-Conf
24=ZX-Evolution BaseConf (no implementado aun)

25=Amstrad +3 (ROM v4.0)
26=Amstrad +3 (ROM v4.1)
27=Amstrad +3 - Espa�ol

28-29 Reservado (Spectrum)
100=colecovision
102=Spectravideo 318
103=Spectravideo 328
110-119 msx:
110 msx1
120=zx80 (old 20)
121=zx81 (old 21)
122=jupiter ace (old 22)
130=z88 (old 30)
140=amstrad cpc464
141=amstrad cpc4128
150=Sam Coupe (old 50)
151-59 reservado para otros sam (old 51-59)
160=QL Standard
161-179 reservado para otros QL

180=MK14 Standard
181-189 reservado para otros MK14
*/

		//defaults
		ay_chip_present.v=0;

			//timer_sleep_machine=original_timer_sleep_machine=20000;
			original_timer_sleep_machine=20000;
        	        set_emulator_speed();

		allow_write_rom.v=0;

		multiface_enabled.v=0;

		//nota: combiene que allow_write_rom.v sea 0 al desactivar superupgrade
		//porque si estaba activo allow_write_rom.v antes, y desactivamos superupgrade,
		//al intentar desactivar allow_write, se produce segmentation fault

		//Si maquina anterior era pentagon, desactivar timing pentagon y activar contended memory
		if (last_machine_type==MACHINE_ID_PENTAGON) {
			debug_printf(VERBOSE_DEBUG,"Disabling pentagon timing and enabling contended memory because previous machine was Pentagon");
			pentagon_timing.v=0;
			contend_enabled.v=1;
		}

		input_file_keyboard_turbo.v=0;

		//Modo turbo no. No, parece que cambiarlo aqui sin mas no provoca cambios cuando se hace smartload
		//no tocarlo, dejamos que el usuario se encargue de ponerlo a 1 si quiere
		//printf ("Setting cpu speed to 1\n");
		//sleep (3);
		//cpu_turbo_speed=1;

		//en spectrum, 32. en pentagon, 36
		cpu_duracion_pulso_interrupcion=32;


		z80_cpu_current_type=Z80_TYPE_GENERIC;


		//cpu_core_loop=cpu_core_loop_spectrum;
			cpu_core_loop_active=CPU_CORE_SPECTRUM;

		debug_breakpoints_enabled.v=0;
		set_cpu_core_loop();

		//esto ya establece el cpu core. Debe estar antes de establecer peek, poke, lee_puerto, out_port
		//dado que "restaura" dichas funciones a sus valores originales... pero la primera vez, esos valores originales valen 0
		//breakpoints_disable();

		out_port=out_port_spectrum;


		//desactivar ram refresh emulation, cpu transaction log y cualquier otra funcion que altere el cpu_core, el peek_byte, etc
		cpu_transaction_log_enabled.v=0;
		cpu_code_coverage_enabled.v=0;
		cpu_history_enabled.v=0;

		push_valor=push_valor_default;
		extended_stack_enabled.v=0;


		//Valores usados en real video
		screen_invisible_borde_superior=8;
		screen_borde_superior=56;

		screen_total_borde_inferior=56;

		screen_total_borde_izquierdo=48;
		screen_total_borde_derecho=48;
		screen_invisible_borde_derecho=96;
		screen_testados_linea=224;

		//normalmente excepto +2a
		port_from_ula=port_from_ula_48k;



		//Resetear a zona memoria por defecto. Evita cuelgues al intentar usar una zona de memoria que ya no esta disponible,
		//ejemplo: iniciar maquina msx. abrir view sprites->activar hardware. F5 y cambiar a spectravideo. F5. Floating point exception.
		//menu_debug_set_memory_zone_mapped();
		//En principio esto ya no hace falta, desde menu_debug_set_memory_zone_attr, menu_debug_get_mapped_byte y menu_debug_write_mapped_byte
		//ya se está conmutando correctamente a memory mapped cuando la zona anterior ya no está disponible

		screen_set_parameters_slow_machines();

		//Cuando se viene aqui desde cambio modo turbo, no interesa vaciar buffer, si no, se oye fatal. Ejemplo: uwol en tsconf
		if (set_machine_empties_audio_buffer.v) audio_empty_buffer();

		if (set_machine_empties_audio_buffer.v) screen_init_colour_table();

		set_machine_empties_audio_buffer.v=1;



		datagear_dma_enable();
		//else datagear_dma_disable();

			tbblue_set_timing_48k();

		        //divmmc arranca desactivado, lo desactivamos asi para que no cambie las funciones peek/poke
			//esto ya se desactiva en set_machine
		        /*if (divmmc_enabled.v) {
		                divmmc_enabled.v=0;
		                diviface_enabled.v=0;
		        }
			*/


                poke_byte=poke_byte_tbblue;
                peek_byte=peek_byte_tbblue;
                peek_byte_no_time=peek_byte_no_time_tbblue;
                poke_byte_no_time=poke_byte_no_time_tbblue;
                lee_puerto=lee_puerto_spectrum;
                ay_chip_present.v=1;

				//Solo forzar real video una vez al entrar aquí. Para poder dejar real video desactivado si el usuario lo quiere,
				//pues aqui se entra siempre al cambiar velocidad cpu (y eso pasa en la rom cada vez que te mueves por el menu del 128k por ejemplo)
				//TODO: siempre que el usuario entre al emulador se activara la primera vez
				if (!tbblue_already_autoenabled_rainbow) {
					enable_rainbow();
					//lo mismo para timex video
					enable_timex_video();
				}

				tbblue_already_autoenabled_rainbow=1;

				multiface_type=MULTIFACE_TYPE_THREE;

								//Si maquina destino es tbblue, forzar a activar border. De momento no se ve bien con border desactivado
								border_enabled.v=1;


	debug_printf(VERBOSE_INFO,"Setting machine %s",get_machine_name(current_machine_type));

	//Recalcular algunos valores cacheados
	recalcular_get_total_ancho_rainbow();
	recalcular_get_total_alto_rainbow();



}

void set_menu_gui_zoom(void)
{
	menu_gui_zoom=2;

	debug_printf (VERBOSE_INFO,"Setting GUI menu zoom to %d",menu_gui_zoom);
}

void post_set_mach_reopen_screen(void)
{
				debug_printf(VERBOSE_INFO,"End Screen");
			scr_end_pantalla();
			debug_printf(VERBOSE_INFO,"Creating Screen");
			screen_init_pantalla_and_others_and_realjoystick();

			//scr_init_pantalla();
}

//Reabrir ventana en caso de que maquina seleccionada sea diferente a la anterior
void post_set_machine_no_rom_load_reopen_window(void)
{
	set_menu_gui_zoom();

	//printf ("last: %d current: %d\n",last_machine_type,current_machine_type);

	if (last_machine_type!=255 && last_machine_type!=current_machine_type) {
		debug_printf (VERBOSE_INFO,"Reopening window so current machine is different and may have different window size");
		//printf ("Reopening window so current machine is different and may have different window size\n");
		post_set_mach_reopen_screen();

		//Rearrange de ventanas en segundo plano, por si la maquina actual es una ventana de ZEsarUX mas pequeña
		//y se saldrian las ventanas zxvision de rango
		debug_printf (VERBOSE_DEBUG,"Rearrange zxvision windows so current machine is different and may have different window size");
		zxvision_rearrange_background_windows();
		return;
	}
}

/*
Vieja funcion
Reabrir ventana en caso de que maquina seleccionada tenga tamanyo diferente que la anterior
TODO: Quiza se podria simplificar esto, se empezó con Z88 a spectrum y se han ido agregando,
se exponen todos los casos de maquinas con diferentes tamanyos de ventana,
pero quiza simplemente habria que ver que el tamanyo anterior fuera diferente al actual y entonces reabrir ventana
O que la maquina actual es diferente de la anterior
*/
void old_post_set_machine_no_rom_load_reopen_window(void)
{

	set_menu_gui_zoom();


}

void post_set_machine_no_rom_load(void)
{

		screen_set_video_params_indices();
		inicializa_tabla_contend();

		init_rainbow();
		init_cache_putpixel();


		post_set_machine_no_rom_load_reopen_window();

		//printf ("antes init layers\n");
		scr_init_layers_menu();
		//printf ("despues init layers\n");
		scr_clear_layer_menu();


		last_machine_type=current_machine_type;
		menu_init_footer();



}

void post_set_machine(char *romfile)
{

                //leer rom
                debug_printf (VERBOSE_INFO,"Loading ROM");
                rom_load(romfile);

		post_set_machine_no_rom_load();
}


void set_machine(char *romfile)
{

	//Si estaba divmmc o divide activo, desactivarlos
	//if (divmmc_enabled.v) divmmc_disable();
	//if (divide_enabled.v) divide_disable();
	if (diviface_enabled.v) diviface_disable();

                //Si se cambia de maquina zxuno a otra no zxuno, desactivar divmmc
		/*
                if (last_machine_type!=255) {
                        if (last_machine_type==14 && machine_type!=14 && divmmc_enabled.v) {
                                debug_printf (VERBOSE_INFO,"Disabling divmmc because it was enabled on ZX-Uno");
                                divmmc_disable();
                        }
                }
		*/



	set_machine_params();
	malloc_mem_machine();

	//Si hay activo divmmc o divide, reactivar. Se pone aqui porque en caso de zxuno es necesario que esten inicializadas las paginas de memoria
	//if (divmmc_enabled.v) divmmc_enable();
	//if (divide_enabled.v) divide_enable();

	post_set_machine(romfile);
}



//Punto de entrada para error al cargar rom de cualquier modelo de spectrum, dado que se lee longitud de la rom diferente de la esperada
//panic
//void rom_load_cpu_panic(char *romfilename,int leidos)
//{
//	cpu_panic("Error loading ROM");
//}


void rom_load(char *romfilename)
{
                FILE *ptr_romfile;
		int leidos;


	if (romfilename==NULL) {
		romfilename="tbblue_loader.rom";
	}

                //ptr_romfile=fopen(romfilename,"rb");
		open_sharedfile(romfilename,&ptr_romfile);
                if (!ptr_romfile)
                {
                    debug_printf(VERBOSE_ERR,"Unable to open rom file %s",romfilename);

					//No hacemos mas un panic por esto. Ayuda a debugar posibles problemas con el path de inicio
					//cpu_panic("Unable to open rom file");

					return;
                }

		//Caso Inves. ROM esta en el final de la memoria asignada

				leidos=fread(tbblue_fpga_rom,1,8192,ptr_romfile);
                                if (leidos!=8192) {
                                        cpu_panic("Error loading ROM");
                                 }

                fclose(ptr_romfile);

}


//Para Windows con pthreads. En todos los sistemas, se permite main loop en pthread, excepto en Windows
#ifdef USE_PTHREADS
int si_thread_main_loop;


void ver_si_enable_thread_main_loop(void)
{

#ifdef MINGW
        si_thread_main_loop=0;
#else
        si_thread_main_loop=1;
#endif

}

#endif



void segint_signal_handler(int sig)
{

	debug_printf (VERBOSE_INFO,"Sigint (CTRL+C) received");

        //para evitar warnings al compilar
        sig++;

//Primero de todo detener el pthread del emulador, que no queremos que siga activo el emulador con el pthread de fondo mientras
//se ejecuta el end_emulator
#ifdef USE_PTHREADS
        if (si_thread_main_loop) {
        	debug_printf (VERBOSE_INFO,"Ending main loop thread");
		pthread_cancel(thread_main_loop);
	}
#endif


	end_emulator_saveornot_config(0);


}

void segterm_signal_handler(int sig)
{

        debug_printf (VERBOSE_INFO,"Sigterm received");

        //para evitar warnings al compilar
        sig++;

//Primero de todo detener el pthread del emulador, que no queremos que siga activo el emulador con el pthread de fondo mientras
//se ejecuta el end_emulator
#ifdef USE_PTHREADS
        if (si_thread_main_loop) {
        	debug_printf (VERBOSE_INFO,"Ending main loop thread");
		pthread_cancel(thread_main_loop);
	}
#endif


        end_emulator_saveornot_config(0);


}



void segfault_signal_handler(int sig)
{
	//para evitar warnings al compilar
	sig++;

	cpu_panic("Segmentation fault");
}


void sigbus_signal_handler(int sig)
{
	//Saltara por ejemplo si empezamos a escribir en un puntero que no se ha inicializado
	//para evitar warnings al compilar
	sig++;

	cpu_panic("Bus error");
}


void sigpipe_signal_handler(int sig)
{
	//Saltara por ejemplo cuando se escribe en un socket que se ha cerrado
	//para evitar warnings al compilar
	sig++;

	debug_printf (VERBOSE_DEBUG,"Received signal sigpipe");


}


void floatingpoint_signal_handler(int sig)
{
        //para evitar warnings al compilar
        sig++;

        cpu_panic("Floating point exception");
}


void main_init_video(void)
{
		//Video init
                debug_printf (VERBOSE_INFO,"Initializing Video Driver");

                //gestion de fallback y con driver indicado

                //scr_init_pantalla=NULL;

#ifdef USE_COCOA
                add_scr_init_array("cocoa",scrcocoa_init,set_scrdriver_cocoa);
                if (!strcmp(driver_screen,"cocoa")) {
                        set_scrdriver_cocoa();
                }
#endif


#ifdef COMPILE_XWINDOWS
                add_scr_init_array("xwindows",scrxwindows_init,set_scrdriver_xwindows);
                if (!strcmp(driver_screen,"xwindows")) {
                        set_scrdriver_xwindows();
                }
#endif


#ifdef COMPILE_SDL
                add_scr_init_array("sdl",scrsdl_init,set_scrdriver_sdl);
                if (!strcmp(driver_screen,"sdl")) {
                        set_scrdriver_sdl();
                }
#endif



#ifdef COMPILE_FBDEV
		add_scr_init_array("fbdev",scrfbdev_init,set_scrdriver_fbdev);
                if (!strcmp(driver_screen,"fbdev")) {
                        set_scrdriver_fbdev();
		}
#endif
                //Y finalmente null video driver
                add_scr_init_array("null",scrnull_init,set_scrdriver_null);
                if (!strcmp(driver_screen,"null")) {
                        set_scrdriver_null();
                }



                if (try_fallback_video.v==1) {
                        //probar drivers de video
                        do_fallback_video();
                }

                //no probar. Inicializar driver indicado. Si falla, fallback a null
                else {
                        if (screen_init_pantalla_and_others() ) {
                                debug_printf (VERBOSE_ERR,"Error using video output driver %s. Fallback to null",driver_screen);
				set_scrdriver_null();
				screen_init_pantalla_and_others();
                        }
                }


}

void main_init_audio(void)
{
		debug_printf (VERBOSE_INFO,"Initializing Audio");
                //Audio init
                audio_init=NULL;
                audio_buffer_switch.v=0;

                interrupt_finish_sound.v=0;
                audio_playing.v=1;

                audio_buffer_one=audio_buffer_one_assigned;
                audio_buffer_two=audio_buffer_two_assigned;

                set_active_audio_buffer();

		audio_empty_buffer();

                //gestion de fallback y con driver indicado.

#ifdef COMPILE_COREAUDIO
                add_audio_init_array("coreaudio",audiocoreaudio_init,set_audiodriver_coreaudio);
                if (!strcmp(driver_audio,"coreaudio")) {
                        set_audiodriver_coreaudio();

                }
#endif


#ifdef COMPILE_SDL
                add_audio_init_array("sdl",audiosdl_init,set_audiodriver_sdl);
                if (!strcmp(driver_audio,"sdl")) {
                        set_audiodriver_sdl();

                }
#endif


#ifdef COMPILE_PULSE
                add_audio_init_array("pulse",audiopulse_init,set_audiodriver_pulse);
                if (!strcmp(driver_audio,"pulse")) {
                        set_audiodriver_pulse();

                }
#endif


#ifdef COMPILE_ALSA
                add_audio_init_array("alsa",audioalsa_init,set_audiodriver_alsa);
                if (!strcmp(driver_audio,"alsa")) {
                        set_audiodriver_alsa();

                }
#endif


#ifdef COMPILE_DSP
                add_audio_init_array("dsp",audiodsp_init,set_audiodriver_dsp);
                if (!strcmp(driver_audio,"dsp")) {
                        set_audiodriver_dsp();

                }
#endif

#ifdef COMPILE_PCSPEAKER
                add_audio_init_array("pcspeaker",audiopcspeaker_init,set_audiodriver_pcspeaker);
                if (!strcmp(driver_audio,"pcspeaker")) {
                        set_audiodriver_pcspeaker();

                }
#endif




                //Y finalmente null audio driver
                add_audio_init_array("null",audionull_init,set_audiodriver_null);
                if (!strcmp(driver_audio,"null")) {
                        set_audiodriver_null();

                }





                if (try_fallback_audio.v==1) {
                        //probar drivers de audio
                        do_fallback_audio();
                }

                //no probar. Inicializar driver indicado. Si falla, fallback a null
                else {
                        if (audio_init()) {
				fallback_audio_null();
                        }
                }

}

char *param_custom_romfile=NULL;
z80_bit opcion_no_welcome_message;


z80_bit command_line_timex_video={0};
z80_bit command_line_ulaplus={0};
z80_bit command_line_mmc={0};
z80_bit command_line_divmmc={0};
z80_bit command_line_divmmc_ports={0};
z80_bit command_line_divmmc_paging={0};

z80_bit command_line_set_breakpoints={0};

z80_bit command_line_enable_midi={0};

char *command_line_load_binary_file=NULL;
int command_line_load_binary_address;
int command_line_load_binary_length;


//cuantos botones-joystick a teclas definidas
int joystickkey_definidas=0;


//void parse_cmdline_options(int argc,char *argv[]) {
int parse_cmdline_options(void) {

		while (!siguiente_parametro()) {
			if (!strcmp(argv[puntero_parametro],"--help")) {
				cpu_help();
				exit(1);
			}

                        if (!strcmp(argv[puntero_parametro],"--experthelp")) {
                                cpu_help_expert();
                                exit(1);
                        }

			if (!strcmp(argv[puntero_parametro],"--helpcustomconfig")) {
				customconfig_help();
				exit(1);
			}


			if (!strcmp(argv[puntero_parametro],"--showcompileinfo")) {
                                show_compile_info();
                                exit(1);
                        }



			if (!strcmp(argv[puntero_parametro],"--debugconfigfile")) {
				//Este parametro aqui se ignora, solo se lee antes del parseo del archivo de configuracion
                        }

			else if (!strcmp(argv[puntero_parametro],"--noconfigfile")) {
                                //Este parametro aqui se ignora, solo se lee antes del parseo del archivo de configuracion
                        }

			else if (!strcmp(argv[puntero_parametro],"--configfile")) {
                                //Este parametro aqui se ignora, solo se lee antes del parseo del archivo de configuracion
					siguiente_parametro_argumento();
                        }

			else if (!strcmp(argv[puntero_parametro],"--saveconf-on-exit")) {
				save_configuration_file_on_exit.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--zoomx")) {
				siguiente_parametro_argumento();
				zoom_x=atoi(argv[puntero_parametro]);
			}

			else if (!strcmp(argv[puntero_parametro],"--zoomy")) {
				siguiente_parametro_argumento();
				zoom_y=atoi(argv[puntero_parametro]);
			}


			else if (!strcmp(argv[puntero_parametro],"--zoom")) {
				siguiente_parametro_argumento();
				zoom_y=zoom_x=atoi(argv[puntero_parametro]);
			}

			else if (!strcmp(argv[puntero_parametro],"--frameskip")) {
                                siguiente_parametro_argumento();
                                frameskip=atoi(argv[puntero_parametro]);
				if (frameskip>49 || frameskip<0) {
					printf ("Frameskip out of range\n");
					exit(1);
				}
    }

			else if (!strcmp(argv[puntero_parametro],"--disable-autoframeskip")) {
					autoframeskip.v=0;
				}

			else if (!strcmp(argv[puntero_parametro],"--testconfig")) {
				test_config_and_exit.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--nochangeslowparameters")) {
				no_cambio_parametros_maquinas_lentas.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--fullscreen")) {
				ventana_fullscreen=1;
			}

            else if (!strcmp(argv[puntero_parametro],"--verbose")) {
                                siguiente_parametro_argumento();
                                verbose_level=atoi(argv[puntero_parametro]);
				if (verbose_level<0 || verbose_level>4) {
					printf ("Invalid Verbose level\n");
					exit(1);
				}
            }

            else if (!strcmp(argv[puntero_parametro],"--disable-debug-console-win")) {
                //Por defecto esta habilitado, por tanto lo desactivamos
                debug_unnamed_console_end();
                debug_unnamed_console_enabled.v=0;
            }

            else if (!strcmp(argv[puntero_parametro],"--verbose-always-console")) {
                debug_always_show_messages_in_console.v=1;
            }

			else if (!strcmp(argv[puntero_parametro],"--nodisableconsole")) {
				//Parametro que solo es de Windows, pero lo admitimos en cualquier sistema
				windows_no_disable_console.v=1;
			}

                        else if (!strcmp(argv[puntero_parametro],"--cpuspeed")) {
                                siguiente_parametro_argumento();
                                porcentaje_velocidad_emulador=atoi(argv[puntero_parametro]);
                                if (porcentaje_velocidad_emulador<1 || porcentaje_velocidad_emulador>9999) {
                                        printf ("Invalid CPU percentage\n");
                                        exit(1);
                                }
                        }

			else if (!strcmp(argv[puntero_parametro],"--random-r-register")) {
				cpu_random_r_register.v=1;
			}

                        else if (!strcmp(argv[puntero_parametro],"--vo")) {
				int drivervook=0;
				try_fallback_video.v=0;

                                siguiente_parametro_argumento();
                                driver_screen=argv[puntero_parametro];

#ifdef USE_COCOA
				if (!strcmp(driver_screen,"cocoa")) drivervook=1;
#endif

#ifdef COMPILE_XWINDOWS
				if (!strcmp(driver_screen,"xwindows")) drivervook=1;
#endif

#ifdef COMPILE_SDL
                                if (!strcmp(driver_screen,"sdl")) drivervook=1;
#endif


#ifdef COMPILE_FBDEV
				if (!strcmp(driver_screen,"fbdev")) drivervook=1;
#endif


				if (!strcmp(driver_screen,"null")) drivervook=1;

				if (!drivervook) {
					printf ("Video Driver %s not supported\n",driver_screen);
					exit(1);
				}



                        }




                        else if (!strcmp(argv[puntero_parametro],"--ao")) {
				try_fallback_audio.v=0;
                                int driveraook=0;
                                siguiente_parametro_argumento();
                                driver_audio=argv[puntero_parametro];



#ifdef COMPILE_DSP
                                if (!strcmp(driver_audio,"dsp")) driveraook=1;
#endif

#ifdef COMPILE_PCSPEAKER
                                if (!strcmp(driver_audio,"pcspeaker")) driveraook=1;
#endif

#ifdef COMPILE_SDL
                                if (!strcmp(driver_audio,"sdl")) driveraook=1;
#endif


#ifdef COMPILE_ALSA
                                if (!strcmp(driver_audio,"alsa")) driveraook=1;
#endif

#ifdef COMPILE_PULSE
                                if (!strcmp(driver_audio,"pulse")) driveraook=1;
#endif


#ifdef COMPILE_COREAUDIO
                                if (!strcmp(driver_audio,"coreaudio")) driveraook=1;
#endif

                                if (!strcmp(driver_audio,"null")) driveraook=1;

                                if (!driveraook) {
                                        printf ("Audio Driver %s not supported\n",driver_audio);
                                        exit(1);
                                }



                        }

                        else if (!strcmp(argv[puntero_parametro],"--aofile")) {
                                siguiente_parametro_argumento();
                                aofilename=argv[puntero_parametro];
                        }

                        else if (!strcmp(argv[puntero_parametro],"--vofile")) {
                                siguiente_parametro_argumento();
                                vofilename=argv[puntero_parametro];
                        }

                        else if (!strcmp(argv[puntero_parametro],"--vofilefps")) {
                                siguiente_parametro_argumento();

                                int valor=atoi(argv[puntero_parametro]);
                                if (valor==1 || valor==2 || valor==5 || valor==10 || valor==25 || valor==50) {
					vofile_fps=50/valor;
				}

				else {
				        printf ("Invalid FPS value\n");
                                        exit(1);
                                }

                        }


                        else if (!strcmp(argv[puntero_parametro],"--disableayspeech")) {
				ay_speech_enabled.v=0;
                        }

			else if (!strcmp(argv[puntero_parametro],"--disableenvelopes")) {
                                ay_envelopes_enabled.v=0;
                        }



                        else if (!strcmp(argv[puntero_parametro],"--debugregisters")) {
				debug_registers=1;
                        }

#ifdef USE_XEXT
			else if (!strcmp(argv[puntero_parametro],"--disableshm"))
				disable_shm=1;
#endif

			else if (!strcmp(argv[puntero_parametro],"--disableborder")) {
				border_enabled.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--hidemousepointer")) {
				mouse_pointer_shown.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--limitopenmenu")) {
				menu_limit_menu_open.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--hide-dirs")) {
				menu_filesel_hide_dirs.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--no-file-previews")) {
				menu_filesel_show_previews.v=0;
			}


			else if (!strcmp(argv[puntero_parametro],"--disablemenumouse")) {
				mouse_menu_disabled.v=1;
			}


			/*else if (!strcmp(argv[puntero_parametro],"--overlayinfo")) {
				enable_second_layer();
			}*/

			else if (!strcmp(argv[puntero_parametro],"--disablefooter")) {
				disable_footer();
			}

			else if (!strcmp(argv[puntero_parametro],"--disablemultitaskmenu")) {
                menu_multitarea=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--disablebw-no-multitask")) {
				//Obsoleto
            			//screen_bw_no_multitask_menu.v=0;
			}


			else if (!strcmp(argv[puntero_parametro],"--loadbinarypath")) {
                                siguiente_parametro_argumento();
                                sprintf(binary_file_load,"%s/",argv[puntero_parametro]);
			}

			else if (!strcmp(argv[puntero_parametro],"--savebinarypath")) {
                                siguiente_parametro_argumento();
                                sprintf(binary_file_save,"%s/",argv[puntero_parametro]);
			}

                        else if (!strcmp(argv[puntero_parametro],"--printerbitmapfile")) {
				siguiente_parametro_argumento();
                                zxprinter_enabled.v=1;
				zxprinter_bitmap_filename=argv[puntero_parametro];
				zxprinter_file_bitmap_init();
			}

			else if (!strcmp(argv[puntero_parametro],"--printertextfile")) {
                                siguiente_parametro_argumento();
                                zxprinter_enabled.v=1;
                                zxprinter_ocr_filename=argv[puntero_parametro];
                                zxprinter_file_ocr_init();
                        }

			else if (!strcmp(argv[puntero_parametro],"--redefinekey")) {
				z80_byte tecla_original, tecla_redefinida;
				siguiente_parametro_argumento();
				tecla_original=parse_string_to_number(argv[puntero_parametro]);

				siguiente_parametro_argumento();
				tecla_redefinida=parse_string_to_number(argv[puntero_parametro]);

				if (util_add_redefinir_tecla(tecla_original,tecla_redefinida)) {
					exit(1);
				}
			}

			else if (!strcmp(argv[puntero_parametro],"--recreatedzx")) {
				recreated_zx_keyboard_support.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--enablekempstonmouse")) {
				kempston_mouse_emulation.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--kempstonmouse-sens")) {
				siguiente_parametro_argumento();
                int valor=atoi(argv[puntero_parametro]);
                if (valor<1 || valor>MAX_KMOUSE_SENSITIVITY) {
               		printf ("Invalid Kempston Mouse Sensitivity value\n");
                    exit(1);
                }
                kempston_mouse_factor_sensibilidad=valor;
			}

			else if (!strcmp(argv[puntero_parametro],"--def-f-function")) {
				siguiente_parametro_argumento();
				if (argv[puntero_parametro][0]!='F' && argv[puntero_parametro][0]!='f') {
					printf ("Unknown key\n");
					exit(1);
				}

				int valor=atoi(&argv[puntero_parametro][1]);

				if (valor<1 || valor>MAX_F_FUNCTIONS_KEYS) {
					printf ("Invalid key\n");
					exit(1);
				}

				siguiente_parametro_argumento();

				if (menu_define_key_function(valor,argv[puntero_parametro])) {
					printf ("Invalid f-function action: %s\n",argv[puntero_parametro]);
					exit(1);
				}


			}



			else if (!strcmp(argv[puntero_parametro],"--tempdir")) {
                                siguiente_parametro_argumento();
                                sprintf(emulator_tmpdir_set_by_user,"%s/",argv[puntero_parametro]);
                        }

			else if (!strcmp(argv[puntero_parametro],"--loadbinary")) {
				siguiente_parametro_argumento();
				command_line_load_binary_file=argv[puntero_parametro];
				siguiente_parametro_argumento();
				command_line_load_binary_address=parse_string_to_number(argv[puntero_parametro]);
				siguiente_parametro_argumento();
				command_line_load_binary_length=parse_string_to_number(argv[puntero_parametro]);

				//Y decimos cual ha sido ultimo archivo binario cargado
				sprintf(binary_file_load,"%s",command_line_load_binary_file);
			}

                        else if (!strcmp(argv[puntero_parametro],"--tool-sox-path")) {
                                siguiente_parametro_argumento();
                                sprintf (external_tool_sox,"%s",argv[puntero_parametro]);
			}

						//deprecated
                        else if (!strcmp(argv[puntero_parametro],"--tool-unzip-path")) {
                                siguiente_parametro_argumento();
                                //sprintf (external_tool_unzip,"%s",argv[puntero_parametro]);
			}

                        else if (!strcmp(argv[puntero_parametro],"--tool-gunzip-path")) {
                                siguiente_parametro_argumento();
                                sprintf (external_tool_gunzip,"%s",argv[puntero_parametro]);
			}

                        else if (!strcmp(argv[puntero_parametro],"--tool-tar-path")) {
                                siguiente_parametro_argumento();
                                sprintf (external_tool_tar,"%s",argv[puntero_parametro]);
			}

                        else if (!strcmp(argv[puntero_parametro],"--tool-unrar-path")) {
                                siguiente_parametro_argumento();
                                sprintf (external_tool_unrar,"%s",argv[puntero_parametro]);
			}

			else if (!strcmp(argv[puntero_parametro],"--mmc-file")) {
				siguiente_parametro_argumento();

                                //Si es ruta relativa, poner ruta absoluta
                                if (!si_ruta_absoluta(argv[puntero_parametro])) {
                                        //printf ("es ruta relativa\n");

                                        //TODO: quiza hacer esto con convert_relative_to_absolute pero esa funcion es para directorios,
                                        //no para directorios con archivo, por tanto quiza habria que hacer un paso intermedio separando
                                        //directorio de archivo
                                        char directorio_actual[PATH_MAX];
                                        getcwd(directorio_actual,PATH_MAX);

                                        sprintf (mmc_file_name,"%s/%s",directorio_actual,argv[puntero_parametro]);

                                }

				else {
					sprintf (mmc_file_name,"%s",argv[puntero_parametro]);
				}

			}

			else if (!strcmp(argv[puntero_parametro],"--enable-mmc")) {
				command_line_mmc.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--mmc-write-protection")) {
				mmc_write_protection.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--enable-divmmc-ports")) {
				command_line_divmmc_ports.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--enable-divmmc-paging")) {
				command_line_divmmc_paging.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--enable-divmmc")) {
				command_line_divmmc.v=1;
			}


#ifdef COMPILE_FBDEV
                	else if (!strcmp(argv[puntero_parametro],"--no-use-ttyfbdev")) {
				fbdev_no_uses_tty=1;
			}

                	else if (!strcmp(argv[puntero_parametro],"--no-use-ttyrawfbdev")) {
				fbdev_no_uses_ttyraw=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--use-all-res-fbdev")) {
                                fbdev_use_all_virtual_res=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--decimal-full-scale-fbdev")) {
				ventana_fullscreen=1;
                                fbdev_decimal_full_scale_fbdev=1;
			}

#ifdef EMULATE_RASPBERRY
			else if (!strcmp(argv[puntero_parametro],"--fbdev-margin-height")) {
				siguiente_parametro_argumento();
				int valor=atoi(argv[puntero_parametro]);
				if (valor<0) {
					printf ("Invalid margin height value\n");
					exit(1);
				}
				fbdev_margin_height=valor;
			}
			else if (!strcmp(argv[puntero_parametro],"--fbdev-margin-width")) {
				siguiente_parametro_argumento();
				int valor=atoi(argv[puntero_parametro]);
				if (valor<0) {
					printf ("Invalid margin width value\n");
					exit(1);
				}
				fbdev_margin_width=valor;
			}
#endif


#endif


			else if (!strcmp(argv[puntero_parametro],"--nosplash")) {
                                screen_show_splash_texts.v=0;
			}

			//Por defecto esta activo. Se mantiene solo por compatibilidad
			else if (!strcmp(argv[puntero_parametro],"--cpu-usage")) {
				screen_show_cpu_usage.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--no-cpu-usage")) {
				screen_show_cpu_usage.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--no-cpu-temp")) {
				screen_show_cpu_temp.v=0;
			}


			else if (!strcmp(argv[puntero_parametro],"--no-fps")) {
				screen_show_fps.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--nowelcomemessage")) {
                                opcion_no_welcome_message.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--hide-menu-percentage-bar")) {
                                menu_hide_vertical_percentaje_bar.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--hide-menu-minimize-button")) {
                                menu_hide_minimize_button.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--menu-mix-method")) {
				siguiente_parametro_argumento();
				int i;
				int encontrado=0;
				for (i=0;i<MAX_MENU_MIX_METHODS;i++) {
					if (!strcasecmp(screen_menu_mix_methods_strings[i],argv[puntero_parametro])) {
						screen_menu_mix_method=i;
						encontrado=1;
					}
				}

				if (!encontrado) {
						printf ("Invalid menu mix method\n");
						exit (1);

				}
			}


			else if (!strcmp(argv[puntero_parametro],"--menu-transparency-perc")) {

			int valor;

					siguiente_parametro_argumento();
					valor=atoi(argv[puntero_parametro]);

					if (valor<0 || valor>95) {
						printf ("Invalid menu transparency value\n");
						exit (1);
					}

        screen_menu_mix_transparency=valor;

			}


			else if (!strcmp(argv[puntero_parametro],"--menu-darken-when-open")) {
				screen_menu_reduce_bright_machine.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--menu-bw-multitask")) {
				screen_machine_bw_no_multitask.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--hide-menu-close-button")) {
                                menu_hide_close_button.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--invert-menu-mouse-scroll")) {
                                menu_invert_mouse_scroll.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--allow-background-windows")) {
                                menu_allow_background_windows=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--allow-background-windows-closed-menu")) {
                                always_force_overlay_visible_when_menu_closed=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--realvideo")) {
				enable_rainbow();
			}

			else if (!strcmp(argv[puntero_parametro],"--no-detect-realvideo")) {
				autodetect_rainbow.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--tbblue-legacy-hicolor")) {
				tbblue_store_scanlines.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--tbblue-legacy-border")) {
				tbblue_store_scanlines_border.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--enableulaplus")) {
				command_line_ulaplus.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--enabletimexvideo")) {
                                command_line_timex_video.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--disablerealbeeper")) {
				beeper_real_enabled=0;
      }

			else if (!strcmp(argv[puntero_parametro],"--disablebeeper")) {
				beeper_enabled.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--enableturbosound")) {

				int valor;

					//TODO. No aparece en el menu el error
					avisar_opcion_obsoleta("--enableturbosound setting is obsolete since version 5.1. Use --totalaychips");
					valor=2;

				set_total_ay_chips(valor);

			}

			else if (!strcmp(argv[puntero_parametro],"--totalaychips")) {

				int valor;

					siguiente_parametro_argumento();
					valor=atoi(argv[puntero_parametro]);

					if (valor>MAX_AY_CHIPS || valor<1) {
						printf ("Invalid ay chip value\n");
						exit (1);
					}

        set_total_ay_chips(valor);

			}

			else if (!strcmp(argv[puntero_parametro],"--ay-stereo-mode")) {
				int valor;

					siguiente_parametro_argumento();
					valor=atoi(argv[puntero_parametro]);

					if (valor>4 || valor<0) {
						printf ("Invalid ay stereo mode value\n");
						exit (1);
					}
				ay3_stereo_mode=valor;
			}

			else if (!strcmp(argv[puntero_parametro],"--ay-stereo-channel")) {
				char canal;
				siguiente_parametro_argumento();

				canal=argv[puntero_parametro][0];
				canal=letra_mayuscula(canal);

				if (canal!='A' && canal!='B' && canal!='C') {
					printf ("Invalid ay stereo channel\n");
					exit(1);
				}

				int valor;
				siguiente_parametro_argumento();
				valor=atoi(argv[puntero_parametro]);

				if (valor<0 || valor>2) {
					printf ("Invalid ay stereo channel position value\n");
					exit(1);
				}

				if (canal=='A') ay3_custom_stereo_A=valor;
				if (canal=='B') ay3_custom_stereo_B=valor;
				if (canal=='C') ay3_custom_stereo_C=valor;

			}

			else if (!strcmp(argv[puntero_parametro],"--enablespecdrum")) {
				//TODO. No aparece en el menu el error
				avisar_opcion_obsoleta("--enablespecdrum setting is obsolete since version 5.1. Use --enableaudiodac");
																audiodac_enabled.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--enableaudiodac")) {
																audiodac_enabled.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--audiodactype")) {
					siguiente_parametro_argumento();

					if (!audiodac_set_type(argv[puntero_parametro]) ) {
						printf ("Invalid audiodactype\n");
						exit (1);
					}

			}


			else if (!strcmp(argv[puntero_parametro],"--audiovolume")) {
                                siguiente_parametro_argumento();
                                int valor=atoi(argv[puntero_parametro]);

			        if (valor>100 || valor<0) {
			                printf ("Invalid volume value\n");
					exit (1);
				}

        			audiovolume=valor;
			}

			else if (!strcmp(argv[puntero_parametro],"--ayplayer-end-exit")) {
				ay_player_exit_emulator_when_finish.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--ayplayer-end-no-repeat")) {
				ay_player_repeat_file.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--ayplayer-inf-length")) {
				siguiente_parametro_argumento();
				int valor=atoi(argv[puntero_parametro]);
				if (valor<1 || valor>1310) {
					printf ("Invalid length value. Must be between 1 and 1310\n");
					exit(1);
				}
				ay_player_limit_infinite_tracks=valor*50;
			}

			else if (!strcmp(argv[puntero_parametro],"--ayplayer-any-length")) {
				siguiente_parametro_argumento();
				int valor=atoi(argv[puntero_parametro]);
				if (valor<1 || valor>1310) {
					printf ("Invalid length value. Must be between 1 and 1310\n");
					exit(1);
				}
				ay_player_limit_any_track=valor*50;
			}

			else if (!strcmp(argv[puntero_parametro],"--enable-midi")) {
				command_line_enable_midi.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--midi-client")) {
				siguiente_parametro_argumento();
				int valor=parse_string_to_number(argv[puntero_parametro]);
				if (valor<0 || valor>255) {
					printf ("Invalid client value. Must be between 0 and 255\n");
					exit(1);
				}
				audio_midi_client=valor;
			}

			else if (!strcmp(argv[puntero_parametro],"--midi-port")) {
				siguiente_parametro_argumento();
				int valor=parse_string_to_number(argv[puntero_parametro]);
				if (valor<0 || valor>255) {
					printf ("Invalid port value. Must be between 0 and 255\n");
					exit(1);
				}
				audio_midi_port=valor;
			}

			else if (!strcmp(argv[puntero_parametro],"--midi-raw-device")) {
				siguiente_parametro_argumento();
				strcpy(audio_raw_midi_device_out,argv[puntero_parametro]);
			}

			else if (!strcmp(argv[puntero_parametro],"--midi-allow-tone-noise")) {
				midi_output_record_noisetone.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--midi-no-raw-mode")) {
				audio_midi_raw_mode=0;
			}


			else if (!strcmp(argv[puntero_parametro],"--noreset-audiobuffer-full")) {
				audio_noreset_audiobuffer_full.v=1;
			}

			//Setting de pcspeaker siempre compilado, simplemente si no esta, no sale en la ayuda
			else if (!strcmp(argv[puntero_parametro],"--pcspeaker-wait-time")) {
				siguiente_parametro_argumento();
				int valor=parse_string_to_number(argv[puntero_parametro]);
				if (valor<0 || valor>64) {
					printf ("Invalid wait time value. Must be between 0 and 64\n");
					exit(1);
				}
				audiopcspeaker_tiempo_espera=valor;
			}



#ifdef COMPILE_ALSA
			else if (!strcmp(argv[puntero_parametro],"--alsaperiodsize")) {
		                 siguiente_parametro_argumento();
                                int valor=atoi(argv[puntero_parametro]);
                                if (valor!=2 && valor!=4) {
                                        printf ("Invalid Alsa Period size\n");
                                        exit(1);
                                }
                                alsa_periodsize=AUDIO_BUFFER_SIZE*valor;
			}


#ifdef USE_PTHREADS
                        else if (!strcmp(argv[puntero_parametro],"--fifoalsabuffersize")) {
                                 siguiente_parametro_argumento();
                                int valor=atoi(argv[puntero_parametro]);
                                if (valor<4 || valor>10) {
                                        printf ("Invalid Alsa Fifo Buffer size\n");
                                        exit(1);
                                }
				fifo_alsa_buffer_size=AUDIO_BUFFER_SIZE*valor;
                        }


#endif
#endif


#ifdef COMPILE_PULSE
#ifdef USE_PTHREADS
                        else if (!strcmp(argv[puntero_parametro],"--pulseperiodsize")) {
                                 siguiente_parametro_argumento();
                                int valor=atoi(argv[puntero_parametro]);
                                //if (valor!=2 && valor!=4 && valor!=1) {
                                if (valor<1 || valor>4) {
                                        printf ("Invalid Pulse Period size\n");
                                        exit(1);
                                }
                                pulse_periodsize=AUDIO_BUFFER_SIZE*valor;
                        }


                        else if (!strcmp(argv[puntero_parametro],"--fifopulsebuffersize")) {
                                 siguiente_parametro_argumento();
                                int valor=atoi(argv[puntero_parametro]);
                                if (valor<4 || valor>10) {
                                        printf ("Invalid Pulse Fifo Buffer size\n");
                                        exit(1);
                                }
                                fifo_pulse_buffer_size=AUDIO_BUFFER_SIZE*valor;
                        }


#endif
#endif


#ifdef COMPILE_COREAUDIO

	else if (!strcmp(argv[puntero_parametro],"--fifocorebuffersize")) {
				 siguiente_parametro_argumento();
				int valor=atoi(argv[puntero_parametro]);
				if (valor<MIN_AUDIOCOREAUDIO_FIFO_MULTIPLIER || valor>MAX_AUDIOCOREAUDIO_FIFO_MULTIPLIER) {
								printf ("Invalid Coreaudio Fifo Buffer size\n");
								exit(1);
				}
				audiocoreaudio_fifo_buffer_size_multiplier=valor;
			}
#endif

#ifdef COMPILE_SDL
			else if (!strcmp(argv[puntero_parametro],"--sdlsamplesize")) {
				siguiente_parametro_argumento();
				int valor=atoi(argv[puntero_parametro]);
				if (valor<128 || valor>2048) {
					printf ("Invalid SDL audio sample size\n");
					exit(1);
				}
				audiosdl_samples=valor;
			}


			else if (!strcmp(argv[puntero_parametro],"--fifosdlbuffersize")) {
						 siguiente_parametro_argumento();
						int valor=atoi(argv[puntero_parametro]);
						if (valor<MIN_AUDIOSDL_FIFO_MULTIPLIER || valor>MAX_AUDIOSDL_FIFO_MULTIPLIER) {
										printf ("Invalid SDL Fifo Buffer size\n");
										exit(1);
						}
						audiosdl_fifo_buffer_size_multiplier=valor;
					}

#endif


			//Este setting lo permitimos siempre, aunque no se haya compilado driver sdl, pues es una variable global, aunque no se verá en la ayuda
			else if (!strcmp(argv[puntero_parametro],"--sdlrawkeyboard")) {
					sdl_raw_keyboard_read.v=1;
			}

                        else if (!strcmp(argv[puntero_parametro],"--blue")) {
                                screen_gray_mode |=1;
                        }

                        else if (!strcmp(argv[puntero_parametro],"--green")) {
                                screen_gray_mode |=2;
                        }

                        else if (!strcmp(argv[puntero_parametro],"--red")) {
                                screen_gray_mode |=4;
                        }

			else if (!strcmp(argv[puntero_parametro],"--inversevideo")) {
                                inverse_video.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--realpalette")) {
                                spectrum_1648_use_real_palette.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--disabletooltips")) {
				tooltip_enabled.v=0;
			}

			else if (!strcmp(argv[puntero_parametro],"--disablemenu")) {
				menu_desactivado.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--disablemenuandexit")) {
				menu_desactivado_andexit.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--disablemenufileutils")) {
				menu_desactivado_file_utilities.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--forcevisiblehotkeys")) {
                                menu_force_writing_inverse_color.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--forceconfirmyes")) {
				force_confirm_yes.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--gui-style")) {
				siguiente_parametro_argumento();
				int i;
				for (i=0;i<ESTILOS_GUI;i++) {
					if (!strcasecmp(argv[puntero_parametro],definiciones_estilos_gui[i].nombre_estilo)) {
						estilo_gui_activo=i;
						set_charset();
						break;
					}
				}
				if (i==ESTILOS_GUI) {
					printf ("Invalid GUI style\n");
					exit(1);
				}
                        }


			else if (!strcmp(argv[puntero_parametro],"--keyboardspoolfile")) {
                                siguiente_parametro_argumento();
				//sprintf(input_file_keyboard_name_buffer,"%s",argv[puntero_parametro]);
				input_file_keyboard_name=argv[puntero_parametro];
			        input_file_keyboard_init();
			}

			else if (!strcmp(argv[puntero_parametro],"--keyboardspoolfile-play")) {
                input_file_keyboard_playing.v=1;
			}

//Si no hay soporte de pthreads, estas opciones las permitimos pero luego no hace nada

			else if (!strcmp(argv[puntero_parametro],"--enable-remoteprotocol")) {
					remote_protocol_enabled.v=1;
		 }

		 else if (!strcmp(argv[puntero_parametro],"--remoteprotocol-port")) {
																siguiente_parametro_argumento();
																int valor=atoi(argv[puntero_parametro]);

						 if (valor>65535 || valor<1) {
										 printf ("Invalid port value\n");
				 exit (1);
			 }

						 remote_protocol_port=valor;
		 }


		 else if (!strcmp(argv[puntero_parametro],"--showfiredbreakpoint")) {
			 siguiente_parametro_argumento();
            int valor=parse_string_to_number(argv[puntero_parametro]);
			if (valor<0 || valor>2) {
				 printf ("Invalid port value\n");
				 exit (1);
			 }

			debug_show_fired_breakpoints_type=valor;
		 }

		 else if (!strcmp(argv[puntero_parametro],"--set-breakpoint")) {
			 siguiente_parametro_argumento();
			 int valor=atoi(argv[puntero_parametro]);
			 valor--;

			 siguiente_parametro_argumento();


			 if (valor<0 || valor>MAX_BREAKPOINTS_CONDITIONS-1) {
				 printf("Index %d out of range setting breakpoint \"%s\"\n",valor+1,argv[puntero_parametro]);
				 exit(1);
			 }

			 debug_set_breakpoint(valor,argv[puntero_parametro]);

		 }

		 else if (!strcmp(argv[puntero_parametro],"--set-watch")) {
			 siguiente_parametro_argumento();
			 int valor=atoi(argv[puntero_parametro]);
			 valor--;

			 siguiente_parametro_argumento();


			 if (valor<0 || valor>DEBUG_MAX_WATCHES-1) {
				 printf("Index %d out of range setting watch \"%s\"\n",valor+1,argv[puntero_parametro]);
				 exit(1);
			 }

			 debug_set_watch(valor,argv[puntero_parametro]);

		 }

		 else if (!strcmp(argv[puntero_parametro],"--set-mem-breakpoint")) {
			 siguiente_parametro_argumento();
			 int direccion=parse_string_to_number(argv[puntero_parametro]);
			 if (direccion<0 || direccion>65535) {
				 printf("Address %d out of range setting memory breakpoint\n",direccion);
				 exit(1);
			 }

			siguiente_parametro_argumento();
			 int valor=parse_string_to_number(argv[puntero_parametro]);
			 if (valor<0 || valor>255) {
				 printf("Type %d out of range setting memory breakpoint at address %04XH\n",valor,direccion);
				 exit(1);
			 }

			 debug_set_mem_breakpoint(direccion,valor);

		 }


		 else if (!strcmp(argv[puntero_parametro],"--set-breakpointaction")) {
			 siguiente_parametro_argumento();
			 int valor=atoi(argv[puntero_parametro]);
			 valor--;

			 siguiente_parametro_argumento();


			 if (valor<0 || valor>MAX_BREAKPOINTS_CONDITIONS-1) {
				 printf("Index %d out of range setting breakpoint action \"%s\"\n",valor+1,argv[puntero_parametro]);
				 exit(1);
			 }

			 debug_set_breakpoint_action(valor,argv[puntero_parametro]);



		 }

		 else if (!strcmp(argv[puntero_parametro],"--enable-breakpoints")) {
		 			 command_line_set_breakpoints.v=1;
		 }

		else if (!strcmp(argv[puntero_parametro],"--show-invalid-opcode")) {
	  				debug_shows_invalid_opcode.v=1;
		}

		else if (!strcmp(argv[puntero_parametro],"--brkp-always")) {
	  				debug_breakpoints_cond_behaviour.v=0;
		}

		else if (!strcmp(argv[puntero_parametro],"--show-display-debug")) {
	  				debug_settings_show_screen.v=1;

		}


		else if (!strcmp(argv[puntero_parametro],"--show-electron-debug")) {
	  				menu_debug_registers_if_showscan.v=1;
		}




		 else if (!strcmp(argv[puntero_parametro],"--hardware-debug-ports")) {
			 hardware_debug_port.v=1;
		 }

		 else if (!strcmp(argv[puntero_parametro],"--hardware-debug-ports-byte-file")) {
			siguiente_parametro_argumento();
			strcpy(zesarux_zxi_hardware_debug_file,argv[puntero_parametro]);
		 }

		 else if (!strcmp(argv[puntero_parametro],"-—dump-ram-to-file")) {
                                siguiente_parametro_argumento();
				strcpy(dump_ram_file,argv[puntero_parametro]);
                 }

			else if (!strcmp(argv[puntero_parametro],"--joystickemulated")) {
                                siguiente_parametro_argumento();
				if (realjoystick_set_type(argv[puntero_parametro])) {
                                        exit(1);
				}

			}


			else if (!strcmp(argv[puntero_parametro],"--disablerealjoystick")) {
				//realjoystick_present.v=0;
				realjoystick_disabled.v=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--no-native-linux-realjoy")) {
				no_native_linux_realjoystick.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--realjoystickpath")) {
				siguiente_parametro_argumento();
				strcpy(string_dev_joystick,argv[puntero_parametro]);

			}

			else if (!strcmp(argv[puntero_parametro],"--realjoystick-calibrate")) {
				siguiente_parametro_argumento();
				int valor=parse_string_to_number(argv[puntero_parametro]);
				if (valor<0 || valor>32000) {
					printf ("Invalid value %d for setting --realjoystick-calibrate\n",valor);
                    exit(1);
				}
				realjoystick_autocalibrate_value=valor;
			}


			else if (!strcmp(argv[puntero_parametro],"--joystickevent")) {
				char *text_button;
				char *text_event;

				//obtener boton
				siguiente_parametro_argumento();
				text_button=argv[puntero_parametro];

				//obtener evento
				siguiente_parametro_argumento();
				text_event=argv[puntero_parametro];

				//Y definir el evento
				if (realjoystick_set_button_event(text_button,text_event)) {
                                        exit(1);
                                }


			}

                        else if (!strcmp(argv[puntero_parametro],"--joystickkeybt")) {

				char *text_button;
                                char *text_key;

                                //obtener boton
                                siguiente_parametro_argumento();
                                text_button=argv[puntero_parametro];

                                //obtener tecla
                                siguiente_parametro_argumento();
                                text_key=argv[puntero_parametro];

				//Y definir el evento
                                if (realjoystick_set_button_key(text_button,text_key)) {
                                        exit(1);
                                }

			}


			else if (!strcmp(argv[puntero_parametro],"--joystickkeyev")) {

				char *text_event;
                                char *text_key;

                                //obtener evento
				siguiente_parametro_argumento();
				text_event=argv[puntero_parametro];

				//Y obtener tecla
				siguiente_parametro_argumento();
				text_key=argv[puntero_parametro];

	                        //Y definir el evento
                                if (realjoystick_set_event_key(text_event,text_key)) {
					exit (1);
                                }


			}

			else if (!strcmp(argv[puntero_parametro],"--clearkeylistonsmart")) {
				realjoystick_clear_keys_on_smartload.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--cleareventlist")) {
                          	realjoystick_clear_events_array();
			}


			else if (!strcmp(argv[puntero_parametro],"--enablejoysticksimulator")) {
				simulador_joystick=1;
			}


			else if (!strcmp(argv[puntero_parametro],"--quickexit")) {
				quickexit.v=1;
			}


	                 else if (!strcmp(argv[puntero_parametro],"--exit-after")) {
                         	siguiente_parametro_argumento();
	                         int valor=atoi(argv[puntero_parametro]);
				if (valor<=0) {
					printf ("Invalid value %d for setting --exit-after\n",valor);
                                 	exit(1);
				}
				exit_emulator_after_seconds=valor;
                         }


			else if (!strcmp(argv[puntero_parametro],"--windowgeometry")) {
				siguiente_parametro_argumento();
				char *nombre;
				int x,y,ancho,alto;

				nombre=argv[puntero_parametro];

				siguiente_parametro_argumento();
				x=parse_string_to_number(argv[puntero_parametro]);

				siguiente_parametro_argumento();
				y=parse_string_to_number(argv[puntero_parametro]);

				siguiente_parametro_argumento();
				ancho=parse_string_to_number(argv[puntero_parametro]);

				siguiente_parametro_argumento();
				alto=parse_string_to_number(argv[puntero_parametro]);

				if (x<0 || y<0 || ancho<0 || alto<0) {
					printf ("Invalid window geometry\n");
					exit(1);
				}

				util_add_window_geometry(nombre,x,y,ancho,alto);

			}


			else if (!strcmp(argv[puntero_parametro],"--clear-all-windowgeometry")) {
				util_clear_all_windows_geometry();
			}

			else if (!strcmp(argv[puntero_parametro],"--restorewindow")) {
				siguiente_parametro_argumento();

				strcpy(restore_window_array[total_restore_window_array_elements++],argv[puntero_parametro]);

			}


			else if (!strcmp(argv[puntero_parametro],"--enable-restore-windows")) {
				menu_reopen_background_windows_on_start.v=1;
			}

			else if (!strcmp(argv[puntero_parametro],"--tonegenerator")) {
				siguiente_parametro_argumento();
                                 int valor=atoi(argv[puntero_parametro]);
                                if (valor<1 || valor>3) {
                                        printf ("Invalid value %d for setting --tonegenerator\n",valor);
                                        exit(1);
                                }
				audio_tone_generator=valor;
			}



			//autodetectar que el parametro es un snap o cinta. Esto tiene que ser siempre el ultimo else if
			else {

				//parametro desconocido
				debug_printf (VERBOSE_ERR,"Unknown parameter : %s . Stopping parsing the rest of parameters",argv[puntero_parametro]);
				return 1;
				//cpu_help();
				//exit(1);
			}


		}

		//Fin de interpretacion de parametros
		return 0;

}



void emulator_main_loop(void)
{
	while (1) {
		if (menu_abierto==1) menu_inicio();

    		//Bucle principal de ejecución de la cpu
    		while (menu_abierto==0) {
    			cpu_core_loop();
    		}


	}


}

#ifdef USE_PTHREADS
void *thread_main_loop_function(void *nada)
{
        emulator_main_loop();

	//aqui no llega nunca, lo hacemos solo para que no se queje el compilador
        nada=0;
        nada++;
	return NULL;
}
#endif


char macos_path_to_executable[PATH_MAX];

//Proceso inicial
int zesarux_main (int main_argc,char *main_argv[]) {

	if (main_argc>1) {
		if (!strcmp(main_argv[1],"--version")) {
		//	printf ("ZEsarUX Version: " EMULATOR_VERSION " Date: " EMULATOR_DATE " - " EMULATOR_EDITION_NAME "\n");
			printf ("ZEsarUX v." EMULATOR_VERSION " - " EMULATOR_EDITION_NAME ". " EMULATOR_DATE  "\n");
			exit(0);
		}
	}

	//de momento ponemos esto a null y los mensajes siempre saldran por un printf normal
	scr_messages_debug=NULL;

#if defined(__APPLE__)
	//Si estamos en Mac y estamos ejecutando desde bundle de la App, cambiar carpeta a directorio de trabajo
	//Esto antes estaba en el zesarux.sh, pero ahora se llama al binario para poder usar los permisos de Documents, Downloads etc
	//de MacOS Catalina

	//Cambiar a la carpeta donde estamos ejecutando el binario

	//por si acaso, por defecto a cadena vacia
	macos_path_to_executable[0]=0;

	uint32_t bufsize=PATH_MAX;

	_NSGetExecutablePath(macos_path_to_executable, &bufsize);

	if (macos_path_to_executable[0]!=0) {

			char dir[PATH_MAX];
			util_get_dir(macos_path_to_executable,dir);

			printf ("Changing to Mac App bundle directory: %s\n",dir);
			chdir(dir);

	}
	/*
	Para testeo, para eliminar permisos de acceso en Catalina, ejecutar:
	tccutil reset SystemPolicyDocumentsFolder com.cesarhernandez.zesarux
	tccutil reset SystemPolicyDownloadsFolder com.cesarhernandez.zesarux
	tccutil reset SystemPolicyDesktopFolder com.cesarhernandez.zesarux
	*/
#endif


/*
Note for developers: If you are doing modifications to ZEsarUX, you should follow the rules from GPL license, as well as
the licenses that cover all the external modules
Also, you should keep the following copyright message, beginning with "Begin Copyright message" and ending with "End Copyright message"
*/

//Begin Copyright message

	printf ("ZEsarUX - ZX Second-Emulator And Released for UniX\n"
	"https://github.com/chernandezba/zesarux\n\n"
    "Copyright (C) 2013 Cesar Hernandez Bano\n"
	"\n"
    "ZEsarUX is free software: you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation, either version 3 of the License, or\n"
    "(at your option) any later version.\n"
	"\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    "GNU General Public License for more details.\n"
	"\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program.  If not, see <https://www.gnu.org/licenses/>.\n"
	"\n"
	);

	printf ("Please read the other licenses used in ZEsarUX, from the menu Help->Licenses or just open files from folder licenses/\n\n\n");



		  //printf ("ZEsarUX Version: " EMULATOR_VERSION " Date: " EMULATOR_DATE " - " EMULATOR_EDITION_NAME "\n"
			printf ("ZEsarUX v." EMULATOR_VERSION " - " EMULATOR_EDITION_NAME ". " EMULATOR_DATE  "\n"

					"\n");


//End Copyright message





#ifdef DEBUG_SECOND_TRAP_STDOUT
		printf ("\n\nWARNING!!!! DEBUG_SECOND_TRAP_STDOUT enabled!!\n"
			"Enable this only when you want to find printing routines\n\n");
		sleep (3);
#endif



	//Unos cuantos valores por defecto
	cpu_step_mode.v=0;
	current_machine_type=MACHINE_ID_TBBLUE;
	ay_speech_enabled.v=1;
	ay_envelopes_enabled.v=1;
	aofilename=NULL;
	aofile_inserted.v=0;
	vofilename=NULL;
	vofile_inserted.v=0;
	input_file_keyboard_inserted.v=0;
	input_file_keyboard_playing.v=0;
	input_file_keyboard_send_pause.v=1;

	scrfile=NULL;
	modificado_border.v=1;
	border_enabled.v=1;

	scr_putpixel=NULL;
	//scr_putpixel_final=NULL;

	keyboard_issue2.v=0;

	menu_splash_text_active.v=0;
	opcion_no_welcome_message.v=0;



try_fallback_video.v=1;
try_fallback_audio.v=1;

last_x_atributo=0;

inverse_video.v=0;

kempston_mouse_emulation.v=0;


scr_set_driver_name("");
audio_set_driver_name("");

transaction_log_filename[0]=0;

debug_printf_sem_init();

debug_unnamed_console_init();


#ifdef COMPILE_XWINDOWS
	#ifdef USE_XEXT
	#else
	disable_shm=1;
	#endif
#endif




rainbow_enabled.v=0;
autodetect_rainbow.v=1;

contend_enabled.v=1;

zxprinter_enabled.v=0;
zxprinter_motor.v=0;
zxprinter_power.v=0;


	audio_ay_player_mem=NULL;


	//Inicializar rutinas de cpu core para que, al parsear breakpoints del config file, donde aun no hay inicializada maquina,
	//funciones como opcode1=XX , peek(x), etc no peten porque utilizan funciones peek. Inicializar también las de puerto por si acaso
	poke_byte=poke_byte_vacio;
	poke_byte_no_time=poke_byte_vacio;
	peek_byte=peek_byte_vacio;
	peek_byte_no_time=peek_byte_vacio;
	lee_puerto=lee_puerto_vacio;
	//lee_puerto_no_time=lee_puerto_vacio;
	out_port=out_port_vacio;
	realjoystick_init=realjoystick_null_init;
	realjoystick_main=realjoystick_null_main;


	//Inicializo tambien la de push
	push_valor=push_valor_default;



	clear_lista_teclas_redefinidas();

	debug_nested_cores_pokepeek_init();


	//esto va aqui, asi podemos parsear el establecer set-breakpoint desde linea de comandos
	init_breakpoints_table();
	init_watches_table();

	extended_stack_clear();

	last_filesused_clear();

	//estos dos se inicializan para que al hacer set_emulator_speed, que se ejecuta antes de init audio,
	//si no hay driver inicializado, no llamarlos
	audio_end=NULL;
	audio_init=NULL;




//Establecer rutas de utilidades externas
#if defined(__APPLE__)
                sprintf (external_tool_tar,"/usr/bin/tar");
                sprintf (external_tool_gunzip,"/usr/bin/gunzip");
#endif

	//antiguo
	//realjoystick_new_set_default_functions();

	//nuevo:
	realjoystick_init_events_keys_tables();


		//por si lanzamos un cpu_panic antes de inicializar video, que esto este a NULL y podamos detectarlo para no ejecutarlo
	scr_end_pantalla=NULL;
	memoria_spectrum=NULL;


	//Primero parseamos archivo de configuracion
	debug_parse_config_file.v=0;

		//Si hay parametro de debug parse config file...
		//main_argc,char *main_argv[])
		//printf ("%d\n",main_argc);
		if (main_argc>1) {
			if (!strcmp(main_argv[1],"--debugconfigfile")) {
				debug_parse_config_file.v=1;
			}
		}


	int noconfigfile=0;

                if (main_argc>1) {
                        if (!strcmp(main_argv[1],"--noconfigfile")) {
				noconfigfile=1;
                        }

                        if (!strcmp(main_argv[1],"--configfile")) {
				customconfigfile=main_argv[2];
                        }

                }



	if (noconfigfile==0) {
			//parametros del archivo de configuracion
			configfile_parse();

        	        argc=configfile_argc;
                	argv=configfile_argv;
			puntero_parametro=0;

        	        if (parse_cmdline_options()) {
				//debug_printf(VERBOSE_ERR,"Error parsing configuration file. Disabling autosave feature");
				//Desactivamos autoguardado para evitar que se genere una configuración incompleta
				save_configuration_file_on_exit.v=0;
			}
	}


  	//Luego parseamos parametros por linea de comandos
  	argc=main_argc;
  	argv=main_argv;
  	puntero_parametro=0;

  	if (parse_cmdline_options()) {
		printf ("\n\n");
        	cpu_help();
        	exit(1);
	}

	if (test_config_and_exit.v) exit(0);

	//Init random value. Usado en AY Chip y Random ram y mensajes "kidding"
init_randomize_noise_value();

#ifdef SNAPSHOT_VERSION
	printf ("Build number: " BUILDNUMBER "\n");

	printf ("WARNING. This is a Snapshot version and not a stable one\n"
			 "Some features may not work, random crashes could happen, abnormal CPU use, or lots of debug messages on console\n\n");

	//Si no coincide ese parametro, hacer pausa
	if (strcmp(parameter_disablebetawarning,EMULATOR_VERSION)) {
		sleep (3);
	}
#endif

#ifdef MINGW
        //Si no se ha pasado ningun parametro, ni parametro --nodisableconsole, sea en consola o en archivo de configuracion, liberar consola, con pausa de 2 segundos para que se vea un poco :P
        if (main_argc==1 && windows_no_disable_console.v==0) {
                sleep(2);
                printf ("Disabling text printing on this console. Specify --nodisableconsole or any other command line setting to avoid it\n");
                FreeConsole();
        }
#endif



		//guardamos zoom original. algunos drivers, como fbdev, lo modifican.
                zoom_x_original=zoom_x;
                zoom_y_original=zoom_y;


		//Pausa para leer texto de inicio, copyright, etc
		//desactivada sleep(1);

		//Inicializacion maquina






	init_cpu_tables();

	//movemos esto antes, asi podemos parsear el establecer set-breakpoint desde linea de comandos
	//init_breakpoints_table();


	init_ulaplus_table();
	screen_init_colour_table();

	init_screen_addr_table();

	inicializa_tabla_contend_speed_higher();



#ifdef USE_LINUXREALJOYSTICK

	//Soporte nativo de linux joystick
	if (no_native_linux_realjoystick.v==0) {
		realjoystick_init=realjoystick_linux_init;
		realjoystick_main=realjoystick_linux_main;
	}
#endif


	TIMESENSOR_INIT();


	debug_printf (VERBOSE_INFO,"Starting emulator");




#ifdef USE_PTHREADS
		debug_printf (VERBOSE_INFO,"Using phtreads");
#else
		debug_printf (VERBOSE_INFO,"Not using phtreads");
#endif

	debug_printf (VERBOSE_INFO,"Initializing Machine");

	//Si hemos especificado una rom diferente por linea de comandos


	if (param_custom_romfile!=NULL) {
		set_machine(param_custom_romfile);
	}


	else {
		set_machine(NULL);
	}

	cold_start_cpu_registers();

	reset_cpu();

  //Inicializamos Video antes que el resto de cosas.
  main_init_video();

  //llamar a set_menu_gui_zoom para establecer zoom menu. Ya se ha llamado desde set_machine pero como no hay driver de video aun ahi,
  //no se aplica zoom de gui dado que eso solo es para driver xwindows, sdl etc y no para curses y otros
  set_menu_gui_zoom();

  set_putpixel_zoom();
	menu_init_footer();


	//Despues de inicializar video, llamar a esta funcion, por si hay que cambiar frameskip (especialmente en cocoa)
	screen_set_parameters_slow_machines();


	main_init_audio();



	init_chip_ay();
	ay_init_filters();

	mid_reset_export_buffers();



	//Inicializar joystick en caso de linux native o simulador
	if (realjoystick_is_linux_native() || simulador_joystick) {
		realjoystick_initialize_joystick();
	}


	if (aofilename!=NULL) {
			init_aofile();
	}

  if (vofilename!=NULL) {
      init_vofile();
  }



	//Load Screen
	if (scrfile!=NULL) {
		load_screen(scrfile);
	}



	scr_refresca_pantalla();



	//Capturar segmentation fault
	//desactivado normalmente en versiones snapshot
	signal(SIGSEGV, segfault_signal_handler);

	//Capturar floating point exception
	//desactivado normalmente en versiones snapshot
	signal(SIGFPE, floatingpoint_signal_handler);

  //Capturar sigbus.
  //desactivado normalmente en versiones snapshot
#ifndef MINGW
  signal(SIGBUS, sigbus_signal_handler);
#endif

	//Capturar segint (CTRL+C)
	signal(SIGINT, segint_signal_handler);

	//Capturar segterm
	signal(SIGTERM, segterm_signal_handler);

#ifndef MINGW
	//Capturar sigpipe
	signal(SIGPIPE, sigpipe_signal_handler);
#endif


	//Restaurar ventanas, si conviene. Hacerlo aqui y no mas tarde, para evitar por ejemplo que al salir el logo de splash
	//aparezcan las ventanas en background
	zxvision_restore_windows_on_startup();

	//Inicio bucle principal
	reg_pc=0;
	interrupcion_maskable_generada.v=0;
	interrupcion_non_maskable_generada.v=0;
	interrupcion_timer_generada.v=0;

	z80_ejecutando_halt.v=0;

	esperando_tiempo_final_t_estados.v=0;
	framescreen_saltar=0;


	if (opcion_no_welcome_message.v==0) {
		set_welcome_message();
	}


	if (command_line_load_binary_file!=NULL) {
		load_binary_file(command_line_load_binary_file,command_line_load_binary_address,command_line_load_binary_length);
	}

	if (command_line_ulaplus.v) enable_ulaplus();

	if (command_line_timex_video.v) enable_timex_video();
	//MMC
	if (command_line_mmc.v) mmc_enable();

	if (command_line_divmmc_ports.v) {
		divmmc_mmc_ports_enable();
	}

	if (command_line_divmmc_paging.v) {
		divmmc_diviface_enable();
	}

	if (command_line_divmmc.v) {
		divmmc_mmc_ports_enable();
		divmmc_diviface_enable();
	}

	if (command_line_set_breakpoints.v) {
		if (debug_breakpoints_enabled.v==0) {
		        debug_breakpoints_enabled.v=1;
		        breakpoints_enable();
		}

	}

	if (command_line_enable_midi.v) {
		if (audio_midi_output_init() ) debug_printf (VERBOSE_ERR,"Error initializing midi device");
	}


	//Si la version actual es mas nueva que la anterior, eso solo si el autoguardado de config esta activado
	if (save_configuration_file_on_exit.v && do_no_show_changelog_when_update.v==0) {
		//if (strcmp(last_version_string,EMULATOR_VERSION)) {  //Si son diferentes
		if (strcmp(last_version_string,BUILDNUMBER) && last_version_string[0]!=0) {  //Si son diferentes y last_version_string no es nula
			//Y si driver permite menu normal
			if (si_normal_menu_video_driver()) {
				menu_event_new_version_show_changes.v=1;
				menu_set_menu_abierto(1);
				//menu_abierto=1;
			}
		}
	}




	start_timer_thread();

	gettimeofday(&z80_interrupts_timer_antes, NULL);


	//Apuntar momento de inicio para estadisticas-uptime
	gettimeofday(&zesarux_start_time, NULL);


	init_network_tables();

	//Iniciar ZRCP
	init_remote_protocol();


//En SDL2 y SDL1, rutinas de refresco de pantalla se deben lanzar desde el thread principal. Por tanto:
#ifdef COMPILE_SDL
	if (!strcmp(driver_screen,"sdl")) {
		debug_printf (VERBOSE_INFO,"Calling main loop emulator on the main thread as it is required by SDL2");
		emulator_main_loop();

		//Aqui no se llega nunca pero por si acaso
		return 0;
	}
#endif

	//si no tenemos pthreads, entrar en el bucle principal tal cual
	//si hay pthreads, lanzarlo como thread aparte y quedarnos en un bucle con sleep

#ifdef USE_PTHREADS


	//Esto deberia estar disponible en todos menos en Windows. Logicamente si USE_PTHREADS esta habilitado
	ver_si_enable_thread_main_loop();


	if (si_thread_main_loop) {
		debug_printf (VERBOSE_INFO,"Calling main loop emulator on a thread");
                if (pthread_create( &thread_main_loop, NULL, &thread_main_loop_function, NULL) ) {
                        cpu_panic("Can not create main loop pthread");
                }
	}

	else {
		debug_printf (VERBOSE_INFO,"Calling main loop emulator without threads (although pthreads are available)");
		emulator_main_loop();
		//De aqui hacia abajo no se deberia llegar nunca... ya que esto es para pthreads y windows
		//(y lo de abajo es para cocoa y mas abajo para sistemas sin pthreads)
	}



	#ifdef USE_COCOA

		//Si hay soporte COCOA, dejar solo el thread con el main loop y volver a main (de scrcocoa)

	#else
		//Bucle cerrado con sleep. El bucle main se ha lanzado como thread
		while (1) {
			timer_sleep(1000);
			//printf ("bucle con sleep\n");
		}
	#endif



#else
	debug_printf (VERBOSE_INFO,"Calling main loop emulator without threads");
	emulator_main_loop();
#endif

	//Aqui solo se llega en caso de cocoa

	return 0;

}

void dump_ram_file_on_exit(void)
{
	if (dump_ram_file[0]) {

			debug_printf (VERBOSE_INFO,"Dumping ram contents to file %s",dump_ram_file);

                        //Crear archivo vacio
                        FILE *ptr_ramfile;
                        ptr_ramfile=fopen(dump_ram_file,"wb");

                        int totalsize=49152;

                        z80_byte valor_grabar;
			z80_int dir=16384;

                        if (ptr_ramfile!=NULL) {
                                while (totalsize) {
					valor_grabar=peek_byte_no_time(dir++);
                                        totalsize--;

                                        fwrite(&valor_grabar,1,1,ptr_ramfile);
                                }
                                fclose(ptr_ramfile);
                        }

			else {
				debug_printf(VERBOSE_ERR,"Error writing dump ram file");
			}

	}
}


//Se pasa parametro que dice si guarda o no la configuración.
//antes se guardaba siempre, pero ahora en casos de recepcion de señales de terminar, no se guarda,
//pues generaba segfaults en las rutinas de guardar ventanas (--restorewindow)
void end_emulator_saveornot_config(int saveconfig)
{
	debug_printf (VERBOSE_INFO,"End emulator");








	dump_ram_file_on_exit();

	top_speed_timer.v=0;

//Si se ha llamado aqui desde otro sitio que no sea el pthread del main_loop_emulator, hay que destruir antes el pthread con:
//#ifdef USE_PTHREADS
// if (si_thread_main_loop) {
//        debug_printf (VERBOSE_INFO,"Ending main loop thread");
//        pthread_cancel(thread_main_loop);
// }
//#endif

	menu_abierto=0;

	if (saveconfig && save_configuration_file_on_exit.v) {
		int uptime_seconds=timer_get_uptime_seconds();

  		total_minutes_use +=uptime_seconds/60;
		util_write_configfile();
	}

	//end_remote_protocol(); porque si no, no se puede finalizar el emulador desde el puerto telnet
	if (!remote_calling_end_emulator.v) {
		end_remote_protocol();
	}

	reset_menu_overlay_function();

	cls_menu_overlay();

	close_aofile();
	close_vofile();
	close_zxprinter_bitmap_file();
	close_zxprinter_ocr_file();

	//Flush write devices
	mmc_flush_flash_to_disk();

	audio_midi_output_finish();
	audio_thread_finish();
	audio_playing.v=0;
	audio_end();

	//printf ("footer: %d\n",menu_footer);

	//Desactivo footer para que no se actualice, sino a veces aparece el footer (cpu, fps, etc) en color grisaceo mientras hace el fade
	menu_footer=0;


  scr_end_pantalla();


	if (remote_calling_end_emulator.v) end_remote_protocol();

  exit(0);

}


void end_emulator(void)
{
	end_emulator_saveornot_config(1);
}
