/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>
#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <errno.h>

#include "audio.h"
#include "contend.h"
#include "datagear.h"
#include "debug.h"
#include "joystick.h"
#include "menu_system.h"
#include "menu.h"
#include "menu_items.h"
#include "multiface.h"
#include "printers.h"
#include "realjoystick.h"
#include "screen.h"
#include "settings.h"
#include "tbblue.h"
#include "timer.h"
#include "uartbridge.h"
#include "ula.h"


#if defined(__APPLE__)
	#include <sys/syslimits.h>

	#include <sys/resource.h>

#endif

#include "compileoptions.h"


#ifdef COMPILE_XWINDOWS
	#include "scrxwindows.h"
#endif


int menu_inicio_opcion_seleccionada=0;
int display_settings_opcion_seleccionada=0;
int mem_breakpoints_opcion_seleccionada=0;
int breakpoints_opcion_seleccionada=0;
int cpu_transaction_log_opcion_seleccionada=0;
int tsconf_layer_settings_opcion_seleccionada=0;
int debug_tsconf_opcion_seleccionada=0;
int audio_settings_opcion_seleccionada=0;
int hardware_settings_opcion_seleccionada=0;
int keyboard_settings_opcion_seleccionada=0;
int hardware_memory_settings_opcion_seleccionada=0;
int debug_settings_opcion_seleccionada=0;
int hardware_advanced_opcion_seleccionada=0;
int hardware_printers_opcion_seleccionada=0;
int hardware_realjoystick_opcion_seleccionada=0;
int hardware_realjoystick_event_opcion_seleccionada=0;
int hardware_realjoystick_keys_opcion_seleccionada=0;
int find_opcion_seleccionada=0;
int find_bytes_opcion_seleccionada=0;
int find_lives_opcion_seleccionada=0;
int poke_opcion_seleccionada=0;
int hardware_redefine_keys_opcion_seleccionada=0;
int hardware_set_f_functions_opcion_seleccionada=0;
int hardware_set_f_func_action_opcion_seleccionada=0;
int ula_settings_opcion_seleccionada=0;
int settings_opcion_seleccionada=0;
int uartbridge_opcion_seleccionada=0;
int settings_audio_opcion_seleccionada=0;
int cpu_settings_opcion_seleccionada=0;
int cpu_stats_opcion_seleccionada=0;
int settings_display_opcion_seleccionada=0;
int settings_debug_opcion_seleccionada=0;
int menu_tbblue_hardware_id_opcion_seleccionada=0;
int menu_watches_opcion_seleccionada=0;
int debug_tsconf_dma_opcion_seleccionada=0;
int debug_pok_file_opcion_seleccionada=0;


void menu_display_settings(MENU_ITEM_PARAMETERS)
{

	menu_item *array_menu_display_settings;
	menu_item item_seleccionado;
	int retorno_menu;
	do {
		menu_add_item_menu_inicial(&array_menu_display_settings,"",MENU_OPCION_UNASSIGNED,NULL,NULL);

			menu_add_item_menu(array_menu_display_settings,"~~Load Screen",MENU_OPCION_NORMAL,menu_display_load_screen,NULL);
			menu_add_item_menu_shortcut(array_menu_display_settings,'l');

		menu_add_item_menu(array_menu_display_settings,"~~Save Screen",MENU_OPCION_NORMAL,menu_display_save_screen,NULL);
		menu_add_item_menu_shortcut(array_menu_display_settings,'s');


		menu_add_item_menu(array_menu_display_settings,"~~View Screen",MENU_OPCION_NORMAL,menu_view_screen,NULL);
		menu_add_item_menu_shortcut(array_menu_display_settings,'v');

                menu_add_item_menu(array_menu_display_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_display_settings);

                retorno_menu=menu_dibuja_menu(&display_settings_opcion_seleccionada,&item_seleccionado,array_menu_display_settings,"Display" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {

			//llamamos por valor de funcion
        	        if (item_seleccionado.menu_funcion!=NULL) {
                	        //printf ("actuamos por funcion\n");
	                        item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
        	        }
		}

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_cpu_transaction_log(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_cpu_transaction_log;
        menu_item item_seleccionado;
        int retorno_menu;
        do {

                char string_transactionlog_shown[18];
                menu_tape_settings_trunc_name(transaction_log_filename,string_transactionlog_shown,18);

                menu_add_item_menu_inicial_format(&array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_file,NULL,"Log file [%s]",string_transactionlog_shown );


                if (transaction_log_filename[0]!=0) {
                        menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable,NULL,"[%c] Transaction log enabled",(cpu_transaction_log_enabled.v ? 'X' : ' ' ) );
						
						menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable_rotate,NULL,"[%c] Autorotate files",(cpu_transaction_log_rotate_enabled.v ? 'X' : ' ' ) );
						if (cpu_transaction_log_rotate_enabled.v) {
							menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_rotate_number,NULL,"[%d] Rotate files",cpu_transaction_log_rotated_files);
							menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_rotate_size,NULL,"[%d] Rotate size (MB)",cpu_transaction_log_rotate_size);
							menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_rotate_lines,NULL,"[%d] Rotate lines",cpu_transaction_log_rotate_lines);
						}

						menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_truncate,NULL,"    Truncate log file");
                }

		menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_ignore_rep_halt,NULL,"[%c] Ignore repeated halt",(cpu_trans_log_ignore_repeated_halt.v ? 'X' : ' '));


		menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable_datetime,NULL,"[%c] Store Date & Time",(cpu_transaction_log_store_datetime.v ? 'X' : ' '));
		menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable_tstates,NULL,"[%c] Store T-States",(cpu_transaction_log_store_tstates.v ? 'X' : ' '));
		menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable_address,NULL,"[%c] Store Address",(cpu_transaction_log_store_address.v ? 'X' : ' '));
		menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable_opcode,NULL,"[%c] Store Opcode",(cpu_transaction_log_store_opcode.v ? 'X' : ' '));
		menu_add_item_menu_format(array_menu_cpu_transaction_log,MENU_OPCION_NORMAL,menu_cpu_transaction_log_enable_registers,NULL,"[%c] Store Registers",(cpu_transaction_log_store_registers.v ? 'X' : ' '));
		


               menu_add_item_menu(array_menu_cpu_transaction_log,"",MENU_OPCION_SEPARADOR,NULL,NULL);

                menu_add_ESC_item(array_menu_cpu_transaction_log);

                retorno_menu=menu_dibuja_menu(&cpu_transaction_log_opcion_seleccionada,&item_seleccionado,array_menu_cpu_transaction_log,"CPU Transaction Log" );

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                                
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

}


void menu_mem_breakpoints(MENU_ITEM_PARAMETERS)
{

	menu_espera_no_tecla();

        menu_item *array_menu_mem_breakpoints;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
		menu_add_item_menu_inicial_format(&array_menu_mem_breakpoints,MENU_OPCION_NORMAL,menu_mem_breakpoints_edit,NULL,"~~Edit Breakpoint");
		menu_add_item_menu_shortcut(array_menu_mem_breakpoints,'e');

		menu_add_item_menu_format(array_menu_mem_breakpoints,MENU_OPCION_NORMAL,menu_mem_breakpoints_list,NULL,"~~List breakpoints");
		menu_add_item_menu_shortcut(array_menu_mem_breakpoints,'l');

		menu_add_item_menu_format(array_menu_mem_breakpoints,MENU_OPCION_NORMAL,menu_mem_breakpoints_clear,NULL,"~~Clear breakpoints");
		menu_add_item_menu_shortcut(array_menu_mem_breakpoints,'c');


                menu_add_item_menu(array_menu_mem_breakpoints,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_mem_breakpoints);
                retorno_menu=menu_dibuja_menu(&mem_breakpoints_opcion_seleccionada,&item_seleccionado,array_menu_mem_breakpoints,"Memory Breakpoints" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_breakpoints(MENU_ITEM_PARAMETERS)
{

	menu_espera_no_tecla();

        menu_item *array_menu_breakpoints;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
		menu_add_item_menu_inicial_format(&array_menu_breakpoints,MENU_OPCION_NORMAL,menu_breakpoints_enable_disable,NULL,"~~Breakpoints: %s",
			(debug_breakpoints_enabled.v ? "On" : "Off") );
		menu_add_item_menu_shortcut(array_menu_breakpoints,'b');

		//char buffer_texto[40];

                int i;

		menu_add_item_menu_format(array_menu_breakpoints,MENU_OPCION_NORMAL,menu_breakpoints_condition_evaluate_new,NULL,"~~Evaluate Expression");
		menu_add_item_menu_shortcut(array_menu_breakpoints,'e');

#define MAX_BRK_SHOWN_MENU 10		

		//Para mostrar los tooltip de cada linea, como contenido del breakpoint
		char buffer_temp_breakpoint_array[MAX_BRK_SHOWN_MENU][MAX_BREAKPOINT_CONDITION_LENGTH];		

		//Maximo 10 breakpoints mostrados en pantalla. Para mas, usar ZRCP
        for (i=0;i<MAX_BREAKPOINTS_CONDITIONS && i<MAX_BRK_SHOWN_MENU;i++) {
			char string_condition_shown[23];
			char string_action_shown[7];

			char string_condition_action[33];

			if (debug_breakpoints_conditions_array_tokens[i][0].tipo!=TPT_FIN) {
			
				//nuevo parser de breakpoints
				char buffer_temp_breakpoint[MAX_BREAKPOINT_CONDITION_LENGTH];
				exp_par_tokens_to_exp(debug_breakpoints_conditions_array_tokens[i],buffer_temp_breakpoint,MAX_PARSER_TOKENS_NUM);

				//metemos en array para mostrar tooltip
				strcpy(buffer_temp_breakpoint_array[i],buffer_temp_breakpoint);

				menu_tape_settings_trunc_name(buffer_temp_breakpoint,string_condition_shown,23);
				
				//printf ("brkp %d [%s]\n",i,string_condition_shown);

				menu_tape_settings_trunc_name(debug_breakpoints_actions_array[i],string_action_shown,7);
				if (debug_breakpoints_actions_array[i][0]) sprintf (string_condition_action,"%s->%s",string_condition_shown,string_action_shown);

				//Si accion es menu, no escribir, para que quepa bien en pantalla
				//else sprintf (string_condition_action,"%s->menu",string_condition_shown);
				else sprintf (string_condition_action,"%s",string_condition_shown);
			}
			else {
				sprintf(string_condition_action,"None");
				//metemos en array para mostrar tooltip
				buffer_temp_breakpoint_array[i][0]=0;			
			}

			char string_condition_action_shown[23];
			menu_tape_settings_trunc_name(string_condition_action,string_condition_action_shown,23);

																																																										//0123456789012345678901234567890
			if (debug_breakpoints_conditions_enabled[i]==0 || debug_breakpoints_enabled.v==0) {														//Di 12345678901234: 12345678
				menu_add_item_menu_format(array_menu_breakpoints,MENU_OPCION_NORMAL,menu_breakpoints_conditions_set,menu_breakpoints_cond,"Di %d: %s",i+1,string_condition_action_shown);
			}
            
			else {
				menu_add_item_menu_format(array_menu_breakpoints,MENU_OPCION_NORMAL,menu_breakpoints_conditions_set,menu_breakpoints_cond,"En %d: %s",i+1,string_condition_action_shown);
			}


			menu_add_item_menu_espacio(array_menu_breakpoints,menu_breakpoints_condition_enable_disable);

			menu_add_item_menu_valor_opcion(array_menu_breakpoints,i);

        }

		menu_add_item_menu(array_menu_breakpoints,"",MENU_OPCION_SEPARADOR,NULL,NULL);

		menu_add_item_menu_format(array_menu_breakpoints,MENU_OPCION_NORMAL,menu_mem_breakpoints,NULL,"~~Memory breakpoints");
		menu_add_item_menu_shortcut(array_menu_breakpoints,'m');


		menu_add_item_menu_format(array_menu_breakpoints,MENU_OPCION_NORMAL,menu_clear_all_breakpoints,NULL,"Clear all breakpoints");


                menu_add_item_menu(array_menu_breakpoints,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_breakpoints);
                retorno_menu=menu_dibuja_menu(&breakpoints_opcion_seleccionada,&item_seleccionado,array_menu_breakpoints,"Breakpoints" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_tsconf_layer_settings(MENU_ITEM_PARAMETERS)
{
	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

	int ancho=20;
	int alto=22;

	int x=menu_center_x()-ancho/2;
	int y;
	//y=1;	

	{
		alto=20;
		//y=1;
	}


	y=menu_center_y()-alto/2;

	zxvision_window ventana;

	zxvision_new_window(&ventana,x,y,ancho,alto,
							ancho-1,alto-2,"Video Layers");
	zxvision_draw_window(&ventana);		


    //Cambiamos funcion overlay de texto de menu
    set_menu_overlay_function(menu_tsconf_layer_overlay);

	menu_tsconf_layer_overlay_window=&ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui	

    menu_item *array_menu_tsconf_layer_settings;
    menu_item item_seleccionado;
    int retorno_menu;						

    do {

		//Valido tanto para cuando multitarea es off y para que nada mas entrar aqui, se vea, sin tener que esperar el medio segundo 
		//que he definido en el overlay para que aparezca
		menu_tsconf_layer_overlay_mostrar_texto();

        int lin=1;

		{
 			menu_add_item_menu_inicial_format(&array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_settings_ula,NULL,"%s",(tbblue_force_disable_layer_ula.v ? "Disabled" : "Enabled "));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,1,lin);		
			lin+=3;			

 			menu_add_item_menu_format(array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_settings_tilemap,NULL,"%s",(tbblue_force_disable_layer_tilemap.v ? "Disabled" : "Enabled "));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,1,lin);			
			lin+=2;

 			menu_add_item_menu_format(array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_reveal_ula,NULL,"%s",(tbblue_reveal_layer_ula.v ? "Reveal" : "Normal"));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,12,lin);	

			lin+=3;					

			menu_add_item_menu_format(array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_settings_sprites,NULL,"%s",(tbblue_force_disable_layer_sprites.v ? "Disabled" : "Enabled "));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,1,lin);
 			menu_add_item_menu_format(array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_reveal_sprites,NULL,"%s",(tbblue_reveal_layer_sprites.v ? "Reveal" : "Normal"));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,12,lin);				
			lin+=3;

			menu_add_item_menu_format(array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_settings_layer_two,NULL,"%s",(tbblue_force_disable_layer_layer_two.v ? "Disabled" : "Enabled "));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,1,lin);
 			menu_add_item_menu_format(array_menu_tsconf_layer_settings,MENU_OPCION_NORMAL,menu_tbblue_layer_reveal_layer2,NULL,"%s",(tbblue_reveal_layer_layer2.v ? "Reveal" : "Normal"));
			menu_add_item_menu_tabulado(array_menu_tsconf_layer_settings,12,lin);				
			lin+=3;				
		}

        retorno_menu=menu_dibuja_menu(&tsconf_layer_settings_opcion_seleccionada,&item_seleccionado,array_menu_tsconf_layer_settings,"TSConf Layers" );

	//En caso de menus tabulados, es responsabilidad de este de borrar la ventana
        cls_menu_overlay();

				//Nombre de ventana solo aparece en el caso de stdout
                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
				//En caso de menus tabulados, es responsabilidad de este de borrar la ventana
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

       //restauramos modo normal de texto de menu
       set_menu_overlay_function(normal_overlay_texto_menu);

	   cls_menu_overlay();

	//En caso de menus tabulados, es responsabilidad de este de liberar ventana
	zxvision_destroy_window(&ventana);			   
}


void menu_debug_tsconf_tbblue_msx(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_debug_tsconf_tbblue_msx;
        menu_item item_seleccionado;
	int retorno_menu;
        do {
		menu_add_item_menu_inicial_format(&array_menu_debug_tsconf_tbblue_msx,MENU_OPCION_NORMAL,menu_debug_tsconf_tbblue_msx_videoregisters,NULL,"Video ~~Info");
		menu_add_item_menu_shortcut(array_menu_debug_tsconf_tbblue_msx,'i');

		menu_add_item_menu_format(array_menu_debug_tsconf_tbblue_msx,MENU_OPCION_NORMAL,menu_tsconf_layer_settings,NULL,"Video ~~Layers");
		menu_add_item_menu_shortcut(array_menu_debug_tsconf_tbblue_msx,'l');

		menu_add_item_menu_format(array_menu_debug_tsconf_tbblue_msx,MENU_OPCION_NORMAL,menu_debug_tsconf_tbblue_msx_spritenav,NULL,"~~Sprite navigator");
		menu_add_item_menu_shortcut(array_menu_debug_tsconf_tbblue_msx,'s');

			menu_add_item_menu_format(array_menu_debug_tsconf_tbblue_msx,MENU_OPCION_NORMAL,menu_debug_tsconf_tbblue_msx_tilenav,NULL,"~~Tile navigator");
			menu_add_item_menu_shortcut(array_menu_debug_tsconf_tbblue_msx,'t');

                menu_add_item_menu(array_menu_debug_tsconf_tbblue_msx,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_debug_tsconf_tbblue_msx);

		char titulo_ventana[33];

		strcpy(titulo_ventana,"Debug TBBlue");
                retorno_menu=menu_dibuja_menu(&debug_tsconf_opcion_seleccionada,&item_seleccionado,array_menu_debug_tsconf_tbblue_msx,titulo_ventana);


		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_find_bytes(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_find_bytes;
        menu_item item_seleccionado;
        int retorno_menu;

        //Si puntero memoria no esta asignado, asignarlo, o si hemos cambiado de tipo de maquina
        menu_find_bytes_alloc_if_needed();


        do {

                menu_add_item_menu_inicial_format(&array_menu_find_bytes,MENU_OPCION_NORMAL,menu_find_bytes_find,NULL,"Find byte");
                menu_add_item_menu_format(array_menu_find_bytes,MENU_OPCION_NORMAL,menu_find_bytes_view_results,NULL,"View results");

                menu_add_item_menu_format(array_menu_find_bytes,MENU_OPCION_NORMAL,menu_find_bytes_clear_results,NULL,"Clear results");

                menu_add_item_menu(array_menu_find_bytes,"",MENU_OPCION_SEPARADOR,NULL,NULL);


                menu_add_ESC_item(array_menu_find_bytes);

                retorno_menu=menu_dibuja_menu(&find_bytes_opcion_seleccionada,&item_seleccionado,array_menu_find_bytes,"Find bytes" );

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_find_lives(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_find_lives;
        menu_item item_seleccionado;
        int retorno_menu;




        do {

								if (menu_find_lives_state==0) {
                	menu_add_item_menu_inicial_format(&array_menu_find_lives,MENU_OPCION_NORMAL,menu_find_lives_initial,NULL,"Tell current lives (initial)");
								}

								if (menu_find_lives_state==1) {
									menu_add_item_menu_inicial_format(&array_menu_find_lives,MENU_OPCION_NORMAL,menu_find_lives_initial,NULL,"Tell current lives (decr.)");
								}

								if (menu_find_lives_state==2) {
									menu_add_item_menu_inicial_format(&array_menu_find_lives,MENU_OPCION_NORMAL,NULL,NULL,"Lives pointer: %d",menu_find_lives_pointer);
									menu_add_item_menu_format(array_menu_find_lives,MENU_OPCION_NORMAL,NULL,NULL,"Lives: %d",peek_byte_z80_moto(menu_find_lives_pointer) );
									menu_add_item_menu_format(array_menu_find_lives,MENU_OPCION_NORMAL,menu_find_lives_set,NULL,"Set lives");
								}

								if (menu_find_lives_state==1 || menu_find_lives_state==2) {
									menu_add_item_menu_format(array_menu_find_lives,MENU_OPCION_NORMAL,menu_find_lives_restart,NULL,"Restart process");
								}

                menu_add_item_menu(array_menu_find_lives,"",MENU_OPCION_SEPARADOR,NULL,NULL);


                menu_add_ESC_item(array_menu_find_lives);

                retorno_menu=menu_dibuja_menu(&find_lives_opcion_seleccionada,&item_seleccionado,array_menu_find_lives,"Find lives" );

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                                
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_find(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_find;
        menu_item item_seleccionado;
        int retorno_menu;

        do {

                menu_add_item_menu_inicial_format(&array_menu_find,MENU_OPCION_NORMAL,menu_find_bytes,NULL,"Find byte");

								menu_add_item_menu_format(array_menu_find,MENU_OPCION_NORMAL,menu_find_lives,NULL,"Find lives address");


                menu_add_item_menu(array_menu_find,"",MENU_OPCION_SEPARADOR,NULL,NULL);

                menu_add_ESC_item(array_menu_find);

                retorno_menu=menu_dibuja_menu(&find_opcion_seleccionada,&item_seleccionado,array_menu_find,"Find" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_poke(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_poke;
        menu_item item_seleccionado;
        int retorno_menu;

        do {


                menu_add_item_menu_inicial_format(&array_menu_poke,MENU_OPCION_NORMAL,menu_debug_poke,NULL,"~~Poke");
                menu_add_item_menu_shortcut(array_menu_poke,'p');

			menu_add_item_menu(array_menu_poke,"Poke from .POK ~~file",MENU_OPCION_NORMAL,menu_debug_poke_pok_file,NULL);
			menu_add_item_menu_shortcut(array_menu_poke,'f');


                menu_add_item_menu(array_menu_poke,"",MENU_OPCION_SEPARADOR,NULL,NULL);


                //menu_add_item_menu(array_menu_poke,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_poke);

                retorno_menu=menu_dibuja_menu(&poke_opcion_seleccionada,&item_seleccionado,array_menu_poke,"Poke" );


                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}



void menu_debug_settings(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_debug_settings;
        menu_item item_seleccionado;
	int retorno_menu;

        do {
                menu_add_item_menu_inicial(&array_menu_debug_settings,"~~Reset",MENU_OPCION_NORMAL,menu_debug_reset,NULL);
		menu_add_item_menu_shortcut(array_menu_debug_settings,'r');

	                menu_add_item_menu(array_menu_debug_settings,"~~Hard Reset",MENU_OPCION_NORMAL,menu_debug_hard_reset,NULL);

    		menu_add_item_menu(array_menu_debug_settings,"Generate ~~Drive",MENU_OPCION_NORMAL,menu_debug_nmi_drive,NULL);
			menu_add_item_menu_shortcut(array_menu_debug_settings,'v');

			if (multiface_enabled.v && (tbblue_registers[6]&8) ) menu_add_item_menu(array_menu_debug_settings,"Generate ~~NMI",MENU_OPCION_NORMAL,menu_debug_nmi_multiface_tbblue,NULL);
			menu_add_item_menu_shortcut(array_menu_debug_settings,'n');

		menu_add_item_menu(array_menu_debug_settings,"~~Debug CPU",MENU_OPCION_NORMAL,menu_debug_registers,NULL);
		menu_add_item_menu_shortcut(array_menu_debug_settings,'d');

		if (datagear_dma_emulation.v) {
			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_dma_tsconf_zxuno,NULL,"Debug D~~MA");
			menu_add_item_menu_shortcut(array_menu_debug_settings,'m');
		}					

		{
			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_ioports,NULL,"Debug ~~I/O Ports");
			menu_add_item_menu_shortcut(array_menu_debug_settings,'i');

			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_cpu_transaction_log,NULL,"~~CPU Transaction Log");
			menu_add_item_menu_shortcut(array_menu_debug_settings,'c');
		}


		{
			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_tsconf_tbblue_msx,NULL,"~~TBBlue");
			menu_add_item_menu_shortcut(array_menu_debug_settings,'t');
		}

        menu_add_item_menu(array_menu_debug_settings,"Debug console",MENU_OPCION_NORMAL,menu_debug_unnamed_console,NULL);


		menu_add_item_menu(array_menu_debug_settings,"He~~xadecimal Editor",MENU_OPCION_NORMAL,menu_debug_hexdump,NULL);
		menu_add_item_menu_shortcut(array_menu_debug_settings,'x');

		menu_add_item_menu(array_menu_debug_settings,"View ~~Basic",MENU_OPCION_NORMAL,menu_debug_view_basic,NULL);
		menu_add_item_menu_shortcut(array_menu_debug_settings,'b');

		if (si_complete_video_driver() ) {
			menu_add_item_menu(array_menu_debug_settings,"View ~~Sprites",MENU_OPCION_NORMAL,menu_debug_view_sprites,NULL);
			menu_add_item_menu_shortcut(array_menu_debug_settings,'s');
		}

#ifdef EMULATE_CPU_STATS
			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_cpu_stats,NULL,"View CPU Statistics");

#endif

    menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_find,NULL,"~~Find");
		menu_add_item_menu_shortcut(array_menu_debug_settings,'f');


		menu_add_item_menu(array_menu_debug_settings,"~~Poke",MENU_OPCION_NORMAL,menu_poke,NULL);
		menu_add_item_menu_shortcut(array_menu_debug_settings,'p');


		menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_load_binary,NULL,"L~~oad binary block");
		menu_add_item_menu_shortcut(array_menu_debug_settings,'o');

		menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_save_binary,NULL,"S~~ave binary block");
		menu_add_item_menu_shortcut(array_menu_debug_settings,'a');


#ifdef TIMESENSORS_ENABLED

		menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_timesensors_enable,NULL,"[%c] Timesensors",
		(timesensors_started ? 'X' : ' ' ));

		if (timesensors_started) {
			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_timesensors,NULL,"    List Timesensors");

			menu_add_item_menu_format(array_menu_debug_settings,MENU_OPCION_NORMAL,menu_debug_timesensors_init,NULL,"    Init Timesensors");
		}

#endif

                menu_add_item_menu(array_menu_debug_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_debug_settings);

                retorno_menu=menu_dibuja_menu(&debug_settings_opcion_seleccionada,&item_seleccionado,array_menu_debug_settings,"Debug" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_uartbridge(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_uartbridge;
        menu_item item_seleccionado;
        int retorno_menu;
        do {

                char string_uartbridge_file_shown[13];

			
                        menu_tape_settings_trunc_name(uartbridge_name,string_uartbridge_file_shown,13);
                        menu_add_item_menu_inicial_format(&array_menu_uartbridge,MENU_OPCION_NORMAL,menu_uartbridge_file,NULL,"~~File [%s]",string_uartbridge_file_shown);
                        menu_add_item_menu_shortcut(array_menu_uartbridge,'f');


						//Lo separamos en dos pues cuando no esta habilitado, tiene que comprobar que el path no sea nulo
						if (uartbridge_enabled.v) {
							menu_add_item_menu_format(array_menu_uartbridge,MENU_OPCION_NORMAL,menu_uartbridge_enable,NULL,"[X] ~~Enabled");
						}
						else {
							menu_add_item_menu_format(array_menu_uartbridge,MENU_OPCION_NORMAL,menu_uartbridge_enable,menu_uartbridge_cond,"[ ] ~~Enabled");
						}	
						menu_add_item_menu_shortcut(array_menu_uartbridge,'e');


#ifndef MINGW

						if (uartbridge_speed==CHDEV_SPEED_DEFAULT) {
							menu_add_item_menu_format(array_menu_uartbridge,MENU_OPCION_NORMAL,menu_uartbridge_speed,menu_uartbridge_speed_cond,"[Default] Speed");
						}
						else {
							menu_add_item_menu_format(array_menu_uartbridge,MENU_OPCION_NORMAL,menu_uartbridge_speed,menu_uartbridge_speed_cond,"[%d] Speed",
							chardevice_getspeed_enum_int(uartbridge_speed));
						}

#endif
					

                        menu_add_item_menu(array_menu_uartbridge,"",MENU_OPCION_SEPARADOR,NULL,NULL);

                menu_add_ESC_item(array_menu_uartbridge);

                retorno_menu=menu_dibuja_menu(&uartbridge_opcion_seleccionada,&item_seleccionado,array_menu_uartbridge,"UART Bridge" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_settings_audio(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_settings_audio;
	menu_item item_seleccionado;
	int retorno_menu;

        do {
		menu_add_item_menu_inicial_format(&array_menu_settings_audio,MENU_OPCION_NORMAL,menu_audio_volume,NULL,"    Output ~~Volume [%d%%]", audiovolume);
		menu_add_item_menu_shortcut(array_menu_settings_audio,'v');


    menu_add_item_menu(array_menu_settings_audio,"",MENU_OPCION_SEPARADOR,NULL,NULL);

			menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_audio_beeper,NULL,"[%c] ~~Beeper",(beeper_enabled.v ? 'X' : ' '));
			menu_add_item_menu_shortcut(array_menu_settings_audio,'b');

                        if (beeper_enabled.v)
                        {
                                menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_audio_beeper_real,NULL,"[%c] ~~Real Beeper",(beeper_real_enabled==1 ? 'X' : ' '));
                                menu_add_item_menu_shortcut(array_menu_settings_audio,'r');

                        menu_add_item_menu(array_menu_settings_audio,"",MENU_OPCION_SEPARADOR,NULL,NULL);

                                menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_audio_beep_filter_on_rom_save,NULL,"[%c] ROM SAVE filter",(output_beep_filter_on_rom_save.v ? 'X' : ' '));

                                if (output_beep_filter_on_rom_save.v) {
                                        menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_audio_beep_alter_volume,NULL,"[%c] Alter beeper volume",
                                        (output_beep_filter_alter_volume.v ? 'X' : ' ') );

                                        if (output_beep_filter_alter_volume.v) {
                                                menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_audio_beep_volume,NULL,"[%d] Beeper volume",output_beep_filter_volume);
                                        }
                                }
                        }

		menu_add_item_menu(array_menu_settings_audio,"",MENU_OPCION_SEPARADOR,NULL,NULL);

		char string_aofile_shown[10];
		menu_tape_settings_trunc_name(aofilename,string_aofile_shown,10);
		menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_aofile,NULL,"Audio ~~out to file [%s]",string_aofile_shown);
		menu_add_item_menu_shortcut(array_menu_settings_audio,'o');


		menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_aofile_insert,menu_aofile_cond,"[%c] Audio file ~~inserted",(aofile_inserted.v ? 'X' : ' ' ));
		menu_add_item_menu_shortcut(array_menu_settings_audio,'i');

			if (!strcmp(audio_new_driver_name,"pcspeaker")) {
				menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_pcspeaker_wait_time,NULL,"[%2d] PC Speaker Wait time",audiopcspeaker_tiempo_espera);

				menu_add_item_menu_format(array_menu_settings_audio,MENU_OPCION_NORMAL,menu_pcspeaker_auto_calibrate_wait_time,NULL,"     Autocalibrate Wait time");					
			}				

                menu_add_item_menu(array_menu_settings_audio,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_settings_audio);

                retorno_menu=menu_dibuja_menu(&settings_audio_opcion_seleccionada,&item_seleccionado,array_menu_settings_audio,"Audio Settings" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
	                //llamamos por valor de funcion
        	        if (item_seleccionado.menu_funcion!=NULL) {
                	        //printf ("actuamos por funcion\n");
	                        item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
        	        }
		}

	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_cpu_settings(MENU_ITEM_PARAMETERS)
{
    menu_item *array_menu_cpu_settings;
    menu_item item_seleccionado;
    int retorno_menu;
    do {
			menu_add_item_menu_inicial_format(&array_menu_cpu_settings,MENU_OPCION_NORMAL,menu_cpu_type,NULL,"Z80 CPU Type [%s]",z80_cpu_types_strings[z80_cpu_current_type]);


                menu_add_item_menu(array_menu_cpu_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_cpu_settings);

                retorno_menu=menu_dibuja_menu(&cpu_settings_opcion_seleccionada,&item_seleccionado,array_menu_cpu_settings,"CPU Settings" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_settings_debug(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_settings_debug;
        menu_item item_seleccionado;
	int retorno_menu;
        do {
                char string_zesarux_zxi_hardware_debug_file_shown[18];

		menu_add_item_menu_inicial_format(&array_menu_settings_debug,MENU_OPCION_NORMAL,menu_debug_registers_console,NULL,"[%c] Show r~~egisters in console",(debug_registers==1 ? 'X' : ' '));
		menu_add_item_menu_shortcut(array_menu_settings_debug,'e');

		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL,menu_debug_shows_invalid_opcode,NULL,"[%c] Show ~~invalid opcode",
			(debug_shows_invalid_opcode.v ? 'X' : ' ') ); 
		menu_add_item_menu_shortcut(array_menu_settings_debug,'i');

		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL,menu_debug_configuration_stepover,NULL,"[%c] Step ~~over interrupt",(remote_debug_settings&32 ? 'X' : ' ') );
		menu_add_item_menu_shortcut(array_menu_settings_debug,'o');

		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_debug_settings_show_screen,NULL,"[%c] Show display on debug",
			( debug_settings_show_screen.v ? 'X' : ' ') );


		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_debug_settings_show_scanline,NULL,"[%c] Shows electron on debug",
			( menu_debug_registers_if_showscan.v ? 'X' : ' ') );


		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL,menu_debug_verbose,NULL,"[%d] Verbose ~~level",verbose_level);
		menu_add_item_menu_shortcut(array_menu_settings_debug,'l');	

		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL,menu_debug_unnamed_console_enable,NULL,"[%c] Enable debug console window",
			( debug_unnamed_console_enabled.v ? 'X' : ' ') );    

		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL,menu_debug_verbose_always_console,NULL,"[%c] Always verbose console",
			( debug_always_show_messages_in_console.v ? 'X' : ' ') );


		menu_add_item_menu(array_menu_settings_debug,"",MENU_OPCION_SEPARADOR,NULL,NULL);		

#ifndef NETWORKING_DISABLED
		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_debug_configuration_remoteproto,NULL,"[%c] ZRCP Remote protocol",(remote_protocol_enabled.v ? 'X' : ' ') );
		menu_add_item_menu_shortcut(array_menu_settings_debug,'r');

		if (remote_protocol_enabled.v) {
			menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_debug_configuration_remoteproto_port,NULL,"[%d] Remote protocol ~~port",remote_protocol_port );
			menu_add_item_menu_shortcut(array_menu_settings_debug,'p');
		}

#endif

		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_hardware_debug_port,NULL,"[%c] Hardware ~~debug ports",(hardware_debug_port.v ? 'X' : ' ') );
		menu_add_item_menu_shortcut(array_menu_settings_debug,'d');


		if (hardware_debug_port.v) {
			menu_tape_settings_trunc_name(zesarux_zxi_hardware_debug_file,string_zesarux_zxi_hardware_debug_file_shown,18);
        	menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL,menu_zesarux_zxi_hardware_debug_file,NULL,"Byte ~~file [%s]",string_zesarux_zxi_hardware_debug_file_shown);
			menu_add_item_menu_shortcut(array_menu_settings_debug,'f');							
		}

		menu_add_item_menu(array_menu_settings_debug,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		
		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_breakpoints_condition_behaviour,NULL,"~~Breakp. behaviour [%s]",(debug_breakpoints_cond_behaviour.v ? "On Change" : "Always") );
		menu_add_item_menu_shortcut(array_menu_settings_debug,'b');


		char show_fired_breakpoint_type[30];
		if (debug_show_fired_breakpoints_type==0) strcpy(show_fired_breakpoint_type,"Always");
		else if (debug_show_fired_breakpoints_type==1) strcpy(show_fired_breakpoint_type,"NoPC");
		else strcpy(show_fired_breakpoint_type,"Never");																	//						   OnlyNonPC
																															//  01234567890123456789012345678901
		menu_add_item_menu_format(array_menu_settings_debug,MENU_OPCION_NORMAL, menu_debug_settings_show_fired_breakpoint,NULL,"Show fired breakpoint [%s]",show_fired_breakpoint_type);


                menu_add_item_menu(array_menu_settings_debug,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_settings_debug);

                retorno_menu=menu_dibuja_menu(&settings_debug_opcion_seleccionada,&item_seleccionado,array_menu_settings_debug,"Debug Settings" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_settings_display(MENU_ITEM_PARAMETERS)
{
	menu_item *array_menu_settings_display;
	menu_item item_seleccionado;
	int retorno_menu;
	do {
		menu_add_item_menu_inicial_format(&array_menu_settings_display,MENU_OPCION_NORMAL,menu_display_rainbow,NULL,"[%c] ~~Real Video",(rainbow_enabled.v==1 ? 'X' : ' '));
		menu_add_item_menu_shortcut(array_menu_settings_display,'r');

		if (rainbow_enabled.v) {
			menu_add_item_menu_format(array_menu_settings_display,MENU_OPCION_NORMAL,menu_display_tbblue_store_scanlines,NULL,"[%c] Legacy hi-color effects",(tbblue_store_scanlines.v ? 'X' : ' '));	

			menu_add_item_menu_format(array_menu_settings_display,MENU_OPCION_NORMAL,menu_display_tbblue_store_scanlines_border,NULL,"[%c] Legacy border effects",(tbblue_store_scanlines_border.v ? 'X' : ' '));	
		}

		menu_add_item_menu(array_menu_settings_display,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_settings_display);

		retorno_menu=menu_dibuja_menu(&settings_display_opcion_seleccionada,&item_seleccionado,array_menu_settings_display,"Display Settings" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {

			//llamamos por valor de funcion
        	        if (item_seleccionado.menu_funcion!=NULL) {
                	        //printf ("actuamos por funcion\n");
	                        item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
        	        }
		}

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_hardware_redefine_keys(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_redefine_keys;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
                char buffer_texto[40];

                int i;
                for (i=0;i<MAX_TECLAS_REDEFINIDAS;i++) {
				z80_byte tecla_original=lista_teclas_redefinidas[i].tecla_original;
				z80_byte tecla_redefinida=lista_teclas_redefinidas[i].tecla_redefinida;
                        if (tecla_original) {
					sprintf(buffer_texto,"Key %c to %c",(tecla_original>=32 && tecla_original <=127 ? tecla_original : '?'),
					(tecla_redefinida>=32 && tecla_redefinida <=127 ? tecla_redefinida : '?') );

								}

                        else {
                                sprintf(buffer_texto,"Unused entry");
                        }

                        if (i==0) menu_add_item_menu_inicial_format(&array_menu_hardware_redefine_keys,MENU_OPCION_NORMAL,menu_hardware_redefine_keys_set_keys,NULL,buffer_texto);
                        else menu_add_item_menu_format(array_menu_hardware_redefine_keys,MENU_OPCION_NORMAL,menu_hardware_redefine_keys_set_keys,NULL,buffer_texto);

                }

                menu_add_item_menu(array_menu_hardware_redefine_keys,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_redefine_keys);

                retorno_menu=menu_dibuja_menu(&hardware_redefine_keys_opcion_seleccionada,&item_seleccionado,array_menu_hardware_redefine_keys,"Redefine keys" );


if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }
        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_hardware_set_f_functions(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_set_f_functions;
        menu_item item_seleccionado;
        int retorno_menu;
        do {

                char buffer_texto[40];

                int i;
                for (i=0;i<MAX_F_FUNCTIONS_KEYS;i++) {

                  enum defined_f_function_ids accion=defined_f_functions_keys_array[i];

					//tabulado todo a misma columna, agregamos un espacio con F entre 1 y 9
                  sprintf (buffer_texto,"Key F%d %s[%s]",i+1,(i+1<=9 ? " " : ""),defined_f_functions_array[accion].texto_funcion);




                        if (i==0) menu_add_item_menu_inicial_format(&array_menu_hardware_set_f_functions,MENU_OPCION_NORMAL,menu_hardware_set_f_func_action,NULL,buffer_texto);
                        else menu_add_item_menu_format(array_menu_hardware_set_f_functions,MENU_OPCION_NORMAL,menu_hardware_set_f_func_action,NULL,buffer_texto);

												menu_add_item_menu_valor_opcion(array_menu_hardware_set_f_functions,i);
												}

                menu_add_item_menu(array_menu_hardware_set_f_functions,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_set_f_functions);

                retorno_menu=menu_dibuja_menu(&hardware_set_f_functions_opcion_seleccionada,&item_seleccionado,array_menu_hardware_set_f_functions,"Set F keys" );

								if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_keyboard_settings(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_keyboard_settings;
	menu_item item_seleccionado;
	int retorno_menu;
        do {
		menu_add_item_menu_inicial(&array_menu_keyboard_settings,"",MENU_OPCION_UNASSIGNED,NULL,NULL);	

		menu_add_item_menu_format(array_menu_keyboard_settings,MENU_OPCION_NORMAL,menu_hardware_keyboard_issue,NULL,"[%c] Keyboard ~~Issue", (keyboard_issue2.v==1 ? '2' : '3'));
		menu_add_item_menu_shortcut(array_menu_keyboard_settings,'i');

                menu_add_item_menu(array_menu_keyboard_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);

        	//Redefine keys
		menu_add_item_menu_format(array_menu_keyboard_settings,MENU_OPCION_NORMAL,menu_hardware_redefine_keys,NULL,"~~Redefine keys");
		menu_add_item_menu_shortcut(array_menu_keyboard_settings,'r');


		//Set F keys functions
		menu_add_item_menu_format(array_menu_keyboard_settings,MENU_OPCION_NORMAL,menu_hardware_set_f_functions,NULL,"Set ~~F keys functions");
		menu_add_item_menu_shortcut(array_menu_keyboard_settings,'f');

                menu_add_item_menu(array_menu_keyboard_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_keyboard_settings);

                retorno_menu=menu_dibuja_menu(&keyboard_settings_opcion_seleccionada,&item_seleccionado,array_menu_keyboard_settings,"Keyboard Settings" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
			//llamamos por valor de funcion
	                if (item_seleccionado.menu_funcion!=NULL) {
        	                //printf ("actuamos por funcion\n");
                	        item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
	                }
		}

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_hardware_realjoystick(MENU_ITEM_PARAMETERS)
{
	menu_item *array_menu_hardware_realjoystick;
	menu_item item_seleccionado;
	int retorno_menu;
	do {

		menu_add_item_menu_inicial_format(&array_menu_hardware_realjoystick,MENU_OPCION_NORMAL,menu_hardware_realjoystick_event,NULL,"Joystick to ~~events");
		menu_add_item_menu_shortcut(array_menu_hardware_realjoystick,'e');

		menu_add_item_menu_format(array_menu_hardware_realjoystick,MENU_OPCION_NORMAL,menu_hardware_realjoystick_keys,NULL,"Joystick to ~~keys");
		menu_add_item_menu_shortcut(array_menu_hardware_realjoystick,'k');

		menu_add_item_menu_format(array_menu_hardware_realjoystick,MENU_OPCION_NORMAL,menu_hardware_realjoystick_test,NULL,"Joystick ~~information");
		menu_add_item_menu_shortcut(array_menu_hardware_realjoystick,'i');

		if (!realjoystick_is_linux_native() ) {
			menu_add_item_menu(array_menu_hardware_realjoystick,"",MENU_OPCION_SEPARADOR,NULL,NULL);
			menu_add_item_menu_format(array_menu_hardware_realjoystick,MENU_OPCION_NORMAL,menu_hardware_realjoystick_autocalibrate,NULL,"[%5d] Auto~~calibrate value",realjoystick_autocalibrate_value);
			menu_add_item_menu_shortcut(array_menu_hardware_realjoystick,'c');
		}

#ifdef USE_LINUXREALJOYSTICK

	menu_add_item_menu_format(array_menu_hardware_realjoystick,MENU_OPCION_NORMAL,menu_hardware_realjoystick_native,NULL,"[%c] Linux native driver",(no_native_linux_realjoystick.v ? ' ' : 'X'));
#endif		

		menu_add_item_menu(array_menu_hardware_realjoystick,"",MENU_OPCION_SEPARADOR,NULL,NULL);

		menu_add_item_menu_format(array_menu_hardware_realjoystick,MENU_OPCION_NORMAL,menu_hardware_realjoystick_set_defaults,NULL,"Set events&keys to default");

		menu_add_item_menu(array_menu_hardware_realjoystick,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_hardware_realjoystick);

		retorno_menu=menu_dibuja_menu(&hardware_realjoystick_opcion_seleccionada,&item_seleccionado,array_menu_hardware_realjoystick,"Real joystick support" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
				//llamamos por valor de funcion
				if (item_seleccionado.menu_funcion!=NULL) {
						//printf ("actuamos por funcion\n");
						item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
						
				}
		}
	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_hardware_printers(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_printers;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
                menu_add_item_menu_inicial_format(&array_menu_hardware_printers,MENU_OPCION_NORMAL,menu_hardware_printers_zxprinter_enable,NULL,"[%c] ZX Printer",(zxprinter_enabled.v==1 ? 'X' : ' ' ));

                char string_bitmapfile_shown[16];
                menu_tape_settings_trunc_name(zxprinter_bitmap_filename,string_bitmapfile_shown,16);

                menu_add_item_menu_format(array_menu_hardware_printers,MENU_OPCION_NORMAL,menu_hardware_zxprinter_bitmapfile,menu_hardware_zxprinter_cond,"Bitmap file [%s]",string_bitmapfile_shown);

		char string_ocrfile_shown[19];
		menu_tape_settings_trunc_name(zxprinter_ocr_filename,string_ocrfile_shown,19);

                menu_add_item_menu_format(array_menu_hardware_printers,MENU_OPCION_NORMAL,menu_hardware_zxprinter_ocrfile,menu_hardware_zxprinter_cond,"OCR file [%s]",string_ocrfile_shown);

                menu_add_item_menu(array_menu_hardware_printers,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_printers);

                retorno_menu=menu_dibuja_menu(&hardware_printers_opcion_seleccionada,&item_seleccionado,array_menu_hardware_printers,"Printing emulation" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }
        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_hardware_settings(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_settings;
	menu_item item_seleccionado;
	int retorno_menu;
        do {
			menu_add_item_menu_inicial(&array_menu_hardware_settings,"",MENU_OPCION_UNASSIGNED,NULL,NULL);

			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_joystick,NULL,"~~Joystick type [%s]",joystick_texto[joystick_emulation]);
			menu_add_item_menu_shortcut(array_menu_hardware_settings,'j');

	                if (joystick_autofire_frequency==0) menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_autofire,NULL,"[ ] ~~Autofire");
        	        else {
                	        menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_autofire,NULL,"[%d Hz] ~~Autofire",50/joystick_autofire_frequency);
	                }

			menu_add_item_menu_shortcut(array_menu_hardware_settings,'a');


			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_kempston_mouse,NULL,"[%c] Kempston Mou~~se emulation",(kempston_mouse_emulation.v==1 ? 'X' : ' '));
			menu_add_item_menu_shortcut(array_menu_hardware_settings,'s');

			if (kempston_mouse_emulation.v) {
			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_kempston_mouse_sensibilidad,NULL,"[%2d] Mouse Sensitivity",kempston_mouse_factor_sensibilidad);
			}

			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_datagear_dma,NULL,"[%c] Datagear DMA emulation",(datagear_dma_emulation.v==1 ? 'X' : ' '));

			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_tbblue_core_version,NULL,"[%d.%d.%d] TBBlue core version",
									tbblue_core_current_version_major,tbblue_core_current_version_minor,tbblue_core_current_version_subminor);

			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_tbblue_rtc_traps,NULL,"[%c] TBBlue RTC traps",(tbblue_use_rtc_traps ? 'X' : ' ') );

		menu_add_item_menu(array_menu_hardware_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);

		menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_keyboard_settings,NULL,"~~Keyboard settings");
		menu_add_item_menu_shortcut(array_menu_hardware_settings,'k');
	
			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_realjoystick,menu_hardware_realjoystick_cond,"~~Real joystick support");
			menu_add_item_menu_shortcut(array_menu_hardware_settings,'r');

			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_printers,NULL,"~~Printing emulation");
			menu_add_item_menu_shortcut(array_menu_hardware_settings,'p');

			menu_add_item_menu_format(array_menu_hardware_settings,MENU_OPCION_NORMAL,menu_hardware_tbblue_ram,NULL,"RA~~M size [%d KB]",tbblue_get_current_ram() );
		menu_add_item_menu_shortcut(array_menu_hardware_settings,'m');

                menu_add_item_menu(array_menu_hardware_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_hardware_settings);

                retorno_menu=menu_dibuja_menu(&hardware_settings_opcion_seleccionada,&item_seleccionado,array_menu_hardware_settings,"Hardware Settings" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
			//llamamos por valor de funcion
	                if (item_seleccionado.menu_funcion!=NULL) {
        	                //printf ("actuamos por funcion\n");
                	        item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
	                }
		}

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_ula_advanced(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_advanced;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
                menu_add_item_menu_inicial_format(&array_menu_hardware_advanced,MENU_OPCION_NORMAL,menu_hardware_advanced_hidden_top_border,NULL,"[%2d] Hidden Top Border",screen_invisible_borde_superior);

                menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,menu_hardware_advanced_visible_top_border,NULL,"[%d] Visible Top Border",screen_borde_superior);
                menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,menu_hardware_advanced_visible_bottom_border,NULL,"[%d] Visible Bottom Border",screen_total_borde_inferior);

		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,menu_hardware_advanced_borde_izquierdo,NULL,"[%d] Left Border TLength",screen_total_borde_izquierdo/2);
		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,menu_hardware_advanced_borde_derecho,NULL,"[%d] Right Border TLength",screen_total_borde_derecho/2);

		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,menu_hardware_advanced_hidden_borde_derecho,NULL,"[%d] Right Hidden B. TLength",screen_invisible_borde_derecho/2);

                menu_add_item_menu(array_menu_hardware_advanced,"",MENU_OPCION_SEPARADOR,NULL,NULL);

		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,NULL,NULL,"Info:");
		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,NULL,NULL,"Total line TLength: %d",screen_testados_linea);
		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,NULL,NULL,"Total scanlines: %d",screen_scanlines);
		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,NULL,NULL,"Total T-states: %d",screen_testados_total);
		menu_add_item_menu_format(array_menu_hardware_advanced,MENU_OPCION_NORMAL,NULL,NULL,"Total Hz: %d",screen_testados_total*50);

                menu_add_item_menu(array_menu_hardware_advanced,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_advanced);

                retorno_menu=menu_dibuja_menu(&hardware_advanced_opcion_seleccionada,&item_seleccionado,array_menu_hardware_advanced,"Advanced Timing Settings" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


int menu_cond_realvideo(void)
{
	return rainbow_enabled.v;

}


void menu_ula_settings(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_ula_settings;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
		menu_add_item_menu_inicial(&array_menu_ula_settings,"",MENU_OPCION_UNASSIGNED,NULL,NULL);

#ifdef EMULATE_CONTEND
                        menu_add_item_menu_format(array_menu_ula_settings,MENU_OPCION_NORMAL,menu_ula_late_timings,NULL,"ULA ~~timing [%s]",(ula_late_timings.v ? "Late" : "Early"));
                        menu_add_item_menu_shortcut(array_menu_ula_settings,'t');

                        menu_add_item_menu_format(array_menu_ula_settings,MENU_OPCION_NORMAL,menu_ula_contend,NULL,"[%c] ~~Contended memory", (contend_enabled.v==1 ? 'X' : ' '));
                        menu_add_item_menu_shortcut(array_menu_ula_settings,'c');
#endif

			menu_add_item_menu_format(array_menu_ula_settings,MENU_OPCION_NORMAL,menu_ula_im2_slow,NULL,"[%c] ULA IM2 ~~slow",(ula_im2_slow.v ? 'X' : ' '));
			menu_add_item_menu_shortcut(array_menu_ula_settings,'s');

                        menu_add_item_menu_format(array_menu_ula_settings,MENU_OPCION_NORMAL,menu_ula_pentagon_timing,NULL,"[%c] ~~Pentagon timing",(pentagon_timing.v ? 'X' : ' '));
                        menu_add_item_menu_shortcut(array_menu_ula_settings,'p');


                menu_add_item_menu(array_menu_ula_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);

		menu_add_item_menu_format(array_menu_ula_settings,MENU_OPCION_NORMAL,menu_ula_advanced,menu_cond_realvideo,"~~Advanced timing settings");
                menu_add_item_menu_shortcut(array_menu_ula_settings,'a');


                menu_add_item_menu(array_menu_ula_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_ula_settings);

                retorno_menu=menu_dibuja_menu(&ula_settings_opcion_seleccionada,&item_seleccionado,array_menu_ula_settings,"ULA Settings" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_settings(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_settings;
	menu_item item_seleccionado;
	int retorno_menu;

        do {
		menu_add_item_menu_inicial_format(&array_menu_settings,MENU_OPCION_NORMAL,menu_settings_audio,NULL,"A~~udio");
		menu_add_item_menu_shortcut(array_menu_settings,'u');

		menu_add_item_menu_format(array_menu_settings,MENU_OPCION_NORMAL,menu_cpu_settings,NULL,"~~CPU");
		menu_add_item_menu_shortcut(array_menu_settings,'c');

		menu_add_item_menu(array_menu_settings,"D~~ebug",MENU_OPCION_NORMAL,menu_settings_debug,NULL);
		menu_add_item_menu_shortcut(array_menu_settings,'e');

		menu_add_item_menu(array_menu_settings,"~~Display",MENU_OPCION_NORMAL,menu_settings_display,NULL);
		menu_add_item_menu_shortcut(array_menu_settings,'d');

		menu_add_item_menu_format(array_menu_settings,MENU_OPCION_NORMAL,menu_hardware_settings,NULL,"~~Hardware");
		menu_add_item_menu_shortcut(array_menu_settings,'h');

		menu_add_item_menu_format(array_menu_settings,MENU_OPCION_NORMAL,menu_ula_settings,NULL,"U~~LA");
		menu_add_item_menu_shortcut(array_menu_settings,'l');

                menu_add_item_menu(array_menu_settings,"UART ~~Bridge emulation",MENU_OPCION_NORMAL,menu_uartbridge,NULL);
		menu_add_item_menu_shortcut(array_menu_settings,'b');

                menu_add_item_menu(array_menu_settings,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_settings);

                retorno_menu=menu_dibuja_menu(&settings_opcion_seleccionada,&item_seleccionado,array_menu_settings,"Settings" );

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
	                //llamamos por valor de funcion
        	        if (item_seleccionado.menu_funcion!=NULL) {
                	        //printf ("actuamos por funcion\n");
	                        item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
				
        	        }
		}

	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_inicio_bucle_main(void)
{

	int retorno_menu;

	menu_item *array_menu_principal;
	menu_item item_seleccionado;

	int salir_menu=0;


	do {

		{		

		if (strcmp(scr_new_driver_name,"xwindows")==0 || strcmp(scr_new_driver_name,"sdl")==0 || strcmp(scr_new_driver_name,"caca")==0 || strcmp(scr_new_driver_name,"fbdev")==0 || strcmp(scr_new_driver_name,"cocoa")==0 ) f_functions=1;
		else f_functions=0;

		menu_add_item_menu_inicial(&array_menu_principal,"~~Display",MENU_OPCION_NORMAL,menu_display_settings,NULL);
		menu_add_item_menu_shortcut(array_menu_principal,'d');

		menu_add_item_menu(array_menu_principal,"D~~ebug",MENU_OPCION_NORMAL,menu_debug_settings,NULL);
		menu_add_item_menu_shortcut(array_menu_principal,'e');

		menu_add_item_menu(array_menu_principal,"Sett~~ings",MENU_OPCION_NORMAL,menu_settings,NULL);
		menu_add_item_menu_shortcut(array_menu_principal,'i');

                menu_add_item_menu(array_menu_principal,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_ESC_item(array_menu_principal);

		menu_add_item_menu_format(array_menu_principal,MENU_OPCION_NORMAL,menu_principal_salir_emulador,NULL,"%sExit emulator",(f_functions==1 ? "F10 ": "") );


		retorno_menu=menu_dibuja_menu(&menu_inicio_opcion_seleccionada,&item_seleccionado,array_menu_principal,"ZEsarUX v." EMULATOR_VERSION );

		//printf ("Opcion seleccionada: %d\n",menu_inicio_opcion_seleccionada);
		//printf ("Tipo opcion: %d\n",item_seleccionado.tipo_opcion);
		//printf ("Retorno menu: %d\n",retorno_menu);
		

		if ( (retorno_menu!=MENU_RETORNO_ESC) &&  (retorno_menu==MENU_RETORNO_F10)  ) {

			//menu_exit_emulator(0);
			menu_principal_salir_emulador(0);

		}


		else if (retorno_menu>=0) {
			//llamamos por valor de funcion
			if (item_seleccionado.menu_funcion!=NULL) {
					//printf ("actuamos por funcion\n");
		
					item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
		

					//si ha generado error, no salir
					if (if_pending_error_message) salir_todos_menus=0;
			}

			
		}



		if (retorno_menu==MENU_RETORNO_ESC || (item_seleccionado.tipo_opcion & MENU_OPCION_ESC) == MENU_OPCION_ESC) {
			//printf ("opcion ESC o pulsado ESC\n");
			salir_menu=1;
		}

		}

	} while (!salir_menu && !salir_todos_menus);

	//printf ("Fin menu_inicio_bucle_main\n");

}


void menu_inicio_bucle(void)
{

	//printf ("inicio de menu_inicio_bucle\n");

	//Si se ha pulsado el logo Z antes de abrir menu principal
	//Si no hiciera esto, se abriria menu, y luego se reabriria al cerrarlo, 
	//dado que tenemos menu_pressed_open_menu_while_in_menu.v activado
	if (menu_pressed_open_menu_while_in_menu.v) {
		//printf ("Forgetting Z logo click action done before executing main menu\n");
					menu_pressed_open_menu_while_in_menu.v=0;
					salir_todos_menus=0;

	}


	//Si reabrimos menu despues de conmutar entre ventanas en background
	int reopen_menu;

	do {
		reopen_menu=0;
		menu_inicio_bucle_main();

		//Se reabre el menu tambien si pulsada tecla F5 en cualquiera de los menus
		if (menu_pressed_open_menu_while_in_menu.v) {
			menu_pressed_open_menu_while_in_menu.v=0;
			reopen_menu=1;
			//printf ("Reabrimos menu\n");
		}



		//Ver si se habia pulsado en una ventana que habia en background
		//Aqui nos quedamos siempre que se pulse en otra ventana, digamos que esto es como el gestor de ventanas "sencillo"
		//es un tanto mágico pero también muy simple
		while (clicked_on_background_windows) {

			//Por si hemos llegado hasta aqui cerrando todos los menus al haber pulsado en otra ventana y la actual no permite background
			salir_todos_menus=0;

			
			clicked_on_background_windows=0;
			debug_printf(VERBOSE_DEBUG,"Clicked on background window, notified at the end of menus");	

			if (which_window_clicked_on_background!=NULL) {
				//printf ("Ventana: %s\n",which_window_clicked_on_background->window_title);
				//printf ("Geometry name ventana: %s\n",which_window_clicked_on_background->geometry_name);

				char *geometry_name;

				geometry_name=which_window_clicked_on_background->geometry_name;

				if (geometry_name[0]!=0) {
				
					int indice=zxvision_find_known_window(geometry_name);

					if (indice<0) {
							//debug_printf (VERBOSE_ERR,"Unknown window to restore: %s",geometry_name);
					}

					else {
					//Lanzar funcion que la crea

						//Guardar funcion de texto overlay activo, para menus como el de visual memory por ejemplo, para desactivar  temporalmente
						void (*previous_function)(void);

						previous_function=menu_overlay_function;

						int antes_menu_overlay_activo=menu_overlay_activo;														
				
						debug_printf (VERBOSE_DEBUG,"Starting window %s",geometry_name);
						zxvision_known_window_names_array[indice].start(0);


						//Restauramos funcion anterior de overlay
						set_menu_overlay_function(previous_function);		

						menu_overlay_activo=antes_menu_overlay_activo;		

							
						//Y reabriremos el menu cuando dejemos de conmutar entre ventanas
						reopen_menu=1;


						//Se reabre el menu tambien si pulsada tecla F5 en cualquiera de los menus
						if (menu_pressed_open_menu_while_in_menu.v) {
							menu_pressed_open_menu_while_in_menu.v=0;
						}
			
					}
				}
				
			}
		}


		which_window_clicked_on_background=NULL;


		//Si hay que reabrirlo, resetear estado de salida de todos
		if (reopen_menu) salir_todos_menus=0;	



	} while (reopen_menu);		

	        

}



void menu_inicio_pre_retorno(void)
{
	menu_inicio_pre_retorno_reset_flags();

    reset_menu_overlay_function();
    menu_set_menu_abierto(0);


    //Para refrescar border en caso de tsconf por ejemplo, en que el menu sobreescribe el border
    //modificado_border.v=1;

    timer_reset();

	//Y refrescar footer. Hacer esto para que redibuje en pantalla y no en layer de mezcla de menu
	menu_init_footer();  
/*
menu_init_footer hace falta pues el layer de menu se borra y se queda negro en las zonas izquierda y derecha del footer
*/

	redraw_footer();


    //O si siempre se fuerza
    if (menu_allow_background_windows && menu_multitarea) {
        if (always_force_overlay_visible_when_menu_closed) overlay_visible_when_menu_closed=1;
    }
    
	if (overlay_visible_when_menu_closed) {
		menu_overlay_activo=1;

		//Y redibujamos las ventanas, para que se vean los titulos sobretodo (pues los overlay en background no redibujan los titulos)
		//decir que ventana principal no esta activa, para indicar que están todas en background
		ventana_tipo_activa=0;

		generic_footertext_print_operating("BKWIND");

		zxvision_redraw_all_windows();
	}

}
