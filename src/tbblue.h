/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TBBLUE_H
#define TBBLUE_H

#include "cpu.h"


#define TBBLUE_CORE_DEFAULT_VERSION_MAJOR     3
#define TBBLUE_CORE_DEFAULT_VERSION_MINOR     1
#define TBBLUE_CORE_DEFAULT_VERSION_SUBMINOR  10

//borde izquierdo + pantalla + borde derecho, multiplicado por 2
#define TBBLUE_LAYERS_PIXEL_WIDTH ((48+256+48)*2)

//De momento esto solo se usa en carga/grabacion snapshots zsf
#define TBBLUE_TOTAL_RAM_SIZE 2048

#define TBBLUE_FPGA_ROM_SIZE 8

#define TBBLUE_FPGA_NOMEM_SIZE 8

#define TBBLUE_PAGE_8K_SHIFT 13
#define TBBLUE_PAGE_16K_SHIFT 14

#define TBBLUE_PAGE_8K_MASK 0x1fff

#define TBBLUE_MAX_SRAM_8KB_BLOCKS 224
/*
   ------------------------------------------------------------
   -- MEMORY DECODING -----------------------------------------
   ------------------------------------------------------------

   -- 0x000000 - 0x00FFFF (64K)  => ZX Spectrum ROM         A20:A16 = 00000
   -- 0x010000 - 0x011FFF ( 8K)  => divMMC ROM              A20:A16 = 00001,000
   -- 0x012000 - 0x013FFF ( 8K)  => unused                  A20:A16 = 00001,001
   -- 0x014000 - 0x017FFF (16K)  => Multiface ROM,RAM       A20:A16 = 00001,01
   -- 0x018000 - 0x01BFFF (16K)  => Alt ROM0 128k           A20:A16 = 00001,10
   -- 0x01c000 - 0x01FFFF (16K)  => Alt ROM1 48k            A20:A16 = 00001,11
   -- 0x020000 - 0x03FFFF (128K) => divMMC RAM              A20:A16 = 00010
   -- 0x040000 - 0x05FFFF (128K) => ZX Spectrum RAM         A20:A16 = 00100
   -- 0x060000 - 0x07FFFF (128K) => Extra RAM
   -- 0x080000 - 0x0FFFFF (512K) => 1st Extra IC RAM (if present)
   -- 0x100000 - 0x17FFFF (512K) => 2nd Extra IC RAM (if present)
   -- 0x180000 - 0x1FFFFF (512K) => 3rd Extra IC RAM (if present)
*/

#define TBBLUE_SRAM_ADDR_ZX_ROM         0x000000
#define TBBLUE_SRAM_ADDR_DIVMMC_ROM     0x010000
#define TBBLUE_SRAM_ADDR_UNUSED         0x012000
#define TBBLUE_SRAM_ADDR_MULTIFACE_ROM  0x014000
#define TBBLUE_SRAM_ADDR_MULTIFACE_RAM  0x016000
#define TBBLUE_SRAM_ADDR_ALT_ROM        0x018000
#define TBBLUE_SRAM_ADDR_DIVMMC_RAM     0x020000
#define TBBLUE_SRAM_ADDR_ZX_RAM         0x040000

// 2048K SRAM + 8K BOOTROM + 8K "out of range"
#define TBBLUE_TOTAL_MEMORY_USED (TBBLUE_TOTAL_RAM_SIZE+TBBLUE_FPGA_ROM_SIZE+TBBLUE_FPGA_NOMEM_SIZE)

#define TBBLUE_MAX_RAM_PAGE_ID (31 + (tbblue_extra_512kb_blocks << 6))

#define TBBLUE_REGISTER_PORT 0x243b
#define TBBLUE_VALUE_PORT 0x253b
#define TBBLUE_LAYER2_PORT 0x123B

#define TBBLUE_SPRITE_INDEX_PORT 0x303B
#define TBBLUE_SPRITE_PALETTE_PORT 0x53
#define TBBLUE_SPRITE_PATTERN_PORT 0x5b
#define TBBLUE_SPRITE_SPRITE_PORT 0x57


#define TBBLUE_UART_TX_PORT 0x133b
//Tambien byte de estado en lectura

#define TBBLUE_UART_RX_PORT 0x143b

#define TBBLUE_UART_STATUS_DATA_READY 1
#define TBBLUE_UART_STATUS_BUSY 2
#define TBBLUE_UART_STATUS_FIFO_FULL 4



#define TBBLUE_SECOND_KEMPSTON_PORT 0x37




#define TBBLUE_COPPER_MEMORY 2048

#define MAX_SPRITES_PER_LINE 100

#define TBBLUE_SPRITE_BORDER 32
#define TBBLUE_TILES_BORDER 32

//Lineas de border superior e inferior ocupados por layer 2 en resoluciones 1 y 2
#define TBBLUE_LAYER2_12_BORDER 32

#define MAX_X_SPRITE_LINE (TBBLUE_SPRITE_BORDER+256+TBBLUE_SPRITE_BORDER)



#define TBBLUE_MAX_PATTERNS 64
#define TBBLUE_SPRITE_8BPP_SIZE 256
#define TBBLUE_SPRITE_4BPP_SIZE 128

#define TBBLUE_SPRITE_ARRAY_PATTERN_SIZE (TBBLUE_MAX_PATTERNS*TBBLUE_SPRITE_8BPP_SIZE)

#define TBBLUE_MAX_SPRITES 128
//#define TBBLUE_TRANSPARENT_COLOR_INDEX 0xE3
//#define TBBLUE_TRANSPARENT_COLOR 0x1C6
#define TBBLUE_SPRITE_ATTRIBUTE_SIZE 5

#define TBBLUE_DEFAULT_TRANSPARENT 0xE3
#define TBBLUE_TRANSPARENT_REGISTER (tbblue_registers[20])
#define TBBLUE_TRANSPARENT_REGISTER_9 (TBBLUE_TRANSPARENT_REGISTER<<1)

#define TBBLUE_SPRITE_HEIGHT 16
#define TBBLUE_SPRITE_WIDTH 16

#define TBBLUE_TILE_HEIGHT 8
#define TBBLUE_TILE_WIDTH 8

#define TBBLUE_MACHINE_TYPE ((tbblue_registers[3])&3)

#define TBBLUE_MACHINE_48K (TBBLUE_MACHINE_TYPE==1)
#define TBBLUE_MACHINE_128_P2 (TBBLUE_MACHINE_TYPE==2)
#define TBBLUE_MACHINE_P2A (TBBLUE_MACHINE_TYPE==3)

#define TBBLUE_CLIP_WINDOW_LAYER2   0
#define TBBLUE_CLIP_WINDOW_SPRITES  1
#define TBBLUE_CLIP_WINDOW_ULA      2
#define TBBLUE_CLIP_WINDOW_TILEMAP  3


#define TBBLUE_RCCH_COPPER_STOP             0x00
#define TBBLUE_RCCH_COPPER_RUN_LOOP_RESET   0x40
#define TBBLUE_RCCH_COPPER_RUN_LOOP         0x80
#define TBBLUE_RCCH_COPPER_RUN_VBI          0xc0


//Hacer que estos valores de border sean multiples de 8
#define TBBLUE_LEFT_BORDER_NO_ZOOM (48*2)
#define TBBLUE_TOP_BORDER_NO_ZOOM (56*2)
#define TBBLUE_BOTTOM_BORDER_NO_ZOOM (56*2)

#define TBBLUE_LEFT_BORDER TBBLUE_LEFT_BORDER_NO_ZOOM*zoom_x
#define TBBLUE_TOP_BORDER TBBLUE_TOP_BORDER_NO_ZOOM*zoom_y
#define TBBLUE_BOTTOM_BORDER TBBLUE_BOTTOM_BORDER_NO_ZOOM*zoom_y

#define TBBLUE_DISPLAY_WIDTH 512
#define TBBLUE_DISPLAY_HEIGHT 384



//Guarda scanline actual y el pattern (los indices a colores) sobre la paleta activa de sprites
//z80_byte sprite_line[MAX_X_SPRITE_LINE];

#define TBBLUE_SPRITE_TRANS_FICT 65535


struct s_tbblue_machine_id_definition {
    z80_byte id;
    char nombre[32];
};


struct s_tbblue_priorities_names {
	char layers[3][20];
};


/* tbblue.c */
extern z80_byte tbblue_core_current_version_major;
extern z80_byte tbblue_core_current_version_minor;
extern z80_byte tbblue_core_current_version_subminor;
extern int tbblue_use_rtc_traps;
extern z80_byte tbblue_registers[];
extern z80_byte tbblue_last_register;
extern z80_byte tbblue_machine_id;
extern struct s_tbblue_machine_id_definition tbblue_machine_id_list[];
extern void tbblue_set_emulator_setting_multiface(void);
extern void tbblue_set_emulator_setting_divmmc(void);
extern void tbblue_set_emulator_setting_turbo(void);
extern void tbblue_set_emulator_setting_reg_8(void);
extern void tbblue_reset_common(void);
extern void tbblue_soft_reset(void);
extern void tbblue_hard_reset(void);
extern void tbblue_set_timing_128k(void);
extern void tbblue_set_timing_48k(void);
extern void tbblue_change_timing(int timing);
extern void tbblue_set_emulator_setting_timing(z80_byte value);
extern void tbblue_set_register_port(z80_byte value);
extern z80_byte tbblue_get_register_port(void);
extern void tbblue_splash_monitor_mode(void);
extern void tbblue_sync_display1_reg_to_others(z80_byte value);
extern void tbblue_set_joystick_1_mode(void);
extern void tbblue_set_value_port(z80_byte value);
extern void tbblue_set_value_port_position(z80_byte index, z80_byte value);
extern z80_byte tbblue_get_value_port(void);
extern z80_byte tbblue_get_value_port_register(z80_byte index);
extern z80_byte tbblue_uartbridge_readdata(void);
extern void tbblue_uartbridge_writedata(z80_byte value);
extern z80_byte tbblue_uartbridge_readstatus(void);


/* tbblue_memory.c */
extern z80_byte *tbblue_ram_memory_pages[TBBLUE_MAX_SRAM_8KB_BLOCKS];
extern z80_byte tbblue_extra_512kb_blocks;
extern void tbblue_set_ram_blocks(int memoria_kb);
extern z80_byte tbblue_return_max_extra_blocks(void);
extern int tbblue_get_current_ram(void);
extern z80_byte *tbblue_fpga_rom;
extern z80_byte *tbblue_no_mem;
extern z80_byte *tbblue_memory_r[8];
extern z80_byte *tbblue_memory_w[8];
extern z80_bit tbblue_bootrom;
extern z80_byte tbblue_active_rom;
extern z80_byte tbblue_rom3;
extern z80_byte tbblue_alt_128;
extern z80_byte tbblue_rom3_visible[2];
extern void tbblue_init_memory_tables(void);
extern void tbblue_select_active_roms(void);
extern z80_byte *tbblue_get_rom_write_mapping(z80_byte mmu_id);
extern z80_byte *tbblue_get_rom_read_mapping(z80_byte mmu_id);
extern void tbblue_set_paged_mmu_rom_write(z80_byte mmu_id);
extern void tbblue_set_paged_mmu_rom_read(z80_byte mmu_id);
extern void tbblue_set_paged_mmu_write(z80_byte mmu_id, z80_byte ram_page_id);
extern void tbblue_set_paged_mmu_read(z80_byte mmu_id, z80_byte ram_page_id);
extern void tbblue_set_paged_mmu_only(z80_byte mmu_id);
extern z80_byte tbblue_get_layer2_mapping(z80_byte mmu_id);
extern void tbblue_set_paged_mmu_or_layer2(z80_byte mmu_id);
extern void tbblue_touch_mmu01(void);
extern void tbblue_touch_mmu2(void);
extern void tbblue_touch_mmu3(void);
extern void tbblue_touch_mmu4(void);
extern void tbblue_touch_mmu5(void);
extern void tbblue_touch_mmu6(void);
extern void tbblue_touch_mmu7(void);
extern void tbblue_touch_mmu012345(void);
extern void tbblue_set_mmu0(z80_byte value);
extern void tbblue_set_mmu1(z80_byte value);
extern void tbblue_set_mmu2(z80_byte value);
extern void tbblue_set_mmu3(z80_byte value);
extern void tbblue_set_mmu4(z80_byte value);
extern void tbblue_set_mmu5(z80_byte value);
extern void tbblue_set_mmu6(z80_byte value);
extern void tbblue_set_mmu7(z80_byte value);
extern void tbblue_set_register_8e(void);
extern z80_byte tbblue_get_register_8e(void);
extern void tbblue_update_memory_ports(z80_byte new_1ffd, z80_byte new_7ffd, z80_byte update_mmu67/*, z80_byte new_dffd*/);
extern void tbblue_out_port_8189(z80_byte value);
extern void tbblue_out_port_32765(z80_byte value);
extern void poke_byte_no_time_tbblue(z80_int dir,z80_byte valor);
extern void poke_byte_tbblue(z80_int dir,z80_byte valor);
extern z80_byte peek_byte_no_time_tbblue(z80_int dir);
extern z80_byte peek_byte_tbblue(z80_int dir);


/* tbblue_sprites.c */
extern z80_byte tbsprite_new_patterns[TBBLUE_SPRITE_ARRAY_PATTERN_SIZE];
extern z80_byte tbsprite_sprites[TBBLUE_MAX_SPRITES][TBBLUE_SPRITE_ATTRIBUTE_SIZE];
extern z80_byte tbsprite_index_pattern,tbsprite_index_pattern_subindex;
extern z80_byte tbsprite_index_sprite,tbsprite_index_sprite_subindex;
extern z80_byte tbsprite_nr_index_sprite;
extern z80_byte tbblue_port_303b;
extern int tbsprite_pattern_get_offset_index_4bpp(z80_byte sprite,int offset_1_pattern,z80_byte index_in_sprite);
extern z80_byte tbsprite_pattern_get_value_index_4bpp(z80_byte sprite,int offset_1_pattern,z80_byte index_in_sprite);
extern int tbsprite_pattern_get_offset_index_8bpp(z80_byte sprite,z80_byte index_in_sprite);
extern z80_byte tbsprite_pattern_get_value_index_8bpp(z80_byte sprite,z80_byte index_in_sprite);
extern void tbsprite_pattern_put_value_index_8bpp(z80_byte sprite,z80_byte index_in_sprite,z80_byte value);
extern int tbsprite_is_lockstep();
extern void tbsprite_increment_index_303b();
extern void tbblue_reset_sprites(void);
extern void tbblue_out_port_sprite_index(z80_byte value);
extern void tbblue_out_sprite_pattern(z80_byte value);
extern void tbblue_out_sprite_sprite(z80_byte value);
extern z80_byte tbblue_get_port_sprite_index(void);


/* tbblue_palettes.c */
extern z80_int tbblue_palette_ula_first[256];
extern z80_int tbblue_palette_ula_second[256];
extern z80_int tbblue_palette_layer2_first[256];
extern z80_int tbblue_palette_layer2_second[256];
extern z80_int tbblue_palette_sprite_first[256];
extern z80_int tbblue_palette_sprite_second[256];
extern z80_int tbblue_palette_tilemap_first[256];
extern z80_int tbblue_palette_tilemap_second[256];
extern z80_int *tbblue_get_palette_rw(void);
extern z80_int tbblue_get_value_palette_rw(z80_byte index);
extern void tbblue_set_value_palette_rw(z80_byte index,z80_int valor);
extern z80_int tbblue_get_palette_active_sprite(z80_byte index);
extern z80_int tbblue_get_palette_active_layer2(z80_byte index);
extern z80_int tbblue_get_palette_active_ula(z80_byte index);
extern z80_int tbblue_get_palette_active_tilemap(z80_byte index);
extern z80_int tbblue_get_9bit_colour(z80_byte valor);
extern void tbblue_reset_palettes(void);
extern int tbblue_write_palette_state;
extern z80_byte tbblue_write_palette_latched_8;
extern void tbblue_reset_palette_write_state(void);
extern void tbblue_increment_palette_index(void);
extern void tbblue_write_palette_value_high8(z80_byte valor);
extern void tbblue_write_palette_value_low1(z80_byte valor);
extern void tbblue_write_palette_value_high8_low1(z80_byte valor);
extern void tbblue_get_string_palette_format(char *texto);
extern void tbblue_splash_palette_format(void);


/* tbblue_layer2.c */
extern z80_byte tbblue_port_123b;
extern z80_byte tbblue_port_123b_layer2_offset;
extern int tbblue_initial_123b_port;
extern int tbblue_write_on_layer2(void);
extern int tbblue_is_active_layer2(void);
extern int tbblue_get_offset_start_layer2_reg(z80_byte register_value);
extern int tbblue_get_offset_start_layer2(void);
extern z80_byte tbblue_get_port_layer2_value(void);
extern void tbblue_out_port_layer2_value(z80_byte value);


/* tbblue_tilemap.c */
extern int tbblue_get_offset_start_tilemap(void);
extern int tbblue_get_offset_start_tiledef(void);
extern int tbblue_get_tilemap_width(void);


/* tbblue_clip.c */
extern z80_byte clip_windows[4][4];
extern void tbblue_inc_clip_window_index(const z80_byte index_mask);
extern z80_byte tbblue_get_clip_window_layer2_index(void);
extern z80_byte tbblue_get_clip_window_sprites_index(void);
extern z80_byte tbblue_get_clip_window_ula_index(void);
extern z80_byte tbblue_get_clip_window_tilemap_index(void);
extern void tbblue_inc_clip_window_layer2_index(void);
extern void tbblue_reset_clip_window_layer2_index(void);
extern void tbblue_inc_clip_window_sprites_index(void);
extern void tbblue_reset_clip_window_sprites_index(void);
extern void tbblue_inc_clip_window_ula_index(void);
extern void tbblue_reset_clip_window_ula_index(void);
extern void tbblue_inc_clip_window_tilemap_index(void);
extern void tbblue_reset_clip_window_tilemap_index(void);


/* tbblue_layers.c */
extern z80_int tbblue_layer_ula[TBBLUE_LAYERS_PIXEL_WIDTH];
extern z80_int tbblue_layer_layer2[TBBLUE_LAYERS_PIXEL_WIDTH];
extern z80_int tbblue_layer_sprites[TBBLUE_LAYERS_PIXEL_WIDTH];
extern z80_bit tbblue_force_disable_layer_ula;
extern z80_bit tbblue_force_disable_layer_tilemap;
extern z80_bit tbblue_force_disable_layer_sprites;
extern z80_bit tbblue_force_disable_layer_layer_two;
extern z80_int *p_layer_first;
extern z80_int *p_layer_second;
extern z80_int *p_layer_third;
extern int (*tbblue_fn_pixel_layer_transp_first)(z80_int color);
extern int (*tbblue_fn_pixel_layer_transp_second)(z80_int color);
extern int (*tbblue_fn_pixel_layer_transp_third)(z80_int color);
extern struct s_tbblue_priorities_names tbblue_priorities_names[];
extern char *tbblue_get_string_layer_prio(int layer,z80_byte prio);
extern z80_byte tbblue_get_layers_priorities(void);
extern void tbblue_set_layer_priorities(void);
extern int tbblue_if_ula_is_enabled(void);


/* tbblue_copper.c */
extern z80_byte tbblue_copper_memory[TBBLUE_COPPER_MEMORY];
extern z80_int tbblue_copper_pc;
extern z80_int tbblue_copper_get_write_position(void);
extern void tbblue_copper_set_write_position(z80_int posicion);
extern void tbblue_copper_increment_write_position(void);
extern void tbblue_copper_write_data(z80_byte value);
extern void tbblue_copper_write_data_16b(z80_byte value1, z80_byte value2);
extern z80_byte tbblue_copper_get_byte(z80_int posicion);
extern z80_int tbblue_copper_get_pc(void);
extern z80_byte tbblue_copper_get_byte_pc(void);
extern void tbblue_copper_get_wait_opcode_parameters(z80_int *line, z80_int *horiz);
extern void tbblue_copper_reset_pc(void);
extern void tbblue_copper_set_stop(void);
extern void tbblue_copper_next_opcode(void);
extern void tbblue_copper_run_opcodes(void);
extern z80_byte tbblue_copper_get_control_bits(void);
extern int tbblue_copper_wait_cond_fired(void);
extern void tbblue_copper_handle_next_opcode(void);
extern void tbblue_copper_handle_vsync(void);
extern void tbblue_copper_write_control_hi_byte(z80_byte value);


/* tbblue_video.c */
extern int tbblue_already_autoenabled_rainbow;
extern int tbblue_get_raster_line(void);
extern z80_byte *get_lores_pointer(int y);
extern int tbblue_get_current_raster_position(void);
extern int tbblue_get_current_raster_horiz_position(void);
extern int tbblue_si_sprite_transp_ficticio(z80_int color);
extern int tbblue_si_transparent(z80_int color);
extern void tbsprite_put_color_line(int x,z80_byte color,int rangoxmin,int rangoxmax);
extern z80_byte tbsprite_do_overlay_get_pattern_xy_8bpp(z80_byte index_pattern,z80_byte sx,z80_byte sy);
extern z80_byte tbsprite_do_overlay_get_pattern_xy_4bpp(z80_byte index_pattern,int offset_1_pattern,z80_byte sx,z80_byte sy);
extern z80_int tbsprite_return_color_index(z80_byte index);
extern int tbblue_if_sprites_enabled(void);
extern int tbblue_if_tilemap_enabled(void);
extern void tbsprite_do_overlay(void);
extern z80_int tbblue_get_border_color(z80_int color);
extern void get_pixel_color_tbblue(z80_byte attribute,z80_int *tinta_orig, z80_int *papel_orig);
extern z80_int tbblue_tile_return_color_index(z80_byte index);
extern void tbblue_do_tile_putpixel(z80_byte pixel_color,z80_byte transparent_colour,z80_byte tpal,z80_int *puntero_a_layer,int ula_over_tilemap);
extern z80_byte tbblue_get_pixel_tile_xy_4bpp(int x,int y,z80_byte *puntero_this_tiledef);
extern int tbblue_tiles_are_monocrome(void);
extern z80_byte tbblue_get_pixel_tile_xy_monocromo(int x,int y,z80_byte *puntero_this_tiledef);
extern z80_byte tbblue_get_pixel_tile_xy(int x,int y,z80_byte *puntero_this_tiledef);
extern void tbblue_do_tile_overlay(int scanline);
extern void tbblue_fast_render_ula_layer(z80_int *puntero_final_rainbow,int estamos_borde_supinf,
	int final_borde_izquierdo,int inicio_borde_derecho,int ancho_rainbow);
extern void tbblue_render_layers_rainbow(int capalayer2,int capasprites);
extern char *tbblue_layer2_video_modes_names[];
extern char *tbblue_get_layer2_mode_name(void);
extern void tbblue_do_layer2_overlay(int linea_render);
extern void tbblue_reveal_layer_draw(z80_int *layer);
extern z80_bit tbblue_reveal_layer_ula;
extern z80_bit tbblue_reveal_layer_layer2;
extern z80_bit tbblue_reveal_layer_sprites;
extern void tbblue_do_ula_standard_overlay();
extern void tbblue_do_ula_lores_overlay();
extern void screen_store_scanline_rainbow_solo_display_tbblue(void);
extern void screen_tbblue_refresca_pantalla_comun_tbblue(int x,int y,unsigned int color);
extern void screen_tbblue_refresca_pantalla_comun(void);
extern void screen_tbblue_refresca_no_rainbow_border(void);
extern void screen_tbblue_refresca_rainbow(void);
extern void screen_tbblue_refresca_no_rainbow(void);

#endif
