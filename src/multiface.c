/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#if defined(__APPLE__)
        #include <sys/syslimits.h>
#endif


#include "multiface.h"
#include "cpu.h"
#include "debug.h"
#include "utils.h"
#include "operaciones.h"
#include "tbblue.h"


z80_bit multiface_enabled={0};

z80_bit multiface_switched_on={0};

//Puerto para  mapeo y desmapeo de memoria multiface segun el tipo de interfaz seleccionado
z80_byte multiface_current_port_in;
z80_byte multiface_current_port_out;

int multiface_lockout=0;

char *multiface_types_string[MULTIFACE_TOTAL_TYPES]={"One","128","Three"};

z80_byte multiface_type=0;

void multiface_enable(void)
{
	multiface_enabled.v=1;
}

void multiface_disable(void)
{
	multiface_enabled.v=0;
}


void multiface_map_memory(void)
{

	multiface_switched_on.v=1;
        tbblue_touch_mmu01();
	//debug_printf(VERBOSE_DEBUG,"Mapping Multiface RAM and ROM with PC=%04XH",reg_pc);

}

void multiface_unmap_memory(void)
{

        multiface_switched_on.v=0;
        tbblue_touch_mmu01();
	//debug_printf(VERBOSE_DEBUG,"Unmapping Multiface RAM and ROM with PC=%04XH",reg_pc);

}
