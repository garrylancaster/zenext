/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"

//Sprites

//Paleta de 256 colores formato RGB9 RRRGGGBBB
//Valores son de 9 bits por tanto lo definimos con z80_int que es de 16 bits
//z80_int tbsprite_palette[256];

//64 patterns de Sprites
/*
In the palette each byte represents the colors in the RRRGGGBB format, and the pink color, defined by standard 1110011, is reserved for the transparent color.
*/
//z80_byte tbsprite_patterns[TBBLUE_MAX_PATTERNS][TBBLUE_SPRITE_SIZE];
z80_byte tbsprite_new_patterns[TBBLUE_SPRITE_ARRAY_PATTERN_SIZE];


/*
[0] 1st: X position (bits 7-0).
[1] 2nd: Y position (0-255).
[2] 3rd: bits 7-4 is palette offset, bit 3 is X MSB, bit 2 is X mirror, bit 1 is Y mirror and bit 0 is visible flag.
[3] 4th: bits 7-6 is reserved, bits 5-0 is Name (pattern index, 0-63).
[4] 5th: if 4bpp pattern, anchor, relative, etc
*/
z80_byte tbsprite_sprites[TBBLUE_MAX_SPRITES][TBBLUE_SPRITE_ATTRIBUTE_SIZE];

//Indices al indicar paleta, pattern, sprites. Subindex indica dentro de cada pattern o sprite a que posicion (0..3 en sprites o 0..255 en pattern ) apunta
//z80_byte tbsprite_index_palette;
z80_byte tbsprite_index_pattern,tbsprite_index_pattern_subindex;
z80_byte tbsprite_index_sprite,tbsprite_index_sprite_subindex;
z80_byte tbsprite_nr_index_sprite;

/*
Port 0x303B, if read, returns some information:

Bits 7-2: Reserved, must be 0.
Bit 1: max sprites per line flag.
Bit 0: Collision flag.
Port 0x303B, if written, defines the sprite slot to be configured by ports 0x55 and 0x57, and also initializes the address of the palette.

*/

z80_byte tbblue_port_303b;


//XXX indica desplazamiento de 1 pattern de 4bits
int tbsprite_pattern_get_offset_index_4bpp(z80_byte sprite,int offset_1_pattern,z80_byte index_in_sprite)
{
	return offset_1_pattern*TBBLUE_SPRITE_4BPP_SIZE + sprite*TBBLUE_SPRITE_8BPP_SIZE+index_in_sprite;
}

z80_byte tbsprite_pattern_get_value_index_4bpp(z80_byte sprite,int offset_1_pattern,z80_byte index_in_sprite)
{
	return tbsprite_new_patterns[tbsprite_pattern_get_offset_index_4bpp(sprite,offset_1_pattern,index_in_sprite)];
}



int tbsprite_pattern_get_offset_index_8bpp(z80_byte sprite,z80_byte index_in_sprite)
{
	return sprite*TBBLUE_SPRITE_8BPP_SIZE+index_in_sprite;
}

z80_byte tbsprite_pattern_get_value_index_8bpp(z80_byte sprite,z80_byte index_in_sprite)
{
	return tbsprite_new_patterns[tbsprite_pattern_get_offset_index_8bpp(sprite,index_in_sprite)];
}

void tbsprite_pattern_put_value_index_8bpp(z80_byte sprite,z80_byte index_in_sprite,z80_byte value)
{
	tbsprite_new_patterns[tbsprite_pattern_get_offset_index_8bpp(sprite,index_in_sprite)]=value;
}




int tbsprite_is_lockstep()
{
	return (tbblue_registers[9]&0x10);
}


void tbsprite_increment_index_303b() {	
	// increment the "port" index
	tbsprite_index_sprite_subindex=0;
	tbsprite_index_sprite++;
	tbsprite_index_sprite %= TBBLUE_MAX_SPRITES;
}

void tbblue_reset_sprites(void)
{

	int i;

	

	//Resetear patterns todos a transparente
	for (i=0;i<TBBLUE_MAX_PATTERNS;i++) {
		int j;
		for (j=0;j<256;j++) {
			//tbsprite_patterns[i][j]=TBBLUE_DEFAULT_TRANSPARENT;
			tbsprite_pattern_put_value_index_8bpp(i,j,TBBLUE_DEFAULT_TRANSPARENT);
		}
	}

	//Poner toda info de sprites a 0. Seria quiza suficiente con poner bit de visible a 0
	for (i=0;i<TBBLUE_MAX_SPRITES;i++) {
		int j;
		for (j=0;j<TBBLUE_SPRITE_ATTRIBUTE_SIZE;j++) {
			tbsprite_sprites[i][j]=0;
		}
	}


	//tbsprite_index_palette=tbsprite_index_pattern=tbsprite_index_sprite=0;
	tbsprite_index_pattern=tbsprite_index_pattern_subindex=0;
	tbsprite_index_sprite=tbsprite_index_sprite_subindex=0;
	tbsprite_nr_index_sprite=0;	

	tbblue_port_303b=0;

	tbblue_registers[22]=0;
	tbblue_registers[23]=0;


}


void tbblue_out_port_sprite_index(z80_byte value)
{
	/*
Port 0x303B (W)
X S S S S S S S
N6 X N N N N N N
A write to this port has two effects.
One is it selects one of 128 sprites for writing sprite attributes via port 0x57.
The other is it selects one of 128 4-bit patterns in pattern memory for writing 
sprite patterns via port 0x5B. The N6 bit shown is the least significant in the 7-bit 
pattern number and should always be zero when selecting one of 64 8-bit patterns indicated by N.	
	*/
	//printf ("Out tbblue_out_port_sprite_index %02XH\n",value);
	tbsprite_index_pattern=value % TBBLUE_MAX_PATTERNS;

	//De esta manera permitimos escrituras de 4bpp patterns en "medio" de un pattern de 8bpp (256 / 2 = 128 = 0x80)
	//casualmente (o no) nuestro incremento es de 128 y ese bit N6 está en la posicion de bit 7 = 128
	tbsprite_index_pattern_subindex=value & 0x80;

	tbsprite_index_sprite=value % TBBLUE_MAX_SPRITES;
	tbsprite_index_sprite_subindex=0;
}


void tbblue_out_sprite_pattern(z80_byte value)
{

/*
Once a pattern number is selected via port 0x303B, the 256-byte or 128-byte pattern can be written to this port. 
The internal pattern pointer auto-increments after each write so as many sequential patterns as desired can be written. 
The internal pattern pointer will roll over from pattern 127 to pattern 0 (4-bit patterns) or from pattern 63 to pattern 0 
(8-bit patterns) automatically.
->esto del rollover es automático. Siempre resetea a 64. La diferencia es que podemos escribir “en medio” de un pattern 
de 256 (8 bit) cuando N6 es 1 (lo que indicaría un pattern de 128 - 4 bit). 
Así también, si N6 es 0 puede ser pattern de 4 bits, aunque da igual. N6 lo tratamos siempre como “sumar 1/2 pattern”
*/


	tbsprite_pattern_put_value_index_8bpp(tbsprite_index_pattern,tbsprite_index_pattern_subindex,value);
	//tbsprite_patterns[tbsprite_index_pattern][tbsprite_index_pattern_subindex]=value;



	if (tbsprite_index_pattern_subindex==255) {
		tbsprite_index_pattern_subindex=0;
		tbsprite_index_pattern++;
		if (tbsprite_index_pattern>=TBBLUE_MAX_PATTERNS) tbsprite_index_pattern=0;
	}
	else tbsprite_index_pattern_subindex++;

}


void tbblue_out_sprite_sprite(z80_byte value)
{
	//printf ("Out tbblue_out_sprite_sprite. Index: %d subindex: %d %02XH\n",tbsprite_index_sprite,tbsprite_index_sprite_subindex,value);

/*
Once a sprite is selected via port 0x303B, 
its attributes can be written to this port one byte after another. 
Sprites can have either four or five attribute bytes and the internal attribute pointer 
will move onto the next sprite after those four or five attribute bytes are written. 
This means you can select a sprite via port 0x303B and write attributes for as many sequential sprites as desired. 
The attribute pointer will roll over from sprite 127 to sprite 0.
*/


	//Indices al indicar paleta, pattern, sprites. Subindex indica dentro de cada pattern o sprite a que posicion 
	//(0..3/4 en sprites o 0..255 en pattern ) apunta
	//z80_byte tbsprite_index_sprite,tbsprite_index_sprite_subindex;

	tbsprite_sprites[tbsprite_index_sprite][tbsprite_index_sprite_subindex]=value;
	if (tbsprite_index_sprite_subindex == 3 && (value & 0x40) == 0) {			
		// 4-byte type, add 0 as fifth
		tbsprite_index_sprite_subindex++;
		tbsprite_sprites[tbsprite_index_sprite][tbsprite_index_sprite_subindex]=0;
	}

	tbsprite_index_sprite_subindex++;

	// Fin de ese sprite
	if (tbsprite_index_sprite_subindex == TBBLUE_SPRITE_ATTRIBUTE_SIZE) {
		tbsprite_increment_index_303b();
	}
}


z80_byte tbblue_get_port_sprite_index(void)
{
	/*
	Port 0x303B, if read, returns some information:

Bits 7-2: Reserved, must be 0.
Bit 1: max sprites per line flag.
Bit 0: Collision flag.
*/
	z80_byte value=tbblue_port_303b;
	//Cuando se lee, se resetean bits 0 y 1
	//printf ("-----Reading port 303b. result value: %d\n",value);
	tbblue_port_303b &=(255-1-2);

	return value;

}

