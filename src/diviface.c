/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>


#include "diviface.h"
#include "cpu.h"
#include "debug.h"
#include "utils.h"
#include "menu.h"
#include "screen.h"
#include "operaciones.h"
#include "contend.h"
#include "tbblue.h"
#include "multiface.h"

z80_bit diviface_enabled={0};

//Si ha entrado pagina divmmc de manera automatica
z80_bit diviface_paginacion_automatica_activa={0};


z80_byte diviface_control_register=0;

z80_bit diviface_allow_automatic_paging={0};


//Si esta la paginacion manual de diviface activa
//z80_bit diviface_paginacion_manual_activa={0};

//Donde empieza la memoria asignada y por tanto la rom
z80_byte *diviface_memory_pointer;

//Donde empieza la ram. En zxuno y spectrum, va justo despues de el firmware (DIVIFACE_FIRMWARE_ALLOCATED_KB)
//En caso de tbblue, va en otro sitio
z80_byte *diviface_ram_memory_pointer;


z80_bit diviface_eprom_write_jumper={0};


void diviface_set_automatic(z80_byte value)
{
        diviface_paginacion_automatica_activa.v = value;
        tbblue_touch_mmu01();
}

//puntero a cpu core normal sin diviface
//void (*cpu_core_loop_no_diviface) (void);

int diviface_current_ram_memory_bits=4; //Por defecto 128 KB
/*
-using 2 bits: 32 kb
-using 3 bits: 64 kb
-using 4 bits: 128 kb (default)
-using 5 bits: 256 kb
-using 6 bits: 512 kb
*/

int get_diviface_ram_mask(void)
{
	int value_mask;

	if (diviface_current_ram_memory_bits<2 || diviface_current_ram_memory_bits>6)
		cpu_panic("Invalid bit mask value for diviface");

	value_mask=(1<<diviface_current_ram_memory_bits)-1;
	//Ej: si vale 2, (1<<2)-1=3
	//si vale 3, (1<<3)-1=7
	//si vale 4, (1<<4)-1=15
	//etc


	//printf ("diviface_current_ram_memory_bits: %d value_mask: %d\n",diviface_current_ram_memory_bits,value_mask);

	return value_mask;
}

int get_diviface_total_ram(void)
{
	return (8<<diviface_current_ram_memory_bits);
}





        int diviface_salta_trap_despues=0;
	int diviface_salta_trap_despaginacion_despues=0;


//Core de cpu loop para hacer traps de cpu
void diviface_pre_opcode_fetch(void)
{

/*
Memory mapping could be invoked manually (by setting CONMEM), or automatically
(CPU has fetched opcode form an entry-point). Automatic mapping is active
only if EPROM/EEPROM is present (jumper EPROM is closed) or bit MAPRAM is set.
Automatic mapping occurs at the begining of refresh cycle after fetching
opcodes (M1 cycle) from 0000h, 0008h, 0038h, 0066h, 04c6h and 0562h. It's
also mapped instantly (100ns after /MREQ of that fetch is falling down) after
executing opcode from area 3d00..3dffh. Memory is automatically disconnected in
refresh cycle of the instruction fetch from so called off-area, which is
1ff8-1fffh.
*/
    if (diviface_allow_automatic_paging.v && !multiface_switched_on.v &&
        !tbblue_bootrom.v && ((tbblue_registers[0x03] & 0x07) != 0x00))
    {
        int diviface_map_enable = 0;
        int diviface_map_always = 0;
        int diviface_map_timing = 0;
        int diviface_map_mmu = 0;

	switch (reg_pc & 0xff00)
        {
                case 0x0000:
                {
                        switch (reg_pc) {
                        case 0x0000:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x01;
                                diviface_map_always  = tbblue_registers[0xb9] & 0x01;
                                diviface_map_timing = tbblue_registers[0xba] & 0x01;
                                break;
                        case 0x0008:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x02;
                                diviface_map_always = tbblue_registers[0xb9] & 0x02;
                                diviface_map_timing = tbblue_registers[0xba] & 0x02;
                                break;
                        case 0x0010:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x04;
                                diviface_map_always = tbblue_registers[0xb9] & 0x04;
                                diviface_map_timing = tbblue_registers[0xba] & 0x04;
                                break;
                        case 0x0018:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x08;
                                diviface_map_always = tbblue_registers[0xb9] & 0x08;
                                diviface_map_timing = tbblue_registers[0xba] & 0x08;
                                break;
                        case 0x0020:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x10;
                                diviface_map_always = tbblue_registers[0xb9] & 0x10;
                                diviface_map_timing = tbblue_registers[0xba] & 0x10;
                                break;
                        case 0x0028:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x20;
                                diviface_map_always = tbblue_registers[0xb9] & 0x20;
                                diviface_map_timing = tbblue_registers[0xba] & 0x20;
                                break;
                        case 0x0030:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x40;
                                diviface_map_always = tbblue_registers[0xb9] & 0x40;
                                diviface_map_timing = tbblue_registers[0xba] & 0x40;
                                break;
                        case 0x0038:
                                diviface_map_enable = tbblue_registers[0xb8] & 0x80;
                                diviface_map_always = tbblue_registers[0xb9] & 0x80;
                                diviface_map_timing = tbblue_registers[0xba] & 0x80;
                                break;
                        case 0x0066:
                                // TODO Not quite right, should only occur if button pressed.
                                diviface_map_enable = tbblue_registers[0xbb] & 0x03;
                                diviface_map_always = 1;
                                diviface_map_timing = tbblue_registers[0xbb] & 0x02;
                                break;
                        }
                        break;
                }

                case 0x0400:
                {
                        switch (reg_pc) {
                        case 0x04c6:
                                diviface_map_enable = tbblue_registers[0xbb] & 0x04;
                                diviface_map_always = 0;
                                diviface_map_timing = 0;
                                break;
                        case 0x04d7:
                                diviface_map_enable = tbblue_registers[0xbb] & 0x10;
                                diviface_map_always = 0;
                                diviface_map_timing = 0;
                                break;
                        }
                        break;
                }

                case 0x0500:
                {
                        switch (reg_pc) {
                        case 0x0562:
                                diviface_map_enable = tbblue_registers[0xbb] & 0x08;
                                diviface_map_always = 0;
                                diviface_map_timing = 0;
                                break;
                        case 0x056a:
                                diviface_map_enable = tbblue_registers[0xbb] & 0x20;
                                diviface_map_always = 0;
                                diviface_map_timing = 0;
                                break;
                        }
                        break;
                }

		case 0x1f00:
                {
                        if ((reg_pc & 0x1ff8) == 0x1ff8)
                        {
                                diviface_map_enable = tbblue_registers[0xbb] & 0x40;
                                diviface_map_always = 1;
                                diviface_map_timing = 2;
                        }
                        break;
                }

                case 0x3d00:
                {
                        diviface_map_enable = tbblue_registers[0xbb] & 0x80;
                        diviface_map_always = 0;
                        diviface_map_timing = 1;
                        diviface_map_mmu    = 1;
                        break;
                }
        }

	diviface_salta_trap_despues=0;
	diviface_salta_trap_despaginacion_despues=0;

        // TODO Not quite right, as ROM3 is "visible" to the h/w even if DivMMC already active.
        if (diviface_map_enable && (diviface_map_always || tbblue_rom3_visible[diviface_map_mmu]))
        {
                switch (diviface_map_timing)
                {
                        case 0:
                                diviface_salta_trap_despues = 1;
                                break;
                        case 1:
                                diviface_set_automatic(1);
                                break;
                        case 2:
                                diviface_salta_trap_despaginacion_despues = 1;
                                break;
                }
        }
    }
}



void diviface_post_opcode_fetch(void)
{

	if (diviface_salta_trap_despues) {
		//printf ("Saltado trap de paginacion despues. pc actual: %d\n",reg_pc);
                debug_printf(VERBOSE_DEBUG, "diviface_post_opcode_fetch: PC=0x%04x, setting automatic\n", reg_pc);
		diviface_set_automatic(1);
	}

	if (diviface_salta_trap_despaginacion_despues) {
		//printf ("Saltado trap de despaginacion despues. pc actual: %d\n",reg_pc);
                debug_printf(VERBOSE_DEBUG, "diviface_post_opcode_fetch: PC=0x%04x, clearing automatic\n", reg_pc);
		diviface_set_automatic(0);
	}


}



//Escritura de puerto de control divide/divmmc. Activar o desactivar paginacion solamente
void diviface_write_control_register(z80_byte value)
{
	diviface_control_register = value;
        tbblue_touch_mmu01();
}


//Lectura de puerto de control divide/divmmc
z80_byte diviface_read_control_register(void)
{

	return diviface_control_register;
}


void diviface_enable()
{
	diviface_write_control_register(0);
	diviface_set_automatic(0);
	diviface_enabled.v=1;
	diviface_allow_automatic_paging.v=1;
}

void diviface_disable(void)
{
        diviface_enabled.v=0;
	diviface_allow_automatic_paging.v=0;
}
