/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"


//Diferentes paletas
//Total:
//     000 = ULA first palette
//     100 = ULA secondary palette
//     001 = Layer 2 first palette
//    101 = Layer 2 secondary palette
//     010 = Sprites first palette 
//     110 = Sprites secondary palette
//     011 = Tilemap first palette
//     111 = Tilemap second palette
//Paletas de 256 colores formato RGB9 RRRGGGBBB
//Valores son de 9 bits por tanto lo definimos con z80_int que es de 16 bits
z80_int tbblue_palette_ula_first[256];
z80_int tbblue_palette_ula_second[256];
z80_int tbblue_palette_layer2_first[256];
z80_int tbblue_palette_layer2_second[256];
z80_int tbblue_palette_sprite_first[256];
z80_int tbblue_palette_sprite_second[256];
z80_int tbblue_palette_tilemap_first[256];
z80_int tbblue_palette_tilemap_second[256];

//Damos la paleta que se esta leyendo o escribiendo en una operacion de I/O
//Para ello mirar bits 6-4  de reg 0x43
z80_int *tbblue_get_palette_rw(void)
{
/*
(R/W) 0x43 (67) => Palette Control
  bit 7 = '1' to disable palette write auto-increment.
  bits 6-4 = Select palette for reading or writing:
     000 = ULA first palette
     100 = ULA secondary palette
     001 = Layer 2 first palette
     101 = Layer 2 secondary palette
     010 = Sprites first palette 
     110 = Sprites secondary palette
  bit 3 = Select Sprites palette (0 = first palette, 1 = secondary palette)
  bit 2 = Select Layer 2 palette (0 = first palette, 1 = secondary palette)
  bit 1 = Select ULA palette (0 = first palette, 1 = secondary palette)
*/
	z80_byte active_palette=(tbblue_registers[0x43]>>4)&7;

	switch (active_palette) {
		case 0:
			return tbblue_palette_ula_first;
		break;

		case 4:
			return tbblue_palette_ula_second;
		break;

		case 1:
			return tbblue_palette_layer2_first;
		break;

		case 5:
			return tbblue_palette_layer2_second;
		break;

		case 2:
			return tbblue_palette_sprite_first;
		break;

		case 6:
			return tbblue_palette_sprite_second;
		break;

		case 3:
			return tbblue_palette_tilemap_first;
		break;		

		case 7:
			return tbblue_palette_tilemap_second;
		break;				

		//por defecto retornar siempre ULA first palette
		default:
			return tbblue_palette_ula_first;
		break;
	}
}

//Damos el valor del color de la paleta que se esta leyendo o escribiendo en una operacion de I/O
z80_int tbblue_get_value_palette_rw(z80_byte index)
{
	z80_int *paleta;

	paleta=tbblue_get_palette_rw();

	return paleta[index];
}


//Modificamos el valor del color de la paleta que se esta leyendo o escribiendo en una operacion de I/O
void tbblue_set_value_palette_rw(z80_byte index,z80_int valor)
{
	z80_int *paleta;

	paleta=tbblue_get_palette_rw();

	paleta[index]=valor;
}

//Damos el valor del color de la paleta que se esta mostrando en pantalla para sprites
//Para ello mirar bit 3 de reg 0x43
z80_int tbblue_get_palette_active_sprite(z80_byte index)
{
/*
(R/W) 0x43 (67) => Palette Control

  bit 3 = Select Sprites palette (0 = first palette, 1 = secondary palette)
  bit 2 = Select Layer 2 palette (0 = first palette, 1 = secondary palette)
  bit 1 = Select ULA palette (0 = first palette, 1 = secondary palette)
*/
	if (tbblue_registers[0x43]&8) return tbblue_palette_sprite_second[index];
	else return tbblue_palette_sprite_first[index];

}

//Damos el valor del color de la paleta que se esta mostrando en pantalla para layer2
//Para ello mirar bit 2 de reg 0x43
z80_int tbblue_get_palette_active_layer2(z80_byte index)
{
/*
(R/W) 0x43 (67) => Palette Control

  bit 3 = Select Sprites palette (0 = first palette, 1 = secondary palette)
  bit 2 = Select Layer 2 palette (0 = first palette, 1 = secondary palette)
  bit 1 = Select ULA palette (0 = first palette, 1 = secondary palette)
*/
	if (tbblue_registers[0x43]&4) return tbblue_palette_layer2_second[index];
	else return tbblue_palette_layer2_first[index];

}

//Damos el valor del color de la paleta que se esta mostrando en pantalla para ula
//Para ello mirar bit 1 de reg 0x43
z80_int tbblue_get_palette_active_ula(z80_byte index)
{
/*
(R/W) 0x43 (67) => Palette Control

  bit 3 = Select Sprites palette (0 = first palette, 1 = secondary palette)
  bit 2 = Select Layer 2 palette (0 = first palette, 1 = secondary palette)
  bit 1 = Select ULA palette (0 = first palette, 1 = secondary palette)
*/
	if (tbblue_registers[0x43]&2) return tbblue_palette_ula_second[index];
	else return tbblue_palette_ula_first[index];

}


//Damos el valor del color de la paleta que se esta mostrando en pantalla para tiles
z80_int tbblue_get_palette_active_tilemap(z80_byte index)
{
/*
Bit	Function
7	1 to enable the tilemap
6	0 for 40x32, 1 for 80x32
5	1 to eliminate the attribute entry in the tilemap
4	palette select (0 = first Tilemap palette, 1 = second)
3	(core 3.0) enable "text mode"
2	Reserved, must be 0
1	1 to activate 512 tile mode (bit 0 of tile attribute is ninth bit of tile-id)
0 to use bit 0 of tile attribute as "ULA over tilemap" per-tile-selector
*/
	if (tbblue_registers[0x6B] & 16) return tbblue_palette_tilemap_second[index];
        else return tbblue_palette_tilemap_first[index];

}


z80_int tbblue_get_9bit_colour(z80_byte valor)
{
	//Retorna color de 9 bits en base a 8 bits
	z80_int valor16=valor;

	//Bit bajo sale de hacer or de bit 1 y 0
	//z80_byte bit_bajo=valor&1;
	z80_byte bit_bajo=(valor&1)|((valor&2)>>1);

	//rotamos a la izquierda para que sean los 8 bits altos
	valor16=valor16<<1;

	valor16 |=bit_bajo;

	return valor16;	
}

void tbblue_reset_palettes(void)
{
	//Inicializar Paletas
	int i;

//z80_int valor16=tbblue_get_9bit_colour(valor);

	//Paletas layer2 & sprites son los mismos valores del indice*2 y metiendo bit 0 como el bit1 inicial
	//(cosa absurda pues no sigue la logica de mezclar bit 0 y bit 1 usado en registro 41H)
	for (i=0;i<256;i++) {
		z80_int color;
		color=i*2;
		if (i&2) color |=1;

 		tbblue_palette_layer2_first[i]=color;
 		tbblue_palette_layer2_second[i]=color;
 		tbblue_palette_sprite_first[i]=color;
 		tbblue_palette_sprite_second[i]=color;
	}


	//Se repiten los 16 colores en los 256. Para colores sin brillo, componente de color vale 5 (101b)
	const z80_int tbblue_default_ula_colours[16]={
0 ,   //000000000 
5 ,   //000000101 
320 , //101000000 
325 , //101000101 
40 ,  //000101000 
45 ,  //000101101 
360 , //101101000 
365 , //101101101 
0 ,   //000000000 
7 ,   //000000111 
448 , //111000000 
455 , //111000111 
56 ,  //000111000 
63 ,  //000111111 
504 , //111111000 
511   //111111111 
	};
	/*
	Nota: para convertir una lista de valores binarios en decimal:
	LINEA=""

while read LINEA; do
echo -n "$((2#$LINEA))"
echo " , //$LINEA "


done < /tmp/archivo_lista.txt
	*/

	int j;

	for (j=0;j<16;j++) {
		for (i=0;i<16;i++) {
			int colorpaleta=tbblue_default_ula_colours[i];


	//bright magenta son colores transparentes por defecto (1C7H y 1C6H  / 2 = E3H)
	//lo cambio a 1CF, que es un color FF24FFH, que no es magenta puro, pero evita el problema de transparente por defecto
	//esto lo corrige igualmente nextos al arrancar, pero si arrancamos tbblue en modo fast-boot, pasaria que los bright
	//magenta se verian transparentes
			if (i==11) colorpaleta=0x1CF;


			tbblue_palette_ula_first[j*16+i]=colorpaleta;
			tbblue_palette_ula_second[j*16+i]=colorpaleta;

		}
	}



}


//Indica si al escribir registro 44h de paleta:
//si 0, se escribe 8 bits superiores
//si no 0, se escribe 1 bit inferior
int tbblue_write_palette_state=0;
z80_byte tbblue_write_palette_latched_8=0;

void tbblue_reset_palette_write_state(void)
{
	tbblue_write_palette_state=0;
}

void tbblue_increment_palette_index(void)
{
//(R/W) 0x43 (67) => Palette Control
//  bit 7 = '1' to disable palette write auto-increment.

	if ((tbblue_registers[0x43] & 128)==0) {
		z80_byte indice=tbblue_registers[0x40];
		indice++;
		tbblue_registers[0x40]=indice;
	}

	tbblue_reset_palette_write_state();
}



//Escribe valor de 8 bits superiores (de total de 9) para indice de color de paleta 
void tbblue_write_palette_value_high8(z80_byte valor)
{
/*
(R/W) 0x40 (64) => Palette Index
  bits 7-0 = Select the palette index to change the default colour. 
  0 to 127 indexes are to ink colours and 128 to 255 indexes are to papers.
  (Except full ink colour mode, that all values 0 to 255 are inks)
  Border colours are the same as paper 0 to 7, positions 128 to 135,
  even at full ink mode. 
  (inks and papers concept only applies to Enhanced ULA palette. 
  Layer 2 and Sprite palettes works as "full ink" mode)

  (R/W) 0x41 (65) => Palette Value (8 bit colour)
  bits 7-0 = Colour for the palette index selected by the register 0x40. Format is RRRGGGBB
  Note the lower blue bit colour will be an OR between bit 1 and bit 0. 
  After the write, the palette index is auto-incremented to the next index. 
  The changed palette remains until a Hard Reset.

(R/W) 0x43 (67) => Palette Control
  bit 7 = '1' to disable palette write auto-increment.


(R/W) 0x44 (68) => Palette Value (9 bit colour)
  Two consecutive writes are needed to write the 9 bit colour
  1st write:
     bits 7-0 = RRRGGGBB
  2nd write. 
     bit 7-1 = Reserved, must be 0
     bit 0 = lsb B
  After the two consecutives writes the palette index is auto-incremented.
  The changed palette remais until a Hard Reset.

*/
	z80_byte indice=tbblue_registers[0x40];

	//Obtenemos valor actual y alteramos los 8 bits altos del total de 9
	//z80_int color_actual=tbblue_get_value_palette_rw(indice);

	//Conservamos bit bajo
	//color_actual &=1;
	//Bit bajo es el mismo que bit 1

	/*z80_int valor16=valor;

	//Bit bajo sale de hacer or de bit 1 y 0
	//z80_byte bit_bajo=valor&1;
	z80_byte bit_bajo=(valor&1)|((valor&2)>>1);

	//rotamos a la izquierda para que sean los 8 bits altos
	valor16=valor16<<1;

	valor16 |=bit_bajo;*/

	z80_int valor16=tbblue_get_9bit_colour(valor);

	//y or del valor de 1 bit de B
	//valor16 |=color_actual;

	tbblue_set_value_palette_rw(indice,valor16);

//printf ("Escribir paleta\n");


}


//Escribe valor de 1 bit inferior (de total de 9) para indice de color de paleta y incrementa indice
void tbblue_write_palette_value_low1(z80_byte valor)
{
/*
(R/W) 0x40 (64) => Palette Index
  bits 7-0 = Select the palette index to change the default colour. 
  0 to 127 indexes are to ink colours and 128 to 255 indexes are to papers.
  (Except full ink colour mode, that all values 0 to 255 are inks)
  Border colours are the same as paper 0 to 7, positions 128 to 135,
  even at full ink mode. 
  (inks and papers concept only applies to Enhanced ULA palette. 
  Layer 2 and Sprite palettes works as "full ink" mode)

  (R/W) 0x41 (65) => Palette Value (8 bit colour)
  bits 7-0 = Colour for the palette index selected by the register 0x40. Format is RRRGGGBB
  Note the lower blue bit colour will be an OR between bit 1 and bit 0. 
  After the write, the palette index is auto-incremented to the next index. 
  The changed palette remains until a Hard Reset.


(R/W) 0x44 (68) => Palette Value (9 bit colour)
  Two consecutive writes are needed to write the 9 bit colour
  1st write:
     bits 7-0 = RRRGGGBB
  2nd write. 
     bit 7-1 = Reserved, must be 0
     bit 0 = lsb B
  After the two consecutives writes the palette index is auto-incremented.
  The changed palette remais until a Hard Reset.

*/
	z80_byte indice=tbblue_registers[0x40];

	//Bit inferior siempre cambia indice anterior
	//indice--;

	//Obtenemos valor actual y conservamos los 8 bits altos del total de 9
	z80_int color_actual=tbblue_get_value_palette_rw(indice);

	//Conservamos 8 bits altos
	color_actual &=0x1FE;

	//Y valor indicado, solo conservar 1 bit
	valor &= 1;

	color_actual |=valor;

	tbblue_set_value_palette_rw(indice,color_actual);

	tbblue_increment_palette_index();


}


//Escribe valor de paleta de registro 44H, puede que se escriba en 8 bit superiores o en 1 inferior
void tbblue_write_palette_value_high8_low1(z80_byte valor)
{
	if (tbblue_write_palette_state==0) {
		tbblue_write_palette_value_high8(valor);
                tbblue_write_palette_latched_8 = valor;
                tbblue_write_palette_state++;
	}

	else tbblue_write_palette_value_low1(valor);
	
}


void tbblue_get_string_palette_format(char *texto)
{
//if (value&128) screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,"Enabling lores video mode. 128x96 256 colours");
	/*
	(R/W) 0x43 (67) => Palette Control
	bit 0 = Disable the standard Spectrum flash feature to enable the extra colours.
  (Reset to 0 after a reset)

	(R/W) 0x42 (66) => Palette Format
  bits 7-0 = Number of the last ink colour entry on palette. (Reset to 15 after a Reset)
  This number can be 1, 3, 7, 15, 31, 63, 127 or 255.
	

	*/


	if ((tbblue_registers[67]&1)==0) strcpy (texto,"Normal Color palette");
	else {

		z80_byte palformat=tbblue_registers[66];

		/*
		Ejemplo: mascara 3:   00000011
		Son 4 tintas
		64 papeles

		Para pasar de tintas a papeles :    00000011 -> inverso -> 11111100
		Dividimos 11111100 entre tintas, para rotar el valor 2 veces a la derecha = 252 / 4 = 63   -> +1 -> 64
		*/

		int tintas=palformat+1;

		int papeles=255-palformat;

		//Dado que tintas siempre es +1, nunca habra division por 0. Pero por si acaso

		if (tintas==0) papeles=0;
		else {
			papeles=(papeles/tintas)+1;
		}

		sprintf (texto,"Extra colors %d inks %d papers",tintas,papeles);

	}
		

}


void tbblue_splash_palette_format(void)
{
//if (value&128) screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,"Enabling lores video mode. 128x96 256 colours");
	/*
	(R/W) 0x43 (67) => Palette Control
	bit 0 = Disable the standard Spectrum flash feature to enable the extra colours.
  (Reset to 0 after a reset)

	(R/W) 0x42 (66) => Palette Format
  bits 7-0 = Number of the last ink colour entry on palette. (Reset to 15 after a Reset)
  This number can be 1, 3, 7, 15, 31, 63, 127 or 255.
	

	*/

	char mensaje[200];
	char videomode[100];

	tbblue_get_string_palette_format(videomode);

	sprintf (mensaje,"Setting %s",videomode);

	screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,mensaje);

	

	/*
	if ((tbblue_registers[67]&1)==0) screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,"Disabling extra colour palette");
	else {

		z80_byte palformat=tbblue_registers[66];

		
		//Ejemplo: mascara 3:   00000011
		//Son 4 tintas
		//64 papeles

		//Para pasar de tintas a papeles :    00000011 -> inverso -> 11111100
		//Dividimos 11111100 entre tintas, para rotar el valor 2 veces a la derecha = 252 / 4 = 63   -> +1 -> 64
		

		int tintas=palformat+1;

		int papeles=255-palformat;

		//Dado que tintas siempre es +1, nunca habra division por 0. Pero por si acaso

		if (tintas==0) papeles=0;
		else {
			papeles=(papeles/tintas)+1;
		}

		char mensaje[200];
		sprintf (mensaje,"Enabling extra colour palette: %d inks %d papers",tintas,papeles);
		screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,mensaje);
	}
	*/

}
