/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MENU_ITEMS_H
#define MENU_ITEMS_H

#include "menu_system.h"

#define SPRITES_X ( menu_origin_x() )
#define SPRITES_Y 0
#define SPRITES_ANCHO 32
#define SPRITES_ALTO 14
#define SPRITES_ALTO_VENTANA (SPRITES_ALTO+10)

#define MENU_DEBUG_NUMBER_FLAGS_OBJECTS 7

#define ANCHO_SCANLINE_CURSOR 32

#define MAX_LENGTH_ADDRESS_MEMORY_ZONE 6

#define DEBUG_HEXDUMP_WINDOW_X (menu_origin_x() )
#define DEBUG_HEXDUMP_WINDOW_Y 1
#define DEBUG_HEXDUMP_WINDOW_ANCHO 32
#define DEBUG_HEXDUMP_WINDOW_ALTO 23

#define TSCONF_SPRITENAV_WINDOW_ANCHO 32
#define TSCONF_SPRITENAV_WINDOW_ALTO 20
#define TSCONF_SPRITENAV_WINDOW_X (menu_center_x()-TSCONF_SPRITENAV_WINDOW_ANCHO/2 )
#define TSCONF_SPRITENAV_WINDOW_Y (menu_center_y()-TSCONF_SPRITENAV_WINDOW_ALTO/2)

#define TSCONF_TILENAV_WINDOW_ANCHO 32
#define TSCONF_TILENAV_WINDOW_ALTO 24
#define TSCONF_TILENAV_WINDOW_X (menu_center_x()-TSCONF_TILENAV_WINDOW_ANCHO/2 )
#define TSCONF_TILENAV_WINDOW_Y (menu_center_y()-TSCONF_TILENAV_WINDOW_ALTO/2)
#define TSCONF_TILENAV_TILES_VERT_PER_WINDOW 64
#define TSCONF_TILENAV_TILES_HORIZ_PER_WINDOW 64
#define DEBUG_TSCONF_TILENAV_MAX_TILES (64*64)

#define REALJOYSTICK_TEST_ANCHO 30
#define REALJOYSTICK_TEST_ALTO 15


extern char binary_file_load[PATH_MAX];
extern char binary_file_save[PATH_MAX];
extern char zxprinter_bitmap_filename_buffer[PATH_MAX];
extern char zxprinter_ocr_filename_buffer[PATH_MAX];
extern menu_z80_moto_int view_sprites_direccion;
extern z80_int view_sprites_ancho_sprite;
extern z80_int view_sprites_alto_sprite;
extern int view_sprites_hardware;
extern z80_bit view_sprites_inverse;
extern int view_sprite_incremento;
extern int view_sprites_ppb;
extern int view_sprites_bpp;
extern int view_sprites_palette;
extern int view_sprites_scr_sprite;
extern int view_sprites_offset_palette;
extern zxvision_window *menu_debug_draw_sprites_window;
extern int view_sprites_bytes_por_linea;
extern int view_sprites_bytes_por_ventana;
extern int view_sprites_increment_cursor_vertical;
extern zxvision_window zxvision_window_view_sprites;
extern int menu_debug_show_memory_zones;
extern int menu_debug_memory_zone;
extern menu_z80_moto_int menu_debug_memory_zone_size;
extern int menu_debug_registers_current_view;
extern menu_z80_moto_int menu_debug_disassemble_last_ptr;
extern z80_bit menu_debug_disassemble_hexa_view;
extern z80_bit menu_debug_follow_pc;
extern menu_z80_moto_int menu_debug_memory_pointer;
extern int menu_debug_line_cursor;
extern char menu_debug_change_registers_last_reg[30];
extern char menu_debug_change_registers_last_val[30];
extern size_t menu_debug_registers_print_registers_longitud_opcode;
extern menu_z80_moto_int menu_debug_memory_pointer_last;
extern int menu_debug_registers_subview_type;
extern int menu_debug_hexdump_with_ascii_modo_ascii;
extern z80_bit menu_breakpoint_exception_pending_show;
extern int continuous_step;
extern zxvision_window *menu_watches_overlay_window;
extern zxvision_window zxvision_window_watches;
extern int menu_debug_continuous_speed;
extern int menu_debug_continuous_speed_step;
extern int menu_debug_registers_buffer_precursor[ANCHO_SCANLINE_CURSOR];
extern int menu_debug_registers_buffer_pre_x;
extern int menu_debug_registers_buffer_pre_y;
extern zxvision_window zxvision_window_menu_debug_registers;
extern zxvision_window *menu_debug_dma_tsconf_zxuno_overlay_window;
extern zxvision_window *menu_debug_unnamed_console_overlay_window;
extern int menu_debug_unnamed_console_indicador_actividad_contador;
extern int menu_debug_unnamed_console_indicador_actividad_visible;
extern zxvision_window zxvision_window_unnamed_console;
extern menu_z80_moto_int menu_debug_hexdump_direccion;
extern int menu_hexdump_edit_position_x;
extern int menu_hexdump_edit_position_y;
extern int menu_hexdump_lineas_total;
extern int menu_hexdump_edit_mode;
extern const int menu_hexdump_bytes_por_linea;
extern int menu_debug_hexdump_cursor_en_zona_ascii;
extern int menu_find_bytes_empty;
extern unsigned char *menu_find_bytes_mem_pointer;
extern int menu_find_lives_state;
extern z80_int menu_find_lives_pointer;
extern int lives_to_find;
extern z80_byte ultimo_menu_find_lives_set;
extern int last_debug_poke_dir; 
extern char aofilename_file[PATH_MAX];
extern menu_z80_moto_int load_binary_last_address;
extern menu_z80_moto_int load_binary_last_length;
extern menu_z80_moto_int save_binary_last_address;
extern menu_z80_moto_int save_binary_last_length;
extern int menu_debug_tsconf_tbblue_msx_videoregisters_valor_contador_segundo_anterior;
extern zxvision_window *menu_debug_tsconf_tbblue_msx_videoregisters_overlay_window;
extern zxvision_window menu_debug_tsconf_tbblue_msx_videoregisters_ventana;
extern int menu_tsconf_layer_valor_contador_segundo_anterior;
extern char *menu_tsconf_layer_aux_usedunused_used;
extern char *menu_tsconf_layer_aux_usedunused_unused;
extern zxvision_window *menu_tsconf_layer_overlay_window;
extern int menu_debug_tsconf_tbblue_msx_spritenav_current_sprite;
extern zxvision_window *menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window;
extern zxvision_window zxvision_window_tsconf_tbblue_spritenav;
extern int menu_debug_tsconf_tbblue_msx_tilenav_current_tilelayer;
extern z80_bit menu_debug_tsconf_tbblue_msx_tilenav_showmap;
extern zxvision_window *menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window;
extern zxvision_window zxvision_window_tsconf_tbblue_tilenav;
extern int menu_info_joystick_last_button;
extern int menu_info_joystick_last_type;
extern int menu_info_joystick_last_value;
extern int menu_info_joystick_last_index;
extern int menu_info_joystick_last_raw_value;
extern defined_f_function defined_f_functions_array[MAX_F_FUNCTIONS];
extern enum defined_f_function_ids defined_f_functions_keys_array[MAX_F_FUNCTIONS_KEYS];


extern void menu_tape_settings_trunc_name(char *orig,char *dest,int max);
extern void menu_view_screen(MENU_ITEM_PARAMETERS);
extern void menu_display_load_screen(MENU_ITEM_PARAMETERS);
extern void menu_display_save_screen(MENU_ITEM_PARAMETERS);
extern void menu_mem_breakpoints_edit(MENU_ITEM_PARAMETERS);
extern void menu_mem_breakpoints_list(MENU_ITEM_PARAMETERS);
extern void menu_mem_breakpoints_clear(MENU_ITEM_PARAMETERS);
extern void menu_clear_all_breakpoints(MENU_ITEM_PARAMETERS);
extern void menu_debug_reset(MENU_ITEM_PARAMETERS);
extern void menu_debug_hard_reset(MENU_ITEM_PARAMETERS);
extern void menu_debug_nmi_drive(MENU_ITEM_PARAMETERS);
extern void menu_debug_nmi_multiface_tbblue(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_truncate(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_file(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable_address(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable_datetime(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable_tstates(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable_opcode(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable_registers(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_enable_rotate(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_rotate_number(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_rotate_size(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_rotate_lines(MENU_ITEM_PARAMETERS);
extern void menu_cpu_transaction_log_ignore_rep_halt(MENU_ITEM_PARAMETERS);
extern void menu_debug_disassemble_export(int p);
extern void menu_debug_disassemble(MENU_ITEM_PARAMETERS);
extern void menu_debug_assemble(MENU_ITEM_PARAMETERS);
extern menu_z80_moto_int menu_debug_disassemble_bajar(menu_z80_moto_int dir_inicial);
extern menu_z80_moto_int menu_debug_disassemble_subir(menu_z80_moto_int dir_inicial);
extern void menu_debug_dissassemble_una_inst_sino_hexa(char *dumpassembler,menu_z80_moto_int dir,int *longitud_final_opcode,int sino_hexa);
extern void menu_debug_dissassemble_una_instruccion(char *dumpassembler,menu_z80_moto_int dir,int *longitud_final_opcode);
extern void menu_debug_set_memory_zone_attr(void);
extern z80_byte menu_debug_get_mapped_byte(int direccion);
extern void menu_debug_write_mapped_byte(int direccion,z80_byte valor);
extern menu_z80_moto_int adjust_address_memory_size(menu_z80_moto_int direccion);
extern void menu_debug_set_memory_zone_mapped(void);
extern int menu_change_memory_zone_list_title(char *titulo);
extern int menu_change_memory_zone_list(void);
extern void menu_set_memzone(int valor_opcion);
extern void menu_debug_change_memory_zone(void);
extern void menu_debug_change_memory_zone_non_interactive(void);
extern void menu_debug_set_memory_zone(int zone);
extern int menu_get_current_memory_zone_name_number(char *s);
extern int menu_debug_get_total_digits_hexa(int valor);
extern int menu_debug_get_total_digits_dec(int valor);
extern void menu_debug_print_address_memory_zone(char *texto, menu_z80_moto_int address);
extern int menu_debug_sprites_return_index_palette(int paleta, z80_byte color);
extern int menu_debug_sprites_return_color_palette(int paleta, z80_byte color);
extern void menu_debug_sprites_change_palette(void);
extern void menu_debug_sprites_get_palette_name(int paleta, char *s);
extern void menu_debug_sprites_change_bpp(void);
extern menu_z80_moto_int menu_debug_draw_sprites_get_pointer_offset(int direccion);
extern z80_byte menu_debug_draw_sprites_get_byte(menu_z80_moto_int puntero);
extern void menu_debug_draw_sprites(void);
extern menu_z80_moto_int menu_debug_view_sprites_change_pointer(menu_z80_moto_int p);
extern int menu_debug_view_sprites_save(menu_z80_moto_int direccion,int ancho, int alto, int ppb, int incremento);
extern void menu_debug_sprites_get_parameters_hardware(void);
extern void menu_debug_view_sprites_textinfo(zxvision_window *ventana);
extern void menu_debug_view_sprites(MENU_ITEM_PARAMETERS);
extern int menu_breakpoints_cond(void);
extern void menu_breakpoints_conditions_set(MENU_ITEM_PARAMETERS);
extern void menu_breakpoints_condition_evaluate_new(MENU_ITEM_PARAMETERS);
extern void menu_breakpoints_enable_disable(MENU_ITEM_PARAMETERS);
extern void menu_breakpoints_condition_enable_disable(MENU_ITEM_PARAMETERS);
extern void menu_debug_registers_dump_hex(char *texto,menu_z80_moto_int direccion,int longitud);
extern void menu_debug_registers_dump_ascii(char *texto,menu_z80_moto_int direccion,int longitud,z80_byte valor_xor);
extern void menu_debug_get_memory_pages(char *s);
extern int get_menu_debug_num_lineas_full(zxvision_window *w);
extern void menu_debug_change_registers(void);
extern void menu_debug_registers_change_ptr(void);
extern void menu_debug_show_register_line(int linea,char *textoregistros);
extern int menu_debug_get_main_list_view(zxvision_window *w);
extern int menu_debug_view_has_disassemly(void);
extern menu_z80_moto_int menu_debug_disassemble_subir_veces(menu_z80_moto_int posicion,int veces);
extern menu_z80_moto_int menu_debug_register_decrement_half(menu_z80_moto_int posicion,zxvision_window *w);
extern int menu_debug_hexdump_change_pointer(int p);
extern menu_z80_moto_int menu_debug_hexdump_adjusta_en_negativo(menu_z80_moto_int dir,int linesize);
extern void menu_debug_next_dis_show_hexa(void);
extern void menu_debug_registers_adjust_ptr_on_follow(void);
extern void menu_debug_registros_parte_derecha(int linea,char *buffer_linea,int columna_registros,int mostrar_separador);
extern int menu_debug_registers_print_registers(zxvision_window *w,int linea);
extern int menu_debug_registers_get_height_ventana_vista(void);
extern void menu_debug_registers_zxvision_ventana_set_height(zxvision_window *w);
extern void menu_debug_registers_set_title(zxvision_window *w);
extern void menu_debug_registers_ventana_common(zxvision_window *ventana);
extern void menu_debug_registers_zxvision_ventana(zxvision_window *ventana);
extern void menu_debug_registers_gestiona_breakpoint(void);
extern void menu_watches_overlay_mostrar_texto(void);
extern void menu_watches_overlay(void);
extern void menu_watches_edit(MENU_ITEM_PARAMETERS);
extern void menu_watches(MENU_ITEM_PARAMETERS);
extern void menu_debug_registers_set_view(zxvision_window *ventana,int vista);
extern void menu_debug_registers_splash_memory_zone(void);
extern void menu_debug_change_memory_zone_splash(void);
extern void menu_debug_cpu_step_over(void);
extern void menu_debug_cursor_up(void);
extern void menu_debug_cursor_down(zxvision_window *w);
extern void menu_debug_cursor_pgup(zxvision_window *w);
extern void menu_debug_cursor_pgdn(zxvision_window *w);
extern void menu_breakpoint_fired(char *s) ;
extern void menu_debug_ret(void);
extern void menu_debug_toggle_breakpoint(void);
extern void menu_debug_runto(void);
extern void menu_debug_get_legend_short_long(char *destination_string,int ancho_visible,char *short_string,char *long_string);
extern int menu_debug_registers_show_ptr_text(zxvision_window *w,int linea);
extern void menu_debug_switch_follow_pc(void);
extern void menu_debug_get_legend(int linea,char *s,zxvision_window *w);
extern void menu_debug_registers_next_cont_speed(void);
extern void menu_debug_registers_if_cls(void);
extern void menu_debug_cont_speed_progress(char *s);
extern void menu_debug_showscan_putpixel(z80_int *destino,int x,int y,int ancho,int color);
extern void menu_debug_registers_show_scan_pos_putcursor(int x_inicial,int y);
extern void menu_debug_registers_show_scan_position(void);
extern int menu_debug_registers_print_legend(zxvision_window *w,int linea);
extern int menu_debug_registers_get_line_legend(zxvision_window *w);
extern void menu_debug_registers_zxvision_save_size(zxvision_window *ventana,int *ventana_ancho_antes,int *ventana_alto_antes);
extern void menu_debug_registers(MENU_ITEM_PARAMETERS);
extern void menu_debug_dma_tsconf_zxuno_overlay(void);
extern void menu_debug_dma_tsconf_zxuno_disable(MENU_ITEM_PARAMETERS);
extern void menu_debug_dma_tsconf_zxuno(MENU_ITEM_PARAMETERS);
extern void menu_debug_ioports(MENU_ITEM_PARAMETERS);
extern void menu_debug_unnamed_console_show_legend(zxvision_window *ventana);
extern void menu_debug_unnamed_console_overlay(void);
extern void menu_debug_unnamed_console(MENU_ITEM_PARAMETERS);
extern void menu_debug_hexdump_with_ascii(char *dumpmemoria,menu_z80_moto_int dir_leida,int bytes_por_linea,z80_byte valor_xor);
extern void menu_debug_hexdump_print_editcursor(zxvision_window *ventana,int x,int y,char caracter);
extern void menu_debug_hexdump_print_editcursor_nibble(zxvision_window *ventana,int x,int y,char caracter);
extern void menu_debug_hexdump_edit_cursor_izquierda(void);
extern void menu_debug_hexdump_edit_cursor_derecha(int escribiendo_memoria);
extern void menu_debug_hexdump_cursor_arriba(void);
extern void menu_debug_hexdump_cursor_abajo(void);
extern void menu_debug_hexdump_copy(void);
extern void menu_debug_hexdump_aviso_edit_filezone(zxvision_window *w);
extern void menu_debug_hexdump_info_subzones(void);
extern void menu_debug_hexdump_crea_ventana(zxvision_window *ventana,int x,int y,int ancho,int alto);
extern menu_z80_moto_int menu_debug_hexdump_get_cursor_pointer(void);
extern void menu_debug_hexdump(MENU_ITEM_PARAMETERS);
extern void menu_debug_view_basic(MENU_ITEM_PARAMETERS);
extern int get_efectivo_tamanyo_find_buffer(void);
extern void menu_find_bytes_clear_results_process(void);
extern void menu_find_bytes_clear_results(MENU_ITEM_PARAMETERS);
extern void menu_find_bytes_view_results(MENU_ITEM_PARAMETERS);
extern int menu_find_bytes_process_from(z80_byte byte_to_find,int inicio);
extern int menu_find_bytes_process(z80_byte byte_to_find);
extern void menu_find_bytes_find(MENU_ITEM_PARAMETERS);
extern void menu_find_bytes_alloc_if_needed(void);
extern void menu_find_lives_restart(MENU_ITEM_PARAMETERS);
extern void menu_find_lives_initial(MENU_ITEM_PARAMETERS);
extern void menu_find_lives_set(MENU_ITEM_PARAMETERS);
extern void menu_debug_poke(MENU_ITEM_PARAMETERS);
extern void menu_debug_poke_pok_file(MENU_ITEM_PARAMETERS);
extern void menu_debug_load_binary(MENU_ITEM_PARAMETERS);
extern void menu_debug_save_binary(MENU_ITEM_PARAMETERS);
extern void menu_debug_tsconf_tbblue_msx_videoregisters_overlay(void);
extern void menu_debug_tsconf_tbblue_msx_videoregisters(MENU_ITEM_PARAMETERS);
extern char *menu_tsconf_layer_aux_usedunused(int value);
extern void menu_tsconf_layer_overlay_mostrar_texto(void);
extern void menu_tsconf_layer_overlay(void);
extern void menu_tbblue_layer_settings_sprites(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_layer_settings_ula(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_layer_settings_tilemap(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_layer_settings_layer_two(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_layer_reveal_ula(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_layer_reveal_layer2(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_layer_reveal_sprites(MENU_ITEM_PARAMETERS);
extern int menu_debug_tsconf_tbblue_msx_spritenav_get_total_sprites(void);
extern int menu_debug_tsconf_tbblue_msx_spritenav_get_total_height_win(void);
extern void menu_debug_tsconf_tbblue_msx_spritenav_lista_sprites(void);
extern void menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites(void);
extern void menu_debug_tsconf_tbblue_msx_spritenav(MENU_ITEM_PARAMETERS);
extern char menu_debug_tsconf_tbblue_msx_tiles_retorna_visualchar(int tnum);
extern int menu_debug_tsconf_tbblue_msx_tilenav_total_vert(void);
extern void menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles(void);
extern void menu_debug_tsconf_tbblue_msx_tilenav_draw_tiles(void);
extern void menu_debug_tsconf_tbblue_msx_tilenav_new_window(zxvision_window *ventana);
extern void menu_debug_tsconf_tbblue_msx_save_geometry(zxvision_window *ventana);
extern void menu_debug_tsconf_tbblue_msx_tilenav(MENU_ITEM_PARAMETERS);
extern void menu_uartbridge_file(MENU_ITEM_PARAMETERS);
extern void menu_uartbridge_enable(MENU_ITEM_PARAMETERS);
extern int menu_uartbridge_cond(void);
extern int menu_uartbridge_speed_cond(void);
extern void menu_uartbridge_speed(MENU_ITEM_PARAMETERS);
extern void menu_audio_beep_filter_on_rom_save(MENU_ITEM_PARAMETERS);
extern void menu_audio_beep_alter_volume(MENU_ITEM_PARAMETERS);
extern void menu_audio_beep_volume(MENU_ITEM_PARAMETERS);
extern void menu_audio_beeper (MENU_ITEM_PARAMETERS);
extern void menu_audio_beeper_real (MENU_ITEM_PARAMETERS);
extern void menu_audio_volume(MENU_ITEM_PARAMETERS);
extern void menu_aofile_insert(MENU_ITEM_PARAMETERS);
extern int menu_aofile_cond(void);
extern void menu_aofile(MENU_ITEM_PARAMETERS);
extern void menu_pcspeaker_wait_time(MENU_ITEM_PARAMETERS);
extern void menu_pcspeaker_auto_calibrate_wait_time(MENU_ITEM_PARAMETERS);
extern void menu_cpu_type(MENU_ITEM_PARAMETERS);
extern void menu_debug_registers_console(MENU_ITEM_PARAMETERS);
extern void menu_debug_shows_invalid_opcode(MENU_ITEM_PARAMETERS);
extern void menu_debug_configuration_stepover(MENU_ITEM_PARAMETERS);
extern void menu_debug_settings_show_screen(MENU_ITEM_PARAMETERS);
extern void menu_debug_settings_show_scanline(MENU_ITEM_PARAMETERS);
extern void menu_debug_verbose(MENU_ITEM_PARAMETERS);
extern void menu_debug_unnamed_console_enable(MENU_ITEM_PARAMETERS);
extern void menu_debug_verbose_always_console(MENU_ITEM_PARAMETERS);
extern void menu_debug_configuration_remoteproto(MENU_ITEM_PARAMETERS);
extern void menu_debug_configuration_remoteproto_port(MENU_ITEM_PARAMETERS);
extern void menu_hardware_debug_port(MENU_ITEM_PARAMETERS);
extern void menu_zesarux_zxi_hardware_debug_file(MENU_ITEM_PARAMETERS);
extern void menu_breakpoints_condition_behaviour(MENU_ITEM_PARAMETERS);
extern void menu_debug_settings_show_fired_breakpoint(MENU_ITEM_PARAMETERS);
extern void menu_display_rainbow(MENU_ITEM_PARAMETERS);
extern void menu_display_tbblue_store_scanlines(MENU_ITEM_PARAMETERS);
extern void menu_display_tbblue_store_scanlines_border(MENU_ITEM_PARAMETERS);
extern void menu_hardware_joystick(MENU_ITEM_PARAMETERS);
extern void menu_hardware_autofire(MENU_ITEM_PARAMETERS);
extern void menu_hardware_kempston_mouse(MENU_ITEM_PARAMETERS);
extern void menu_hardware_kempston_mouse_sensibilidad(MENU_ITEM_PARAMETERS);
extern void menu_hardware_datagear_dma(MENU_ITEM_PARAMETERS);
extern void menu_hardware_tbblue_core_version(MENU_ITEM_PARAMETERS);
extern void menu_tbblue_rtc_traps(MENU_ITEM_PARAMETERS);
extern void menu_hardware_keyboard_issue(MENU_ITEM_PARAMETERS);
extern void menu_hardware_redefine_keys_set_keys(MENU_ITEM_PARAMETERS);
extern void menu_hardware_set_f_func_action(MENU_ITEM_PARAMETERS);
extern int menu_hardware_realjoystick_cond(void);
extern void menu_hardware_realjoystick_event_button(MENU_ITEM_PARAMETERS);
extern int menu_joystick_event_list(void);
extern void menu_hardware_realjoystick_keys_button(MENU_ITEM_PARAMETERS);
extern void menu_print_text_axis(char *buffer,int button_type,int button_number);
extern void menu_hardware_realjoystick_clear_keys(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_keys(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_clear_events(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_event(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_test_reset_last_values(void);
extern void menu_hardware_realjoystick_test_fill_bars(int valor,char *string,int limite_barras);
extern void menu_hardware_realjoystick_test(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_autocalibrate(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_set_defaults(MENU_ITEM_PARAMETERS);
extern void menu_hardware_realjoystick_native(MENU_ITEM_PARAMETERS);
extern void menu_hardware_printers_zxprinter_enable(MENU_ITEM_PARAMETERS);
extern int menu_hardware_zxprinter_cond(void);
extern void menu_hardware_zxprinter_bitmapfile(MENU_ITEM_PARAMETERS);
extern void menu_hardware_zxprinter_ocrfile(MENU_ITEM_PARAMETERS);
extern void menu_hardware_tbblue_ram(MENU_ITEM_PARAMETERS);
extern int menu_hardware_advanced_input_value(int minimum,int maximum,char *texto,int *variable);
extern void menu_hardware_advanced_reload_display(void);
extern void menu_hardware_advanced_hidden_top_border(MENU_ITEM_PARAMETERS);
extern void menu_hardware_advanced_visible_top_border(MENU_ITEM_PARAMETERS);
extern void menu_hardware_advanced_visible_bottom_border(MENU_ITEM_PARAMETERS);
extern void menu_hardware_advanced_borde_izquierdo(MENU_ITEM_PARAMETERS);
extern void menu_hardware_advanced_borde_derecho(MENU_ITEM_PARAMETERS);
extern void menu_hardware_advanced_hidden_borde_derecho(MENU_ITEM_PARAMETERS);
extern void menu_ula_late_timings(MENU_ITEM_PARAMETERS);
extern void menu_ula_contend(MENU_ITEM_PARAMETERS);
extern void menu_ula_im2_slow(MENU_ITEM_PARAMETERS);
extern void menu_ula_pentagon_timing(MENU_ITEM_PARAMETERS);
extern void menu_exit_emulator(MENU_ITEM_PARAMETERS);
extern void menu_principal_salir_emulador(MENU_ITEM_PARAMETERS);

#ifdef TIMESENSORS_ENABLED

extern void menu_debug_timesensors(MENU_ITEM_PARAMETERS);
extern void menu_debug_timesensors_enable(MENU_ITEM_PARAMETERS);
extern void menu_debug_timesensors_init(MENU_ITEM_PARAMETERS);

#endif

#ifdef EMULATE_CPU_STATS

extern int cpu_stats_valor_contador_segundo_anterior;
extern zxvision_window *menu_debug_cpu_resumen_stats_overlay_window;
extern zxvision_window menu_debug_cpu_resumen_stats_ventana;

extern void menu_debug_cpu_stats_clear_disassemble_array(void);
extern void menu_debug_cpu_stats_diss_no_print(z80_byte index,z80_byte opcode,char *dumpassembler);
extern void menu_debug_cpu_stats_diss_complete_no_print (z80_byte opcode,char *buffer,z80_byte preffix1,z80_byte preffix2);
extern void menu_cpu_full_stats(unsigned int *stats_table,char *title,z80_byte preffix1,z80_byte preffix2);
extern void menu_cpu_full_stats_codsinpr(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_codpred(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_codprcb(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_codprdd(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_codprfd(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_codprddcb(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_codprfdcb(MENU_ITEM_PARAMETERS);
extern void menu_cpu_full_stats_clear(MENU_ITEM_PARAMETERS);
extern void menu_debug_cpu_stats(MENU_ITEM_PARAMETERS);
extern void menu_debug_cpu_resumen_stats_overlay(void);
extern void menu_debug_cpu_resumen_stats(MENU_ITEM_PARAMETERS);

#endif


#endif

