/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"


z80_byte tbblue_core_current_version_major=TBBLUE_CORE_DEFAULT_VERSION_MAJOR;
z80_byte tbblue_core_current_version_minor=TBBLUE_CORE_DEFAULT_VERSION_MINOR;
z80_byte tbblue_core_current_version_subminor=TBBLUE_CORE_DEFAULT_VERSION_SUBMINOR;

int tbblue_use_rtc_traps=1;
z80_byte tbblue_registers[256];
z80_byte tbblue_last_register;
z80_byte tbblue_machine_id=8;

void tbblue_set_emulator_setting_multiface(void)
{
        
        //(R/W) 0x06 (06) => Peripheral 2 setting:
  //bit 3 = Enable Multiface (1 = enabled)(0 after a PoR or Hard-reset)
        

        //de momento nada
        //return;

        multiface_type=MULTIFACE_TYPE_THREE; //Vamos a suponer este tipo
        z80_byte multisetting=1; // HACK: should be tbblue_registers[6]&8;

        if (multisetting) {
                multiface_enable();
        }
        else {
                multiface_disable();
        }
}


void tbblue_set_emulator_setting_divmmc(void)
{

/*
(W)             06 => Peripheral 2 setting, only in bootrom or config mode:
                        bit 7 = Enable turbo hotkey (0 = disabled, 1 = enabled)
                        bit 6 = DAC chip mode (0 = I2S, 1 = JAP)
                        bit 5 = Enable Lightpen  (1 = enabled)
                        bit 4 = Enable DivMMC (1 = enabled) -> divmmc automatic paging. divmmc memory is supported using manual
                */
        //z80_byte diven=tbblue_config2&4;
                                z80_byte diven=tbblue_registers[0x0a] & 0x10;
        debug_printf (VERBOSE_INFO,"Apply config.divmmc change: %s",(diven ? "enabled" : "disabled") );
        //printf ("Apply config2.divmmc change: %s\n",(diven ? "enabled" : "disabled") );

                                if (diven) {
                                        //printf ("Activando diviface automatic paging\n");
                                        diviface_allow_automatic_paging.v=1;
                                }

        //else divmmc_diviface_disable();
                                else {
                                        //printf ("Desactivando diviface automatic paging\n");
                                        diviface_allow_automatic_paging.v=0;
                                        //Y hacer un page-out si hay alguna pagina activa
                                        diviface_set_automatic(0);
                                }

}


void tbblue_set_emulator_setting_turbo(void)
{
        /*
        (R/W)   07 => Turbo mode
        bit 1-0 = Set CPU speed (soft reset = %00)

%00 = 3.5MHz
%01 = 7MHz
%10 = 14MHz
%11 = 28MHz (works since core 3.0         


        */

        z80_byte t=tbblue_registers[7] & 3;

        //printf ("Setting turbo: value %d on pc %04XH\n",t,reg_pc);                    

        if (t==0) cpu_turbo_speed=1;
        else if (t==1) cpu_turbo_speed=2;
        else if (t==2) cpu_turbo_speed=4;
        else if (t==3) cpu_turbo_speed=8;

        cpu_set_turbo_speed();
}


void tbblue_reset_common(void)
{

        tbblue_port_123b=0;
        tbblue_port_123b_layer2_offset=0;


        tbblue_registers[18]=8;
        tbblue_registers[19]=11;

        tbblue_registers[20]=TBBLUE_DEFAULT_TRANSPARENT;

        tbblue_registers[21]=0;
        tbblue_registers[22]=0;
        tbblue_registers[23]=0;

        tbblue_registers[28]=0;

        tbblue_registers[30]=0;
        tbblue_registers[31]=0;
        tbblue_registers[34]=0;
        tbblue_registers[35]=0;
        tbblue_registers[50]=0;
        tbblue_registers[51]=0;
        tbblue_registers[66]=15;
        tbblue_registers[67]=0;
        tbblue_registers[74]=0;
        tbblue_registers[75]=0xE3;

/*
(R/W) 0x4C (76) => Transparency index for the tilemap
  bits 7-4 = Reserved, must be 0
  bits 3-0 = Set the index value (0xF after reset)
        */

        tbblue_registers[76]=0xF;


        tbblue_registers[97]=0;
        tbblue_registers[98]=0;

        //Aunque no esté especificado como tal, ponemos este a 0
        /*
Bit     Function
7       1 to enable the tilemap
6       0 for 40x32, 1 for 80x32
5       1 to eliminate the attribute entry in the tilemap
4       palette select (0 = first Tilemap palette, 1 = second)
3       (core 3.0) enable "text mode"
2       Reserved, must be 0
1       1 to activate 512 tile mode (bit 0 of tile attribute is ninth bit of tile-id)
0 to use bit 0 of tile attribute as "ULA over tilemap" per-tile-selector
        */

        tbblue_registers[107]=0;


        tbblue_registers[112]=0;

        // TODO These should occur on EITHER hard or soft reset, depending upon bit 31.
        tbblue_registers[0x82] = 0xff;
        tbblue_registers[0x83] = 0xff;
        tbblue_registers[0x84] = 0xff;
        tbblue_registers[0x85] = 0xff;
        tbblue_registers[0x86] = 0xff;
        tbblue_registers[0x87] = 0xff;
        tbblue_registers[0x88] = 0xff;
        tbblue_registers[0x89] = 0xff;

        tbblue_registers[0xb8] = 0x83;
        tbblue_registers[0xb9] = 0x01;
        tbblue_registers[0xba] = 0x00;
        tbblue_registers[0xbb] = 0xcd;

        clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][0]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][1]=255;
        clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][2]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][3]=191;

        clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][0]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][1]=255;
        clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][2]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][3]=191;

        clip_windows[TBBLUE_CLIP_WINDOW_ULA][0]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_ULA][1]=255;
        clip_windows[TBBLUE_CLIP_WINDOW_ULA][2]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_ULA][3]=191;

        clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][0]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][1]=159;
        clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][2]=0;
        clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][3]=255;



        tbblue_copper_pc=0;
        
        puerto_32765 = 0x00;
        puerto_8189 = 0x00;
        tbblue_set_mmu0(0xff);
        tbblue_set_mmu1(0xff);
        tbblue_set_mmu2(0x0a);
        tbblue_set_mmu3(0x0b);
        tbblue_set_mmu4(0x04);
        tbblue_set_mmu5(0x05);
        tbblue_set_mmu6(0x00);
        tbblue_set_mmu7(0x01);
}


void tbblue_soft_reset(void)
{
        tbblue_registers[2]=1;  // last reset type: soft




 /*
 0x8C (140) => Alternate ROM
 (R/W) (hard reset = 0)
 IMMEDIATE
   bit 7 = 1 to enable alt rom
   bit 6 = 1 to make alt rom visible only during writes, otherwise replaces rom during reads
   bit 5 = 1 to lock ROM1 (48K rom)
   bit 4 = 1 to lock ROM0 (128K rom)
 AFTER SOFT RESET (copied into bits 7-4)
   bit 3 = 1 to enable alt rom
   bit 2 = 1 to make alt rom visible only during writes, otherwise replaces rom during reads
   bit 1 = 1 to lock ROM1 (48K rom)
   bit 0 = 1 to lock ROM0 (128K rom)
 The locking mechanism also applies if the alt rom is not enabled. For the +3 and zx next, if the two lock bits are not
 zero, then the corresponding rom page is locked in place. Other models use the bits to preferentially lock the corresponding
 48K rom or the 128K rom.

 */

       z80_byte reg8c_low=tbblue_registers[0x8c];

       //Moverlo a bits altos
       reg8c_low = reg8c_low << 4;

       //Quitar de origen los bits altos

       tbblue_registers[0x8c] &=0xF;

       //Y meterle los altos

       tbblue_registers[0x8c] |=reg8c_low;      
 

        tbblue_reset_common();



}


void tbblue_hard_reset(void)
{
        tbblue_registers[2]=2;  // last reset type: hard


        tbblue_registers[3]=0;
        tbblue_registers[4]=0;
        tbblue_registers[5]=1;
        tbblue_registers[6]=0;
        tbblue_registers[7]=0;
        tbblue_registers[8]=16;
        tbblue_registers[9]=0;
        tbblue_registers[0xa]=0;


        tbblue_registers[0x8c]=0;

        tbblue_reset_common();


        tbblue_reset_palette_write_state();


                tbblue_bootrom.v=1;
        //printf ("----setting bootrom to 1\n");
                tbblue_set_emulator_setting_divmmc();



        tbblue_reset_sprites();
        tbblue_reset_palettes();


}


void tbblue_set_timing_128k(void)
{
        contend_read=contend_read_128k;
        contend_read_no_mreq=contend_read_no_mreq_128k;
        contend_write_no_mreq=contend_write_no_mreq_128k;

        ula_contend_port_early=ula_contend_port_early_128k;
        ula_contend_port_late=ula_contend_port_late_128k;


        screen_testados_linea=228;
        screen_invisible_borde_superior=7;
        screen_invisible_borde_derecho=104;

        port_from_ula=port_from_ula_p2a;
        contend_pages_128k_p2a=contend_pages_p2a;

}


void tbblue_set_timing_48k(void)
{
        contend_read=contend_read_48k;
        contend_read_no_mreq=contend_read_no_mreq_48k;
        contend_write_no_mreq=contend_write_no_mreq_48k;

        ula_contend_port_early=ula_contend_port_early_48k;
        ula_contend_port_late=ula_contend_port_late_48k;

        screen_testados_linea=224;
        screen_invisible_borde_superior=8;
        screen_invisible_borde_derecho=96;

        port_from_ula=port_from_ula_48k;

        //esto no se usara...
        contend_pages_128k_p2a=contend_pages_128k;

}

void tbblue_change_timing(int timing)
{
        if (timing==0) tbblue_set_timing_48k();
        else if (timing==1) tbblue_set_timing_128k();

        screen_set_video_params_indices();
        inicializa_tabla_contend();

}


void tbblue_set_register_port(z80_byte value)
{
        tbblue_last_register=value;
}


z80_byte tbblue_get_register_port(void)
{
        return tbblue_last_register;
}


void tbblue_splash_monitor_mode(void)
{

        char buffer_mensaje[100];

        int refresco=50;

        if (tbblue_registers[5] & 4) refresco=60;

        int vga_mode=1;

        if ( (tbblue_registers[17] & 7) ==7 ) vga_mode=0;

        if (vga_mode) sprintf(buffer_mensaje,"Setting monitor VGA mode %d %d Hz",tbblue_registers[17] & 7,refresco);
        else sprintf(buffer_mensaje,"Setting monitor HDMI mode %d Hz",refresco);



        screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,buffer_mensaje);
}


void tbblue_sync_display1_reg_to_others(z80_byte value)
{
/*
(W) 0x69 (105) => DISPLAY CONTROL 1 REGISTER

Bit     Function
7       Enable the Layer 2 (alias for Layer 2 Access Port ($123B) bit 1)
6       Enable ULA shadow (bank 7) display (alias for Memory Paging Control ($7FFD) bit 3)
5-0     alias for Timex Sinclair Video Mode Control ($xxFF) bits 5:0

*/


        //123B bit 1 = Layer2 ON or OFF set=ON,
        tbblue_port_123b &=(255-2);
        if (value & 128) tbblue_port_123b |=2;

        //Bit shadow
        puerto_32765 &= (255-8);
        if (value&64) puerto_32765|=8;

        //Puerto timex
        z80_byte temp_timex_ff=timex_port_ff;
        temp_timex_ff &= (128+64); //Solo conservar bits altos
        temp_timex_ff |= (value&63); //Y del valor de entrada enviar los 6 bits bajos
        timex_port_ff=temp_timex_ff;
        //TODO: esto no genera splash de cambio de modo. Se podria llamar a set_timex_port_ff, donde este si que hace los splash,
        //pero al final sincroniza valor de timex hacia registro 105 (lo inverso de aqui), que aunque no pasaria nada, es
        //redundante
}


void tbblue_set_joystick_1_mode(void)
{

        z80_byte joystick_mode=((tbblue_registers[5] >>6) & 3 ) | ((tbblue_registers[5] >> 1) & 4) ;

        //printf("joystick mode: %d\n",joystick_mode);

        /*

        0x05 (05) => Peripheral 1 Setting
(R/W)
  bits 7:6 = Joystick 1 mode (LSB)
  bits 5:4 = Joystick 2 mode (LSB)
  bit 3 = Joystick 1 mode (MSB)
  bit 2 = 50/60 Hz mode (0 = 50Hz, 1 = 60Hz, Pentagon is always 50Hz)
  bit 1 = Joystick 2 mode (MSB)
  bit 0 = Enable scandoubler (1 = enabled)
Joystick modes:
  000 = Sinclair 2 (12345)
  001 = Kempston 1 (port 0x1F)
  010 = Cursor (56780)
  011 = Sinclair 1 (67890)
  100 = Kempston 2 (port 0x37)
  101 = MD 1 (3 or 6 button joystick port 0x1F)
  110 = MD 2 (3 or 6 button joystick port 0x37)
  111 = I/O Mode
Both joysticks are placed in I/O Mode if either is set to I/O Mode. The underlying
joystick type is not changed and reads of this register will continue to return
the last joystick type. Whether the joystick is in io mode or not is invisible
but this state can be cleared either through reset or by re-writing the register
with joystick type not equal to 111. Recovery time for a normal joystick read after
leaving I/O Mode is at most 64 scan lines.

        */

        //TODO: que es I/O Mode ???

        //Solo hacemos caso a estos:

/*
  000 = Sinclair 2 (12345)
  001 = Kempston 1 (port 0x1F)
  010 = Cursor (56780)
  011 = Sinclair 1 (67890)
  100 = Kempston 2 (port 0x37)
  101 = MD 1 (3 or 6 button joystick port 0x1F) (como si fuera kempston)
  */

 //cualquier otro, dejarlo como estaba
 //printf ("joy mode: %d\n",joystick_mode);

 switch (joystick_mode) {
         case 0:
                joystick_emulation=JOYSTICK_SINCLAIR_2;
                debug_printf(VERBOSE_DEBUG,"Setting joystick 1 emulation to Sinclair 2");
         break;

         case 1:
         case 4:
     case 5:
                joystick_emulation=JOYSTICK_KEMPSTON;
                debug_printf(VERBOSE_DEBUG,"Setting joystick 1 emulation to Kempston");
         break;

         case 2:
                joystick_emulation=JOYSTICK_CURSOR_WITH_SHIFT;
                debug_printf(VERBOSE_DEBUG,"Setting joystick 1 emulation to Cursor");
         break;

         case 3:
                joystick_emulation=JOYSTICK_SINCLAIR_1;
                debug_printf(VERBOSE_DEBUG,"Setting joystick 1 emulation to Sinclair 1");
         break;

     default:
        debug_printf(VERBOSE_DEBUG,"Unemulated joystick mode: %d",joystick_mode);
    break;

 }


}


z80_byte tbblue_uartbridge_readdata(void)
{

        return uartbridge_readdata();
}


void tbblue_uartbridge_writedata(z80_byte value)
{
 
        uartbridge_writedata(value);


}

z80_byte tbblue_uartbridge_readstatus(void)
{
        //No dispositivo abierto
        if (!uartbridge_available()) return 0;

        
        int status=chardevice_status(uartbridge_handler);
        

        z80_byte status_retorno=0;

        if (status & CHDEV_ST_RD_AVAIL_DATA) status_retorno |= TBBLUE_UART_STATUS_DATA_READY;

        return status_retorno;
}
