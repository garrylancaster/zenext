/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"


z80_int tbblue_layer_ula[TBBLUE_LAYERS_PIXEL_WIDTH];
z80_int tbblue_layer_layer2[TBBLUE_LAYERS_PIXEL_WIDTH];
z80_int tbblue_layer_sprites[TBBLUE_LAYERS_PIXEL_WIDTH];

z80_bit tbblue_force_disable_layer_ula={0};
z80_bit tbblue_force_disable_layer_tilemap={0};
z80_bit tbblue_force_disable_layer_sprites={0};
z80_bit tbblue_force_disable_layer_layer_two={0};

z80_int *p_layer_first;
z80_int *p_layer_second;
z80_int *p_layer_third;

int (*tbblue_fn_pixel_layer_transp_first)(z80_int color);
int (*tbblue_fn_pixel_layer_transp_second)(z80_int color);
int (*tbblue_fn_pixel_layer_transp_third)(z80_int color);


struct s_tbblue_priorities_names tbblue_priorities_names[8]={
	{ { "Sprites" ,  "Layer 2"  ,  "ULA&Tiles" } },
	{ { "Layer 2" ,  "Sprites"  ,  "ULA&Tiles" } },
	{ { "Sprites" ,  "ULA&Tiles"  ,  "Layer 2" } },
	{ { "Layer 2" ,  "ULA&Tiles"  ,  "Sprites" } },
	{ { "ULA&Tiles" ,  "Sprites"  ,  "Layer 2" } },
	{ { "ULA&Tiles" ,  "Layer 2"  ,  "Sprites" } },

//TODO:
	{ { "Invalid" ,  "Invalid"  ,  "Invalid" } },
	{ { "Invalid" ,  "Invalid"  ,  "Invalid" } },
};

//Retorna el texto de la capa que corresponde segun el byte de prioridad y la capa demandada en layer
//La capa de arriba del todo, es capa 0. La de en medio, la 1, etc
char *tbblue_get_string_layer_prio(int layer,z80_byte prio)
{
/*
     Reset default is 000, sprites over the Layer 2, over the ULA graphics
     000 - S L U
     001 - L S U
     010 - S U L
     011 - L U S
     100 - U S L
     101 - U L S

	 TODO:
	110 - (U|T)S(T|U)(B+L) Blending layer and Layer 2 combined, colours clamped to [0,7]
    111 - (U|T)S(T|U)(B+L-5) Blending layer and Layer 2 combined, colours clamped to [0,7]
*/

	//por si acaso. capa entre 0 y 7
	prio = prio & 7;

	//layer entre 0 y 2
	layer = layer % 3;

	return tbblue_priorities_names[prio].layers[layer];

}


z80_byte tbblue_get_layers_priorities(void)
{
	return (tbblue_registers[0x15] >> 2)&7;
}

void tbblue_set_layer_priorities(void)
{
	//Por defecto
	//sprites over the Layer 2, over the ULA graphics
	p_layer_first=tbblue_layer_sprites;
	p_layer_second=tbblue_layer_layer2;
	p_layer_third=tbblue_layer_ula;



	tbblue_fn_pixel_layer_transp_first=tbblue_si_sprite_transp_ficticio;
	tbblue_fn_pixel_layer_transp_third=tbblue_si_sprite_transp_ficticio;
	tbblue_fn_pixel_layer_transp_second=tbblue_si_sprite_transp_ficticio;

	/*
	(R/W) 0x15 (21) => Sprite and Layers system
  bit 7 - LoRes mode, 128 x 96 x 256 colours (1 = enabled)
  bits 6-5 = Reserved, must be 0
  bits 4-2 = set layers priorities:
     Reset default is 000, sprites over the Layer 2, over the ULA graphics
     000 - S L U
     001 - L S U
     010 - S U L
     011 - L U S
     100 - U S L
     101 - U L S
  bit 1 = Over border (1 = yes)(Back to 0 after a reset)
  bit 0 = Sprites visible (1 = visible)(Back to 0 after a reset)
  */
	//z80_byte prio=(tbblue_registers[0x15] >> 2)&7;
	z80_byte prio=tbblue_get_layers_priorities();

	//printf ("prio: %d\n",prio);

	switch (prio) {
		case 0:
			p_layer_first=tbblue_layer_sprites;
			p_layer_second=tbblue_layer_layer2;
			p_layer_third=tbblue_layer_ula;

		break;

		case 1:
			p_layer_first=tbblue_layer_layer2;
			p_layer_second=tbblue_layer_sprites;
			p_layer_third=tbblue_layer_ula;

		break;


		case 2:
			p_layer_first=tbblue_layer_sprites;
			p_layer_second=tbblue_layer_ula;
			p_layer_third=tbblue_layer_layer2;

		break;

		case 3:
			p_layer_first=tbblue_layer_layer2;
			p_layer_second=tbblue_layer_ula;
			p_layer_third=tbblue_layer_sprites;

		break;

		case 4:
			p_layer_first=tbblue_layer_ula;
			p_layer_second=tbblue_layer_sprites;
			p_layer_third=tbblue_layer_layer2;

		break;

		case 5:
			p_layer_first=tbblue_layer_ula;
			p_layer_second=tbblue_layer_layer2;
			p_layer_third=tbblue_layer_sprites;

		break;

		default:
			p_layer_first=tbblue_layer_sprites;
			p_layer_second=tbblue_layer_layer2;
			p_layer_third=tbblue_layer_ula;

		break;	
	}

}


int tbblue_if_ula_is_enabled(void)
{
	/*
(R/W) 0x68 (104) => ULA Control
  bit 7    = 1 to disable ULA output
  bit 6    = 0 to select the ULA colour for blending in SLU modes 6 & 7
           = 1 to select the ULA/tilemap mix for blending in SLU modes 6 & 7
  bits 5-1 = Reserved must be 0
  bit 0    = 1 to enable stencil mode when both the ULA and tilemap are enabled
            (if either are transparent the result is transparent otherwise the
             result is a logical AND of both colours)
						 */

	if (tbblue_registers[104]&128) return 0;
	else return 1;
}
