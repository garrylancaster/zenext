/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"

z80_byte *tbblue_ram_memory_pages[TBBLUE_MAX_SRAM_8KB_BLOCKS];

z80_byte tbblue_extra_512kb_blocks = 3;

void tbblue_set_ram_blocks(int memoria_kb)
{
	if (memoria_kb>=1536) tbblue_extra_512kb_blocks=3;
	else if (memoria_kb>=1024) tbblue_extra_512kb_blocks=2;
	else if (memoria_kb>=512) tbblue_extra_512kb_blocks=1;
	else tbblue_extra_512kb_blocks=0;
}

z80_byte tbblue_return_max_extra_blocks(void)
{
	return 32+tbblue_extra_512kb_blocks*64;
}

int tbblue_get_current_ram(void)
{
	return 256+8*tbblue_return_max_extra_blocks();
}


z80_byte *tbblue_fpga_rom;
z80_byte *tbblue_no_mem;

z80_byte *tbblue_memory_r[8];
z80_byte *tbblue_memory_w[8];

z80_bit tbblue_bootrom={1};

// TODO contend_pages

z80_byte tbblue_active_rom;
z80_byte tbblue_rom3;
z80_byte tbblue_alt_128;

z80_byte tbblue_rom3_visible[2];

void tbblue_init_memory_tables(void)
{
	int i,indice;

	// Boot loader and "no ram" pages come after the physical 2048K SRAM.
	tbblue_fpga_rom=&memoria_spectrum[2*1024*1024];
	tbblue_no_mem=&memoria_spectrum[2*1024*1024+8192];

	//224 Paginas RAM spectrum 512k
	for (i=0;i<TBBLUE_MAX_SRAM_8KB_BLOCKS;i++) {
		indice=TBBLUE_SRAM_ADDR_ZX_RAM+(i << TBBLUE_PAGE_8K_SHIFT);
		tbblue_ram_memory_pages[i]=&memoria_spectrum[indice];
	}
}


void tbblue_select_active_roms(void)
{
        // TODO Currently ignoring:   7. romcs expansion bus
        z80_byte alt_lock_1 = (tbblue_registers[0x8c] >> 5) & 0x01;
        z80_byte alt_lock_0 = (tbblue_registers[0x8c] >> 4) & 0x01;
        z80_byte port_1ffd_1 = (puerto_8189 >> 2) & 0x01;
        z80_byte port_1ffd_0 = (puerto_32765 >> 4) & 0x01;

        switch (tbblue_registers[0x03] & 0x07)
        {
                case 0x01:      // machine_type_48
                        tbblue_active_rom = 0;
                        tbblue_rom3 = 1;
                        tbblue_alt_128 = (!alt_lock_1) & alt_lock_0 & 0x01;
                        break;

                case 0x03:      // machine_type_p3
                        if (alt_lock_1 | alt_lock_0)
                        {
                                tbblue_active_rom = (alt_lock_1 << 1) | alt_lock_0;
                                tbblue_rom3 = alt_lock_1 & alt_lock_0;
                                tbblue_alt_128 = (!alt_lock_1) & alt_lock_0 & 0x01;
                        }
                        else
                        {
                                tbblue_active_rom = (port_1ffd_1 << 1) | port_1ffd_0;
                                tbblue_rom3 = port_1ffd_1 & port_1ffd_0;
                                tbblue_alt_128 = !port_1ffd_0;
                        }
                        break;

                default:        // machine_type_128
                        if (alt_lock_1 | alt_lock_0)
                        {
                                tbblue_active_rom = alt_lock_1;
                                tbblue_rom3 = alt_lock_1;
                                tbblue_alt_128 = !alt_lock_1;
                        }
                        else
                        {
                                tbblue_active_rom = port_1ffd_0;
                                tbblue_rom3 = port_1ffd_0;
                                tbblue_alt_128 = !port_1ffd_0;
                        }
                        break;
        }
}

z80_byte *tbblue_get_rom_write_mapping(z80_byte mmu_id)
{
        if ((tbblue_registers[0x8c] & 0xc0) == 0xc0)
        {
                // AltROM visible during writes.
                tbblue_select_active_roms();

                return &memoria_spectrum[TBBLUE_SRAM_ADDR_ALT_ROM
                        + ((tbblue_alt_128 ^ 0x1) << TBBLUE_PAGE_16K_SHIFT)
                        + (mmu_id << TBBLUE_PAGE_8K_SHIFT)];
        }
        else
        {
                return tbblue_no_mem;
        }
}


z80_byte *tbblue_get_rom_read_mapping(z80_byte mmu_id)
{
        tbblue_select_active_roms();

        if ((tbblue_registers[0x8c] & 0xc0) == 0x80)
        {
                // AltROM visible during reads.
                tbblue_rom3_visible[mmu_id] = !tbblue_alt_128;

                return &memoria_spectrum[TBBLUE_SRAM_ADDR_ALT_ROM
                        + ((tbblue_alt_128 ^ 0x1) << TBBLUE_PAGE_16K_SHIFT)
                        + (mmu_id << TBBLUE_PAGE_8K_SHIFT)];
        }
        else
        {
                tbblue_rom3_visible[mmu_id] = tbblue_rom3;

                return &memoria_spectrum[TBBLUE_SRAM_ADDR_ZX_ROM
                        + (tbblue_active_rom << TBBLUE_PAGE_16K_SHIFT)
                        + (mmu_id << TBBLUE_PAGE_8K_SHIFT)];
        }
}


void tbblue_set_paged_mmu_rom_write(z80_byte mmu_id)
{
        z80_byte ram_page_id = tbblue_registers[0x50+mmu_id];

        if (ram_page_id == 0xff)
        {
                tbblue_memory_w[mmu_id] = tbblue_get_rom_write_mapping(mmu_id);
        }
        else if (ram_page_id <= TBBLUE_MAX_RAM_PAGE_ID)
        {
                tbblue_memory_w[mmu_id] = tbblue_ram_memory_pages[ram_page_id];
        }
        else
        {
                tbblue_memory_w[mmu_id] = tbblue_no_mem;
        }
}


void tbblue_set_paged_mmu_rom_read(z80_byte mmu_id)
{
        z80_byte ram_page_id = tbblue_registers[0x50+mmu_id];

        if (ram_page_id == 0xff)
        {
                tbblue_memory_r[mmu_id] = tbblue_get_rom_read_mapping(mmu_id);
        }
        else if (ram_page_id <= TBBLUE_MAX_RAM_PAGE_ID)
        {
                tbblue_memory_r[mmu_id] = tbblue_ram_memory_pages[ram_page_id];
        }
}


void tbblue_set_paged_mmu_write(z80_byte mmu_id, z80_byte ram_page_id)
{
        if (ram_page_id <= TBBLUE_MAX_RAM_PAGE_ID)
        {
                tbblue_memory_w[mmu_id] = tbblue_ram_memory_pages[ram_page_id];
        }
        else
        {
                tbblue_memory_w[mmu_id] = tbblue_no_mem;
        }
}


void tbblue_set_paged_mmu_read(z80_byte mmu_id, z80_byte ram_page_id)
{
        if (ram_page_id <= TBBLUE_MAX_RAM_PAGE_ID)
        {
                tbblue_memory_r[mmu_id] = tbblue_ram_memory_pages[ram_page_id];
        }
}


void tbblue_set_paged_mmu_only(z80_byte mmu_id)
{
        z80_byte ram_page_id = tbblue_registers[0x50+mmu_id];
        if (ram_page_id <= TBBLUE_MAX_RAM_PAGE_ID)
        {
                tbblue_memory_r[mmu_id] = tbblue_memory_w[mmu_id] = tbblue_ram_memory_pages[ram_page_id];
        }
        else
        {
                tbblue_memory_w[mmu_id] = tbblue_no_mem;
        }
}


z80_byte tbblue_get_layer2_mapping(z80_byte mmu_id)
{
        z80_byte l2_base_16K =
                tbblue_registers[0x12 + ((tbblue_port_123b >> 3) & 0x1)]
                + tbblue_port_123b_layer2_offset;

        switch (tbblue_port_123b >> 6)
        {
                case 1: l2_base_16K++;          break;
                case 2: l2_base_16K += 2;       break;
        }

        return (l2_base_16K << 1) + mmu_id;
}


void tbblue_set_paged_mmu_or_layer2(z80_byte mmu_id)
{
        if ((tbblue_port_123b & 0xc0) == 0xc0)
        {
                // 48K layer2 mapping
                z80_byte ram_page_id = tbblue_get_layer2_mapping(mmu_id);

                if (tbblue_port_123b & 0x04)
                {
                        tbblue_set_paged_mmu_read(mmu_id + 0, ram_page_id + 0);
                        tbblue_set_paged_mmu_read(mmu_id + 1, ram_page_id + 1);
                }
                else
                {
                        tbblue_set_paged_mmu_read(mmu_id + 0, tbblue_registers[0x50 + mmu_id]);
                        tbblue_set_paged_mmu_read(mmu_id + 1, tbblue_registers[0x51 + mmu_id]);
                }

                if (tbblue_port_123b & 0x01)
                {
                        tbblue_set_paged_mmu_write(mmu_id + 0, ram_page_id + 0);
                        tbblue_set_paged_mmu_write(mmu_id + 1, ram_page_id + 1);
                }
                else
                {
                        tbblue_set_paged_mmu_write(mmu_id + 0, tbblue_registers[0x50 + mmu_id]);
                        tbblue_set_paged_mmu_write(mmu_id + 1, tbblue_registers[0x51 + mmu_id]);
                }
        }
        else
        {
                tbblue_set_paged_mmu_only(mmu_id);
        }
}


/*      -- memory decode order
        --
        -- 0-16k:
        --   1. bootrom
        --   2. machine config mapping
        --   3. multiface
        --   4. divmmc
        --   5. layer 2 mapping
        --   6. mmu
        --   7. romcs expansion bus
        --   8. rom
*/


void tbblue_touch_mmu01(void)
{
        tbblue_rom3_visible[0] = tbblue_rom3_visible[1] = 0;

        if (tbblue_bootrom.v)
        {
                tbblue_memory_r[0] =
                tbblue_memory_r[1] = tbblue_fpga_rom;

                tbblue_memory_w[0] =
                tbblue_memory_w[1] = tbblue_no_mem;
        }
        else if ((tbblue_registers[0x03] & 0x07) == 0x00)
        {
                z80_byte config_page_id = (tbblue_registers[0x04] & 0x7f) << 1;
                tbblue_memory_r[0] =
                tbblue_memory_w[0] = &memoria_spectrum[config_page_id << TBBLUE_PAGE_8K_SHIFT];

                tbblue_memory_r[1] =
                tbblue_memory_w[1] = &memoria_spectrum[(config_page_id + 1) << TBBLUE_PAGE_8K_SHIFT];
        }
        else if (multiface_switched_on.v)
        {
                tbblue_memory_r[0] = &memoria_spectrum[TBBLUE_SRAM_ADDR_MULTIFACE_ROM];
                tbblue_memory_w[0] = tbblue_no_mem;

                tbblue_memory_r[1] =
                tbblue_memory_w[1] = &memoria_spectrum[TBBLUE_SRAM_ADDR_MULTIFACE_RAM];
        }
        else if (diviface_paginacion_automatica_activa.v || (diviface_control_register & 0x80))
        {
                int divmmc_ram_addr = TBBLUE_SRAM_ADDR_DIVMMC_RAM
                        + ((diviface_control_register & 0x0f) << TBBLUE_PAGE_8K_SHIFT);

                if ((diviface_control_register & 0xc0) != 0x40)
                {
                        // CONMEM=1, MAPRAM=don't care
                        // CONMEM=0, MAPRAM=0
                        tbblue_memory_r[0] = &memoria_spectrum[TBBLUE_SRAM_ADDR_DIVMMC_ROM];
                        tbblue_memory_w[0] = tbblue_no_mem;

                        tbblue_memory_r[1] =
                        tbblue_memory_w[1] = &memoria_spectrum[divmmc_ram_addr];
                }
                else if (diviface_control_register & 0x40)
                {
                        // CONMEM=0, MAPRAM=1
                        int divmmc_ram3_addr = TBBLUE_SRAM_ADDR_DIVMMC_RAM
                                + (3 << TBBLUE_PAGE_8K_SHIFT);

                        tbblue_memory_r[0] = &memoria_spectrum[divmmc_ram3_addr];
                        tbblue_memory_w[0] = tbblue_no_mem;

                        tbblue_memory_r[1] = &memoria_spectrum[divmmc_ram_addr];

                        // TODO Velesoft's divIDE memory page says bank3 is read-only
                        //      in both slots, but don't think this is the case
                        //      for Next.
                        /*
                        if (divmmc_ram_addr == divmmc_ram3_addr)
                        {
                                tbblue_memory_w[1] = tbblue_no_mem;
                        }
                        else */
                        {
                                tbblue_memory_w[1] = &memoria_spectrum[divmmc_ram_addr];
                        }
                }
        }
        else if (tbblue_port_123b & 0x05)
        {
                // Layer2 mapping
                z80_byte ram_page_id = tbblue_get_layer2_mapping(0);

                if (tbblue_port_123b & 0x04)
                {
                        tbblue_set_paged_mmu_read(0, ram_page_id + 0);
                        tbblue_set_paged_mmu_read(1, ram_page_id + 1);
                }
                else
                {
                        tbblue_set_paged_mmu_rom_read(0);
                        tbblue_set_paged_mmu_rom_read(1);
                }

                if (tbblue_port_123b & 0x01)
                {
                        tbblue_set_paged_mmu_write(0, ram_page_id + 0);
                        tbblue_set_paged_mmu_write(1, ram_page_id + 1);
                }
                else
                {
                        tbblue_set_paged_mmu_rom_write(0);
                        tbblue_set_paged_mmu_rom_write(1);
                }
        }
        else
        {
                // ROM.
                tbblue_set_paged_mmu_rom_read(0);
                tbblue_set_paged_mmu_rom_read(1);
                tbblue_set_paged_mmu_rom_write(0);
                tbblue_set_paged_mmu_rom_write(1);
        }

        debug_paginas_memoria_mapeadas[0] = (tbblue_memory_r[0] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
        debug_paginas_memoria_mapeadas[1] = (tbblue_memory_r[1] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


/*      -- memory decode order
        --
        -- 16k-48k:
        --   1. layer 2 mapping
        --   2. mmu
*/


void tbblue_touch_mmu2(void)
{
        tbblue_set_paged_mmu_or_layer2(2);

        debug_paginas_memoria_mapeadas[2] = (tbblue_memory_r[2] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


void tbblue_touch_mmu3(void)
{
        tbblue_set_paged_mmu_or_layer2(3);

        debug_paginas_memoria_mapeadas[3] = (tbblue_memory_r[3] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


void tbblue_touch_mmu4(void)
{
        tbblue_set_paged_mmu_or_layer2(4);

        debug_paginas_memoria_mapeadas[4] = (tbblue_memory_r[4] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


void tbblue_touch_mmu5(void)
{
        tbblue_set_paged_mmu_or_layer2(5);

        debug_paginas_memoria_mapeadas[5] = (tbblue_memory_r[5] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


/*      -- memory decode order
        --
        -- 48k-64k:
        --   1. mmu
*/


void tbblue_touch_mmu6(void)
{
        tbblue_set_paged_mmu_only(6);

        debug_paginas_memoria_mapeadas[6] = (tbblue_memory_r[6] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


void tbblue_touch_mmu7(void)
{
        tbblue_set_paged_mmu_only(7);

        debug_paginas_memoria_mapeadas[7] = (tbblue_memory_r[7] - memoria_spectrum)
                                                >> TBBLUE_PAGE_8K_SHIFT;
}


void tbblue_touch_mmu012345(void)
{
        tbblue_touch_mmu01();
        tbblue_touch_mmu2();
        tbblue_touch_mmu3();
        tbblue_touch_mmu4();
        tbblue_touch_mmu5();
}


void tbblue_set_mmu0(z80_byte value)
{
        tbblue_registers[0x50] = value;
        tbblue_touch_mmu01();
}


void tbblue_set_mmu1(z80_byte value)
{
        tbblue_registers[0x51] = value;
        tbblue_touch_mmu01();
}


void tbblue_set_mmu2(z80_byte value)
{
        tbblue_registers[0x52] = value;
        tbblue_touch_mmu2();
}


void tbblue_set_mmu3(z80_byte value)
{
        tbblue_registers[0x53] = value;
        tbblue_touch_mmu3();
}


void tbblue_set_mmu4(z80_byte value)
{
        tbblue_registers[0x54] = value;
        tbblue_touch_mmu4();
}


void tbblue_set_mmu5(z80_byte value)
{
        tbblue_registers[0x55] = value;
        tbblue_touch_mmu5();
}


void tbblue_set_mmu6(z80_byte value)
{
        tbblue_registers[0x56] = value;
        tbblue_touch_mmu6();
}


void tbblue_set_mmu7(z80_byte value)
{
        tbblue_registers[0x57] = value;
        tbblue_touch_mmu7();
}


void tbblue_update_memory_ports(z80_byte new_1ffd, z80_byte new_7ffd, z80_byte update_mmu67/*, z80_byte new_dffd*/)
{
        z80_byte old_1ffd = puerto_8189;

        // Update port values.
        puerto_8189 = new_1ffd;
        puerto_32765 = new_7ffd;

        // TODO Doesn't work in different memory mapping modes (reg 0x8f)
        if (puerto_8189 & 0x01)
        {
                // Special mapping now in force.
                switch ((puerto_8189 >> 1) & 0x03)
                {
                        case 0:
                                tbblue_set_mmu0(0*2);
                                tbblue_set_mmu1(0*2+1);
                                tbblue_set_mmu2(1*2);
                                tbblue_set_mmu3(1*2+1);
                                tbblue_set_mmu4(2*2);
                                tbblue_set_mmu5(2*2+1);
                                tbblue_set_mmu6(3*2);
                                tbblue_set_mmu7(3*2+1);
                                break;

                        case 1:
                                tbblue_set_mmu0(4*2);
                                tbblue_set_mmu1(4*2+1);
                                tbblue_set_mmu2(5*2);
                                tbblue_set_mmu3(5*2+1);
                                tbblue_set_mmu4(6*2);
                                tbblue_set_mmu5(6*2+1);
                                tbblue_set_mmu6(7*2);
                                tbblue_set_mmu7(7*2+1);
                                break;

                        case 2:
                                tbblue_set_mmu0(4*2);
                                tbblue_set_mmu1(4*2+1);
                                tbblue_set_mmu2(5*2);
                                tbblue_set_mmu3(5*2+1);
                                tbblue_set_mmu4(6*2);
                                tbblue_set_mmu5(6*2+1);
                                tbblue_set_mmu6(3*2);
                                tbblue_set_mmu7(3*2+1);
                                break;

                        case 3:
                                tbblue_set_mmu0(4*2);
                                tbblue_set_mmu1(4*2+1);
                                tbblue_set_mmu2(7*2);
                                tbblue_set_mmu3(7*2+1);
                                tbblue_set_mmu4(6*2);
                                tbblue_set_mmu5(6*2+1);
                                tbblue_set_mmu6(3*2);
                                tbblue_set_mmu7(3*2+1);
                                break;
                }
        }
        else if ((old_1ffd ^ puerto_8189) & 0x01)
        {
                // Changed from special to standard mapping.
                tbblue_set_mmu0(0xff);
                tbblue_set_mmu1(0xff);
                tbblue_set_mmu2(0x0a);
                tbblue_set_mmu3(0x0b);
                tbblue_set_mmu4(0x04);
                tbblue_set_mmu5(0x05);
                tbblue_set_mmu6((puerto_32765 & 0x07) << 1);
                tbblue_set_mmu7(((puerto_32765 & 0x07) << 1) + 1);
        }
        else
        {
                // Standard mapping in force.
                tbblue_set_mmu0(0xff);
                tbblue_set_mmu1(0xff);

                if (update_mmu67)
                {
                        tbblue_set_mmu6((puerto_32765 & 0x07) << 1);
                        tbblue_set_mmu7(((puerto_32765 & 0x07) << 1) + 1);
                }
        }

        // Update NextReg 0x69 (Display Control 1).
        // Bit 6 of the nextreg mirrors bit 3 of port 7ffd.
        tbblue_registers[0x69] &= 0xbf;
        if (puerto_32765 & 0x08)
        {
                tbblue_registers[0x69] |= 0x40;
        }

        // Update NextReg 0x08 (Peripheral 3).
        // Bit 7 of the nextreg mirror-inverts bit 5 of port 7ffd.
        tbblue_registers[0x08] &= 0x7f;
        if ((puerto_32765 & 0x20) == 0)
        {
                tbblue_registers[0x08] |= 0x80;
        }
}


void tbblue_out_port_8189(z80_byte value)
{
        if ((puerto_32765 & 0x20) || ((tbblue_registers[0x82] & 0x08) == 0))
        {
                // Paging locked or port disabled.
                return;
        }

        tbblue_update_memory_ports(value, puerto_32765, 0);
}


void tbblue_out_port_32765(z80_byte value)
{
        if ((puerto_32765 & 0x20) || ((tbblue_registers[0x82] & 0x02) == 0))
        {
                // Paging locked or port disabled.
                return;
        }

        tbblue_update_memory_ports(puerto_8189, value, 1);
}


void poke_byte_no_time_tbblue(z80_int dir,z80_byte valor)
{

#ifdef EMULATE_VISUALMEM
        set_visualmembuffer(dir);
#endif

        z80_byte *segment_base = tbblue_memory_w[dir >> TBBLUE_PAGE_8K_SHIFT];
        segment_base[dir & TBBLUE_PAGE_8K_MASK] = valor;
}

void poke_byte_tbblue(z80_int dir,z80_byte valor)
{
#ifdef EMULATE_CONTEND
        int segmento = dir >> TBBLUE_PAGE_16K_SHIFT;

        if (contend_pages_actual[segmento]) {
                t_estados += contend_table[ t_estados ];
        }
#endif

        t_estados += 3;

        poke_byte_no_time_tbblue(dir,valor);
}



z80_byte peek_byte_no_time_tbblue(z80_int dir)
{
#ifdef EMULATE_VISUALMEM
        set_visualmemreadbuffer(dir);
#endif

        z80_byte *segment_base = tbblue_memory_r[dir >> TBBLUE_PAGE_8K_SHIFT];
        return segment_base[dir & TBBLUE_PAGE_8K_MASK];
}


z80_byte peek_byte_tbblue(z80_int dir)
{
#ifdef EMULATE_CONTEND
        int segmento = dir >> TBBLUE_PAGE_16K_SHIFT;

        if (contend_pages_actual[segmento]) {
                t_estados += contend_table[ t_estados ];
        }
#endif

        t_estados += 3;

        return peek_byte_no_time_tbblue(dir);
}

