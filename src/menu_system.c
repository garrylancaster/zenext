/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>
#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <errno.h>

#include "audio.h"
#include "autoselectoptions.h"
#include "ay38912.h"
#include "charset.h"
#include "debug.h"
#include "joystick.h"
#include "menu_system.h"
#include "menu.h"
#include "menu_items.h"
#include "network.h"
#include "realjoystick.h"
#include "remote.h"
#include "screen.h"
#include "settings.h"
#include "tbblue.h"
#include "timer.h"
#include "ula.h"


#if defined(__APPLE__)
	#include <sys/syslimits.h>

	#include <sys/resource.h>

#endif

#include "compileoptions.h"


#ifdef COMPILE_XWINDOWS
	#include "scrxwindows.h"
#endif

#define MAX_TEXTO_BROWSER (MAX_TEXTO_GENERIC_MESSAGE-1024)


int menu_overlay_activo=0;
int menu_gui_zoom=1;
int menu_char_width=8;
int menu_last_cpu_use=0;
int menu_footer=1;
int menu_multitarea=1;
int menu_abierto=0;
int menu_speech_tecla_pulsada=0;

z80_bit menu_limit_menu_open={0};
z80_bit menu_filesel_hide_dirs={0};
z80_bit menu_desactivado={0};
z80_bit menu_desactivado_andexit={0};
z80_bit menu_desactivado_file_utilities={0};
z80_bit menu_hide_vertical_percentaje_bar={0};
z80_bit menu_hide_minimize_button={0};
z80_bit menu_hide_close_button={0};
z80_bit menu_invert_mouse_scroll={0};
z80_bit menu_event_open_menu={0};
z80_bit menu_was_open_by_left_mouse_button={0};

int menu_recent_files_opcion_seleccionada=0;

void (*menu_overlay_function)(void);
overlay_screen overlay_screen_array[OVERLAY_SCREEN_MAX_WIDTH*OVERLAY_SCREEN_MAX_HEIGTH];
overlay_screen footer_screen_array[WINDOW_FOOTER_COLUMNS*WINDOW_FOOTER_LINES];
int overlay_usado_screen_array[OVERLAY_SCREEN_MAX_WIDTH*OVERLAY_SCREEN_MAX_HEIGTH];
int overlay_visible_when_menu_closed=0;

int if_pending_error_message=0;
char pending_error_message[1024];

int salir_todos_menus;

char *esc_key_message="ESC";
char *openmenu_key_message="F5/Button";

z80_bit menu_button_exit_emulator={0};
z80_bit menu_event_drag_drop={0};
z80_bit menu_event_new_version_show_changes={0};
z80_bit menu_event_new_update={0};
z80_bit menu_event_remote_protocol_enterstep={0};
z80_bit menu_button_f_function={0};
z80_bit menu_button_smartload={0};

int mouse_is_dragging=0;
int window_is_being_moved=0;
int window_is_being_resized=0;
int window_mouse_x_before_move=0;
int window_mouse_y_before_move=0;

int last_x_mouse_clicked=0;
int last_y_mouse_clicked=0;
int mouse_is_clicking=0;
int menu_mouse_left_double_click_counter=0;
int menu_mouse_left_double_click_counter_initial=0;

int mouse_is_double_clicking=0;

int menu_button_f_function_index;
int menu_button_f_function_action=0;

z80_bit force_confirm_yes={0};
z80_bit mouse_menu_disabled={0};
z80_bit menu_pressed_open_menu_while_in_menu={0};

z80_bit direct_menus_button_pressed={0};
int direct_menus_button_pressed_which=0;

void menu_dibuja_cuadrado(int x1,int y1,int x2,int y2,int color);
void menu_desactiva_cuadrado(void);
void menu_establece_cuadrado(int x1,int y1,int x2,int y2,int color);
void menu_util_cut_line_at_spaces(int posicion_corte, char *texto,char *linea1, char *linea2);
void menu_espera_tecla_timeout_tooltip(void);
z80_byte menu_da_todas_teclas(void);
int menu_simple_two_choices(char *texto_ventana,char *texto_interior,char *opcion1,char *opcion2);
void menu_file_viewer_read_file(char *title,char *file_name);
void menu_file_viewer_read_text_file(char *title,char *file_name);

int cuadrado_activo=0;
int cuadrado_x1,cuadrado_y1,cuadrado_x2,cuadrado_y2,cuadrado_color;

int cuadrado_activo_resize=0;
int ventana_activa_tipo_zxvision=0;

int ventana_es_background=0;
int draw_bateria_contador=0;
int draw_cpu_use=0;
int draw_cpu_temp=0;
int draw_fps=0;

z80_byte *menu_clipboard_pointer=NULL;
int menu_clipboard_size=0;

int f_functions;

z80_bit menu_writing_inverse_color={0};
z80_bit menu_force_writing_inverse_color={0};
int menu_escribe_linea_startx=1;
z80_bit menu_disable_special_chars={0};

int colores_franja_speccy_brillo[]={2+8,6+8,4+8,5+8};
int colores_franja_speccy_oscuro[]={2,6,4,5};

int estilo_gui_activo=0;

estilos_gui definiciones_estilos_gui[ESTILOS_GUI]={
	{0,"ZEsarUX",7+8,0,
		0,1,1,0, 		//No mostrar cursor,mostrar recuadro,mostrar rainbow
		5+8,0, 		//Colores para opcion seleccionada
		7+8,2,7,2, 	//Colores para opcion no disponible

		0,7+8,        	//Colores para el titulo y linea recuadro ventana
		7+8,0,        	//Colores para el titulo y linea recuadro ventana inactiva

		1,		//Color waveform
		7,		//Color para zona no usada en visualmem
		2,7+8,		//Color para opcion marcada
		'*',
		2, //color de aviso
		colores_franja_speccy_brillo,colores_franja_speccy_oscuro
		},
};



int current_win_x,current_win_y,current_win_ancho,current_win_alto;
int ventana_tipo_activa=1;


struct s_filesel_item{
	char d_name[PATH_MAX];

        struct s_filesel_item *next;
};

typedef struct s_filesel_item filesel_item;

int filesel_total_items;
filesel_item *primer_filesel_item;

int filesel_linea_seleccionada;
int filesel_archivo_seleccionado;
int filesel_zona_pantalla;
char filesel_nombre_archivo_seleccionado[PATH_MAX];
z80_bit menu_filesel_show_utils={0};
z80_bit menu_filesel_show_previews={1};
int filesel_no_cabe_todo;
int filesel_total_archivos;
int filesel_porcentaje_visible;


int menu_tooltip_counter;
int menu_window_splash_counter;
int menu_window_splash_counter_ms;
z80_bit tooltip_enabled;

char *string_config_key_aid_startup=NULL;
int realjoystick_detected_startup=0;

void menu_filesel_chdir(char *dir);
void menu_change_audio_driver(MENU_ITEM_PARAMETERS);

z80_bit menu_splash_text_active;
int menu_splash_segundos=0;
void menu_simple_ventana(char *titulo,char *texto);
char **filesel_filtros;
int menu_avisa_si_extension_no_habitual(char *filtros[],char *archivo);
char **filesel_filtros_iniciales;
char *filtros_todos_archivos[2];
char menu_filesel_last_directory_seen[PATH_MAX];
char last_files_used_array[MAX_LAST_FILESUSED][PATH_MAX];
char filesel_directorio_inicial[PATH_MAX];


int menu_define_key_function(int tecla,char *funcion)
{
	if (tecla<1 || tecla>MAX_F_FUNCTIONS_KEYS) return 1;

	//Buscar en todos los strings de funciones cual es

	int i;

	for (i=0;i<MAX_F_FUNCTIONS;i++) {
		if (!strcasecmp(funcion,defined_f_functions_array[i].texto_funcion)) {
			enum defined_f_function_ids id=defined_f_functions_array[i].id_funcion;
			defined_f_functions_keys_array[tecla-1]=id;
			return 0;
		}
	}

	return 1;
}


void menu_chdir_sharedfiles(void)
{

	//cambia a los dos directorios. se quedara en el ultimo que exista
	debug_printf(VERBOSE_INFO,"Trying ../Resources");
	menu_filesel_chdir("../Resources");

	char installshare[PATH_MAX];
	sprintf (installshare,"%s/%s",INSTALL_PREFIX,"/share/zesarux/");
	debug_printf(VERBOSE_INFO,"Trying %s",installshare);
	menu_filesel_chdir(installshare);


}


//retorna dentro de un array de N teclas, la tecla pulsada
char menu_get_key_array_n_teclas(z80_byte valor_puerto,char *array_teclas,int teclas)
{

        int i;
        for (i=0;i<teclas;i++) {
                if ((valor_puerto&1)==0) return *array_teclas;
                valor_puerto=valor_puerto >> 1;
                array_teclas++;
        }

        return 0;

}



//retorna dentro de un array de 5 teclas, la tecla pulsada
char menu_get_key_array(z80_byte valor_puerto,char *array_teclas)
{

	return	menu_get_key_array_n_teclas(valor_puerto,array_teclas,5);

}

//funcion que retorna la tecla pulsada, solo tener en cuenta caracteres y numeros, sin modificador (mayus, etc)
//y por tanto solo 1 tecla a la vez

/*
z80_byte puerto_65278=255; //    db    		 255  ; V    C    X    Z    Sh    ;0
z80_byte puerto_65022=255; //    db    		 255  ; G    F    D    S    A     ;1
z80_byte puerto_64510=255; //    db              255  ; T    R    E    W    Q     ;2
z80_byte puerto_63486=255; //    db              255  ; 5    4    3    2    1     ;3
z80_byte puerto_61438=255; //    db              255  ; 6    7    8    9    0     ;4
z80_byte puerto_57342=255; //    db              255  ; Y    U    I    O    P     ;5
z80_byte puerto_49150=255; //    db              255  ; H    J    K    L    Enter ;6
z80_byte puerto_32766=255; //    db              255  ; B    N    M    Simb Space ;7

//puertos especiales no presentes en spectrum
z80_byte puerto_especial1=255; //   ;  .  .  .  . ESC ;
z80_byte puerto_especial2=255; //   F5 F4 F3 F2 F1
z80_byte puerto_especial3=255; //  F10 F9 F8 F7 F6
z80_byte puerto_especial4=255; //  F15 F14 F13 F12 F11
*/

static char menu_array_keys_65022[]="asdfg";
static char menu_array_keys_64510[]="qwert";
static char menu_array_keys_63486[]="12345";
static char menu_array_keys_61438[]="09876";
static char menu_array_keys_57342[]="poiuy";
static char menu_array_keys_49150[]="\x0dlkjh";

//arrays especiales
static char menu_array_keys_65278[]="zxcv";
static char menu_array_keys_32766[]="mnb";


/*
valores de teclas especiales:
2  ESC
3  Tecla de background
8  cursor left
9  cursor right
10 cursor down
11 cursor up
12 Delete o joystick left
13 Enter o joystick fire
15 SYM+MAY(TAB)
24 PgUp
25 PgDn

Joystick izquierda funcionara como Delete, no como cursor left. Resto de direcciones de joystick (up, down, right) se mapean como cursores

*/

int menu_if_pressed_background_button(void)
{
	//Si pulsada tecla background

	//Si se pulsa tecla F que no es default
	if (menu_button_f_function.v && menu_button_f_function_index>=0) {

		//printf ("Pulsada alguna tecla de funcion\n");

		//Estas variables solo se activan cuando   //Abrir menu si funcion no es defecto y no es background window
  		//if (accion!=F_FUNCION_DEFAULT && accion!=F_FUNCION_BACKGROUND_WINDOW) {

		int indice=menu_button_f_function_index;

		//Si accion es backgroundwindow
		enum defined_f_function_ids accion=defined_f_functions_keys_array[indice];
		{

			//Si tecla F6, es default, retornar ok si es Default
			if (indice==6-1 && accion==F_FUNCION_DEFAULT) {
				//liberamos indicador de tecla de funcion
				menu_button_f_function.v=0;
				//printf ("Es F6 por defecto\n");
				return 1;
			}

			//liberamos indicador de tecla de funcion si funcion es nothing
			if (accion==F_FUNCION_NOTHING) menu_button_f_function.v=0;			

			return 0;
		}

	}

	//Si es F6 por default
	if ((puerto_especial3&1)==0) {
		//printf ("Pulsada F6\n");
		//sleep(1);

		//Ver si funcion F6 no esta asignada 
		int indice=6-1;
		enum defined_f_function_ids accion=defined_f_functions_keys_array[indice];

		if (accion==F_FUNCION_DEFAULT) {
				//liberamos indicador de tecla de funcion
				menu_button_f_function.v=0;
				//printf ("Es F6 por defecto\n");
				return 1;
		}

		return 0;
	}


	return 0;
}


int menu_if_pressed_menu_button(void)
{
	//Si pulsada tecla menu

	//Si se pulsa tecla F que no es default
	if (menu_button_f_function.v && menu_button_f_function_index>=0) {

		//Estas variables solo se activan cuando   //Abrir menu si funcion no es defecto y no es background window
  		//if (accion!=F_FUNCION_DEFAULT && accion!=F_FUNCION_BACKGROUND_WINDOW) {

		int indice=menu_button_f_function_index;

		//Si accion es openmenu
		enum defined_f_function_ids accion=defined_f_functions_keys_array[indice];
		if (accion==F_FUNCION_OPENMENU) {
			//liberamos esa tecla
			menu_button_f_function.v=0;
			printf ("Pulsada tecla F abrir menu\n");
			//sleep(1);
			return 1;
		}


		else return 0;

	}

	//Sera tecla F5 por defecto, ya que no se ha pulsado tecla con no default
	if ((puerto_especial2&16)==0) {
		//printf ("Pulsada F5 por defecto\n");
		//sleep(1);
		return 1;
	}


	return 0;
}

z80_byte menu_get_pressed_key_no_modifier(void)
{
	z80_byte tecla;

	


	//ESC significa Shift+Space en ZX-Uno y tambien ESC puerto_especial para menu.
	//Por tanto si se pulsa ESC, hay que leer como tal ESC antes que el resto de teclas (Espacio o Shift)
	if ((puerto_especial1&1)==0) return 2;

	//if (menu_pressed_background_key() && menu_allow_background_windows) return 3; //Tecla background F6
	if (menu_if_pressed_background_button() && menu_allow_background_windows) return 3; //Tecla background F6


	//Si menu esta abierto y pulsamos de nuevo la tecla de menu, cerrar todas ventanas y reabrir menu
	//No acabo de tener claro que este sea el mejor sitio para comprobar esto... o si?
	if (menu_if_pressed_menu_button()) {
	//if ((puerto_especial2&16)==0) {
		//printf ("Pulsada tecla abrir menu\n");
		//sleep(1);
		menu_pressed_open_menu_while_in_menu.v=1;
		/*
		-si no se permite background, cerrar todos menus abiertos y volver a abrir el menu principal
-si se permite background:
—si ventana activa se puede enviar a background, enviarla a background
—si ventana activa no permite enviar a background, cerrarla
y luego en cualquiera de los dos casos, abrir el menu principal

las condiciones de "ventana activa se puede enviar a background o no" son comunes de cuando se pulsa en otra ventana. hacer función común??

	Estas decisiones son parecidas en casos:
		*/
	salir_todos_menus=1;

		if (!menu_allow_background_windows) {
			//Temp retornar escape
			return 2; //Escape
		}

		else {
			                                //Si la ventana activa permite ir a background, mandarla a background
                                if (zxvision_current_window->can_be_backgrounded) {
                                        return 3; //Tecla background F6
                                }

                                //Si la ventana activa no permite ir a background, cerrarla
                                else {
                                        return 2; //Escape
                                }
		}
	}

	tecla=menu_get_key_array(puerto_65022,menu_array_keys_65022);
	if (tecla) return tecla;

	tecla=menu_get_key_array(puerto_64510,menu_array_keys_64510);
	if (tecla) return tecla;

	tecla=menu_get_key_array(puerto_63486,menu_array_keys_63486);
	if (tecla) return tecla;

	tecla=menu_get_key_array(puerto_61438,menu_array_keys_61438);
	if (tecla) return tecla;

	tecla=menu_get_key_array(puerto_57342,menu_array_keys_57342);
	if (tecla) return tecla;

	tecla=menu_get_key_array(puerto_49150,menu_array_keys_49150);
	if (tecla) return tecla;

        tecla=menu_get_key_array_n_teclas(puerto_65278>>1,menu_array_keys_65278,4);
        if (tecla) return tecla;

        tecla=menu_get_key_array_n_teclas(puerto_32766>>2,menu_array_keys_32766,3);
        if (tecla) return tecla;

	//Y espacio
	if ((puerto_32766&1)==0) return ' ';

	//PgUp
	if ((puerto_especial1&2)==0) return 24;

	//PgDn
	if ((puerto_especial1&4)==0) return 25;



	return 0;
}




z80_bit menu_symshift={0};
z80_bit menu_capshift={0};
z80_bit menu_backspace={0};
z80_bit menu_tab={0};

//devuelve tecla pulsada teniendo en cuenta mayus, sym shift
z80_byte menu_get_pressed_key(void)
{

	//Ver tambien eventos de mouse de zxvision
	//int pulsado_boton_cerrar=
	zxvision_handle_mouse_events(zxvision_current_window);

	if (mouse_pressed_close_window) {
		return 2; //Como ESC
	}

	if (mouse_pressed_background_window) {
		//printf ("pulsado background en menu_get_pressed_key\n");
		//sleep(5);		
		return 3; //Como F6 background
	}	

	if (mouse_pressed_hotkey_window) {
		mouse_pressed_hotkey_window=0;
		//printf ("Retornamos hoykey %c desde menu_get_pressed_key\n",mouse_pressed_hotkey_window_key);
		return mouse_pressed_hotkey_window_key;
	}

	z80_byte tecla;

	//primero joystick
	if (puerto_especial_joystick) {
		//z80_byte puerto_especial_joystick=0; //Fire Up Down Left Right
		if ((puerto_especial_joystick&1)) return 9;

		if ((puerto_especial_joystick&2)) return 8;

		//left joystick hace delete en menu.NO
		//if ((puerto_especial_joystick&2)) return 12;

		if ((puerto_especial_joystick&4)) return 10;
		if ((puerto_especial_joystick&8)) return 11;
//8  cursor left
//9  cursor right
//10 cursor down
//11 cursor up

		//Fire igual que enter
		if ((puerto_especial_joystick&16)) return 13;
	}

	

	if (menu_tab.v) {
		//printf ("Pulsado TAB\n");
		return 15;
	}

	if (menu_backspace.v) return 12;


	tecla=menu_get_pressed_key_no_modifier();



	if (tecla==0) return 0;


	//ver si hay algun modificador

	//mayus

//z80_byte puerto_65278=255; //    db              255  ; V    C    X    Z    Sh    ;0
	//if ( (puerto_65278&1)==0) {
	if (menu_capshift.v) {

	

		//si son letras, ponerlas en mayusculas
		if (tecla>='a' && tecla<='z') {
			tecla=tecla-('a'-'A');
			return tecla;
		}
		//seran numeros


		switch (tecla) {
			case '0':
				//delete
				return 12;
			break;

			
		}

	}

	//sym shift
	//else if ( (puerto_32766&2)==0) {
	else if (menu_symshift.v) {
		//ver casos concretos
		switch (tecla) {

			case 'z':
				return ':';
			break;

			case 'x':
				return 96; //Libra
			break;

			case 'c':
				return '?';
			break;

			case 'v':
				return '/';
			break;			

			case 'b':
				return '*';
			break;	

			case 'n':
				return ',';
			break;			

			case 'm':
				return '.';
			break;	

			case 'a':
				return '~'; //Aunque esta sale con ext+symbol
			break;

			case 's':
				return '|'; //Aunque esta sale con ext+symbol
			break;			

			case 'd':
				return '\\'; //Aunque esta sale con ext+symbol
			break;

			case 'f':
				return '{'; //Aunque esta sale con ext+symbol
			break;

			case 'g':
				return '}'; //Aunque esta sale con ext+symbol
			break;		

			case 'h':
				return 94; //Símbolo exponente/circunflejo
			break;						

			case 'j':
				return '-';
			break;

			case 'k':
				return '+';
			break;

			case 'l':
				return '=';
			break;			



			case 'r':
				return '<';
			break;

			case 't':
				return '>';
			break;

			case 'y':
				return '[';
			break;

			case 'u':
				return ']';
			break;	

			//Faltaria el (C) del ext+sym+p. Se podria mapear a sym+I, pero esto genera el codigo 127,

			case 'o':
				return ';';
			break;

			case 'p':
				return '"';
			break;	



			case '1':
				return '!';
			break;

			case '2':
				return '@';
			break;

			case '3':
				return '#';
			break;

			case '4':
				return '$';
			break;

			case '5':
				return '%';
			break;

			case '6':
				return '&';
			break;

			case '7':
				return '\'';
			break;

			case '8':
				return '(';
			break;

			case '9':
				return ')';
			break;

			case '0':
				return '_';
			break;




			//no hace falta esta tecla. asi tambien evitamos que alguien la use en nombre de
			//archivo pensando que se puede introducir un filtro tipo *.tap, etc.
			//case 'b':
			//	return '*';
			//break;

		}
	}


	return tecla;

}

void menu_fire_event_open_menu(void)
{
	//printf ("Ejecutar menu_fire_event_open_menu\n");
	menu_abierto=1;
	menu_event_open_menu.v=1;
}

//escribe la cadena de texto
void menu_scanf_print_string(char *string,int offset_string,int max_length_shown,int x,int y)
{
	int papel=ESTILO_GUI_PAPEL_NORMAL;
	int tinta=ESTILO_GUI_TINTA_NORMAL;
	char cadena_buf[2];

	string=&string[offset_string];

	//contar que hay que escribir el cursor
	max_length_shown--;

	//y si offset>0, primer caracter sera '<'
	if (offset_string) {
		menu_escribe_texto(x,y,tinta,papel,"<");
		max_length_shown--;
		x++;
		string++;
	}

	for (;max_length_shown && (*string)!=0;max_length_shown--) {
		cadena_buf[0]=*string;
		cadena_buf[1]=0;
		menu_escribe_texto(x,y,tinta,papel,cadena_buf);
		x++;
		string++;
	}

        //menu_escribe_texto(x,y,tinta,papel,"_");
				putchar_menu_overlay_parpadeo(x,y,'_',tinta,papel,1);
        x++;


        for (;max_length_shown!=0;max_length_shown--) {
                menu_escribe_texto(x,y,tinta,papel," ");
                x++;
        }




}

//funcion que guarda el contenido del texto del menu. Usado por ejemplo en scanf cuando se usa teclado en pantalla
void menu_save_overlay_text_contents(overlay_screen *destination,int size)
{
	//int size=sizeof(overlay_screen_array);
	debug_printf(VERBOSE_DEBUG,"Saving overlay text contents. Size=%d bytes",size);

	memcpy(destination,overlay_screen_array,size);
}

//funcion que restaura el contenido del texto del menu. Usado por ejemplo en scanf cuando se usa teclado en pantalla
void menu_restore_overlay_text_contents(overlay_screen *origin,int size)
{
	//int size=sizeof(overlay_screen_array);
	debug_printf(VERBOSE_DEBUG,"Restoring overlay text contents. Size=%d bytes",size);

	memcpy(overlay_screen_array,origin,size);
}


//devuelve cadena de texto desde teclado
//max_length contando caracter 0 del final, es decir, para un texto de 4 caracteres, debemos especificar max_length=5
//ejemplo, si el array es de 50, se le debe pasar max_length a 50
int menu_scanf(char *string,unsigned int max_length,int max_length_shown,int x,int y)
{

	z80_byte tecla;

	//ajustar offset sobre la cadena de texto visible en pantalla
	int offset_string;

	int j;
	j=strlen(string);
	if (j>max_length_shown-1) offset_string=j-max_length_shown+1;
	else offset_string=0;


	//max_length ancho maximo del texto, sin contar caracter 0
	//por tanto si el array es de 50, se le debe pasar max_length a 50

	max_length--;

	//cursor siempre al final del texto

	do {
		menu_scanf_print_string(string,offset_string,max_length_shown,x,y);

		if (menu_multitarea==0) menu_refresca_pantalla();

		menu_espera_tecla();
		//printf ("Despues de espera tecla\n");
		tecla=menu_get_pressed_key();
		//printf ("tecla leida=%d\n",tecla);
		menu_espera_no_tecla();



		//si tecla normal, agregar:
		if (tecla>31 && tecla<128) {
			if (strlen(string)<max_length) {
				int i;
				i=strlen(string);
				string[i++]=tecla;
				string[i]=0;

				if (i>=max_length_shown) offset_string++;

			}
		}

		//tecla borrar o tecla izquierda
		if (tecla==12 || tecla==8) {
			if (strlen(string)>0) {
                                int i;
                                i=strlen(string)-1;


                                string[i]=0;
				if (offset_string>0) {
					offset_string--;
					//printf ("offset string: %d\n",offset_string);
				}
			}

		}


	} while (tecla!=13 && tecla!=15 && tecla!=2);

	//if (tecla==13) printf ("salimos con enter\n");
	//if (tecla==15) printf ("salimos con tab\n");

	menu_reset_counters_tecla_repeticion();
	return tecla;

//papel=7+8;
//tinta=0;

}



//funcion para asignar funcion de overlay
void set_menu_overlay_function(void (*funcion)(void) )
{

	menu_overlay_function=funcion;

	//para que al oscurecer la pantalla tambien refresque el border
	modificado_border.v=1;
	menu_overlay_activo=1;

	//Necesario para que al poner la capa de menu, se repinte todo
	clear_putpixel_cache();	

	//Y por si acaso, aunque ya deberia haber buffer de capas activo, asignarlo
	scr_init_layers_menu();
}


//funcion para desactivar funcion de overlay
void reset_menu_overlay_function(void)
{
	//para que al oscurecer la pantalla tambien refresque el border
	modificado_border.v=1;



	menu_overlay_activo=0;

	scr_clear_layer_menu();


}

//funcion para escribir un caracter en el buffer de overlay
//tinta y/o papel pueden tener brillo (color+8)

//Nota: funciones putchar_menu_overlay* vienen heredadas del anterior entorno sin zxvision
//aunque ahora solo se deberian usar para escribir caracteres que van al titulo de la ventana.
//Para escribir dentro de la ventana (no en el titulo) se deben usar funciones de zxvision_*
void putchar_menu_overlay_parpadeo(int x,int y,z80_byte caracter,int tinta,int papel,int parpadeo)
{

	int xusado=x;

	if (menu_char_width!=8) {
		xusado=(x*menu_char_width)/8;		
	}

	//int xfinal=((x*menu_char_width)+menu_char_width-1)/8;

	//Controlar limite
	if (x<0 || y<0 || x>=scr_get_menu_width() || y>=scr_get_menu_height() ) {
		//printf ("Out of range. X: %d Y: %d Character: %c\n",x,y,caracter);
		return;
	}

	int pos_array=y*scr_get_menu_width()+x;
	overlay_screen_array[pos_array].tinta=tinta;
	overlay_screen_array[pos_array].papel=papel;
	overlay_screen_array[pos_array].parpadeo=parpadeo;

	if (ESTILO_GUI_SOLO_MAYUSCULAS) overlay_screen_array[pos_array].caracter=letra_mayuscula(caracter);
	else overlay_screen_array[pos_array].caracter=caracter;


	overlay_usado_screen_array[y*scr_get_menu_width()+xusado]=1;

	
}


//funcion para escribir un caracter en el buffer de overlay
//tinta y/o papel pueden tener brillo (color+8)
void putchar_menu_overlay(int x,int y,z80_byte caracter,int tinta,int papel)
{
	putchar_menu_overlay_parpadeo(x,y,caracter,tinta,papel,0); //sin parpadeo
}





void menu_scr_putpixel(int x,int y,int color)
{

	//int margenx_izq,margeny_arr;
	//scr_return_margenxy_rainbow(&margenx_izq,&margeny_arr);

	x *=menu_gui_zoom;
	y *=menu_gui_zoom;	

	//Esto ya no hace falta desde el uso de dos layers de menu y maquina
	/*if (rainbow_enabled.v) {
		x+=margenx_izq;
		y+=margeny_arr;
	}*/



	scr_putpixel_gui_zoom(x,y,color,menu_gui_zoom);
}


//sin el zoom de ventana, solo el posible de menu. usado en keyboard help
void menu_scr_putpixel_no_zoom(int x,int y,int color)
{

	//int margenx_izq,margeny_arr;
	//scr_return_margenxy_rainbow(&margenx_izq,&margeny_arr);

	x *=menu_gui_zoom;
	y *=menu_gui_zoom;	

	//Esto ya no hace falta desde el uso de dos layers de menu y maquina
	/*if (rainbow_enabled.v) {
		x+=margenx_izq;
		y+=margeny_arr;
	}*/



	scr_putpixel_gui_no_zoom(x,y,color,menu_gui_zoom);
}

void new_menu_putchar_footer(int x,int y,z80_byte caracter,int tinta,int papel)
{

	putchar_footer_array(x,y,caracter,tinta,papel,0);


}




void menu_putstring_footer(int x,int y,char *texto,int tinta,int papel)
{
	while ( (*texto)!=0) {
		new_menu_putchar_footer(x++,y,*texto,tinta,papel);
		texto++;
	}

	//Solo en putstring actualizamos el footer. En putchar, no
	redraw_footer();
}


void menu_footer_activity(char *texto)
{

	char buffer_texto[32];
	//Agregar espacio delante y detras
	sprintf (buffer_texto," %s ",texto);

	int inicio_x=32-strlen(buffer_texto);

	menu_putstring_footer(inicio_x,1,buffer_texto,WINDOW_FOOTER_PAPER,WINDOW_FOOTER_INK);

}

void menu_delete_footer_activity(void)
{
	//45678901
	menu_putstring_footer(24,1,"        ",WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);
}

int menu_si_mostrar_footer_f5_menu(void)
{
					//Y si no hay texto por encima de cinta autodetectada
					if (tape_options_set_first_message_counter==0 && tape_options_set_second_message_counter==0) {
							return 1;
					}

	return 0;
}

void menu_footer_f5_menu(void)
{

        //Decir F5 menu en linea de tarjetas de memoria de z88
        //Y si es la primera vez
        if (menu_si_mostrar_footer_f5_menu() ) {
												//Borrar antes con espacios si hay algo               //01234567890123456789012345678901
												//Sucede que al cargar emulador con un tap, se pone abajo primero el nombre de emulador y version,
												//y cuando se quita el splash, se pone este texto. Si no pongo espacios, se mezcla parte del texto de F5 menu etc con la version del emulador

												menu_putstring_footer(0,WINDOW_FOOTER_ELEMENT_Y_F5MENU,"                                ",WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);
                        char texto_f_menu[32];
                        sprintf(texto_f_menu,"%s Menu",openmenu_key_message);
                        menu_putstring_footer(0,WINDOW_FOOTER_ELEMENT_Y_F5MENU,texto_f_menu,WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);
				}


}

void menu_footer_zesarux_emulator(void)
{

	if (menu_si_mostrar_footer_f5_menu() ) {
		debug_printf (VERBOSE_DEBUG,"Showing ZEsarUX footer message");
		menu_putstring_footer(0,WINDOW_FOOTER_ELEMENT_Y_ZESARUX_EMULATOR,"ZEsarUX emulator v."EMULATOR_VERSION,WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);
	}

}

void menu_clear_footer(void)
{
	if (!menu_footer) return;


	debug_printf (VERBOSE_DEBUG,"Clearing Footer");

        //Borrar footer
        if (si_complete_video_driver() ) {

                int alto=WINDOW_FOOTER_SIZE;
                int ancho=screen_get_window_size_width_no_zoom_border_en();

                int yinicial=screen_get_window_size_height_no_zoom_border_en()-alto;

                int x,y;

                //Para no andar con problemas de putpixel en el caso de realvideo desactivado,
                //usamos putpixel tal cual y calculamos zoom nosotros manualmente

                alto *=zoom_y;
                ancho *=zoom_x;

                yinicial *=zoom_y;

                int color=WINDOW_FOOTER_PAPER;

                for (y=yinicial;y<yinicial+alto;y++) {
                        //printf ("%d ",y);
                        for (x=0;x<ancho;x++) {
                                scr_putpixel(x,y,color);
                        }
                }


        }

}

void menu_footer_bottom_line(void)
{
	menu_footer_zesarux_emulator();
}

void menu_footer_clear_bottom_line(void)
{

	//                         01234567890123456789012345678901
	menu_putstring_footer(0,2,"                                ",WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);

}

//Escribir textos iniciales en el footer
void menu_init_footer(void)
{
	if (!menu_footer) return;


        //int margeny_arr=screen_borde_superior*border_enabled.v;

	//Si no hay driver video
	if (scr_putpixel==NULL || scr_putpixel_zoom==NULL) return;

	debug_printf (VERBOSE_INFO,"init_footer");

	//Al iniciar emulador, si aun no hay definidas funciones putpixel, volver


	//Borrar footer con pixeles blancos
	menu_clear_footer();

	//Inicializar array footer
	cls_footer();


	//Borrar zona con espacios
	//Tantos espacios como el nombre mas largo de maquina (Microdigital TK90X (Spanish))
	menu_putstring_footer(0,0,"                            ",WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);

	//Obtener maquina activa
	menu_putstring_footer(0,0,get_machine_name(current_machine_type),WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);

	autoselect_options_put_footer();

	menu_footer_bottom_line();

	//Si hay lectura de flash activa en ZX-Uno
	//Esto lo hago asi porque al iniciar ZX-Uno, se ha activado el contador de texto "FLASH",
	//y en driver xwindows suele generar un evento ConfigureNotify, que vuelve a llamar a init_footer y borraria dicho texto FLASH
	//y por lo tanto no se veria el texto "FLASH" al arrancar la maquina
	//en otros drivers de video en teoria no haria falta



}



//funcion para limpiar el buffer de overlay y si hay cuadrado activo
void cls_menu_overlay(void)
{

	int i;

	//Borrar solo el tamanyo de menu activo
	for (i=0;i<scr_get_menu_width()*scr_get_menu_height();i++) {
		overlay_screen_array[i].caracter=0;
		overlay_usado_screen_array[i]=0;
	}

	menu_desactiva_cuadrado();

        //si hay segunda capa, escribir la segunda capa en esta primera
	//copy_second_first_overlay();




	//Si es CPC, entonces aqui el border es variable y por tanto tenemos que redibujarlo, pues quiza el menu esta dentro de zona de border
	modificado_border.v=1;

	scr_clear_layer_menu();
}



//funcion para escribir un caracter en el buffer de footer
//tinta y/o papel pueden tener brillo (color+8)
void putchar_footer_array(int x,int y,z80_byte caracter,int tinta,int papel,int parpadeo)
{

	if (!menu_footer) return;

	//int xfinal=((x*menu_char_width)+menu_char_width-1)/8;

	//Controlar limite
	if (x<0 || y<0 || x>WINDOW_FOOTER_COLUMNS || y>WINDOW_FOOTER_LINES) {
		//printf ("Out of range. X: %d Y: %d Character: %c\n",x,y,caracter);
		return;
	}

	if (ESTILO_GUI_SOLO_MAYUSCULAS) caracter=letra_mayuscula(caracter);

	int pos_array=y*WINDOW_FOOTER_COLUMNS+x;
	footer_screen_array[pos_array].tinta=tinta;
	footer_screen_array[pos_array].papel=papel;
	footer_screen_array[pos_array].parpadeo=parpadeo;
	footer_screen_array[pos_array].caracter=caracter;

	
}


void cls_footer(void)
{
	if (!menu_footer) return;

	int x,y;
	for (y=0;y<WINDOW_FOOTER_LINES;y++) {
		for (x=0;x<WINDOW_FOOTER_COLUMNS;x++) {
			putchar_footer_array(x,y,' ',WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER,0);
		}
	}
	
}

void redraw_footer_continue(void)
{
	if (!menu_footer) return;

	//printf ("redraw footer\n");

	int x,y;
	int tinta,papel,caracter,parpadeo;
	int pos_array=0;	
	for (y=0;y<WINDOW_FOOTER_LINES;y++) {
		for (x=0;x<WINDOW_FOOTER_COLUMNS;x++,pos_array++) {

			caracter=footer_screen_array[pos_array].caracter;

			tinta=footer_screen_array[pos_array].tinta;
			papel=footer_screen_array[pos_array].papel;
			parpadeo=footer_screen_array[pos_array].parpadeo;

			//Si esta multitask, si es caracter con parpadeo y si el estado del contador del parpadeo indica parpadear
			if (menu_multitarea && parpadeo && estado_parpadeo.v) caracter=' '; //si hay parpadeo y toca, meter espacio tal cual (se oculta)

			scr_putchar_footer(x,y,caracter,tinta,papel);
			
		}
	}

}

void redraw_footer(void)

{


	if (!menu_footer) return;

	redraw_footer_continue();
}

//Esta funcion antes se usaba para poner color oscuro o no al abrir el menu
//Actualmente solo cambia el valor de menu_abierto
void menu_set_menu_abierto(int valor)
{

        menu_abierto=valor;
}

//refresco de pantalla, avisando cambio de border, 
void menu_refresca_pantalla(void)
{
	modificado_border.v=1;
    all_interlace_scr_refresca_pantalla();
	redraw_footer();
}

//Borra la pantalla del menu, refresca la pantalla de spectrum
void menu_cls_refresh_emulated_screen()
{

                cls_menu_overlay();

				menu_refresca_pantalla();

}

void enable_footer(void)
{

        menu_footer=1;

        //forzar redibujar algunos contadores
        draw_bateria_contador=0;

}

void disable_footer(void)
{

        menu_footer=0;

        

}





//retornar puntero a campo desde texto, separado por espacios. se permiten multiples espacios entre campos
char *menu_get_cpu_use_idle_value(char *m,int campo)
{

	char c;

	while (campo>1) {
		c=*m;

		if (c==' ') {
			while (*m==' ') m++;
			campo--;
		}

		else m++;
	}

	return m;
}


long menu_cpu_use_seconds_antes=0;
long menu_cpu_use_seconds_ahora=0;
long menu_cpu_use_idle_antes=0;
long menu_cpu_use_idle_ahora=0;

int menu_cpu_use_num_cpus=1;

//devuelve valor idle desde /proc/stat de cpu
//devuelve <0 si error

//long temp_idle;

long menu_get_cpu_use_idle(void)
{

	//printf ("llamando a menu_get_cpu_use_idle\n");

//En Mac OS X, obtenemos consumo cpu de este proceso
#if defined(__APPLE__)

	struct rusage r_usage;

	if (getrusage(RUSAGE_SELF, &r_usage)) {
		return -1;
	    /* ... error handling ... */
	}

	//printf("Total User CPU = %ld.%d\n",        r_usage.ru_utime.tv_sec,        r_usage.ru_utime.tv_usec);

	long cpu_use_mac=r_usage.ru_utime.tv_sec*100+(r_usage.ru_utime.tv_usec/10000);   //el 10000 sale de /1000000*100

	//printf ("Valor retorno: %ld\n",cpu_use_mac);

	return cpu_use_mac;

#endif

//En Linux en cambio obtenemos uso de cpu de todo el sistema
//cat /proc/stat
//cpu  2383406 37572370 7299316 91227807 7207258 18372 473173 0 0 0
//     user    nice    system   idle

	//int max_leer=DEBUG_MAX_MESSAGE_LENGTH-200;

	#define MAX_LEER (DEBUG_MAX_MESSAGE_LENGTH-200)

	//dado que hacemos un debug_printf con este texto,
	//el maximo del debug_printf es DEBUG_MAX_MESSAGE_LENGTH. Quitamos 200 que da margen para poder escribir sin
	//hacer segmentation fault

	//metemos +1 para poder poner el 0 del final
	char procstat_buffer[MAX_LEER+1];

	//char buffer_nulo[100];
	//char buffer_idle[100];
	char *buffer_idle;
	long cpu_use_idle=0;

	char *archivo_cpuuse="/proc/stat";

	if (si_existe_archivo(archivo_cpuuse) ) {
		int leidos=lee_archivo(archivo_cpuuse,procstat_buffer,MAX_LEER);

			if (leidos<1) {
				debug_printf (VERBOSE_DEBUG,"Error reading cpu status on %s",archivo_cpuuse);
	                        return -1;
        	        }

			//leidos es >=1

			//temp
			//printf ("leidos: %d DEBUG_MAX_MESSAGE_LENGTH: %d sizeof: %d\n",leidos,DEBUG_MAX_MESSAGE_LENGTH,sizeof(procstat_buffer) );

			//establecemos final de cadena
			procstat_buffer[leidos]=0;

			debug_printf (VERBOSE_PARANOID,"procstat_buffer: %s",procstat_buffer);

			//miramos numero cpus
			menu_cpu_use_num_cpus=0;

			char *p;
			p=procstat_buffer;

			while (p!=NULL) {
				p=strstr(p,"cpu");
				if (p!=NULL) {
					p++;
					menu_cpu_use_num_cpus++;
				}
			}

			if (menu_cpu_use_num_cpus==0) {
				//como minimo habra 1
				menu_cpu_use_num_cpus=1;
			}

			else {
				//se encuentra cabecera con "cpu" y luego "cpu0, cpu1", etc, por tanto,restar 1
				menu_cpu_use_num_cpus--;
			}

			debug_printf (VERBOSE_DEBUG,"cpu number: %d",menu_cpu_use_num_cpus);

			//parsear valores, usamos scanf
			//fscanf(ptr_procstat,"%s %s %s %s %s",buffer_nulo,buffer_nulo,buffer_nulo,buffer_nulo,buffer_idle);

			//parsear valores, usamos funcion propia
			buffer_idle=menu_get_cpu_use_idle_value(procstat_buffer,5);



			if (buffer_idle!=NULL) {
				//ponemos 0 al final
				int i=0;
				while (buffer_idle[i]!=' ') {
					i++;
				}

				buffer_idle[i]=0;


				debug_printf (VERBOSE_DEBUG,"idle value: %s",buffer_idle);

				cpu_use_idle=atoi(buffer_idle);
			}
	}

	else {
		cpu_use_idle=-1;
	}

	return cpu_use_idle;

}

void menu_get_cpu_use_perc(void)
{

	int usocpu=0;

	struct timeval menu_cpu_use_time;

	gettimeofday(&menu_cpu_use_time, NULL);
	menu_cpu_use_seconds_ahora=menu_cpu_use_time.tv_sec;

	menu_cpu_use_idle_ahora=menu_get_cpu_use_idle();

	if (menu_cpu_use_idle_ahora<0) {
		menu_last_cpu_use=-1;
		return;
	}

	if (menu_cpu_use_seconds_antes!=0) {
		long dif_segundos=menu_cpu_use_seconds_ahora-menu_cpu_use_seconds_antes;
		long dif_cpu_idle=menu_cpu_use_idle_ahora-menu_cpu_use_idle_antes;

		debug_printf (VERBOSE_PARANOID,"sec now: %ld before: %ld cpu now: %ld before: %ld",menu_cpu_use_seconds_ahora,menu_cpu_use_seconds_antes,
			menu_cpu_use_idle_ahora,menu_cpu_use_idle_antes);

		long uso_cpu_idle;

		//proteger division por cero
		if (dif_segundos==0) uso_cpu_idle=100;
		else uso_cpu_idle=dif_cpu_idle/dif_segundos/menu_cpu_use_num_cpus;

#if defined(__APPLE__)
		debug_printf (VERBOSE_PARANOID,"cpu use: %ld",uso_cpu_idle);
		usocpu=uso_cpu_idle;
#else
		debug_printf (VERBOSE_PARANOID,"cpu idle: %ld",uso_cpu_idle);
		//pasamos a int
		usocpu=100-uso_cpu_idle;
#endif
	}

	menu_cpu_use_seconds_antes=menu_cpu_use_seconds_ahora;
	menu_cpu_use_idle_antes=menu_cpu_use_idle_ahora;

	menu_last_cpu_use=usocpu;
}

int cpu_use_total_acumulado=0;
int cpu_use_total_acumulado_medidas=0;

void menu_draw_cpu_use_last(void)
{

	int cpu_use=menu_last_cpu_use;
	debug_printf (VERBOSE_PARANOID,"cpu: %d",cpu_use );

	//error
	if (cpu_use<0) return;

	//control de rango
	if (cpu_use>100) cpu_use=100;
	if (cpu_use<0) cpu_use=0;

	//temp
	//cpu_use=100;

	//printf ("mostrando cpu use\n");

	char buffer_perc[9];
	sprintf (buffer_perc,"%3d%% CPU",cpu_use);

	int x;

	x=WINDOW_FOOTER_ELEMENT_X_CPU_USE;

	int color_tinta=WINDOW_FOOTER_INK;

	//Color en rojo si uso cpu sube
	if (cpu_use>=85) color_tinta=ESTILO_GUI_COLOR_AVISO;

	menu_putstring_footer(x,WINDOW_FOOTER_ELEMENT_Y_CPU_USE,buffer_perc,color_tinta,WINDOW_FOOTER_PAPER);

}

void menu_draw_cpu_use(void)
{


	if (top_speed_timer.v) {
		debug_printf (VERBOSE_DEBUG,"Refreshing footer cpu topspeed");
		menu_putstring_footer(WINDOW_FOOTER_ELEMENT_X_CPU_USE,WINDOW_FOOTER_ELEMENT_Y_CPU_USE,"TOPSPEED",ESTILO_GUI_COLOR_AVISO,WINDOW_FOOTER_PAPER);
		return;
	}

        //solo redibujarla de vez en cuando
        if (draw_cpu_use!=0) {
                draw_cpu_use--;
                return;
        }

        //cada 5 segundos
        draw_cpu_use=50*5;

	menu_get_cpu_use_perc();

	int cpu_use=menu_last_cpu_use;
	debug_printf (VERBOSE_PARANOID,"cpu: %d",cpu_use );

	//error
	if (cpu_use<0) return;

	//control de rango
	if (cpu_use>100) cpu_use=100;
	if (cpu_use<0) cpu_use=0;


	cpu_use_total_acumulado +=cpu_use;
	cpu_use_total_acumulado_medidas++;	

	menu_draw_cpu_use_last();

}



//Retorna -1 si hay algun error
int menu_get_cpu_temp(void)
{

	char procstat_buffer[10];

	//sensor generico
	char *posible_archivo_cputemp1="/sys/class/thermal/thermal_zone0/temp";

	//sensor especifico para mi pc linux
	char *posible_archivo_cputemp2="/sys/devices/platform/smsc47b397.1152/hwmon/hwmon0/temp1_input";

	char *archivo_cputemp;

	if (si_existe_archivo(posible_archivo_cputemp1) ) {
		archivo_cputemp=posible_archivo_cputemp1;
	}

	else if (si_existe_archivo(posible_archivo_cputemp2) ) {
		archivo_cputemp=posible_archivo_cputemp2;
	}

	else return -1;


	int leidos=lee_archivo(archivo_cputemp,procstat_buffer,9);

	if (leidos<1) {
        debug_printf (VERBOSE_DEBUG,"Error reading cpu status on %s",archivo_cputemp);
        return -1;
    }

    //establecemos final de cadena
    procstat_buffer[leidos]=0;


	return atoi(procstat_buffer);
	
}

void menu_draw_cpu_temp(void)
{
        //solo redibujarla de vez en cuando
        if (draw_cpu_temp!=0) {
                draw_cpu_temp--;
                return;
        }

        //cada 5 segundos
        draw_cpu_temp=50*5;

        int cpu_temp=menu_get_cpu_temp();
        debug_printf (VERBOSE_DEBUG,"CPU temp: %d",cpu_temp );

	//algun error al leer temperatura
	if (cpu_temp<0) return;

        //control de rango
        if (cpu_temp>99999) cpu_temp=99999;


        //temp forzar
        //cpu_temp=10000;

        char buffer_temp[6];

		int grados_entero=cpu_temp/1000; //2 cifras
		int grados_decimal=(cpu_temp%1000)/100; //1 cifra

        sprintf (buffer_temp,"%2d.%dC",grados_entero,grados_decimal );

        //primero liberar esas zonas
        int x;

	int color_tinta=WINDOW_FOOTER_INK;

	//Color en rojo si temperatura alta
	if (grados_entero>=80) color_tinta=ESTILO_GUI_COLOR_AVISO;


        //luego escribimos el texto
        x=WINDOW_FOOTER_ELEMENT_X_CPU_TEMP;


	menu_putstring_footer(x,WINDOW_FOOTER_ELEMENT_Y_CPU_TEMP,buffer_temp,color_tinta,WINDOW_FOOTER_PAPER);
}

void menu_draw_last_fps(void)
{


        int fps=ultimo_fps;
        debug_printf (VERBOSE_PARANOID,"FPS: %d",fps);

        //algun error al leer fps
        if (fps<0) return;

        //control de rango
        if (fps>50) fps=50;

	const int ancho_maximo=6;

			//printf ("mostrando fps\n");	

        char buffer_fps[ancho_maximo+1];
        sprintf (buffer_fps,"%02d FPS",fps);

        //primero liberar esas zonas
        int x;


        //luego escribimos el texto
        x=WINDOW_FOOTER_ELEMENT_X_FPS;


		int color_tinta=WINDOW_FOOTER_INK;

	//Color en rojo si uso fps bajo sube
	if (fps<10) color_tinta=ESTILO_GUI_COLOR_AVISO;


	menu_putstring_footer(x,WINDOW_FOOTER_ELEMENT_Y_FPS,buffer_fps,color_tinta,WINDOW_FOOTER_PAPER);

}

void menu_draw_fps(void)
{

	        //solo redibujarla de vez en cuando
        if (draw_fps!=0) {
                draw_fps--;
                return;
        }



        //cada 1 segundo
        draw_fps=50*1;

		menu_draw_last_fps();

}



int menu_get_bateria_perc(void)
{
        //temp forzar
        return 25;

}






//Aqui se llama desde cada driver de video al refrescar la pantalla
//Importante que lo que se muestre en footer se haga cada cierto tiempo y no siempre, sino saturaria la cpu probablemente
void draw_middle_footer(void)
{

	if (menu_footer==0) return;

	//temp forzado
	//menu_draw_cpu_temp();

#ifdef __linux__
	if (screen_show_cpu_temp.v) {
    	menu_draw_cpu_temp();
	}
#endif

	if (screen_show_cpu_usage.v) {
		menu_draw_cpu_use();
	}

	if (screen_show_fps.v) {
		menu_draw_fps();
	}


      

//01234567890123456789012345678901
//50 FPS 100% CPU 99.9C TEMP

}


//0 si no valido
//1 si valido
int si_valid_char(z80_byte caracter)
{
	if (si_complete_video_driver() ) {
		if (caracter<32 || caracter>MAX_CHARSET_GRAPHIC) return 0;
	}

	else {
		if (caracter<32 || caracter>127) return 0;
	}

	return 1;
}

void menu_draw_background_windows_overlay_after_normal(void)
{

	zxvision_window *ventana;
	ventana=zxvision_current_window;
	zxvision_draw_overlays_below_windows(ventana);
	//printf ("overlay funcion desde menu_draw_background_windows_overlay\n");
}




//funcion normal de impresion de overlay de buffer de texto y cuadrado de lineas usado en los menus
void normal_overlay_texto_menu(void)
{

	//printf ("inicio normal_overlay_texto_menu\n");

	int x,y;
	int tinta,papel,parpadeo;

	z80_byte caracter;
	int pos_array=0;


	//printf ("normal_overlay_texto_menu\n");
	for (y=0;y<scr_get_menu_height();y++) {
		for (x=0;x<scr_get_menu_width();x++,pos_array++) {
			caracter=overlay_screen_array[pos_array].caracter;
			//si caracter es 0, no mostrar

			//if (overlay_usado_screen_array[pos_array]) {
			if (caracter) {
				//128 y 129 corresponden a franja de menu y a letra enye minuscula
				if (si_valid_char(caracter) ) {
					tinta=overlay_screen_array[pos_array].tinta;
					papel=overlay_screen_array[pos_array].papel;
					parpadeo=overlay_screen_array[pos_array].parpadeo;

					//Si esta multitask, si es caracter con parpadeo y si el estado del contador del parpadeo indica parpadear
					if (menu_multitarea && parpadeo && estado_parpadeo.v) caracter=' '; //si hay parpadeo y toca, meter espacio tal cual (se oculta)

					scr_putchar_menu(x,y,caracter,tinta,papel);
				}

				else if (caracter==255) {
					//Significa no mostrar caracter. Usado en pantalla panic
				}

				//Si caracter no valido, mostrar ?
				else {
					tinta=overlay_screen_array[pos_array].tinta;
					papel=overlay_screen_array[pos_array].papel;
					scr_putchar_menu(x,y,'?',tinta,papel);
				}
			}
		}
	}

	if (cuadrado_activo && ventana_tipo_activa) {
		menu_dibuja_cuadrado(cuadrado_x1,cuadrado_y1,cuadrado_x2,cuadrado_y2,cuadrado_color);

		//Y si tiene marca de redimensionado
		//if (cuadrado_activo_resize) menu_dibuja_cuadrado_resize(cuadrado_x1,cuadrado_y1,cuadrado_x2,cuadrado_y2,cuadrado_color);
	}

	//Dibujar ventanas en background pero solo si menu está abierto, esto evita que aparezcan las ventanas cuando hay un 
	//mensaje de splash y el menú está cerrado
	if (menu_allow_background_windows && 
	  (menu_abierto || overlay_visible_when_menu_closed)
	) {
		//printf("redrawing windows on normal_overlay\n");
		//Conservar estado de tecla pulsada o no para el speech
		int antes_menu_speech_tecla_pulsada=menu_speech_tecla_pulsada;
		menu_draw_background_windows_overlay_after_normal();
		menu_speech_tecla_pulsada=antes_menu_speech_tecla_pulsada;
	}
 

}


//establece cuadrado activo usado en los menus para xwindows y fbdev
void menu_establece_cuadrado(int x1,int y1,int x2,int y2,int color)
{

	cuadrado_x1=x1;
	cuadrado_y1=y1;
	cuadrado_x2=x2;
	cuadrado_y2=y2;
	cuadrado_color=color;
	cuadrado_activo=1;

	//Por defecto no se ve marca de resize, para compatibilidad con ventanas no zxvision
	cuadrado_activo_resize=0;
	ventana_activa_tipo_zxvision=0;

}

//desactiva cuadrado  usado en los menus para xwindows y fbdev
void menu_desactiva_cuadrado(void)
{
	cuadrado_activo=0;
	cuadrado_activo_resize=0;
	ventana_activa_tipo_zxvision=0;
}

//Devuelve 1 si hay dos ~~ seguidas en la posicion del indice o ~^ 
//Sino, 0
int menu_escribe_texto_si_inverso(char *texto, int indice)
{

	if (menu_disable_special_chars.v) return 0;

	if (texto[indice++]!='~') return 0;
	if (texto[indice]!='~' && texto[indice]!='^') {
		return 0;
	}

	indice++;

	//Y siguiente caracter no es final de texto
	if (texto[indice]==0) return 0;

	return 1;
}

//Devuelve 1 si hay dos ^^ seguidas en la posicion del indice
//Sino, 0
int menu_escribe_texto_si_parpadeo(char *texto, int indice)
{

	if (menu_disable_special_chars.v) return 0;

    if (texto[indice++]!='^') return 0;
    if (texto[indice++]!='^') return 0;

    //Y siguiente caracter no es final de texto
    if (texto[indice]==0) return 0;

    return 1;
}


int menu_escribe_texto_si_cambio_tinta(char *texto,int indice)
{
	if (menu_disable_special_chars.v) return 0;

    if (texto[indice++]!='$') return 0;
    if (texto[indice++]!='$') return 0;
	if (texto[indice]<'0' || texto[indice]>'7'+8) return 0; //Soportar colores con brillo
	indice++;

    //Y siguiente caracter no es final de texto
    if (texto[indice]==0) return 0;

    return 1;

}

//Quita simbolos ^^y ~~ y $$X de un texto. Puede que esta funcion este repetida en algun otro sitio
void menu_convierte_texto_sin_modificadores(char *texto,char *texto_destino)
{
	int origen,destino;

	char c;

	for (origen=0,destino=0;texto[origen];origen++,destino++) {
		//printf ("origen: %d destino: %d\n",origen,destino);
		if (menu_escribe_texto_si_inverso(texto,origen) || menu_escribe_texto_si_parpadeo(texto,origen) ) {
			origen +=2;
		}

		else if (menu_escribe_texto_si_cambio_tinta(texto,origen)) {
			origen +=3;
		}

		else {
			c=texto[origen];
			texto_destino[destino]=c;
		}
		//printf ("origen: %d destino: %d\n",origen,destino);
	}

	texto_destino[destino]=0;

}

int menu_es_prefijo_utf(z80_byte caracter)
{
	if (caracter==0xD0 || caracter==0xD1 || caracter==0xC3) return 1;
	else return 0;
}

unsigned char menu_escribe_texto_convert_utf(unsigned char prefijo_utf,unsigned char caracter)
{

	if (prefijo_utf==0xC3) {
		if (caracter==0xB1) {
			//Eñe
			if (si_complete_video_driver()) {
                                return 129; //Eñe
                        }
                        else {
                                return 'n';
                        }
                }

	}

	if (prefijo_utf==0xD0) {
		if (caracter==0x90) return 'A';
		if (caracter==0x92) return 'B';
		if (caracter==0x9C) return 'M'; //cyrillic capital letter em (U+041C)
		if (caracter==0xA1) return 'C';
		if (caracter==0xA8) { //Ш
			if (si_complete_video_driver()) {
				return 131;
			}
			else {
				return 'W';
			}
		}
		if (caracter==0xB0) return 'a';
		if (caracter==0xB2) return 'B';

		

		if (caracter==0xB3) {
			if (si_complete_video_driver()) {
				return 133; //г
			}
			else {
				return 'g';
			}
		}

		if (caracter==0xB4) {
			if (si_complete_video_driver()) {
				return 135; //д
			}
			else {
				return 'D';
			}
		}

		if (caracter==0xB5) return 'e';
		if (caracter==0xB8) {
			if (si_complete_video_driver()) {
				return 130; //CYRILLIC SMALL LETTER I и
			}
			else {
				return 'i';
			}
		}
		if (caracter==0xBA) return 'k';
		if (caracter==0xBB) { //л
			if (si_complete_video_driver()) {
				return 132;
			}
			else {
				return 'l';
			}
		}		
		if (caracter==0xBC) return 'M';
		if (caracter==0xBD) return 'H';
		if (caracter==0xBE) return 'o';
	}

	if (prefijo_utf==0xD1) {
				if (caracter==0x80) return 'p';
				if (caracter==0x81) return 'c';
                if (caracter==0x82) return 'T';
                if (caracter==0x83) return 'y';
                if (caracter==0x85) return 'x';



				if (caracter==0x87) {
					if (si_complete_video_driver()) {
						return 134; //ч
					}
					else {
						return 'y';
					}
				}

				//я
				if (caracter==0x8F) {
					if (si_complete_video_driver()) {
						return 136; //я
					}
					else {
						return 'a'; //no es lo mismo, sonaria como una "ja" en dutch, pero bueno
					}
				}				

        }

	return '?';


	//Nota: caracteres que generan texto fuera de la tabla normal, considerar si es un driver de texto o grafico, con if (si_complete_video_driver() ) {
}


//escribe una linea de texto
//coordenadas relativas al interior de la pantalla de spectrum (0,0=inicio pantalla)

//Si codigo de color inverso, invertir una letra
//Codigo de color inverso: dos ~ seguidas
void menu_escribe_texto(int x,int y,int tinta,int papel,char *texto)
{
        unsigned int i;
	z80_byte letra;

	int parpadeo=0;

	int era_utf=0;

    //y luego el texto
    for (i=0;i<strlen(texto);i++) {
		letra=texto[i];

		//Si dos ^ seguidas, invertir estado parpadeo
		if (menu_escribe_texto_si_parpadeo(texto,i)) {
			parpadeo ^=1;
			//y saltamos esos codigos de negado
                        i +=2;
                        letra=texto[i];
		}

		//codigo control color tinta
		if (menu_escribe_texto_si_cambio_tinta(texto,i)) {
			tinta=texto[i+2]-'0';
			i+=3;
			letra=texto[i];
		}

		//ver si dos ~~ seguidas y cuidado al comparar que no nos vayamos mas alla del codigo 0 final
		if (menu_escribe_texto_si_inverso(texto,i)) {
			//y saltamos esos codigos de negado
			i +=2;
			letra=texto[i];

			if (menu_writing_inverse_color.v) putchar_menu_overlay_parpadeo(x,y,letra,papel,tinta,parpadeo);
			else putchar_menu_overlay_parpadeo(x,y,letra,tinta,papel,parpadeo);
		}

		else {

			//Si estaba prefijo utf activo

			if (era_utf) {
				letra=menu_escribe_texto_convert_utf(era_utf,letra);
				era_utf=0;

				//Caracter final utf
				putchar_menu_overlay_parpadeo(x,y,letra,tinta,papel,parpadeo);
			}


			//Si no, ver si entra un prefijo utf
			else {
				//printf ("letra: %02XH\n",letra);
				//Prefijo utf
                	        if (menu_es_prefijo_utf(letra)) {
        	                        era_utf=letra;
					//printf ("activado utf\n");
	                        }

				else {
					//Caracter normal
					putchar_menu_overlay_parpadeo(x,y,letra,tinta,papel,parpadeo);
				}
			}


		}

		//if (x>=32) {
		//	printf ("Escribiendo caracter [%c] en x: %d\n",letra,x);
		//}


		if (!era_utf) x++;
	}

}



//escribe una linea de texto
//coordenadas relativas al interior de la ventana (0,0=inicio zona "blanca")
void menu_escribe_texto_ventana(int x,int y,int tinta,int papel,char *texto)
{

	menu_escribe_texto(current_win_x+x,current_win_y+y+1,tinta,papel,texto);


}

void menu_retorna_colores_linea_opcion(int indice,int opcion_actual,int opcion_activada,int *papel_orig,int *tinta_orig)
{
	int papel,tinta;

	/*
	4 combinaciones:
	opcion seleccionada, disponible (activada)
	opcion seleccionada, no disponible
	opcion no seleccionada, disponible
	opcion no seleccionada, no disponible
	*/

        if (opcion_actual==indice) {
                if (opcion_activada==1) {
                        papel=ESTILO_GUI_PAPEL_SELECCIONADO;
                        tinta=ESTILO_GUI_TINTA_SELECCIONADO;
                }
                else {
                        papel=ESTILO_GUI_PAPEL_SEL_NO_DISPONIBLE;
                        tinta=ESTILO_GUI_TINTA_SEL_NO_DISPONIBLE;
                }
        }

        else {
                if (opcion_activada==1) {
                        papel=ESTILO_GUI_PAPEL_NORMAL;
                        tinta=ESTILO_GUI_TINTA_NORMAL;
                }
                else {
                        papel=ESTILO_GUI_PAPEL_NO_DISPONIBLE;
                        tinta=ESTILO_GUI_TINTA_NO_DISPONIBLE;
                }
        }

	*papel_orig=papel;
	*tinta_orig=tinta;

}



//escribe opcion de linea de texto
//coordenadas "indice" relativa al interior de la ventana (0=inicio)
//opcion_actual indica que numero de linea es la seleccionada
//opcion activada indica a 1 que esa opcion es seleccionable
void menu_escribe_linea_opcion_zxvision(zxvision_window *ventana,int indice,int opcion_actual,int opcion_activada,char *texto_entrada)
{

	char texto[MAX_ESCR_LINEA_OPCION_ZXVISION_LENGTH+1]; 
	//Le doy 1 byte mas. Por si acaso alguien llama aqui sin contar el byte 0 del final y la lia...

	int papel,tinta;
	int i;

	//tinta=0;


	menu_retorna_colores_linea_opcion(indice,opcion_actual,opcion_activada,&papel,&tinta);


	//Obtenemos colores de una opcion sin seleccion y activada, para poder tener texto en ventana con linea en dos colores
	int papel_normal,tinta_normal;
	menu_retorna_colores_linea_opcion(0,-1,1,&papel_normal,&tinta_normal);

	//Buscamos a ver si en el texto hay el caracter "||" y en ese caso lo eliminamos del texto final
	int encontrado=-1;
	int destino=0;
	for (i=0;texto_entrada[i];i++) {
		if (menu_disable_special_chars.v==0 && texto_entrada[i]=='|' && texto_entrada[i+1]=='|') {
			encontrado=i;
			i ++;
		}
		else {
			texto[destino++]=texto_entrada[i];
		}
	}

	texto[destino]=0;


	//linea entera con espacios
	for (i=0;i<current_win_ancho;i++) {
		zxvision_print_string(ventana,i,indice,0,papel,0," ");
	}

	//y texto propiamente
	int startx=menu_escribe_linea_startx;
	zxvision_print_string(ventana,startx,indice,tinta,papel,0,texto);

	//Si tiene dos colores
	if (encontrado>=0) {
		//menu_escribe_texto_ventana(startx+encontrado,indice,tinta_normal,papel_normal,&texto[encontrado]);
		zxvision_print_string(ventana,startx+encontrado,indice,tinta_normal,papel_normal,0,&texto[encontrado]);
	}

	//si el driver de video no tiene colores o si el estilo de gui lo indica, indicamos opcion activa con un cursor
	if (!scr_tiene_colores || ESTILO_GUI_MUESTRA_CURSOR) {
		if (opcion_actual==indice) {
			if (opcion_activada==1) {
				//menu_escribe_texto_ventana(0,indice,tinta,papel,">");
				zxvision_print_string(ventana,0,indice,tinta,papel,0,">");
			}
			else {
				//menu_escribe_texto_ventana(0,indice,tinta,papel,"x");
				zxvision_print_string(ventana,0,indice,tinta,papel,0,"x");
			}
		}
	}



}


//escribe opcion de linea de texto
//coordenadas "indice" relativa al interior de la ventana (0=inicio)
//opcion_actual indica que numero de linea es la seleccionada
//opcion activada indica a 1 que esa opcion es seleccionable
void menu_escribe_linea_opcion(int indice,int opcion_actual,int opcion_activada,char *texto_entrada)
{

	char texto[64];

	int papel,tinta;
	int i;

	//tinta=0;


	menu_retorna_colores_linea_opcion(indice,opcion_actual,opcion_activada,&papel,&tinta);


	//Obtenemos colores de una opcion sin seleccion y activada, para poder tener texto en ventana con linea en dos colores
	int papel_normal,tinta_normal;
	menu_retorna_colores_linea_opcion(0,-1,1,&papel_normal,&tinta_normal);

	//Buscamos a ver si en el texto hay el caracter "||" y en ese caso lo eliminamos del texto final
	int encontrado=-1;
	int destino=0;
	for (i=0;texto_entrada[i];i++) {
		if (menu_disable_special_chars.v==0 && texto_entrada[i]=='|' && texto_entrada[i+1]=='|') {
			encontrado=i;
			i ++;
		}
		else {
			texto[destino++]=texto_entrada[i];
		}
	}

	texto[destino]=0;


	//linea entera con espacios
	for (i=0;i<current_win_ancho;i++) menu_escribe_texto_ventana(i,indice,0,papel," ");

	//y texto propiamente
	int startx=menu_escribe_linea_startx;
        menu_escribe_texto_ventana(startx,indice,tinta,papel,texto);

	//Si tiene dos colores
	if (encontrado>=0) {
		menu_escribe_texto_ventana(startx+encontrado,indice,tinta_normal,papel_normal,&texto[encontrado]);
	}

	//si el driver de video no tiene colores o si el estilo de gui lo indica, indicamos opcion activa con un cursor
	if (!scr_tiene_colores || ESTILO_GUI_MUESTRA_CURSOR) {
		if (opcion_actual==indice) {
			if (opcion_activada==1) menu_escribe_texto_ventana(0,indice,tinta,papel,">");
			else menu_escribe_texto_ventana(0,indice,tinta,papel,"x");
		}
	}

}




//escribe opcion de texto tabulado
//coordenadas "indice" relativa al interior de la ventana (0=inicio)
//opcion_actual indica que numero de linea es la seleccionada
//opcion activada indica a 1 que esa opcion es seleccionable
void menu_escribe_linea_opcion_tabulado_zxvision(zxvision_window *ventana,int indice,int opcion_actual,int opcion_activada,char *texto,int x,int y)
{

        int papel,tinta;


        menu_retorna_colores_linea_opcion(indice,opcion_actual,opcion_activada,&papel,&tinta);


		zxvision_print_string(ventana,x,y,tinta,papel,0,texto);
		//printf ("Escribiendo texto tabulado %s en %d,%d\n",texto,x,y);


}

void menu_retorna_margenes_border(int *miz, int *mar)
{
	//margenes de zona interior de pantalla. para modo rainbow
	int margenx_izq=screen_total_borde_izquierdo*border_enabled.v;
	int margeny_arr=screen_borde_superior*border_enabled.v;

	{
//margenes para realvideo
margenx_izq=TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v;
					margeny_arr=TBBLUE_TOP_BORDER_NO_ZOOM*border_enabled.v;
	}	

	*miz=margenx_izq;
	*mar=margeny_arr;

}


//dibuja cuadrado (4 lineas) usado en los menus para xwindows y fbdev
//Entrada: x1,y1 punto superior izquierda,x2,y2 punto inferior derecha en resolucion de zx spectrum. Color
//nota: realmente no es un cuadrado porque el titulo ya hace de franja superior
void menu_dibuja_cuadrado(int x1,int y1,int x2,int y2,int color)
{

	if (!ESTILO_GUI_MUESTRA_RECUADRO) return;


	int x,y;



	//Para poner una marca en la ventana indicando si es de tipo zxvision
	int centro_marca_zxvison_x=x2-3-6;
	int centro_marca_zxvison_y=y1+3+2;
		
	//int longitud_marca_zxvision=3;
	//int mitad_long_marca_zxvision=longitud_marca_zxvision/2;
	int color_marca_zxvision=ESTILO_GUI_PAPEL_NORMAL;


	//printf ("Cuadrado %d,%d - %d,%d\n",x1,y1,x2,y2);


	//solo hacerlo en el caso de drivers completos
	if (si_complete_video_driver() ) {

		//if (rainbow_enabled.v) {
		if (1) {

			//parte inferior
			for (x=x1;x<=x2;x++) {
				if (mouse_is_dragging && (x%2)==0) continue; //punteado cuando se mueve o redimensiona
				scr_putpixel_gui_zoom(x*menu_gui_zoom,y2*menu_gui_zoom,color,menu_gui_zoom);
			}


			//izquierda
			for (y=y1;y<=y2;y++) {
				if (mouse_is_dragging && (y%2)==0) continue; //punteado cuando se mueve o redimensiona
				scr_putpixel_gui_zoom(x1*menu_gui_zoom,y*menu_gui_zoom,color,menu_gui_zoom);
			}

			

			//derecha
			for (y=y1;y<=y2;y++) {
				if (mouse_is_dragging && (y%2)==0) continue; //punteado cuando se mueve o redimensiona
				scr_putpixel_gui_zoom(x2*menu_gui_zoom,y*menu_gui_zoom,color,menu_gui_zoom);
			}


                      

			//Marca redimensionado
			if (cuadrado_activo_resize) {
				//marca de redimensionado
				//		  *
				//		 **
				//		***	


				//Arriba del todo
				scr_putpixel_gui_zoom((x2-1)*menu_gui_zoom,(y2-3)*menu_gui_zoom,color,menu_gui_zoom);	

				//Medio
				scr_putpixel_gui_zoom((x2-1)*menu_gui_zoom,(y2-2)*menu_gui_zoom,color,menu_gui_zoom);
				scr_putpixel_gui_zoom((x2-2)*menu_gui_zoom,(y2-2)*menu_gui_zoom,color,menu_gui_zoom);				

				//Abajo del todo
				scr_putpixel_gui_zoom((x2-1)*menu_gui_zoom,(y2-1)*menu_gui_zoom,color,menu_gui_zoom);
				scr_putpixel_gui_zoom((x2-2)*menu_gui_zoom,(y2-1)*menu_gui_zoom,color,menu_gui_zoom);
				scr_putpixel_gui_zoom((x2-3)*menu_gui_zoom,(y2-1)*menu_gui_zoom,color,menu_gui_zoom);
			}

			if (!ventana_activa_tipo_zxvision) {
				//Poner un pixel avisando que ventana no es zxvision
				scr_putpixel_gui_zoom((centro_marca_zxvison_x)*menu_gui_zoom,(centro_marca_zxvison_y)*menu_gui_zoom,color_marca_zxvision,menu_gui_zoom);					
			}				


		}

		/*else {

		
 	               //parte inferior
        	        for (x=x1;x<=x2;x++) {
						if (mouse_is_dragging && (x%2)==0) continue; //punteado cuando se mueve o redimensiona
						scr_putpixel_gui_zoom(x*menu_gui_zoom,y2*menu_gui_zoom,color,menu_gui_zoom);
					}



	                //izquierda
        	        for (y=y1;y<=y2;y++) {
						if (mouse_is_dragging && (y%2)==0) continue; //punteado cuando se mueve o redimensiona
						scr_putpixel_gui_zoom(x1*menu_gui_zoom,y*menu_gui_zoom,color,menu_gui_zoom);
					}

					

	                //derecha
        	        for (y=y1;y<=y2;y++) {
						if (mouse_is_dragging && (y%2)==0) continue; //punteado cuando se mueve o redimensiona
						scr_putpixel_gui_zoom(x2*menu_gui_zoom,y*menu_gui_zoom,color,menu_gui_zoom);
					}
                               


			//Marca redimensionado
			if (cuadrado_activo_resize) {
				//marca de redimensionado
				//		  *
				//		 **
				//		***	

				//Arriba del todo
				scr_putpixel_gui_zoom((x2-1)*menu_gui_zoom,(y2-3)*menu_gui_zoom,color,menu_gui_zoom);

				//Medio
				scr_putpixel_gui_zoom((x2-1)*menu_gui_zoom,(y2-2)*menu_gui_zoom,color,menu_gui_zoom);		
				scr_putpixel_gui_zoom((x2-2)*menu_gui_zoom,(y2-2)*menu_gui_zoom,color,menu_gui_zoom);	

				//Abajo del todo
				scr_putpixel_gui_zoom((x2-1)*menu_gui_zoom,(y2-1)*menu_gui_zoom,color,menu_gui_zoom);		
				scr_putpixel_gui_zoom((x2-2)*menu_gui_zoom,(y2-1)*menu_gui_zoom,color,menu_gui_zoom);	
				scr_putpixel_gui_zoom((x2-3)*menu_gui_zoom,(y2-1)*menu_gui_zoom,color,menu_gui_zoom);							
			}


			if (!ventana_activa_tipo_zxvision) {
				

				//Poner solo un pixel
				scr_putpixel_gui_zoom((centro_marca_zxvison_x)*menu_gui_zoom,(centro_marca_zxvison_y)*menu_gui_zoom,color_marca_zxvision,menu_gui_zoom);					
			}	

		}
		*/
	}


}

void menu_muestra_pending_error_message(void)
{
	if (if_pending_error_message) {
		if_pending_error_message=0;
		debug_printf (VERBOSE_INFO,"Showing pending error message on menu");
		//menu_generic_message("ERROR",pending_error_message);
		menu_error_message(pending_error_message);
	}
}


//x,y origen ventana, ancho ventana
void menu_dibuja_ventana_franja_arcoiris_oscuro(int x, int y, int ancho,int indice)
{

	if (!ventana_tipo_activa) return;

	//int cr[]={2,6,4,5};

	int cr[4]; 
	//Copiar del estilo actual aqui, pues internamente lo modificamos
	int i;
	int *temp_ptr;
	temp_ptr=ESTILO_GUI_FRANJAS_OSCURAS;
	for (i=0;i<4;i++) {
		cr[i]=temp_ptr[i];
	}
	//int *cr;
	//cr=ESTILO_GUI_FRANJAS_OSCURAS;

	//int indice=4-franjas;

	if (indice>=0 && indice<=3) {
		//cr[indice]+=8;
		//Coger color de las normales brillantes
		int *temp_ptr_brillo;
		temp_ptr_brillo=ESTILO_GUI_FRANJAS_NORMALES;
		cr[indice]=temp_ptr_brillo[indice];
	}

	int restar=0;

	if (zxvision_window_can_be_backgrounded(zxvision_current_window)) restar++;

	x-=restar;


	if (ESTILO_GUI_MUESTRA_RAINBOW) {

		if (si_complete_video_driver() ) {
		                	putchar_menu_overlay(x+ancho-6,y,128,cr[0],ESTILO_GUI_PAPEL_TITULO);
        	        	putchar_menu_overlay(x+ancho-5,y,128,cr[1],cr[0]);
                		putchar_menu_overlay(x+ancho-4,y,128,cr[2],cr[1]);
	                	putchar_menu_overlay(x+ancho-3,y,128,cr[3],cr[2]);
        	        	putchar_menu_overlay(x+ancho-2,y,128,ESTILO_GUI_PAPEL_TITULO,cr[3]);
		}

	}
}

//x,y origen ventana, ancho ventana
void menu_dibuja_ventana_franja_arcoiris_trozo(int x, int y, int ancho,int franjas)
{

	if (!ventana_tipo_activa) return;

	//int cr[]={2+8,6+8,4+8,5+8};
	int *cr;
	cr=ESTILO_GUI_FRANJAS_NORMALES;

	int restar=0;

	if (zxvision_window_can_be_backgrounded(zxvision_current_window)) restar++;

	x-=restar;	

	if (ESTILO_GUI_MUESTRA_RAINBOW) {
		//en el caso de drivers completos, hacerlo real
		if (si_complete_video_driver() ) {
			//5 espacios negro primero
			int i;
			for (i=6;i>=2;i--) putchar_menu_overlay(x+ancho-i,y,' ',ESTILO_GUI_PAPEL_TITULO,ESTILO_GUI_PAPEL_TITULO);
	                if (franjas==4) {
	                	putchar_menu_overlay(x+ancho-6,y,128,cr[0],ESTILO_GUI_PAPEL_TITULO);
        	        	putchar_menu_overlay(x+ancho-5,y,128,cr[1],cr[0]);
                		putchar_menu_overlay(x+ancho-4,y,128,cr[2],cr[1]);
	                	putchar_menu_overlay(x+ancho-3,y,128,cr[3],cr[2]);
        	        	putchar_menu_overlay(x+ancho-2,y,128,ESTILO_GUI_PAPEL_TITULO,cr[3]);
        	        }

        	     	if (franjas==3) {
        	        	putchar_menu_overlay(x+ancho-5,y,128,cr[1],ESTILO_GUI_PAPEL_TITULO);
                		putchar_menu_overlay(x+ancho-4,y,128,cr[2],cr[1]);
	                	putchar_menu_overlay(x+ancho-3,y,128,cr[3],cr[2]);
        	        	putchar_menu_overlay(x+ancho-2,y,128,ESTILO_GUI_PAPEL_TITULO,cr[3]);
        	        }


        	        if (franjas==2) {
                		putchar_menu_overlay(x+ancho-4,y,128,cr[2],ESTILO_GUI_PAPEL_TITULO);
	                	putchar_menu_overlay(x+ancho-3,y,128,cr[3],cr[2]);
        	        	putchar_menu_overlay(x+ancho-2,y,128,ESTILO_GUI_PAPEL_TITULO,cr[3]);
        	        }

        	        if (franjas==1) {
	                	putchar_menu_overlay(x+ancho-3,y,128,cr[3],ESTILO_GUI_PAPEL_TITULO);
        	        	putchar_menu_overlay(x+ancho-2,y,128,ESTILO_GUI_PAPEL_TITULO,cr[3]);
        	        }

        	      
	        }

	}
}


void menu_dibuja_ventana_franja_arcoiris(int x, int y, int ancho)
{
	menu_dibuja_ventana_franja_arcoiris_trozo(x,y,ancho,4);
}

void menu_dibuja_ventana_franja_arcoiris_trozo_current(int trozos)
{

	menu_dibuja_ventana_franja_arcoiris_trozo(current_win_x,current_win_y,current_win_ancho,trozos);
}

void menu_dibuja_ventana_franja_arcoiris_oscuro_current(int indice)
{

	menu_dibuja_ventana_franja_arcoiris_oscuro(current_win_x,current_win_y,current_win_ancho,indice);
}

//Da ancho titulo de ventana segun el texto titulo, boton cerrado si/no, y franjas de color
int menu_da_ancho_titulo(char *titulo)
{
		int ancho_boton_cerrar=2;

        if (menu_hide_close_button.v) ancho_boton_cerrar=0;

		int ancho_franjas_color=MENU_ANCHO_FRANJAS_TITULO;

		if (!ESTILO_GUI_MUESTRA_RAINBOW) ancho_franjas_color=0;

		int margen_adicional=2; //1 para que no se pegue el titulo a la derecha, otro mas para el caracter de minimizar

		int ancho_total=strlen(titulo)+ancho_boton_cerrar+ancho_franjas_color+margen_adicional; //+1 de margen, para que no se pegue el titulo

		if (zxvision_window_can_be_backgrounded(zxvision_current_window)) {
			//printf ("Sumamos 1\n");
			//sleep(5);
			ancho_total++;
		}

		return ancho_total;
}



int menu_dibuja_ventana_ret_ancho_titulo(int ancho,char *titulo)
{
	int ancho_mostrar_titulo=menu_da_ancho_titulo(titulo);

	int ancho_disponible_titulo=ancho;

	if (ancho_disponible_titulo<ancho_mostrar_titulo) ancho_mostrar_titulo=ancho_disponible_titulo;

	return ancho_mostrar_titulo;
}

z80_byte menu_retorna_caracter_minimizar(zxvision_window *w)
{
	z80_byte caracter_mostrar='-';
	if (w->is_minimized) caracter_mostrar='=';

	return caracter_mostrar;
}

int zxvision_window_can_be_backgrounded(zxvision_window *w)
{
	if (w==NULL) return 0;

	if (menu_allow_background_windows && w->can_be_backgrounded) return 1;
	else return 0;
}

//Retorna el caracter que indica que una ventana esta en background
char zxvision_get_character_backgrounded_window(void)
{

    //Si estamos redibujando con menu cerrado, para indicar que no se interactua directamente con esa ventana
    if (overlay_visible_when_menu_closed) return '/';

    else return '!';
}

void menu_dibuja_ventana_boton_background(int x,int y,int ancho,zxvision_window *w)
{


			//Boton de background
			if (zxvision_window_can_be_backgrounded(w)) {
				if (ventana_tipo_activa) {
					//Boton de background, con ventana activa
					putchar_menu_overlay(x+ancho-2,y,zxvision_get_character_backgrounded_window(),ESTILO_GUI_TINTA_TITULO,ESTILO_GUI_PAPEL_TITULO);
				}

				else {
					//ventana inactiva. mostrar "!" con parpadeo
					//printf ("ventana inactiva\n");
					if (w->overlay_function!=NULL) {
						//printf ("boton background\n");
						//zxvision_print_char_simple(zxvision_current_window,ancho-2,0,ESTILO_GUI_PAPEL_TITULO,ESTILO_GUI_TINTA_TITULO,1,'!');
						putchar_menu_overlay_parpadeo(x+ancho-2,y,zxvision_get_character_backgrounded_window(),ESTILO_GUI_TINTA_TITULO_INACTIVA,ESTILO_GUI_PAPEL_TITULO_INACTIVA,1);
					}
				}
				
			}
}

void menu_dibuja_ventana_botones(void)
{

	int x=current_win_x;
	int y=current_win_y;
	int ancho=current_win_ancho;
	//int alto=ventana_alto;

		
		if (ventana_activa_tipo_zxvision) {

			//Boton de minimizar
			if (ventana_tipo_activa) {
				if (cuadrado_activo_resize) {
					z80_byte caracter_mostrar=menu_retorna_caracter_minimizar(zxvision_current_window);
					if (menu_hide_minimize_button.v) caracter_mostrar=' ';
					//Si no mostrar, meter solo espacio. es importante esto, si no hay boton, y no escribieramos espacio,
					//se veria el texto de titulo en caso de que ancho de ventana la hagamos pequeña

					//if (zxvision_current_window->is_minimized) caracter_mostrar='+';
					putchar_menu_overlay(x+ancho-1,y,caracter_mostrar,ESTILO_GUI_TINTA_TITULO,ESTILO_GUI_PAPEL_TITULO);
				}



			}

			menu_dibuja_ventana_boton_background(x,y,ancho,zxvision_current_window);


		}


		//putchar_menu_overlay(x+ancho-1,y,'-',ESTILO_GUI_TINTA_TITULO,ESTILO_GUI_PAPEL_TITULO);
}

//si no mostramos mensajes de error pendientes
int no_dibuja_ventana_muestra_pending_error_message=0;

//dibuja ventana de menu, con:
//titulo
//contenido blanco
//recuadro de lineas
//Entrada: x,y posicion inicial. ancho, alto. Todo coordenadas en caracteres 0..31 y 0..23
void menu_dibuja_ventana(int x,int y,int ancho,int alto,char *titulo)
{

	//Para draw below windows, no mostrar error pendiente cuando esta dibujando ventanas de debajo
	if (!no_dibuja_ventana_muestra_pending_error_message) menu_muestra_pending_error_message();
	//valores en pixeles
	int xpixel,ypixel,anchopixel,altopixel;
	int i,j;

	//guardamos valores globales de la ventana mostrada
	current_win_x=x;
	current_win_y=y;
	current_win_ancho=ancho;
	current_win_alto=alto;

	xpixel=x*menu_char_width;
	ypixel=y*8;
	anchopixel=ancho*menu_char_width;
	altopixel=alto*8;

	int xderecha=xpixel+anchopixel-1;
	//printf ("x derecha: %d\n",xderecha);

	//if (menu_char_width!=8) xderecha++; //?????

	//contenido en blanco normalmente en estilo ZEsarUX
	for (i=0;i<alto-1;i++) {
		for (j=0;j<ancho;j++) {
			//putchar_menu_overlay(x+j,y+i+1,' ',0,7+8);
			putchar_menu_overlay(x+j,y+i+1,' ',ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL);
		}
	}

	//recuadro
	//menu_establece_cuadrado(xpixel,ypixel,xpixel+anchopixel-1,ypixel+altopixel-1,0);
	//printf ("cuadrado: %d,%d %dX%d\n",xpixel,ypixel,xpixel+anchopixel-1,ypixel+altopixel-1);
	
	menu_establece_cuadrado(xpixel,ypixel,xderecha,ypixel+altopixel-1,ESTILO_GUI_PAPEL_TITULO);


	int color_tinta_titulo;
	int color_papel_titulo;


	if (ventana_tipo_activa) {
		color_tinta_titulo=ESTILO_GUI_TINTA_TITULO;
		color_papel_titulo=ESTILO_GUI_PAPEL_TITULO;
	}

	else {
		color_tinta_titulo=ESTILO_GUI_TINTA_TITULO_INACTIVA;
		color_papel_titulo=ESTILO_GUI_PAPEL_TITULO_INACTIVA;		
	}


        //titulo
        //primero franja toda negra normalmente en estilo ZEsarUX
        for (i=0;i<ancho;i++) {
			putchar_menu_overlay(x+i,y,' ',color_tinta_titulo,color_papel_titulo);
		}


		int ancho_mostrar_titulo=menu_dibuja_ventana_ret_ancho_titulo(ancho,titulo);

		char titulo_mostrar[64];
		char caracter_cerrar=ESTILO_GUI_BOTON_CERRAR;

		if (menu_hide_close_button.v || ventana_es_background ) strcpy(titulo_mostrar,titulo);
		else sprintf (titulo_mostrar,"%c %s",caracter_cerrar,titulo);


        //y luego el texto. titulo mostrar solo lo que cabe de ancho


	//Boton de cerrado



        for (i=0;i<ancho_mostrar_titulo && titulo_mostrar[i];i++) {
			char caracter_mostrar=titulo_mostrar[i];
			
			putchar_menu_overlay(x+i,y,caracter_mostrar,color_tinta_titulo,color_papel_titulo);
		}



        //y las franjas de color
	if (ESTILO_GUI_MUESTRA_RAINBOW && ventana_tipo_activa) {
		//en el caso de drivers completos, hacerlo real
		menu_dibuja_ventana_franja_arcoiris(x,y,ancho);
	
	}

		menu_dibuja_ventana_botones();

}

int last_mouse_x,last_mouse_y;
int mouse_movido=0;

int menu_mouse_x=0;
int menu_mouse_y=0;

zxvision_window *zxvision_current_window=NULL;


//Para cargar las ventanas al inicio, las va rellenando al leer los parámetros en arranque
char restore_window_array[MAX_RESTORE_WINDOWS_START][MAX_NAME_WINDOW_GEOMETRY];

int total_restore_window_array_elements=0;

//Ventanas conocidas y sus funciones que las inicializan. Usado al restaurar ventanas al inicio
//La ultima siempre finaliza con funcion NULL
zxvision_known_window_names zxvision_known_window_names_array[]={
#ifdef EMULATE_CPU_STATS
	{"cpucompactstatistics",menu_debug_cpu_resumen_stats},
#endif

	{"sprites",menu_debug_view_sprites},
	{"watches",menu_watches},
	{"videoinfo",menu_debug_tsconf_tbblue_msx_videoregisters},
	{"tsconftbbluespritenav",menu_debug_tsconf_tbblue_msx_spritenav},
	{"tsconftbbluetilenav",menu_debug_tsconf_tbblue_msx_tilenav},
	{"debugcpu",menu_debug_registers},
    {"debugconsole",menu_debug_unnamed_console},

	{"",NULL} //NO BORRAR ESTA!!
};



//Decir que con una ventana zxvision visible, las pulsaciones de teclas no se envian a maquina emulada
int zxvision_keys_event_not_send_to_machine=1;

//indicar que estamos restaurando ventanas y por tanto las funciones que las crean tienen que volver nada mas entrar
int zxvision_currently_restoring_windows_on_start=0;

//Retorna posicion a indice de zxvision_known_window_names_array si se encuentra
//-1 si no
int zxvision_find_known_window(char *nombre)
{
	int i;


	for (i=0;zxvision_known_window_names_array[i].start!=NULL;i++) {

		 if (!strcmp(zxvision_known_window_names_array[i].nombre,nombre)) return i;

	}
	return -1;
}

void zxvision_restore_windows_on_startup(void)
{
	if (!menu_allow_background_windows) return;

	if (menu_reopen_background_windows_on_start.v==0) return;

	//Si no hay multitask, no restaurar, porque esto puede implicar que se abran ventanas que necesitan multitask, 
	//y se quejen con "This menu item needs multitask enabled", y ese mensaje no se ve el error, y espera una tecla
	if (!menu_multitarea) return;

	//indicar que estamos restaurando ventanas y por tanto las funciones que las crean tienen que volver nada mas entrar
	zxvision_currently_restoring_windows_on_start=1;

	//Iterar sobre todas
	int i;

	menu_speech_tecla_pulsada=1; //Si no, envia continuamente los textos de las ventanas a speech

	//Si ha habido un error y mostramos al final
	int error_restoring_window=0;
	int error_restoring_window_index;

	//Guardar valores funciones anteriores
	int antes_menu_overlay_activo=menu_overlay_activo;

	//printf("zxvision_restore_windows_on_startup: menu_overlay_activo: %d\n",menu_overlay_activo);


	for (i=0;i<total_restore_window_array_elements;i++) {
		//printf ("Restoring window %s\n",restore_window_array[i]);

		int indice=zxvision_find_known_window(restore_window_array[i]);

		if (indice<0) {
			//Si hay error con alguna ventana desconocida, nos guardamos el error para el final
			//si no, en medio de la restauracion salta esta ventana y ademas no se redimensiona a zxdesktop
			error_restoring_window=1;
			error_restoring_window_index=i;
			//debug_printf (VERBOSE_ERR,"Unknown window to restore: %s",restore_window_array[i]);
		}

		else {
		//Lanzar funcion que la crea
			zxvision_known_window_names_array[indice].start(0);

			//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background,
			//siempre que no sea la de normal overlay o null
			zxvision_set_window_overlay_from_current(zxvision_current_window);

			//restauramos modo normal de texto de menu
    		set_menu_overlay_function(normal_overlay_texto_menu);

			/*
			Esa ventana ya viene de background por tanto no hay que guardar nada en la ventana., 
			es más, si estamos aquí es que se ha salido de la ventana con escape (y la current window ya no estará) 
			o con f6. Total que no hay que guardar nada. 
			Pero si que conviene dejar el overlay como estaba antes 
			*/
		}

	}

	zxvision_currently_restoring_windows_on_start=0;

	if (error_restoring_window) {
		debug_printf (VERBOSE_ERR,"Unknown window to restore: %s",restore_window_array[error_restoring_window_index]);
	}

	//printf ("End restoring windows\n");

	//Si antes no estaba activo, ponerlo a 0. El cambio a normal_overlay_texto_menu que se hace en el bucle
	//no lo desactiva
	//Si no hicieramos esto, al restaurar ventanas y, no siempre, se quedan las ventanas abiertas,
	//sin dibujar el contenido, pero con los marcos y titulo visible, aunque el menu está cerrado
	//quiza sucede cuando la maquina al arrancar es tsconf o cualquier otra que no tiene el tamaño de ventana standard de spectrum
	if (!antes_menu_overlay_activo) {
		menu_overlay_activo=0;
	}

}

void zxvision_set_draw_window_parameters(zxvision_window *w)
{
	ventana_activa_tipo_zxvision=1;

	cuadrado_activo_resize=w->can_be_resized;

}

void zxvision_draw_below_windows_nospeech(zxvision_window *w)
{
	//Redibujar las de debajo
	//printf ("antes draw below\n");

	int antes_menu_speech_tecla_pulsada=menu_speech_tecla_pulsada;
	//No enviar a speech las ventanas por debajo
	menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech
	
	zxvision_draw_below_windows(w);

	menu_speech_tecla_pulsada=antes_menu_speech_tecla_pulsada;
	//printf ("despues draw below\n");
}

//Controlar rangos excepto tamaño ventana en estatico
void zxvision_new_window_check_range(int *x,int *y,int *visible_width,int *visible_height)
{

	//Controlar rangos. Cualquier valor que se salga de rango, hacemos ventana maximo 32x24

	//Rango xy es el total de ventana. Rango ancho y alto es 32x24, aunque luego se pueda hacer mas grande

	if (

	 (*x<0               || *x>ZXVISION_MAX_X_VENTANA) ||
	 (*y<0               || *y>ZXVISION_MAX_Y_VENTANA) ||

	 //Rangos estaticos de ventana
	 (*visible_width<=0) ||
	 (*visible_height<=0) ||

	//Rangos de final de ventana. ZXVISION_MAX_X_VENTANA normalmente vale 31. ZXVISION_MAX_Y_VENTANA normalmente vale 23. Si esta en ancho 31 y le suma 1, es ok. Si suma 2, es error
	 ((*x)+(*visible_width)>ZXVISION_MAX_X_VENTANA+1) ||
	 ((*y)+(*visible_height)>ZXVISION_MAX_Y_VENTANA+1) 

	)
		{
                debug_printf (VERBOSE_INFO,"zxvision_new_window: window out of range: %d,%d %dx%d. Returning fixed safe values",*x,*y,*visible_width,*visible_height);
				//printf ("zxvision_new_window: window out of range: %d,%d %dx%d\n",*x,*y,*visible_width,*visible_height);
                *x=0;
                *y=0;
                *visible_width=ZXVISION_MAX_ANCHO_VENTANA;
                *visible_height=ZXVISION_MAX_ALTO_VENTANA;

		}
}

//Comprobar que el alto y ancho no pase de un fijo estatico (32x24 normalmente),
//para tener ventanas que normalmente no excedan ese 32x24 al crearse
//Nota: menu_filesel no hace este check
void zxvision_new_window_check_static_size_range(int *x,int *y,int *visible_width,int *visible_height)
{


	if (


	 //Rangos estaticos de ancho ventana
	 (*visible_width>ZXVISION_MAX_ANCHO_VENTANA) ||
	 (*visible_height>ZXVISION_MAX_ALTO_VENTANA) 


	)
		{
                debug_printf (VERBOSE_INFO,"zxvision_new_window: window out of range: %d,%d %dx%d",*x,*y,*visible_width,*visible_height);
				//printf ("zxvision_new_window: window out of range: %d,%d %dx%d\n",*x,*y,*visible_width,*visible_height);
                *x=0;
                *y=0;
                *visible_width=ZXVISION_MAX_ANCHO_VENTANA;
                *visible_height=ZXVISION_MAX_ALTO_VENTANA;

		}
}


//Buscar la ventana mas antigua debajo de esta en cascada
zxvision_window *zxvision_find_first_window_below_this(zxvision_window *w)
{
		//Primero ir a buscar la de abajo del todo
	zxvision_window *pointer_window;

	//printf ("original window: %p\n",w);
	//printf ("title pointer: %p\n",w->window_title);
	//printf ("title contents: %02XH\n",w->window_title[0]);
    //printf ("Title: %s\n",w->window_title);

	//printf ("after original window\n");



	pointer_window=w;

	while (pointer_window->previous_window!=NULL) {
		//printf ("zxvision_find_first_window. current window %p below window: %p title below: %s\n",pointer_window,pointer_window->previous_window,pointer_window->previous_window->window_title);
		pointer_window=pointer_window->previous_window;
	}

	return pointer_window;
}



//decir si ventana ya existe, en base a su puntero a estructura
int zxvision_if_window_already_exists(zxvision_window *w)
{
	if (zxvision_current_window==NULL) return 0;

	//Primero buscar la inicial

	zxvision_window *pointer_window;

	pointer_window=zxvision_find_first_window_below_this(zxvision_current_window);




	
	while (pointer_window!=NULL) {
		//while (pointer_window!=w) {
		//printf ("window from bottom to top %p. next: %p nombre: %s\n",pointer_window,pointer_window->next_window,pointer_window->window_title);

		if (pointer_window==w) {
			//printf ("Window already exists!!\n");
			return 1;
		}
		

		pointer_window=pointer_window->next_window;
	}	

	return 0;


}

//Borrar ventana si ya existe
//Esto se suele hacer cuando se conmuta entre ventanas en background
void zxvision_delete_window_if_exists(zxvision_window *ventana)
{
    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
    if (zxvision_if_window_already_exists(ventana)) {
        //printf ("Window already exists! We are possibly running on background. Make this the top window\n");

		//menu_generic_message_splash("Background task","OK. Window removed from background");	

		debug_printf (VERBOSE_DEBUG,"Window removed from background");

		menu_speech_tecla_pulsada=1; //Para que no envie a speech la ventana que esta por debajo de la que borramos

		//Al borrar la ventana se leeria la ventana que hay justo debajo
        zxvision_window_delete_this_window(ventana);	

		//Forzar a leer la siguiente ventana que se abra (o sea a la que conmutamos)
		menu_speech_tecla_pulsada=0;
	
    }   
}

void zxvision_cls(zxvision_window *w)
{

	int total_width=w->total_width;
	int total_height=w->total_height;

	int buffer_size=total_width*total_height;


	//Inicializarlo todo con texto blanco

	int i;
	overlay_screen *p;
	p=w->memory;

	for (i=0;i<buffer_size;i++) {
		p->tinta=ESTILO_GUI_TINTA_NORMAL;
		p->papel=ESTILO_GUI_PAPEL_NORMAL;
		p->parpadeo=0;
		p->caracter=' ';

		p++;
	}	
}

void zxvision_new_window_no_check_range(zxvision_window *w,int x,int y,int visible_width,int visible_height,int total_width,int total_height,char *title)
{

	//Alto visible se reduce en 1 - por el titulo de ventana
	
	w->x=x;
	w->y=y;
	w->visible_width=visible_width;
	w->visible_height=visible_height;

	w->total_width=total_width;
	w->total_height=total_height;

	w->offset_x=0;
	w->offset_y=0;

	//Establecer titulo ventana
	strcpy(w->window_title,title);	

	int buffer_size=total_width*total_height;
	w->memory=malloc(buffer_size*sizeof(overlay_screen));

	if (w->memory==NULL) cpu_panic ("Can not allocate memory for window");

	//Inicializarlo todo con texto blanco
	//putchar_menu_overlay(x+j,y+i+1,' ',ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL);

	zxvision_cls(w);

	/*
	int i;
	overlay_screen *p;
	p=w->memory;

	for (i=0;i<buffer_size;i++) {
		p->tinta=ESTILO_GUI_TINTA_NORMAL;
		p->papel=ESTILO_GUI_PAPEL_NORMAL;
		p->parpadeo=0;
		p->caracter=' ';

		p++;
	}
	*/

	//Ventana anterior
	w->previous_window=zxvision_current_window;
	//printf ("Previous window: %p\n",w->previous_window);
	w->next_window=NULL;

	//Ventana siguiente, decir a la anterior, es la actual
	if (zxvision_current_window!=NULL) {
		//printf ("Decimos que siguiente ventana es: %p\n",zxvision_current_window);
		zxvision_current_window->next_window=w;
	}

	//Ventana actual
	zxvision_current_window=w;


	//Decir que al abrir la ventana, las pulsaciones de teclas no se envian por defecto a maquina emulada
	zxvision_keys_event_not_send_to_machine=1;

	ventana_tipo_activa=1;

	

	//Decimos que se puede redimensionar
	w->can_be_resized=1;

	w->can_be_backgrounded=0;
	w->is_minimized=0;
	w->is_maximized=0;
	w->height_before_max_min_imize=visible_height;	
	w->width_before_max_min_imize=visible_width;	

	w->x_before_max_min_imize=x;	
	w->y_before_max_min_imize=y;	

	w->can_use_all_width=0;
	//w->applied_can_use_all_width=0;

	w->can_mouse_send_hotkeys=0;

	w->visible_cursor=0;
	w->cursor_line=0;

	w->upper_margin=0;
	w->lower_margin=0;

	//Por defecto no tiene nombre de guardado de geometria
	w->geometry_name[0]=0;

	//Y textos margen nulos
	//w->text_margin[0]=NULL;


	//Funcion de overlay inicializada a NULL
	w->overlay_function=NULL;


	zxvision_set_draw_window_parameters(w);

	//Redibujar las de debajo
	zxvision_draw_below_windows_nospeech(w);


}

//La quita de la lista pero no libera su memoria usada
void zxvision_window_delete_this_window_no_free_mem(zxvision_window *ventana)
{
		//hacer esta la ventana activa
		/*
		Ventana activa: zxvision_current_window. Si no hay ventanas, vale NULL
		Por debajo: se enlaza con previous_window. Hacia arriba se enlaza con next_window

		Para hacer esta ventana la activa

		NULL *prev* <- A  *prev* <-  -> *next* ventana *prev* <-  -> *next* C <-> D <-> E <-> zxvision_current_window -> NULL


		NULL *prev* <- A  *prev* <-  -> *next*                       *next* C <-> D <-> E <->  <-> ventana -> NULL
		*/

		zxvision_window *next_to_ventana=ventana->next_window;
		zxvision_window *prev_to_ventana=ventana->previous_window;
		//zxvision_window *prev_to_current_window=zxvision_current_window->previous_window;

		//Primero cambiar la anterior a esta, diciendo que nos saltamos "ventana"
		if (prev_to_ventana!=NULL) {
			//La siguiente a esta, sera la siguiente a la ventana actual
			prev_to_ventana->next_window=next_to_ventana;
		}

		//Luego la que era la siguiente a esta "ventana", decir que su anterior es la anterior a "ventana"
		if (next_to_ventana!=NULL) {
			next_to_ventana->previous_window=prev_to_ventana;
		}

		//Si era la de arriba del todo, hacer que apunte a la anterior. Esto tambien cumple el caso de ser la unica ventana
		if (zxvision_current_window==ventana) {
			zxvision_current_window=prev_to_ventana;
			//printf ("Somos la de arriba\n");
			//printf ("Current: %p\n",zxvision_current_window);
			//sleep(5);
		}

		
}

//Elimina la ventana de la lista y libera su memoria usada
void zxvision_window_delete_this_window(zxvision_window *ventana)
{

	zxvision_window_delete_this_window_no_free_mem(ventana); 

	if (ventana->memory!=NULL) free(ventana->memory);


	zxvision_redraw_all_windows();

}


//Elimina todas las ventanas
void zxvision_window_delete_all_windows(void)
{

	//No hacerlo si no hay ventanas
	if (zxvision_current_window==NULL) return;

	zxvision_window *ventana;

	ventana=zxvision_current_window;

	do {

		zxvision_window *previa;

		previa=ventana->previous_window;
		zxvision_window_delete_this_window(ventana);


		//Saltamos a la ventana anterior
		ventana=previa;


	} while (ventana!=NULL);

}


//Elimina todas las ventanas y borra geometria ventanas
void zxvision_window_delete_all_windows_and_clear_geometry(void) 
{

	if (menu_allow_background_windows) zxvision_window_delete_all_windows();

	//Olvidar geometria ventanas
	util_clear_all_windows_geometry();		

}

void zxvision_window_move_this_window_on_top(zxvision_window *ventana)
{
		//hacer esta la ventana activa
		/*
		Ventana activa: zxvision_current_window. Si no hay ventanas, vale NULL
		Por debajo: se enlaza con previous_window. Hacia arriba se enlaza con next_window

		Para hacer esta ventana la activa

		NULL *prev* <- A  *prev* <-  -> *next* ventana *prev* <-  -> *next* C <-> D <-> E <-> zxvision_current_window -> NULL


		NULL *prev* <- A  *prev* <-  -> *next*                       *next* C <-> D <-> E <->  <-> ventana -> NULL
		*/

		zxvision_window_delete_this_window_no_free_mem(ventana);



		//Hasta aqui lo que hemos hecho ha sido quitar nuestra ventana



		//Ahora la subimos arriba del todo
		if (zxvision_current_window!=NULL) {
			zxvision_current_window->next_window=ventana;
		}

		//Y mi actual ahora es la actual que habia de current
		ventana->previous_window=zxvision_current_window;

		//Y no tenemos siguiente, o sea NULL
		ventana->next_window=NULL;

		//Y la actual somos nosotros
		//if (zxvision_current_window!=NULL) {
			zxvision_current_window=ventana;
		//}


		zxvision_redraw_all_windows();
}


//Retorna ventana empezando por la 0 desde arriba hasta abajo
//NULL si no existe
zxvision_window *zxvision_return_n_window_from_top(int indice)
{
	zxvision_window *ventana=zxvision_current_window;

	//printf ("zxvision_return_n_window_from_top. indice: %d\n",indice);

	int i;


	for (i=0;i<indice && ventana!=NULL;i++) {
		//printf ("ventana: %p indice: %d\n",ventana,indice);
		ventana=ventana->previous_window;
	}

	return ventana;


}

void zxvision_new_window(zxvision_window *w,int x,int y,int visible_width,int visible_height,int total_width,int total_height,char *title)
{

	zxvision_new_window_check_range(&x,&y,&visible_width,&visible_height);
	zxvision_new_window_check_static_size_range(&x,&y,&visible_width,&visible_height);
	zxvision_new_window_no_check_range(w,x,y,visible_width,visible_height,total_width,total_height,title);
}

void zxvision_new_window_nocheck_staticsize(zxvision_window *w,int x,int y,int visible_width,int visible_height,int total_width,int total_height,char *title)
{

	zxvision_new_window_check_range(&x,&y,&visible_width,&visible_height);
	zxvision_new_window_no_check_range(w,x,y,visible_width,visible_height,total_width,total_height,title);
}



//Borrar contenido ventana con espacios
void zxvision_clear_window_contents(zxvision_window *w)
{

	int y;

	for (y=0;y<w->total_height;y++) {
		zxvision_fill_width_spaces(w,y);
	}

}

void zxvision_fill_window_transparent(zxvision_window *w)
{
		//Metemos todo el contenido de la ventana con caracter transparente, para que no haya parpadeo
		//en caso de drivers xwindows por ejemplo, pues continuamente redibuja el texto (espacios) y encima el overlay
		//Al meter caracter transparente, el normal_overlay lo ignora y no dibuja ese caracter
		int x,y;
		
		for (y=0;y<w->total_height;y++) {
			for (x=0;x<w->total_width;x++) {
				zxvision_print_string_defaults(w,x,y,"\xff");
			}
		}
}

void zxvision_destroy_window(zxvision_window *w)
{
	zxvision_current_window=w->previous_window;


	//printf ("Setting current window to %p\n",zxvision_current_window);

	//printf ("Next window was %p\n",w->next_window);

	
	ventana_tipo_activa=1;
	zxvision_keys_event_not_send_to_machine=1;

	int antes_menu_speech_tecla_pulsada=menu_speech_tecla_pulsada;

	menu_speech_tecla_pulsada=1; //Para no leer las ventanas de detrás al cerrar la actual

	if (zxvision_current_window!=NULL) {
		//Dibujar las de detras
		//printf ("Dibujando ventanas por detras\n");

		//printf ("proxima ventana antes de dibujar: %p\n",w->next_window);
		zxvision_draw_below_windows_nospeech(w);

		
		zxvision_set_draw_window_parameters(zxvision_current_window);

		//Dibujar ventana que habia debajo
		zxvision_draw_window(zxvision_current_window);
		zxvision_draw_window_contents(zxvision_current_window);
		//printf ("Dibujando ventana de debajo que ahora es de frente\n");
	}

	menu_speech_tecla_pulsada=antes_menu_speech_tecla_pulsada;

	//Liberar memoria cuando ya no se use para nada
	free(w->memory);

	//Decir que esta ventana no tiene siguiente. 
	//TODO: solo podemos hacer destroy de la ultima ventana creada,
	//habria que tener metodo para poder destruir ventana de en medio
	//TODO2: si hacemos esto justo despues de zxvision_current_window=w->previous_window; acaba provocando segfault al redibujar. por que?
	if (zxvision_current_window!=NULL) zxvision_current_window->next_window=NULL;

	//para poder hacer destroy de ventana de en medio seria tan simple como hacer que zxvision_current_window->next_window= fuera el next que habia al principio

}

z80_byte zxvision_read_keyboard(void)
{

	//printf ("antes menu_get_pressed_key\n");
    z80_byte tecla = 0;
	
	if (!mouse_pressed_close_window && !mouse_pressed_background_window && !mouse_pressed_hotkey_window) {
		tecla=menu_get_pressed_key();


		//Si ventana inactiva y se ha pulsado tecla, excepto ESC y excepto background, no leer dicha tecla
		if (tecla!=0 && tecla!=2 && tecla!=3 && zxvision_keys_event_not_send_to_machine==0) {
			//printf ("no leemos tecla en ventana pues esta inactiva\n");
			tecla=0; 
		}
	}

	//Si pulsado boton cerrar ventana, enviar ESC
	if (mouse_pressed_close_window) {
		return 2;
	}

	//Si pulsado boton background ventana, enviar tecla background
	if (mouse_pressed_background_window) {
		//printf ("pulsado background en zxvision_read_keyboard\n");
		//sleep(5);
		return 3;
	}	

	if (mouse_pressed_hotkey_window) {
		mouse_pressed_hotkey_window=0;
		//printf ("Retornamos hoykey %c desde zxvision_read_keyboard\n",mouse_pressed_hotkey_window_key);
		return mouse_pressed_hotkey_window_key;
	}

	return tecla;
}

int zxvision_wait_until_esc(zxvision_window *w)
{
	z80_byte tecla;

	do {
		tecla=zxvision_common_getkey_refresh();
		zxvision_handle_cursors_pgupdn(w,tecla);
	} while (tecla!=2 && tecla!=3);

	return tecla;

}


//escribe la cadena de texto
void zxvision_scanf_print_string(zxvision_window *ventana,char *string,int offset_string,int max_length_shown,int x,int y,int pos_cursor_x)
{

	char cadena_buf[2];

	string=&string[offset_string];


	int rel_x=0;

	//y si offset>0, primer caracter sera '<'
	if (offset_string) {
		zxvision_print_string_defaults(ventana,x,y,"<");
		max_length_shown--;
		x++;
		rel_x++;
		string++;
	}

	for (;max_length_shown;max_length_shown--) {

		if (rel_x==pos_cursor_x) {
			//printf ("Escribir cursor en medio o final en %d %d\n",x,y);
			zxvision_print_string(ventana,x,y,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,1,"_");
		}

		else {
			if ( (*string) !=0) {
				cadena_buf[0]=*string;
				cadena_buf[1]=0;
		
				zxvision_print_string_defaults(ventana,x,y,cadena_buf);
				string++;
			}

			else {
				//meter espacios
				zxvision_print_string_defaults(ventana,x,y," ");
			}
		}
		x++;
		rel_x++;

		
	}

	zxvision_draw_window_contents(ventana);


}

void menu_scanf_cursor_izquierda(int *offset_string,int *pos_cursor_x)
{
                        
				//Desplazar siempre offset que se pueda
				if ((*offset_string)>0) {
					(*offset_string)--;
					//printf ("offset string: %d\n",*offset_string);
				}

				else if ((*pos_cursor_x)>0) (*pos_cursor_x)--;
}

void menu_scanf_cursor_derecha(char *texto,int *pos_cursor_x,int *offset_string,int max_length_shown)
{

			int i;
			i=strlen(texto);

			int pos_final=(*offset_string)+(*pos_cursor_x);


			if (pos_final<i) {  

				if ((*pos_cursor_x)<max_length_shown-1) {
						(*pos_cursor_x)++;
						//printf ("mover cursor\n");
				}
					//Si no mueve cursor, puede que haya que desplazar offset del inicio
				
				else if (i>=max_length_shown) {
					//printf ("Scroll\n");
					(*offset_string)++;
				}
			}
}

//devuelve cadena de texto desde teclado
//max_length contando caracter 0 del final, es decir, para un texto de 4 caracteres, debemos especificar max_length=5
//ejemplo, si el array es de 50, se le debe pasar max_length a 50
//volver_si_fuera_foco dice si vuelve al pulsar en linea de edicion pero mas a la izquierda o derecha de esa zona
int zxvision_scanf(zxvision_window *ventana,char *string,unsigned int max_length,int max_length_shown,int x,int y,int volver_si_fuera_foco)
{


	//Al menos 2 de maximo a mostrar. Si no, salir
	if (max_length_shown<2) {
		debug_printf (VERBOSE_ERR,"Edit field size too small. Returning null string");
		string[0]=0;
		return 2; //Devolvemos escape

	}	

	z80_byte tecla;

	//ajustar offset sobre la cadena de texto visible en pantalla
	int offset_string;

	int j;
	j=strlen(string);
	if (j>max_length_shown-1) offset_string=j-max_length_shown+1;
	else offset_string=0;

	int pos_cursor_x; //Donde esta el cursor

	pos_cursor_x=j;
	if (pos_cursor_x>max_length_shown-1) pos_cursor_x=max_length_shown;


	//max_length ancho maximo del texto, sin contar caracter 0
	//por tanto si el array es de 50, se le debe pasar max_length a 50

	max_length--;

	//cursor siempre al final del texto

	do { 
		zxvision_scanf_print_string(ventana,string,offset_string,max_length_shown,x,y,pos_cursor_x);

		if (menu_multitarea==0) menu_refresca_pantalla();

		menu_espera_tecla();
		//printf ("Despues de espera tecla\n");
		tecla=zxvision_common_getkey_refresh();	
		//printf ("tecla leida=%d\n",tecla);
		int mouse_left_estaba_pulsado=mouse_left;
		menu_espera_no_tecla();



		//si tecla normal, agregar en la posicion del cursor:
		if (tecla>31 && tecla<128) {
			if (strlen(string)<max_length) {
			
				//int i;
				//i=strlen(string);

				int pos_agregar=pos_cursor_x+offset_string;
				//printf ("agregar letra en %d\n",pos_agregar);
				util_str_add_char(string,pos_agregar,tecla);


				//Y mover cursor a la derecha
				menu_scanf_cursor_derecha(string,&pos_cursor_x,&offset_string,max_length_shown);

				//printf ("offset_string %d pos_cursor %d\n",offset_string,pos_cursor_x);

			}
		}

		//tecla derecha
		if (tecla==9) {
				menu_scanf_cursor_derecha(string,&pos_cursor_x,&offset_string,max_length_shown);
				//printf ("offset_string %d pos_cursor %d\n",offset_string,pos_cursor_x);
		}			

		//tecla borrar
		if (tecla==12) {
			//Si longitud texto no es 0
			if (strlen(string)>0) {

				int pos_eliminar=pos_cursor_x+offset_string-1;

				//no borrar si cursor a la izquierda del todo
				if (pos_eliminar>=0) {

					//printf ("borrar\n");
					
                           
					//Eliminar ese caracter
					util_str_del_char(string,pos_eliminar);

					//Y mover cursor a la izquierda
					menu_scanf_cursor_izquierda(&offset_string,&pos_cursor_x);	
					
				}
			}

		}

		//tecla izquierda
		if (tecla==8) {
				menu_scanf_cursor_izquierda(&offset_string,&pos_cursor_x);
				//printf ("offset_string %d pos_cursor %d\n",offset_string,pos_cursor_x);
		}				

		//tecla abajo. borrar todo
		if (tecla==10) {

            string[0]=0;
			offset_string=0;
			pos_cursor_x=0;
	
		}

		//Si pulsado boton
		//printf ("mouse_left: %d\n",mouse_left_estaba_pulsado);
		if (volver_si_fuera_foco && tecla==0 && mouse_left_estaba_pulsado) {			
			//printf ("Pulsado boton izquierdo en zxvision_scanf\n");

			//Si se pulsa mas alla de la zona de edicion
			//printf("mouse_x %d mouse_y %d x %d y %d\n",menu_mouse_x,menu_mouse_y,x,y);

			

			int mouse_y_ventana=menu_mouse_y-1;

			//printf("mouse_x %d mouse_y_ventana %d x %d y %d max_length_shown %d\n",menu_mouse_x,mouse_y_ventana,x,y,max_length_shown);

			if (mouse_y_ventana==y && 
			
				(menu_mouse_x>=x+max_length_shown || menu_mouse_x<x)
			 
			){
				//printf("Pulsado mas a la derecha de la zona de edicion\n");

				//Como si fuera enter , para volver
				tecla=13;
			}

			//O si se pulsa en coordenada y por debajo de input pero dentro de ventana
			if (mouse_y_ventana>y && mouse_y_ventana<y+ventana->visible_height-1) {
				//printf("pulsado por debajo coordenada Y\n");
				tecla=13;
			}
		}


	} while (tecla!=13 && tecla!=15 && tecla!=2);

	//if (tecla==13) printf ("salimos con enter\n");
	//if (tecla==15) printf ("salimos con tab\n");

	menu_reset_counters_tecla_repeticion();
	return tecla;

//papel=7+8;
//tinta=0;

}


int zxvision_generic_message_cursor_down(zxvision_window *ventana)
{

	//int linea_retornar;

	if (ventana->visible_cursor) {

		//Movemos el cursor si es que es posible
		if (ventana->cursor_line<ventana->total_height-1) {
			//printf ("Incrementamos linea cursor\n");
			ventana->cursor_line++;
		}
		else {
			
			return ventana->cursor_line;
		}

		//Ver en que offset estamos
		int offset_y=ventana->offset_y;
		//Y donde esta el cursor
		int cursor=ventana->cursor_line;

		//Y si cursor no esta visible, lo ponemos para que este abajo del todo (hemos de suponer que estaba abajo y ha bajado 1 mas)
		if (cursor<offset_y || cursor>=offset_y+ventana->visible_height-2) {
			ventana->cursor_line=offset_y+ventana->visible_height-2;
			zxvision_send_scroll_down(ventana);
			//printf ("Bajamos linea cursor y bajamos offset\n");
		}
		else {
			//Redibujamos contenido
			//printf ("Solo redibujamos\n");
			zxvision_draw_window_contents(ventana);
			//zxvision_draw_scroll_bars(w);
		}

		return ventana->cursor_line;
	}

	else {	
		zxvision_send_scroll_down(ventana);
		return (ventana->offset_y + ventana->visible_height-3);
	}



}

int zxvision_generic_message_cursor_up(zxvision_window *ventana)
{
	if (ventana->visible_cursor) {

		//Movemos el cursor si es que es posible
		if (ventana->cursor_line>0) {
			//printf ("Decrementamos linea cursor\n");
			ventana->cursor_line--;
		}
		else return ventana->cursor_line;

		//Ver en que offset estamos
		int offset_y=ventana->offset_y;
		//Y donde esta el cursor
		int cursor=ventana->cursor_line;

		//Y si cursor no esta visible, lo ponemos para que este arriba del todo (hemos de suponer que estaba arriba i ha subido 1 mas)
		if (cursor<offset_y || cursor>=offset_y+ventana->visible_height-2) {
			if (offset_y>0) ventana->cursor_line=offset_y-1;
			zxvision_send_scroll_up(ventana);
			//printf ("Subimos linea cursor y subimos offset\n");
		}
		else {
			//Redibujamos contenido
			//printf ("Solo redibujamos\n");
			zxvision_draw_window_contents(ventana);
			//zxvision_draw_scroll_bars(w);
		}

		return ventana->cursor_line;
	}

	else {	
		zxvision_send_scroll_up(ventana);
		return ventana->offset_y;
	}



}		

int menu_origin_x(void)
{
	return 0;
}


int menu_center_x(void)
{
	return scr_get_menu_width()/2;
}

int menu_center_y(void)
{
	return scr_get_menu_height()/2;
}

//Funcion generica para preguntar por un archivo de texto a grabar, con un unico filtro de texto
//Retorna 0 si se cancela
int menu_ask_file_to_save(char *titulo_ventana,char *filtro,char *file_save)
{
	//char file_save[PATH_MAX];

	char *filtros[2];

	filtros[0]=filtro;
    filtros[1]=0;

    int ret;

	ret=menu_filesel(titulo_ventana,filtros,file_save);

	if (ret==1) {

		//Ver si archivo existe y preguntar
		if (si_existe_archivo(file_save)) {

			if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return 0;

        }

		return 1;

	}

	return 0;
}


//Muestra un mensaje en ventana troceando el texto en varias lineas de texto con estilo zxvision
//volver_timeout: si vale 1, significa timeout normal como ventanas splash. Si vale 2, no finaliza, muestra franjas de color continuamente
//return_after_print_text, si no es 0, se usa para que vuelva a la funcion que llama justo despues de escribir texto,
//usado en opciones de mostrar First Aid y luego agregarle opciones de menu tabladas,
//por lo que agrega cierta altura a la ventana. Se agregan tantas lineas como diga el parametro return_after_print_text
void zxvision_generic_message_tooltip(char *titulo, int return_after_print_text,int volver_timeout, int tooltip_enabled, int mostrar_cursor, generic_message_tooltip_return *retorno, int resizable, const char * texto_format , ...)
{
	//Buffer de entrada

        char texto[MAX_TEXTO_GENERIC_MESSAGE];
        va_list args;
        va_start (args, texto_format);
        vsprintf (texto,texto_format, args);
	va_end (args);

	//printf ("input text: %s\n",texto);

	if (volver_timeout) {
		menu_window_splash_counter=0;
		menu_window_splash_counter_ms=0;
	}
	//linea cursor en el caso que se muestre cursor
	//int linea_cursor=0;
	int tecla;


	//texto que contiene cada linea con ajuste de palabra. Al trocear las lineas aumentan
	char buffer_lineas[MAX_LINEAS_TOTAL_GENERIC_MESSAGE][MAX_ANCHO_LINEAS_GENERIC_MESSAGE];

	const int max_ancho_texto=30;

	//Primera linea que mostramos en la ventana
	//int primera_linea=0;

	int indice_linea=0;  //Numero total de lineas??
	int indice_texto=0;
	int ultimo_indice_texto=0;
	int longitud=strlen(texto);


	//Copia del texto de entrada (ya formateado con vsprintf) que se leera solo al copiar clipboard
	//Al pulsar tecla de copy a cliboard, se lee el texto que haya aqui,
	//y no el contenido en el char *texto, pues ese se ha alterado quitando saltos de linea y otros caracteres
	char *menu_generic_message_tooltip_text_initial;


	debug_printf(VERBOSE_INFO,"Allocating %d bytes to initial text",longitud+1);
	menu_generic_message_tooltip_text_initial=malloc(longitud+1);
	if (menu_generic_message_tooltip_text_initial==NULL) {
		debug_printf(VERBOSE_ERR,"Can not allocate buffer for initial text");
	}


	//En caso que se haya podido asignar el buffer de clonado
	if (menu_generic_message_tooltip_text_initial!=NULL) {
		strcpy(menu_generic_message_tooltip_text_initial,texto);
	}

	int ultima_linea_buscada=-1;
	char buffer_texto_buscado[33];

	//int indice_segunda_linea;

	//int texto_no_cabe=0;

	do {
		indice_texto+=max_ancho_texto;

		//temp
		//printf ("indice_linea: %d\n",indice_linea);

		//Controlar final de texto
		if (indice_texto>=longitud) indice_texto=longitud;

		//Si no, miramos si hay que separar por espacios
		else indice_texto=menu_generic_message_aux_wordwrap(texto,ultimo_indice_texto,indice_texto);

		//Separamos por salto de linea, filtramos caracteres extranyos
		indice_texto=menu_generic_message_aux_filter(texto,ultimo_indice_texto,indice_texto);

		//copiar texto
		int longitud_texto=indice_texto-ultimo_indice_texto;


		//snprintf(buffer_lineas[indice_linea],longitud_texto,&texto[ultimo_indice_texto]);


		menu_generic_message_aux_copia(&texto[ultimo_indice_texto],buffer_lineas[indice_linea],longitud_texto);
		buffer_lineas[indice_linea++][longitud_texto]=0;
		//printf ("copiado %d caracteres desde %d hasta %d: %s\n",longitud_texto,ultimo_indice_texto,indice_texto,buffer_lineas[indice_linea-1]);


		//printf ("texto: %s\n",buffer_lineas[indice_linea-1]);

		if (indice_linea==MAX_LINEAS_TOTAL_GENERIC_MESSAGE) {
                        //cpu_panic("Max lines on menu_generic_message reached");
			debug_printf(VERBOSE_INFO,"Max lines on menu_generic_message reached (%d)",MAX_LINEAS_TOTAL_GENERIC_MESSAGE);
			//finalizamos bucle
			indice_texto=longitud;
		}

		ultimo_indice_texto=indice_texto;
		//printf ("ultimo indice: %d %c\n",ultimo_indice_texto,texto[ultimo_indice_texto]);

	} while (indice_texto<longitud);


	//printf ("\ntext after converting to lines: %s\n",texto);


	debug_printf (VERBOSE_INFO,"Read %d lines (word wrapped)",indice_linea);

	int alto_ventana=indice_linea+2;

	int alto_total_ventana=indice_linea;

	if (return_after_print_text) {
		//Darle mas altura	
		alto_ventana +=return_after_print_text;
		alto_total_ventana +=return_after_print_text;
	}

	if (alto_ventana-2>MAX_LINEAS_VENTANA_GENERIC_MESSAGE) {
		alto_ventana=MAX_LINEAS_VENTANA_GENERIC_MESSAGE+2;
		//texto_no_cabe=1;
	}


	//printf ("alto ventana: %d\n",alto_ventana);
	//int ancho_ventana=max_ancho_texto+2;
	int ancho_ventana=max_ancho_texto+2;

	int xventana=menu_center_x()-ancho_ventana/2;
	int yventana=menu_center_y()-alto_ventana/2;

	if (tooltip_enabled==0) {
		menu_espera_no_tecla_con_repeticion();
		cls_menu_overlay();
	}

	zxvision_window *ventana;
	
	//Se puede usar en el bloque else siguiente
	//Definirla aqui a nivel de funcion y no en el bloque else o seria un error
	zxvision_window mi_ventana;

	if (return_after_print_text) {
		//Dado que vamos a volver con la ventana activa que se crea aquí, hay que asignar la estructura en memoria global
		ventana=malloc(sizeof(zxvision_window));
		//printf ("tamanyo memoria ventana %d\n",sizeof(zxvision_window));
		if (ventana==NULL) cpu_panic("Can not allocate memory for zxvision window");
	}

	else {
		ventana=&mi_ventana;
	}
	//printf ("antes de zxvision_new_window\n");		


	zxvision_new_window(ventana,xventana,yventana,ancho_ventana,alto_ventana,
							ancho_ventana-1,alto_total_ventana,titulo);	

	//printf ("despues de zxvision_new_window\n");							

	if (!resizable) zxvision_set_not_resizable(ventana);	

	if (mostrar_cursor) ventana->visible_cursor=1;	

	zxvision_draw_window(ventana);

	//printf ("despues de zxvision_draw_window\n");

				//Decir que se ha pulsado tecla asi no se lee todo cuando el cursor esta visible
				if (ventana->visible_cursor) menu_speech_tecla_pulsada=1;
	int i;
	/*for (i=0;i<indice_linea-primera_linea && i<MAX_LINEAS_VENTANA_GENERIC_MESSAGE;i++) {
		if (mostrar_cursor) {
			menu_escribe_linea_opcion(i,linea_cursor,1,buffer_lineas[i+primera_linea]);
		}
        	else menu_escribe_linea_opcion(i,-1,1,buffer_lineas[i+primera_linea]);
		//printf ("i: %d linea_cursor: %d primera_linea: %d\n",i,linea_cursor,primera_linea);
		//printf ("Linea seleccionada: %d (%s)\n",linea_cursor+primera_linea,buffer_lineas[linea_cursor+primera_linea]);
	}*/

	for (i=0;i<indice_linea;i++) {
		zxvision_print_string(ventana,1,i,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,0,buffer_lineas[i]);
	}

	zxvision_draw_window_contents(ventana);

	if (return_after_print_text) return;

	do {

        if (!menu_multitarea) {
			//printf ("refresca pantalla\n");
			menu_refresca_pantalla();
		}

		/*else {
			menu_cpu_core_loop();
		}*/

		if (volver_timeout) {
			//printf ("antes de tooltip\n");
			zxvision_espera_tecla_timeout_window_splash(volver_timeout);
			if (volver_timeout==2) menu_espera_no_tecla();
			//printf ("despues de tooltip\n");
		}
		else {
			//printf ("Antes espera tecla\n");
			menu_cpu_core_loop();
        	menu_espera_tecla();


		}

//printf ("antes de leer tecla\n");
				tecla=zxvision_read_keyboard();

//printf ("despues de leer tecla\n");             

				//Si se pulsa boton mouse, al final aparece como enter y no es lo que quiero
				//if (tecla==13 && mouse_left && zxvision_keys_event_not_send_to_machine && !mouse_is_dragging) {
				if (tecla==13 && mouse_left) {	
					tecla=0;
				}


		if (volver_timeout) tecla=13;
						
							

		if (tooltip_enabled==0 && tecla) {
			//printf ("Esperamos no tecla\n");
			menu_espera_no_tecla_con_repeticion();
		}


		int contador_pgdnup;

        switch (tecla) {
                        case 8:
						zxvision_handle_cursors_pgupdn(ventana,tecla);
                        break;

                        case 9:
						zxvision_handle_cursors_pgupdn(ventana,tecla);
                        break;						

						//PgUp
						case 24:
							for (contador_pgdnup=0;contador_pgdnup<ventana->visible_height-2;contador_pgdnup++) {
								zxvision_generic_message_cursor_up(ventana);
							}

							//Y recargar ventana para que la relea
							zxvision_draw_window_contents(ventana);
						break;

                    	//PgDn
                    	case 25:
                    		for (contador_pgdnup=0;contador_pgdnup<ventana->visible_height-2;contador_pgdnup++) {
								zxvision_generic_message_cursor_down(ventana);
                        	}
							//Y recargar ventana para que la relea
							zxvision_draw_window_contents(ventana);
                    	break;
						
                                        case 'c':
                                        	menu_copy_clipboard(menu_generic_message_tooltip_text_initial);
                                        	menu_generic_message_splash("Clipboard","Text copied to ZEsarUX clipboard. Go to file utils and press P to paste to a file");

											zxvision_draw_window(ventana);
											zxvision_draw_window_contents(ventana);
                                        break;

						
						/*
						Desactivado para evitar confusiones. Mejor hay que hacer antes copy y paste en file utls
                         case 's':
						 	menu_save_text_to_file(menu_generic_message_tooltip_text_initial,"Save Text");
                 											zxvision_draw_window(ventana);
											zxvision_draw_window_contents(ventana);
                        break;
						*/


						
					//Buscar texto
					case 'f':
					case 'n':

						if (tecla=='f' || ultima_linea_buscada==-1) {

							buffer_texto_buscado[0]=0;
			        			menu_ventana_scanf("Text to find",buffer_texto_buscado,33);

							//ultima_linea_buscada=0; //Si lo pusiera a 0, no encontraria nada en primera linea
							//pues no se cumpliria la condicion de mas abajo de i>ultima_linea_buscada

							ultima_linea_buscada=-1;

						}

						int i;
						char *encontrado=NULL;
						for (i=0;i<indice_linea;i++) {
							debug_printf(VERBOSE_DEBUG,"Searching text on line %d: %s",i,buffer_lineas[i]);
							encontrado=util_strcasestr(buffer_lineas[i], buffer_texto_buscado);
							if (encontrado && i>ultima_linea_buscada) {
								break;
							}
						}

						if (encontrado) {
							ultima_linea_buscada=i;
							//mover cursor hasta ahi
							//primera_linea=0;
							//linea_cursor=0;

							//printf ("mover cursor hasta linea: %d\n",ultima_linea_buscada);

							//Mostramos cursor para poder indicar en que linea se ha encontrado el texto
							mostrar_cursor=1;

							ventana->visible_cursor=1;

							ventana->cursor_line=i;

							//Si no esta visible, cambiamos offset
							zxvision_set_offset_y_visible(ventana,i);
							//if (i<ventana->offset_y || i>=ventana->offset_y+ventana->visible_height-2) zxvision_set_offset_y(ventana,i);

							/*int contador;
							for (contador=0;contador<ultima_linea_buscada;contador++) {
									primera_linea=menu_generic_message_cursor_abajo_mostrar_cursor(primera_linea,alto_ventana,indice_linea,mostrar_cursor,&linea_cursor);
							}*/

							//menu_speech_tecla_pulsada=0;
						}

						else {
							menu_speech_tecla_pulsada=0; //para decir que siempre se escuchara el mensaje
							menu_warn_message("Text not found");
						}

						zxvision_draw_window(ventana);
						zxvision_draw_window_contents(ventana);


					break;

					//Movimiento y redimensionado ventana con teclado
					                        //derecha
                    case 'Q':
					case 'A':
					case 'O':
					case 'P':
                    case 'W':
					case 'S':
					case 'K':
					case 'L':
						zxvision_handle_cursors_pgupdn(ventana,tecla);
                    break;		
					
				}

	//Salir con Enter o ESC o fin de tooltip
	} while (tecla!=13 && tecla!=2 && tooltip_enabled==0);

	if (retorno!=NULL) {
		int linea_final;

		//printf ("mostrar cursor %d cursor_line %d ventana->offset_x %d\n",mostrar_cursor,ventana->cursor_line,ventana->offset_x);

		if (mostrar_cursor) linea_final=ventana->cursor_line;
		else linea_final=ventana->offset_x;


		strcpy(retorno->texto_seleccionado,buffer_lineas[linea_final]);
		retorno->linea_seleccionada=linea_final;

		// int estado_retorno; //Retorna 1 si sale con enter. Retorna 0 si sale con ESC
		if (tecla==2) retorno->estado_retorno=0;
		else retorno->estado_retorno=1;

		//printf ("\n\nLinea seleccionada: %d (%s)\n",linea_final,buffer_lineas[linea_final]);

	}

    cls_menu_overlay();
	zxvision_destroy_window(ventana);



        if (tooltip_enabled==0) menu_espera_no_tecla_con_repeticion();

	


	if (menu_generic_message_tooltip_text_initial!=NULL) {
		debug_printf(VERBOSE_INFO,"Freeing previous buffer for initial text");
		free(menu_generic_message_tooltip_text_initial);
	}
	//if (tooltip_enabled==0)



}

//Retorna 1 si se debe perder 1 de ancho visible por la linea de scroll vertical (lo habitual)
//Retorna 0 si no
int zxvision_get_minus_width_byscrollvbar(zxvision_window *w)
{
	if (w->can_use_all_width==0) return 1;
	else return 0;
}

int zxvision_get_effective_width(zxvision_window *w)
{
	//Ancho del contenido es 1 menos, por la columna a la derecha de margen
	return w->visible_width-zxvision_get_minus_width_byscrollvbar(w);
}

int zxvision_get_effective_height(zxvision_window *w)
{
	//Alto del contenido es 2 menos, por el titulo de ventana y la linea por debajo de margen
	return w->visible_height-2;
}

int zxvision_if_vertical_scroll_bar(zxvision_window *w)
{
	if (w->can_use_all_width==1) {
		//w->applied_can_use_all_width=1;
		return 0;
	}
	int effective_height=zxvision_get_effective_height(w);
	if (w->total_height>effective_height && w->visible_height>=6) return 1;

	return 0;
}

int zxvision_if_horizontal_scroll_bar(zxvision_window *w)
{
	int effective_width=zxvision_get_effective_width(w);
	if (w->total_width>effective_width && w->visible_width>=6) return 1;

	return 0;
}

void zxvision_draw_vertical_scroll_bar(zxvision_window *w,int estilo_invertido)
{

	int effective_height=zxvision_get_effective_height(w);
		//Dibujar barra vertical
		int valor_parcial=w->offset_y+effective_height;
		if (valor_parcial<0) valor_parcial=0;

		//Caso especial arriba del todo cero, valor_parcial es 0 y no contemplamos el alto visible
		//if (w->offset_y==0) valor_parcial=0;		

		int valor_total=w->total_height;
		if (valor_total<=0) valor_total=1; //Evitar divisiones por cero o negativos


		int porcentaje=(valor_parcial*100)/(1+valor_total);  

		//Caso especial arriba del todo
		if (w->offset_y==0) {
			//printf ("Scroll vertical cursor is at the minimum\n");
			porcentaje=0;
		}		

		//Caso especial abajo del todo
		if (w->offset_y+(w->visible_height)-2==w->total_height) { //-2 de perder linea titulo y linea scroll
			//printf ("Scroll vertical cursor is at the maximum\n");
			porcentaje=100;
		}

		menu_ventana_draw_vertical_perc_bar(w->x,w->y,w->visible_width,w->visible_height-1,porcentaje,estilo_invertido);	
}

void zxvision_draw_horizontal_scroll_bar(zxvision_window *w,int estilo_invertido)
{

	int effective_width=w->visible_width-1;
	//Dibujar barra horizontal
		int valor_parcial=w->offset_x+effective_width;
		if (valor_parcial<0) valor_parcial=0;

		//Si offset es cero, valor_parcial es 0 y no contemplamos el ancho visible
		//if (w->offset_x==0) valor_parcial=0;

		int valor_total=w->total_width;
		if (valor_total<=0) valor_total=1; //Evitar divisiones por cero o negativos


		int porcentaje=(valor_parcial*100)/(1+valor_total);  

		//Caso especial izquierda del todo
		if (w->offset_x==0) {
			//printf ("Scroll horizontal cursor is at the minimum\n");
			porcentaje=0;
		}		

		//Caso especial derecha del todo
		if (w->offset_x+(w->visible_width)-1==w->total_width) { //-1 de perder linea scroll
			//printf ("Scroll horizontal cursor is at the maximum\n");
			porcentaje=100;
		}

		menu_ventana_draw_horizontal_perc_bar(w->x,w->y,effective_width,w->visible_height,porcentaje,estilo_invertido);
}

void zxvision_draw_scroll_bars(zxvision_window *w)
{
	//Barras de desplazamiento
	//Si hay que dibujar barra derecha de desplazamiento vertical
	//int effective_height=zxvision_get_effective_height(w);
	//int effective_width=zxvision_get_effective_width(w);
	//int effective_width=w->visible_width-1;

	if (zxvision_if_vertical_scroll_bar(w)) {
		zxvision_draw_vertical_scroll_bar(w,0);
	}

	if (zxvision_if_horizontal_scroll_bar(w)) {
		zxvision_draw_horizontal_scroll_bar(w,0);
	}	
}

void zxvision_draw_window(zxvision_window *w)
{
	menu_dibuja_ventana(w->x,w->y,w->visible_width,w->visible_height,w->window_title);


	//Ver si se puede redimensionar
	//Dado que cada vez que se dibuja ventana, la marca de resize se establece por defecto a desactivada
	cuadrado_activo_resize=w->can_be_resized;
	ventana_activa_tipo_zxvision=1;


	//Si no hay barras de desplazamiento, alterar scroll horiz o vertical segun corresponda
	if (!zxvision_if_horizontal_scroll_bar(w)) {
		//printf ("no hay barra scroll horizontal y por eso ponemos offset x a 0\n");
		w->offset_x=0;
	}
	if (!zxvision_if_vertical_scroll_bar(w)) {
		//printf ("no hay barra scroll vertical y por eso ponemos offset y a 0\n");
		w->offset_y=0;
	}

	zxvision_draw_scroll_bars(w);

	//Mostrar boton de minimizar
	menu_dibuja_ventana_botones();

	//Mostrar boton background
	menu_dibuja_ventana_boton_background(w->x,w->y,w->visible_width,w);


}

void zxvision_set_not_resizable(zxvision_window *w)
{
	//Decimos que no se puede redimensionar
	//printf ("set not resizable\n");
	cuadrado_activo_resize=0;

	w->can_be_resized=0;
}

void zxvision_set_resizable(zxvision_window *w)
{
	//Decimos que se puede redimensionar
	//printf ("set resizable\n");
	cuadrado_activo_resize=1;

	w->can_be_resized=1;
}

void zxvision_set_offset_x(zxvision_window *w,int offset_x)
{
	//Si se pasa por la izquierda
	if (offset_x<0) return;

	//Si se pasa por la derecha
	if (offset_x+w->visible_width-1>w->total_width) return; //-1 porque se pierde 1 a la derecha con la linea scroll

	w->offset_x=offset_x;	

	zxvision_draw_window_contents(w);
	zxvision_draw_scroll_bars(w);
}

int zxvision_maximum_offset_y(zxvision_window *w)
{
	return w->total_height+2-w->visible_height;
}

void zxvision_set_offset_y(zxvision_window *w,int offset_y)
{
	//Si se pasa por arriba
	if (offset_y<0) return;

	//Si se pasa por abajo

	//if (offset_y+w->visible_height-2 > (w->total_height ) ) return; //-2 porque se pierde 2 linea scroll y la linea titulo
	int maximum_offset=zxvision_maximum_offset_y(w);

	if (offset_y>maximum_offset) {
		//printf ("Maximum offset y reached\n");
		return;
	}

	w->offset_y=offset_y;	

	zxvision_draw_window_contents(w);
	zxvision_draw_scroll_bars(w);
}

void zxvision_set_offset_y_or_maximum(zxvision_window *w,int offset_y)
{
        int maximum_offset=zxvision_maximum_offset_y(w);

        if (offset_y>maximum_offset) {
                //printf ("Maximum offset y reached. Setting maximum\n");
		offset_y=maximum_offset;
        }

	zxvision_set_offset_y(w,offset_y);
}



//Si no esta visible, cambiamos offset
void zxvision_set_offset_y_visible(zxvision_window *w,int y)
{

	int linea_final;

	//El cursor esta por arriba. Decimos que este lo mas arriba posible
	if (y<w->offset_y) {
		linea_final=y;
		//printf ("adjust verticall scroll por arriba to %d\n",linea_final);
		
	}

	//El cursor esta por abajo. decimos que el cursor este lo mas abajo posible
	else if (y>=w->offset_y+w->visible_height-2) {
		linea_final=y-(w->visible_height-2)+1;
		//Ejemplo
		//total height 12
		//visble 10->efectivos son 8
		//establecemos a linea 7
		//linea_final=7-(10-2)+1 = 7-8+1=0

		//printf ("adjust verticall scroll por abajo to %d\n",linea_final);
	}

	else return;

	int ultima_linea_scroll=w->total_height-(w->visible_height-2);
	
	/*
	Ejemplo: visible_height 10-> efectivos son 8
	total_height 12
	podremos hacer 4 veces scroll
	12-(10-2)=12-8=4
	*/

	if (ultima_linea_scroll<0) ultima_linea_scroll=0;
	if (linea_final>ultima_linea_scroll) linea_final=ultima_linea_scroll;

	//printf ("final scroll %d\n",linea_final);

	zxvision_set_offset_y(w,linea_final);


}


void zxvision_send_scroll_down(zxvision_window *w)
{
	if (w->offset_y<(w->total_height-1)) {
						zxvision_set_offset_y(w,w->offset_y+1);
	}	
}


void zxvision_send_scroll_up(zxvision_window *w)
{
	if (w->offset_y>0) {
		zxvision_set_offset_y(w,w->offset_y-1);
	}
}


void zxvision_send_scroll_left(zxvision_window *w)
{
	if (w->offset_x>0) {
		zxvision_set_offset_x(w,w->offset_x-1);
	}
}

void zxvision_send_scroll_right(zxvision_window *w)
{
	if (w->offset_x<(w->total_width-1)) {
		zxvision_set_offset_x(w,w->offset_x+1);
	}
}

int zxvision_cursor_out_view(zxvision_window *ventana)
{

        //int linea_retornar;

        if (ventana->visible_cursor) {

                //Ver en que offset estamos
                int offset_y=ventana->offset_y;
                //Y donde esta el cursor
                int cursor=ventana->cursor_line;

                //Y si cursor no esta visible, lo ponemos para que este abajo del todo (hemos de suponer que estaba abajo y ha bajado 1 mas)
                if (cursor<offset_y || cursor>=offset_y+ventana->visible_height-2) {
			return 1;
                }
        }

        return 0;

}


//Retorna 1 si ha reajustado el cursor
int zxvision_adjust_cursor_bottom(zxvision_window *ventana)
{

	//int linea_retornar;

	if (zxvision_cursor_out_view(ventana)) {

		//Ver en que offset estamos
		int offset_y=ventana->offset_y;
		//Y donde esta el cursor
		//int cursor=ventana->cursor_line;

			//printf ("Reajustar cursor\n");
			ventana->cursor_line=offset_y+ventana->visible_height-2-ventana->upper_margin-ventana->lower_margin;
			return 1;
	}

	return 0;

}

//Retorna 1 si ha reajustado el cursor
int zxvision_adjust_cursor_top(zxvision_window *ventana)
{

	if (zxvision_cursor_out_view(ventana)) {

		//Ver en que offset estamos
		int offset_y=ventana->offset_y;
		//Y donde esta el cursor
		//int cursor=ventana->cursor_line;

			if (offset_y>0) {
				//printf ("Reajustar cursor\n");
				ventana->cursor_line=offset_y-1;
				return 1;
			}

	}

	return 0;


}

int zxvision_out_bonds(int x,int y,int ancho,int alto)
{
	if (x<0 || y<0) return 1;

	if (x+ancho>scr_get_menu_width() || y+alto>scr_get_menu_height()) return 1;

	return 0;
}



//Dibujar todas las ventanas que hay debajo de esta en cascada, desde la mas antigua hasta arriba
void zxvision_draw_below_windows(zxvision_window *w)
{
	//Primero ir a buscar la de abajo del todo
	zxvision_window *pointer_window;

	//printf ("original window: %p\n",w);
        //printf ("\noriginal window: %p. Title: %s\n",w,w->window_title);




	pointer_window=zxvision_find_first_window_below_this(w);

	//printf ("after while pointer_window->previous_window!=NULL\n");

	int antes_ventana_tipo_activa=ventana_tipo_activa;
	ventana_tipo_activa=0; //Redibujar las de debajo como inactivas

	//Redibujar diciendo que estan por debajo
	ventana_es_background=1;

	//Y ahora de ahi hacia arriba
	//Si puntero es NULL, es porque se ha borrado alguna ventana de debajo. Salir
	//esto puede suceder haciendo esto:
	//entrar a debug cpu-breakpoints. activarlo y dejar que salte el tooltip
	//ir a ZRCP. Meter breakpoint que de error, ejemplo: "sb 1 pc=kkkk("
	//ir a menu. enter y enter. Se provoca esta situacion. Por que? Probablemente porque se ha llamado a destroy window y
	//se ha generado una ventana de error cuando habia un tooltip abierto
	//Ver comentarios en zxvision_destroy_window

	//if (pointer_window==NULL) {
	//	printf ("Pointer was null before loop redrawing below windows\n");
	//}	

	//printf ("\nStart loop redrawing below windows\n");

	//no mostrar mensajes de error pendientes
	//si eso se hiciera, aparece en medio de la lista de ventanas una que apunta a null y de ahi la condicion pointer_window!=NULL
	//asi entonces dicha condicion pointer_window!=NULL ya no seria necesaria pero la dejamos por si acaso...
	int antes_no_dibuja_ventana_muestra_pending_error_message=no_dibuja_ventana_muestra_pending_error_message;
	no_dibuja_ventana_muestra_pending_error_message=1;

	while (pointer_window!=w && pointer_window!=NULL) {
		//printf ("window from bottom to top %p\n",pointer_window);
		//printf ("window from bottom to top %p. name: %s\n",pointer_window,pointer_window->window_title);
		
		zxvision_draw_window(pointer_window);
	    zxvision_draw_window_contents(pointer_window);


		pointer_window=pointer_window->next_window;
	}


	no_dibuja_ventana_muestra_pending_error_message=antes_no_dibuja_ventana_muestra_pending_error_message;



	ventana_es_background=0;
	ventana_tipo_activa=antes_ventana_tipo_activa;
}


int zxvision_drawing_in_background=0;


//Llama al overlay de la ventana, si es que existe
void zxvision_draw_overlay_if_exists(zxvision_window *w)
{
		void (*overlay_function)(void);
		overlay_function=w->overlay_function;

		//printf ("Funcion overlay: %p. ventana: %s. current window: %p\n",overlay_function,w->window_title,zxvision_current_window);		


		//Esto pasa en ventanas que por ejemplo actualizan no a cada frame, al menos refrescar aqui con ultimo valor				

		if (overlay_function!=NULL) {
			//printf ("llamando a funcion overlay %p\n",overlay_function);
			
			overlay_function(); //llamar a funcion overlay
		}
}

//Dibujar todos los overlay de las ventanas que hay debajo de esta en cascada, desde la mas antigua hasta arriba, pero llamando solo las que tienen overlay
void zxvision_draw_overlays_below_windows(zxvision_window *w)
{


	//Primero ir a buscar la de abajo del todo
	zxvision_window *pointer_window;


	//if (w!=NULL) printf ("\nDraw with overlay. original window: %p. Title: %s\n",w,w->window_title);


	//Si no hay ventanas, volver
	if (zxvision_current_window==NULL) return;

	pointer_window=w;

	while (pointer_window->previous_window!=NULL) {
			//debug_printf (VERBOSE_PARANOID,"zxvision_draw_overlays_below_windows below window: %p",pointer_window->previous_window);
			pointer_window=pointer_window->previous_window;
	}

	int antes_ventana_tipo_activa=ventana_tipo_activa;
	ventana_tipo_activa=0; //Redibujar las de debajo como inactivas

	//Redibujar diciendo que estan por debajo
	ventana_es_background=1;

	//Y ahora de ahi hacia arriba, incluido la ultima


	//printf ("\n");

	zxvision_drawing_in_background=1;


	//Dibujar todas ventanas excepto la de mas arriba. 
	//while (pointer_window!=w && pointer_window!=NULL) {

	//Dibujar todas ventanas. 
	while (pointer_window!=NULL) {
		//while (pointer_window!=w) {
				//printf ("window from bottom to top %p. next: %p nombre: %s\n",pointer_window,pointer_window->next_window,pointer_window->window_title);

		//Somos la ventana de mas arriba 
		if (pointer_window==w) {
			ventana_es_background=0;
			ventana_tipo_activa=antes_ventana_tipo_activa;
		};

		//en principio no hace falta. Ya se redibuja por el redibujado normal
		//zxvision_draw_window(pointer_window);

		//Dibujamos contenido anterior, ya que draw_window la borra con espacios
		//en principio no hace falta. Ya se redibuja por el redibujado normal
		//zxvision_draw_window_contents(pointer_window);


		zxvision_draw_overlay_if_exists(pointer_window);
	
		

		pointer_window=pointer_window->next_window;
	}



	zxvision_drawing_in_background=0;

	ventana_es_background=0;
	ventana_tipo_activa=antes_ventana_tipo_activa;

}

void zxvision_message_put_window_background(void)
{
	//Conviene esperar no tecla porque a veces esta ventana splash no aparece
	menu_espera_no_tecla();
	//menu_generic_message_splash("Background task","OK. Window put on the background");
	debug_printf (VERBOSE_DEBUG,"OK. Window put on the background");
}

//Pone en la estructura de ventana la funcion de overlay que haya activa ahora
//Siempre que no sea la de normal overlay 
void zxvision_set_window_overlay_from_current(zxvision_window *ventana)
{

	/*
	realmente comparar con la de normal overlay nos sirve para evitar que ventanas que no tienen overlay
	pero que pueden ir a background (como debug cpu) les ponga como overlay el propio de normal overlay
	Es un poco chapucero pero funciona
	TODO: seria mejor indicar con un flag (por defecto a 0) que la ventana tiene un overlay activo diferente de normal_overlay
	*/
	if (menu_overlay_function!=normal_overlay_texto_menu) {
		ventana->overlay_function=menu_overlay_function;
	}
}


void zxvision_redraw_window_on_move(zxvision_window *w)
{
	cls_menu_overlay();
	zxvision_draw_below_windows_nospeech(w);
	zxvision_draw_window(w);
	zxvision_draw_window_contents(w);
}

void zxvision_redraw_all_windows(void)
{
	if (zxvision_current_window!=NULL) {
		zxvision_redraw_window_on_move(zxvision_current_window);
	}
}

void zxvision_set_x_position(zxvision_window *w,int x)
{
	if (zxvision_out_bonds(x,w->y,w->visible_width,w->visible_height)) return;

	w->x=x;
	zxvision_redraw_window_on_move(w);

}

void zxvision_set_y_position(zxvision_window *w,int y)
{
	if (zxvision_out_bonds(w->x,y,w->visible_width,w->visible_height)) return;

	w->y=y;
	zxvision_redraw_window_on_move(w);

}


void zxvision_set_visible_width(zxvision_window *w,int visible_width)
{
	if (zxvision_out_bonds(w->x,w->y,visible_width,w->visible_height)) {
		//printf ("Window out of bounds trying to set width\n");
		return;
	}

	if (visible_width<1) return;

	w->visible_width=visible_width;
	zxvision_redraw_window_on_move(w);

}

void zxvision_set_visible_height(zxvision_window *w,int visible_height)
{
	if (zxvision_out_bonds(w->x,w->y,w->visible_width,visible_height)) return;

	if (visible_height<1) return;

	w->visible_height=visible_height;
	zxvision_redraw_window_on_move(w);

}

//Dice si unas coordenadas están dentro de una ventana concreta
int zxvision_coords_in_window(zxvision_window *w,int x,int y)
{

	int other_x=w->x;
	int other_y=w->y;
	int other_width=w->visible_width;
	int other_height=w->visible_height;

	//printf ("x %d y %d other x %d y %d w %d h %d\n",x,y,other_x,other_y,other_width,other_height); 

	if (x>=other_x && x<other_x+other_width &&
		y>=other_y && y<other_y+other_height
		)
		{
			return 1;
		}

	return 0;

}

//Dice si las coordenadas de ventana indicada coinciden con cualquiera de las ventanas que tenga encima
int zxvision_coords_in_superior_windows(zxvision_window *w,int x,int y)
{
	if (!menu_allow_background_windows) return 0;

	if (w==NULL) return 0;

	if (zxvision_current_window==w) return 0;

	do {
		zxvision_window *superior_window;

		superior_window=w->next_window;

		if (superior_window!=NULL) {

			if (zxvision_coords_in_window(superior_window,x,y)) return 1;

		}


		w=superior_window;

	} while (w!=zxvision_current_window && w!=NULL);

	return 0;

}



//Dice si las coordenadas indicadas coinciden con cualquiera de las ventanas que estén en las ventanas de debajo de la indicada
//Retorna la ventana implicada, o NULL si no
zxvision_window *zxvision_coords_in_below_windows(zxvision_window *w,int x,int y)
{
	if (!menu_allow_background_windows) return NULL;

	if (w==NULL) return NULL;

	//Empezamos de arriba hacia abajo

	do {
		zxvision_window *lower_window;

		lower_window=w->previous_window;

		if (lower_window!=NULL) {

			if (zxvision_coords_in_window(lower_window,x,y)) return lower_window;

		}


		w=lower_window;

	} while (w!=NULL);

	return NULL;

}

//Dice si las coordenadas de ventana indicada coinciden con la zona ocupada por la ventana current
//esto se usa cuando está activado background window, para que las ventanas por detrás no tapen a la ventana actual
//Realmente es un poco chapuza, aunque efectivo. Lo ideal seria que las ventanas en background no se redibujasen
//desde una funcion de overlay, sino de otra manera mas limpia y ordenada
//Esto no impide que los pixeles de los overlay puedan pasar por encima de cualquier ventana (excepto la current, pues llamamos a aqui tambien)
//Creo ademas que esta funcion ya no se usa
/*
int zxvision_coords_in_front_window(zxvision_window *w,int x,int y)
{

	if (!menu_allow_background_windows) return 0;

	if (zxvision_current_window==NULL) return 0;

	if (zxvision_current_window==w) return 0;

	return zxvision_coords_in_window(zxvision_current_window,x,y);


}
*/

void zxvision_draw_window_contents(zxvision_window *w)
{
	int width,height;

	width=zxvision_get_effective_width(w);

	//Alto del contenido es 2 menos, por el titulo de ventana y la linea por debajo de margen
	height=zxvision_get_effective_height(w);

	int x,y;

	for (y=0;y<height;y++) {
		for (x=0;x<width;x++) {

			//printf ("x %d y %d\n",x,y);
		
			int xdestination=w->x+x;
			int ydestination=(w->y)+1+y; //y +1 porque empezamos a escribir debajo del titulo

			//Ver si caracter final tiene ventana por encima
			int ventana_encima=zxvision_coords_in_superior_windows(w,xdestination,ydestination);

			
			//obtener caracter
			int out_of_bonds=0;

			int offset_x_final=x+w->offset_x;
			if (offset_x_final>=w->total_width) out_of_bonds=1;

			int offset_y_final=y+w->offset_y;

			int lower_margin_starts_at=height-(w->lower_margin);

			//printf ("sonda 1\n");
				
				//Texto leyenda parte superior
				if (y<w->upper_margin) {
					offset_y_final=y;
				}
				//Texto leyenda parte inferior
				else if (y>=lower_margin_starts_at) {
					int effective_height=height-w->upper_margin-w->lower_margin;
					int final_y=y-effective_height;
					offset_y_final=final_y;
				}
				else {
					offset_y_final +=w->lower_margin; //Dado que ya hemos pasado la parte superior, saltar la inferior
				}
			//printf ("sonda 2\n");

			if (offset_y_final>=w->total_height) out_of_bonds=1;

			if (!out_of_bonds) {

				//Origen de donde obtener el texto
				int offset_caracter;
				
				offset_caracter=((offset_y_final)*w->total_width)+offset_x_final;

				overlay_screen *caracter;
				caracter=w->memory;
				caracter=&caracter[offset_caracter];

				z80_byte caracter_escribir=caracter->caracter;

				int tinta=caracter->tinta;
				int papel=caracter->papel;

				//Si esta linea cursor visible
				int linea_cursor=w->cursor_line;
				//tener en cuenta desplazamiento de margenes superior e inferior
				linea_cursor +=w->lower_margin;
				linea_cursor +=w->upper_margin;
				if (w->visible_cursor && linea_cursor==offset_y_final) {
					tinta=ESTILO_GUI_TINTA_SELECCIONADO;
					papel=ESTILO_GUI_PAPEL_SELECCIONADO;
				} 
			
				//Chapucilla para evitar que las ventanas en background sobreescriban a la current
				//if (!zxvision_coords_in_front_window(w,xdestination,ydestination)) {

				//Chapucilla para evitar que las ventanas en background sobreescriban a cualquiera que haya encima
				if (!ventana_encima) {
				//if (!zxvision_coords_in_superior_windows(w,xdestination,ydestination)) {

				//printf ("antes de putchar\n");
				putchar_menu_overlay_parpadeo(xdestination,ydestination,
					caracter_escribir,tinta,papel,caracter->parpadeo);

					//printf ("despues de putchar\n");
				}
			}

			//Fuera de rango. Metemos espacio
			else {
				//printf ("fuera de rango\n");
				if (!ventana_encima) {
				putchar_menu_overlay_parpadeo(xdestination,ydestination,
				' ',ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,0);
				}
			}
			//printf ("sonda 3\n");

		}

		//printf ("sonda 4\n");
	}


}


//Funcion derivada de zxvision_draw_window_contents
//usada para obtener los hotkeys de mouse
void zxvision_get_character_at_mouse(zxvision_window *w,int x,int y,overlay_screen *caracter_retorno)
{


	int height;

	//Alto del contenido es 2 menos, por el titulo de ventana y la linea por debajo de margen
	height=zxvision_get_effective_height(w);



	//for (y=0;y<height;y++) {
		
		//for (x=0;x<width;x++) {

			//printf ("x %d y %d\n",x,y);
		
			//int xdestination=w->x+x;
			//int ydestination=(w->y)+1+y; //y +1 porque empezamos a escribir debajo del titulo

			//Ver si caracter final tiene ventana por encima
			int ventana_encima=0;

			
			//obtener caracter
			int out_of_bonds=0;

			int offset_x_final=x+w->offset_x;
			if (offset_x_final>=w->total_width) out_of_bonds=1;

			int offset_y_final=y+w->offset_y;

			int lower_margin_starts_at=height-(w->lower_margin);

			//printf ("sonda 1\n");
				
				//Texto leyenda parte superior
				if (y<w->upper_margin) {
					offset_y_final=y;
				}
				//Texto leyenda parte inferior
				else if (y>=lower_margin_starts_at) {
					int effective_height=height-w->upper_margin-w->lower_margin;
					int final_y=y-effective_height;
					offset_y_final=final_y;
				}
				else {
					offset_y_final +=w->lower_margin; //Dado que ya hemos pasado la parte superior, saltar la inferior
				}
			//printf ("sonda 2\n");

			if (offset_y_final>=w->total_height) out_of_bonds=1;

			if (!out_of_bonds) {

				//Origen de donde obtener el texto
				int offset_caracter;
				
				offset_caracter=((offset_y_final)*w->total_width)+offset_x_final;

				overlay_screen *caracter;
				caracter=w->memory;
				caracter=&caracter[offset_caracter];

				z80_byte caracter_escribir=caracter->caracter;

				//Si esta linea cursor visible
				int linea_cursor=w->cursor_line;
				//tener en cuenta desplazamiento de margenes superior e inferior
				linea_cursor +=w->lower_margin;
				linea_cursor +=w->upper_margin;
			
				//Chapucilla para evitar que las ventanas en background sobreescriban a la current
				//if (!zxvision_coords_in_front_window(w,xdestination,ydestination)) {

				//Chapucilla para evitar que las ventanas en background sobreescriban a cualquiera que haya encima
				if (!ventana_encima) {
				//if (!zxvision_coords_in_superior_windows(w,xdestination,ydestination)) {

				//printf ("antes de putchar\n");

				caracter_retorno->caracter=caracter_escribir;

				caracter_retorno->tinta=caracter->tinta;
				caracter_retorno->papel=caracter->papel;
				caracter_retorno->parpadeo=caracter->parpadeo;
				return;
				//putchar_menu_overlay_parpadeo(xdestination,ydestination,
				//	caracter_escribir,tinta,papel,caracter->parpadeo);

					//printf ("despues de putchar\n");


				}
			}

			//Fuera de rango. Retornamos 0
			else {
				//printf ("fuera de rango\n");
				if (!ventana_encima) {
					caracter_retorno->caracter=0;
				}
			}


	caracter_retorno->caracter=0;

	


}


void zxvision_draw_window_contents_no_speech(zxvision_window *ventana)
{
                //No queremos que el speech vuelva a leer la ventana, solo cargar ventana
		int antes_menu_speech_tecla_pulsada=menu_speech_tecla_pulsada;
                menu_speech_tecla_pulsada=1;
                zxvision_draw_window_contents(ventana);

		menu_speech_tecla_pulsada=antes_menu_speech_tecla_pulsada;

}


//Escribir caracter en la memoria de la ventana
void zxvision_print_char(zxvision_window *w,int x,int y,overlay_screen *caracter)
{
	//Comprobar limites
	if (x>=w->total_width || x<0 || y>=w->total_height || y<0) return;

	//Sacamos offset
	int offset=(y*w->total_width)+x;



	//Puntero
	overlay_screen *p;

	p=w->memory; //Puntero inicial

	p=&p[offset]; //Puntero con offset

	p->tinta=caracter->tinta;
	p->papel=caracter->papel;
	p->parpadeo=caracter->parpadeo;
	p->caracter=caracter->caracter;

	
}

void zxvision_print_char_simple(zxvision_window *w,int x,int y,int tinta,int papel,int parpadeo,z80_byte caracter)
{
	overlay_screen caracter_aux;
	caracter_aux.caracter=caracter;
	caracter_aux.tinta=tinta;
	caracter_aux.papel=papel;
	caracter_aux.parpadeo=parpadeo;		

	zxvision_print_char(w,x,y,&caracter_aux);
}

void zxvision_print_string(zxvision_window *w,int x,int y,int tinta,int papel,int parpadeo,char *texto)
{


	int inverso_letra=0;
	int minuscula_letra=1;
	int era_utf=0;

	while (*texto) {

		overlay_screen caracter_aux;
		caracter_aux.caracter=*texto;

		//TODO: gestion caracteres de control
//Si dos ^ seguidas, invertir estado parpadeo
		if (menu_escribe_texto_si_parpadeo(texto,0)) {
			parpadeo ^=1;
			//y saltamos esos codigos de negado
                        texto +=2;
                        caracter_aux.caracter=*texto;
		}

		//Codigo control color tinta
		if (menu_escribe_texto_si_cambio_tinta(texto,0)) {
			tinta=texto[2]-'0';
			texto+=3;
			caracter_aux.caracter=*texto;
		}

		//ver si dos ~~ o ~^ seguidas y cuidado al comparar que no nos vayamos mas alla del codigo 0 final
		if (menu_escribe_texto_si_inverso(texto,0)) {
			minuscula_letra=1;
			//y saltamos esos codigos de negado. Ver si era ~^, con lo que indica que no hay que bajar a minusculas
			texto++;
			if (*texto=='^') minuscula_letra=0;
			texto++;
			caracter_aux.caracter=*texto;

			if (menu_writing_inverse_color.v) inverso_letra=1;
			else inverso_letra=0;

		}

		//else {

			//Si estaba prefijo utf activo

			if (era_utf) {
				caracter_aux.caracter=menu_escribe_texto_convert_utf(era_utf,*texto);
				era_utf=0;

				//Caracter final utf
				//putchar_menu_overlay_parpadeo(x,y,letra,tinta,papel,parpadeo);
			}


			//Si no, ver si entra un prefijo utf
			else {
				//printf ("letra: %02XH\n",letra);
				//Prefijo utf
                if (menu_es_prefijo_utf(*texto)) {
        	        era_utf=*texto;
					//printf ("activado utf\n");
	            }

				/*else {
					//Caracter normal
					putchar_menu_overlay_parpadeo(x,y,letra,tinta,papel,parpadeo);
				}*/
			}


		//}


		if (!inverso_letra) {
			caracter_aux.tinta=tinta;
			caracter_aux.papel=papel;
		}
		else {
			caracter_aux.tinta=papel;
			caracter_aux.papel=tinta;			
			//Los hotkeys de menu siempre apareceran en minusculas para ser coherentes
			//De la misma manera, no se soportan hotkeys en menus que sean minusculas
			if (minuscula_letra) caracter_aux.caracter=letra_minuscula(caracter_aux.caracter);			
		}

		inverso_letra=0;


		caracter_aux.parpadeo=parpadeo;


		zxvision_print_char(w,x,y,&caracter_aux);
		if (!era_utf) x++;
		texto++;
	}	
}

void zxvision_print_string_defaults(zxvision_window *w,int x,int y,char *texto)
{

	zxvision_print_string(w,x,y,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,0,texto);

}

//Imprimir 1 caracter
void zxvision_print_char_defaults(zxvision_window *w,int x,int y,char c)
{

    char buffer[2];

    buffer[0]=c;
    buffer[1]=0;

    zxvision_print_string_defaults(w,x,y,buffer);
}

void zxvision_fill_width_spaces(zxvision_window *w,int y)
{
	overlay_screen caracter_aux;
	caracter_aux.caracter=' ';
	caracter_aux.tinta=ESTILO_GUI_TINTA_NORMAL;
	caracter_aux.papel=ESTILO_GUI_PAPEL_NORMAL;
	caracter_aux.parpadeo=0;		

	int i;
	for (i=0;i<w->total_width;i++) {
		zxvision_print_char(w,i,y,&caracter_aux);
	}
}

//Igual que la anterior pero antes borra la linea con espacios
void zxvision_print_string_defaults_fillspc(zxvision_window *w,int x,int y,char *texto)
{

	/*overlay_screen caracter_aux;
	caracter_aux.caracter=' ';
	caracter_aux.tinta=ESTILO_GUI_TINTA_NORMAL;
	caracter_aux.papel=ESTILO_GUI_PAPEL_NORMAL;
	caracter_aux.parpadeo=0;		

	int i;
	for (i=0;i<w->total_width;i++) {
		zxvision_print_char(w,i,y,&caracter_aux);
	}*/
	zxvision_fill_width_spaces(w,y);

	zxvision_print_string(w,x,y,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,0,texto);

}

void zxvision_putpixel(zxvision_window *w,int x,int y,int color)
{

	//int final_x,final_y;

	/*

-Como puede ser que al redimensionar ay sheet la ventana tenga más tamaño total... se crea de nuevo al redimensionar?
->no, porque dibuja pixeles con overlay y eso no comprueba si sale del limite virtual de la ventana
Creo que el putpixel en overlay no controla ancho total sino ancho visible. Exacto

Efectivamente. Se usa tamaño visible
Hacerlo constar en zxvision_putpixel como un TODO. Aunque si no hubiera este “fallo”, al redimensionar ay sheet no se vería el tamaño adicional , habría que cerrar la ventana y volverla a abrir ya con el tamaño total nuevo (ya que guarda geometría)
Es lo que pasa con otras ventanas de texto, que no se amplía el ancho total al no recrearse la ventana , y hay que salir y volver a entrar. Ejemplos??


*/
	/*
	    int offsetx=PIANO_GRAPHIC_BASE_X*menu_char_width+12;
    int offsety=piano_graphic_base_y*scale_y_chip(8)+18;

*/
	//Obtener coordenadas en pixeles de zona ventana dibujable
	int window_pixel_start_x=(w->x)*menu_char_width;
	int window_pixel_start_y=((w->y)+1)*8;
	int window_pixel_final_x=window_pixel_start_x+((w->visible_width)-zxvision_get_minus_width_byscrollvbar(w))*menu_char_width;
	int window_pixel_final_y=window_pixel_start_y+((w->visible_height)-2)*8;

	//Obtener coordenada x,y final donde va a parar
	int xfinal=x+window_pixel_start_x-(w->offset_x)*menu_char_width;
	int yfinal=y+window_pixel_start_y-(w->offset_y)*8;

	//Ver si esta dentro de rango
	if (xfinal>=window_pixel_start_x && xfinal<window_pixel_final_x && yfinal>=window_pixel_start_y && yfinal<window_pixel_final_y) {

    //Chapucilla para evitar que las ventanas en background sobreescriban a las de arriba
    //if (!zxvision_coords_in_front_window(w,xfinal/menu_char_width,yfinal/8)) {		
	if (!zxvision_coords_in_superior_windows(w,xfinal/menu_char_width,yfinal/8)) {				
		menu_scr_putpixel(xfinal,yfinal,color);
	}

	}
	else {
		//printf ("pixel out of window %d %d\n",x,y);
	}
}


//Hacer putpixel sin tener en cuenta zoom_x ni y. Usado en help keyboard
void zxvision_putpixel_no_zoom(zxvision_window *w,int x,int y,int color)
{

	//int final_x,final_y;

	/*

-Como puede ser que al redimensionar ay sheet la ventana tenga más tamaño total... se crea de nuevo al redimensionar?
->no, porque dibuja pixeles con overlay y eso no comprueba si sale del limite virtual de la ventana
Creo que el putpixel en overlay no controla ancho total sino ancho visible. Exacto

Efectivamente. Se usa tamaño visible
Hacerlo constar en zxvision_putpixel como un TODO. Aunque si no hubiera este “fallo”, al redimensionar ay sheet no se vería el tamaño adicional , habría que cerrar la ventana y volverla a abrir ya con el tamaño total nuevo (ya que guarda geometría)
Es lo que pasa con otras ventanas de texto, que no se amplía el ancho total al no recrearse la ventana , y hay que salir y volver a entrar. Ejemplos??


*/
	/*
	    int offsetx=PIANO_GRAPHIC_BASE_X*menu_char_width+12;
    int offsety=piano_graphic_base_y*scale_y_chip(8)+18;

*/
	//Obtener coordenadas en pixeles de zona ventana dibujable
	//En este caso multiplicar por zoom_x zoom_y pues coordenadas finales no tienen en cuenta zoom
	int window_pixel_start_x=(w->x)*menu_char_width*zoom_x;
	int window_pixel_start_y=((w->y)+1)*8*zoom_y;

	int window_pixel_final_x=window_pixel_start_x+((w->visible_width)-zxvision_get_minus_width_byscrollvbar(w))*menu_char_width*zoom_x;
	int window_pixel_final_y=window_pixel_start_y+((w->visible_height)-2)*8*zoom_y;

	//Obtener coordenada x,y final donde va a parar
	int xfinal=x+window_pixel_start_x-(w->offset_x)*menu_char_width*zoom_x;
	int yfinal=y+window_pixel_start_y-(w->offset_y)*8*zoom_y;


	//int total_width_window=((w->visible_width)-zxvision_get_minus_width_byscrollvbar(w))*menu_char_width*zoom_x;
	//int total_height_window=((w->visible_height)-2)*8*zoom_y;



	//Ver si esta dentro de rango. Metodo nuevo pero que no va bien
	//if (x>=0 && x<total_width_window && y>=0 && y<=total_height_window) {
	
	//Antiguo metodo que tiene en cuenta los offsets
	if (xfinal>=window_pixel_start_x && xfinal<window_pixel_final_x && yfinal>=window_pixel_start_y && yfinal<window_pixel_final_y) {

		//Chapucilla para evitar que las ventanas en background sobreescriban a las de arriba
		//if (!zxvision_coords_in_front_window(w,xfinal/menu_char_width,yfinal/8)) {		
		if (!zxvision_coords_in_superior_windows(w,(xfinal/menu_char_width)/zoom_x,(yfinal/8)/zoom_y)  ) {				
			menu_scr_putpixel_no_zoom(xfinal,yfinal,color);
		}

	}
	else {
		//printf ("pixel out of window %d %d width %d height: %d\n",x,y,total_width_sin_zoom,total_height_sin_zoom);
	}
}


void zxvision_handle_mouse_move_aux(zxvision_window *w)
{
				int movimiento_x=menu_mouse_x-window_mouse_x_before_move;
				int movimiento_y=menu_mouse_y-window_mouse_y_before_move;

				//printf ("Windows has been moved. menu_mouse_x: %d (%d) menu_mouse_y: %d (%d)\n",menu_mouse_x,movimiento_x,menu_mouse_y,movimiento_y);
				


				//Actualizar posicion
				int new_x=w->x+movimiento_x;
				int new_y=w->y+movimiento_y;


				zxvision_set_x_position(w,new_x);
				zxvision_set_y_position(w,new_y);
}

void zxvision_handle_mouse_resize_aux(zxvision_window *w)
{
				int incremento_ancho=menu_mouse_x-(w->visible_width)+1;
				int incremento_alto=menu_mouse_y-(w->visible_height)+1;

				//printf ("Incremento %d x %d\n",incremento_ancho,incremento_alto);

				int ancho_final=(w->visible_width)+incremento_ancho;
				int alto_final=(w->visible_height)+incremento_alto;

				//Evitar ventana de ancho pequeño, aunque se puede hacer, pero las franjas de colores se van por la izquierda
				if (ancho_final>7) {
					zxvision_set_visible_width(w,ancho_final);
				}

				//Evitar ventana de alto 1, aunque se puede hacer, pero luego no habria zona de redimensionado
				if (alto_final>1) {
					zxvision_set_visible_height(w,alto_final);
				}
}

int zxvision_mouse_in_bottom_right(zxvision_window *w)
{
	if (menu_mouse_x==(w->visible_width)-1 && menu_mouse_y==w->visible_height-1) return 1;

	return 0;
}


void zxvision_handle_minimize(zxvision_window *w)
{

	if (w->can_be_resized) {

		//Para cualquiera de los dos casos, la ponemos como minimizada
		//Luego en restaurar, restauramos valores originales
		//Se hace asi para que se pueda partir de un tamaño minimo y poder restaurar a su tamaño original
		//Si no, las funciones de establecer x,y, ancho, alto, podrian detectar fuera de rango de pantalla y no restaurar

		//Cambiar alto
		zxvision_set_visible_height(w,2);

		//Cambiar ancho
		//primero poner ancho inicial y luego reducir a ancho minimo para que quepa el titulo
		zxvision_set_visible_width(w,w->width_before_max_min_imize);
							
		int ancho_ventana_final=menu_dibuja_ventana_ret_ancho_titulo(w->visible_width,w->window_title);

		//printf ("ancho final: %d\n",ancho_ventana_final);
		zxvision_set_visible_width(w,ancho_ventana_final);

		//Al minimizar/restaurar, desactivamos maximizado
		w->is_maximized=0;

		if (w->is_minimized) {
			//Des-minimizar. Dejar posicion y tamaño original
			//printf ("Unminimize window\n");
			zxvision_set_x_position(w,w->x_before_max_min_imize);
			zxvision_set_y_position(w,w->y_before_max_min_imize);
			zxvision_set_visible_height(w,w->height_before_max_min_imize);
			zxvision_set_visible_width(w,w->width_before_max_min_imize);
			w->is_minimized=0;
		}
		
		else {
			//Ya la hemos minimizado antes. solo indicarlo
			//printf ("Minimize window\n");
			w->is_minimized=1;
		}

		zxvision_draw_window(w);
		zxvision_draw_window_contents(w);
	}

}


void zxvision_handle_maximize(zxvision_window *w)
{

	if (w->can_be_resized) {

		//Para cualquiera de los dos casos, la ponemos como minimizada
		//Luego en restaurar, restauramos valores originales
		//Se hace asi para que se pueda partir de un tamaño minimo y poder restaurar a su tamaño original
		//Si no, las funciones de establecer x,y, ancho, alto, podrian detectar fuera de rango de pantalla y no restaurar

		//Cambiar alto
		zxvision_set_visible_height(w,2);

		//Cambiar ancho
		//primero poner ancho inicial y luego reducir a ancho minimo para que quepa el titulo
		zxvision_set_visible_width(w,w->width_before_max_min_imize);	

		int ancho_ventana_final=menu_dibuja_ventana_ret_ancho_titulo(w->visible_width,w->window_title);

		//printf ("ancho final: %d\n",ancho_ventana_final);
		zxvision_set_visible_width(w,ancho_ventana_final);



		//Al maximizar/restaurar, desactivamos minimizado
		w->is_minimized=0;

		if (w->is_maximized) {
			//Des-minimizar. Dejar posicion y tamaño original
			debug_printf (VERBOSE_DEBUG,"Unmaximize window");
			zxvision_set_x_position(w,w->x_before_max_min_imize);
			zxvision_set_y_position(w,w->y_before_max_min_imize);
			zxvision_set_visible_height(w,w->height_before_max_min_imize);
			zxvision_set_visible_width(w,w->width_before_max_min_imize);

			w->is_maximized=0;
		}
		
		else {
			debug_printf (VERBOSE_DEBUG,"Maximize window");
			zxvision_set_x_position(w,0);
			zxvision_set_y_position(w,0);
			int max_width=scr_get_menu_width();
			int max_height=scr_get_menu_height();
			//printf ("visible width %d\n",max_width);
			zxvision_set_visible_width(w,max_width);
			zxvision_set_visible_height(w,max_height);
			
			w->is_maximized=1;
		}

		zxvision_draw_window(w);
		zxvision_draw_window_contents(w);
	}

}

void zxvision_send_scroll_right_and_draw(zxvision_window *w)
{
						//printf ("Pulsado en scroll derecha\n");
						zxvision_send_scroll_right(w);

						//Redibujar botones scroll. Esto es necesario solo en el caso que,
						//al empezar a pulsar boton, este se invierte el color, y si está el scroll en el limite y no actua,
						//se quedaria el color del boton invertido
						zxvision_draw_horizontal_scroll_bar(w,0);	
}

void zxvision_send_scroll_left_and_draw(zxvision_window *w)
{
						//printf ("Pulsado en scroll izquierda\n");
						zxvision_send_scroll_left(w);

						//Redibujar botones scroll. Esto es necesario solo en el caso que,
						//al empezar a pulsar boton, este se invierte el color, y si está el scroll en el limite y no actua,
						//se quedaria el color del boton invertido
						zxvision_draw_horizontal_scroll_bar(w,0);
}

void zxvision_send_scroll_up_and_draw(zxvision_window *w)
{
						//printf ("Pulsado en scroll arriba\n");
						zxvision_send_scroll_up(w);

						//Redibujar botones scroll. Esto es necesario solo en el caso que,
						//al empezar a pulsar boton, este se invierte el color, y si está el scroll en el limite y no actua,
						//se quedaria el color del boton invertido
						zxvision_draw_vertical_scroll_bar(w,0);
}

void zxvision_send_scroll_down_and_draw(zxvision_window *w)
{
						//printf ("Pulsado en scroll abajo\n");
						zxvision_send_scroll_down(w);

						//Redibujar botones scroll. Esto es necesario solo en el caso que,
						//al empezar a pulsar boton, este se invierte el color, y si está el scroll en el limite y no actua,
						//se quedaria el color del boton invertido
						zxvision_draw_vertical_scroll_bar(w,0);	
}

//Si se habia pulsado en una ventana por debajo de la actual
int clicked_on_background_windows=0;

zxvision_window *which_window_clicked_on_background=NULL;

void zxvision_handle_mouse_ev_switch_back_wind(zxvision_window *ventana_pulsada)
{
	clicked_on_background_windows=1;
	which_window_clicked_on_background=ventana_pulsada;

	//Se ha pulsado en otra ventana. Conmutar a dicha ventana. Cerramos el menu y todos los menus raíz
	salir_todos_menus=1;

	//Si la ventana activa permite ir a background, mandarla a background
	if (zxvision_current_window->can_be_backgrounded) {
		mouse_pressed_background_window=1;
	}

	//Si la ventana activa no permite ir a background, cerrarla
	else {
		mouse_pressed_close_window=1;
	}
			
}

z80_byte zxvision_get_char_at_position(zxvision_window *w,int x,int y,int *inverso)
{

	//Asumimos
	*inverso=0;

	overlay_screen caracter;

	zxvision_get_character_at_mouse(w,x,y,&caracter);

	//printf ("Caracter: %c (%d)\n",(caracter.caracter>31 && caracter.caracter<126 ? caracter.caracter : '.') ,caracter.caracter);

	//Interpretar si es inverso
	if (caracter.caracter>=32 && caracter.caracter<=126) {
		if (caracter.tinta==ESTILO_GUI_PAPEL_NORMAL && caracter.papel==ESTILO_GUI_TINTA_NORMAL) {
			//printf ("Caracter es inverso\n");
			*inverso=1;
				
		}
	}
			

	return caracter.caracter;
}

z80_byte zxvision_get_key_hotkey(zxvision_window *w,int x,int y)
{


	//int xorig=x;

	int inverso;
			/*
-Hot key ratón:

-buscar a la izquierda hasta x cero o espacio
-de ahí hacia la derecha hasta espacio, final de ancho o : puntos
-contar caracteres inverso: si solo 1, enviar Tecla. Si es más de 1 puede ser “enter” y por tanto ignorar. O si es “ent” enviar 13

-cuidado con Y siendo mayor que el rango			
			*/

			//Ir hacia inicio del todo a la izquierda



	for (;x>=0;x--) {

	
		z80_byte caracter=zxvision_get_char_at_position(w,x,y,&inverso);

		//Espacio, salir
		if (caracter==32) break;

	}

	x++;

	//de ahí hacia la derecha hasta espacio, final de ancho o : puntos
	//-contar caracteres inverso: si solo 1, enviar Tecla. Si es más de 1 puede ser “enter” y por tanto ignorar. O si es “ent” enviar 13

	int total_inversos=0;

	z80_byte caracter_inverso=0;

	for (;x<=w->visible_width;x++) {


		z80_byte caracter=zxvision_get_char_at_position(w,x,y,&inverso);

		//printf ("X %d Y %d car: %c inverso: %d\n",x,y,caracter,inverso);

		//Interpretar si es inverso
		if (inverso) {
			total_inversos++;
			caracter_inverso=caracter;
		}

		//Espacio, salir
		if (caracter==32 || caracter==':') break;

	}					

	if (total_inversos==1 && caracter_inverso!=0) {
		//printf ("Detectada tecla hotkey: %c\n",caracter_inverso);
		return caracter_inverso;
	}

			

	return 0;
}

//int zxvision_mouse_events_counter=0;
//int tempconta;
//Retorna 1 si pulsado boton de cerrar ventana
void zxvision_handle_mouse_events(zxvision_window *w)
{

	if (w==NULL) return; // 0; 

	if (!si_menu_mouse_activado()) return; // 0;

	//printf ("zxvision_handle_mouse_events: mouse_left: %d\n",mouse_left);
	//int pulsado_boton_cerrar=0;

	menu_calculate_mouse_xy();

	if (mouse_left && !mouse_is_dragging) {

		//Si se pulsa dentro de ventana y no esta arrastrando
	 	if (si_menu_mouse_en_ventana() && !zxvision_keys_event_not_send_to_machine) {
			debug_printf (VERBOSE_DEBUG,"Clicked inside window. Events are not sent to emulated machine");
			zxvision_keys_event_not_send_to_machine=1;
			ventana_tipo_activa=1;
			zxvision_draw_window(w);
			zxvision_draw_window_contents(w);
		}

	

		else if (!si_menu_mouse_en_ventana() && zxvision_keys_event_not_send_to_machine) {
			//Si se pulsa fuera de ventana
			debug_printf (VERBOSE_DEBUG,"Clicked outside window. Events are sent to emulated machine. X=%d Y=%d",menu_mouse_x,menu_mouse_y);
			zxvision_keys_event_not_send_to_machine=0;
			ventana_tipo_activa=0;
			zxvision_draw_window(w);
			zxvision_draw_window_contents(w);

			int absolute_mouse_x,absolute_mouse_y;
			
			menu_calculate_mouse_xy_absolute_interface(&absolute_mouse_x,&absolute_mouse_y);

			//Vamos a ver en que ventana se ha pulsado, si tenemos background activado
			zxvision_window *ventana_pulsada;

			ventana_pulsada=zxvision_coords_in_below_windows(zxvision_current_window,absolute_mouse_x,absolute_mouse_y);			


			if (ventana_pulsada!=NULL) {
				debug_printf (VERBOSE_DEBUG,"Clicked on window: %s",ventana_pulsada->window_title);

				zxvision_handle_mouse_ev_switch_back_wind(ventana_pulsada);
			
			}

		}
	}


	if (mouse_movido) {
		//printf ("mouse movido\n");
		if (si_menu_mouse_en_ventana() ) {
				//if (menu_mouse_x>=0 && menu_mouse_y>=0 && menu_mouse_x<ventana_ancho && menu_mouse_y<ventana_alto ) {
					//printf ("dentro ventana\n");
					if (menu_mouse_y==0) {
						//printf ("En barra titulo\n");
					}
					//Descartar linea titulo y ultima linea
		}

	}

	if (mouse_left) {
		//printf ("Pulsado boton izquierdo\n");

		if (!mouse_movido) {
			if (!mouse_is_clicking) {
				//printf ("Mouse started clicking\n");
				mouse_is_clicking=1;
				last_x_mouse_clicked=menu_mouse_x;
				last_y_mouse_clicked=menu_mouse_y;


				//Gestion doble click
				if (menu_multitarea) {
					if (menu_mouse_left_double_click_counter-menu_mouse_left_double_click_counter_initial<25) {
						//printf ("-IT is DOBLE click\n");
						mouse_is_double_clicking=1;
					}
					else {
						menu_mouse_left_double_click_counter_initial=menu_mouse_left_double_click_counter;
						mouse_is_double_clicking=0;
					}
				}

				else {
					//Sin multitarea nunca hay doble click
					mouse_is_double_clicking=0;
				}
			}
		}
	}

	//Si empieza a pulsar botón izquierdo
	if (mouse_left && mouse_is_clicking) {

		if (si_menu_mouse_en_ventana()) {
			//Pulsado en barra titulo
			if (last_y_mouse_clicked==0) {
				if (!mouse_is_double_clicking) {
						//Si pulsa boton cerrar ventana
					if (last_x_mouse_clicked==0 && menu_hide_close_button.v==0) {
						//printf ("pulsado boton cerrar\n");
						//pulsado_boton_cerrar=1;
						mouse_pressed_close_window=1;
						//Mostrar boton cerrar pulsado
						putchar_menu_overlay(w->x,w->y,ESTILO_GUI_BOTON_CERRAR,ESTILO_GUI_PAPEL_TITULO,ESTILO_GUI_TINTA_TITULO);
					}

					//Si pulsa zona background  window
					if (last_x_mouse_clicked==w->visible_width-2 && w->can_be_backgrounded && menu_allow_background_windows) {
						mouse_pressed_background_window=1;
						//Mostrar boton background pulsado
						putchar_menu_overlay(w->x+w->visible_width-2,w->y,zxvision_get_character_backgrounded_window(),ESTILO_GUI_PAPEL_TITULO,ESTILO_GUI_TINTA_TITULO);						
					}			


					//Si se pulsa en boton minimizar, indicar que se esta pulsando
					if (last_x_mouse_clicked==w->visible_width-1 && menu_hide_minimize_button.v==0 && w->can_be_resized) {
						putchar_menu_overlay(w->x+w->visible_width-1,w->y,menu_retorna_caracter_minimizar(w),ESTILO_GUI_PAPEL_TITULO,ESTILO_GUI_TINTA_TITULO);
					}

				}
			}


			//Si se pulsa en ventana y alrededor tecla hotkey
			/*
			if (w->can_mouse_send_hotkeys && last_y_mouse_clicked>0) {
				

				z80_byte caracter=zxvision_get_key_hotkey(w,last_x_mouse_clicked,last_y_mouse_clicked-1);
				
				if (caracter>=32 && caracter<=126) {

						mouse_pressed_hotkey_window=1;
						mouse_pressed_hotkey_window_key=caracter;
					
				}

				else {
					mouse_pressed_hotkey_window=0;
				}
			}
			*/
					

			//Pulsado en botones Scroll horizontal
			if (zxvision_if_horizontal_scroll_bar(w)) {
				if (last_y_mouse_clicked==w->visible_height-1) {
					//Linea scroll horizontal
					int posicion_flecha_izquierda=1;
					int posicion_flecha_derecha=w->visible_width-2;

					//Flecha izquierda
					if (last_x_mouse_clicked==posicion_flecha_izquierda) {
						//printf ("Pulsado en scroll izquierda\n");
						//putchar_menu_overlay(w->x+posicion_flecha_izquierda,w->y+w->visible_height-1,'<',ESTILO_GUI_PAPEL_NORMAL,ESTILO_GUI_TINTA_NORMAL);
						zxvision_draw_horizontal_scroll_bar(w,1);

					}
					//Flecha derecha
					if (last_x_mouse_clicked==posicion_flecha_derecha) {
						//printf ("Pulsado en scroll derecha\n");
						//putchar_menu_overlay(w->x+posicion_flecha_derecha,w->y+w->visible_height-1,'>',ESTILO_GUI_PAPEL_NORMAL,ESTILO_GUI_TINTA_NORMAL);
						zxvision_draw_horizontal_scroll_bar(w,2);
					
					}

					//Zona porcentaje
					if (last_x_mouse_clicked>posicion_flecha_izquierda && last_x_mouse_clicked<posicion_flecha_derecha) {
						//printf ("Pulsado en zona scroll horizontal\n");
						zxvision_draw_horizontal_scroll_bar(w,3);
					}

				}
			}

			//Pulsado en botones Scroll vertical
			if (zxvision_if_vertical_scroll_bar(w)) {
				if (last_x_mouse_clicked==w->visible_width-1) {
					//Linea scroll vertical
					int posicion_flecha_arriba=1;
					int posicion_flecha_abajo=w->visible_height-2;

					//Flecha arriba
					if (last_y_mouse_clicked==posicion_flecha_arriba) {
						//printf ("Pulsado en scroll arriba\n");
						//putchar_menu_overlay(w->x+w->visible_width-1,w->y+posicion_flecha_arriba,'^',ESTILO_GUI_PAPEL_NORMAL,ESTILO_GUI_TINTA_NORMAL);
						zxvision_draw_vertical_scroll_bar(w,1);
					}

					//Flecha abajo
					if (last_y_mouse_clicked==posicion_flecha_abajo) {
						//printf ("Pulsado en scroll abajo\n");
						//putchar_menu_overlay(w->x+w->visible_width-1,w->y+posicion_flecha_abajo,'v',ESTILO_GUI_PAPEL_NORMAL,ESTILO_GUI_TINTA_NORMAL);
						zxvision_draw_vertical_scroll_bar(w,2);
					}

					if (last_y_mouse_clicked>posicion_flecha_arriba && last_y_mouse_clicked<posicion_flecha_abajo) {
						//printf ("Pulsado en zona scroll vertical\n");
						zxvision_draw_vertical_scroll_bar(w,3);
					}

				}
			}

		}
	}

	//Liberación boton izquierdo
	if (!mouse_left && mouse_is_clicking && !mouse_is_dragging) {
			//printf ("Mouse stopped clicking mouse_is_dragging %d\n",mouse_is_dragging);
			mouse_is_clicking=0;
			//Pulsacion en sitios de ventana
			//Si en barra titulo
			if (si_menu_mouse_en_ventana() && last_y_mouse_clicked==0) {
				//printf ("Clicked on title\n");
				//Y si ha sido doble click
				if (mouse_is_double_clicking) {
					debug_printf (VERBOSE_DEBUG,"Double clicked on title");

					zxvision_handle_maximize(w);
					
					
				}
				else {
					//Simple click
					//Si pulsa zona minimizar
					if (last_x_mouse_clicked==w->visible_width-1 && menu_hide_minimize_button.v==0) {
						//Mostrar boton minimizar pulsado
						//printf ("minimizar\n");
						//putchar_menu_overlay(w->x+w->visible_width-2,w->y,'X',ESTILO_GUI_TINTA_TITULO,ESTILO_GUI_PAPEL_TITULO);
						//menu_refresca_pantalla();
						zxvision_handle_minimize(w);
					}
					//Si pulsa boton cerrar ventana
					/*if (last_x_mouse_clicked==0 && menu_hide_close_button.v==0) {
						printf ("pulsado boton cerrar\n");
						//pulsado_boton_cerrar=1;
						mouse_pressed_close_window=1;
					}*/

		

				}

				
			}


			//Si se pulsa en ventana y alrededor tecla hotkey
			
			if (w->can_mouse_send_hotkeys && si_menu_mouse_en_ventana() && last_y_mouse_clicked>0 && last_y_mouse_clicked<w->visible_height-1) {

				//printf ("visible height: %d\n",w->visible_height);
				debug_printf (VERBOSE_DEBUG,"Looking for hotkeys at mouse position");
				

				z80_byte caracter=zxvision_get_key_hotkey(w,last_x_mouse_clicked,last_y_mouse_clicked-1);
				
				if (caracter>=32 && caracter<=126) {
						debug_printf (VERBOSE_DEBUG,"Sending hotkey from mouse: %c",caracter);
						mouse_pressed_hotkey_window=1;
						mouse_pressed_hotkey_window_key=caracter;

						return;
					
				}

				else {
					mouse_pressed_hotkey_window=0;
				}
			}	
					



		//Si se pulsa dentro de cualquier otra ventana o en logo Z. Esto solo cuando se libera boton
		//Y si no tenemos el foco
		if (!zxvision_keys_event_not_send_to_machine) {

			int absolute_mouse_x,absolute_mouse_y;
			
			menu_calculate_mouse_xy_absolute_interface(&absolute_mouse_x,&absolute_mouse_y);

			//Vamos a ver en que ventana se ha pulsado, si tenemos background activado
			zxvision_window *ventana_pulsada;

			ventana_pulsada=zxvision_coords_in_below_windows(zxvision_current_window,absolute_mouse_x,absolute_mouse_y);			
			
			if (ventana_pulsada!=NULL) {
				debug_printf (VERBOSE_DEBUG,"Clicked inside other window or zlogo. Events are not sent to emulated machine");
				zxvision_keys_event_not_send_to_machine=1;
				ventana_tipo_activa=1;
				zxvision_draw_window(w);
				zxvision_draw_window_contents(w);		
		
				
			}
		}


			//Scroll horizontal
			if (zxvision_if_horizontal_scroll_bar(w)) {
				if (last_y_mouse_clicked==w->visible_height-1) {
					//Linea scroll horizontal
					int posicion_flecha_izquierda=1;
					int posicion_flecha_derecha=w->visible_width-2;

					//Flecha izquierda
					if (last_x_mouse_clicked==posicion_flecha_izquierda) {
						zxvision_send_scroll_left_and_draw(w);			
					}

					//Flecha derecha
					if (last_x_mouse_clicked==posicion_flecha_derecha) {
						zxvision_send_scroll_right_and_draw(w);			
					}

					if (last_x_mouse_clicked>posicion_flecha_izquierda && last_x_mouse_clicked<posicion_flecha_derecha) {
						//printf ("Pulsado en zona scroll horizontal\n");
						//Sacamos porcentaje
						int total_ancho=posicion_flecha_derecha-posicion_flecha_izquierda;
						if (total_ancho==0) total_ancho=1; //Evitar dividir por cero

						int parcial_ancho=last_x_mouse_clicked-posicion_flecha_izquierda;

						int porcentaje=(parcial_ancho*100)/total_ancho;

						//printf ("Porcentaje: %d\n",porcentaje);

						int offset_to_mult=w->total_width-w->visible_width+1; //+1 porque se pierde linea derecha por scroll
						//printf ("Multiplicando sobre %d\n",offset_to_mult);

						//Establecemos offset horizontal
						int offset=((offset_to_mult)*porcentaje)/100;

						//printf ("set offset: %d\n",offset);

						//Casos especiales de izquierda del todo y derecha del todo
						if (last_x_mouse_clicked==posicion_flecha_izquierda+1) {
							//printf ("Special case: clicked on the top left. Set offset 0\n");
							offset=0;
						}

						if (last_x_mouse_clicked==posicion_flecha_derecha-1) {
							//printf ("Special case: clicked on the top right. Set offset to maximum\n");
							offset=w->total_width-w->visible_width+1;
						}

						zxvision_set_offset_x(w,offset);


						//Redibujar botones scroll. Esto es necesario solo en el caso que,
                                                //al empezar a pulsar boton, este se invierte el color, y si está el scroll en el limite y no actua,
                                                //se quedaria el color del boton invertido
                                                zxvision_draw_horizontal_scroll_bar(w,0);

					}
				}
			} 


			//Scroll vertical
			if (zxvision_if_vertical_scroll_bar(w)) {
				if (last_x_mouse_clicked==w->visible_width-1) {
					//Linea scroll vertical
					int posicion_flecha_arriba=1;
					int posicion_flecha_abajo=w->visible_height-2;

					//Flecha arriba
					if (last_y_mouse_clicked==posicion_flecha_arriba) {
						zxvision_send_scroll_up_and_draw(w);
					}

					//Flecha abajo
					if (last_y_mouse_clicked==posicion_flecha_abajo) {
						zxvision_send_scroll_down_and_draw(w);					
					}

					if (last_y_mouse_clicked>posicion_flecha_arriba && last_y_mouse_clicked<posicion_flecha_abajo) {
						//printf ("Pulsado en zona scroll vertical\n");
						//Sacamos porcentaje
						int total_alto=posicion_flecha_abajo-posicion_flecha_arriba;
						if (total_alto==0) total_alto=1; //Evitar dividir por cero

						int parcial_alto=last_y_mouse_clicked-posicion_flecha_arriba;

						int porcentaje=(parcial_alto*100)/total_alto;

						//printf ("Porcentaje: %d\n",porcentaje);


						int offset_to_mult=w->total_height-w->visible_height+2; //+2 porque se pierde linea abajo de scroll y titulo
						//printf ("Multiplicando sobre %d\n",offset_to_mult);

						//Establecemos offset vertical
						int offset=((offset_to_mult)*porcentaje)/100;

						//printf ("set offset: %d\n",offset);

						//Casos especiales de arriba del todo y abajo del todo
						if (last_y_mouse_clicked==posicion_flecha_arriba+1) {
							//printf ("Special case: clicked on the top. Set offset 0\n");
							offset=0;
						}

						if (last_y_mouse_clicked==posicion_flecha_abajo-1) {
							//printf ("Special case: clicked on the bottom. Set offset to maximum\n");
							offset=w->total_height-w->visible_height+2;
						}

						zxvision_set_offset_y(w,offset);

						//Redibujar botones scroll. Esto es necesario solo en el caso que,
                                                //al empezar a pulsar boton, este se invierte el color, y si está el scroll en el limite y no actua,
                                                //se quedaria el color del boton invertido
                                                zxvision_draw_vertical_scroll_bar(w,0);

					}
				}
			} 


	}

	if (mouse_wheel_vertical && zxvision_if_vertical_scroll_bar(w)) {
		int leido_mouse_wheel_vertical=mouse_wheel_vertical;
		//printf ("Read mouse vertical wheel from zxvision_handle_mouse_events : %d\n",leido_mouse_wheel_vertical);

		//Si invertir movimiento
		if (menu_invert_mouse_scroll.v) leido_mouse_wheel_vertical=-leido_mouse_wheel_vertical;

		while (leido_mouse_wheel_vertical<0) {
			zxvision_send_scroll_down_and_draw(w);
			leido_mouse_wheel_vertical++;
		}

		while (leido_mouse_wheel_vertical>0) {
			zxvision_send_scroll_up_and_draw(w);
			leido_mouse_wheel_vertical--;
		}
		
		//Y resetear a 0. importante
		mouse_wheel_vertical=0;
	}

	if (mouse_wheel_horizontal && zxvision_if_horizontal_scroll_bar(w)) {
		int leido_mouse_wheel_horizontal=mouse_wheel_horizontal;
		//printf ("Read mouse horizontal wheel from zxvision_handle_mouse_events : %d\n",leido_mouse_wheel_horizontal);
	

		//Si invertir movimiento
		if (menu_invert_mouse_scroll.v) leido_mouse_wheel_horizontal=-leido_mouse_wheel_horizontal;		


		while (leido_mouse_wheel_horizontal<0) {
			zxvision_send_scroll_right_and_draw(w);
			leido_mouse_wheel_horizontal++;
		}

		while (leido_mouse_wheel_horizontal>0) {
			zxvision_send_scroll_left_and_draw(w);
			leido_mouse_wheel_horizontal--;
		}
		
		//Y resetear a 0. importante
		mouse_wheel_horizontal=0;
	}	

	if (!mouse_is_dragging) {
		if (mouse_left && mouse_movido) {
			//printf ("Mouse has begun to drag\n");
			mouse_is_dragging=1;

			//Si estaba en titulo
			if (si_menu_mouse_en_ventana()) {
				if (menu_mouse_y==0) {
					//printf ("Arrastrando ventana\n");
					window_is_being_moved=1;
					window_mouse_x_before_move=menu_mouse_x;
					window_mouse_y_before_move=menu_mouse_y;
				}

				//Si esta en esquina inferior derecha (donde se puede redimensionar) y se permite resize
				if (zxvision_mouse_in_bottom_right(w) && w->can_be_resized) {
					//printf ("Mouse dragging in bottom right\n");

					window_is_being_resized=1;
					window_mouse_x_before_move=menu_mouse_x;
					window_mouse_y_before_move=menu_mouse_y;					
				}				
			}


		}
	}

	if (mouse_is_dragging) {
		//printf ("mouse is dragging\n");
		if (!mouse_left) { 
			//printf ("Mouse has stopped to drag\n");
			mouse_is_dragging=0;
			mouse_is_clicking=0; //Cuando se deja de arrastrar decir que se deja de pulsar tambien
			if (window_is_being_moved) {

				//printf ("Handle moved window\n");
				zxvision_handle_mouse_move_aux(w);
				window_is_being_moved=0;

			}

			if (window_is_being_resized) {

				//printf ("Handle resized window\n");
				zxvision_handle_mouse_resize_aux(w);
				

				window_is_being_resized=0;
			}
		}

		else {
			if (window_is_being_moved) {
				//Si se ha movido un poco
				if (menu_mouse_y!=window_mouse_y_before_move || menu_mouse_x!=window_mouse_x_before_move) {
					//printf ("Handle moved window\n");
					zxvision_handle_mouse_move_aux(w);
				
					//Hay que recalcular menu_mouse_x y menu_mouse_y dado que son relativos a la ventana que justo se ha movido
					//menu_mouse_y siempre sera 0 dado que el titulo de la ventana, donde se puede arrastrar para mover, es posicion relativa 0
					menu_calculate_mouse_xy();

					window_mouse_y_before_move=menu_mouse_y;
					window_mouse_x_before_move=menu_mouse_x;
									
				}
			}

			if (window_is_being_resized) {
				//Si se ha redimensionado un poco
				if (menu_mouse_y!=window_mouse_y_before_move || menu_mouse_x!=window_mouse_x_before_move) {
					//printf ("Handle resized window\n");
					zxvision_handle_mouse_resize_aux(w);

					window_mouse_y_before_move=menu_mouse_y;
					window_mouse_x_before_move=menu_mouse_x;					
				}
			}
		}
	}

	//if (mouse_left && mouse_movido) printf ("Mouse is dragging\n");
	//return pulsado_boton_cerrar;
}


//Guardar tamanyo en variables por si cambia
void zxvision_window_save_size(zxvision_window *ventana,int *ventana_ancho_antes,int *ventana_alto_antes)
{

	//Guardar ancho y alto anterior para recrear la ventana si cambia
	*ventana_ancho_antes=ventana->visible_width;
	*ventana_alto_antes=ventana->visible_height;
}



//Funcion comun que usan algunas ventanas para movimiento de cursores y pgup/dn
void zxvision_handle_cursors_pgupdn(zxvision_window *ventana,z80_byte tecla)
{
	int contador_pgdnup;
	int tecla_valida=1;
					switch (tecla) {

		                //abajo
                        case 10:
						zxvision_send_scroll_down(ventana);

						//Decir que se ha pulsado tecla para que no se relea
						menu_speech_tecla_pulsada=1;
                        break;

                        //arriba
                        case 11:
						zxvision_send_scroll_up(ventana);

						//Decir que se ha pulsado tecla para que no se relea
						menu_speech_tecla_pulsada=1;
                        break;

                        //izquierda
                        case 8:
						zxvision_send_scroll_left(ventana);

						//Decir que se ha pulsado tecla para que no se relea
						menu_speech_tecla_pulsada=1;
                        break;

                        //derecha
                        case 9:
						zxvision_send_scroll_right(ventana);

						//Decir que se ha pulsado tecla para que no se relea
						menu_speech_tecla_pulsada=1;
                        break;						

						//PgUp
						case 24:
							for (contador_pgdnup=0;contador_pgdnup<ventana->visible_height-2;contador_pgdnup++) {
								zxvision_send_scroll_up(ventana);
							}
							//Decir que no se ha pulsado tecla para que se relea
							menu_speech_tecla_pulsada=0;
						break;

                    	//PgDn
                    	case 25:
                    		for (contador_pgdnup=0;contador_pgdnup<ventana->visible_height-2;contador_pgdnup++) {
								zxvision_send_scroll_down(ventana);
                        	}

							//Decir que no se ha pulsado tecla para que se relea
							menu_speech_tecla_pulsada=0;
                    	break;

						//Mover ventana 
						case 'Q':
							zxvision_set_y_position(ventana,ventana->y-1);
						break;

						case 'A':
							zxvision_set_y_position(ventana,ventana->y+1);
						break;

						case 'O':
							zxvision_set_x_position(ventana,ventana->x-1);
						break;						

						case 'P':
							zxvision_set_x_position(ventana,ventana->x+1);
						break;						

						//Redimensionar ventana
						//Mover ventana 
						case 'W':
							if (ventana->visible_height-1>1) zxvision_set_visible_height(ventana,ventana->visible_height-1);
						break;		

						case 'S':
							zxvision_set_visible_height(ventana,ventana->visible_height+1);
						break;	

						case 'K':
							if (ventana->visible_width-1>5) zxvision_set_visible_width(ventana,ventana->visible_width-1);
						break;									

						case 'L':
							zxvision_set_visible_width(ventana,ventana->visible_width+1);
						break;											
					
						default:
							tecla_valida=0;
						break;

				}

	if (tecla_valida) {
		//Refrescamos pantalla para reflejar esto, util con multitask off
		if (!menu_multitarea) {
			//printf ("refresca pantalla\n");
			menu_refresca_pantalla();
		}		
	}

}

/*
Funcion comun usados en algunas ventanas que:
-refrescan pantalla
-ejecutan core loop si multitask activo
-leen tecla y esperan a liberar dicha tecla
*/
z80_byte zxvision_common_getkey_refresh(void)
{
	z80_byte tecla;

	     if (!menu_multitarea) {
			//printf ("refresca pantalla\n");
			menu_refresca_pantalla();
		}					

		
	            menu_cpu_core_loop();


				menu_espera_tecla();
				tecla=zxvision_read_keyboard();

				//con enter no salimos. TODO: esto se hace porque el mouse esta enviando enter al pulsar boton izquierdo, y lo hace tambien al hacer dragging
				//lo ideal seria que mouse no enviase enter al pulsar boton izquierdo y entonces podemos hacer que se salga tambien con enter
				if (tecla==13 && mouse_left) {	
					tecla=0;
				}

		if (tecla) {
			//printf ("Esperamos no tecla\n");
			menu_espera_no_tecla_con_repeticion();
		}	

	return tecla;
}


//Igual que zxvision_common_getkey_refresh pero sin esperar a no tecla
z80_byte zxvision_common_getkey_refresh_noesperanotec(void)
{
	z80_byte tecla;

	     if (!menu_multitarea) {
			//printf ("refresca pantalla\n");
			menu_refresca_pantalla();
		}					

		
	            menu_cpu_core_loop();


				menu_espera_tecla();
				tecla=zxvision_read_keyboard();

				//con enter no salimos. TODO: esto se hace porque el mouse esta enviando enter al pulsar boton izquierdo, y lo hace tambien al hacer dragging
				//lo ideal seria que mouse no enviase enter al pulsar boton izquierdo y entonces podemos hacer que se salga tambien con enter
				if (tecla==13 && mouse_left) {	
					tecla=0;
				}

	return tecla;
}

z80_byte zxvision_common_getkey_refresh_noesperatecla(void)
//Igual que zxvision_common_getkey_refresh pero sin esperar tecla cuando multitarea activa
{

	z80_byte tecla;

                menu_cpu_core_loop();

            	//si no hay multitarea, refrescar pantalla para mostrar contenido ventana rellenada antes, esperar tecla, 
                if (menu_multitarea==0) {
						menu_refresca_pantalla();
                        menu_espera_tecla();
                        //acumulado=0;
                }				

				tecla=zxvision_read_keyboard();

				//Nota: No usamos zxvision_common_getkey_refresh porque necesitamos que el bucle se ejecute continuamente para poder 
				//refrescar contenido de ventana, dado que aqui no llamamos a menu_espera_tecla
				//(a no ser que este multitarea off)

				if (tecla==13 && mouse_left) {	
					tecla=0;
				}					


		if (tecla) {
			//printf ("Esperamos no tecla\n");
			menu_espera_no_tecla_con_repeticion();
		}	

	return tecla;
}

//Retorna 1 si la tecla no se tiene que enviar a la maquina emulada
//esto es , cuando el menu esta abierto y la ventana tiene el foco
//En cambio retorna 0 (la tecla se va a enviar a la maquina emulada), cuando el menu esta cerrado o la ventana no tiene el foco
int zxvision_key_not_sent_emulated_mach(void)
{
	if (menu_abierto==1 && zxvision_keys_event_not_send_to_machine) return 1;
	else return 0;
}



//Crea ventana simple de 1 de alto con funcion para condicion de salida, y funcion de print. 
void zxvision_simple_progress_window(char *titulo, int (*funcioncond) (zxvision_window *),void (*funcionprint) (zxvision_window *) )
{
	    zxvision_window ventana;

		int alto_ventana=4;
		int ancho_ventana=28;


        int x_ventana=menu_center_x()-ancho_ventana/2; 
        int y_ventana=menu_center_y()-alto_ventana/2; 

        zxvision_new_window(&ventana,x_ventana,y_ventana,ancho_ventana,alto_ventana,ancho_ventana-1,alto_ventana-2,titulo);

        zxvision_draw_window(&ventana);


        zxvision_draw_window_contents(&ventana);

             
		zxvision_espera_tecla_condicion_progreso(&ventana,funcioncond,funcionprint);


        cls_menu_overlay();

        zxvision_destroy_window(&ventana);
}



void zxvision_rearrange_background_windows(void)
{

	//printf("rearrange windows\n");
	//debug_exec_show_backtrace();

	//Por si acaso 
	if (!menu_allow_background_windows) return;

	int origen_x=menu_origin_x();

	//printf ("origen_x: %d\n",origen_x);

	int ancho=scr_get_menu_width();

	//printf ("ancho: %d\n",ancho);

	int xfinal=origen_x+ancho;


	int alto=scr_get_menu_height();

	//printf ("alto: %d\n",alto);

	int yfinal=alto;


	//Empezamos una a una, desde la de mas abajo
	zxvision_window *ventana;

	ventana=zxvision_current_window;

	if (ventana==NULL) return;

	ventana=zxvision_find_first_window_below_this(ventana);

	if (ventana==NULL) return;

	int origen_y=0;

	//Y de ahi para arriba
	int x=origen_x;
	int y=origen_y;

	int alto_maximo_en_fila=0;

	int cambio_coords_origen=0;

	while (ventana!=NULL) {

		debug_printf (VERBOSE_DEBUG,"Setting window %s to %d,%d",ventana->window_title,x,y);

		ventana->x=x;
		ventana->y=y;

		//Y guardar la geometria
		util_add_window_geometry_compact(ventana);

		if (ventana->visible_height>alto_maximo_en_fila) alto_maximo_en_fila=ventana->visible_height;

		int ancho_antes=ventana->visible_width;

		ventana=ventana->next_window;
		if (ventana!=NULL) {
			x +=ancho_antes;
			//printf ("%d %d %d\n",x,ventana->visible_width,ancho);
			if (x+ventana->visible_width>xfinal) {

				//printf ("Next column\n");
				//Siguiente fila
				x=origen_x;

				y+=alto_maximo_en_fila;

				alto_maximo_en_fila=0;


			}

			//Si volver al principio
			if (y+ventana->visible_height>yfinal) {

				debug_printf (VERBOSE_DEBUG,"Restart x,y coordinates");

				//alternamos coordenadas origen, para darles cierto "movimiento", 4 caracteres derecha y abajo
				cambio_coords_origen ^=4;

				x=origen_x + cambio_coords_origen;
				y=origen_y + cambio_coords_origen;
						
			}
		}
	}

	cls_menu_overlay();
}





//Retorna el item i
menu_item *menu_retorna_item(menu_item *m,int i)
{

	menu_item *item_next;

        while (i>0)
        {
        	//printf ("m: %p i: %d\n",m,i);
        	item_next=m->next;
        	if (item_next==NULL) return m;  //Controlar si final

                m=item_next;
		i--;
        }

	return m;


}


//Retorna el item i segun posicion x,y del mouse
menu_item *menu_retorna_item_tabulado_xy(menu_item *m,int x,int y,int *linea_buscada)
{

	menu_item *item_next;
	int indice=0;
	int encontrado=0;

	//printf ("buscar item x: %d y: %d\n",x,y);

        while (!encontrado)
        {

        	//Ver si coincide y. x tiene que estar en el rango del texto
        	int longitud_texto=menu_calcular_ancho_string_item(m->texto_opcion);
        	if (y==m->menu_tabulado_y && 
        	    x>=m->menu_tabulado_x && x<m->menu_tabulado_x+longitud_texto) 
        	{
        		encontrado=1;
        	}

        	else {

        		//printf ("m: %p i: %d\n",m,i);
        		item_next=m->next;
	        	if (item_next==NULL) return NULL;  //Controlar si final

                	m=item_next;
			//i--;
			indice++;
		}
        }

        if (encontrado) {
        	*linea_buscada=indice;
		return m;
	}

	else return NULL;


}

void menu_cpu_core_loop(void)
{
                if (menu_multitarea==1) cpu_core_loop();
                else {
                        scr_actualiza_tablas_teclado();
			realjoystick_main();

                        //0.5 ms
                        usleep(MENU_CPU_CORE_LOOP_SLEEP_NO_MULTITASK);


			//printf ("en menu_cpu_core_loop\n");
                }

}



int si_menu_mouse_en_ventana(void)
{
	if (menu_mouse_x>=0 && menu_mouse_y>=0 && menu_mouse_x<current_win_ancho && menu_mouse_y<current_win_alto ) return 1;
	return 0;
}


int menu_allows_mouse(void)
{
	//Primero, fbdev no permite raton
	if (!strcmp(scr_new_driver_name,"fbdev")) return 0;

	//Luego, el resto de los drivers completos (xwindows, sdl, cocoa, ...)

	return si_complete_video_driver();
}


//Retorna las coordenadas absolutas del raton (en tamaño de pixel) teniendo en cuenta todo el tamaño de la interfaz del emulador
void menu_calculate_mouse_xy_absolute_interface_pixel(int *resultado_x,int *resultado_y)
{
		int x,y;


		int mouse_en_emulador=0;
		//printf ("x: %04d y: %04d\n",mouse_x,mouse_y);

		int ancho=screen_get_window_size_width_zoom_border_en();

		if (mouse_x>=0 && mouse_y>=0
			&& mouse_x<=ancho && mouse_y<=screen_get_window_size_height_zoom_border_en() ) {
				//Si mouse esta dentro de la ventana del emulador
				mouse_en_emulador=1;
		}

		if (  (mouse_x!=last_mouse_x || mouse_y !=last_mouse_y) && mouse_en_emulador) {
			mouse_movido=1;
		}
		else mouse_movido=0;

		last_mouse_x=mouse_x;
		last_mouse_y=mouse_y;

		//printf ("x: %04d y: %04d movido=%d\n",mouse_x,mouse_y,mouse_movido);

		//Quitarle el zoom
		x=mouse_x/zoom_x;
		y=mouse_y/zoom_y;

		//Considerar borde pantalla

		//Todo lo que sea negativo o exceda border, nada.

		//printf ("x: %04d y: %04d\n",x,y);



        //margenes de zona interior de pantalla. para modo rainbow
				int margenx_izq;
				int margeny_arr;
				menu_retorna_margenes_border(&margenx_izq,&margeny_arr);

	//Ya no hace falta restar margenes
	margenx_izq=margeny_arr=0;

	x -=margenx_izq;
	y -=margeny_arr;

	//printf ("x: %04d y: %04d\n",x,y);

	//Aqui puede dar negativo, en caso que cursor este en el border
	//si esta justo en los ultimos 8 pixeles, dara entre -7 y -1. al dividir entre 8, retornaria 0, diciendo erroneamente que estamos dentro de ventana

	if (x<0) x-=(menu_char_width*menu_gui_zoom); //posicion entre -7 y -1 y demas, cuenta como -1, -2 al dividir entre 8
	if (y<0) y-=(8*menu_gui_zoom);

	//x /=menu_char_width;
	//y /=8;

	//x /= menu_gui_zoom;
	//y /= menu_gui_zoom;

	//printf ("antes de restar: %d,%d\n",x,y);
	*resultado_x=x;
	*resultado_y=y;
}

//Retorna las coordenadas absolutas del raton (en tamaño de caracter) teniendo en cuenta todo el tamaño de la interfaz del emulador
void menu_calculate_mouse_xy_absolute_interface(int *resultado_x,int *resultado_y)
{
	int x,y;

	menu_calculate_mouse_xy_absolute_interface_pixel(&x,&y);
	x /=menu_char_width;
	y /=8;

	x /= menu_gui_zoom;
	y /= menu_gui_zoom;

	//printf ("antes de restar: %d,%d\n",x,y);
	*resultado_x=x;
	*resultado_y=y;



	
}

//Parecido al anterior pero considerando coordenadas relativas a la ventana actual
void menu_calculate_mouse_xy(void)
{
	int x,y;
	if (menu_allows_mouse() ) {
		menu_calculate_mouse_xy_absolute_interface(&x,&y);
	x -=current_win_x;
	y -=current_win_y;

	menu_mouse_x=x;
	menu_mouse_y=y;
	}
}



//No dejar aparecer el osd keyboard dentro del mismo osd keyboard
int osd_kb_no_mostrar_desde_menu=0;
int timer_osd_keyboard_menu=0;

z80_byte menu_da_todas_teclas(void)
{

	//Ver tambien eventos de mouse de zxvision
    //int pulsado_boton_cerrar=
	zxvision_handle_mouse_events(zxvision_current_window);

	z80_byte acumulado;

	acumulado=255;

	//symbol i shift no cuentan por separado
	acumulado=acumulado & (puerto_65278 | 1) & puerto_65022 & puerto_64510 & puerto_63486 & puerto_61438 & puerto_57342 & puerto_49150 & (puerto_32766 |2) & puerto_especial1 & puerto_especial2 & puerto_especial3 & puerto_especial4;


	//Boton cerrar ventana
	if (mouse_pressed_close_window) {
		acumulado |=1;
	}

	//Boton background ventana
	if (mouse_pressed_background_window) {
		//printf ("pulsado background en menu_da_todas_teclas\n");
		//sleep(5);		
		acumulado |=1;
	}	

	//Boton hotkey ventana
	if (mouse_pressed_hotkey_window) {
		//printf ("pulsado hotkey desde menu_da_todas_teclas\n");
		acumulado &=(255-1);
		//NOTA: indicamos aqui que ha habido pulsacion de tecla,
		//dado que partimos de mascara 255, poner ese bit a 0 le decimos que hay pulsada una tecla
		//Misterio: porque con mouse_pressed_close_window y mouse_pressed_background_window le hago OR 1? no tiene sentido....
	}	


	//no ignorar disparo
	z80_byte valor_joystick=(puerto_especial_joystick&31)^255;
	
	acumulado=acumulado & valor_joystick;

	//printf ("acumulado 0 %d\n",acumulado);

	//contar tambien botones mouse
	if (si_menu_mouse_activado()) {
		//menu_calculate_mouse_xy(); //Ya no hacemos esto pues se ha calculado ya arriba en zxvision_handle_mouse_events
		//quiza pareceria que no hay problema en leerlo dos veces, el problema es con la variable mouse_leido,
		//que al llamarla aqui la segunda vez, siempre dira que el mouse no se ha movido

		//printf("mouse left %d mouse_right %d mouse_movido %d\n",mouse_left,mouse_right,mouse_movido);

		z80_byte valor_botones_mouse=(mouse_left | mouse_right | mouse_movido)^255;
		acumulado=acumulado & valor_botones_mouse;
	}

	//printf ("acumulado 00 %d\n",acumulado);

	//Contar también algunas teclas solo menu:
	z80_byte valor_teclas_menus=(menu_backspace.v|menu_tab.v)^255;
	//printf("valor_teclas_menus: %d\n",valor_teclas_menus);
	acumulado=acumulado & valor_teclas_menus;



	//printf("acumulado: %d\n",acumulado);

	if ( (acumulado&MENU_PUERTO_TECLADO_NINGUNA) !=MENU_PUERTO_TECLADO_NINGUNA) {
		//printf ("Retornamos acumulado en menu_da_todas_teclas: %d\n",acumulado);
		return acumulado;
	}

	
	//printf ("Retornamos acumulado en menu_da_todas_teclas_2: %d\n",acumulado);
	return acumulado;


}


//Para forzar desde remote command a salir de la funcion, sin haber pulsado tecla realmente
//z80_bit menu_espera_tecla_no_cpu_loop_flag_salir={0};

//Esperar a pulsar una tecla sin ejecutar cpu
void menu_espera_tecla_no_cpu_loop(void)
{ 

	z80_byte acumulado;

        do {

                scr_actualiza_tablas_teclado();
		realjoystick_main();

                //0.5 ms
                usleep(500);

                acumulado=menu_da_todas_teclas();

        } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA
				//&&	menu_espera_tecla_no_cpu_loop_flag_salir.v==0
							);


	//menu_espera_tecla_no_cpu_loop_flag_salir.v=0;

}



void menu_espera_no_tecla_no_cpu_loop(void)
{

        //Esperar a liberar teclas
        z80_byte acumulado;

        do {

                scr_actualiza_tablas_teclado();
		realjoystick_main();

                //0.5 ms
                usleep(500);

                acumulado=menu_da_todas_teclas();

        //printf ("menu_espera_no_tecla_no_cpu_loop acumulado: %d\n",acumulado);

        } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) != MENU_PUERTO_TECLADO_NINGUNA);

}


void menu_espera_tecla_timeout_tooltip(void)
{

        //Esperar a pulsar una tecla o timeout de tooltip
        z80_byte acumulado;

        acumulado=menu_da_todas_teclas();

        int resetear_contadores=0;

        //Si se entra y no hay tecla pulsada, resetear contadores
        if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA) {
        	resetear_contadores=1;
        }

        do {
                menu_cpu_core_loop();


                acumulado=menu_da_todas_teclas();

		//printf ("menu_espera_tecla_timeout_tooltip acumulado: %d\n",acumulado);

        } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA && menu_tooltip_counter<TOOLTIP_SECONDS);

	if (resetear_contadores) {
        	menu_reset_counters_tecla_repeticion();
	}

}

/*
void menu_espera_tecla_timeout_window_splash(void)
{
	//printf ("espera splash\n");

        //Esperar a pulsar una tecla o timeout de window splash
        z80_byte acumulado;

        int contador_antes=menu_window_splash_counter_ms;
        int trozos=4;
        //WINDOW_SPLASH_SECONDS. 
        //5 pasos. total de WINDOW_SPLASH_SECONDS
        int tiempototal=1000*WINDOW_SPLASH_SECONDS;
        //Quitamos 1 segundo
        tiempototal-=1000;

        //Intervalo de cambio
        int intervalo=tiempototal/5; //5 pasos
        //printf ("intervalo: %d\n",intervalo);



        do {
                menu_cpu_core_loop();


                acumulado=menu_da_todas_teclas();

                //Cada 400 ms
                if (menu_window_splash_counter_ms-contador_antes>intervalo) {
                	trozos--;
                	contador_antes=menu_window_splash_counter_ms;
                	//printf ("dibujar franjas trozos: %d\n",trozos);
                	if (trozos>=0) {		
                		menu_dibuja_ventana_franja_arcoiris_trozo_current(trozos);
                	}
                }


		//printf ("menu_espera_tecla_timeout_tooltip acumulado: %d\n",acumulado);
		//printf ("contador splash: %d\n",menu_window_splash_counter);
		

	} while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA && menu_window_splash_counter<WINDOW_SPLASH_SECONDS);

}*/

//tipo: 1 volver timeout normal como ventanas splash. 2. no finaliza, franjas continuamente moviendose
void zxvision_espera_tecla_timeout_window_splash(int tipo)
{

	z80_byte tecla;
	//printf ("espera splash\n");
	do {

        //Esperar a pulsar una tecla o timeout de window splash
        //z80_byte acumulado;

        int contador_antes=menu_window_splash_counter_ms;
        int trozos=4;
        //WINDOW_SPLASH_SECONDS. 
        //5 pasos. total de WINDOW_SPLASH_SECONDS
        int tiempototal=1000*WINDOW_SPLASH_SECONDS;
        //Quitamos 1 segundo
        tiempototal-=1000;

        //Intervalo de cambio
        int intervalo=tiempototal/5; //5 pasos
        //printf ("intervalo: %d\n",intervalo);

		int indice_apagado=0;


	

    do {
                menu_cpu_core_loop();


                //acumulado=menu_da_todas_teclas();
				tecla=zxvision_read_keyboard();

				//con boton izquierdo no salimos
				if (tecla==13 && mouse_left) {	
					tecla=0;
				}				

                //Cada 400 ms
                if (menu_window_splash_counter_ms-contador_antes>intervalo) {
                	trozos--;
                	contador_antes=menu_window_splash_counter_ms;
                	//printf ("dibujar franjas trozos: %d\n",trozos);
                	if (trozos>=0) {		
                		if (tipo==1) menu_dibuja_ventana_franja_arcoiris_trozo_current(trozos);
                	}

					if (tipo==2) menu_dibuja_ventana_franja_arcoiris_oscuro_current(indice_apagado);
					indice_apagado++;
                }


		//printf ("menu_espera_tecla_timeout_tooltip acumulado: %d\n",acumulado);
		//printf ("contador splash: %d\n",menu_window_splash_counter);
		

	} while (tecla==0 && menu_window_splash_counter<WINDOW_SPLASH_SECONDS);

	menu_window_splash_counter=0;
	menu_window_splash_counter_ms=0;

	} while (tipo==2 && tecla==0);

}

//Esperar a tecla ESC, o que la condicion de funcion sea diferente de 0
//Cada medio segundo se llama la condicion y tambien la funcion de print
//Poner alguna a NULL si no se quiere llamar
//Funciones de condicion y progreso tambien funcionan aun sin multitarea
void zxvision_espera_tecla_condicion_progreso(zxvision_window *w,int (*funcioncond) (zxvision_window *),void (*funcionprint) (zxvision_window *) )
{

	z80_byte tecla;
	int condicion=0;
	int contador_antes=menu_window_splash_counter_ms;
	int intervalo=20*12; //12 frames de pantalla

	//contador en us
	int contador_no_multitask=0;


	//printf ("espera splash\n");
	do {

                menu_cpu_core_loop();
				int pasado_cuarto_segundo=0;

				//TODO: se puede dar el caso que se llame aqui pero el thread aun no se haya creado, lo que provoca
				//que dice que el thread no esta en ejecucion aun y por tanto cree que esta finalizado, diciendo que la condicion de salida es verdadera
				//y salga cuando aun no ha finalizado
				//Seria raro, porque el intervalo de comprobacion es cada 1/4 de segundo, y en ese tiempo se tiene que haber lanzado el thread de sobra

	 			if (!menu_multitarea) {
					contador_no_multitask+=MENU_CPU_CORE_LOOP_SLEEP_NO_MULTITASK;

					//Cuando se llega a 1/4 segundo ms
					if (contador_no_multitask>=intervalo*1000) {
						//printf ("Pasado medio segundo %d\n",contador_no_multitask);
						contador_no_multitask=0;
						pasado_cuarto_segundo=1;

						//printf ("refresca pantalla\n");
						menu_refresca_pantalla();	
				
					}
				}

                //acumulado=menu_da_todas_teclas();
				tecla=zxvision_read_keyboard();

				//con boton izquierdo no salimos
				if (tecla==13 && mouse_left) {	
					tecla=0;
				}				

				if (menu_window_splash_counter_ms-contador_antes>intervalo) pasado_cuarto_segundo=1;

                //Cada 224 ms
                if (pasado_cuarto_segundo) {
                	//trozos--;
                	contador_antes=menu_window_splash_counter_ms;
                	//printf ("dibujar franjas trozos: %d\n",trozos);
                	//llamar a la condicion
					if (funcioncond!=NULL) condicion=funcioncond(w);

					//llamar a funcion print
					if (funcionprint!=NULL) funcionprint(w);
						
                }
		

	} while (tecla==0 && !condicion);


}



void menu_espera_tecla(void)
{

        //Esperar a pulsar una tecla
        z80_byte acumulado;

	//Si al entrar aqui ya hay tecla pulsada, volver
        acumulado=menu_da_todas_teclas();
        if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) !=MENU_PUERTO_TECLADO_NINGUNA) return;


	do {
		menu_cpu_core_loop();


		acumulado=menu_da_todas_teclas();


	} while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA);

	//Al salir del bucle, reseteamos contadores de repeticion
	menu_reset_counters_tecla_repeticion();

}

void menu_espera_tecla_o_joystick(void)
{

		realjoystick_hit=0;

        //Esperar a pulsar una tecla o joystick
        z80_byte acumulado;

        do {
                menu_cpu_core_loop();


                acumulado=menu_da_todas_teclas();

		//printf ("menu_espera_tecla_o_joystick acumulado: %d\n",acumulado);

        } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA && !realjoystick_hit );

}




void menu_espera_no_tecla(void)
{

        //Esperar a liberar teclas. No ejecutar ni una instruccion cpu si la tecla esta liberada
	//con eso evitamos que cuando salte un breakpoint, que llama aqui, no se ejecute una instruccion y el registro PC apunte a la siguiente instruccion
        z80_byte acumulado;
	int salir=0;

        do {
		acumulado=menu_da_todas_teclas();
		if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) == MENU_PUERTO_TECLADO_NINGUNA) {
			salir=1;
		}

		else {
			menu_cpu_core_loop();
		}

	//printf ("menu_espera_no_tecla acumulado: %d\n",acumulado);

	} while (!salir);

}


#define CONTADOR_HASTA_REPETICION (25)
#define CONTADOR_ENTRE_REPETICION (1)


//0.5 segundos para empezar repeticion (25 frames)
int menu_contador_teclas_repeticion;

int menu_segundo_contador_teclas_repeticion;


void menu_reset_counters_tecla_repeticion(void)
{
	//printf ("menu_reset_counters_tecla_repeticion\n");
                        menu_contador_teclas_repeticion=CONTADOR_HASTA_REPETICION;
                        menu_segundo_contador_teclas_repeticion=CONTADOR_ENTRE_REPETICION;
}

void menu_espera_no_tecla_con_repeticion(void)
{

        //Esperar a liberar teclas, pero si se deja pulsada una tecla el tiempo suficiente, se retorna
        z80_byte acumulado;

        //printf ("menu_espera_no_tecla_con_repeticion %d\n",menu_contador_teclas_repeticion);

	//x frames de segundo entre repeticion
	menu_segundo_contador_teclas_repeticion=CONTADOR_ENTRE_REPETICION;

        do {
                menu_cpu_core_loop();

                acumulado=menu_da_todas_teclas();

        	//printf ("menu_espera_no_tecla_con_repeticion acumulado: %d\n",acumulado);

		//si no hay tecla pulsada, restablecer contadores
		if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) == MENU_PUERTO_TECLADO_NINGUNA) menu_reset_counters_tecla_repeticion();

		//printf ("contadores: 1 %d  2 %d\n",menu_contador_teclas_repeticion,menu_segundo_contador_teclas_repeticion);

        } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) != MENU_PUERTO_TECLADO_NINGUNA && menu_segundo_contador_teclas_repeticion!=0);


}






//Quita de la linea los caracteres de atajo ~~ o ^^ o $$X
void menu_dibuja_menu_stdout_texto_sin_atajo(char *origen, char *destino)
{

	int indice_orig,indice_dest;

	indice_orig=indice_dest=0;

	while (origen[indice_orig]) {
		if (menu_escribe_texto_si_inverso(origen,indice_orig)) {
			indice_orig +=2;
		}

		if (menu_escribe_texto_si_parpadeo(origen,indice_orig)) {
			indice_orig +=2;
		}

		if (menu_escribe_texto_si_cambio_tinta(origen,indice_orig)) {
			indice_orig +=3;
		}


		destino[indice_dest++]=origen[indice_orig++];
	}

	destino[indice_dest]=0;

}





int menu_dibuja_menu_stdout(int *opcion_inicial,menu_item *item_seleccionado,menu_item *m,char *titulo)
{
	int linea_seleccionada=*opcion_inicial;
	char texto_linea_sin_shortcut[64];

	menu_item *aux;

	//Titulo
	printf ("\n");
        printf ("%s\n",titulo);
	printf ("------------------------\n\n");

	aux=m;

        int max_opciones=0;

	int tecla=13;

	//para speech stdout. asumir no tecla pulsada. si se pulsa tecla, para leer menu
	menu_speech_tecla_pulsada=0;

        do {

                max_opciones++;

		if (aux->tipo_opcion!=MENU_OPCION_SEPARADOR) {

			//Ver si esta activa
                        t_menu_funcion_activo menu_funcion_activo;

                        menu_funcion_activo=aux->menu_funcion_activo;

                        if ( menu_funcion_activo!=NULL) {
				if (menu_funcion_activo()!=0) {
					printf ("%2d)",max_opciones);
					char buf[12];
					sprintf (buf,"%d",max_opciones);
				}

				else {
					printf ("x  ");
				}
			}

			else {
				printf ("%2d)",max_opciones);
					char buf[12];
					sprintf (buf,"%d",max_opciones);
			}

			//Imprimir linea menu pero descartando los ~~ de los atajo de teclado o ^^
			menu_dibuja_menu_stdout_texto_sin_atajo(aux->texto_opcion,texto_linea_sin_shortcut);


			printf ( "%s",texto_linea_sin_shortcut);

		}


		printf ("\n");

                aux=aux->next;
        } while (aux!=NULL);

	printf ("\n");

	char texto_opcion[256];

	int salir_opcion;


	do {

		salir_opcion=1;
		printf("Option number? (prepend the option with h for help, t for tooltip). Write esc to go back. ");

		fflush(stdout);
		scanf("%s",texto_opcion);

		if (!strcasecmp(texto_opcion,"esc")) {
			tecla=MENU_RETORNO_ESC;
		}

		else if (texto_opcion[0]=='h' || texto_opcion[0]=='t') {
				salir_opcion=0;
                                char *texto_ayuda;
				linea_seleccionada=atoi(&texto_opcion[1]);
				linea_seleccionada--;
				if (linea_seleccionada>=0 && linea_seleccionada<max_opciones) {

					char *titulo_texto;

					if (texto_opcion[0]=='h') {
		                                texto_ayuda=menu_retorna_item(m,linea_seleccionada)->texto_ayuda;
						titulo_texto="Help";
					}

					else {
						texto_ayuda=menu_retorna_item(m,linea_seleccionada)->texto_tooltip;
						titulo_texto="Tooltip";
					}


        	                        if (texto_ayuda!=NULL) {
                	                        menu_generic_message(titulo_texto,texto_ayuda);
					}
					else {
						printf ("Item has no %s\n",titulo_texto);
					}
                                }
				else {
					printf ("Invalid option number\n");
				}
		}

		else {
			linea_seleccionada=atoi(texto_opcion);

			if (linea_seleccionada<1 || linea_seleccionada>max_opciones) {
				printf ("Incorrect option number\n");
				salir_opcion=0;
			}

			//Numero correcto
			else {
				linea_seleccionada--;

				//Ver si item no es separador
				menu_item *item_seleccionado;
				item_seleccionado=menu_retorna_item(m,linea_seleccionada);

				if (item_seleccionado->tipo_opcion==MENU_OPCION_SEPARADOR) {
					salir_opcion=0;
					printf ("Item is a separator\n");
				}

				else {


					//Ver si item esta activo
        	                	t_menu_funcion_activo menu_funcion_activo;
	        	                menu_funcion_activo=item_seleccionado->menu_funcion_activo;

	                	        if ( menu_funcion_activo!=NULL) {

        	                	        if (menu_funcion_activo()==0) {
							//opcion inactiva
							salir_opcion=0;
							printf ("Item is disabled\n");
						}
					}
				}
                        }



		}

	} while (salir_opcion==0);

        menu_item *menu_sel;
        menu_sel=menu_retorna_item(m,linea_seleccionada);

        item_seleccionado->menu_funcion=menu_sel->menu_funcion;
        item_seleccionado->tipo_opcion=menu_sel->tipo_opcion;
	item_seleccionado->valor_opcion=menu_sel->valor_opcion;
	
		strcpy(item_seleccionado->texto_opcion,menu_sel->texto_opcion);
		strcpy(item_seleccionado->texto_misc,menu_sel->texto_misc);


        //Liberar memoria del menu
        aux=m;
        menu_item *nextfree;

        do {
                //printf ("Liberando %x\n",aux);
                nextfree=aux->next;
                free(aux);
                aux=nextfree;
        } while (aux!=NULL);

	*opcion_inicial=linea_seleccionada;


	if (tecla==MENU_RETORNO_ESC) return MENU_RETORNO_ESC;

	return MENU_RETORNO_NORMAL;
}


//devuelve a que numero de opcion corresponde el atajo pulsado
//-1 si a ninguno
//NULL si a ninguno
int menu_retorna_atajo(menu_item *m,z80_byte tecla)
{

	//Si letra en mayusculas, bajar a minusculas
	if (tecla>='A' && tecla<='Z') tecla +=('a'-'A');

	int linea=0;

	while (m!=NULL) {
		if (m->atajo_tecla==tecla) {
			debug_printf (VERBOSE_DEBUG,"Shortcut found at entry number: %d",linea);
			return linea;
		}
		m=m->next;
		linea++;
	}

	//no encontrado atajo. escribir entradas de menu con atajos para informar al usuario
	menu_writing_inverse_color.v=1;
	return -1;

}

int menu_active_item_primera_vez=1;



void menu_escribe_opciones_zxvision(zxvision_window *ventana,menu_item *aux,int linea_seleccionada,int max_opciones)
{

        int i;

		//Opcion esta permitida seleccionarla (no esta en rojo)
        int opcion_activada;

		int menu_tabulado=0;
		if (aux->es_menu_tabulado) menu_tabulado=1;


		//Opcion de donde esta el cursor
		char texto_opcion_seleccionada[100];
		//Asumimos por si acaso que no hay ninguna activa
		texto_opcion_seleccionada[0]=0;
		



        for (i=0;i<max_opciones;i++) {

			//si la opcion seleccionada es un separador, el cursor saltara a la siguiente
			//Nota: el separador no puede ser final de menu
			//if (linea_seleccionada==i && aux->tipo_opcion==MENU_OPCION_SEPARADOR) linea_seleccionada++;

			t_menu_funcion_activo menu_funcion_activo;

			menu_funcion_activo=aux->menu_funcion_activo;

			if (menu_funcion_activo!=NULL) {
				opcion_activada=menu_funcion_activo();
			}

			else {
				opcion_activada=1;
			}

			//Al listar opciones de menu, decir si la opcion está desabilitada

			//Cuando haya opcion_activa, nos la apuntamos para decirla al final en speech.
			//Y si es la primera vez en ese menu, dice "Selected item". Sino, solo dice el nombre de la opcion
			if (linea_seleccionada==i) {
				if (menu_active_item_primera_vez) {
					sprintf (texto_opcion_seleccionada,"Selected item: %s",aux->texto_opcion);
					menu_active_item_primera_vez=0;
				}

				else {
					sprintf (texto_opcion_seleccionada,"%s",aux->texto_opcion);
				}

			}

			if (menu_tabulado) {
				menu_escribe_linea_opcion_tabulado_zxvision(ventana,i,linea_seleccionada,opcion_activada,aux->texto_opcion,aux->menu_tabulado_x,aux->menu_tabulado_y);
			}
            
			
			else {
				int y_destino=i;
				int linea_seleccionada_destino=linea_seleccionada;

				if (y_destino>=0) {
				
						menu_escribe_linea_opcion_zxvision(ventana,y_destino,linea_seleccionada_destino,opcion_activada,aux->texto_opcion);
					
				}
				
		
			}
            
			
			aux=aux->next;

        }



		if (texto_opcion_seleccionada[0]!=0) {
			//Selected item siempre quiero que se escuche

			//Guardamos estado actual
			int antes_menu_speech_tecla_pulsada=menu_speech_tecla_pulsada;
			menu_speech_tecla_pulsada=0;


			//Al decir la linea seleccionada de menu, decir si la opcion está desabilitada




			//Restauro estado
			//Pero si se ha pulsado tecla, no restaurar estado
			//Esto sino provocaria que , por ejemplo, en la ventana de confirmar yes/no,
			//se entra con menu_speech_tecla_pulsada=0, se pulsa tecla mientras se esta leyendo el item activo,
			//y luego al salir de aqui, se pierde el valor que se habia metido (1) y se vuelve a poner el 0 del principio
			//provocando que cada vez que se mueve el cursor, se relea la ventana entera
			if (menu_speech_tecla_pulsada==0) menu_speech_tecla_pulsada=antes_menu_speech_tecla_pulsada;
		}
}



int menu_dibuja_menu_cursor_arriba(int linea_seleccionada,int max_opciones,menu_item *m)
{
	if (linea_seleccionada==0) linea_seleccionada=max_opciones-1;
	else {
		linea_seleccionada--;
		//ver si la linea seleccionada es un separador
		int salir=0;
		while (menu_retorna_item(m,linea_seleccionada)->tipo_opcion==MENU_OPCION_SEPARADOR && !salir) {
			linea_seleccionada--;
			//si primera linea es separador, nos iremos a -1
			if (linea_seleccionada==-1) {
				linea_seleccionada=max_opciones-1;
				salir=1;
			}
		}
	}
	//si linea resultante es separador, decrementar
	while (menu_retorna_item(m,linea_seleccionada)->tipo_opcion==MENU_OPCION_SEPARADOR) linea_seleccionada--;

	//Decir que se ha pulsado tecla
	menu_speech_tecla_pulsada=1;

	return linea_seleccionada;
}

int menu_dibuja_menu_cursor_abajo(int linea_seleccionada,int max_opciones,menu_item *m)
{
	if (linea_seleccionada==max_opciones-1) linea_seleccionada=0;
	else {
		linea_seleccionada++;
		//ver si la linea seleccionada es un separador
		int salir=0;
		while (menu_retorna_item(m,linea_seleccionada)->tipo_opcion==MENU_OPCION_SEPARADOR && !salir) {
			linea_seleccionada++;
			//si ultima linea es separador, nos salimos de rango
			if (linea_seleccionada==max_opciones) {
				linea_seleccionada=0;
				salir=0;
			}
		}
	}
	//si linea resultante es separador, incrementar
	while (menu_retorna_item(m,linea_seleccionada)->tipo_opcion==MENU_OPCION_SEPARADOR) linea_seleccionada++;

	//Decir que se ha pulsado tecla
	menu_speech_tecla_pulsada=1;

	return linea_seleccionada;
}


int menu_dibuja_menu_cursor_abajo_tabulado(int linea_seleccionada,int max_opciones,menu_item *m)
{

	if (linea_seleccionada==max_opciones-1) linea_seleccionada=0;

		else {

		//Ubicarnos primero en el item de menu seleccionado
		menu_item *m_aux=menu_retorna_item(m,linea_seleccionada);

		//Su coordenada y original
		int orig_tabulado_y=m_aux->menu_tabulado_y;
		int orig_tabulado_x=m_aux->menu_tabulado_x;


		//Y vamos hacia abajo hasta que coordenada y sea diferente
		do {
			//printf ("antes vert orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
			linea_seleccionada=menu_dibuja_menu_cursor_abajo(linea_seleccionada,max_opciones,m);
			m_aux=menu_retorna_item(m,linea_seleccionada);
			//printf ("despues vert orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
		} while (m_aux->menu_tabulado_y==orig_tabulado_y);

		int posible_posicion=linea_seleccionada;
		int final_y=m_aux->menu_tabulado_y;

		//Y ahora buscar la que tenga misma coordenada x o mas a la derecha, si la hubiera
		while (m_aux->menu_tabulado_y==final_y && m_aux->menu_tabulado_x<orig_tabulado_x) {
			posible_posicion=linea_seleccionada;
			//printf ("antes horiz orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
			linea_seleccionada=menu_dibuja_menu_cursor_abajo(linea_seleccionada,max_opciones,m);
			m_aux=menu_retorna_item(m,linea_seleccionada);
			//printf ("despues horiz orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
		};

		//Si no estamos en misma posicion y, volver a posicion
		if (m_aux->menu_tabulado_y!=final_y) linea_seleccionada=posible_posicion;
	}

	//Decir que se ha pulsado tecla
	menu_speech_tecla_pulsada=1;

	return linea_seleccionada;
}

int menu_dibuja_menu_cursor_arriba_tabulado(int linea_seleccionada,int max_opciones,menu_item *m)
{

	if (linea_seleccionada==0) linea_seleccionada=max_opciones-1;

	else {

		//Ubicarnos primero en el item de menu seleccionado
		menu_item *m_aux=menu_retorna_item(m,linea_seleccionada);

		//Su coordenada y original
		int orig_tabulado_y=m_aux->menu_tabulado_y;
		int orig_tabulado_x=m_aux->menu_tabulado_x;


		//Y vamos hacia arriba hasta que coordenada y sea diferente
		do {
			//printf ("antes vert orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
			linea_seleccionada=menu_dibuja_menu_cursor_arriba(linea_seleccionada,max_opciones,m);
			m_aux=menu_retorna_item(m,linea_seleccionada);
			//printf ("despues vert orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
		} while (m_aux->menu_tabulado_y==orig_tabulado_y);

		int posible_posicion=linea_seleccionada;
		int final_y=m_aux->menu_tabulado_y;

		//Y ahora buscar la que tenga misma coordenada x o mas a la derecha, si la hubiera
		while (m_aux->menu_tabulado_y==final_y && m_aux->menu_tabulado_x>orig_tabulado_x) {
			posible_posicion=linea_seleccionada;
			//printf ("antes horiz orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
			linea_seleccionada=menu_dibuja_menu_cursor_arriba(linea_seleccionada,max_opciones,m);
			m_aux=menu_retorna_item(m,linea_seleccionada);
			//printf ("despues horiz orig y: %d y: %d linea_seleccionada: %d texto: %s\n",orig_tabulado_y,m_aux->menu_tabulado_y,linea_seleccionada,m_aux->texto_opcion);
		};

		//Si no estamos en misma posicion y, volver a posicion
		if (m_aux->menu_tabulado_y!=final_y) linea_seleccionada=posible_posicion;

	}

	//Decir que se ha pulsado tecla
	menu_speech_tecla_pulsada=1;

	return linea_seleccionada;
}


int menu_dibuja_menu_cursor_abajo_common(int linea_seleccionada,int max_opciones,menu_item *m)
{
	//Mover abajo
			
	if (m->es_menu_tabulado==0) linea_seleccionada=menu_dibuja_menu_cursor_abajo(linea_seleccionada,max_opciones,m);
	else linea_seleccionada=menu_dibuja_menu_cursor_abajo_tabulado(linea_seleccionada,max_opciones,m);
			
	return linea_seleccionada;
}


int menu_dibuja_menu_cursor_arriba_common(int linea_seleccionada,int max_opciones,menu_item *m)
{
	//Mover arriba
			
	if (m->es_menu_tabulado==0) linea_seleccionada=menu_dibuja_menu_cursor_arriba(linea_seleccionada,max_opciones,m);
	else linea_seleccionada=menu_dibuja_menu_cursor_arriba_tabulado(linea_seleccionada,max_opciones,m);

	return linea_seleccionada;

}



void menu_dibuja_menu_help_tooltip(char *texto, int si_tooltip)
{



                                        //Guardar funcion de texto overlay activo, para menus como el de visual memory por ejemplo, para desactivar temporalmente
                                        void (*previous_function)(void);

                                        previous_function=menu_overlay_function;

                                       //restauramos modo normal de texto de menu
                                       set_menu_overlay_function(normal_overlay_texto_menu);



        if (si_tooltip) {
			//menu_generic_message_tooltip("Tooltip",0,1,0,NULL,"%s",texto);
			//printf ("justo antes de message tooltip\n");
			zxvision_generic_message_tooltip("Tooltip" , 0 ,0,1,0,NULL,0,"%s",texto);
		}
	
		else menu_generic_message("Help",texto);

        //Restauramos funcion anterior de overlay
        set_menu_overlay_function(previous_function);

		if (zxvision_current_window!=NULL) {
			zxvision_draw_window(zxvision_current_window);
			//printf ("antes draw windows contents\n");
			zxvision_draw_window_contents(zxvision_current_window);
		}

		//printf ("antes refrescar pantalla\n");
        menu_refresca_pantalla();

		

}


//Indica que el menu permite repeticiones de teclas. Solo valido al pulsar hotkeys
int menu_dibuja_menu_permite_repeticiones_hotk=0;

void menu_dibuja_menu_espera_no_tecla(void)
{
	if (menu_dibuja_menu_permite_repeticiones_hotk) menu_espera_no_tecla_con_repeticion();
	else menu_espera_no_tecla();
}

int menu_calcular_ancho_string_item(char *texto)
{
	//Devuelve longitud de texto teniendo en cuenta de no sumar caracteres ~~ o ^^ o $$X
	unsigned int l;
	int ancho_calculado=strlen(texto);

	for (l=0;l<strlen(texto);l++) {
			if (menu_escribe_texto_si_inverso(texto,l)) ancho_calculado-=2;
			if (menu_escribe_texto_si_parpadeo(texto,l)) ancho_calculado-=2;
			if (menu_escribe_texto_si_cambio_tinta(texto,l)) ancho_calculado-=3;
	}

	return ancho_calculado;
}

//Decir si usamos hasta la ultima columna, pues no se muestra barra scroll,
//o bien se muestra barra scroll y no usamos hasta ultima columna
//Si hemos cambiado la ventana, retornar no 0
int menu_dibuja_menu_adjust_last_column(zxvision_window *w,int ancho,int alto)
{
			//Si no hay barra scroll vertical, usamos hasta la ultima columna
		int incremento_por_columna=0;
		//printf ("visible height: %d ancho %d alto %d\n",w->visible_height,ancho,alto);
		if (w->visible_height>=alto) {
			incremento_por_columna=1;
		}							

		if (incremento_por_columna) {
			if (w->can_use_all_width==0) {
				//printf ("Usamos hasta la ultima columna\n");
				w->can_use_all_width=1; //Para poder usar la ultima columna de la derecha donde normalmente aparece linea scroll
				w->total_width=ancho-1+1;
				return 1;
			}
		}
		else {
			if (w->can_use_all_width) {
				//printf ("NO usamos hasta la ultima columna\n");
				w->can_use_all_width=0; 
				w->total_width=ancho-1;
				return 1;
			}
		}

		/*printf ("total width: %d ancho: %d\n",ventana_menu.total_width,ancho);
		ventana_menu.total_width=10;
		printf ("total width: %d ancho: %d\n",ventana_menu.total_width,ancho);*/
		//ventana_menu.total_width=
		//printf ("total width: %d visible width %d\n",ventana_menu.total_width,ventana_menu.visible_width);
		//ventana_menu.can_use_all_width=1;  //Esto falla porque en algun momento despues se pierde este parametro

	return 0;

}

z80_int menu_mouse_frame_counter=0;
z80_int menu_mouse_frame_counter_anterior=0;

//Funcion de gestion de menu
//Entrada: opcion_inicial: puntero a opcion inicial seleccionada
//m: estructura de menu (estructura en forma de lista con punteros)
//titulo: titulo de la ventana del menu
//Nota: x,y, ancho, alto de la ventana se calculan segun el contenido de la misma

//Retorno
//valor retorno: tecla pulsada: 0 normal (ENTER), 1 ESCAPE,... MENU_RETORNO_XXXX

//opcion_inicial contiene la opcion seleccionada.
//asigna en item_seleccionado valores de: tipo_opcion, menu_funcion (debe ser una estructura ya asignada)

 

int menu_dibuja_menu(int *opcion_inicial,menu_item *item_seleccionado,menu_item *m,char *titulo)
{


	//no escribir letras de atajos de teclado al entrar en un menu
	menu_writing_inverse_color.v=0;

	//Si se fuerza siempre que aparezcan letras de atajos
	if (menu_force_writing_inverse_color.v) menu_writing_inverse_color.v=1;

	//Primera vez decir selected item. Luego solo el nombre del item
	menu_active_item_primera_vez=1;

    if (!strcmp(scr_new_driver_name,"stdout") ) {

		//Para que se envie a speech
		//TODO: el texto se muestra dos veces en consola: 
		//1- pues es un error y todos se ven en consola. 
		//2- pues es una ventana de stdout y se "dibuja" tal cual en consola
		menu_muestra_pending_error_message();

                //se abre menu con driver stdout. Llamar a menu alternativo

		//si hay menu tabulado, agregamos ESC (pues no se incluye nunca)
		if (m->es_menu_tabulado) menu_add_ESC_item(m);

                return menu_dibuja_menu_stdout(opcion_inicial,item_seleccionado,m,titulo);
    }
/*
        if (if_pending_error_message) {
                if_pending_error_message=0;
                menu_generic_message("ERROR",pending_error_message);
        }
*/


	//esto lo haremos ligeramente despues menu_speech_tecla_pulsada=0;

	if (!menu_dibuja_menu_permite_repeticiones_hotk) {
		//printf ("llamar a menu_reset_counters_tecla_repeticion desde menu_dibuja_menu al inicio\n");
		menu_reset_counters_tecla_repeticion();
	}


	//nota: parece que scr_actualiza_tablas_teclado se debe llamar en el caso de xwindows para que refresque la pantalla->seguramente viene por un evento


	int max_opciones;
	int linea_seleccionada=*opcion_inicial;

	//si la anterior opcion era la final (ESC), establecemos el cursor a 0
	//if (linea_seleccionada<0) linea_seleccionada=0;

	int x,y,ancho,alto;

	menu_item *aux;

	aux=m;

	//contar el numero de opciones totales
	//calcular ancho maximo de la ventana
	int ancho_calculado=0;

	//Para permitir menus mas grandes verticalmente de lo que cabe en ventana.
	//int scroll_opciones=0;


	ancho=menu_dibuja_ventana_ret_ancho_titulo(ZXVISION_MAX_ANCHO_VENTANA,titulo);



//printf ("despues menu_dibuja_ventana_ret_ancho_titulo\n");


	max_opciones=0;
	do {
		


		ancho_calculado=menu_calcular_ancho_string_item(aux->texto_opcion)+2; //+2 espacios


		if (ancho_calculado>ancho) ancho=ancho_calculado;
		//printf ("%s\n",aux->texto);
		aux=aux->next;
		max_opciones++;
	} while (aux!=NULL);

	//printf ("Opciones totales: %d\n",max_opciones);

	alto=max_opciones+2;

	x=menu_center_x()-ancho/2;
	y=menu_center_y()-alto/2;

	int ancho_visible=ancho;
	int alto_visible=alto;

	if (x<0 || y<0 || x+ancho>scr_get_menu_width() || y+alto>scr_get_menu_height()) {
		//char window_error_message[100];
		//sprintf(window_error_message,"Window out of bounds: x: %d y: %d ancho: %d alto: %d",x,y,ancho,alto);
		//cpu_panic(window_error_message);

		//Ajustar limites
		if (x<0) x=0;
		if (y<0) y=0;
		if (x+ancho>scr_get_menu_width()) ancho_visible=scr_get_menu_width()-x;
		if (y+alto>scr_get_menu_height()) alto_visible=scr_get_menu_height()-y;
	}

	int redibuja_ventana;
	int tecla;

	//Apuntamos a ventana usada. Si no es menu tabulado, creamos una nosotros
	//Si es tabulado, usamos current_window (pues ya alguien la ha creado antes)
	zxvision_window *ventana;
	zxvision_window ventana_menu;



	if (m->es_menu_tabulado==0) {
		



		//zxvision_new_window(&ventana_menu,x,y,ancho_visible,alto_visible,
		//					ancho-1,alto-2,titulo);		 //hacer de momento igual de ancho que ancho visible para poder usar ultima columna


		//Hacer 1 mas de ancho total para poder usar columna derecha
		zxvision_new_window(&ventana_menu,x,y,ancho_visible,alto_visible,
							ancho-1+1,alto-2,titulo);		 //hacer de momento igual de ancho que ancho visible para poder usar ultima columna


		//Si no hay barra scroll vertical, usamos hasta la ultima columna
		menu_dibuja_menu_adjust_last_column(&ventana_menu,ancho,alto);



		ventana=&ventana_menu;

	}

	else {
		ventana=zxvision_current_window;
	}


	zxvision_draw_window(ventana);	

	//printf ("despues de zxvision_draw_window\n");

	//Entrar aqui cada vez que se dibuje otra subventana aparte, como tooltip o ayuda
	do {
		redibuja_ventana=0;

		//printf ("Entrada desde subventana aparte, como tooltip o ayuda\n");


		menu_tooltip_counter=0;


		tecla=0;

        //si la opcion seleccionada es mayor que el total de opciones, seleccionamos linea 0
        //esto pasa por ejemplo cuando activamos realvideo, dejamos el cursor por debajo, y cambiamos a zxspectrum
        //printf ("linea %d max %d\n",linea_seleccionada,max_opciones);
        if (linea_seleccionada>=max_opciones) {
                debug_printf(VERBOSE_INFO,"Selected Option beyond limits. Set option to 0");
                linea_seleccionada=0;
        }


	//menu_retorna_item(m,linea_seleccionada)->tipo_opcion==MENU_OPCION_SEPARADOR
	//si opcion activa es un separador (que esto pasa por ejemplo cuando activamos realvideo, dejamos el cursor por debajo, y cambiamos a zxspectrum)
	//en ese caso, seleccionamos linea 0
	if (menu_retorna_item(m,linea_seleccionada)->tipo_opcion==MENU_OPCION_SEPARADOR) {
		debug_printf(VERBOSE_INFO,"Selected Option is a separator. Set option to 0");
		linea_seleccionada=0;
	}


	while (tecla!=13 && tecla!=32 && tecla!=MENU_RETORNO_ESC && tecla!=MENU_RETORNO_F1 && tecla!=MENU_RETORNO_F2 && tecla!=MENU_RETORNO_F10 && tecla!=MENU_RETORNO_BACKGROUND && redibuja_ventana==0 && menu_tooltip_counter<TOOLTIP_SECONDS) {

		//printf ("tecla desde bucle: %d\n",tecla);
		//Ajustar scroll
		//scroll_opciones=0;


		//desactivado en zxvision , tiene su propio scroll
	


		//Si menu tabulado, ajustamos scroll de zxvision
		if (m->es_menu_tabulado) {
			int linea_cursor=menu_retorna_item(m,linea_seleccionada)->menu_tabulado_y;
			//printf ("ajustar scroll a %d\n",linea_cursor);
			zxvision_set_offset_y_visible(ventana,linea_cursor);
		}

		else {
			zxvision_set_offset_y_visible(ventana,linea_seleccionada);
		}





		//escribir todas opciones
		//printf ("Escribiendo de nuevo las opciones\n");
		menu_escribe_opciones_zxvision(ventana,m,linea_seleccionada,max_opciones);


	
		//printf ("Linea seleccionada: %d\n",linea_seleccionada);
		//No queremos que el speech vuelva a leer la ventana
		//menu_speech_tecla_pulsada=1;
		zxvision_draw_window_contents_no_speech(ventana);

		//printf ("despues de zxvision_draw_window_contents_no_speech\n");


        menu_refresca_pantalla();

		//printf ("despues de menu_refresca_pantalla\n");

		tecla=0;

		//la inicializamos a 0. aunque parece que no haga falta, podria ser que el bucle siguiente
		//no se entrase (porque menu_tooltip_counter<TOOLTIP_SECONDS) y entonces tecla_leida tendria valor indefinido
		int tecla_leida=0;


		//Si se estaba escuchando speech y se pulsa una tecla, esa tecla debe entrar aqui tal cual y por tanto, no hacemos espera_no_tecla
		//temp menu_espera_no_tecla();
		if (menu_speech_tecla_pulsada==0) {
			//menu_espera_no_tecla();
			menu_dibuja_menu_espera_no_tecla();
		}
		menu_speech_tecla_pulsada=0;

		while (tecla==0 && redibuja_ventana==0 && menu_tooltip_counter<TOOLTIP_SECONDS) {


			//Si no hay barra scroll vertical, usamos hasta la ultima columna, solo para menus no tabulados
			if (m->es_menu_tabulado==0) {
				if (menu_dibuja_menu_adjust_last_column(ventana,ancho,alto)) {
					//printf ("Redibujar ventana pues hay cambio en columna final de scroll\n");

					//Es conveniente llamar antes a zxvision_draw_window pues este establece parametros de ventana_ancho y alto,
					//que se leen luego en menu_escribe_opciones_zxvision
					//sin embargo, al llamar a menu_escribe_opciones_zxvision, el cursor sigue apareciendo como mas pequeño hasta que
					//no se pulsa tecla
					//printf ("ventana ancho antes: %d\n",ventana_ancho);
					zxvision_draw_window(ventana);
					//printf ("ventana ancho despues: %d\n",ventana_ancho);

					//borrar contenido ventana despues de redimensionarla con espacios
					int i;
					for (i=0;i<ventana->total_height;i++) zxvision_print_string_defaults_fillspc(ventana,0,i,"");

					menu_escribe_opciones_zxvision(ventana,m,linea_seleccionada,max_opciones);
					
					zxvision_draw_window_contents(ventana);
				}
			}

			//Si no hubera este menu_refresca_pantalla cuando multitask esta a off,
			//no se moverian las ventanas con refresco al mover raton
			//el resto de cosas funcionaria bien
             if (!menu_multitarea) {
                        menu_refresca_pantalla();
                }


			menu_espera_tecla_timeout_tooltip();

			//Guardamos valor de mouse_movido pues se perdera el valor al leer el teclado de nuevo
			int antes_mouse_movido=mouse_movido;

			tecla_leida=zxvision_read_keyboard();

			//printf ("Despues tecla leida: %d\n",tecla_leida);

			mouse_movido=antes_mouse_movido;

			//Para poder usar repeticiones
			if (tecla_leida==0) {
				//printf ("llamar a menu_reset_counters_tecla_repeticion desde menu_dibuja_menu cuando tecla=0\n");
				menu_reset_counters_tecla_repeticion();
			}

			else {
				//printf ("no reset counter tecla %d\n",tecla);
			}




		
			//printf ("mouse_movido: %d\n",mouse_movido);


			//printf ("tecla_leida: %d\n",tecla_leida);
			if (mouse_movido) {
				//printf ("mouse x: %d y: %d menu mouse x: %d y: %d\n",mouse_x,mouse_y,menu_mouse_x,menu_mouse_y);
				//printf ("ventana x %d y %d ancho %d alto %d\n",ventana_x,ventana_y,ventana_ancho,ventana_alto);
				if (si_menu_mouse_en_ventana() ) {
				//if (menu_mouse_x>=0 && menu_mouse_y>=0 && menu_mouse_x<ventana_ancho && menu_mouse_y<ventana_alto ) {
					//printf ("dentro ventana\n");
					//Descartar linea titulo y ultima linea

					if (menu_mouse_y>0 && menu_mouse_y<current_win_alto-1) {
						//printf ("dentro espacio efectivo ventana\n");
						//Ver si hay que subir o bajar cursor
						int posicion_raton_y=menu_mouse_y-1;

						//tener en cuenta scroll
						posicion_raton_y +=ventana->offset_y;

						//Si no se selecciona separador. Menu no tabulado
						if (m->es_menu_tabulado==0) {
							if (menu_retorna_item(m,posicion_raton_y)->tipo_opcion!=MENU_OPCION_SEPARADOR) {
								linea_seleccionada=posicion_raton_y;
								redibuja_ventana=1;
								menu_tooltip_counter=0;
							}
						}
						else {
							menu_item *buscar_tabulado;
							int linea_buscada;
							int posicion_raton_x=menu_mouse_x;
							buscar_tabulado=menu_retorna_item_tabulado_xy(m,posicion_raton_x,posicion_raton_y,&linea_buscada);

							if (buscar_tabulado!=NULL) {
								//Buscar por coincidencia de coordenada x,y
								if (buscar_tabulado->tipo_opcion!=MENU_OPCION_SEPARADOR) {
									linea_seleccionada=linea_buscada;
									redibuja_ventana=1;
									menu_tooltip_counter=0;
								}
							}
							else {
								//printf ("item no encontrado\n");
							}
						}

					}
					//else {
					//	printf ("En espacio ventana no usable\n");
					//}
				}
				//else {
				//	printf ("fuera ventana\n");
				//}
			}

			//mouse boton izquierdo es como enter
			int mouse_en_zona_opciones=1;

			//if (menu_mouse_x>=0 && menu_mouse_y>=0 && menu_mouse_x<ventana_ancho && menu_mouse_y<ventana_alto ) return 1;

			//Mouse en columna ultima de la derecha,
			//o mouse en primera linea
			//o mouse en ultima linea
			// no enviamos enter si pulsamos boton
			if (menu_mouse_x==current_win_ancho-1 || menu_mouse_y==0 || menu_mouse_y==current_win_alto-1) mouse_en_zona_opciones=0;

			//printf ("Despues tecla leida2: %d\n",tecla_leida);

			if (si_menu_mouse_en_ventana() && mouse_left && mouse_en_zona_opciones && !mouse_is_dragging) {
				//printf ("Enviamos enter\n");
				tecla=13;
			}					


			else if (tecla_leida==11) tecla='7';
			else if (tecla_leida==10) tecla='6';
			else if (tecla_leida==13) tecla=13;
			else if (tecla_leida==24) tecla=24;
			else if (tecla_leida==25) tecla=25;


			//Teclas para menus tabulados
			else if (tecla_leida==8) tecla='5';	
			else if (tecla_leida==9) tecla='8';	

			else if (tecla_leida==2) {
				//tecla=2; //ESC que viene de cerrar ventana al pulsar con raton boton de cerrar en titulo
				tecla=MENU_RETORNO_ESC;
				//printf ("tecla final es ESC\n");
			}

			else if (tecla_leida==3) {
				//printf("Pressed background key on menu\n");
				tecla=MENU_RETORNO_BACKGROUND;
			}


			else if ((puerto_especial1 & 1)==0) {
				//Enter
				//printf ("Leido ESC\n");
				tecla=MENU_RETORNO_ESC;
			}



			//En principio ya no volvemos mas con F1, dado que este se usa para ayuda contextual de cada funcion

			//F1 (ayuda) o h en drivers que no soportan F
            else if ((puerto_especial2 & 1)==0 || (tecla_leida=='h' && f_functions==0) ) {
                                //F1
				char *texto_ayuda;
				texto_ayuda=menu_retorna_item(m,linea_seleccionada)->texto_ayuda;
				if (texto_ayuda!=NULL) {
					//Forzar que siempre suene
					//Esperamos antes a liberar tecla, sino lo que hara sera que esa misma tecla F1 cancelara el speech texto de ayuda
					menu_espera_no_tecla();
					menu_speech_tecla_pulsada=0;


					menu_dibuja_menu_help_tooltip(texto_ayuda,0);


					redibuja_ventana=1;
					menu_tooltip_counter=0;
					//Y volver a decir "selected item"
					menu_active_item_primera_vez=1;

				}
                        }


                        else if ((puerto_especial2 & 2)==0) {
                                //F2
                                tecla=MENU_RETORNO_F2;
                        }

                        else if ((puerto_especial3 & 16)==0) {
                                //F10
                                tecla=MENU_RETORNO_F10;
                        }


			//teclas de atajos. De momento solo admitido entre a y z
			else if ( (tecla_leida>='a' && tecla_leida<='z') || (tecla_leida>='A' && tecla_leida<='Z')) {
				debug_printf (VERBOSE_DEBUG,"Read key: %c. Possibly shortcut",tecla_leida);
				tecla=tecla_leida;
			}

			//tecla espacio. acciones adicionales. Ejemplo en breakpoints para desactivar
			else if (tecla_leida==32) {
				debug_printf (VERBOSE_DEBUG,"Pressed key space");
				tecla=32;
            }

				


			else {
				//printf ("Final ponemos tecla a 0. Era %d\n",tecla);
				tecla=0;
			}


			//printf ("menu tecla: %d\n",tecla);
		}

		//Si no se ha pulsado tecla de atajo:
		if (!((tecla_leida>='a' && tecla_leida<='z') || (tecla_leida>='A' && tecla_leida<='Z')) ) {
			menu_espera_no_tecla();

		

		}



        t_menu_funcion_activo sel_activo;

		t_menu_funcion funcion_espacio;

		if (tecla!=0) menu_tooltip_counter=0;

		int lineas_mover_pgup_dn;
		int conta_mover_pgup_dn;

		//printf ("tecla: %d\n",tecla);

		switch (tecla) {
			case 13:
				//ver si la opcion seleccionada esta activa

				sel_activo=menu_retorna_item(m,linea_seleccionada)->menu_funcion_activo;

				if (sel_activo!=NULL) {
		                	if ( sel_activo()==0 ) tecla=0;  //desactivamos seleccion
				}
                        break;


			//Mover Izquierda, solo en tabulados
            case '5':
            	//en menus tabulados, misma funcion que arriba para un no tabulado
                if (m->es_menu_tabulado==0) break;

                //Si es tabulado, seguira hasta la opcion '7'
				linea_seleccionada=menu_dibuja_menu_cursor_arriba(linea_seleccionada,max_opciones,m);
			break; 


			//Mover Derecha, solo en tabulados
			case '8':
				//en menus tabulados, misma funcion que abajo para un no tabulado
				if (m->es_menu_tabulado==0) break;

				linea_seleccionada=menu_dibuja_menu_cursor_abajo(linea_seleccionada,max_opciones,m);
			break;

			//Mover abajo
			case '6':
				linea_seleccionada=menu_dibuja_menu_cursor_abajo_common(linea_seleccionada,max_opciones,m);
			break;

			//Mover arriba
			case '7':
				linea_seleccionada=menu_dibuja_menu_cursor_arriba_common(linea_seleccionada,max_opciones,m);
			break;			

			//PgUp
			case 24:
				lineas_mover_pgup_dn=ventana->visible_height-3;
				//Ver si al limite de arriba
				if (linea_seleccionada-lineas_mover_pgup_dn<0) {
					lineas_mover_pgup_dn=linea_seleccionada-1; //el -1 final es por tener en cuenta el separador de siempre
				}

				//TODO esto movera el cursor tantas lineas como lineas visibles tiene el menu,
				//si hay algun item como separador, se lo saltara, moviendo el cursor mas lineas de lo deseado
				//printf ("lineas mover: %d\n",lineas_mover_pgup_dn);
				for (conta_mover_pgup_dn=0;conta_mover_pgup_dn<lineas_mover_pgup_dn;conta_mover_pgup_dn++) linea_seleccionada=menu_dibuja_menu_cursor_arriba_common(linea_seleccionada,max_opciones,m);
				
			break;

			//PgUp
			case 25:
				lineas_mover_pgup_dn=ventana->visible_height-3;
				//Ver si al limite de abajo
				if (linea_seleccionada+lineas_mover_pgup_dn>=max_opciones) {
					lineas_mover_pgup_dn=max_opciones-linea_seleccionada-1-1; //el -1 final es por tener en cuenta el separador de siempre
				}

				//TODO esto movera el cursor tantas lineas como lineas visibles tiene el menu,
				//si hay algun item como separador, se lo saltara, moviendo el cursor mas lineas de lo deseado
				//printf ("lineas mover: %d\n",lineas_mover_pgup_dn);
				//int i;
				for (conta_mover_pgup_dn=0;conta_mover_pgup_dn<lineas_mover_pgup_dn;conta_mover_pgup_dn++) linea_seleccionada=menu_dibuja_menu_cursor_abajo_common(linea_seleccionada,max_opciones,m);
				
			break;



			case 32:
				//Accion para tecla espacio
				//printf ("Pulsado espacio\n");
                                //decimos que se ha pulsado Enter
                                //tecla=13;

				//Ver si tecla asociada a espacio
				funcion_espacio=menu_retorna_item(m,linea_seleccionada)->menu_funcion_espacio;

				if (funcion_espacio==NULL) {
					debug_printf (VERBOSE_DEBUG,"No space key function associated to this menu item");
					tecla=0;
				}

				else {

					debug_printf (VERBOSE_DEBUG,"Found space key function associated to this menu item");

	                                //ver si la opcion seleccionada esta activa

        	                        sel_activo=menu_retorna_item(m,linea_seleccionada)->menu_funcion_activo;

                	                if (sel_activo!=NULL) {
                        	                if ( sel_activo()==0 ) {
							tecla=0;  //desactivamos seleccion
							debug_printf (VERBOSE_DEBUG,"Menu item is disabled");
						}
                                	}

				}

			break;



		}

		//teclas de atajos. De momento solo admitido entre a y z
		if ( (tecla>='a' && tecla<='z') || (tecla>='A' && tecla<='Z')) {
			//printf ("buscamos atajo\n");

			int entrada_atajo;
			entrada_atajo=menu_retorna_atajo(m,tecla);


			//Encontrado atajo
			if (entrada_atajo!=-1) {
				linea_seleccionada=entrada_atajo;

				//Mostrar por un momento opciones y letras
				menu_writing_inverse_color.v=1;
				menu_escribe_opciones_zxvision(ventana,m,entrada_atajo,max_opciones);
				menu_refresca_pantalla();
				//menu_espera_no_tecla();
				menu_dibuja_menu_espera_no_tecla();

				//decimos que se ha pulsado Enter
				tecla=13;

	                        //Ver si esa opcion esta habilitada o no
        	                t_menu_funcion_activo sel_activo;
                	        sel_activo=menu_retorna_item(m,linea_seleccionada)->menu_funcion_activo;
                        	if (sel_activo!=NULL) {
	                                //opcion no habilitada
        	                        if ( sel_activo()==0 ) {
                	                        debug_printf (VERBOSE_DEBUG,"Shortcut found at entry number %d but entry disabled",linea_seleccionada);
						tecla=0;
                                	}
	                        }


			}

			else {
				debug_printf (VERBOSE_DEBUG,"No shortcut found for read key: %c",tecla);
				tecla=0;
				menu_espera_no_tecla();
			}
		}



	}

	//NOTA: contador de tooltip se incrementa desde bucle de timer, ejecutado desde cpu loop
	//Si no hay multitask de menu, NO se incrementa contador y por tanto no hay tooltip

	if (menu_tooltip_counter>=TOOLTIP_SECONDS) {

        redibuja_ventana=1;

		//Por defecto asumimos que no saltara tooltip y por tanto que no queremos que vuelva a enviar a speech la ventana
		//Aunque si que volvera a decir el "Selected item: ..." en casos que se este en una opcion sin tooltip,
		//no aparecera el tooltip pero vendra aqui con el timeout y esto hara redibujar la ventana por redibuja_ventana=1
		//si quitase ese redibujado, lo que pasaria es que no aparecerian los atajos de teclado para cada opcion
		//Entonces tal y como esta ahora:
		//Si la opcion seleccionada tiene tooltip, salta el tooltip
		//Si no tiene tooltip, no salta tooltip, pero vuelve a decir "Selected item: ..."
		menu_speech_tecla_pulsada=1;

		//Si ventana no esta activa, no mostrar tooltips,
		//porque esto hace que, por ejemplo, si el foco está en la máquina emulada, al saltar el tooltip, cambiaria el foco a la ventana de menu
		if (tooltip_enabled.v && ventana_tipo_activa) {
			char *texto_tooltip;
			texto_tooltip=menu_retorna_item(m,linea_seleccionada)->texto_tooltip;
			if (texto_tooltip!=NULL) {
				//printf ("mostramos tooltip\n");
				//Forzar que siempre suene
				menu_speech_tecla_pulsada=0;


				menu_dibuja_menu_help_tooltip(texto_tooltip,1);

				//printf ("despues de mostrar tooltip\n");


				//Esperar no tecla
				menu_espera_no_tecla();


				//Y volver a decir "Selected item"
				menu_active_item_primera_vez=1;


				//Y reactivar parametros ventana usados en menu_dibuja_ventana
				//zxvision_set_draw_window_parameters(ventana);

	        }

			else {
				//printf ("no hay tooltip\n");

				//No queremos que se vuelva a leer cuando tooltip es inexistente. si no, estaria todo el rato releyendo la linea
				//TODO: esto no tiene efecto, sigue releyendo cuando estas sobre item que no tiene tooltip
				//menu_speech_tecla_pulsada=1;	
	
			}

		}

		//else printf ("No mostrar tooltip\n");

		//Hay que dibujar las letras correspondientes en texto inverso
		menu_writing_inverse_color.v=1;

		menu_tooltip_counter=0;
	}

	} while (redibuja_ventana==1);

	*opcion_inicial=linea_seleccionada;

	//nos apuntamos valor de retorno

	menu_item *menu_sel;
	menu_sel=menu_retorna_item(m,linea_seleccionada);

	//Si tecla espacio
	if (tecla==32) {
		item_seleccionado->menu_funcion=menu_sel->menu_funcion_espacio;
		tecla=13;
	}
	else item_seleccionado->menu_funcion=menu_sel->menu_funcion;

	item_seleccionado->tipo_opcion=menu_sel->tipo_opcion;
	item_seleccionado->valor_opcion=menu_sel->valor_opcion;
	strcpy(item_seleccionado->texto_opcion,menu_sel->texto_opcion);
	strcpy(item_seleccionado->texto_misc,menu_sel->texto_misc);

	//printf ("misc selected: %s %s\n",item_seleccionado->texto_misc,menu_sel->texto_misc);
	
	//guardamos antes si el tipo es tabulado antes de
	//liberar el item de menu
	int es_tabulado=m->es_menu_tabulado;


	//Liberar memoria del menu
        aux=m;
	menu_item *nextfree;

        do {
		//printf ("Liberando %x\n",aux);
		nextfree=aux->next;
		free(aux);
                aux=nextfree;
        } while (aux!=NULL);


	//Salir del menu diciendo que no se ha pulsado tecla
	menu_speech_tecla_pulsada=0;


	//En caso de menus tabulados, es responsabilidad de este de borrar con cls y liberar ventana 
	if (es_tabulado==0) {
		cls_menu_overlay();
		zxvision_destroy_window(ventana);
	}

	//printf ("tecla: %d\n",tecla);

	if (tecla==MENU_RETORNO_ESC) return MENU_RETORNO_ESC;
	else if (tecla==MENU_RETORNO_F1) return MENU_RETORNO_F1;
	else if (tecla==MENU_RETORNO_F2) return MENU_RETORNO_F2;
	else if (tecla==MENU_RETORNO_F10) return MENU_RETORNO_F10;
	else if (tecla==MENU_RETORNO_BACKGROUND) return MENU_RETORNO_BACKGROUND;

	else return MENU_RETORNO_NORMAL;

}

















//Agregar el item inicial del menu
//Parametros: puntero al puntero de menu_item inicial. texto
void menu_add_item_menu_inicial(menu_item **p,char *texto,int tipo_opcion,t_menu_funcion menu_funcion,t_menu_funcion_activo menu_funcion_activo)
{

	menu_item *m;

	m=malloc(sizeof(menu_item));

	//printf ("%d\n",sizeof(menu_item));

        if (m==NULL) cpu_panic("Cannot allocate initial menu item");

	//comprobacion de maximo
	if (strlen(texto)>MAX_TEXTO_OPCION) cpu_panic ("Text item greater than maximum");

	//m->texto=texto;
	strcpy(m->texto_opcion,texto);





	m->tipo_opcion=tipo_opcion;
	m->menu_funcion=menu_funcion;
	m->menu_funcion_activo=menu_funcion_activo;
	m->texto_ayuda=NULL;
	m->texto_tooltip=NULL;

	//Por defecto inicializado a ""
	m->texto_misc[0]=0;

	m->atajo_tecla=0;
	m->menu_funcion_espacio=NULL;


	m->es_menu_tabulado=0; //por defecto no es menu tabulado. esta opcion se hereda en cada item, desde el primero


	m->next=NULL;


	*p=m;
}

//Agregar un item al menu
//Parametros: puntero de menu_item inicial. texto
void menu_add_item_menu(menu_item *m,char *texto,int tipo_opcion,t_menu_funcion menu_funcion,t_menu_funcion_activo menu_funcion_activo)
{
	//busca el ultimo item i le añade el indicado. O hasta que encuentre uno con MENU_OPCION_UNASSIGNED, que tendera a ser el ultimo

	while (m->next!=NULL && m->tipo_opcion!=MENU_OPCION_UNASSIGNED)
	{
		m=m->next;
	}

	menu_item *next;




	if (m->tipo_opcion==MENU_OPCION_UNASSIGNED) {
		debug_printf (VERBOSE_DEBUG,"Overwrite last item menu because it was MENU_OPCION_UNASSIGNED");
		next=m;
	}

	else {

		next=malloc(sizeof(menu_item));
		//printf ("%d\n",sizeof(menu_item));

		if (next==NULL) cpu_panic("Cannot allocate menu item");

		m->next=next;
	}


	//Si era menu tabulado. Heredamos la opcion. Aunque se debe establecer la x,y luego para cada item, lo mantenemos asi para que cada item,
	//tengan ese parametro
	int es_menu_tabulado;
	es_menu_tabulado=m->es_menu_tabulado;

	//comprobacion de maximo
	if (strlen(texto)>MAX_TEXTO_OPCION) cpu_panic ("Text item greater than maximum");

	//next->texto=texto;
	strcpy(next->texto_opcion,texto);



	next->tipo_opcion=tipo_opcion;
	next->menu_funcion=menu_funcion;
	next->menu_funcion_activo=menu_funcion_activo;
	next->texto_ayuda=NULL;
	next->texto_tooltip=NULL;

	//Por defecto inicializado a ""
	next->texto_misc[0]=0;

	next->atajo_tecla=0;
	next->menu_funcion_espacio=NULL;
	next->es_menu_tabulado=es_menu_tabulado;
	next->next=NULL;
}

//Agregar ayuda al ultimo item de menu
void menu_add_item_menu_ayuda(menu_item *m,char *texto_ayuda)
{
       //busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

	m->texto_ayuda=texto_ayuda;
}

//Agregar tooltip al ultimo item de menu
void menu_add_item_menu_tooltip(menu_item *m,char *texto_tooltip)
{
       //busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

        m->texto_tooltip=texto_tooltip;
}

//Agregar atajo de tecla al ultimo item de menu
void menu_add_item_menu_shortcut(menu_item *m,z80_byte tecla)
{
       //busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

        m->atajo_tecla=tecla;
}


//Agregar funcion de gestion de tecla espacio
void menu_add_item_menu_espacio(menu_item *m,t_menu_funcion menu_funcion_espacio)
{
//busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

        m->menu_funcion_espacio=menu_funcion_espacio;
}


//Indicar que es menu tabulado. Se hace para todos los items, dado que establece coordenada x,y
void menu_add_item_menu_tabulado(menu_item *m,int x,int y)
{
//busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

        m->es_menu_tabulado=1;
	m->menu_tabulado_x=x;
	m->menu_tabulado_y=y;
}


//Agregar un valor como opcion al ultimo item de menu
//Esto sirve, por ejemplo, para que cuando esta en el menu de z88, insertar slot,
//se pueda saber que slot se ha seleccionado
void menu_add_item_menu_valor_opcion(menu_item *m,int valor_opcion)
{
       //busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

	//printf ("temp. agregar valor opcion %d\n",valor_opcion);

        m->valor_opcion=valor_opcion;
}


//Agregar texto misc al ultimo item de menu
//Esto sirve, por ejemplo, para guardar url en navegador online
void menu_add_item_menu_misc(menu_item *m,char *texto_misc)
{
       //busca el ultimo item i le añade el indicado

        while (m->next!=NULL)
        {
                m=m->next;
        }

		

        strcpy(m->texto_misc,texto_misc);

		//printf ("agregado texto misc %s\n",m->texto_misc);
}


//Agregar un item al menu
//Parametros: puntero de menu_item inicial. texto con formato
void menu_add_item_menu_format(menu_item *m,int tipo_opcion,t_menu_funcion menu_funcion,t_menu_funcion_activo menu_funcion_activo,const char * format , ...)
{
	char buffer[100];
	va_list args;
	va_start (args, format);
	vsprintf (buffer,format, args);
	va_end (args);

	menu_add_item_menu(m,buffer,tipo_opcion,menu_funcion,menu_funcion_activo);
}


//Agregar el item inicial del menu
//Parametros: puntero al puntero de menu_item inicial. texto con formato
void menu_add_item_menu_inicial_format(menu_item **p,int tipo_opcion,t_menu_funcion menu_funcion,t_menu_funcion_activo menu_funcion_activo,const char * format , ...)
{
        char buffer[100];
        va_list args;
        va_start (args, format);
        vsprintf (buffer,format, args);
	va_end (args);

        menu_add_item_menu_inicial(p,buffer,tipo_opcion,menu_funcion,menu_funcion_activo);

}

char *string_esc_go_back="ESC always goes back to the previous menu, or return back to the emulated machine if you are in main menu";

//Agrega item de ESC normalmente.  En caso de aalib y consola es con tecla TAB
void menu_add_ESC_item(menu_item *array_menu_item)
{

        char mensaje_esc_back[32];

        sprintf (mensaje_esc_back,"%s Back",esc_key_message);

        menu_add_item_menu(array_menu_item,mensaje_esc_back,MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
		menu_add_item_menu_tooltip(array_menu_item,string_esc_go_back);
		menu_add_item_menu_ayuda(array_menu_item,string_esc_go_back);

}


void menu_linea_zxvision(zxvision_window *ventana,int x,int y1,int y2,int color)
{

	int yorigen;
	int ydestino;


	//empezamos de menos a mas
	if (y1<y2) {
		yorigen=y1;
		ydestino=y2;
	}

	else {
		yorigen=y2;
		ydestino=y1;
	}


	for (;yorigen<=ydestino;yorigen++) {
		zxvision_putpixel(ventana,x,yorigen,color);
	}
}


//devuelve 1 si el directorio cumple el filtro
//realmente lo que hacemos aqui es ocultar/mostrar carpetas que empiezan con .
int menu_file_filter_dir(const char *name,char *filtros[])
{

        int i;
        //char extension[1024];

        //directorio ".." siempre se muestra
        if (!strcmp(name,"..")) return 1;

        char *f;


        //Bucle por cada filtro
        for (i=0;filtros[i];i++) {
                //si filtro es "", significa todo (*)
                //supuestamente si hay filtro "" no habrian mas filtros pasados en el array...

 	       f=filtros[i];
                if (f[0]==0) return 1;

                //si filtro no es *, ocultamos los que empiezan por "."
                if (name[0]=='.') return 0;

        }


	//y finalmente mostramos el directorio
        return 1;

}





//devuelve 1 si el archivo cumple el filtro
int menu_file_filter(const char *name,char *filtros[])
{

	int i;
	char extension[NAME_MAX];

	/*
	//obtener extension del nombre
	//buscar ultimo punto

	int j;
	j=strlen(name);
	if (j==0) extension[0]=0;
	else {
		for (;j>=0 && name[j]!='.';j--);

		if (j>=0) strcpy(extension,&name[j+1]);
		else extension[0]=0;
	}

	//printf ("Extension: %s\n",extension);

	*/

	//El archivo MENU_LAST_DIR_FILE_NAME zesarux_last_dir.txt usado para abrir archivos comprimidos, no lo mostrare nunca
	if (!strcmp(name,MENU_LAST_DIR_FILE_NAME)) return 0;

	//Archivo usado para indicar que archivo es la pantalla del juego. Usado en previews de tap, tzx etc
	if (!strcmp(name,MENU_SCR_INFO_FILE_NAME)) return 0;

	util_get_file_extension((char *) name,extension);

	char *f;

	//Si filtro[0]=="nofiles" no muestra ningun archivo
	if (!strcasecmp(filtros[0],"nofiles")) return 0;


	//Bucle por cada filtro 
	for (i=0;filtros[i];i++) {
		//si filtro es "", significa todo (*)
		//supuestamente si hay filtro "" no habrian mas filtros pasados en el array...

		f=filtros[i];
		//printf ("f: %d\n",f);
		if (f[0]==0) return 1;

		//si filtro no es *, ocultamos los que empiezan por "."
		//Aparentemente esto no tiene mucho sentido, con esto ocultariamos archivo de nombre tipo ".xxxx.tap" por ejemplo
		//Pero bueno, para volumenes que vienen de mac os x, los metadatos se guardan en archivos tipo:
		//._0186.tap
		if (name[0]=='.') return 0;


		//comparamos extension
		if (!strcasecmp(extension,f)) return 1;
	}

	//Si es zip, tambien lo soportamos
	if (!strcasecmp(extension,"zip")) return 1;

	//Si es gz, tambien lo soportamos
	if (!strcasecmp(extension,"gz")) return 1;

	//Si es tar, tambien lo soportamos
	if (!strcasecmp(extension,"tar")) return 1;

	//Si es rar, tambien lo soportamos
	if (!strcasecmp(extension,"rar")) return 1;

	//Si es mdv, tambien lo soportamos
	if (!strcasecmp(extension,"mdv")) return 1;

	//Si es hdf, tambien lo soportamos
	if (!strcasecmp(extension,"hdf")) return 1;

	//Si es dsk, tambien lo soportamos
	if (!strcasecmp(extension,"dsk")) return 1;

	//Si es tap, tambien lo soportamos
	if (!strcasecmp(extension,"tap")) return 1;	

	//Si es tzx, tambien lo soportamos
	if (!strcasecmp(extension,"tzx")) return 1;	

	//Si es trd, tambien lo soportamos
	if (!strcasecmp(extension,"trd")) return 1;		

	//Si es scl, tambien lo soportamos
	if (!strcasecmp(extension,"scl")) return 1;			

	//Si es epr, eprom o flash, tambien lo soportamos
	if (!strcasecmp(extension,"epr")) return 1;
	if (!strcasecmp(extension,"eprom")) return 1;
	if (!strcasecmp(extension,"flash")) return 1;		

	//NOTA: Aqui agregamos todas las extensiones que en principio pueden generar muchos diferentes tipos de archivos,
	//ya sea porque son archivos comprimidos (p.ej. zip) o porque son archivos que se pueden expandir (p.j. tap)
	//Hay algunos que se pueden expandir y directamente los excluyo (como .P o .O) por ser su uso muy limitado 
	//(solo generan .baszx80 y .baszx81 en este caso)

	return 0;

}

int menu_filesel_filter_func(const struct dirent *d)
{


	int tipo_archivo=get_file_type((char *)d->d_name);


	//si es directorio, ver si empieza con . y segun el filtro activo
	//Y si setting no mostrar directorios, no mostrar
	if (tipo_archivo == 2) {
		if (menu_filesel_hide_dirs.v) return 0;
		if (menu_file_filter_dir(d->d_name,filesel_filtros)==1) return 1;
		return 0;
	}

	//Si no es archivo ni link, no ok

	if (tipo_archivo  == 0) {


		debug_printf (VERBOSE_DEBUG,"Item is not a directory, file or link");

		return 0;
	}

	//es un archivo. ver el nombre

	if (menu_file_filter(d->d_name,filesel_filtros)==1) return 1;


	return 0;
}

int menu_filesel_alphasort(const struct dirent **d1, const struct dirent **d2)
{

	//printf ("menu_filesel_alphasort %s %s\n",(*d1)->d_name,(*d2)->d_name );

	//compara nombre
	return (strcasecmp((*d1)->d_name,(*d2)->d_name));
}

int menu_filesel_readdir(void)
{

/*
       lowing macro constants for the value returned in d_type:

       DT_BLK      This is a block device.

       DT_CHR      This is a character device.

       DT_DIR      This is a directory.

       DT_FIFO     This is a named pipe (FIFO).

       DT_LNK      This is a symbolic link.

       DT_REG      This is a regular file.

       DT_SOCK     This is a UNIX domain socket.

       DT_UNKNOWN  The file type is unknown.

*/

debug_printf(VERBOSE_DEBUG,"Reading directory");

filesel_total_items=0;
primer_filesel_item=NULL;


    struct dirent **namelist;

	struct dirent *nombreactual;

    int n;
//printf ("usando scandir\n");

	filesel_item *item = NULL;
	filesel_item *itemanterior = NULL;


#ifndef MINGW
	n = scandir(".", &namelist, menu_filesel_filter_func, menu_filesel_alphasort);
#else
	//alternativa scandir, creada por mi
	n = scandir_mingw(".", &namelist, menu_filesel_filter_func, menu_filesel_alphasort);
#endif

    if (n < 0) {
		debug_printf (VERBOSE_ERR,"Error reading directory contents: %s",strerror(errno));
		return 1;
	}

    else {
        int i;

	//printf("total elementos directorio: %d\n",n);

        for (i=0;i<n;i++) {
		nombreactual=namelist[i];
            //printf("%s\n", nombreactual->d_name);
            //printf("%d\n", nombreactual->d_type);


		item=malloc(sizeof(filesel_item));
		if (item==NULL) cpu_panic("Error allocating file item");

		strcpy(item->d_name,nombreactual->d_name);


		item->next=NULL;

		//primer item
		if (primer_filesel_item==NULL) {
			primer_filesel_item=item;
		}

		//siguientes items
		else {
			itemanterior->next=item;
		}

		itemanterior=item;
		free(namelist[i]);


		filesel_total_items++;
        }

		free(namelist);

    }

	return 0;
	//free(namelist);

}


//Retorna 1 si ok
//Retorna 0 si no ok
int menu_avisa_si_extension_no_habitual(char *filtros[],char *archivo)
{

	int i;

	//Si es filtro "autosnap" es en teoria zsf
	if (!strcmp(filtros[0],"autosnap")) {
		if (!util_compare_file_extension(archivo,"zsf")) return 1;
	}


	for (i=0;filtros[i];i++) {
		if (!util_compare_file_extension(archivo,filtros[i])) return 1;

		//si filtro es "", significa todo (*)
		if (!strcmp(filtros[i],"")) return 1;

	}



	//no es extension habitual. Avisar
	return menu_confirm_yesno_texto("Unusual file extension","Do you want to use this file?");
}


void menu_warn_message(char *texto)
{
	menu_generic_message_warn("Warning",texto);

}

void menu_error_message(char *texto)
{
	menu_generic_message_warn("ERROR",texto);

}

//Similar a snprintf
void menu_generic_message_aux_copia(char *origen,char *destino, int longitud)
{
	while (longitud) {
		*destino=*origen;
		origen++;
		destino++;
		longitud--;
	}
}

//Aplicar filtros para caracteres extranyos y cortar linea en saltos de linea
int menu_generic_message_aux_filter(char *texto,int inicio, int final)
{
	//int copia_inicio=inicio;

	unsigned char caracter;

	int prefijo_utf=0;

        while (inicio!=final) {
		caracter=texto[inicio];

                if (caracter=='\n' || caracter=='\r') {
			//printf ("detectado salto de linea en posicion %d\n",inicio);
			texto[inicio]=' ';
			return inicio+1;
		}

		//TAB. Lo cambiamos por espacio
		else if (caracter==9) {
			texto[inicio]=' ';
		}

		else if (menu_es_prefijo_utf(caracter)) {
			//Si era prefijo utf, saltar
			prefijo_utf=1;
		}

		//Y si venia de prefijo utf, saltar ese caracter
		else if (prefijo_utf) {
			prefijo_utf=0;
		}

		//Caracter 255 significa "transparente"
		else if ( !(si_valid_char(caracter)) && caracter!=255 ) {
			//printf ("detectado caracter extranyo %d en posicion %d\n",caracter,inicio);

			texto[inicio]='?';
		}

                inicio++;
        }

        return final;

}


//Cortar las lineas, si se puede, por espacio entre palabras
int menu_generic_message_aux_wordwrap(char *texto,int inicio, int final)
{

	int copia_final=final;

	//ya acaba en espacio, volver
	//if (texto[final]==' ') return final;

	while (final!=inicio) {
		if (texto[final]==' ' || texto[final]=='\n' || texto[final]=='\r') return final+1;
		final--;
	}

	return copia_final;
}

int menu_generic_message_cursor_arriba(int primera_linea)
{
	if (primera_linea>0) primera_linea--;
	return primera_linea;
}

int menu_generic_message_cursor_arriba_mostrar_cursor(int primera_linea,int mostrar_cursor,int *linea_cursor)
{
                                     if (mostrar_cursor) {
                                                        int off=0;
                                                        //no limitar primera linea if (primera_linea) off++;
                                                        if (*linea_cursor>off) (*linea_cursor)--;
                                                        else primera_linea=menu_generic_message_cursor_arriba(primera_linea);
                                                }
                                                else {
                                                        primera_linea=menu_generic_message_cursor_arriba(primera_linea);
                                                }

	return primera_linea;
}



int menu_generic_message_cursor_abajo (int primera_linea,int alto_ventana,int indice_linea)
{


	//if (primera_linea<indice_linea-2) primera_linea++;
	if (primera_linea+alto_ventana-2<indice_linea) primera_linea++;
	return primera_linea;


}


int menu_generic_message_cursor_abajo_mostrar_cursor(int primera_linea,int alto_ventana,int indice_linea,int mostrar_cursor,int *linea_cursor)
{
                                                if (mostrar_cursor) {
                                                        if (*linea_cursor<alto_ventana-3) (*linea_cursor)++;
                                                        else primera_linea=menu_generic_message_cursor_abajo(primera_linea,alto_ventana,indice_linea);
                                                }
                                                else {
                                                        primera_linea=menu_generic_message_cursor_abajo(primera_linea,alto_ventana,indice_linea);
                                                }

	return primera_linea;
}


//int menu_generic_message_final_abajo(int primera_linea,int alto_ventana,int indice_linea,int mostrar_cursor,int linea_cursor)
int menu_generic_message_final_abajo(int primera_linea,int alto_ventana,int indice_linea)
{
	/*if (mostrar_cursor) {
		if (linea_cursor<alto_ventana-3) return 1;
	}

	else*/ if (primera_linea+alto_ventana-2<indice_linea) return 1;

	return 0;
}


//dibuja ventana simple, una sola linea de texto interior, sin esperar tecla
void menu_simple_ventana(char *titulo,char *texto)
{


	unsigned int ancho_ventana=strlen(titulo);
	if (strlen(texto)>ancho_ventana) ancho_ventana=strlen(texto);

	int alto_ventana=3;

	ancho_ventana +=2;

	//(unsigned int) para evitar el warning al compilar de: comparison of integers of different signs: 'unsigned int' and 'int' [-Wsign-compare]
	if (ancho_ventana>(unsigned int)ZXVISION_MAX_ANCHO_VENTANA) {
		cpu_panic("window width too big");
	}

        int xventana=menu_center_x()-ancho_ventana/2;
        int yventana=menu_center_y()-alto_ventana/2;


        menu_dibuja_ventana(xventana,yventana,ancho_ventana,alto_ventana,titulo);

	menu_escribe_linea_opcion(0,-1,1,texto);

}

void menu_copy_clipboard(char *texto)
{

	//Si puntero no NULL, liberamos clipboard anterior
	if (menu_clipboard_pointer!=NULL) {
		debug_printf(VERBOSE_INFO,"Freeing previous clipboard memory");
		free(menu_clipboard_pointer);
		menu_clipboard_pointer=NULL;
	}

	//Si puntero NULL, asignamos memoria
	if (menu_clipboard_pointer==NULL) {
		menu_clipboard_size=strlen(texto);
		debug_printf(VERBOSE_INFO,"Allocating %d bytes to clipboard",menu_clipboard_size+1);
		menu_clipboard_pointer=malloc(menu_clipboard_size+1); //+1 del 0 final
		if (menu_clipboard_pointer==NULL) {
			debug_printf(VERBOSE_ERR,"Error allocating clipboard memory");
			return;
		}
		strcpy((char *)menu_clipboard_pointer,texto);
	}

	
}

void menu_paste_clipboard_to_file(char *destination_file)
{
	//util_file_save(destination_file,menu_clipboard_pointer,menu_clipboard_size);
	//extern void util_file_save(char *filename,z80_byte *puntero, long int tamanyo);
	//extern void util_save_file(z80_byte *origin, long int tamanyo_origen, char *destination_file);

	util_save_file(menu_clipboard_pointer,menu_clipboard_size,destination_file);
}


//Cortar linea en dos, pero teniendo en cuenta que solo puede cortar por los espacios
void menu_util_cut_line_at_spaces(int posicion_corte, char *texto,char *linea1, char *linea2)
{

	int indice_texto=0;
	int ultimo_indice_texto=0;
	int longitud=strlen(texto);

	indice_texto+=posicion_corte;


		//Si longitud es menor
		if (indice_texto>=longitud) {
			strcpy(linea1,texto);
			linea2[0]=0;
			return;
		}


		//Si no, miramos si hay que separar por espacios
		else indice_texto=menu_generic_message_aux_wordwrap(texto,ultimo_indice_texto,indice_texto);

		//Separamos por salto de linea, filtramos caracteres extranyos
		//indice_texto=menu_generic_message_aux_filter(texto,ultimo_indice_texto,indice_texto);


		menu_generic_message_aux_copia(texto,linea1,indice_texto);
		linea1[indice_texto]=0;

		//copiar texto
		int longitud_texto=longitud-indice_texto;

		//printf ("indice texto: %d longitud: %d\n",indice_texto,longitud_texto);

		menu_generic_message_aux_copia(&texto[indice_texto],linea2,longitud_texto);
		linea2[longitud_texto]=0;

}

//estilo_invertido:
//0: no invertir colores
//1: invertir color boton arriba
//2: invertir color boton abajo
//3: invertir color barra
void menu_ventana_draw_horizontal_perc_bar(int x,int y,int ancho,int alto,int porcentaje,int estilo_invertido)
{
		if (porcentaje<0) porcentaje=0;
		if (porcentaje>100) porcentaje=100;

		// mostrar * abajo para indicar donde estamos en porcentaje
		int xbase=x+2;

		int tinta_boton_arriba=ESTILO_GUI_TINTA_NORMAL;
		int tinta_boton_abajo=ESTILO_GUI_TINTA_NORMAL;
		int tinta_barra=ESTILO_GUI_TINTA_NORMAL;

		int papel_boton_arriba=ESTILO_GUI_PAPEL_NORMAL;
		int papel_boton_abajo=ESTILO_GUI_PAPEL_NORMAL;
		int papel_barra=ESTILO_GUI_PAPEL_NORMAL;	

		int tinta_aux;

		switch (estilo_invertido) {
			case 1:
				tinta_aux=tinta_boton_arriba;
				tinta_boton_arriba=papel_boton_arriba;
				papel_boton_arriba=tinta_aux;
			break;

			case 2:
				tinta_aux=tinta_boton_abajo;
				tinta_boton_abajo=papel_boton_abajo;
				papel_boton_abajo=tinta_aux;
			break;	

			case 3:
				tinta_aux=tinta_barra;
				tinta_barra=papel_barra;
				papel_barra=tinta_aux;
			break;

		}			


			//mostrar cursores izquierda y derecha
		putchar_menu_overlay(xbase-1,y+alto-1,'<',tinta_boton_arriba,papel_boton_arriba);
		putchar_menu_overlay(xbase+ancho-3,y+alto-1,'>',tinta_boton_abajo,papel_boton_abajo);

		//mostrar linea horizontal para indicar que es zona de porcentaje
		z80_byte caracter_barra='-';
		if (menu_hide_vertical_percentaje_bar.v) caracter_barra=' ';

		int i;
		for (i=0;i<ancho-3;i++) putchar_menu_overlay(xbase+i,y+alto-1,caracter_barra,tinta_barra,papel_barra);	

		
		int sumarancho=((ancho-4)*porcentaje)/100;

		putchar_menu_overlay(xbase+sumarancho,y+alto-1,'*',ESTILO_GUI_PAPEL_NORMAL,ESTILO_GUI_TINTA_NORMAL);
}

//estilo_invertido:
//0: no invertir colores
//1: invertir color boton arriba
//2: invertir color boton abajo
//3: invertir color barra
void menu_ventana_draw_vertical_perc_bar(int x,int y,int ancho,int alto,int porcentaje,int estilo_invertido)
{
		if (porcentaje<0) porcentaje=0;
		if (porcentaje>100) porcentaje=100;

		// mostrar * a la derecha para indicar donde estamos en porcentaje
		int ybase=y+2;

		int tinta_boton_arriba=ESTILO_GUI_TINTA_NORMAL;
		int tinta_boton_abajo=ESTILO_GUI_TINTA_NORMAL;
		int tinta_barra=ESTILO_GUI_TINTA_NORMAL;

		int papel_boton_arriba=ESTILO_GUI_PAPEL_NORMAL;
		int papel_boton_abajo=ESTILO_GUI_PAPEL_NORMAL;
		int papel_barra=ESTILO_GUI_PAPEL_NORMAL;	

		int tinta_aux;

		switch (estilo_invertido) {
			case 1:
				tinta_aux=tinta_boton_arriba;
				tinta_boton_arriba=papel_boton_arriba;
				papel_boton_arriba=tinta_aux;
			break;

			case 2:
				tinta_aux=tinta_boton_abajo;
				tinta_boton_abajo=papel_boton_abajo;
				papel_boton_abajo=tinta_aux;
			break;	

			case 3:
				tinta_aux=tinta_barra;
				tinta_barra=papel_barra;
				papel_barra=tinta_aux;
			break;

		}	


		//mostrar cursores arriba y abajo
		putchar_menu_overlay(x+ancho-1,ybase-1,'^',tinta_boton_arriba,papel_boton_arriba);
		putchar_menu_overlay(x+ancho-1,ybase+alto-3,'v',tinta_boton_abajo,papel_boton_abajo);

		//mostrar linea vertical para indicar que es zona de porcentaje
		z80_byte caracter_barra='|';
		if (menu_hide_vertical_percentaje_bar.v) caracter_barra=' ';		

		//mostrar linea vertical para indicar que es zona de porcentaje
		int i;
		for (i=0;i<alto-3;i++) 	putchar_menu_overlay(x+ancho-1,ybase+i,caracter_barra,tinta_barra,papel_barra);	
		
		
		int sumaralto=((alto-4)*porcentaje)/100;
		putchar_menu_overlay(x+ancho-1,ybase+sumaralto,'*',ESTILO_GUI_PAPEL_NORMAL,ESTILO_GUI_TINTA_NORMAL);
}



int splash_zesarux_logo_paso=0;
int splash_zesarux_logo_active=0;

void reset_splash_zesarux_logo(void)
{
	splash_zesarux_logo_active=0;
}



//Esta rutina estaba originalmente en screen.c pero dado que se ha modificado para usar rutinas auxiliares de aqui, mejor que este aqui
void screen_print_splash_text(int y,int tinta,int papel,char *texto)
{

        //Si no hay driver video
        if (scr_putpixel==NULL || scr_putpixel_zoom==NULL) return;


  if (menu_abierto==0 && screen_show_splash_texts.v==1) {
                cls_menu_overlay();

                int x;

#define MAX_LINEAS_SPLASH 24
        const int max_ancho_texto=31;
	//al trocear, si hay un espacio despues, se agrega, y por tanto puede haber linea de 31+1=32 caracteres

        //texto que contiene cada linea con ajuste de palabra. Al trocear las lineas aumentan
	//33 es ancho total linea(32)+1
        char buffer_lineas[MAX_LINEAS_SPLASH][33];



        int indice_linea=0;
        int indice_texto=0;
        int ultimo_indice_texto=0;
        int longitud=strlen(texto);

        //int indice_segunda_linea;


        do {
                indice_texto+=max_ancho_texto;

                //Controlar final de texto
                if (indice_texto>=longitud) indice_texto=longitud;

                //Si no, miramos si hay que separar por espacios
                else indice_texto=menu_generic_message_aux_wordwrap(texto,ultimo_indice_texto,indice_texto);

                //Separamos por salto de linea, filtramos caracteres extranyos
                indice_texto=menu_generic_message_aux_filter(texto,ultimo_indice_texto,indice_texto);

                //copiar texto
                int longitud_texto=indice_texto-ultimo_indice_texto;



                menu_generic_message_aux_copia(&texto[ultimo_indice_texto],buffer_lineas[indice_linea],longitud_texto);
                buffer_lineas[indice_linea++][longitud_texto]=0;
                //printf ("copiado %d caracteres desde %d hasta %d: %s\n",longitud_texto,ultimo_indice_texto,indice_texto,buffer_lineas[indice_linea-1]);


        //printf ("texto indice: %d : longitud: %d: -%s-\n",indice_linea-1,longitud_texto,buffer_lineas[indice_linea-1]);
		//printf ("indice_linea: %d indice_linea+y: %d MAX: %d\n",indice_linea,indice_linea+y,MAX_LINEAS_SPLASH);

                if (indice_linea==MAX_LINEAS_SPLASH) {
                        //cpu_panic("Max lines on menu_generic_message reached");
                        debug_printf(VERBOSE_INFO,"Max lines on screen_print_splash_text reached (%d)",MAX_LINEAS_SPLASH);
                        //finalizamos bucle
                        indice_texto=longitud;
                }

                ultimo_indice_texto=indice_texto;
                //printf ("ultimo indice: %d %c\n",ultimo_indice_texto,texto[ultimo_indice_texto]);

        } while (indice_texto<longitud);

	int i;
	for (i=0;i<indice_linea && y<24;i++) {
		debug_printf (VERBOSE_DEBUG,"line %d y: %d length: %d contents: -%s-",i,y,strlen(buffer_lineas[i]),buffer_lineas[i]);
		x=menu_center_x()-strlen(buffer_lineas[i])/2;
		if (x<0) x=0;
		menu_escribe_texto(x,y,tinta,papel,buffer_lineas[i]);
		y++;
	}


        set_menu_overlay_function(normal_overlay_texto_menu);
        menu_splash_text_active.v=1;
        menu_splash_segundos=5;

				//no queremos que reaparezca el logo, por si no había llegado al final de splash. Improbable? Si. Pero mejor ser precavidos
				reset_splash_zesarux_logo();
   }

}



//Esta rutina estaba originalmente en screen.c pero dado que se ha modificado para usar rutinas auxiliares de aqui, mejor que este aqui
void screen_print_splash_text_center(int tinta,int papel,char *texto)
{
	screen_print_splash_text(menu_center_y(),tinta,papel,texto);
}

//retorna 1 si y
//otra cosa, 0
int menu_confirm_yesno_texto(char *texto_ventana,char *texto_interior)
{

	//Si se fuerza siempre yes
	if (force_confirm_yes.v) return 1;


	//printf ("confirm\n");
        cls_menu_overlay();

        menu_espera_no_tecla();


	menu_item *array_menu_confirm_yes_no;
        menu_item item_seleccionado;
        int retorno_menu;

	//Siempre indicamos el NO
	int confirm_yes_no_opcion_seleccionada=2;
        do {

		menu_add_item_menu_inicial_format(&array_menu_confirm_yes_no,MENU_OPCION_SEPARADOR,NULL,NULL,texto_interior);

                menu_add_item_menu_format(array_menu_confirm_yes_no,MENU_OPCION_NORMAL,NULL,NULL,"~~Yes");
		menu_add_item_menu_shortcut(array_menu_confirm_yes_no,'y');

                menu_add_item_menu_format(array_menu_confirm_yes_no,MENU_OPCION_NORMAL,NULL,NULL,"~~No");
		menu_add_item_menu_shortcut(array_menu_confirm_yes_no,'n');

                //separador adicional para que quede mas grande la ventana y mas mono
                menu_add_item_menu_format(array_menu_confirm_yes_no,MENU_OPCION_SEPARADOR,NULL,NULL," ");



                retorno_menu=menu_dibuja_menu(&confirm_yes_no_opcion_seleccionada,&item_seleccionado,array_menu_confirm_yes_no,texto_ventana);

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
			if (confirm_yes_no_opcion_seleccionada==1) return 1;
			else return 0;
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

	return 0;


}


//retorna 1 si opcion 1
//retorna 2 si opcion 2
//retorna 0 si ESC
int menu_simple_two_choices(char *texto_ventana,char *texto_interior,char *opcion1,char *opcion2)
{

        cls_menu_overlay();

        menu_espera_no_tecla();


	menu_item *array_menu_simple_two_choices;
        menu_item item_seleccionado;
        int retorno_menu;

	//Siempre indicamos la primera opcion
	int simple_two_choices_opcion_seleccionada=1;
        do {

		menu_add_item_menu_inicial_format(&array_menu_simple_two_choices,MENU_OPCION_SEPARADOR,NULL,NULL,texto_interior);

                menu_add_item_menu_format(array_menu_simple_two_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion1);

                menu_add_item_menu_format(array_menu_simple_two_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion2);

                //separador adicional para que quede mas grande la ventana y mas mono
                menu_add_item_menu_format(array_menu_simple_two_choices,MENU_OPCION_SEPARADOR,NULL,NULL," ");



                retorno_menu=menu_dibuja_menu(&simple_two_choices_opcion_seleccionada,&item_seleccionado,array_menu_simple_two_choices,texto_ventana);

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        return simple_two_choices_opcion_seleccionada;
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

	return 0;


}


//retorna 1 si opcion 1
//retorna 2 si opcion 2
//retorna 3 si opcion 3
//retorna 0 si ESC
int menu_simple_three_choices(char *texto_ventana,char *texto_interior,char *opcion1,char *opcion2,char *opcion3)
{

        cls_menu_overlay();

        menu_espera_no_tecla();


	menu_item *array_menu_simple_three_choices;
        menu_item item_seleccionado;
        int retorno_menu;

	//Siempre indicamos la primera opcion
	int simple_three_choices_opcion_seleccionada=1;
        do {

		menu_add_item_menu_inicial_format(&array_menu_simple_three_choices,MENU_OPCION_SEPARADOR,NULL,NULL,texto_interior);

                menu_add_item_menu_format(array_menu_simple_three_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion1);

                menu_add_item_menu_format(array_menu_simple_three_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion2);

				menu_add_item_menu_format(array_menu_simple_three_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion3);

                //separador adicional para que quede mas grande la ventana y mas mono
                menu_add_item_menu_format(array_menu_simple_three_choices,MENU_OPCION_SEPARADOR,NULL,NULL," ");



                retorno_menu=menu_dibuja_menu(&simple_three_choices_opcion_seleccionada,&item_seleccionado,array_menu_simple_three_choices,texto_ventana);

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        return simple_three_choices_opcion_seleccionada;
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

	return 0;


}


//retorna 1 si opcion 1
//retorna 2 si opcion 2
//retorna 3 si opcion 3
//retorna 4 si opcion 4
//retorna 0 si ESC
int menu_simple_four_choices(char *texto_ventana,char *texto_interior,char *opcion1,char *opcion2,char *opcion3,char *opcion4)
{

        cls_menu_overlay();

        menu_espera_no_tecla();


	menu_item *array_menu_simple_four_choices;
        menu_item item_seleccionado;
        int retorno_menu;

	//Siempre indicamos la primera opcion
	int simple_four_choices_opcion_seleccionada=1;
        do {

		menu_add_item_menu_inicial_format(&array_menu_simple_four_choices,MENU_OPCION_SEPARADOR,NULL,NULL,texto_interior);

                menu_add_item_menu_format(array_menu_simple_four_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion1);

                menu_add_item_menu_format(array_menu_simple_four_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion2);

				menu_add_item_menu_format(array_menu_simple_four_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion3);

                menu_add_item_menu_format(array_menu_simple_four_choices,MENU_OPCION_NORMAL,NULL,NULL,opcion4);

                //separador adicional para que quede mas grande la ventana y mas mono
                menu_add_item_menu_format(array_menu_simple_four_choices,MENU_OPCION_SEPARADOR,NULL,NULL," ");



                retorno_menu=menu_dibuja_menu(&simple_four_choices_opcion_seleccionada,&item_seleccionado,array_menu_simple_four_choices,texto_ventana);

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        return simple_four_choices_opcion_seleccionada;
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

	return 0;


}



//Retorna 0=Cancel, 1=Append, 2=Truncate, 3=Rotate
int menu_ask_no_append_truncate_texto(char *texto_ventana,char *texto_interior)
{

	

        cls_menu_overlay();

        menu_espera_no_tecla();


	menu_item *array_menu_ask_no_append_truncate;
        menu_item item_seleccionado;
        int retorno_menu;

	//Siempre indicamos el Cancel
	int ask_no_append_truncate_opcion_seleccionada=1;
	do {

		menu_add_item_menu_inicial_format(&array_menu_ask_no_append_truncate,MENU_OPCION_SEPARADOR,NULL,NULL,texto_interior);

		menu_add_item_menu_format(array_menu_ask_no_append_truncate,MENU_OPCION_NORMAL,NULL,NULL,"~~Cancel");
		menu_add_item_menu_shortcut(array_menu_ask_no_append_truncate,'c');
		menu_add_item_menu_tooltip(array_menu_ask_no_append_truncate,"Cancel operation and don't set file");
		menu_add_item_menu_ayuda(array_menu_ask_no_append_truncate,"Cancel operation and don't set file");		

		menu_add_item_menu_format(array_menu_ask_no_append_truncate,MENU_OPCION_NORMAL,NULL,NULL,"~~Append");
		menu_add_item_menu_shortcut(array_menu_ask_no_append_truncate,'a');
		menu_add_item_menu_tooltip(array_menu_ask_no_append_truncate,"Open the selected file in append mode");
		menu_add_item_menu_ayuda(array_menu_ask_no_append_truncate,"Open the selected file in append mode");			

		menu_add_item_menu_format(array_menu_ask_no_append_truncate,MENU_OPCION_NORMAL,NULL,NULL,"~~Truncate");
		menu_add_item_menu_shortcut(array_menu_ask_no_append_truncate,'t');
		menu_add_item_menu_tooltip(array_menu_ask_no_append_truncate,"Truncates selected file to 0 size");
		menu_add_item_menu_ayuda(array_menu_ask_no_append_truncate,"Truncates selected file to 0 size");			

		menu_add_item_menu_format(array_menu_ask_no_append_truncate,MENU_OPCION_NORMAL,NULL,NULL,"~~Rotate");
		menu_add_item_menu_shortcut(array_menu_ask_no_append_truncate,'r');	
		menu_add_item_menu_tooltip(array_menu_ask_no_append_truncate,"Rotate selected file to keep history files");
		menu_add_item_menu_ayuda(array_menu_ask_no_append_truncate,"Rename selected file adding extension suffix .1. \n"
			"If that file also exists, the extension suffix is renamed to .2. \n"
			"If that file also exists, the extension suffix is renamed to .3, and so on... \n"
			"You can set the maximum file rotations, by default 10."
		
			);							

		//separador adicional para que quede mas grande la ventana y mas mono
		menu_add_item_menu_format(array_menu_ask_no_append_truncate,MENU_OPCION_SEPARADOR,NULL,NULL," ");



		retorno_menu=menu_dibuja_menu(&ask_no_append_truncate_opcion_seleccionada,&item_seleccionado,array_menu_ask_no_append_truncate,texto_ventana);

		

		if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
				//llamamos por valor de funcion
	//if (ask_no_append_truncate_opcion_seleccionada==1) return 1;
	//else return 0;
				return ask_no_append_truncate_opcion_seleccionada-1; //0=Cancel, 1=Append, 2=Truncate, 3=Rotate
		}

	} while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

	return 0;


}


//Funcion para preguntar opcion de una lista, usando interfaz de menus
//Entradas de lista finalizadas con NULL
//Retorna 0=Primera opcion, 1=Segunda opcion, etc
//Retorna <0 si salir con ESC
int menu_ask_list_texto(char *texto_ventana,char *texto_interior,char *entradas_lista[])
{



        cls_menu_overlay();

        menu_espera_no_tecla();


        menu_item *array_menu_ask_list;
        menu_item item_seleccionado;
        int retorno_menu;

        //Siempre indicamos primera opcion
        int ask_list_texto_opcion_seleccionada=1;
        do {

                menu_add_item_menu_inicial_format(&array_menu_ask_list,MENU_OPCION_SEPARADOR,NULL,NULL,texto_interior);

                int i=0;

                while(entradas_lista[i]!=NULL) {
                        menu_add_item_menu_format(array_menu_ask_list,MENU_OPCION_NORMAL,NULL,NULL,entradas_lista[i]);
			i++;
                }

                //separador adicional para que quede mas grande la ventana y mas mono
                menu_add_item_menu_format(array_menu_ask_list,MENU_OPCION_SEPARADOR,NULL,NULL," ");



                retorno_menu=menu_dibuja_menu(&ask_list_texto_opcion_seleccionada,&item_seleccionado,array_menu_ask_list,texto_ventana);

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        return ask_list_texto_opcion_seleccionada-1;
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);

        return -1;

}


void menu_generic_message_format(char *titulo, const char * texto_format , ...)
{

        //Buffer de entrada
        char texto[MAX_TEXTO_GENERIC_MESSAGE];
        va_list args;
        va_start (args, texto_format);
        vsprintf (texto,texto_format, args);
        va_end (args);


	//menu_generic_message_tooltip(titulo, 0, 0, 0, NULL, "%s", texto);
	zxvision_generic_message_tooltip(titulo , 0 , 0, 0, 0, NULL, 1, "%s", texto);


	//En Linux esto funciona bien sin tener que hacer las funciones va_ previas:
	//menu_generic_message_tooltip(titulo, 0, texto_format)
	//Pero en Mac OS X no obtiene los valores de los parametros adicionales
}

void menu_generic_message(char *titulo, const char * texto)
{

        //menu_generic_message_tooltip(titulo, 0, 0, 0, NULL, "%s", texto);
		zxvision_generic_message_tooltip(titulo , 0 , 0, 0, 0, NULL, 1, "%s", texto);
}

//Mensaje con setting para marcar
void zxvision_menu_generic_message_setting(char *titulo, const char *texto, char *texto_opcion, int *valor_opcion)
{

	int lineas_agregar=4;

	//Asumimos opcion ya marcada
	*valor_opcion=1;
	
	zxvision_generic_message_tooltip(titulo , lineas_agregar , 0, 0, 0, NULL, 1, "%s", texto);

	zxvision_window *ventana;

	//Nuestra ventana sera la actual
	ventana=zxvision_current_window;

	int posicion_y_opcion=ventana->visible_height-lineas_agregar-1;
	//printf ("%d %d\n",posicion_y_opcion,ventana->visible_height);

	int ancho_ventana=ventana->visible_width;
	int posicion_centro_x=ancho_ventana/2-1; //un poco mas a la izquierda

	if (posicion_centro_x<0) posicion_centro_x=0;


		menu_item *array_menu_generic_message_setting;
        menu_item item_seleccionado;
		int array_menu_generic_message_setting_opcion_seleccionada=1;
        int retorno_menu;

	int salir=0;
    do {


		char buffer_texto_opcion[64];
		char buffer_texto_ok[64];

		sprintf (buffer_texto_opcion,"[%c] %s",(*valor_opcion ? 'X' : ' ' ),texto_opcion);
		strcpy(buffer_texto_ok,"<OK>");

		//Tengo antes los textos para sacar longitud y centrarlos

		int posicion_x_opcion=posicion_centro_x-strlen(buffer_texto_opcion)/2;
		int posicion_x_ok=posicion_centro_x-strlen(buffer_texto_ok)/2;


		menu_add_item_menu_inicial_format(&array_menu_generic_message_setting,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto_opcion);
		menu_add_item_menu_tabulado(array_menu_generic_message_setting,posicion_x_opcion,posicion_y_opcion);


		menu_add_item_menu_format(array_menu_generic_message_setting,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto_ok);
		menu_add_item_menu_tabulado(array_menu_generic_message_setting,posicion_x_ok,posicion_y_opcion+2);


		//Nombre de ventana solo aparece en el caso de stdout
    	retorno_menu=menu_dibuja_menu(&array_menu_generic_message_setting_opcion_seleccionada,&item_seleccionado,array_menu_generic_message_setting,titulo);


        if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
	
				//Si opcion 1, conmutar valor		
				//Conmutar valor
				if (array_menu_generic_message_setting_opcion_seleccionada==0) *valor_opcion ^=1;

				//Si opcion 2, volver
				if (array_menu_generic_message_setting_opcion_seleccionada==1) {
					salir=1;
				}
                
        }

    } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus && !salir);


	cls_menu_overlay();
	zxvision_destroy_window(ventana);

	//Y liberar esa memoria, dado que la ventana esta asignada en memoria global
	free(ventana);
}


void menu_generic_message_splash(char *titulo, const char * texto)
{

        //menu_generic_message_tooltip(titulo, 1, 0, 0, NULL, "%s", texto);
		zxvision_generic_message_tooltip(titulo , 0 , 1, 0, 0, NULL, 0, "%s", texto);
		menu_espera_no_tecla();
}

void menu_generic_message_warn(char *titulo, const char * texto)
{

        //menu_generic_message_tooltip(titulo, 1, 0, 0, NULL, "%s", texto);
		zxvision_generic_message_tooltip(titulo , 0 , 2, 0, 0, NULL, 0, "%s", texto);
}

int menu_confirm_yesno(char *texto_ventana)
{
	return menu_confirm_yesno_texto(texto_ventana,"Sure?");
}


//max_length contando caracter 0 del final, es decir, para un texto de 4 caracteres, debemos especificar max_length=5
void menu_ventana_scanf(char *titulo,char *texto,int max_length)
{

	//int scanf_x=1;
	//int scanf_y=10;
	int scanf_ancho=30;
	int scanf_alto=3;	
	int scanf_x=menu_center_x()-scanf_ancho/2;
	int scanf_y=menu_center_y()-scanf_alto/2;


        menu_espera_no_tecla();

	zxvision_window ventana;

	zxvision_new_window(&ventana,scanf_x,scanf_y,scanf_ancho,scanf_alto,
							scanf_ancho-1,scanf_alto-2,titulo);

	//No queremos que se pueda redimensionar
	ventana.can_be_resized=0;

	zxvision_draw_window(&ventana);


	zxvision_scanf(&ventana,texto,max_length,scanf_ancho-2,1,0,0);

	//menu_scanf(texto,max_length,scanf_ancho-2,scanf_x+1,scanf_y+1);
	//int menu_scanf(char *string,unsigned int max_length,int max_length_shown,int x,int y)

	//Al salir
	//menu_refresca_pantalla();
	menu_cls_refresh_emulated_screen();

	zxvision_destroy_window(&ventana);

}

void menu_ventana_scanf_number_aux(zxvision_window *ventana,char *texto,int max_length,int x_texto_input)
{
	//En entrada de texto no validamos el maximo y minimo. Eso lo tiene que seguir haciendo la funcion que llama a menu_ventana_scanf_numero
	//Si que se controla al pulsar botones de + y -
	zxvision_scanf(ventana,texto,max_length,max_length,x_texto_input,0,1);
}

void menu_ventana_scanf_number_print_buttons(zxvision_window *ventana,char *texto,int x_boton_menos,int x_boton_mas,int x_texto_input,int x_boton_ok,int x_boton_cancel)
{
			//Borrar linea entera
		zxvision_print_string_defaults_fillspc(ventana,x_boton_menos,0,"");

		//Escribir - +
		zxvision_print_string_defaults(ventana,x_boton_menos,0,"-");
		zxvision_print_string_defaults(ventana,x_boton_mas,0,"+");

		//Escribir numero
		zxvision_print_string_defaults(ventana,x_texto_input,0,texto);

		zxvision_print_string_defaults(ventana,x_boton_ok,2,"<OK>");	

		zxvision_print_string_defaults(ventana,x_boton_cancel,2,"<Cancel>");	
}

//busca donde apunta el mouse y retorna opcion seleccionada
int menu_ventana_scanf_number_ajust_cursor_mouse(menu_item *m,int posicion_raton_x,int posicion_raton_y)
{

	//printf ("buscando en %d,%d\n",posicion_raton_x,posicion_raton_y);

	menu_item *buscar_tabulado;
	int linea_buscada;

	buscar_tabulado=menu_retorna_item_tabulado_xy(m,posicion_raton_x,posicion_raton_y,&linea_buscada);

	int linea_seleccionada=-1;

	if (buscar_tabulado!=NULL) {
		//Buscar por coincidencia de coordenada x,y
		if (buscar_tabulado->tipo_opcion!=MENU_OPCION_SEPARADOR) {
			linea_seleccionada=linea_buscada;
			//printf("encontrada opcion en %d\n",linea_buscada);
			//redibuja_ventana=1;
			//menu_tooltip_counter=0;
		}
	}
	else {
		//printf ("item no encontrado\n");
	}

	return linea_seleccionada;
}

/*
max_length: maxima longitud, contando caracter 0 del final
minimo: valor minimo admitido
maximo: valor maximo admitido
circular: si al pasar umbral, se resetea al otro umbral

En entrada de texto no validamos el maximo y minimo. Eso lo tiene que seguir haciendo la funcion que llama a menu_ventana_scanf_numero
Si que se controla al pulsar botones de + y -
	
*/

//Retorna -1 si pulsado ESC
int menu_ventana_scanf_numero(char *titulo,char *texto,int max_length,int incremento,int minimo,int maximo,int circular)
{

    //En caso de stdout, es mas simple, mostrar texto y esperar texto
	//Lo gestiona la propia rutina de menu_ventana_scanf
	if (!strcmp(scr_new_driver_name,"stdout")) {
		menu_ventana_scanf(titulo,texto,max_length);
		return 0;
	}


	int ancho_ventana=32;
	int alto_ventana=5;


	int xventana=menu_center_x()-ancho_ventana/2;
	int yventana=menu_center_y()-alto_ventana/2;

	zxvision_window ventana;

	zxvision_new_window(&ventana,xventana,yventana,ancho_ventana,alto_ventana,
                                                        ancho_ventana-1,alto_ventana-2,titulo);

	//Dado que es una variable local, siempre podemos usar este nombre array_menu_common
	menu_item *array_menu_common;
	menu_item item_seleccionado;
	int retorno_menu;

	//El foco en el numero
	int comun_opcion_seleccionada=1;


	//Donde van los bloques

	//int inicio_bloque_x=8;
	//int inicio_bloque_y=2;
	//int ancho_bloque=6;

	//int linea=inicio_bloque_y;

	int max_input_visible=ancho_ventana-2-2-2; //2 laterales, 2 de los botones, y 2 de espacios entre botones
	if (max_length<max_input_visible) max_input_visible=max_length;

	int x_boton_menos=1;
	int x_texto_input=x_boton_menos+2;
	int x_boton_mas=x_texto_input+max_input_visible+1;
	int x_boton_ok=1;	
	int x_boton_cancel=x_boton_ok+5;

	//Dibujar texto interior
	menu_ventana_scanf_number_print_buttons(&ventana,texto,x_boton_menos,x_boton_mas,x_texto_input,x_boton_ok,x_boton_cancel);

	//Dibujar ventana antes de scanf
	zxvision_draw_window(&ventana);
	zxvision_draw_window_contents(&ventana);
	
	//Entramos primero editando el numero
	menu_ventana_scanf_number_aux(&ventana,texto,max_length,x_texto_input);

	//Cambiar la opcion seleccionada a la del OK, al pulsar enter
	//comun_opcion_seleccionada=3;	



	//Decir que habra que ajustar raton segun posicion mouse actual
	int debe_ajustar_cursor_segun_mouse=1;


	do {



		//Escribir primero numero

		//Dibujar texto interior
		menu_ventana_scanf_number_print_buttons(&ventana,texto,x_boton_menos,x_boton_mas,x_texto_input,x_boton_ok,x_boton_cancel);

		
		menu_add_item_menu_inicial_format(&array_menu_common,MENU_OPCION_NORMAL,NULL,NULL,"-");
		menu_add_item_menu_tabulado(array_menu_common,x_boton_menos,0);

		menu_add_item_menu_format(array_menu_common,MENU_OPCION_NORMAL,NULL,NULL,texto);
		menu_add_item_menu_tabulado(array_menu_common,x_texto_input,0);			

		menu_add_item_menu_format(array_menu_common,MENU_OPCION_NORMAL,NULL,NULL,"+");
		menu_add_item_menu_tabulado(array_menu_common,x_boton_mas,0);	
		
		menu_add_item_menu_format(array_menu_common,MENU_OPCION_NORMAL,NULL,NULL,"<OK>");
		menu_add_item_menu_tabulado(array_menu_common,x_boton_ok,2);	

		menu_add_item_menu_format(array_menu_common,MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL,"<Cancel>");
		menu_add_item_menu_tabulado(array_menu_common,x_boton_cancel,2);	


		//Antes de abrir el menu, ajustar la opcion seleccionada cuando ha salido del input de numero
		//y a que boton apunta el mouse
		if (debe_ajustar_cursor_segun_mouse) {
			debe_ajustar_cursor_segun_mouse=0;

			//Asumimos que esta en OK
			//Cambiar la opcion seleccionada a la del OK, al pulsar enter
			comun_opcion_seleccionada=3;

			int opcion_sel=menu_ventana_scanf_number_ajust_cursor_mouse(array_menu_common,menu_mouse_x,menu_mouse_y-1);		
			//printf("opcion seleccionada: %d\n",opcion_sel);
			if (opcion_sel>=0) {
				comun_opcion_seleccionada=opcion_sel;
			}
		}

		retorno_menu=menu_dibuja_menu(&comun_opcion_seleccionada,&item_seleccionado,array_menu_common,titulo);

			
			if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
				

					//botones menos y mas
					if (comun_opcion_seleccionada==0 || comun_opcion_seleccionada==2) {

						int numero=parse_string_to_number(texto);

						if (comun_opcion_seleccionada==0) {							
							numero-=incremento;
							if (numero<minimo) {
								if (circular) {
									numero=maximo;
								}
								else {
									numero=minimo;
								}
							}
						}	

						if (comun_opcion_seleccionada==2) {
							numero+=incremento;

							if (numero>maximo) {
								if (circular) {
									numero=minimo;
								}
								else {
									numero=maximo;
								}
							}							
						}	

						sprintf(texto,"%d",numero);	
					}							

					if (comun_opcion_seleccionada==1) {
						menu_ventana_scanf_number_aux(&ventana,texto,max_length,x_texto_input);
						//zxvision_scanf(&ventana,texto,max_length,max_length,x_texto_input,0,1);
						//menu_espera_no_tecla();



						//Pero ajustar el mouse si apunta a alguna opcion
						debe_ajustar_cursor_segun_mouse=1;
					}

			

			}

    } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus && comun_opcion_seleccionada<3);


    //En caso de menus tabulados, es responsabilidad de este de liberar ventana
    zxvision_destroy_window(&ventana);	

	cls_menu_overlay();

	if (comun_opcion_seleccionada==4 || retorno_menu==MENU_RETORNO_ESC) return -1; //Pulsado Cancel

	else return 0;
}

//Similar a menu_ventana_scanf_numero pero evita tener que crear el buffer de char temporal
//Y ademas muestra error si limites se exceden
void menu_ventana_scanf_numero_enhanced(char *titulo,int *variable,int max_length,int incremento,int minimo,int maximo,int circular)
{

	//Asignar memoria para el buffer
	char *buf_texto;

	buf_texto=malloc(max_length);
	if (buf_texto==NULL) cpu_panic("Can not allocate memory for input text");

	sprintf(buf_texto,"%d",*variable);

	int ret=menu_ventana_scanf_numero(titulo,buf_texto,max_length,incremento,minimo,maximo,circular);
	if (ret>=0) {

		int numero=parse_string_to_number(buf_texto);

		if (numero<minimo || numero>maximo) {
			debug_printf(VERBOSE_ERR,"Out of range. Allowed range: minimum: %d maximum: %d",minimo,maximo);
		}
		else {
			*variable=numero;
		}
	}

	//printf("liberando memoria\n");
	free(buf_texto);
	
}


void menu_inicio_pre_retorno_reset_flags(void)
{
    //desactivar botones de acceso directo
    menu_button_smartload.v=0;
    menu_button_exit_emulator.v=0;
    menu_event_drag_drop.v=0;
    menu_breakpoint_exception.v=0;
    menu_event_remote_protocol_enterstep.v=0;
    menu_button_f_function.v=0;
	menu_event_open_menu.v=0;
}

void menu_process_f_function_pause(void)
{

	int antes_multitarea;

	//Guardar valor anterior multitarea
	antes_multitarea=menu_multitarea;
	menu_multitarea=0;
	audio_playing.v=0;
	z80_byte tecla=0;

	while (tecla==0) {
		menu_espera_tecla();
		tecla=menu_get_pressed_key();
	}

	menu_espera_no_tecla();

	//restaurar
	menu_multitarea=antes_multitarea;
}

void menu_process_f_function_topspeed(void)
{
	timer_toggle_top_speed_timer();

	if (top_speed_timer.v) {
		//Parece que ni footer ni splash aparece pues el menu esta medio abierto
		//En vez de esto meteremos el texto TOPSPEED cuando se actualiza footer
		//generic_footertext_print_operating("TSPEED");
	}
	else {
		//screen_print_splash_text_center(ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,"Disabling CPU Top speed");
	}
}

void menu_process_f_functions_by_action(int accion)
{


	switch (accion)
	{
		case F_FUNCION_DEFAULT:
		break;

		case F_FUNCION_NOTHING:
		break;

		case F_FUNCION_RESET:
			reset_cpu();
		break;

		case F_FUNCION_HARDRESET:
			hard_reset_cpu();
		break;

		case F_FUNCION_NMI:
			generate_nmi_multiface_tbblue();
		break;

		case F_FUNCION_DRIVE:
			generate_nmi_drive();
		break;

		case F_FUNCION_OPENMENU:
			if (util_if_open_just_menu() ) menu_inicio_bucle();
		break;

		case F_FUNCION_LOADBINARY:
			menu_debug_load_binary(0);
		break;

		case F_FUNCION_SAVEBINARY:
			menu_debug_save_binary(0);
		break;

		case F_FUNCION_DEBUGCPU:
			menu_debug_registers(0);
		break;

		case F_FUNCION_EXITEMULATOR:
			end_emulator();
		break;

	}

}

void menu_process_f_functions(void)
{



	int indice=menu_button_f_function_index;

	enum defined_f_function_ids accion=defined_f_functions_keys_array[indice];

	//printf ("Menu process Tecla: F%d Accion: %s\n",indice+1,defined_f_functions_array[accion].texto_funcion);

	menu_process_f_functions_by_action(accion);

}


void menu_inicio_reset_emulated_keys(void)
{
	//Resetear todas teclas excepto bits de puertos especiales y esperar a no pulsar tecla
	z80_byte p_puerto_especial1,p_puerto_especial2,p_puerto_especial3,p_puerto_especial4;

	p_puerto_especial1=puerto_especial1;
	p_puerto_especial2=puerto_especial2;
	p_puerto_especial3=puerto_especial3;
	p_puerto_especial4=puerto_especial4;

	reset_keyboard_ports();

	//Restaurar estado teclas especiales, para poder esperar a liberar dichas teclas, por ejemplo
	puerto_especial1=p_puerto_especial1;
	puerto_especial2=p_puerto_especial2;
	puerto_especial3=p_puerto_especial3;
	puerto_especial4=p_puerto_especial4;


	//Desactivar fire, por si esta disparador automatico
	joystick_release_fire(1);	

	menu_espera_no_tecla();
}

//menu principal
void menu_inicio(void)
{
	//Pulsado boton salir del emulador, en drivers xwindows, sdl, etc, en casos con menu desactivado, sale del todo
	if (menu_button_exit_emulator.v && (menu_desactivado.v || menu_desactivado_andexit.v)
		) {
		end_emulator();
	}

	//Menu desactivado y volver
	if (menu_desactivado.v) {
		menu_inicio_pre_retorno_reset_flags();

    	menu_set_menu_abierto(0);		
		return;
	}

	//Menu desactivado y salida del emulador
	if (menu_desactivado_andexit.v) end_emulator();

	//No permitir aparecer osd keyboard desde aqui. Luego se reactiva en cada gestion de tecla
	osd_kb_no_mostrar_desde_menu=1;

	menu_contador_teclas_repeticion=CONTADOR_HASTA_REPETICION;

	int liberar_teclas_y_esperar=1; //Si se liberan teclas y se espera a liberar teclado


	


	if (menu_breakpoint_exception.v) {
		if (!debug_if_breakpoint_action_menu(catch_breakpoint_index)) {
			//Accion no es de abrir menu
			/*
			Tecnicamente, haciendo esto, no estamos controlando que se dispare un evento de breakpoin accion, por ejemplo , printe,
			y a la vez, se genere otro evento, por ejemplo quickload. En ese caso sucederia que al llamar a quickload, no se liberarian
			las teclas en la maquina emulada ni se esperaria a no pulsar tecla
			Para evitar este remoto caso, habria que hacer que no se liberen las teclas aqui al principio, sino que cada evento
			libere teclas por su cuenta
			*/
			liberar_teclas_y_esperar=0;
		}
	}

	if (liberar_teclas_y_esperar) {
		menu_inicio_reset_emulated_keys();
	}


	//printf ("after menu_inicio_reset_emulated_keys\n");

	//Si se ha pulsado tecla de OSD keyboard, al llamar a espera_no_tecla, se abrira osd y no conviene.
	


			//printf ("Event open menu: %d\n",menu_event_open_menu.v);

	//printf ("after menu_espera_no_tecla\n");


        if (!strcmp(scr_new_driver_name,"stdout")) {
		//desactivar menu multitarea con stdout
		menu_multitarea=0;
        }


	if (menu_multitarea==0) {
		audio_playing.v=0;
		//audio_thread_finish();
	}


	//quitar splash text por si acaso
	menu_splash_segundos=1;
	reset_welcome_message();


	cls_menu_overlay();
    set_menu_overlay_function(normal_overlay_texto_menu);
	overlay_visible_when_menu_closed=0;


	//Y refrescar footer. Hacer esto para que redibuje en pantalla y no en layer de mezcla de menu
	//menu_init_footer();
	menu_clear_footer();
	redraw_footer();

	//Establecemos variable de salida de todos menus a 0
	salir_todos_menus=0;

	//printf ("inicio menu_inicio2\n");



	if (menu_button_exit_emulator.v) {
		//Pulsado salir del emulador
        //para evitar que entre con la pulsacion de teclas activa
        //menu_espera_no_tecla_con_repeticion();
        //menu_espera_no_tecla();
		osd_kb_no_mostrar_desde_menu=0; //Volver a permitir aparecer teclado osd

        menu_exit_emulator(0);
        cls_menu_overlay();
	}


	//ha saltado un breakpoint
	if (menu_breakpoint_exception.v) {
		//Ver tipo de accion para ese breakpoint
		//printf ("indice breakpoint & accion : %d\n",catch_breakpoint_index);
		osd_kb_no_mostrar_desde_menu=0; //Volver a permitir aparecer teclado osd


		//Si accion nula o menu o break
		if (debug_if_breakpoint_action_menu(catch_breakpoint_index)) {

			//menu_espera_no_tecla();
      //desactivamos multitarea, guardando antes estado multitarea
			int antes_menu_multitarea=menu_multitarea;
      			menu_multitarea=0;
      			audio_playing.v=0;
			//printf ("pc: %d\n",reg_pc);

			menu_breakpoint_fired(catch_breakpoint_message);


			menu_debug_registers(0);

			//restaurar estado multitarea

			menu_multitarea=antes_menu_multitarea;

      cls_menu_overlay();


			//Y despues de un breakpoint hacer que aparezca el menu normal y no vuelva a la ejecucion
			if (!salir_todos_menus) menu_inicio_bucle();
		}

		else {
			//Gestion acciones
			debug_run_action_breakpoint(debug_breakpoints_actions_array[catch_breakpoint_index]);
		}


	}

	if (menu_event_remote_protocol_enterstep.v) {
		//Entrada
		//menu_espera_no_tecla();
		osd_kb_no_mostrar_desde_menu=0; //Volver a permitir aparecer teclado osd

		remote_ack_enter_cpu_step.v=1; //Avisar que nos hemos enterado
		//Mientras no se salga del modo step to step del remote protocol
		while (menu_event_remote_protocol_enterstep.v) {
			timer_sleep(100);

			//Truco para que desde windows se pueda ejecutar el core loop desde aqui cuando zrcp lo llama
			/*if (towindows_remote_cpu_run_loop) {
				remote_cpu_run_loop(towindows_remote_cpu_run_misocket,towindows_remote_cpu_run_verbose,towindows_remote_cpu_run_limite);
				towindows_remote_cpu_run_loop=0;
			}*/
#ifdef MINGW
			int antes_menu_abierto=menu_abierto;
			menu_abierto=0; //Para que no aparezca en gris al refrescar
				scr_refresca_pantalla();
			menu_abierto=antes_menu_abierto;
				scr_actualiza_tablas_teclado();
#endif

		}

		debug_printf (VERBOSE_DEBUG,"Exiting remote enter step from menu");

		//Salida
		cls_menu_overlay();
	}

	if (menu_button_f_function.v) {

		//Si se reabre menu, resetear flags de teclas pulsadas especiales
		//Esto evita por ejemplo que al abrir menu con F5, si se entra a submenu, se crea que hemos pulsado F5 y cierre el menu y vuelva a abrir menu principal
		menu_button_f_function.v=0;

		//printf ("pulsada tecl de funcion\n");
		//Entrada
		//menu_espera_no_tecla();
		osd_kb_no_mostrar_desde_menu=0; //Volver a permitir aparecer teclado osd

		//Procesar comandos F

		//Procesamos cuando se pulsa tecla F concreta
		if (menu_button_f_function_action==0) menu_process_f_functions();
		else {
			//O procesar cuando se envia una accion concreta
			menu_process_f_functions_by_action(menu_button_f_function_action);
			menu_button_f_function_action=0;
		}

		menu_muestra_pending_error_message(); //Si se genera un error derivado de funcion F
		cls_menu_overlay();
	}

	if (menu_event_open_menu.v) {

		osd_kb_no_mostrar_desde_menu=0; //Volver a permitir aparecer teclado osd
		menu_inicio_bucle();

	}


	menu_was_open_by_left_mouse_button.v=0;


	//printf ("salir menu\n");


	//Volver
	menu_inicio_pre_retorno();



}



//Escribe bloque de cuadrado de color negro  
void set_splash_zesarux_logo_put_space(int x,int y)
{
	if (!strcmp(scr_new_driver_name,"aa")) {
		putchar_menu_overlay(x,y,'X',7,0);
	}
	else putchar_menu_overlay(x,y,' ',7,0);
}


//Hace cuadrado de 2x2
void set_splash_zesarux_logo_cuadrado(int x,int y)
{
	set_splash_zesarux_logo_put_space(x,y);
	set_splash_zesarux_logo_put_space(x+1,y);
	set_splash_zesarux_logo_put_space(x,y+1);
	set_splash_zesarux_logo_put_space(x+1,y+1);
}




//Escribe caracter  128 (franja de color-triangulo)
void set_splash_zesarux_franja_color(int x,int y,int tinta, int papel)
{
	if (si_complete_video_driver() ) {
		putchar_menu_overlay(x,y,128,tinta,papel);
	}
	else {
		putchar_menu_overlay(x,y,'/',tinta,7);
	}
}

//Escribe caracter ' ' con color
void set_splash_zesarux_cuadrado_color(int x,int y,int color)
{
	if (si_complete_video_driver() ) {
		putchar_menu_overlay(x,y,' ',0,color);
	}
}

void set_splash_zesarux_franja_color_repetido(int x,int y,int longitud, int color1, int color2)
{

	int j;
	for (j=0;j<longitud;j++) {
		set_splash_zesarux_franja_color(x+j,y-j,color1,color2);
	}

}


int get_zsplash_y_coord(void)
{
	return menu_center_y()-4;
}


//Dibuja el logo pero en diferentes pasos:
//0: solo la z
//1: franja roja
//2: franja roja y amarilla
//3: franja roja y amarilla y verde
//4 o mayor: franja roja y amarilla y verde y cyan
void set_splash_zesarux_logo_paso(int paso)
{
	int x,y;

	int ancho_z=6;
	int alto_z=6;

	int x_inicial=menu_center_x()-ancho_z;  //Centrado
	int y_inicial=get_zsplash_y_coord();

	debug_printf(VERBOSE_DEBUG,"Drawing ZEsarUX splash logo, step %d",paso);


	//Primero todo texto en gris. Envolvemos un poco mas
	for (y=y_inicial-1;y<y_inicial+ancho_z*2+1;y++) {
		for (x=x_inicial-1;x<x_inicial+ancho_z*2+1;x++) {
			putchar_menu_overlay_parpadeo(x,y,' ',0,7,0);


		}
	}


	y=y_inicial;

	//Linea Arriba Z, Abajo
	for (x=x_inicial;x<x_inicial+ancho_z*2;x++) {
		set_splash_zesarux_logo_put_space(x,y);
		set_splash_zesarux_logo_put_space(x,y+1);

		set_splash_zesarux_logo_put_space(x,y+alto_z*2-2);
		set_splash_zesarux_logo_put_space(x,y+alto_z*2-1);
	}

	//Cuadrados diagonales
	y+=2;

	for (x=x_inicial+(ancho_z-2)*2;x>x_inicial;x-=2,y+=2) {
		set_splash_zesarux_logo_cuadrado(x,y);
	}

	//Y ahora las lineas de colores
	//Rojo amarillo verde cyan
	//2      6       4     5

	if (paso==0) return;

	/*

    012345678901
0	XXXXXXXXXXXX
1	XXXXXXXXXXXX
2	        XX
3	        XX /
4	      XX  /
5	      XX / /
6	    XX  / /
7		XX / / /
8	  XX  / / /
9	  XX / / / /
0	XXXXXXXXXXXX
1	XXXXXXXXXXXX

    012345678901
	*/

/*
  RRRY
 RRRYY
RRRYYY

*/

/*
	        XX .
	      XX  .x
	      XX .x.
	    XX  .x.x
		XX .x.x.
	  XX  .x.x.x
	  XX .x.x.x.
	XXXXXXXXXXXX
	XXXXXXXXXXXX

    012345678901
	*/



	int j;


	set_splash_zesarux_franja_color_repetido(x_inicial+5,y_inicial+9,7, 2, 7);

	for (j=0;j<6;j++) {
		set_splash_zesarux_cuadrado_color(x_inicial+6+j,y_inicial+9-j,2);
	}

	//Lo que queda a la derecha de esa franja - el udg diagonal con el color de papel igual que tinta anterior, y papel blanco
	if (paso==1) {
		if (si_complete_video_driver() ) {
			set_splash_zesarux_franja_color_repetido(x_inicial+7,y_inicial+9,5, 7, 2);
		}
		return;
	}


	set_splash_zesarux_franja_color_repetido(x_inicial+7,y_inicial+9,5, 6, 2);

	for (j=0;j<4;j++) {
		set_splash_zesarux_cuadrado_color(x_inicial+8+j,y_inicial+9-j,6);
	}

	//Lo que queda a la derecha de esa franja - el udg diagonal con el color de papel igual que tinta anterior, y papel blanco
	if (paso==2) {
		if (si_complete_video_driver() ) {
			set_splash_zesarux_franja_color_repetido(x_inicial+9,y_inicial+9,3, 7, 6);
		}
		return;
	}



	set_splash_zesarux_franja_color_repetido(x_inicial+9,y_inicial+9,3, 4, 6);

	for (j=0;j<2;j++) {
		set_splash_zesarux_cuadrado_color(x_inicial+10+j,y_inicial+9-j,4);
	}

	//Lo que queda a la derecha de esa franja - el udg diagonal con el color de papel igual que tinta anterior, y papel blanco
	if (paso==3) {
		if (si_complete_video_driver() ) {
			set_splash_zesarux_franja_color(x_inicial+ancho_z*2-1,y_inicial+ancho_z*2-3,7,4);
		}
		return;
	}

	set_splash_zesarux_franja_color(x_inicial+ancho_z*2-1,y_inicial+ancho_z*2-3,5,4);

}




//Retorna color de paleta spectrum segun letra color logo ascii W: white, X: Black, etc
//en mayusculas es con brillo, sin mayusculas es sin brillo
int return_color_zesarux_ascii(char c)
{

	int color;

	int brillo=0;

	if (c>='A' && c<='Z') {
		brillo=1;
		c=letra_minuscula(c);
	}

	switch (c) {


		//Black
		case 'x':
			color=0;
		break;

		//Blue
		case 'b':
			color=1;
		break;		

		//Red
		case 'r':
			color=2;
		break;

		//Magenta
		case 'm':
			color=3;
		break;		

		//Green
		case 'g':
			color=4;
		break;

		//Cyan
		case 'c':
			color=5;
		break;

		//Yellow
		case 'y':
			color=6;
		break;

		//White
		case 'w':
			color=7;
		break;		

		//Black default
		default:
			color=0;
		break;
	}

	return color+brillo*8;
}

void set_splash_zesarux_logo(void)
{
	splash_zesarux_logo_active=1;
	splash_zesarux_logo_paso=0;
	set_splash_zesarux_logo_paso(splash_zesarux_logo_paso);
}

void set_welcome_message(void)
{
	cls_menu_overlay();
	char texto_welcome[40];
	sprintf(texto_welcome," Welcome to ZEsarUX v." EMULATOR_VERSION " ");

	int yinicial=get_zsplash_y_coord()-6;

	//centramos texto
	int x=menu_center_x()-strlen(texto_welcome)/2;
	if (x<0) x=0;

	menu_escribe_texto(x,yinicial++,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,texto_welcome);

	set_splash_zesarux_logo();


        char texto_edition[40];
        sprintf(texto_edition," " EMULATOR_EDITION_NAME " ");

		int longitud_texto=strlen(texto_edition);
		//temporal, como estamos usando parpadeo mediante caracteres ^^, no deben contar en la longitud
		//cuando no se use parpadeo, quitar esta resta
		//longitud_texto -=4;

        //centramos texto
        x=menu_center_x()-longitud_texto/2;
        if (x<0) x=0;

        menu_escribe_texto(x,yinicial++,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,texto_edition);




	char texto_esc_menu[32];
	sprintf(texto_esc_menu," Press %s for menu ",openmenu_key_message);
	longitud_texto=strlen(texto_esc_menu);
        x=menu_center_x()-longitud_texto/2;
        if (x<0) x=0;	
	menu_escribe_texto(x,yinicial++,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,texto_esc_menu);

	set_menu_overlay_function(normal_overlay_texto_menu);
	menu_splash_text_active.v=1;
	menu_splash_segundos=5;

}

int first_time_menu_footer_f5_menu=1;


//2 segundos antes de que se avise si hay detectado joystick o no
int menu_tell_if_realjoystick_detected_counter=2;

void menu_tell_if_realjoystick_detected(void)
{
			//Si detectado real joystick
			//Si detectado joystick real y si hay autoguardado de config
			if (save_configuration_file_on_exit.v) {
					if (realjoystick_present.v) {
							menu_set_menu_abierto(1);
							//printf ("decir menu abierto\n");
							realjoystick_detected_startup=1;
					}
			}				
}

void reset_welcome_message(void)
{
	if (menu_splash_text_active.v==1) {

		//printf ("%d\n",menu_splash_segundos);
		menu_splash_segundos--;
		if (menu_splash_segundos==0) {
			reset_splash_zesarux_logo();
			menu_splash_text_active.v=0;
			cls_menu_overlay();
			reset_menu_overlay_function();
			debug_printf (VERBOSE_DEBUG,"End splash text");

			//Quitamos el splash text pero dejamos el F5 menu abajo en el footer, hasta que algo borre ese mensaje
			//(por ejemplo que cargamos una cinta/snap con configuracion y genera mensaje en texto inverso en el footer)
			if (first_time_menu_footer_f5_menu) {
				menu_footer_f5_menu();
				first_time_menu_footer_f5_menu=0; //Solo mostrarlo una sola vez
			}



			//abrir el menu si hay first aid en startup disponible
			//Para que aparezca el mensaje del dia, tiene que estar habilitado el setting de welcome message
			//Si no, no llegara aqui nunca



		}

		else {
			if (splash_zesarux_logo_active) {
				splash_zesarux_logo_paso++;
				set_splash_zesarux_logo_paso(splash_zesarux_logo_paso);
			}
		}


	}
}


#define FILESEL_INICIAL_ANCHO 32
#define FILESEL_MAX_ANCHO OVERLAY_SCREEN_MAX_WIDTH

#define FILESEL_INICIAL_ALTO 24

#define FILESEL_INICIAL_X (menu_center_x()-FILESEL_INICIAL_ANCHO/2)
#define FILESEL_INICIAL_Y (menu_center_y()-FILESEL_INICIAL_ALTO/2)

#define FILESEL_INICIO_DIR 4

#define ZXVISION_POS_FILTER 6
#define ZXVISION_POS_LEYENDA 7




void zxvision_menu_filesel_print_filters(zxvision_window *ventana,char *filtros[])
{


        if (menu_filesel_show_utils.v) return; //Si hay utilidades activas, no mostrar filtros

        //texto para mostrar filtros. darle bastante margen aunque no quepa en pantalla
        char buffer_filtros[FILESEL_MAX_ANCHO+1]; //+1 para el 0 final


        char *f;

        int i,p;
        p=0;
        sprintf(buffer_filtros,"Filter: ");

        p=p+8;  //8 es lo que ocupa el texto "Filter: "


        for (i=0;filtros[i];i++) {
                //si filtro es "", significa todo (*)

                f=filtros[i];
                if (f[0]==0) f="*";

                //copiamos
                //sprintf(&buffer_filtros[p],"*.%s ",f);
                sprintf(&buffer_filtros[p],"%s ",f);
                p=p+strlen(f)+1;

        }

//Si texto filtros pasa del tope, rellenar con "..."
		int max_visible=(ventana->visible_width)-2;
	
        if (p>max_visible && max_visible>=3) {
                p=max_visible;
                buffer_filtros[p-1]='.';
                buffer_filtros[p-2]='.';
                buffer_filtros[p-3]='.';
        }


        buffer_filtros[p]=0;


        //borramos primero con espacios


	int posicion_filtros=ZXVISION_POS_FILTER;


	zxvision_print_string_defaults_fillspc(ventana,1,posicion_filtros,"");


        //y luego escribimos


        //si esta filesel_zona_pantalla=2, lo ponemos en otro color. TODO
        int inverso=0;
        if (filesel_zona_pantalla==2) inverso=1;



	int tinta=ESTILO_GUI_TINTA_NORMAL;
	int papel=ESTILO_GUI_PAPEL_NORMAL;

	if (inverso) {
		tinta=ESTILO_GUI_TINTA_SELECCIONADO;
		papel=ESTILO_GUI_PAPEL_SELECCIONADO;
	}


	zxvision_print_string(ventana,1,posicion_filtros,tinta,papel,0,buffer_filtros);
}



void zxvision_menu_filesel_print_legend(zxvision_window *ventana)
{

                //Forzar a mostrar atajos
                z80_bit antes_menu_writing_inverse_color;
                antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
                menu_writing_inverse_color.v=1;

	int posicion_leyenda=ZXVISION_POS_LEYENDA;
	int posicion_filtros=ZXVISION_POS_FILTER;

	char leyenda_inferior[64];

	/*
#ifdef MINGW

			//01234567890123456789012345678901
			// TAB: Section R: Recent D: Drive
	sprintf (leyenda_inferior,"~~T~~A~~B:Section ~~Recent ~~Drive");
#else
	sprintf (leyenda_inferior,"~~T~~A~~B: Section ~~R: Recent");
#endif

*/

	//Drive también mostrado en Linux y Mac
			//01234567890123456789012345678901
			// TAB: Section R: Recent D: Drive
	sprintf (leyenda_inferior,"~^T~^A~^B:Section ~^Recent ~^Drives");	

	zxvision_print_string_defaults_fillspc(ventana,1,posicion_leyenda,leyenda_inferior);


        if (menu_filesel_show_utils.v) {


                                                                //    01234  567890  12345  678901  2345678901
                zxvision_print_string_defaults_fillspc(ventana,1,posicion_filtros-1,"~^View ~^Trunc D~^El M~^Kdr C~^Onv ~^Inf");
                zxvision_print_string_defaults_fillspc(ventana,1,posicion_filtros,"~^Copy ~^Move Re~^N ~^Paste ~^Filemem");

        }

                //Restaurar comportamiento mostrar atajos
                menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;
}



filesel_item *menu_get_filesel_item(int index)
{
	filesel_item *p;

	p=primer_filesel_item;

	int i;

	for(i=0;i<index;i++) {
		p=p->next;
	}

	return p;

}

//Dice si archivo es de tipo comprimido/empaquetado. filename tiene que ser sin directorio
int menu_util_file_is_compressed(char *filename)
{
		//Si seleccion es archivo comprimido
							if (
							    //strstr(item_seleccionado->d_name,".zip")!=NULL ||
							    !util_compare_file_extension(filename,"zip") ||
                                                            !util_compare_file_extension(filename,"gz")  ||
                                                            !util_compare_file_extension(filename,"tar") ||
                                                            !util_compare_file_extension(filename,"rar") 


							) {
								return 1;
							}
	else return 0;
}

//obtiene linea a escribir con nombre de archivo + carpeta
void menu_filesel_print_file_get(char *buffer, char *s,unsigned int max_length_shown)
{
	unsigned int i;

        for (i=0;i<max_length_shown && (s[i])!=0;i++) {
                buffer[i]=s[i];
        }


        //si sobra espacio, rellenar con espacios
        for (;i<max_length_shown;i++) {
                buffer[i]=' ';
        }

        buffer[i]=0;


        //si no cabe, poner puntos suspensivos
        if (strlen(s)>max_length_shown && i>=3) {
                buffer[i-1]='.';
                buffer[i-2]='.';
                buffer[i-3]='.';
        }

    //y si es un directorio (sin nombre nulo ni espacio), escribir "<dir>
	//nota: se envia nombre " " (un espacio) cuando se lista el directorio y sobran lineas en blanco al final

	int test_dir=1;

	if (s[0]==0) test_dir=0;
	if (s[0]==' ' && s[1]==0) test_dir=0;

	if (test_dir) {
	        if (get_file_type(s) == 2 && i>=5) {
        	        buffer[i-1]='>';
                	buffer[i-2]='r';
	                buffer[i-3]='i';
        	        buffer[i-4]='d';
                	buffer[i-5]='<';
	        }

			//O si es empaquetado
			/*else if (menu_util_file_is_compressed(s) && i>=5) {
				    buffer[i-1]='>';
                	buffer[i-2]='p';
	                buffer[i-3]='x';
        	        buffer[i-4]='e';
                	buffer[i-5]='<';
			}*/
	}


}

//escribe el nombre de archivo o carpeta

//Margen de 8 lineas (4+4) de leyendas
#define ZXVISION_FILESEL_INITIAL_MARGIN 8

void zxvision_menu_filesel_print_file(zxvision_window *ventana,char *s,unsigned int max_length_shown,int y)
{

        char buffer[PATH_MAX];



        menu_filesel_print_file_get(buffer, s, max_length_shown);


	zxvision_print_string_defaults_fillspc(ventana,1,y+ZXVISION_FILESEL_INITIAL_MARGIN,buffer);	
}




void menu_filesel_switch_filters(void)
{

	//si filtro inicial, ponemos el *.*
	if (filesel_filtros==filesel_filtros_iniciales)
		filesel_filtros=filtros_todos_archivos;

	//si filtro *.* , ponemos el filtro inicial
	else filesel_filtros=filesel_filtros_iniciales;

}

void menu_filesel_chdir(char *dir)
{
	chdir(dir);
}

/*char menu_minus_letra(char letra)
{
	if (letra>='A' && letra<='Z') letra=letra+('a'-'A');
	return letra;
}*/



void zxvision_menu_filesel_localiza_letra(zxvision_window *ventana,char letra)
{

        int i;
        filesel_item *p;
        p=primer_filesel_item;

        for (i=0;i<filesel_total_items;i++) {
                if (letra_minuscula(p->d_name[0])>=letra_minuscula(letra)) {
                        filesel_linea_seleccionada=0;
                        filesel_archivo_seleccionado=i;
			ventana->cursor_line=i;
			zxvision_set_offset_y_or_maximum(ventana,i);
			//printf ("linea seleccionada en localizacion: %d\n",i);
                        return;
                }


                p=p->next;
        }

}



void zxvision_menu_filesel_localiza_archivo(zxvision_window *ventana,char *nombrebuscar)
{
        debug_printf (VERBOSE_DEBUG,"Searching last file %s",nombrebuscar);
        int i;
        filesel_item *p;
        p=primer_filesel_item;

        for (i=0;i<filesel_total_items;i++) {
                debug_printf (VERBOSE_DEBUG,"File number: %d Name: %s",i,p->d_name);
                //if (menu_minus_letra(p->d_name[0])>=menu_minus_letra(letra)) {
                if (strcasecmp(nombrebuscar,p->d_name)<=0) {
                        filesel_linea_seleccionada=0;
                        filesel_archivo_seleccionado=i;
						ventana->cursor_line=i;
						zxvision_set_offset_y_or_maximum(ventana,i);
                        debug_printf (VERBOSE_DEBUG,"Found at position %d",i);
                        return;
                }


                p=p->next;
        }

}


int si_menu_filesel_no_mas_alla_ultimo_item(int linea)
{
	if (filesel_archivo_seleccionado+linea<filesel_total_items-1) return 1;
	return 0;
}

//Cargar archivo en memory zone
void file_utils_file_mem_load(char *archivo)
{
		int tamanyo=get_file_size(archivo);
		//Asignar memoria si no estaba asignada

		int error_limite=0;

		//Max 16 mb  (0x1000000), para no usar mas de 6 digitos al mostrar la direccion
		if (tamanyo>0x1000000) {
			tamanyo=0x1000000;
			error_limite=1;
		}

		//liberar si habia algo
		if (memory_zone_by_file_size>0) {
			debug_printf(VERBOSE_DEBUG,"Freeing previous file memory zone");
			free(memory_zone_by_file_pointer);
		}

		debug_printf(VERBOSE_DEBUG,"Allocating %d bytes for file memory zone",tamanyo);
		memory_zone_by_file_pointer=malloc(tamanyo);
		if (memory_zone_by_file_pointer==NULL) {
			cpu_panic("Can not allocate memory for file read");
		}

		memory_zone_by_file_size=tamanyo;

                FILE *ptr_load;
                ptr_load=fopen(archivo,"rb");

                if (!ptr_load) {
                        debug_printf (VERBOSE_ERR,"Unable to open file %s",archivo);
                        return;
                }

/*
extern char memory_zone_by_file_name[];
extern z80_byte *memory_zone_by_file_pointer;
extern int memory_zone_by_file_size;
*/

		//Copiamos el nombre del archivo aunque de momento no lo uso
		strcpy(memory_zone_by_file_name,archivo);


                int leidos=fread(memory_zone_by_file_pointer,1,tamanyo,ptr_load);
                if (leidos!=tamanyo) {
                        debug_printf (VERBOSE_ERR,"Error reading file. Bytes read: %d bytes",leidos);
                }

		fclose(ptr_load);

		if (error_limite) menu_warn_message("File too big. Reading first 16 Mb");
		else menu_generic_message_splash("File memory zone","File loaded to File memory zone");
}


//parametro rename: 
//si 0, move
//si 1, es rename
//si 2, copy
void file_utils_move_rename_copy_file(char *archivo,int rename_move)
{
	char nombre_sin_dir[PATH_MAX+1];
	char directorio[PATH_MAX+1];
	char nombre_final[(PATH_MAX+1)*2];

	util_get_dir(archivo,directorio);
	util_get_file_no_directory(archivo,nombre_sin_dir);



	int ejecutar_accion=1;

	//Rename
	if (rename_move==1) {
		menu_ventana_scanf("New name",nombre_sin_dir,PATH_MAX);
		sprintf(nombre_final,"%s/%s",directorio,nombre_sin_dir);
	}

	//Copy or move
	else if (rename_move==2 || rename_move==0) {
		//Move or copy
		char *filtros[2];

       	 	filtros[0]="nofiles";
        	filtros[1]=0;


        	//guardamos directorio actual
        	char directorio_actual[PATH_MAX];
        	getcwd(directorio_actual,PATH_MAX);

        	int ret;


        	char nada[PATH_MAX];


        	//Ocultar utilidades
        	menu_filesel_show_utils.v=0;
        	ret=menu_filesel("Enter dir & press ESC",filtros,nada);
        	//Volver a mostrar utilidades
        	menu_filesel_show_utils.v=1;


        	//Si sale con ESC
        	if (ret==0) {

        		//Move
                      	if (rename_move==0) debug_printf (VERBOSE_DEBUG,"Move file %s to directory %s",archivo,menu_filesel_last_directory_seen);

                      	//Copy
                      	if (rename_move==2) debug_printf (VERBOSE_DEBUG,"Copy file %s to directory %s",archivo,menu_filesel_last_directory_seen);
                	sprintf(nombre_final,"%s/%s",menu_filesel_last_directory_seen,nombre_sin_dir);

        	}
        	else {
        		//TODO: hacer de manera facil que menu_filesel no deje seleccionar archivos con enter y solo deje salir con ESC
        		menu_warn_message("You must select the directory exiting with ESC key. Aborting!");
        		ejecutar_accion=0;
        	}


        	//volvemos a directorio inicial
        	menu_filesel_chdir(directorio_actual);
	}

	if (ejecutar_accion) {
		debug_printf (VERBOSE_INFO,"Original name: %s dir: %s new name %s final name %s"
				,archivo,directorio,nombre_sin_dir,nombre_final);


		if (menu_confirm_yesno_texto("Confirm operation","Sure?")==0) return;

		//Si existe archivo destino
		if (si_existe_archivo(nombre_final)) {
			if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return;
		}

		if (rename_move==2) util_copy_file(archivo,nombre_final);
		else rename(archivo,nombre_final);


		//Copy
		if (rename_move==2) menu_generic_message("Copy file","OK. File copied");
		//Rename
		else if (rename_move==1) menu_generic_message("Rename file","OK. File renamed");
		//Move
		else menu_generic_message("Move file","OK. File moved");
	}
}



void file_utils_paste_clipboard(void)
{

	if (menu_clipboard_pointer==NULL) {
		debug_printf(VERBOSE_ERR,"Clipboard is empty, you can fill it from a text window and press key c");
		return;
	}

	char directorio_actual[PATH_MAX];
    getcwd(directorio_actual,PATH_MAX);

	char nombre_sin_dir[PATH_MAX+1];
	char nombre_final[(PATH_MAX+1)*2];


	nombre_sin_dir[0]=0;
	menu_ventana_scanf("Filename?",nombre_sin_dir,PATH_MAX);
	sprintf(nombre_final,"%s/%s",directorio_actual,nombre_sin_dir);


	//Ver si archivo existe y preguntar
	if (si_existe_archivo(nombre_final)) {

		if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return;

    }


	menu_paste_clipboard_to_file(nombre_final);

	menu_generic_message_splash("Clipboard","File saved with ZEsarUX clipboard contents");


}



void zxvision_menu_filesel_cursor_arriba(zxvision_window *ventana)
{
	//ver que no sea primer archivo
    if (filesel_archivo_seleccionado+filesel_linea_seleccionada!=0) {
	 ventana->cursor_line--;
                                                //ver si es principio de pantalla
                                                if (filesel_linea_seleccionada==0) {
							zxvision_send_scroll_up(ventana);
                                                        filesel_archivo_seleccionado--;
                                                }
                                                else {
                                                        filesel_linea_seleccionada--;
                                                }
                                        }

	//Por si el cursor no esta visible en pantalla (al haberse hecho scroll con raton)	
	if (zxvision_adjust_cursor_top(ventana)) {
		zxvision_send_scroll_up(ventana);
		filesel_linea_seleccionada=0;
		filesel_archivo_seleccionado=ventana->cursor_line;
	}
}

int zxvision_get_filesel_alto_dir(zxvision_window *ventana)
{
	return ventana->visible_height - ventana->upper_margin - ventana->lower_margin - 2;
}

int zxvision_get_filesel_pos_filters(zxvision_window *ventana)
{
	return ventana->visible_height - 3;
}


void zxvision_menu_filesel_cursor_abajo(zxvision_window *ventana)
{
	//ver que no sea ultimo archivo
	if (si_menu_filesel_no_mas_alla_ultimo_item(filesel_linea_seleccionada)) {
		ventana->cursor_line++;
                                                //ver si es final de pantalla
                                                if (filesel_linea_seleccionada==zxvision_get_filesel_alto_dir(ventana)-1) {
                                                        filesel_archivo_seleccionado++;
							zxvision_send_scroll_down(ventana);
                                                }
                                                else {
                                                        filesel_linea_seleccionada++;
                                                }
                                        }
	//Por si el cursor no esta visible en pantalla (al haberse hecho scroll con raton)									
	if (zxvision_adjust_cursor_bottom(ventana)) {
		zxvision_send_scroll_down(ventana);
		filesel_linea_seleccionada=zxvision_get_filesel_alto_dir(ventana)-1;
		filesel_archivo_seleccionado=ventana->cursor_line-filesel_linea_seleccionada;
	}

}


//liberar memoria usada por la lista de archivos
void menu_filesel_free_mem(void)
{

	filesel_item *aux;
        filesel_item *nextfree;


        aux=primer_filesel_item;

        //do {

	//puede que no haya ningun archivo, por tanto esto es NULL
	//sucede con las carpetas /home en macos por ejemplo
	while (aux!=NULL) {

                //printf ("Liberando %p : %s\n",aux,aux->d_name);
                nextfree=aux->next;
                free(aux);

                aux=nextfree;
        };
        //} while (aux!=NULL);

	//printf ("fin liberar filesel\n");


}


int menu_filesel_mkdir(char *directory)
{
#ifndef MINGW
     int tmpdirret=mkdir(directory,S_IRWXU);
#else
	int tmpdirret=mkdir(directory);
#endif

     if (tmpdirret!=0 && errno!=EEXIST) {
                  debug_printf (VERBOSE_ERR,"Error creating %s directory : %s",directory,strerror(errno) );
     }

	return tmpdirret;

}

void menu_filesel_exist_ESC(void)
{
                                                cls_menu_overlay();
                                                menu_espera_no_tecla();
                                                menu_filesel_chdir(filesel_directorio_inicial);
                                                menu_filesel_free_mem();
}

//Elimina la extension de un nombre
void menu_filesel_file_no_ext(char *origen, char *destino)
{



	int j;

        j=strlen(origen);

	//buscamos desde el punto final

        for (;j>=0 && origen[j]!='.';j--);

	if (j==-1) {
		//no hay punto
		j=strlen(origen);
	}

	//printf ("posicion final: %d\n",j);


	//y copiamos
	for (;j>0;j--) {
		*destino=*origen;
		origen++;
		destino++;
	}

	//Y final de cadena
	*destino = 0;

	//printf ("archivo sin extension: %s\n",copiadestino);

}


#define COMPRESSED_ZIP 1
#define COMPRESSED_GZ  2
#define COMPRESSED_TAR 3
#define COMPRESSED_RAR 4

int menu_filesel_is_compressed(char *archivo)
{
  int compressed_type=0;

	//if ( strstr(archivo,".zip")!=NULL || strstr(archivo,".ZIP")!=NULL) {
	if ( !util_compare_file_extension(archivo,"zip") ) {
		debug_printf (VERBOSE_DEBUG,"Is a zip file");
		compressed_type=COMPRESSED_ZIP;
	}

        else if ( !util_compare_file_extension(archivo,"gz") ) {
                debug_printf (VERBOSE_DEBUG,"Is a gz file");
                compressed_type=COMPRESSED_GZ;
        }

        else if ( !util_compare_file_extension(archivo,"tar") ) {
                debug_printf (VERBOSE_DEBUG,"Is a tar file");
                compressed_type=COMPRESSED_TAR;
        }

        else if ( !util_compare_file_extension(archivo,"rar") ) {
                debug_printf (VERBOSE_DEBUG,"Is a rar file");
                compressed_type=COMPRESSED_RAR;
        }

	return compressed_type;	
}


//escribir archivo que indique directorio anterior
void menu_filesel_write_file_last_dir(char *directorio_anterior)
{
	debug_printf (VERBOSE_DEBUG,"Writing temp file " MENU_LAST_DIR_FILE_NAME " to tell last directory before uncompress (%s)",directorio_anterior);


    FILE *ptr_lastdir;
    ptr_lastdir=fopen(MENU_LAST_DIR_FILE_NAME,"wb");

	if (ptr_lastdir!=NULL) {
	        fwrite(directorio_anterior,1,strlen(directorio_anterior),ptr_lastdir);
        	fclose(ptr_lastdir);
	}
}

//leer contenido de archivo que indique directorio anterior
//Devuelve 0 si OK. 1 si error
int menu_filesel_read_file_last_dir(char *directorio_anterior)
{

        FILE *ptr_lastdir;
        ptr_lastdir=fopen(MENU_LAST_DIR_FILE_NAME,"rb");

	if (ptr_lastdir==NULL) {
		debug_printf (VERBOSE_DEBUG,"Error opening " MENU_LAST_DIR_FILE_NAME);
                return 1;
        }


        int leidos=fread(directorio_anterior,1,PATH_MAX,ptr_lastdir);
        fclose(ptr_lastdir);

	if (leidos) {
		directorio_anterior[leidos]=0;
	}
	else {
		if (leidos==0) debug_printf (VERBOSE_DEBUG,"Error. Read 0 bytes from " MENU_LAST_DIR_FILE_NAME);
		if (leidos<0) debug_printf (VERBOSE_DEBUG,"Error reading from " MENU_LAST_DIR_FILE_NAME);
		return 1;
	}

	return 0;
}



int zxvision_si_mouse_zona_archivos(zxvision_window *ventana)
{
    int inicio_y_dir=1+FILESEL_INICIO_DIR;

    if (menu_mouse_y>=inicio_y_dir && menu_mouse_y<inicio_y_dir+zxvision_get_filesel_alto_dir(ventana) && menu_mouse_x<ventana->visible_width-1) {
		//printf ("Mouse en zona de archivos\n");
		return 1;
	}
    
	return 0;
}


void zxvision_menu_filesel_print_text_contents(zxvision_window *ventana)
{
	zxvision_print_string_defaults_fillspc(ventana,1,2,"Directory Contents:");
}

void file_utils_info_file(char *archivo)
{

	long int tamanyo=get_file_size(archivo);
	//fecha
       int hora;
        int minutos;
        int segundos;

        int anyo;
        int mes;
        int dia;


        get_file_date_from_name(archivo,&hora,&minutos,&segundos,&dia,&mes,&anyo);



	menu_generic_message_format("Info file","Full path: %s\n\nSize: %ld bytes\nModified: %02d:%02d:%02d %02d/%02d/%02d",
		archivo,tamanyo,hora,minutos,segundos,dia,mes,anyo);

}


//Si hay que iniciar el filesel pero mover el cursor a un archivo concreto
z80_bit menu_filesel_posicionar_archivo={0};
char menu_filesel_posicionar_archivo_nombre[PATH_MAX]="";

void menu_filesel_change_to_tmp(char *tmpdir)
{
                                                                        //cambiar a tmp dir.

                                                                        //Dejar antes un archivo temporal en ese directorio que indique directorio anterior
                                                                        char directorio_actual[PATH_MAX];
                                                                        getcwd(directorio_actual,PATH_MAX);

                                                                        menu_filesel_chdir(tmpdir);

                                                                        //escribir archivo que indique directorio anterior
                                                                        menu_filesel_write_file_last_dir(directorio_actual);

                                                                        menu_filesel_free_mem();
}


void estilo_gui_retorna_nombres(void)
{
	int i;

	for (i=0;i<ESTILOS_GUI;i++) {
		printf ("%s ",definiciones_estilos_gui[i].nombre_estilo);
	}
}

void set_charset(void)
{

	if (estilo_gui_activo==ESTILO_GUI_CPC) char_set=char_set_cpc;
	else if (estilo_gui_activo==ESTILO_GUI_MSX) char_set=char_set_msx;
	else if (estilo_gui_activo==ESTILO_GUI_Z88) char_set=char_set_z88;
	else if (estilo_gui_activo==ESTILO_GUI_SAM) char_set=char_set_sam;
	else if (estilo_gui_activo==ESTILO_GUI_MANSOFTWARE) char_set=char_set_mansoftware;
	else if (estilo_gui_activo==ESTILO_GUI_QL) char_set=char_set_ql;
	else if (estilo_gui_activo==ESTILO_GUI_RETROMAC) char_set=char_set_retromac;
	else char_set=char_set_spectrum;
}

void zxvision_menu_print_dir(int inicial,zxvision_window *ventana)
{

	//TODO: no tiene sentido usar variable "inicial"
	inicial=0;

	//printf ("\nmenu_print_dir\n");

	//escribir en ventana directorio de archivos

	//Para speech
	char texto_opcion_activa[PATH_MAX+100]; //Dado que hay que meter aqui el nombre del archivo y un poquito mas de texto
	//Asumimos por si acaso que no hay ninguna activa
	texto_opcion_activa[0]=0;



	filesel_item *p;
	int i;

	int mostrados_en_pantalla=(ventana->visible_height)-10;
	//trucar el maximo en pantalla. dado que somos zxvision, se pueden mostar ya todos en resolucion virtual de ventana
	mostrados_en_pantalla=999999;

	p=menu_get_filesel_item(inicial);

	//Para calcular total de archivos de ese directorio, siguiendo el filtro. Util para mostrar indicador de porcentaje '*'
	//int total_archivos=inicial;

	filesel_total_archivos=inicial;

	for (i=0;p!=NULL;i++,filesel_total_archivos++) {
		//printf ("file: %s\n",p->d_name);

		//Solo hacer esto si es visible en pantalla
		if (i<mostrados_en_pantalla) {
		
		zxvision_menu_filesel_print_file(ventana,p->d_name,(ventana->total_width)-2,i);
		

		//if (filesel_linea_seleccionada==i) {
		if (ventana->cursor_line==i) {
			char buffer[OVERLAY_SCREEN_MAX_WIDTH+1],buffer2[OVERLAY_SCREEN_MAX_WIDTH+1+32];
			

			strcpy(filesel_nombre_archivo_seleccionado,p->d_name);

			//menu_tape_settings_trunc_name(filesel_nombre_archivo_seleccionado,buffer,22);
			//printf ("antes de trunc\n");

			int tamanyo_mostrar=ventana->visible_width-6-1; //6 ocupa el texto "File: "

				menu_tape_settings_trunc_name(filesel_nombre_archivo_seleccionado,buffer,tamanyo_mostrar); 

			sprintf (buffer2,"File: %s",buffer);
			
			zxvision_print_string_defaults_fillspc(ventana,1,1,buffer2);


				debug_printf (VERBOSE_DEBUG,"Selected: %s. filesel_zona_pantalla: %d",p->d_name,filesel_zona_pantalla);
				//Para speech
				//Si estamos en zona central del selector de archivos, decirlo
				if (filesel_zona_pantalla==1) {

	                                if (menu_active_item_primera_vez) {
						

        	                                sprintf (texto_opcion_activa,"Selected item: %s %s",p->d_name,(get_file_type(p->d_name) == 2 ? "directory" : ""));
                	                        menu_active_item_primera_vez=0;
                        	        }

                                	else {
	                                        sprintf (texto_opcion_activa,"%s %s",p->d_name,(get_file_type(p->d_name) == 2 ? "directory" : ""));
        	                        }

				}


		}
		}

		p=p->next;

    }



	//int texto_no_cabe=0;
	filesel_no_cabe_todo=0;

	debug_printf (VERBOSE_DEBUG,"Total files read (applying filters): %d",filesel_total_archivos);
	if (filesel_total_archivos>mostrados_en_pantalla) {
		filesel_no_cabe_todo=1;
	}




	//Imprimir directorio actual
	//primero borrar con espacios

    //menu_escribe_texto_ventana(14,0,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,"               ");


	char current_dir[PATH_MAX];
	char buffer_dir[OVERLAY_SCREEN_MAX_WIDTH+1];
	char buffer3[OVERLAY_SCREEN_MAX_WIDTH+1+32];
	getcwd(current_dir,PATH_MAX);

	menu_tape_settings_trunc_name(current_dir,buffer_dir,ventana->visible_width-14); //14 es lo que ocupa el texto "Current dir: "
	sprintf (buffer3,"Current dir: %s",buffer_dir);
	//menu_escribe_texto_ventana(1,0,ESTILO_GUI_TINTA_NORMAL,ESTILO_GUI_PAPEL_NORMAL,buffer3);
	zxvision_print_string_defaults_fillspc(ventana,1,0,buffer3);


}


              //Si en linea de "File"
int menu_filesel_change_zone_if_clicked(zxvision_window *ventana,int *filesel_zona_pantalla,int *tecla)
{
     if (!si_menu_mouse_en_ventana() ) return 0;
	if (!mouse_left) return 0;

	int futura_zona=-1;
    if (menu_mouse_y==2 && menu_mouse_x<ventana->visible_width-1) {
                //printf ("Pulsado zona File\n");
						futura_zona=0;
    }

                //Si en linea de filtros
    if (menu_mouse_y==zxvision_get_filesel_pos_filters(ventana)  && menu_mouse_x<ventana->visible_width-1) {
								//printf ("Pulsado zona Filtros\n");
                                                                futura_zona=2;
    }


		//En zona seleccion archivos
    if (zxvision_si_mouse_zona_archivos(ventana)) {	
							//printf ("En zona seleccion archivos\n");
							futura_zona=1;
		}


	if (futura_zona!=-1) {
		if (futura_zona!=*filesel_zona_pantalla) {
			//Cambio de zona
			menu_reset_counters_tecla_repeticion();
			*filesel_zona_pantalla=futura_zona;
			*tecla=0;
			return 1;
		}
	}




	return 0;
}


//Cambiar unidad Windows
//Retorna 0 si cancelado
char menu_filesel_cambiar_unidad_windows(void)
{

	char buffer_unidades[100]; //Aunque son 26 maximo, pero por si acaso
	int unidades=util_get_available_drives(buffer_unidades);
	if (unidades==0) {
		menu_error_message("No available drives");
		return 0;
	}

	//printf ("total unidades: %d string Unidades: %s 0 %d 1 %d 2 %d 3 %d\n",unidades,buffer_unidades,buffer_unidades[0],buffer_unidades[1],buffer_unidades[2],buffer_unidades[3]);


        menu_item *array_menu_filesel_unidad;
        menu_item item_seleccionado;
        int retorno_menu;

	int menu_filesel_unidad_opcion_seleccionada=0;


	menu_add_item_menu_inicial(&array_menu_filesel_unidad,"",MENU_OPCION_UNASSIGNED,NULL,NULL);

	int i;

	for (i=0;i<unidades;i++) {
		char letra=buffer_unidades[i];
		menu_add_item_menu_format(array_menu_filesel_unidad,MENU_OPCION_NORMAL,NULL,NULL,"~~%c:",letra);
		menu_add_item_menu_shortcut(array_menu_filesel_unidad,letra_minuscula(letra));
		menu_add_item_menu_valor_opcion(array_menu_filesel_unidad,letra);
	}

                menu_add_item_menu(array_menu_filesel_unidad,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_filesel_unidad);
                retorno_menu=menu_dibuja_menu(&menu_filesel_unidad_opcion_seleccionada,&item_seleccionado,array_menu_filesel_unidad,"Select Drive" );

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
				//Sacamos la letra del texto mismo
				char unidad=item_seleccionado.valor_opcion;
				//printf ("Leida unidad de menu: %c\n",unidad);
				return unidad;
                }



	return 0;
}

//Retorna 1 si ha realizado cambio cursor. 0 si no
int menu_filesel_set_cursor_at_mouse(zxvision_window *ventana)
{
								int inicio_y_dir=1+FILESEL_INICIO_DIR;
                            //if (menu_mouse_y>=inicio_y_dir && menu_mouse_y<inicio_y_dir+(FILESEL_ALTO-10)) {
                            //printf ("Dentro lista archivos\n");

                            //Ver si linea dentro de rango
                            int linea_final=menu_mouse_y-inicio_y_dir;

                            //Si esta en la zona derecha de selector de porcentaje no hacer nada
                            
                            
							if (menu_mouse_x==(ventana->visible_width)-1) return 0;
							

                            //filesel_linea_seleccionada=menu_mouse_y-inicio_y_dir;

                            if (si_menu_filesel_no_mas_alla_ultimo_item(linea_final-1)) {

								//Ajustar cursor ventana

								//if (zxvision_cursor_out_view(ventana)) {
								filesel_archivo_seleccionado=ventana->offset_y;
								ventana->cursor_line=filesel_archivo_seleccionado;
								//}

								//ventana->cursor_line -=filesel_linea_seleccionada;
	
								//printf ("Seleccionamos item %d\n",linea_final);
                                filesel_linea_seleccionada=linea_final;

								ventana->cursor_line +=filesel_linea_seleccionada;
                                menu_speech_tecla_pulsada=1;
								return 1;
                            }
                            else {
                                //printf ("Cursor mas alla del ultimo item\n");
                            }

	return 0;

}

void menu_filesel_recent_files_clear(MENU_ITEM_PARAMETERS)
{
	last_filesused_clear();
	menu_generic_message_splash("Clear List","OK. List cleared");
}

char *menu_filesel_recent_files(void)
{
	menu_item *array_menu_recent_files;
    menu_item item_seleccionado;
    int retorno_menu;


	menu_add_item_menu_inicial(&array_menu_recent_files,"",MENU_OPCION_UNASSIGNED,NULL,NULL);

    char string_last_file_shown[29];


    int i;
	int hay_alguno=0;
    for (i=0;i<MAX_LAST_FILESUSED;i++) {
		if (last_files_used_array[i][0]!=0) {

			//Mostrar solo nombre de archivo sin path
			char archivo_sin_dir[PATH_MAX];
			util_get_file_no_directory(last_files_used_array[i],archivo_sin_dir);

            menu_tape_settings_trunc_name(archivo_sin_dir,string_last_file_shown,29);
			menu_add_item_menu_format(array_menu_recent_files,MENU_OPCION_NORMAL,NULL,NULL,string_last_file_shown);

			//Agregar tooltip con ruta entera
			menu_add_item_menu_tooltip(array_menu_recent_files,last_files_used_array[i]);

			hay_alguno=1;
		}
	}

	if (!hay_alguno) menu_add_item_menu_format(array_menu_recent_files,MENU_OPCION_NORMAL,NULL,NULL,"<Empty List>");

	menu_add_item_menu(array_menu_recent_files,"",MENU_OPCION_SEPARADOR,NULL,NULL);
	menu_add_item_menu_format(array_menu_recent_files,MENU_OPCION_NORMAL,menu_filesel_recent_files_clear,NULL,"Clear List");

    menu_add_item_menu(array_menu_recent_files,"",MENU_OPCION_SEPARADOR,NULL,NULL);
    menu_add_ESC_item(array_menu_recent_files);

    retorno_menu=menu_dibuja_menu(&menu_recent_files_opcion_seleccionada,&item_seleccionado,array_menu_recent_files,"Recent files" );

    if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {

		//Seleccion de borrar lista
		if (item_seleccionado.menu_funcion!=NULL) {
            item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
        }

		//Seleccion de un archivo
		else if (hay_alguno) {
        	int indice=menu_recent_files_opcion_seleccionada;

//lastfilesuser_scrolldown
//Quitar el que hay ahi, desplazando hacia abajo y ponerlo arriba del todo
//Copiarlo temporamente a otro sitio
		char buffer_recent[PATH_MAX];
		strcpy(buffer_recent,last_files_used_array[indice]);

		//Movemos el trozo desde ahi hasta arriba
		lastfilesuser_scrolldown(0,indice);

		//Y lo metemos arriba del todo
		strcpy(last_files_used_array[0],buffer_recent);

		//Por tanto el indice final sera 0
		indice=0;

		//Y cursor ponerloa arriba entonces tambien
		menu_recent_files_opcion_seleccionada=0;

			debug_printf (VERBOSE_INFO,"Returning recent file %s",last_files_used_array[indice]);
			return last_files_used_array[indice];
		}
	}

	return NULL;

}


int menu_filesel_cambiar_unidad_o_volumen(void)
{

	int releer_directorio=0;

#ifdef MINGW
	//Mostrar selector de unidades
						char letra=menu_filesel_cambiar_unidad_windows();
					//printf ("letra: %d\n",letra);
					if (letra!=0) {
						char directorio[3];
						sprintf (directorio,"%c:",letra);

						//printf ("Changing to unit %s\n",directorio);

						menu_filesel_chdir(directorio);
						releer_directorio=1;
						
					}
#else

//Cambiar a ruta /media (en linux) o a /Volumes en Mac

	#if defined(__APPLE__)

//En Mac
		menu_filesel_chdir("/Volumes");
		releer_directorio=1;

	#else

//En Linux

		menu_filesel_chdir("/media");
		releer_directorio=1;	

	#endif


#endif

	return releer_directorio;
}

//Ultimas coordenadas de filesel
int last_filesel_ventana_x=0;
int last_filesel_ventana_y=0;
int last_filesel_ventana_visible_ancho=FILESEL_INICIAL_ANCHO;
int last_filesel_ventana_visible_alto=FILESEL_INICIAL_ALTO;
int filesel_primera_vez=1;

void menu_filesel_save_params_window(zxvision_window *ventana)
{
				//Guardar anteriores tamaños ventana
			/*last_filesel_ventana_x=ventana->x;
			last_filesel_ventana_y=ventana->y;

			last_filesel_ventana_visible_ancho=ventana->visible_width;
			last_filesel_ventana_visible_alto=ventana->visible_height;*/

	//util_add_window_geometry("filesel",ventana->x,ventana->y,ventana->visible_width,ventana->visible_height);
	util_add_window_geometry_compact(ventana);
}


zxvision_window *menu_filesel_overlay_window;

int menu_filesel_overlay_valor_contador_segundo_anterior;


//Estructura de memoria para mostrar previews. coordenadas, color

struct s_filesel_preview_mem {
	int color;
};

//Datos del ultimo preview mostrado

int menu_filesel_overlay_last_preview_width=0;
int menu_filesel_overlay_last_preview_height=0;

//Puntero a la previsualizacion
struct s_filesel_preview_mem *menu_filesel_overlay_last_preview_memory=NULL;


void menu_filesel_overlay_assign_memory_preview(int width,int height)
{

	//Liberar si conviene
	if (menu_filesel_overlay_last_preview_memory==NULL) free(menu_filesel_overlay_last_preview_memory);


	int elementos=width*height;

	int total_mem=elementos*sizeof(struct s_filesel_preview_mem);

	menu_filesel_overlay_last_preview_memory=malloc(total_mem);

	if (menu_filesel_overlay_last_preview_memory==NULL) cpu_panic("Cannot allocate memory for image preview");

	menu_filesel_overlay_last_preview_width=width;
	menu_filesel_overlay_last_preview_height=height;
}


void menu_filesel_overlay_draw_preview(void)
{
	//No hay imagen asignada?
	if (menu_filesel_overlay_last_preview_memory==NULL) return;	

	//Pero tiene tamanyo?
	if (menu_filesel_overlay_last_preview_width<=0 || menu_filesel_overlay_last_preview_height<=0) return;


		//printf("putpixel preview\n");

		//zxvision_putpixel
        //Ancho de zona waveform variable segun el tamanyo de ventana
        int ancho_ventana=menu_filesel_overlay_window->visible_width-1;

		int alto_ventana=menu_filesel_overlay_window->visible_height-2;		


		//Preview pegar a la derecha
		int xorigen=ancho_ventana-menu_filesel_overlay_last_preview_width/menu_char_width;

		//Restar barra desplazamiento, texto <dir> y mas margen
        int margen_x_coord=7;
		xorigen=xorigen-margen_x_coord;


		int yorigen;


		/*
		int zxvision_get_filesel_alto_dir(zxvision_window *ventana)
{
	return ventana->visible_height - ventana->upper_margin - ventana->lower_margin - 2;
}

cursor:
filesel_linea_seleccionada   

alto:
zxvision_get_filesel_alto_dir(ventana)-1
		*/

		//Con esto se clavaria justo en el cursor
		//yorigen=menu_filesel_overlay_window->upper_margin+filesel_linea_seleccionada;

		int alto_zona_dir=zxvision_get_filesel_alto_dir(menu_filesel_overlay_window)-1;

		//Si cursor esta por arriba
		if (filesel_linea_seleccionada<=alto_zona_dir/2+1) {
			//El preview esta abajo
			yorigen=alto_zona_dir-menu_filesel_overlay_last_preview_height/8+1;
		}

		else {
			//Si no, arriba
			yorigen=0;
		}


		//Sumar zona de cabeceras
		yorigen +=menu_filesel_overlay_window->upper_margin;

		//Y ver que no se salga por la izquierda por ejemplo
		if (xorigen<0 || yorigen<0) return;

		//ver que haya un tamaño suficiente de ventana
        //printf("ventana ancho %d alto %d\n",ancho_ventana,alto_ventana);

        if (ancho_ventana<31) {
            //debug_printf(VERBOSE_DEBUG,"Fileselector width size too small: %d",ancho_ventana);
            return;
        }

        //En caso de tener un ancho no muy grande, desplazamos el preview a la derecha quitando el margen
        if (ancho_ventana<38) {
            //debug_printf(VERBOSE_DEBUG,"Setting preview to the right as we have a small window");
            xorigen=xorigen+margen_x_coord-1; //-1 de la barra de progreso
        }
        

		//Sumar scroll ventana
		xorigen +=menu_filesel_overlay_window->offset_x;
		yorigen +=menu_filesel_overlay_window->offset_y;


		ancho_ventana *=menu_char_width;
		alto_ventana *=8;
		xorigen *=menu_char_width;
		yorigen *=8;


		int x,y;
		int contador=0;

		for (y=0;y<menu_filesel_overlay_last_preview_height;y++) {
			for (x=0;x<menu_filesel_overlay_last_preview_width;x++) {
			
				int color=menu_filesel_overlay_last_preview_memory[contador].color;
				contador++;

				//Por si acaso comprobar rangos
				if (color<0 || color>=EMULATOR_TOTAL_PALETTE_COLOURS) color=0;
				zxvision_putpixel(menu_filesel_overlay_window,xorigen+x,yorigen+y,color);
			}
		}

		//Le pongo recuadro en el mismo tamaño del preview
		int color_recuadro=ESTILO_GUI_PAPEL_TITULO;

		//Horizontal
		for (x=0;x<menu_filesel_overlay_last_preview_width;x++) {
			zxvision_putpixel(menu_filesel_overlay_window,xorigen+x,yorigen,color_recuadro);
			zxvision_putpixel(menu_filesel_overlay_window,xorigen+x,yorigen+menu_filesel_overlay_last_preview_height-1,color_recuadro);
		}

		//Vertical
		for (y=0;y<menu_filesel_overlay_last_preview_height;y++) {
			zxvision_putpixel(menu_filesel_overlay_window,xorigen,yorigen+y,color_recuadro);
			zxvision_putpixel(menu_filesel_overlay_window,xorigen+menu_filesel_overlay_last_preview_width-1,yorigen+y,color_recuadro);
		}

		//ponerle sombreado

		int offset_sombra=4;

		int grosor_sombra=4;

		int color_sombra=7;
		//TODO: quizar buscar mejor color o hacerlo dependiendo del tema

		//Vertical
		for (y=offset_sombra;y<menu_filesel_overlay_last_preview_height+grosor_sombra;y++) {
			int i;
			for (i=0;i<grosor_sombra;i++) 
			{
				zxvision_putpixel(menu_filesel_overlay_window,xorigen+menu_filesel_overlay_last_preview_width+i,yorigen+y,color_sombra);
			}
		}

		//Horizontal
		for (x=offset_sombra;x<menu_filesel_overlay_last_preview_width+grosor_sombra;x++) {
			int i;
			for (i=0;i<grosor_sombra;i++) 
			{
				zxvision_putpixel(menu_filesel_overlay_window,xorigen+x,yorigen+menu_filesel_overlay_last_preview_height+i,color_sombra);
			}
		}		
}


//Reduce una imagen de un buffer , monocroma, a la mitad con destino en preview
//Entrada: colores son 0  o 1
//Salida: colores son 7 o 0
void menu_filesel_preview_reduce_monochome(int *buffer_intermedio,int ancho, int alto)
{


	int x,y;

	int offset_final=0;

	for (y=0;y<alto;y+=2) {
		for (x=0;x<ancho;x+=2) {

			int offset_orig;
			offset_orig=y*ancho+x;
			int color1=buffer_intermedio[offset_orig];

			offset_orig=y*ancho+x+1;
			int color2=buffer_intermedio[offset_orig];

			offset_orig=(y*ancho+1)+x;
			int color3=buffer_intermedio[offset_orig];

			offset_orig=(y*ancho+1)+x+1;
			int color4=buffer_intermedio[offset_orig];

			int suma=color1+color2+color3+color4;

			//maximo sera 4

			int color_final=(suma>2 ? 0 : 7);

			//int offset_final=(y/2)*ancho_final+x/2;

			//buffer_intermedio[offset_final++]=color_final;

			menu_filesel_overlay_last_preview_memory[offset_final++].color=color_final;

		}
	}


}


//Reduce una imagen de un buffer , color, a la mitad con destino en preview
void menu_filesel_preview_reduce_scr_color(int *buffer_intermedio,int ancho, int alto)
{


	int x,y;

	int offset_final=0;



	for (y=0;y<alto;y+=2) {
		for (x=0;x<ancho;x+=2) {

			int colores_cuadricula[4];

			//Sacar los 4 colores de la cuadricula de 2x2
			int offset_orig;
			offset_orig=y*ancho+x;
			colores_cuadricula[0]=buffer_intermedio[offset_orig];

			offset_orig=y*ancho+x+1;
			colores_cuadricula[1]=buffer_intermedio[offset_orig];

			offset_orig=(y*ancho+1)+x;
			colores_cuadricula[2]=buffer_intermedio[offset_orig];

			offset_orig=(y*ancho+1)+x+1;
			colores_cuadricula[3]=buffer_intermedio[offset_orig];



			//Dado que partimos de una pantalla de spectrum, en una cuadricula de 2x2 habran como mucho 2 colores diferentes
			//Ver cual de los dos se repite mas

			//Asumimos el primer color, para simplificar la comparacion mas abajo
			int color_final1=colores_cuadricula[0];
			//Segundo color inicialmente a nada valido
			int color_final2=-1;

			int veces_color_final1=0;
			int veces_color_final2=0;		

			int i;

			for (i=0;i<3;i++) {	

				if (colores_cuadricula[i]==color_final1) {
					veces_color_final1++;
				}
				else {
					color_final2=colores_cuadricula[i];
					veces_color_final2++;
				}

			}
									

			int color_final;

			if (veces_color_final1>veces_color_final2) color_final=color_final1;
			else color_final=color_final2;

			menu_filesel_overlay_last_preview_memory[offset_final++].color=color_final;

		}
	}


}

void menu_filesel_preview_render_scr(char *archivo_scr)
{
			//printf("es pantalla\n");

	//Si no existe archivo, liberar preview
	if (!si_existe_archivo(archivo_scr)) {
		debug_printf(VERBOSE_DEBUG,"File SCR %s does not exist",archivo_scr);
		menu_filesel_overlay_last_preview_width=0;
		menu_filesel_overlay_last_preview_height=0;
		return;	
	}

	

		//Leemos el archivo en memoria


		debug_printf(VERBOSE_DEBUG,"Rendering SCR");  

		//buffer lectura archivo
		z80_byte *buf_pantalla;

		buf_pantalla=malloc(6912);

		if (buf_pantalla==NULL) cpu_panic("Can not allocate buffer for screen read");

		int leidos=lee_archivo(archivo_scr,(char *)buf_pantalla,6912);

		if (leidos<=0) return;

		//fread(buf_pantalla,1,6912,ptr_scrfile);

		//fclose(ptr_scrfile);



		//Asignamos primero buffer intermedio
		int *buffer_intermedio;

		int ancho=256;
		int alto=192;


		int elementos=ancho*alto;

		buffer_intermedio=malloc(sizeof(int)*elementos);

		if (buffer_intermedio==NULL)  cpu_panic("Cannot allocate memory for reduce buffer");					  
		

		int x,y,bit_counter;

		z80_int offset_lectura=0;
		for (y=0;y<192;y++) {
			for (x=0;x<32;x++) {
				z80_byte leido;
				int offset_orig=screen_addr_table[y*32+x];
				//fread(&leido,1,1,ptr_scrfile);
				leido=buf_pantalla[offset_orig];

				//int xdestino,ydestino;

				//esta funcion no es muy rapida pero....
				//util_spectrumscreen_get_xy(offset_lectura,&xdestino,&ydestino);

				offset_lectura++;

				int offset_destino=y*256+x*8;

				int tinta;
				int papel;

				z80_byte atributo;

				int pos_attr;

				//pos_attr=(ydestino/8)*32+(xdestino/8);

				pos_attr=6144+((y/8)*32)+x;
				//printf("%d\n",pos_attr);

				atributo=buf_pantalla[pos_attr];

				//atributo=56;

				tinta=(atributo)&7;
				papel=(atributo>>3)&7;

				if (atributo & 64) {
					tinta +=8;
					papel +=8;
				}

				

				for (bit_counter=0;bit_counter<8;bit_counter++) {
					
					//de momento solo 0 o 1
					int color=(leido & 128 ? tinta : papel);

					
					//menu_filesel_overlay_last_preview_memory[offset].color=color;

					buffer_intermedio[offset_destino+bit_counter]=color;
					leido=leido << 1;
				}
			}
		}
		


		free(buf_pantalla);

		//Reducir a 128x96
		menu_filesel_overlay_assign_memory_preview(128,96);							

		//menu_filesel_preview_reduce_monochome(buffer_intermedio,256,192);

		menu_filesel_preview_reduce_scr_color(buffer_intermedio,256,192);

		free(buffer_intermedio);



}


char menu_filesel_last_preview_file[PATH_MAX]="";


//Overlay para mostrar los previews
void menu_filesel_overlay(void)
{
normal_overlay_texto_menu();


}

//Retorna 1 si seleccionado archivo. Retorna 0 si sale con ESC
//Si seleccionado archivo, lo guarda en variable *archivo
//Si sale con ESC, devuelve en menu_filesel_last_directory_seen ultimo directorio
int menu_filesel(char *titulo,char *filtros[],char *archivo)
{

	menu_reset_counters_tecla_repeticion();

	int tecla;


	filesel_zona_pantalla=1;

	getcwd(filesel_directorio_inicial,PATH_MAX);


    //printf ("confirm\n");

	//printf ("antes menu_espera_no_tecla en menu filesel\n");

	menu_espera_no_tecla();

	//printf ("despues menu_espera_no_tecla en menu filesel\n");
    	
	zxvision_window ventana_filesel;
	zxvision_window *ventana;

	//Inicialmente a NULL
	ventana=NULL;


	//guardamos filtros originales
	filesel_filtros_iniciales=filtros;



    filtros_todos_archivos[0]="";
    filtros_todos_archivos[1]=0;

	filesel_filtros=filtros;

	filesel_item *item_seleccionado;

	int aux_pgdnup;
	//menu_active_item_primera_vez=1;

	//Inicializar mouse wheel a 0, por si acaso
	mouse_wheel_vertical=mouse_wheel_horizontal=0;


//Esto lo hago para poder debugar facilmente la opcion de cambio de unidad
/*#ifdef MINGW
	int we_are_windows=1;
#else
	int we_are_windows=0;
	
#endif*/


	do {
		menu_active_item_primera_vez=1;
		filesel_linea_seleccionada=0;
		filesel_archivo_seleccionado=0;
		//leer todos archivos
		int ret=menu_filesel_readdir();
		if (ret) {
			//Error leyendo directorio
			//restauramos modo normal de texto de menu
     		set_menu_overlay_function(normal_overlay_texto_menu);
			cls_menu_overlay();
			menu_espera_no_tecla();
			menu_filesel_chdir(filesel_directorio_inicial);
			menu_filesel_free_mem();
			zxvision_destroy_window(ventana);
			return 0;
                                		
		}


		//printf ("Total archivos en directorio: %d\n",filesel_total_items);
		//printf ("despues leer directorio\n");
		//Crear ventana. Si ya existia, borrarla
		if (ventana!=NULL) {
			//printf ("Destroy previous filesel window\n");
			cls_menu_overlay();

			//Guardar anteriores tamaños ventana
			menu_filesel_save_params_window(ventana);

			//restauramos modo normal de texto de menu
     		set_menu_overlay_function(normal_overlay_texto_menu);

			zxvision_destroy_window(ventana);
		}
		ventana=&ventana_filesel;

		int alto_total=filesel_total_items+ZXVISION_FILESEL_INITIAL_MARGIN; //Sumarle las leyendas, etc
		

		//Usar ultimas coordenadas y tamaño, sin comprobar rango de maximo ancho y alto 32x24
		//Si no hay ultimas, poner las de por defecto
		if (!util_find_window_geometry("filesel",&last_filesel_ventana_x,&last_filesel_ventana_y,&last_filesel_ventana_visible_ancho,&last_filesel_ventana_visible_alto)) {
			last_filesel_ventana_x=FILESEL_INICIAL_X;
			last_filesel_ventana_y=FILESEL_INICIAL_Y;
			last_filesel_ventana_visible_ancho=FILESEL_INICIAL_ANCHO;
			last_filesel_ventana_visible_alto=FILESEL_INICIAL_ALTO;	
		}


		//zxvision_new_window_check_range(&last_filesel_ventana_x,&last_filesel_ventana_y,&last_filesel_ventana_visible_ancho,&last_filesel_ventana_visible_alto);
		//zxvision_new_window_no_check_range(ventana,last_filesel_ventana_x,last_filesel_ventana_y,last_filesel_ventana_visible_ancho,last_filesel_ventana_visible_alto,last_filesel_ventana_visible_ancho-1,alto_total,titulo);
		zxvision_new_window_nocheck_staticsize(ventana,last_filesel_ventana_x,last_filesel_ventana_y,last_filesel_ventana_visible_ancho,last_filesel_ventana_visible_alto,last_filesel_ventana_visible_ancho-1,alto_total,titulo);

	    ventana->upper_margin=4;
	    ventana->lower_margin=4;
		ventana->visible_cursor=1;
		strcpy(ventana->geometry_name,"filesel");

		if (menu_filesel_show_utils.v) {
			//Activar los hotkeys desde raton en el caso de file utilities
			ventana->can_mouse_send_hotkeys=1;
		}

		zxvision_draw_window(ventana);


		//Overlay para los previews. Siempre que tengamos video driver completo
        if (si_complete_video_driver() ) {
                
			if (menu_filesel_show_previews.v) {
				menu_filesel_overlay_window=ventana;
				set_menu_overlay_function(menu_filesel_overlay);
			}

		}

		zxvision_menu_filesel_print_filters(ventana,filesel_filtros);
		zxvision_menu_filesel_print_text_contents(ventana);
		zxvision_menu_filesel_print_legend(ventana);
		int releer_directorio=0;



		//El menu_print_dir aqui no hace falta porque ya entrara en el switch (filesel_zona_pantalla) inicialmente cuando filesel_zona_pantalla vale 1
		//menu_print_dir(filesel_archivo_seleccionado);

		zxvision_draw_window_contents(ventana);

		menu_refresca_pantalla();


		if (menu_filesel_posicionar_archivo.v) {
			zxvision_menu_filesel_localiza_archivo(ventana,menu_filesel_posicionar_archivo_nombre);

			menu_filesel_posicionar_archivo.v=0;
		}


		do {
			//printf ("\nReleer directorio\n");
			//printf ("cursor_line: %d filesel_linea_seleccionada: %d filesel_archivo_seleccionado %d\n",
			//	ventana->cursor_line,filesel_linea_seleccionada,filesel_archivo_seleccionado);

			//printf("filesel_linea_seleccionada: %d\n",filesel_linea_seleccionada);


			//printf ("(FILESEL_ALTO-10): %d zxvision_get_filesel_alto_dir: %d\n",(FILESEL_ALTO-10),zxvision_get_filesel_alto_dir(ventana) );

			switch (filesel_zona_pantalla) {
				case 0:
				//zona superior de nombre de archivo
				ventana->visible_cursor=0;
		                zxvision_menu_print_dir(filesel_archivo_seleccionado,ventana);
				zxvision_draw_window_contents(ventana);
                		//para que haga lectura del edit box
		                menu_speech_tecla_pulsada=0;

				int ancho_mostrado=ventana->visible_width-6-2;
				if (ancho_mostrado<2) {
					//La ventana es muy pequeña como para editar
					menu_reset_counters_tecla_repeticion();
					filesel_zona_pantalla=1;
					//no releer todos archivos
					menu_speech_tecla_pulsada=1;					

				}

				else {


				tecla=zxvision_scanf(ventana,filesel_nombre_archivo_seleccionado,PATH_MAX,ancho_mostrado,7,1,0);
				//); //6 ocupa el texto "File: "

				if (tecla==15) {
					//printf ("TAB. siguiente seccion\n");
					menu_reset_counters_tecla_repeticion();
					filesel_zona_pantalla=1;
					//no releer todos archivos
					menu_speech_tecla_pulsada=1;
				}

				//ESC
                if (tecla==2) {
                	menu_filesel_exist_ESC();
					//restauramos modo normal de texto de menu
     				set_menu_overlay_function(normal_overlay_texto_menu);

					zxvision_destroy_window(ventana);
                    return 0;
				}

				if (tecla==13) {

					//Si es Windows y se escribe unidad: (ejemplo: "D:") hacer chdir
					int unidadwindows=0;
#ifdef MINGW
					if (filesel_nombre_archivo_seleccionado[0] &&
						filesel_nombre_archivo_seleccionado[1]==':' &&
						filesel_nombre_archivo_seleccionado[2]==0 )
						{
						debug_printf (VERBOSE_INFO,"%s is a Windows drive",filesel_nombre_archivo_seleccionado);
						unidadwindows=1;
					}
#endif


					//si es directorio, cambiamos
					struct stat buf_stat;
					int stat_valor;
					stat_valor=stat(filesel_nombre_archivo_seleccionado, &buf_stat);
					if (
						(stat_valor==0 && S_ISDIR(buf_stat.st_mode) ) ||
						(unidadwindows)
						) {
						debug_printf (VERBOSE_DEBUG,"%s Is a directory or windows drive. Change",filesel_nombre_archivo_seleccionado);
                                                menu_filesel_chdir(filesel_nombre_archivo_seleccionado);
						menu_filesel_free_mem();
                                                releer_directorio=1;
						filesel_zona_pantalla=1;

					        //Decir directorio activo
						//Esperar a liberar tecla si no la tecla invalida el speech
						menu_espera_no_tecla();


					}


					//sino, devolvemos nombre con path, siempre que extension sea conocida
					else {
                    	cls_menu_overlay();
                        menu_espera_no_tecla();

						if (menu_avisa_si_extension_no_habitual(filtros,filesel_nombre_archivo_seleccionado)) {

                        //unimos directorio y nombre archivo. siempre que inicio != '/'
						if (filesel_nombre_archivo_seleccionado[0]!='/') {
                        	getcwd(archivo,PATH_MAX);
                            sprintf(&archivo[strlen(archivo)],"/%s",filesel_nombre_archivo_seleccionado);
						}

						else sprintf(archivo,"%s",filesel_nombre_archivo_seleccionado);


                        menu_filesel_chdir(filesel_directorio_inicial);
						menu_filesel_free_mem();

						//return menu_avisa_si_extension_no_habitual(filtros,archivo);
						//restauramos modo normal de texto de menu
			     		set_menu_overlay_function(normal_overlay_texto_menu);

						cls_menu_overlay();
						zxvision_destroy_window(ventana);
						last_filesused_insert(archivo);
						return 1;

						}

						else {
							//Extension no conocida. No modificar variable archivo
							//printf ("Unknown extension. Do not modify archivo. Contents: %s\n",archivo);
							//restauramos modo normal de texto de menu
     						set_menu_overlay_function(normal_overlay_texto_menu);

							cls_menu_overlay();
							zxvision_destroy_window(ventana);
							return 0;
						}
						



						//Volver con OK
                        //return 1;

					}
				}

				}

				break;
			
			case 1:
				//zona selector de archivos

				debug_printf (VERBOSE_DEBUG,"Read directory. menu_speech_tecla_pulsada=%d",menu_speech_tecla_pulsada);
				ventana->visible_cursor=1;
				zxvision_menu_print_dir(filesel_archivo_seleccionado,ventana);
				zxvision_draw_window_contents(ventana);
				//Para no releer todas las entradas
				menu_speech_tecla_pulsada=1;


				tecla=zxvision_common_getkey_refresh();
				//printf ("Despues lee tecla\n");


				//Si se ha pulsado boton de raton
                                if (mouse_left) {
					//printf ("Pulsado boton raton izquierdo\n");

					 //Si en linea de "File"
					menu_filesel_change_zone_if_clicked(ventana,&filesel_zona_pantalla,&tecla);
                                        /*if (menu_mouse_y==2 && menu_mouse_x<ventana->visible_width-1) {
						printf ("Pulsado zona File\n");
                                                                menu_reset_counters_tecla_repeticion();
                                                                filesel_zona_pantalla=0;
                                                                tecla=0;
                                        }*/

					if (si_menu_mouse_en_ventana() && zxvision_si_mouse_zona_archivos(ventana) ) {
						//Ubicamos cursor donde indica raton
						if (menu_filesel_set_cursor_at_mouse(ventana)) {
							//Como pulsar enter
							tecla=13;
						}
					}
				}


				//Si se ha movido raton. Asumimos que ha vuelto de leer tecla, tecla=0 y no se ha pulsado mouse
				if (!tecla && !mouse_left) {
				 //if (mouse_movido) {
                    //printf ("mouse x: %d y: %d menu mouse x: %d y: %d\n",mouse_x,mouse_y,menu_mouse_x,menu_mouse_y);
                    //printf ("ventana x %d y %d ancho %d alto %d\n",ventana_x,ventana_y,ventana_ancho,ventana_alto);
                    if (si_menu_mouse_en_ventana() ) {
                        //printf ("dentro ventana\n");
                        //Ver en que zona esta
                        
                        if (zxvision_si_mouse_zona_archivos(ventana)) {
							menu_filesel_set_cursor_at_mouse(ventana);						

                        }

                        else if (menu_mouse_y==(ventana->visible_height-4)+1) {
                            //En la linea de filtros
                            //nada en especial
                            //printf ("En linea de filtros\n");
                        }
                    }
                else {
                    //printf ("fuera ventana\n");
                }

        }



				switch (tecla) {
					//abajo
					case 10:
						zxvision_menu_filesel_cursor_abajo(ventana);
						//Para no releer todas las entradas
						menu_speech_tecla_pulsada=1;
					break;

					//arriba
					case 11:
						zxvision_menu_filesel_cursor_arriba(ventana);
						//Para no releer todas las entradas
						menu_speech_tecla_pulsada=1;
					break;

					//PgDn
					case 25:
						for (aux_pgdnup=0;aux_pgdnup<zxvision_get_filesel_alto_dir(ventana);aux_pgdnup++)
							zxvision_menu_filesel_cursor_abajo(ventana);
						//releer todas entradas
						menu_speech_tecla_pulsada=0;
						//y decir selected item
						menu_active_item_primera_vez=1;
                    break;

					//PgUp
					case 24:
						for (aux_pgdnup=0;aux_pgdnup<zxvision_get_filesel_alto_dir(ventana);aux_pgdnup++)
							zxvision_menu_filesel_cursor_arriba(ventana);
						//releer todas entradas
						menu_speech_tecla_pulsada=0;
						//y decir selected item
						menu_active_item_primera_vez=1;
                    break;


					case 15:
					//tabulador
						menu_reset_counters_tecla_repeticion();
						if (menu_filesel_show_utils.v==0) filesel_zona_pantalla=2;
						else filesel_zona_pantalla=0; //Si hay utils, cursor se va arriba
					break;

					//ESC
					case 2:
						//meter en menu_filesel_last_directory_seen nombre directorio
						//getcwd(archivo,PATH_MAX);
						getcwd(menu_filesel_last_directory_seen,PATH_MAX);
						//printf ("salimos con ESC. nombre directorio: %s\n",archivo);
                        menu_filesel_exist_ESC();

                        //Guardamos geometria al pulsar Escape
                        menu_filesel_save_params_window(ventana);

						//restauramos modo normal de texto de menu
			     		set_menu_overlay_function(normal_overlay_texto_menu);

						zxvision_destroy_window(ventana);
                        return 0;

					break;

					//Expandir archivos
					case 32:

                                                item_seleccionado=menu_get_filesel_item(filesel_archivo_seleccionado+filesel_linea_seleccionada);
                                                menu_reset_counters_tecla_repeticion();

                                                //printf ("despues de get filesel item. item_seleccionado=%p\n",item_seleccionado);

                                                if (item_seleccionado==NULL) {
                                                        //Esto pasa en las carpetas vacias, como /home en Mac OS
                                                                        menu_filesel_exist_ESC();
																		//restauramos modo normal de texto de menu
																		set_menu_overlay_function(normal_overlay_texto_menu);


																		zxvision_destroy_window(ventana);
                                                                        return 0;


                                                }

						if (get_file_type(item_seleccionado->d_name)==2) {
							debug_printf(VERBOSE_INFO,"Can't expand directories");
						}

						else {
								debug_printf(VERBOSE_DEBUG,"Expanding file %s",item_seleccionado->d_name);
                                                                {
									//TODO: Si lanzo este warning se descuadra el dibujado de ventana
									//menu_warn_message("Don't know how to expand that file");
									debug_printf(VERBOSE_INFO,"Don't know how to expand that file");
                                                                }
						}


					break;

					case 13:


						//si seleccion es directorio
						item_seleccionado=menu_get_filesel_item(filesel_archivo_seleccionado+filesel_linea_seleccionada);
						menu_reset_counters_tecla_repeticion();

						//printf ("despues de get filesel item. item_seleccionado=%p\n",item_seleccionado);

						if (item_seleccionado==NULL) {
							//Esto pasa en las carpetas vacias, como /home en Mac OS
                                                                        menu_filesel_exist_ESC();
																		//restauramos modo normal de texto de menu
     																	set_menu_overlay_function(normal_overlay_texto_menu);

																		zxvision_destroy_window(ventana);
                                                                        return 0;


						}

						if (get_file_type(item_seleccionado->d_name)==2) {
							debug_printf (VERBOSE_DEBUG,"Is a directory. Change");
							char *directorio_a_cambiar;

							//suponemos esto:
							directorio_a_cambiar=item_seleccionado->d_name;
							char last_directory[PATH_MAX];

							//si es "..", ver si directorio actual contiene archivo que indica ultimo directorio
							//en caso de descompresiones
							if (!strcmp(item_seleccionado->d_name,"..")) {
								debug_printf (VERBOSE_DEBUG,"Is directory ..");
								if (si_existe_archivo(MENU_LAST_DIR_FILE_NAME)) {
									debug_printf (VERBOSE_DEBUG,"Directory has file " MENU_LAST_DIR_FILE_NAME " Changing "
											"to previous directory");

									if (menu_filesel_read_file_last_dir(last_directory)==0) {
										debug_printf (VERBOSE_DEBUG,"Previous directory was: %s",last_directory);

										directorio_a_cambiar=last_directory;
									}

								}
							}

							debug_printf (VERBOSE_DEBUG,"Changing to directory %s",directorio_a_cambiar);

							menu_filesel_chdir(directorio_a_cambiar);


							menu_filesel_free_mem();
							releer_directorio=1;

						        //Decir directorio activo
							//Esperar a liberar tecla si no la tecla invalida el speech
							menu_espera_no_tecla();

						}

						else {

							//Si seleccion es archivo comprimido
							if (menu_util_file_is_compressed(item_seleccionado->d_name) ) {
								debug_printf (VERBOSE_DEBUG,"Is a compressed file");

                                                                {
									menu_filesel_exist_ESC();
									//restauramos modo normal de texto de menu
     								set_menu_overlay_function(normal_overlay_texto_menu);

									zxvision_destroy_window(ventana);
									return 0;
								}

							}

							else {
								//Enter. No es directorio ni archivo comprimido
								//Si estan las file utils, enter no hace nada
								if (menu_filesel_show_utils.v==0) { 

					                cls_menu_overlay();
        	                        menu_espera_no_tecla();

									if (menu_avisa_si_extension_no_habitual(filtros,filesel_nombre_archivo_seleccionado)) {

									//unimos directorio y nombre archivo
									getcwd(archivo,PATH_MAX);
									sprintf(&archivo[strlen(archivo)],"/%s",item_seleccionado->d_name);

									menu_filesel_chdir(filesel_directorio_inicial);
									menu_filesel_free_mem();

									//return menu_avisa_si_extension_no_habitual(filtros,archivo);
									//Guardar anteriores tamaños ventana
									menu_filesel_save_params_window(ventana);

									//restauramos modo normal de texto de menu
									set_menu_overlay_function(normal_overlay_texto_menu);

									cls_menu_overlay();
									zxvision_destroy_window(ventana);
									last_filesused_insert(archivo);
									return 1;

									}

                                    else {
                                                        //Extension no conocida. No modificar variable archivo
                                                        //printf ("Unknown extension. Do not modify archivo. Contents: %s\n",archivo);

														//restauramos modo normal de texto de menu
														set_menu_overlay_function(normal_overlay_texto_menu);

														cls_menu_overlay();
														zxvision_destroy_window(ventana);
                                                        return 0;
                                    }


									//Volver con OK
									//return 1;
								}
							}

						}
					break;
				}

				//entre a y z y numeros
				if ( (tecla>='a' && tecla<='z') || (tecla>='0' && tecla<='9') ) {
					zxvision_menu_filesel_localiza_letra(ventana,tecla);
				}


				if (tecla=='D') {
					releer_directorio=menu_filesel_cambiar_unidad_o_volumen();
					
				}

				if (tecla=='R') {	

					//Archivos recientes
					char *archivo_reciente=menu_filesel_recent_files();
					if (archivo_reciente!=NULL) {
						//printf ("Loading file %s\n",archivo_reciente);
						strcpy(archivo,archivo_reciente);

                                                                      menu_filesel_chdir(filesel_directorio_inicial);
                                                                        menu_filesel_free_mem();

                                                                        //return menu_avisa_si_extension_no_habitual(filtros,archivo);

																		//restauramos modo normal de texto de menu
																		set_menu_overlay_function(normal_overlay_texto_menu);


                                                                        cls_menu_overlay();
                                                                        zxvision_destroy_window(ventana);
                                                                        return 1;

					}
				}

				//Si esta filesel, opciones en mayusculas
				if (menu_filesel_show_utils.v) {
					
					if ( (tecla>='A' && tecla<='Z') ) {
						menu_espera_no_tecla();
						//TODO: Si no se pone espera_no_tecla,
						//al aparecer menu de, por ejemplo truncate, el texto se fusiona con el fondo de manera casi transparente,
						//como si no borrase el putpixel cache
						//esto también sucede en otras partes del código del menú pero no se por que es

						menu_reset_counters_tecla_repeticion();
						
						//Comun para acciones que usan archivo seleccionado
						if (tecla=='V' || tecla=='T' || tecla=='E' || tecla=='M' || tecla=='N' || tecla=='C' || tecla=='P' || tecla=='F' || tecla=='O' || tecla=='I') {
							
							//Obtener nombre del archivo al que se apunta
							char file_utils_file_selected[PATH_MAX]="";
							item_seleccionado=menu_get_filesel_item(filesel_archivo_seleccionado+filesel_linea_seleccionada);
							if (item_seleccionado!=NULL) {
								//Esto pasa en las carpetas vacias, como /home en Mac OS
									//unimos directorio y nombre archivo
									getcwd(file_utils_file_selected,PATH_MAX);
									sprintf(&file_utils_file_selected[strlen(file_utils_file_selected)],"/%s",item_seleccionado->d_name);								
								//Info para cualquier tipo de archivo
								if (tecla=='I') file_utils_info_file(file_utils_file_selected);

								//Si no es directorio
								if (get_file_type(item_seleccionado->d_name)!=2) {
									//unimos directorio y nombre archivo
									//getcwd(file_utils_file_selected,PATH_MAX);
									//sprintf(&file_utils_file_selected[strlen(file_utils_file_selected)],"/%s",item_seleccionado->d_name);
									
									//Truncate
									if (tecla=='T') {
										if (menu_confirm_yesno_texto("Truncate","Sure?")) util_truncate_file(file_utils_file_selected);
									}

									//Delete
									if (tecla=='E') {
										if (menu_confirm_yesno_texto("Delete","Sure?")) {
											util_delete(file_utils_file_selected);
											//unlink(file_utils_file_selected);
											releer_directorio=1;
										}

									}
									//Move
									if (tecla=='M') {
										file_utils_move_rename_copy_file(file_utils_file_selected,0);
										//Restaurar variables globales que se alteran al llamar al otro filesel
										//TODO: hacer que estas variables no sean globales sino locales de esta funcion menu_filesel
										filesel_filtros_iniciales=filtros;
										filesel_filtros=filtros;
		
										releer_directorio=1;

									}

									//Rename
									if (tecla=='N') {
										file_utils_move_rename_copy_file(file_utils_file_selected,1);
										releer_directorio=1;
									}

									//Filemem
									if (tecla=='F') {
										file_utils_file_mem_load(file_utils_file_selected);
									}
						

									//Copy
									if (tecla=='C') {
										file_utils_move_rename_copy_file(file_utils_file_selected,2);
										//Restaurar variables globales que se alteran al llamar al otro filesel
										//TODO: hacer que estas variables no sean globales sino locales de esta funcion menu_filesel
										filesel_filtros_iniciales=filtros;
										filesel_filtros=filtros;
										
										releer_directorio=1;
									}

									

								}
							}

							
						}

						//Mkdir
						if (tecla=='K') {
							char string_carpeta[200];
							string_carpeta[0]=0;
							menu_ventana_scanf("Folder name",string_carpeta,200);
							if (string_carpeta[0]) {
								menu_filesel_mkdir(string_carpeta);
								releer_directorio=1;
							}
						}


						//Paste text
						if (tecla=='P') {
							file_utils_paste_clipboard();
										
										
							releer_directorio=1;
						}
			
						

						//Redibujar ventana
						//releer_directorio=1;
						
						zxvision_menu_filesel_print_filters(ventana,filesel_filtros);
						zxvision_menu_filesel_print_legend(ventana);

						zxvision_menu_filesel_print_text_contents(ventana);
					}
					
				}

				//menu_espera_no_tecla();
				menu_espera_no_tecla_con_repeticion();




			break;

			case 2:
				//zona filtros
				ventana->visible_cursor=0;
                                zxvision_menu_print_dir(filesel_archivo_seleccionado,ventana);

                                //para que haga lectura de los filtros
                                menu_speech_tecla_pulsada=0;

				zxvision_menu_filesel_print_filters(ventana,filesel_filtros);
				zxvision_draw_window_contents(ventana);
	

				tecla=zxvision_common_getkey_refresh();


				if (menu_filesel_change_zone_if_clicked(ventana,&filesel_zona_pantalla,&tecla)) {
					zxvision_menu_filesel_print_filters(ventana,filesel_filtros);
                                         releer_directorio=1;

                                                menu_espera_no_tecla();
				}

		                //ESC
                                else if (tecla==2) {
                                                cls_menu_overlay();
                                                menu_espera_no_tecla();
                                                menu_filesel_chdir(filesel_directorio_inicial);
						menu_filesel_free_mem();

						//restauramos modo normal de texto de menu
						set_menu_overlay_function(normal_overlay_texto_menu);


						zxvision_destroy_window(ventana);
                                                return 0;
                                }

				//cambiar de zona con tab
				else if (tecla==15) {
					menu_reset_counters_tecla_repeticion();
					filesel_zona_pantalla=0;
					zxvision_menu_filesel_print_filters(ventana,filesel_filtros);
					//no releer todos archivos
					menu_speech_tecla_pulsada=1;
				}


				else {

					//printf ("conmutar filtros\n");
					if (tecla || (tecla==0 && mouse_left)) { 

						//conmutar filtros
						menu_filesel_switch_filters();

					        zxvision_menu_filesel_print_filters(ventana,filesel_filtros);
						releer_directorio=1;

						menu_espera_no_tecla();
					}
				}

			break;
			}

		} while (releer_directorio==0);
	} while (1);


	//Aqui no se va a llegar nunca


}



//Inicializar vacio
void last_filesused_clear(void)
{

	int i;
	for (i=0;i<MAX_LAST_FILESUSED;i++) {
		last_files_used_array[i][0]=0;
	}
}

//Desplazar hacia abajo desde posicion superior indicada. La posicion indicada sera un duplicado de la siguiente posicion por tanto
void lastfilesuser_scrolldown(int posicion_up,int posicion_down)
{
	int i;
	for (i=posicion_down;i>=posicion_up+1;i--) {
		strcpy(last_files_used_array[i],last_files_used_array[i-1]);
	}	
}

//Insertar entrada en last smartload
void last_filesused_insert(char *s)
{
	//Desplazar todos hacia abajo e insertar en posicion 0
	//Desde abajo a arriba

	//int i;
	/*for (i=MAX_LAST_FILESUSED-1;i>=1;i--) {
		strcpy(last_files_used_array[i],last_files_used_array[i-1]);
	}*/

	lastfilesuser_scrolldown(0,MAX_LAST_FILESUSED-1);


	//Meter en posicion 0
	strcpy(last_files_used_array[0],s);

	debug_printf (VERBOSE_INFO,"Inserting recent file %s at position 0",s);

	//printf ("Dump smartload:\n");

	//for (i=0;i<MAX_LAST_FILESUSED;i++) {
	//	printf ("Entry %d: [%s]\n",i,last_files_used_array[i]);
	//}
}


void menu_network_error(int error)
{
	menu_error_message(z_sock_get_error(error));
}
