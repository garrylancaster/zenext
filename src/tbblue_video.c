/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"



/*
   //es zona de vsync y borde superior
                                                                //Aqui el contador raster tiene valor (192+56 en adelante)
                                                                //contador de scanlines del core, entre 0 y screen_indice_inicio_pant ,
                                                                if (t_scanline<screen_indice_inicio_pant) {
                                                                        if (t_scanline==linea_raster-192-screen_total_borde_inferior) disparada_raster=1;
                                                                }

                                                                //Esto es zona de paper o borde inferior
                                                                //Aqui el contador raster tiene valor 0 .. <(192+56)
                                                                //contador de scanlines del core, entre screen_indice_inicio_pant y screen_testados_total
                                                                else {
                                                                        if (t_scanline-screen_indice_inicio_pant==linea_raster) disparada_raster=1;
                                                                }

https://github.com/z88dk/z88dk/blob/master/libsrc/_DEVELOPMENT/target/zxn/config/config_zxn_copper.m4#L74

# 50Hz                          60Hz
# Lines                         Lines
#
#   0-191  Display                0-191  Display
# 192-247  Bottom Border        192-223  Bottom Border
# 248-255  Vsync (interrupt)    224-231  Vsync (interrupt)
# 256-311 Top Border 232-261 Top Border


# Horizontally the display is the same in 50Hz or 60Hz mode but it
# varies by model.  It consists of 448 pixels (0-447) in 48k mode
# and 456 pixels (0-455) in 128k mode.  Grouped in eight pixels
# that's screen bytes 0-55 in 48k mode and 0-56 in 128k mode.
#
# 48k mode                      128k mode
# Bytes  Pixels                 Bytes  Pixels
#
#  0-31    0-255  Display        0-31    0-255  Display
# 32-39  256-319  Right Border  32-39  256-319  Right Border
# 40-51  320-415  HBlank        40-51  320-415  HBlank
# 52-55  416-447  Left Border   52-56  416-455  Left Border
#
# The ZXN Copper understands two operations:
#
# (1) Wait for a particular line (0-311 @ 50Hz or 0-261 @ 60Hz)
#     and a horizontal character position (0-55 or 0-56)
#
# (2) Write a value to a nextreg.

int screen_invisible_borde_superior;
//normalmente a 56.
int screen_borde_superior;

//estos dos anteriores se suman aqui. es 64 en 48k, y 63 en 128k. por tanto, uno de los dos valores vale 1 menos
int screen_indice_inicio_pant;

//suma del anterior+192
int screen_indice_fin_pant;

//normalmente a 56
int screen_total_borde_inferior;

zona borde invisible: 0 .. screen_invisible_borde_superior;
zona borde visible: screen_invisible_borde_superior .. screen_invisible_borde_superior+screen_borde_superior
zona visible pantalla: screen_indice_inicio_pant .. screen_indice_inicio_pant+192
zona inferior: screen_indice_fin_pant .. screen_indice_fin_pant+screen_total_borde_inferior 



*/

int tbblue_already_autoenabled_rainbow=0;


int tbblue_get_raster_line(void)
{
	/*
	Line 0 is first video line. In truth the line is the Y counter, Video is from 0 to 191, borders and hsync is >192
Same this page: http://www.zxdesign.info/vertcontrol.shtml


Row	Start	Row End	Length	Description
0		191	192	Video Display
192	247	56	Bottom Border
248	255	8	Vertical Sync
256	312	56	Top Border

*/
	if (t_scanline>=screen_indice_inicio_pant) return t_scanline-screen_indice_inicio_pant;
	else return t_scanline+192+screen_total_borde_inferior;


}


//Devuelve puntero a direccion de memoria donde esta el scanline en modo lores para direccion y
z80_byte *get_lores_pointer(int y)
{
	z80_byte *base_pointer;

	//Siempre saldra de ram 5
	base_pointer=tbblue_ram_memory_pages[5*2];	

	//128x96 one byte per pixel in left to right, top to bottom order so that the 
	//top half of the screen is in the first timex display file at 0x4000 
	//and the bottom half is in the second timex display file at 0x6000
	
	z80_int offset=0;

	//int yorig=y;

	const int mitad_alto=96/2;

	//Segunda mitad
	if (y>=mitad_alto) {
		//printf ("segundo bloque. y=%d offset=%d\n",y,offset);
		offset +=0x2000;
		y=y-mitad_alto;
	}

	//Sumamos desplazamiento por y
	offset +=y*128;

	//printf ("y: %d offset: %d\n",yorig,offset);

	base_pointer +=offset;

	return base_pointer;
}


int tbblue_get_current_raster_position(void)
{
	int raster;

	if (t_scanline<screen_invisible_borde_superior) {
		//En zona borde superior invisible (vsync)
		//Ajustamos primero a desplazamiento entre 0 y esa zona
		raster=t_scanline;

		//Sumamos offset de la zona raster
		
		raster +=192+screen_total_borde_inferior;
		//printf ("scanline: %d raster: %d\n",t_scanline,raster);
		return raster;
	}

	if (t_scanline<screen_indice_inicio_pant) {
		//En zona borde superior visible
		//Ajustamos primero a desplazamiento entre 0 y esa zona
		raster=t_scanline-screen_invisible_borde_superior;

		//Sumamos offset de la zona raster
		raster +=192+screen_total_borde_inferior+screen_invisible_borde_superior;

		//printf ("scanline: %d raster: %d\n",t_scanline,raster);
		return raster;
	}

	if (t_scanline<screen_indice_fin_pant) {
		//En zona visible pantalla
		//Ajustamos primero a desplazamiento entre 0 y esa zona
		raster=t_scanline-screen_indice_inicio_pant;

		//Sumamos offset de la zona raster
                raster +=0  ; //solo para que quede mas claro

		//printf ("scanline: %d raster: %d\n",t_scanline,raster);
                return raster;
        }

	//Caso final. Zona borde inferior
		//Ajustamos primero a desplazamiento entre 0 y esa zona
                raster=t_scanline-screen_indice_fin_pant;

		//Sumamos offset de la zona raster
                raster +=192;
		//printf ("scanline: %d raster: %d\n",t_scanline,raster);
                return raster;

}


int tbblue_get_current_raster_horiz_position(void)
{
/*
# Horizontally the display is the same in 50Hz or 60Hz mode but it
# varies by model.  It consists of 448 pixels (0-447) in 48k mode
# and 456 pixels (0-455) in 128k mode.  Grouped in eight pixels
# that's screen bytes 0-55 in 48k mode and 0-56 in 128k mode.
#
# 48k mode                      128k mode
# Bytes  Pixels                 Bytes  Pixels
#
#  0-31    0-255  Display        0-31    0-255  Display
# 32-39  256-319  Right Border  32-39  256-319  Right Border
# 40-51  320-415  HBlank        40-51  320-415  HBlank
# 52-55  416-447  Left Border   52-56  416-455  Left Border
*/
	int estados_en_linea=t_estados % screen_testados_linea;
	int horizontal_actual=estados_en_linea;

	//Dividir por la velocidad turbo
	horizontal_actual /=cpu_turbo_speed;

	//Con esto tendremos rango entre 0 y 223. Multiplicar por dos para ajustar a rango 0-448
	horizontal_actual *=2;

	//Dividir entre 8 para ajustar  rango 0-56
	horizontal_actual /=8;

	return horizontal_actual;

}



//Dice si un color de la capa de sprites es igual al color transparente ficticio inicial
int tbblue_si_sprite_transp_ficticio(z80_int color)
{
        if (color==TBBLUE_SPRITE_TRANS_FICT) return 1;
        return 0;
}


//Dice si un color de la paleta rbg9 es transparente
int tbblue_si_transparent(z80_int color)
{
	//if ( (color&0x1FE)==TBBLUE_TRANSPARENT_COLOR) return 1;
	color=(color>>1)&0xFF;
	if (color==TBBLUE_TRANSPARENT_REGISTER) return 1;
	return 0;
}


/*
Port 0x243B is write-only and is used to set the registry number.

Port 0x253B is used to access the registry value.

Register:
(R/W) 21 => Sprite system
 bits 7-2 = Reserved, must be 0
 bit 1 = Over border (1 = yes)
 bit 0 = Sprites visible (1 = visible)
*/
void tbsprite_put_color_line(int x,z80_byte color,int rangoxmin,int rangoxmax)
{

	//Si coordenadas invalidas, volver
	//if (x<0 || x>=MAX_X_SPRITE_LINE) return;

	//Si coordenadas fuera de la parte visible (border si o no), volver
	if (x<rangoxmin || x>rangoxmax) return;


	//Si fuera del viewport. Clip window on Sprites only work when the "over border bit" is disabled
	int clipxmin=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][0]+TBBLUE_SPRITE_BORDER;
	int clipxmax=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][1]+TBBLUE_SPRITE_BORDER;
	z80_byte sprites_over_border=tbblue_registers[21]&2;
	if (sprites_over_border==0 && (x<clipxmin || x>clipxmax)) return;

	//Si index de color es transparente, no hacer nada
/*
The sprites have now a new register for sprite transparency. Unlike the Global Transparency Colour register this refers to an index and  should be set when using indices other than 0xE3:

(R/W) 0x4B (75) => Transparency index for Sprites
bits 7-0 = Set the index value. (0XE3 after a reset)
	*/
	//if (color==tbblue_registers[75]) return;

	z80_int color_final=tbblue_get_palette_active_sprite(color);


	int xfinal=x;

	xfinal +=screen_total_borde_izquierdo*border_enabled.v;
	xfinal -=TBBLUE_SPRITE_BORDER;

	xfinal *=2; //doble de ancho


	//Ver si habia un color y activar bit colision
	z80_int color_antes=tbblue_layer_sprites[xfinal];

	//if (!tbblue_si_transparent(color_antes)) {
	if (!tbblue_si_sprite_transp_ficticio(color_antes) ) {
		//colision
		tbblue_port_303b |=1;
		//printf ("set colision flag. result value: %d\n",tbblue_port_303b);
	}
	

	//sprite_line[x]=color;
	tbblue_layer_sprites[xfinal]=color_final;
	tbblue_layer_sprites[xfinal+1]=color_final; //doble de ancho

	//if (xfinal<0 || xfinal>TBBLUE_LAYERS_PIXEL_WIDTH) printf ("out of range x sprites: %d\n",xfinal);

}

z80_byte tbsprite_do_overlay_get_pattern_xy_8bpp(z80_byte index_pattern,z80_byte sx,z80_byte sy)
{

	//return tbsprite_patterns[index_pattern][sy*TBBLUE_SPRITE_WIDTH+sx];
	return tbsprite_pattern_get_value_index_8bpp(index_pattern,sy*TBBLUE_SPRITE_WIDTH+sx);
}

z80_byte tbsprite_do_overlay_get_pattern_xy_4bpp(z80_byte index_pattern,int offset_1_pattern,z80_byte sx,z80_byte sy)
{
	z80_byte valor=tbsprite_pattern_get_value_index_4bpp(index_pattern,offset_1_pattern, (sy*TBBLUE_SPRITE_WIDTH+sx)/2  );

	if ( (sx % 2) == 0) valor=valor>>4;

	valor &= 0xF;

	return valor;

}


z80_int tbsprite_return_color_index(z80_byte index)
{
	//z80_int color_final=tbsprite_palette[index];

	z80_int color_final=tbblue_get_palette_active_sprite(index);
	//return RGB9_INDEX_FIRST_COLOR+color_final;
	return color_final;
}

int tbblue_if_sprites_enabled(void) 
{

	return tbblue_registers[21]&1;

}


int tbblue_if_tilemap_enabled(void) 
{

	return tbblue_registers[107]&128;

}

void tbsprite_do_overlay(void)
{


		if (!tbblue_if_sprites_enabled() ) return;

		//printf ("tbblue sprite chip activo\n");


        //int scanline_copia=t_scanline_draw-screen_indice_inicio_pant;
        int y=t_scanline_draw; //0..63 es border (8 no visibles)

		int border_no_visible=screen_indice_inicio_pant-TBBLUE_SPRITE_BORDER;

		y -=border_no_visible;

		//Ejemplo: scanline_draw=32 (justo donde se ve sprites). border_no_visible=64-32 =32
		//y=y-32 -> y=0


		//Situamos el 0 32 pixeles por encima de dentro de pantalla, tal cual como funcionan las cordenadas de sprite de tbblue


		//Calculos exclusivos para puntero buffer rainbow
		int rainbowy=t_scanline_draw-screen_invisible_borde_superior;
		if (border_enabled.v==0) rainbowy=rainbowy-screen_borde_superior;
		 

		//Aqui tenemos el y=0 arriba del todo del border

        //Bucle para cada sprite
        int conta_sprites;
		z80_byte index_pattern;

		int i;
		//int offset_pattern;

		z80_byte sprites_over_border=tbblue_registers[21]&2;


		int rangoxmin, rangoxmax;
		int rangoymin, rangoymax;

		if (sprites_over_border) {
			rangoxmin=0;
			rangoxmax=TBBLUE_SPRITE_BORDER+256+TBBLUE_SPRITE_BORDER-1;
			rangoymin=0;
			rangoymax=TBBLUE_SPRITE_BORDER+192+TBBLUE_SPRITE_BORDER-1;			

			if (tbblue_registers[21]&0x20) {
					// sprite clipping "over border" enabled, double the X coordinate of clip window
					rangoxmin=2*clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][0];
					rangoxmax=2*clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][1]+1;
					rangoymin=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][2];
					rangoymax=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][3];
					if (rangoxmax > TBBLUE_SPRITE_BORDER+256+TBBLUE_SPRITE_BORDER-1) {
						// clamp rangoxmax to 319
						rangoxmax = TBBLUE_SPRITE_BORDER+256+TBBLUE_SPRITE_BORDER-1;
					}
			}
		}

		else {
			// take clip window coordinates, but limit them to [0,0]->[255,191] (and offset them +32,+32)
			rangoxmin=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][0] + TBBLUE_SPRITE_BORDER;
			rangoxmax=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][1] + TBBLUE_SPRITE_BORDER-1;
			rangoymin=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][2] + TBBLUE_SPRITE_BORDER;
			rangoymax=clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][3] + TBBLUE_SPRITE_BORDER-1;
			if (rangoymax > TBBLUE_SPRITE_BORDER+192-1) {
				// clamp rangoymax to 32+191 (bottom edge of PAPER)
				rangoymax = TBBLUE_SPRITE_BORDER+192-1;
			}
		}

		if (y<rangoymin || y>rangoymax) return;


		int total_sprites=0;

		int sprite_visible;

		int anchor_x;
		int anchor_y;
		z80_byte anchor_palette_offset;
		z80_byte anchor_index_pattern;
		int anchor_visible;
		int anchor_sprite_es_4bpp;

		anchor_x=anchor_y=anchor_palette_offset=anchor_index_pattern=anchor_visible=anchor_sprite_es_4bpp=0;

		int sprite_has_5_bytes;

        for (conta_sprites=0;conta_sprites<TBBLUE_MAX_SPRITES && total_sprites<MAX_SPRITES_PER_LINE;conta_sprites++) {
					int sprite_x;
					int sprite_y;



					/*

					OLD
					[0] 1st: X position (bits 7-0).
					[1] 2nd: Y position (0-255).
					[2] 3rd: bits 7-4 is palette offset, bit 3 is X mirror, bit 2 is Y mirror, bit 1 is visible flag and bit 0 is X MSB.
					[3] 4th: bits 7-6 is reserved, bits 5-0 is Name (pattern index, 0-63).

					NEW
					[0] 1st: X position (bits 7-0).
					[1] 2nd: Y position (0-255).
					[2] 3rd: bits 7-4 is palette offset, bit 3 is X mirror, bit 2 is Y mirror, bit 1 is rotate flag and bit 0 is X MSB.
					[3] 4th: bit 7 is visible flag, bit 6 is reserved, bits 5-0 is Name (pattern index, 0-63).


					*/
					/*
					Because sprites can be displayed on top of the ZX Spectrum border, the coordinates of each sprite can range
					from 0 to 319 for the X axis and 0 to 255 for the Y axis. For both axes, values from 0 to 31 are reserved
					for the Left or top border, for the X axis the values 288 to 319 is reserved for the right border and for
					the Y axis values 224 to 255 for the lower border.

If the display of the sprites on the border is disabled, the coordinates of the sprites range from (32,32) to (287,223).
*/
					int relative_sprite=0;

					sprite_visible=tbsprite_sprites[conta_sprites][3]&128;

					sprite_has_5_bytes=tbsprite_sprites[conta_sprites][3] & 64;

							if (sprite_has_5_bytes) {
								//Pattern es de 5 bytes
								

								//Relative sprites
								//H N6 T X X Y Y Y8
								//{H,N6} must not equal {0,1} as this combination is used to indicate a relative sprite.
								if ((tbsprite_sprites[conta_sprites][4] & (128+64))==64) {

									relative_sprite=1;

									//printf ("Relative sprite number %d\n",conta_sprites);
									/*
									The sprite module records the following information from the anchor:

									Anchor.visible
									Anchor.X
									Anchor.Y
									Anchor.palette_offset
									Anchor.N (pattern number)
									Anchor.H (indicates if the sprite uses 4-bit patterns)
									*/

									//Relative sprites is visible if anchor and this sprite are both visibles
									//The visibility of a particular relative sprite is the result of ANDing the anchor’s visibility 
									//with the relative sprite’s visibility. In other words, if the anchor is invisible then so are all its relatives.
									if (sprite_visible && anchor_visible) sprite_visible=128; 
									//Realmente con 1 valdria pero lo hago para que coincida con el valor normal cuando es visible


									else sprite_visible=0;
									
									//sprite_visible=anchor_visible;

									//printf("visible: %d\n",sprite_visible);
								}

								else {
									//No es relativo. Guardar la visibilidad del ultimo anchor
									anchor_visible=sprite_visible;
								}

							}



					//Si sprite visible
					
					
					if (sprite_visible) {

						sprite_x=tbsprite_sprites[conta_sprites][0] | ((tbsprite_sprites[conta_sprites][2]&1)<<8);

						//printf ("sprite %d x: %d \n",conta_sprites,sprite_x);

						sprite_y=tbsprite_sprites[conta_sprites][1];

						if (sprite_has_5_bytes && !relative_sprite) {
							//Sprite Attribute 4
							//A. Extended Anchor Sprite
							//H N6 T X X Y Y Y8
							//Y8 = Ninth bit of the sprite’s Y coordinate

							sprite_y |= ((tbsprite_sprites[conta_sprites][4]&1)<<8);
						}

						//Posicionamos esa y teniendo en cuenta que nosotros contamos 0 arriba del todo del border en cambio sprites aqui
						//Considera y=32 dentro de pantalla y y=0..31 en el border
						//sprite_y +=screen_borde_superior-32;

						//Si y==32-> y=32+48-32=32+16=48
						//Si y==0 -> y=48-32=16


						//3rd: bits 7-4 is palette offset, bit 3 is X mirror, bit 2 is Y mirror, bit 1 is rotate flag and bit 0 is X MSB.
						//Offset paleta se lee tal cual sin rotar valor
						z80_byte palette_offset=(tbsprite_sprites[conta_sprites][2]) & 0xF0;

						index_pattern=tbsprite_sprites[conta_sprites][3]&63;
						
						//Sprite Attribute 4
						//0 1 N6 X X Y Y PO
						//TODO: solo para relative composite sprite, no unified
						z80_byte sprite_zoom_x=(tbsprite_sprites[conta_sprites][4] >> 3)&3;
						z80_byte sprite_zoom_y=(tbsprite_sprites[conta_sprites][4] >> 1)&3;

						//Si era sprite relativo
						if (relative_sprite) {
							//printf("Using the last anchor values\n");

							//No estoy seguro de estos AND 0xFF
							//Pero si los quito, el test de SpritRel.sna se ve peor
							sprite_x=(sprite_x+anchor_x) & 0xFF;
							sprite_y=(sprite_y+anchor_y) & 0xFF;

							/*
							If the relative sprite has its PR bit set in sprite attribute 2, 
							then the anchor’s palette offset is added to the relative sprite’s to determine the active 
							palette offset for the relative sprite. Otherwise the relative sprite uses its own palette 
							offset as usual.

							If the relative sprite has its PO bit set in sprite attribute 4, then the anchor’s pattern 
							number is added to the relative sprite’s to determine the pattern used for display. Otherwise 
							the relative sprite uses its own pattern number as usual. The intention is to supply a method 
							to easily animate a large sprite by manipulating the pattern number in the anchor.
							*/
							//P P P P XM YM R X8/PR
							if (tbsprite_sprites[conta_sprites][2]&1) {
								palette_offset=(palette_offset+anchor_palette_offset)& 0xF0;
							}

							//0 1 N6 X X Y Y PO
							if (tbsprite_sprites[conta_sprites][4]&1) {
								index_pattern=(index_pattern+anchor_index_pattern)&63;
							}
						}

						else {
							//Guardamos estos valores como el ultimo anchor
							//if (sprite_x > 512-128) sprite_x -= 512;                // -127 .. +384 (cover 8x scaleX)

							anchor_x=sprite_x;
							anchor_y=sprite_y;
							anchor_palette_offset=palette_offset;
							anchor_index_pattern=index_pattern;
						}

						//printf("x: %d\n",sprite_x);

						z80_byte mirror_x=tbsprite_sprites[conta_sprites][2]&8;
						//[2] 3rd: bits 7-4 is palette offset, bit 3 is X mirror, bit 2 is Y mirror, bit 1 is rotate flag and bit 0 is X MSB.
						z80_byte mirror_y=tbsprite_sprites[conta_sprites][2]&4;						

						//Si coordenada y esta en margen y sprite activo
						int diferencia=(y-sprite_y)>>sprite_zoom_y;


						int rangoymin, rangoymax;

						if (sprites_over_border) {
							rangoymin=0;
							rangoymax=TBBLUE_SPRITE_BORDER+192+TBBLUE_SPRITE_BORDER-1;
						}

						else {
							rangoymin=TBBLUE_SPRITE_BORDER;
							rangoymax=TBBLUE_SPRITE_BORDER+191;
						}


						//Pintar el sprite si esta en rango de coordenada y
						if (diferencia>=0 && diferencia<TBBLUE_SPRITE_HEIGHT && y>=rangoymin && y<=rangoymax) {

							//printf ("y: %d t_scanline_draw: %d rainbowy:%d sprite_y: %d\n",y,t_scanline_draw,rainbowy,sprite_y);
							z80_byte sx=0,sy=0; //Coordenadas x,y dentro del pattern
							//offset_pattern=0;

							//Incrementos de x e y
							int incx=+1;
							int incy=0;

							//Aplicar mirror si conviene y situarnos en la ultima linea
							if (mirror_y) {
								//offset_pattern=offset_pattern+TBBLUE_SPRITE_WIDTH*(TBBLUE_SPRITE_HEIGHT-1);
								sy=TBBLUE_SPRITE_HEIGHT-1-diferencia;
								//offset_pattern -=TBBLUE_SPRITE_WIDTH*diferencia;
							}
							else {
								//offset_pattern +=TBBLUE_SPRITE_WIDTH*diferencia;
								sy=diferencia;
							}



							//Dibujar linea x

							//Cambiar offset si mirror x, ubicarlo a la derecha del todo
							if (mirror_x) {
								//offset_pattern=offset_pattern+TBBLUE_SPRITE_WIDTH-1;
								sx=TBBLUE_SPRITE_WIDTH-1;
								incx=-1;
							}

							z80_byte sprite_rotate;

							sprite_rotate=tbsprite_sprites[conta_sprites][2]&2;

							/*
							Comparar bits rotacion con ejemplo en media/spectrum/tbblue/sprites/rotate_example.png
							*/
							/*
							Basicamente sin rotar un sprite, se tiene (reduzco el tamaño a la mitad aqui para que ocupe menos)


							El sentido normal de dibujado viene por ->, aumentando coordenada X


					->  ---X----
							---XX---
							---XXX--
							---XXXX-
							---X----
							---X----
							---X----
							---X----

							Luego cuando se rota 90 grados, en vez de empezar de arriba a la izquierda, se empieza desde abajo y reduciendo coordenada Y:

							    ---X----
									---XX---
									---XXX--
									---XXXX-
									---X----
									---X----
							^ 	---X----
							|		---X----

							Entonces, al dibujar empezando asi, la imagen queda rotada:

							--------
							--------
							XXXXXXXX
							----XXX-
							----XX--
							----X---
							--------

							De ahi que el incremento y sea -incremento x , incremento x sera 0

							Aplicando tambien el comportamiento para mirror, se tiene el resto de combinaciones

							*/


							if (sprite_rotate) {
								z80_byte sy_old=sy;
								sy=(TBBLUE_SPRITE_HEIGHT-1)-sx;
								sx=sy_old;

								incy=-incx;
								incx=0;
							}


							int sprite_es_4bpp=0;

							int offset_4bpp_N6=0;

							if (tbsprite_sprites[conta_sprites][3] & 64) {
								//Pattern es de 5 bytes

								//En caso de anchor:
								//H N6 T X X Y Y Y8
								//H = 1 if the sprite pattern is 4-bit
								//N6 = 7th pattern bit if the sprite pattern is 4-bit

								

								if (!relative_sprite) {

									if (tbsprite_sprites[conta_sprites][4] & 128) sprite_es_4bpp=1;

									if (sprite_es_4bpp) {
										if (tbsprite_sprites[conta_sprites][4] & 64) offset_4bpp_N6=1;
									}

									anchor_sprite_es_4bpp=sprite_es_4bpp;
								}

								else {


									//En caso de relative sprites, el valor de H viene del anchor
									/*
									B. Relative Sprite, Composite Type
									0 1 N6 X X Y Y PO
									C. Relative Sprite, Unified Type
									0 1 N6 0 0 0 0 PO

									Ver que el bit N6 se desplaza respecto a cuando es un anchor
									*/

									sprite_es_4bpp=anchor_sprite_es_4bpp;

									if (sprite_es_4bpp) {
										if (tbsprite_sprites[conta_sprites][4] & 32) offset_4bpp_N6=1;
									}

									
								}

								//TODO: Y8
							}

							for (i=0;i<TBBLUE_SPRITE_WIDTH;i++) {
								z80_byte index_color;

								if (sprite_es_4bpp) {
									//printf("es 4bpp\n");
									index_color=tbsprite_do_overlay_get_pattern_xy_4bpp(index_pattern,offset_4bpp_N6,sx,sy);

									//index_color +=7;
								}
								else {
									//printf("es 8 bpp\n");
									//printf("pattern %d\n",index_pattern);
									index_color=tbsprite_do_overlay_get_pattern_xy_8bpp(index_pattern,sx,sy);

									//index_color +=2;
								}

									//Si index de color es transparente, no hacer nada
/*
The sprites have now a new register for sprite transparency. Unlike the Global Transparency Colour register this refers to an index and  should be set when using indices other than 0xE3:

(R/W) 0x4B (75) => Transparency index for Sprites
bits 7-0 = Set the index value. (0XE3 after a reset)
	*/

								sx=sx+incx;
								sy=sy+incy;

								int sumar_x=1<<sprite_zoom_x;


								if (index_color!=tbblue_registers[75]) {

								//Sumar palette offset. Logicamente si es >256 el resultado, dará la vuelta el contador
								index_color +=palette_offset;

								//printf ("index color: %d\n",index_color);
								//printf ("palette offset: %d\n",palette_offset);
								

								if (sprite_zoom_x==0) {
									tbsprite_put_color_line(sprite_x,index_color,rangoxmin,rangoxmax);
								}
								else {
									int zz=0;
									for (zz=0;zz<sumar_x;zz++) {
										tbsprite_put_color_line(sprite_x+zz,index_color,rangoxmin,rangoxmax);
									}
								}

								}
								sprite_x+=sumar_x;


							}

							total_sprites++;
							//printf ("total sprites in this line: %d\n",total_sprites);
							if (total_sprites==MAX_SPRITES_PER_LINE) {
								//max sprites per line flag
								tbblue_port_303b |=2;
								//printf ("set max sprites per line flag\n");
							}

						}

				}
			}


}

z80_int tbblue_get_border_color(z80_int color)
{
    int flash_disabled = tbblue_registers[0x43]&1;  //flash_disabled se llamaba antes. ahora indica "enable ulanext"
    int is_timex_hires = timex_video_emulation.v && ((timex_port_ff&7) == 6);
    // 1) calculate correct color index into palette
	if (is_timex_hires) {
        // Timex HiRes 512x256 enforces border color by the FF port value, with priority over other methods
        color=get_timex_paper_mode6_color();        //0..7 PAPER index
        if (flash_disabled) color += 128;           // current HW does not bother with Bright in ULANext ON mode
        else color += 8 + 16;                       // +8 for BRIGHT 1, +16 for PAPER color in ULANext OFF mode
	}
    else if (flash_disabled) {   // ULANext mode ON

        if (tbblue_registers[0x42] == 255) {    // full-ink mode takes border colour from "fallback"
        //    // in this case this is final result, just return it (no further processing needed)
            return RGB9_INDEX_FIRST_COLOR + tbblue_get_9bit_colour(tbblue_registers[0x4A]);
        }

        // other ULANext modes take border color from palette starting at 128..135
        color += 128;
    }
    else {  // ULANext mode OFF (border colors are 16..23)
        color += 16;
    }
    // 2) convert index to actual color from palette
    color = tbblue_get_palette_active_ula(color);
    // 3) check for transparent colour -> use fallback colour if border is "transparent"
    if (tbblue_si_transparent(color)) {
        color = tbblue_get_9bit_colour(tbblue_registers[0x4A]);
    }
    return color + RGB9_INDEX_FIRST_COLOR;
}

void get_pixel_color_tbblue(z80_byte attribute,z80_int *tinta_orig, z80_int *papel_orig)
{

	/*

(R/W) 0x43 (67) => Palette Control
  bit 0 = Enabe ULANext mode if 1. (0 after a reset)

	*/

	z80_byte ink=*tinta_orig;
	z80_byte paper=*papel_orig;

	z80_byte palette_format=tbblue_registers[0x42];
	z80_byte flash_disabled=tbblue_registers[0x43]&1; //flash_disabled se llamaba antes. ahora indica "enable ulanext"


        z80_byte bright,flash;
        z80_int aux;



	if (!flash_disabled) {

/*
(R/W) 0x40 (64) => Palette Index
  bits 7-0 = Select the palette index to change the associated colour.

  For the ULA only, INKs are mapped to indices 0-7, Bright INKS to indices 8-15,
   PAPERs to indices 16-23 and Bright PAPERs to indices 24-31.

  In ULANext mode, INKs come from a subset of indices 0-127 and PAPERs come from
   a subset of indices 128-255.  The number of active indices depends on the number
   of attribute bits assigned to INK and PAPER out of the attribute byte.
  The ULA always takes border colour from paper.
*/

                        ink=attribute &7; 
                        paper=((attribute>>3) &7)+16; //colores papel empiezan en 16
                        bright=(attribute)&64; 
                        flash=(attribute)&128; 
                        if (flash) { 
                                if (estado_parpadeo.v) { 
                                        aux=paper; 
                                        paper=ink; 
                                        ink=aux; 
                                } 
                        } 
            
            if (bright) {   
                paper+=8; 
                ink+=8; 
            } 

	}

	else {
      /*

Nuevo:
(R/W) 0x42 (66) => ULANext Attribute Byte Format
  bits 7-0 = Mask indicating which bits of an attribute byte are used to
             represent INK.  The rest will represent PAPER.  (15 on reset)
             The mask can only indicate a solid sequence of bits on the right
             side of the attribute byte (1, 3, 7, 15, 31, 63, 127 or 255).
             The 255 value enables the full ink colour mode and all the the palette entries 
             will be inks with all paper colours mapping to position 128.

OLD:
(R/W) 0x42 (66) => Palette Format
  bits 7-0 = Number of the last ink colour entry on palette. (Reset to 15 after a Reset)
  This number can be 1, 3, 7, 15, 31, 63, 127 or 255.
  The 255 value enables the full ink colour mode and
  all the the palette entries are inks but the paper will be the colour at position 128.
  (only applies to ULANext palette. Layer 2 and Sprite palettes works as "full ink")

TODO: el significado es el mismo antes que ahora?
        */
		int rotacion_papel=1;
		int mascara_tinta=palette_format;
		int mascara_papel=255-mascara_tinta;

		//Estos valores se podrian tener ya calculados al llamar desde la funcion de screen_store_scanline_rainbow_solo_display_tbblue
		//o incluso calcularlos en cuanto se modificase el registro 42h o 43h
		//Como realmente son pocas variables a calcular, quiza ni merece la pena

		switch (mascara_tinta) {
			case 1:
				rotacion_papel=1;
			break;
	
			case 3:
				rotacion_papel=2;
			break;

			case 7:
				rotacion_papel=3;
			break;

			case 15:
				rotacion_papel=4;
			break;

			case 31:
				rotacion_papel=5;
			break;

			case 63:
				rotacion_papel=6;
			break;

			case 127:
				rotacion_papel=7;
			break;

		}

		if (mascara_tinta==255) {
			paper=128;
			ink=attribute;
		}

		else {
			ink=attribute & mascara_tinta;
			paper=((attribute & mascara_papel) >> rotacion_papel)+128;
		}	

	}			

	*tinta_orig=ink;		
	*papel_orig=paper;

}

z80_int tbblue_tile_return_color_index(z80_byte index)
{
        z80_int color_final=tbblue_get_palette_active_tilemap(index);
        return color_final;
}

void tbblue_do_tile_putpixel(z80_byte pixel_color,z80_byte transparent_colour,z80_byte tpal,z80_int *puntero_a_layer,int ula_over_tilemap)
{

			if (pixel_color!=transparent_colour) {
				//No es color transparente el que ponemos
				pixel_color |=tpal;

				//Vemos lo que hay en la capa
				z80_int color_previo_capa;
				color_previo_capa=*puntero_a_layer;

				//Poner pixel tile si color de ula era transparente o bien la ula está por debajo
				if (tbblue_si_sprite_transp_ficticio(color_previo_capa) || !ula_over_tilemap) { 
					*puntero_a_layer=tbblue_tile_return_color_index(pixel_color);
				}

			}


}

//Devuelve el color del pixel dentro de un tilemap
z80_byte tbblue_get_pixel_tile_xy_4bpp(int x,int y,z80_byte *puntero_this_tiledef)
{
	//4bpp
	int offset_x=x/2;

	int pixel_a_derecha=x%2;

	int offset_y=y*4; //Cada linea ocupa 4 bytes

	int offset_final=offset_y+offset_x;


	z80_byte byte_leido=puntero_this_tiledef[offset_final];
	if (pixel_a_derecha) {
		return byte_leido & 0xF;
	}

	else {
		return (byte_leido>>4) & 0xF;
	}

}

int tbblue_tiles_are_monocrome(void)
{
/*
Registro 6BH


  
    (R/W) 0x6B (107) => Tilemap Control


Bit	Function
7	1 to enable the tilemap
6	0 for 40x32, 1 for 80x32
5	1 to eliminate the attribute entry in the tilemap
4	palette select (0 = first Tilemap palette, 1 = second)
3	(core 3.0) enable "text mode"
2	Reserved, must be 0
1	1 to activate 512 tile mode (bit 0 of tile attribute is ninth bit of tile-id)
0 to use bit 0 of tile attribute as "ULA over tilemap" per-tile-selector

0	1 to enforce "tilemap over ULA" layer priority

Bits 7 & 6 enable the tilemap and select resolution.

Bit 5 changes the structure of the tilemap so that it contains only 8-bit tilemap-id 
entries instead of 16-bit tilemap-id and tile-attribute entries.

If 8-bit tilemap is selected, the tilemap contains only tile numbers and the attributes are taken 
from Default Tilemap Attribute Register ($6C).

Bit 4 selects one of two tilemap palettes used for final colour lookup.

Bit 1 enables the 512-tile-mode when the tile attribute (either global in $6C or per tile in map data) 
contains ninth bit of tile-id value. 
In this mode the tiles are drawn under ULA pixels, unless bit 0 is used to force whole tilemap over ULA.

Bit 0 can enforce tilemap over ULA either in 512-tile-mode, or even override the per-tile bit selector from tile attributes. 
If zero, the tilemap priority is either decided by attribute bit or in 512-tile-mode it is under ULA.


*/


	return tbblue_registers[0x6b] & 8; //bit "text mode"/monocromo

}

/*
int tbblue_tiles_512_mode(void)
{
	
// (R/W) 0x6B (107) => Tilemap Control
//   bit 7    = 1 to enable the tilemap
//   bit 6    = 0 for 40x32, 1 for 80x32
//   bit 5    = 1 to eliminate the attribute entry in the tilemap
//   bit 4    = palette select
//   bits 3-2 = Reserved set to 0
//   bit 1    = 1 to activate 512 tile mode
//   bit 0    = 1 to force tilemap on top of ULA
// 
// 512 tile mode is solely entered via bit 1.  Whether the ula is enabled or not makes no difference
// 
// when in 512 tile mode, the ULA is on top of the tilemap.  You can change this by setting bit 0	
	

	return tbblue_registers[0x6b] & 1;
}
*/


//Devuelve el color del pixel dentro de un tilemap
z80_byte tbblue_get_pixel_tile_xy_monocromo(int x,int y,z80_byte *puntero_this_tiledef)
{


	//Cada linea ocupa 1 bytes


	z80_byte byte_leido=puntero_this_tiledef[y];



	
	return (byte_leido>> (7-x) ) & 0x1;



}


z80_byte tbblue_get_pixel_tile_xy(int x,int y,z80_byte *puntero_this_tiledef)
{
	//si monocromo
	if (tbblue_tiles_are_monocrome() ) {
		return tbblue_get_pixel_tile_xy_monocromo(x,y,puntero_this_tiledef);
	}

	else {
		return tbblue_get_pixel_tile_xy_4bpp(x,y,puntero_this_tiledef);
	}
}

/*int temp_tile_rebote_x=10;
int temp_tile_rebote_y=10;
int temp_tile_rebote_incx=+1;
int temp_tile_rebote_incy=+1;
int temp_tile_rebote_veces=0;*/

void tbblue_do_tile_overlay(int scanline)
{
	//Gestion scroll vertical
	int scroll_y=tbblue_registers[49];

	//Renderizar en array tbblue_layer_ula el scanline indicado
	//leemos del tile y indicado, sumando scroll vertical
	int scanline_efectivo=scanline+scroll_y;
	scanline_efectivo %=256; 
	

	int posicion_y=scanline_efectivo/8;

	int linea_en_tile=scanline_efectivo %8;

  int tbblue_bytes_per_tile=2;

	int tilemap_width=tbblue_get_tilemap_width();

	int multiplicador_ancho=1;
	if (tilemap_width==40) multiplicador_ancho=2;
/*
//borde izquierdo + pantalla + borde derecho
#define TBBLUE_LAYERS_PIXEL_WIDTH (48+256+48)

z80_int tbblue_layer_ula[TBBLUE_LAYERS_PIXEL_WIDTH];
*/

	z80_int *puntero_a_layer;
	puntero_a_layer=&tbblue_layer_ula[(48-32)*2]; //Inicio de pantalla es en offset 48, restamos 32 pixeles que es donde empieza el tile
																								//*2 porque es doble de ancho

	z80_int *orig_puntero_a_layer;
	orig_puntero_a_layer=puntero_a_layer;

  /*
Bit	Function
7	1 to enable the tilemap
6	0 for 40x32, 1 for 80x32
5	1 to eliminate the attribute entry in the tilemap
4	palette select (0 = first Tilemap palette, 1 = second)
3	(core 3.0) enable "text mode"
2	Reserved, must be 0
1	1 to activate 512 tile mode (bit 0 of tile attribute is ninth bit of tile-id)
0 to use bit 0 of tile attribute as "ULA over tilemap" per-tile-selector
   */
	z80_byte tbblue_tilemap_control=tbblue_registers[107];

	if (tbblue_tilemap_control&32) tbblue_bytes_per_tile=1;




	z80_byte *puntero_tilemap;	
	z80_byte *puntero_tiledef;

	//Gestion scroll
/*(R/W) 0x2F (47) => Tilemap Offset X MSB
  bits 7-2 = Reserved, must be 0
  bits 1-0 = MSB X Offset
  Meaningful Range is 0-319 in 40 char mode, 0-639 in 80 char mode

(R/W) 0x30 (48) => Tilemap Offset X LSB
  bits 7-0 = LSB X Offset
  Meaningful range is 0-319 in 40 char mode, 0-639 in 80 char mode

(R/W) 0x31 (49) => Tilemap Offset Y
  bits 7-0 = Y Offset (0-191)
*/
	int scroll_x=tbblue_registers[48]+256*(tbblue_registers[47] & 3);

	//Llevar control de posicion x pixel en destino dentro del rango (0..40*8, 0..80*8)
	int max_destino_x_pixel=tilemap_width*8;

	scroll_x %=max_destino_x_pixel;

	int destino_x_pixel=0;

	int offset_sumar=0;
	if (scroll_x) {
		//Si hay scroll_x, no que hacemos es empezar a escribir por la parte final derecha
		destino_x_pixel=max_destino_x_pixel-scroll_x;
		offset_sumar=destino_x_pixel;
	}


	offset_sumar *=multiplicador_ancho;
	puntero_a_layer +=offset_sumar;


	//Clipwindow horizontal. Limites
	
				/*
				The tilemap display surface extends 32 pixels around the central 256×192 display.
The origin of the clip window is the top left corner of this area 32 pixels to the left and 32 pixels above 
the central 256×192 display. The X coordinates are internally doubled to cover the full 320 pixel width of the surface.
 The clip window indicates the portion of the tilemap display that is non-transparent and its indicated extent is inclusive; 
 it will extend from X1*2 to X2*2+1 horizontally and from Y1 to Y2 vertically.
			*/




	int clipwindow_min_x=clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][0]*2;
	int clipwindow_max_x=(clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][1]+1)*2;




	//Para controlar clipwindow. Coordenadas de destino_x_pixel van de 0 a 319 en modo 40 columnas, o de 0 a 639 en modo 80 columnas
	if (tilemap_width==80) {
		clipwindow_min_x *=2;
		clipwindow_max_x *=2;
	}

	//printf ("clipwindow_min_x %d clipwindow_max_x %d\n",clipwindow_min_x,clipwindow_max_x);

	//Inicio del tilemap
	puntero_tilemap=tbblue_ram_memory_pages[5*2]+(256*tbblue_get_offset_start_tilemap());

	//Obtener offset sobre tilemap
	int offset_tilemap=tbblue_bytes_per_tile*tilemap_width*posicion_y;


	puntero_tilemap +=offset_tilemap;  //Esto apuntara al primer tile de esa posicion y y con x=0


	//Inicio del tiledef
	puntero_tiledef=tbblue_ram_memory_pages[5*2]+(256*tbblue_get_offset_start_tiledef());

	//puntero_a_layer -=scroll_x; //temp chapuza


	int x;

	int xmirror,ymirror,rotate;
	z80_byte tpal;

	z80_byte byte_first;
	z80_byte byte_second;

	int ula_over_tilemap;

    // 0 when tilemap-over-ULA is enforced, 1 when attribute ULA-over-tilemap bit should be used
    int ula_over_tilemap_mask = (tbblue_tilemap_control&1)^1;	

	//tilemap_width=40;
/*
(R/W) 0x4C (76) => Transparency index for the tilemap
  bits 7-4 = Reserved, must be 0
  bits 3-0 = Set the index value (0xF after reset)
Defines the transparent colour index for tiles. The 4-bit pixels of a tile definition are compared to this value to determine if they are transparent.
*/
	z80_byte transparent_colour=tbblue_registers[76] & 0xF;



	//printf ("y: %d t_scanline_draw: %d rainbowy:%d sprite_y: %d\n",y,t_scanline_draw,rainbowy,sprite_y);
	z80_byte tbblue_default_tilemap_attr=tbblue_registers[108];



		


/* Antiguo bloque. Mejorado en el fork de Peter Ped Helcmanovsky mas abajo



	for (x=0;x<tilemap_width;x++) {
		//TODO stencil mode
		byte_first=*puntero_tilemap;
		puntero_tilemap++;
		if (tbblue_bytes_per_tile==2) {
			byte_second=*puntero_tilemap;
			puntero_tilemap++;
		}
                                        
		int tnum=byte_first;


// bits 15-12 : palette offset
//   bit     11 : x mirror
//   bit     10 : y mirror
//   bit      9 : rotate
//   bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
//   bits   7-0 : tile number
//                                       


		if (tbblue_bytes_per_tile==1) {
                                        

//                                                 (R/W) 0x6C (108) => Default Tilemap Attribute
//   bits 7-4 = Palette Offset
//   bit 3    = X mirror
//   bit 2    = Y mirror
//   bit 1    = Rotate
//   bit 0    = ULA over tilemap
//             (bit 8 of tile id if the ULA is disabled)
//                                                 
			tpal=(tbblue_default_tilemap_attr)&0xF0;

			xmirror=(tbblue_default_tilemap_attr>>3)&1;
			ymirror=(tbblue_default_tilemap_attr>>2)&1;
			rotate=(tbblue_default_tilemap_attr>>1)&1;

			if (tbblue_if_ula_is_enabled() ) {
    
                                           
//                                                 108
//                                                   bit 0    = ULA over tilemap
//              (bit 8 of tile id if the ULA is disabled)
                                                
				ula_over_tilemap=tbblue_default_tilemap_attr &1;
			}

			else {
				tnum |=(tbblue_default_tilemap_attr&1)<<8; // bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
			}

			//printf ("1 bytes por tile\n");

		}

		else {
			//printf ("2 bytes por tile\n");
																				
                                                

//                                          bits 15-12 : palette offset
//   bit     11 : x mirror
//   bit     10 : y mirror
//   bit      9 : rotate
//   bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
//                                       
			tpal=(byte_second)&0xF0;
			xmirror=(byte_second>>3)&1;
			ymirror=(byte_second>>2)&1;
			rotate=(byte_second>>1)&1;
			//ula_over_tilemap=byte_second &1;

			//printf ("Color independiente. tpal:%d byte_second: %02XH\n",tpal,byte_second);

			if (tbblue_if_ula_is_enabled() ) {
        //
        //bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number) 
        //                                        
				ula_over_tilemap=byte_second &1;
			}

			else {
				tnum |=(byte_second&1)<<8; // bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
			}
		}

		//Sacar puntero a principio tiledef. 
		int offset_tiledef;


		if (tbblue_tiles_are_monocrome()) {
			

			//TODO: mejorar este caso


			//byte_second=*(puntero_tilemap-1);

			xmirror=0;
			ymirror=0;
			rotate=0;


			

// bits 15-9: palette offset (7 bits)
// bit 8 : ULA over tilemap (in 512 tile mode, bit 8 of the tile number)
// bits 7-0 : tile number

			if (tbblue_tiles_512_mode() ) {
				//printf ("512 mode\n");

				tnum=byte_first | ((byte_second&1)*256);
				ula_over_tilemap=0;
			}

			else {
				tnum=byte_first;
				ula_over_tilemap=byte_second &1;
			}


			tpal=(byte_second >> 1) & 127;

			//TODO: parche feo. no se porque, si no, cp/m se ven mal los colores.
			tpal=(byte_second >> 3) & 31;

			//madre mia que mal esta documentado esto.... sin rotar un bit a la derecha??? en fin
			tpal=(byte_second)&0xFE;
			
			offset_tiledef=tnum*TBBLUE_TILE_HEIGHT;
//printf ("tnum: %d off: %d\n",tnum,offset_tiledef);

			
// 			In text mode the interpretation of the tilemap entry changes.  Normally it's:

// bits 15-12 : palette offset
// bit 11 : x mirror
// bit 10 : y mirror
// bit 9 : rotate
// bit 8 : ULA over tilemap (in 512 tile mode, bit 8 of the tile number)
// bits 7-0 : tile number
// 
// but it changes to:
// 
// bits 15-9: palette offset (7 bits)
// bit 8 : ULA over tilemap (in 512 tile mode, bit 8 of the tile number)
// bits 7-0 : tile number
// 
// The tiles are defined like UDGs (1 bit per pixel) and that 1 bit is combined with 
// the 7-bit palette offset to form the 8-bit pixel that gets looked up in the tilemap palette.
			
		}
		else {
			//4 bpp. cada tiledef ocupa 4 bytes * 8 = 32
			offset_tiledef=tnum*(TBBLUE_TILE_WIDTH/2)*TBBLUE_TILE_HEIGHT;
		}


FIN Antiguo bloque. Mejorado del fork de Peter Ped Helcmanovsky */


// Bloque mejorado del fork de Peter Ped Helcmanovsky

        for (x=0;x<tilemap_width;x++) {
                //TODO stencil mode
                byte_first=*puntero_tilemap;
                puntero_tilemap++;
                if (tbblue_bytes_per_tile==2) {
                        byte_second=*puntero_tilemap;
                        puntero_tilemap++;
                } else {
                        byte_second = tbblue_default_tilemap_attr;
                }

                int tnum=byte_first;

/*
  bits 15-12 : palette offset
  bit     11 : x mirror
  bit     10 : y mirror
  bit      9 : rotate
  bit      8 : ULA over tilemap OR bit 8 of tile number (512 tile mode)
  bits   7-0 : tile number
  */

                if (tbblue_tiles_are_monocrome()) {
					//En modo texto:
// bits 15-9: palette offset (7 bits)
// bit 8 : ULA over tilemap (in 512 tile mode, bit 8 of the tile number)
// bits 7-0 : tile number
// 
// The tiles are defined like UDGs (1 bit per pixel) and that 1 bit is combined with 
// the 7-bit palette offset to form the 8-bit pixel that gets looked up in the tilemap palette.

						//Que mal documentado esta el tema de paleta... no se rota bits a la derecha
                        tpal=(byte_second)&0xFE;
                        xmirror=0;
                        ymirror=0;
                        rotate=0;
                } else {
						//Que mal documentado esta el tema de paleta... no se rota bits a la derecha
                        tpal=(byte_second)&0xF0;
                        xmirror=(byte_second>>3)&1;
                        ymirror=(byte_second>>2)&1;
                        rotate=(byte_second>>1)&1;
                }


                if (tbblue_tilemap_control&2) {
                        // 512 tile mode
                        tnum |= (byte_second&1)<<8;
                        ula_over_tilemap = ula_over_tilemap_mask;
                } else {
                        // 256 tile mode, "ULA over tilemap" bit used from attribute (plus "force tilemap")
                        ula_over_tilemap = byte_second & ula_over_tilemap_mask;
                }

                //printf ("Color independiente. tpal:%d byte_second: %02XH\n",tpal,byte_second);

                //Sacar puntero a principio tiledef.
                int offset_tiledef;


                if (tbblue_tiles_are_monocrome()) {
                        offset_tiledef=tnum*TBBLUE_TILE_HEIGHT;
                }
                else {
                        //4 bpp. cada tiledef ocupa 4 bytes * 8 = 32
                        offset_tiledef=tnum*(TBBLUE_TILE_WIDTH/2)*TBBLUE_TILE_HEIGHT;
                }

                //sumar posicion y
                //offset_tiledef += linea_en_tile*4;

                //tiledef

                //printf ("tpal %d\n",tpal);
	


//FIN del bloque mejorado del fork de Peter Ped Helcmanovsky




		//Renderizar los 8 pixeles del tile
		int pixel_tile;
		z80_byte *puntero_this_tiledef;
		puntero_this_tiledef=&puntero_tiledef[offset_tiledef];


		//Incrementos de x e y
		int incx=+1;
		int incy=0;

		z80_byte sx=0,sy=0; //Coordenadas x,y dentro del tile

		//sumar posicion y
		sy += linea_en_tile;		


		//Aplicar mirror si conviene y situarnos en la ultima linea
		if (ymirror) {
			//sy=TBBLUE_TILE_HEIGHT-1-diferencia;
			sy=TBBLUE_TILE_HEIGHT-1-linea_en_tile;
		}
		else {
			//sy=diferencia;
		}

		//Cambiar offset si mirror x, ubicarlo a la derecha del todo
		if (xmirror) {
			sx=TBBLUE_TILE_WIDTH-1;
			incx=-1;
		}


	//Rotacion. Mismo metodo que con sprites
							/*
              Comparar bits rotacion con ejemplo en media/spectrum/tbblue/sprites/rotate_example.png
              */
              /*
             Basicamente sin rotar un sprite, se tiene (reduzco el tamaño a la mitad aqui para que ocupe menos)


            El sentido normal de dibujado viene por ->, aumentando coordenada X


				->  ---X----
						---XX---
						---XXX--
						---XXXX-
						---X----
						---X----
						---X----
						---X----

				Luego cuando se rota 90 grados, en vez de empezar de arriba a la izquierda, se empieza desde abajo y reduciendo coordenada Y:

						---X----
						---XX---
						---XXX--
						---XXXX-
						---X----
						---X----
		^       ---X----
		|     	---X----

				Entonces, al dibujar empezando asi, la imagen queda rotada:

						--------
						--------
						XXXXXXXX
						----XXX-
						----XX--
						----X---
						--------

				De ahi que el incremento y sea -incremento x , incremento x sera 0

				Aplicando tambien el comportamiento para mirror, se tiene el resto de combinaciones

				*/

			
		if (rotate) {
			z80_byte sy_old=sy;
			sy=(TBBLUE_TILE_HEIGHT-1)-sx;
			sx=sy_old;

			incy=-incx;
			incx=0;
			//printf ("Tiles con rotacion size %d\n",tbblue_bytes_per_tile);
		}


		for (pixel_tile=0;pixel_tile<8;pixel_tile+=2) { //Saltamos de dos en dos porque son 4bpp


			z80_byte pixel_izq,pixel_der;

			//Pixel izquierdo
			pixel_izq=tbblue_get_pixel_tile_xy(sx,sy,puntero_this_tiledef);

			if (destino_x_pixel>=clipwindow_min_x && destino_x_pixel<clipwindow_max_x) {
				tbblue_do_tile_putpixel(pixel_izq,transparent_colour,tpal,puntero_a_layer,ula_over_tilemap);
				if (tilemap_width==40) tbblue_do_tile_putpixel(pixel_izq,transparent_colour,tpal,puntero_a_layer+1,ula_over_tilemap);
			}
			puntero_a_layer++;
			if (tilemap_width==40) puntero_a_layer++;
			destino_x_pixel++;



			sx=sx+incx;
			sy=sy+incy;

			//Controlar si se sale por la derecha (pues hay scroll)
			if (destino_x_pixel==max_destino_x_pixel) {
				destino_x_pixel=0;
				puntero_a_layer=orig_puntero_a_layer;
			}



			//Pixel derecho
			pixel_der=tbblue_get_pixel_tile_xy(sx,sy,puntero_this_tiledef);

			if (destino_x_pixel>=clipwindow_min_x && destino_x_pixel<clipwindow_max_x) {
				tbblue_do_tile_putpixel(pixel_der,transparent_colour,tpal,puntero_a_layer,ula_over_tilemap);
				if (tilemap_width==40) tbblue_do_tile_putpixel(pixel_der,transparent_colour,tpal,puntero_a_layer+1,ula_over_tilemap);
			}
			puntero_a_layer++;
			if (tilemap_width==40) puntero_a_layer++;
			destino_x_pixel++;

			sx=sx+incx;
			sy=sy+incy;

			//Controlar si se sale por la derecha (pues hay scroll)
			if (destino_x_pixel==max_destino_x_pixel) {
				destino_x_pixel=0;
				puntero_a_layer=orig_puntero_a_layer;
			}


		}


  }

}

void tbblue_fast_render_ula_layer(z80_int *puntero_final_rainbow,int estamos_borde_supinf,
	int final_borde_izquierdo,int inicio_borde_derecho,int ancho_rainbow)
{


	int i;
		z80_int color;


	//(R/W) 0x4A (74) => Transparency colour fallback
	//	bits 7-0 = Set the 8 bit colour.
	//	(0 = black on reset on reset)
	z80_int fallbackcolour = RGB9_INDEX_FIRST_COLOR + tbblue_get_9bit_colour(tbblue_registers[74]);

	for (i=0;i<ancho_rainbow;i++) {


		//Primera capa
		color=tbblue_layer_ula[i];
		if (!tbblue_si_sprite_transp_ficticio(color) ) {
			*puntero_final_rainbow=RGB9_INDEX_FIRST_COLOR+color;
		}

	
					
				else {
					if (estamos_borde_supinf) {
						//Si estamos en borde inferior o superior, no hacemos nada, dibujar color borde
					}

					else {
						//Borde izquierdo o derecho o pantalla. Ver si estamos en pantalla
						if (i>=final_borde_izquierdo && i<inicio_borde_derecho) {
							//Poner color indicado por "Transparency colour fallback" registro
							*puntero_final_rainbow=fallbackcolour;
						}
						else {
							//Es borde. dejar ese color
						}
					
					}
				}


	

		puntero_final_rainbow++;

		
	}

}

//int tempconta;

//Nos situamos en la linea justo donde empiezan los tiles
void tbblue_render_layers_rainbow(int capalayer2,int capasprites)
{


	//(R/W) 0x4A (74) => Transparency colour fallback
		//	bits 7-0 = Set the 8 bit colour.
		//	(0 = black on reset on reset)
	z80_int fallbackcolour = RGB9_INDEX_FIRST_COLOR + tbblue_get_9bit_colour(tbblue_registers[74]);

	int y;
	int diferencia_border_tiles;

	//diferencia_border_tiles=screen_indice_inicio_pant-TBBLUE_TILES_BORDER;
	//Tamaño del border efectivo restando espacio usado por tiles/layer2 en border
	diferencia_border_tiles=screen_borde_superior-TBBLUE_TILES_BORDER;

    y=t_scanline_draw-screen_invisible_borde_superior;


	//y=t_scanline_draw-screen_indice_inicio_pant;
    if (border_enabled.v==0) y=y-screen_borde_superior;


		//if (y<diferencia_border_tiles || y>=(screen_indice_inicio_pant+192+TBBLUE_TILES_BORDER)) {	

		if (y<diferencia_border_tiles || y>=(screen_borde_superior+192+TBBLUE_TILES_BORDER)) {	
			
			//printf ("t_scanline_draw: %d y: %d diferencia_border_tiles: %d screen_indice_inicio_pant: %d screen_invisible_borde_superior: %d TBBLUE_TILES_BORDER: %d\n",
			//	t_scanline_draw,y,diferencia_border_tiles,screen_indice_inicio_pant,screen_invisible_borde_superior,TBBLUE_TILES_BORDER);

			//Si estamos por encima o por debajo de la zona de tiles/layer2,
			//que es la mas alta de todas las capas

			return; 

		}
		

		//Calcular donde hay border
		int final_border_superior=screen_indice_inicio_pant-screen_invisible_borde_superior;
		int inicio_border_inferior=final_border_superior+192;

		//Vemos si linea esta en zona border
		int estamos_borde_supinf=0;
		if (y<final_border_superior || y>=inicio_border_inferior) estamos_borde_supinf=1;

		//Zona borde izquierdo y derecho
		int final_borde_izquierdo=2*screen_total_borde_izquierdo*border_enabled.v;
		int inicio_borde_derecho=final_borde_izquierdo+TBBLUE_DISPLAY_WIDTH;




		int ancho_rainbow=get_total_ancho_rainbow();

	z80_int *puntero_final_rainbow=&rainbow_buffer[ y*ancho_rainbow ];

	//Por defecto
	//sprites over the Layer 2, over the ULA graphics
	//Si solo hay capa ula, hacer render mas rapido
	//printf ("%d %d %d\n",capalayer2,capasprites,tbblue_get_layers_priorities());
	//if (capalayer2==0 && capasprites==0 && tbblue_get_layers_priorities()==0) {  //prio 0=S L U
	if (capalayer2==0 && capasprites==0) { 	 
		//Hará fast render cuando no haya capa de layer2 o sprites, aunque tambien,
		//estando esas capas, cuando este en zona de border o no visible de dichas capas
		tbblue_fast_render_ula_layer(puntero_final_rainbow,estamos_borde_supinf,final_borde_izquierdo,inicio_borde_derecho,ancho_rainbow);

	}	



	else {


	tbblue_set_layer_priorities();

	z80_int color;
	
	//printf ("ancho total: %d size layers: %d\n",get_total_ancho_rainbow(),TBBLUE_LAYERS_PIXEL_WIDTH );

	int i;


	for (i=0;i<ancho_rainbow;i++) {


		//Primera capa
		color=p_layer_first[i];
		if (!tbblue_fn_pixel_layer_transp_first(color) ) {
			*puntero_final_rainbow=RGB9_INDEX_FIRST_COLOR+color;
		}

		else {
			color=p_layer_second[i];
			if (!tbblue_fn_pixel_layer_transp_second(color) ) {
				*puntero_final_rainbow=RGB9_INDEX_FIRST_COLOR+color;
			}

			else {
				color=p_layer_third[i];
				if (!tbblue_fn_pixel_layer_transp_third(color) ) {
					*puntero_final_rainbow=RGB9_INDEX_FIRST_COLOR+color;
				}
					
				else {
					if (estamos_borde_supinf) {
						//Si estamos en borde inferior o superior, no hacemos nada, dibujar color borde
					}

					else {
						//Borde izquierdo o derecho o pantalla. Ver si estamos en pantalla
						if (i>=final_borde_izquierdo && i<inicio_borde_derecho) {
							//Poner color indicado por "Transparency colour fallback" registro:
							*puntero_final_rainbow=fallbackcolour;
						}
						else {
							//Es borde. dejar ese color
						}
					
					}
				}
			}

		}

		puntero_final_rainbow++;

		
	}

	}
}


char *tbblue_layer2_video_modes_names[]={
	"256x192 8bpp",
	"320x256 8bpp",
	"640x256 4bpp",
	"Unknown"
};

char *tbblue_get_layer2_mode_name(void)
{
		//Resolucion si 256x192x8, organizacion en scanlines, o las otras resoluciones que organizan en columnas
		//00=256x192x8. 01=320x256x8, 10=640x256x4
		int layer2_resolution=(tbblue_registers[112]>>4) & 3; 

		return tbblue_layer2_video_modes_names[layer2_resolution];
}


void tbblue_do_layer2_overlay(int linea_render)
{


		if (!tbblue_is_active_layer2() || tbblue_force_disable_layer_layer_two.v) return;

		//Resolucion si 256x192x8, organizacion en scanlines, o las otras resoluciones que organizan en columnas
		//00=256x192x8. 01=320x256x8, 10=640x256x4
		int layer2_resolution=(tbblue_registers[112]>>4) & 3; 


		//Obtener offset paleta color
		int palette_offset=tbblue_registers[112] & 15;

		

		//Obtener inicio pantalla layer2
		int tbblue_layer2_offset=tbblue_get_offset_start_layer2();


		//Scroll vertical

/*
(R/W) 0x17 (23) => Layer2 Offset Y
  bits 7-0 = Y Offset (0-191)(Reset to 0 after a reset)
*/		
		//Mantener el offset y en 0..191
		z80_byte tbblue_reg_23=tbblue_registers[23]; 


		int offset_scroll=tbblue_reg_23+linea_render;

		if (layer2_resolution) {
			offset_scroll %=256;
			tbblue_layer2_offset +=offset_scroll;
		}

		else {
			offset_scroll %=192;
			tbblue_layer2_offset +=offset_scroll*256;
		}



		//Scroll horizontal
/*

(R/W) 22 => Layer2 Offset X
  bits 7-0 = X Offset (0-255)(Reset to 0 after a reset)
0x71 (113) => Layer 2 X Scroll MSB
(R/W)
   bits 7:1 = Reserved, must be 0
   bit 0 = MSB of scroll amount		
*/

		int tbblue_reg_22=tbblue_registers[22] + (tbblue_registers[113]&1)*256;



		//Valor final de scroll x, acotar a valores validos
		if (layer2_resolution) {
			tbblue_reg_22 %=320;
		}

		else {
			tbblue_reg_22 %=256;
		}


		//Para la gestión de la posicion x del pixel
		int pos_x_origen=tbblue_reg_22;

		

		//Inicio de la posicion en el layer final
		int posicion_array_layer=0;


		int borde_no_escribible=screen_total_borde_izquierdo;
		if (layer2_resolution>0) borde_no_escribible-=TBBLUE_LAYER2_12_BORDER;

		posicion_array_layer +=borde_no_escribible*2; //doble de ancho

		int posx;

		//Total pixeles por defecto
		int total_x=256;

		
		int clip_min=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][0];
		int clip_max=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][1];

		if (layer2_resolution) {
			total_x +=TBBLUE_LAYER2_12_BORDER*2;

			//Se multiplica por 2 siempre en estas resoluciones de 320x256 y 640x256
			clip_min *=2;
			clip_max *=2;			

		}


       	for (posx=0;posx<total_x;posx++) {
				

			//printf ("posx: %d pos_x_origen: %d\n",posx,pos_x_origen);
				
			if (posx>=clip_min && posx<=clip_max ) {
			
				int offset_pixel;

				z80_byte pixel_izq,pixel_der;

				if (layer2_resolution) {
					offset_pixel=tbblue_layer2_offset+pos_x_origen*256;
				}

				else {
					offset_pixel=tbblue_layer2_offset+pos_x_origen;
				}


				offset_pixel &=0x1FFFFF;


				

				z80_byte byte_leido=memoria_spectrum[offset_pixel];

				if (layer2_resolution==2) {
					pixel_izq=(byte_leido>>4) & 0xF;
					pixel_der=(byte_leido   ) & 0xF;					

				}

				else {
					pixel_izq=byte_leido;
					pixel_der=pixel_izq;
				}

				z80_int final_color_layer2_izq=tbblue_get_palette_active_layer2(pixel_izq+palette_offset);

				//Ver si color resultante es el transparente de ula, y cambiarlo por el color transparente ficticio
				if (tbblue_si_transparent(final_color_layer2_izq)) final_color_layer2_izq=TBBLUE_SPRITE_TRANS_FICT;


				z80_int final_color_layer2_der=tbblue_get_palette_active_layer2(pixel_der+palette_offset);

				//Ver si color resultante es el transparente de ula, y cambiarlo por el color transparente ficticio
				if (tbblue_si_transparent(final_color_layer2_der)) final_color_layer2_der=TBBLUE_SPRITE_TRANS_FICT;		

				tbblue_layer_layer2[posicion_array_layer]=final_color_layer2_izq;
				tbblue_layer_layer2[posicion_array_layer+1]=final_color_layer2_der;

			}

			//Este incremento se tiene que hacer siempre fuera, para que se aplique siempre, se haga o no clipping
			posicion_array_layer+=2;



			//else {
			//	printf ("fuera rango\n");
			//}

						
	
			//Siguiente posicion
			pos_x_origen++;
			if (pos_x_origen>=total_x) {
				pos_x_origen=0;
			}
				

	    }


	 
}

void tbblue_reveal_layer_draw(z80_int *layer)
{
	int i;

	for (i=0;i<TBBLUE_LAYERS_PIXEL_WIDTH;i++) {
		z80_int color=*layer;
	
		if (!tbblue_si_sprite_transp_ficticio(color)) {

			//Color de revelado es blanco o negro segun cuadricula:
			// Negro Blanco Negro ...
			// Blanco Negro Blanco ...
			// Negro Blanco Negro ....
			// .....

			//Por tanto tener en cuenta posicion x e y
			int posx=i&1;
			int posy=t_scanline_draw&1;

			//0,0: 0
			//0,1: 1
			//1,0: 1
			//1,0: 0
			//Es un xor

			int si_blanco_negro=posx ^ posy;

			*layer=511*si_blanco_negro; //ultimo de los colores en paleta rgb9 de tbblue -> blanco, negro es 0
		}

		layer++;
	}
}


//Forzar a dibujar capa con color fijo, para debug
z80_bit tbblue_reveal_layer_ula={0};
z80_bit tbblue_reveal_layer_layer2={0};
z80_bit tbblue_reveal_layer_sprites={0};

void tbblue_do_ula_standard_overlay()
{


	//Render de capa standard ULA (normal, timex) 

	//printf ("scan line de pantalla fisica (no border): %d\n",t_scanline_draw);

	//linea que se debe leer
	int scanline_copia=t_scanline_draw-screen_indice_inicio_pant;



	int x,bit;
	z80_int direccion;
	z80_byte byte_leido;


	int color=0;
	z80_byte attribute;
	z80_int ink,paper;


	z80_byte *screen=get_base_mem_pantalla();


	/*
	(R/W) 0x32 (50) => ULA / LoRes Offset X
bits 7-0 = X Offset (0-255)(Reset to 0 after a reset)
ULA can only scroll in multiples of 8 pixels so the lowest 3 bits have no effect at this time.
	*/

	//entonces sumar 1 posicion por cada 8 del scroll

	z80_byte ula_offset_x=tbblue_registers[50];
	ula_offset_x /=8;
	int indice_origen_bytes=ula_offset_x*2; //*2 dado que leemos del puntero_buffer_atributos que guarda 2 bytes: pixel y atributo	

	/*
	(R/W) 0x33 (51) => ULA / LoRes Offset Y
bits 7-0 = Y Offset (0-191)(Reset to 0 after a reset)


	linea_lores +=tbblue_registers[0x33];

	linea_lores=linea_lores % 192;

	*/

	z80_byte tbblue_scroll_y=tbblue_registers[51];
	

	scanline_copia +=tbblue_scroll_y;
	scanline_copia=scanline_copia % 192;



	//Usado cuando hay scroll vertical y por tanto los pixeles y atributos salen de la pantalla tal cual (modo sin rainbow)
	int pos_no_rainbow_pix_x;


	//scroll x para modo no rainbow (es decir, cuando hay scroll vertical)
	pos_no_rainbow_pix_x=ula_offset_x;
	pos_no_rainbow_pix_x %=32;	


	//Estos direccion y dir_atributo usados cuando hay scroll vertical y por tanto los pixeles y atributos salen de la pantalla tal cual (modo sin rainbow),
	//y tambien en timex 512x192
	direccion=screen_addr_table[(scanline_copia<<5)];

	int fila=scanline_copia/8;
	int dir_atributo=6144+(fila*32);


	z80_byte *puntero_buffer_atributos;
	z80_byte col6=0;
	z80_byte tin6, pap6;

	z80_byte timex_video_mode=timex_port_ff&7;
	z80_bit si_timex_hires={0};
	z80_bit si_timex_8_1={0};

	if (timex_video_mode==2) si_timex_8_1.v=1;

	//Por defecto
	puntero_buffer_atributos=scanline_buffer;

	if (timex_video_emulation.v) {
	//Modos de video Timex
	/*
000 - Video data at address 16384 and 8x8 color attributes at address 22528 (like on ordinary Spectrum);

001 - Video data at address 24576 and 8x8 color attributes at address 30720;

010 - Multicolor mode: video data at address 16384 and 8x1 color attributes at address 24576;

110 - Extended resolution: without color attributes, even columns of video data are taken from address 16384, and odd columns of video data are taken from address 24576
	*/
		switch (timex_video_mode) {

			case 4:
			case 6:
				//512x192 monocromo. 
				//y color siempre fijo
				/*
	bits D3-D5: Selection of ink and paper color in extended screen resolution mode (000=black/white, 001=blue/yellow, 010=red/cyan, 011=magenta/green, 100=green/magenta, 101=cyan/red, 110=yellow/blue, 111=white/black); these bits are ignored when D2=0

				black, blue, red, magenta, green, cyan, yellow, white
				*/

				//Si D2==0, these bits are ignored when D2=0?? Modo 4 que es??

				tin6=get_timex_ink_mode6_color();


				//Obtenemos color
				pap6=get_timex_paper_mode6_color();
				//printf ("papel: %d\n",pap6);

				//Y con brillo
				col6=((pap6*8)+tin6)+64;

			
				si_timex_hires.v=1;
			break;


		}
	}

	//Capa de destino
	int posicion_array_layer=0;
	posicion_array_layer +=(screen_total_borde_izquierdo*border_enabled.v*2); //Doble de ancho


	int columnas=32;

	if (si_timex_hires.v) {
		columnas=64;
	}

    for (x=0;x<columnas;x++) {

		if (tbblue_scroll_y) {
			//Si hay scroll vertical (no es 0) entonces el origen de los bytes no se obtiene del buffer de pixeles y color en alta resolucion,
			//Si no que se obtiene de la pantalla tal cual
			//TODO: esto es una limitacion de tal y como hace el render el tbblue, en que hago render de una linea cada vez,
			//para corregir esto, habria que tener un buffer destino con todas las lineas de ula y hacer luego overlay con cada
			//capa por separado, algo completamente impensable
			//de todas maneras esto es algo extraño que suceda: que alguien le de por hacer efectos en color en alta resolucion, en capa ula,
			//y activar el scroll vertical. En teoria tambien puede hacer parpadeos en juegos normales, pero quien va a querer cambiar el scroll en juegos
			//que no estan preparados para hacer scroll?
			byte_leido=screen[direccion+pos_no_rainbow_pix_x];


			if (si_timex_8_1.v==0) {
				attribute=screen[dir_atributo+pos_no_rainbow_pix_x];	
			}

			else {
				//timex 8x1
				attribute=screen[direccion+pos_no_rainbow_pix_x+8192];
			}



		}

		else {

			//Modo sin scroll vertical. Permite scroll horizontal. Es modo rainbow

			

			//Pero si no tenemos scanline
			if (tbblue_store_scanlines.v==0) {
				byte_leido=screen[direccion+pos_no_rainbow_pix_x];
				attribute=screen[dir_atributo+pos_no_rainbow_pix_x];	
				indice_origen_bytes+=2;
			}

			else {
				byte_leido=puntero_buffer_atributos[indice_origen_bytes++];
				attribute=puntero_buffer_atributos[indice_origen_bytes++];
			}

		}



		//32 columnas
		//truncar siempre a modulo 64 (2 bytes: pixel y atributo)
		indice_origen_bytes %=64;

		if (si_timex_hires.v) {
			if ((x&1)==0) byte_leido=screen[direccion+pos_no_rainbow_pix_x];
			else byte_leido=screen[direccion+pos_no_rainbow_pix_x+8192];

			attribute=col6;
		}			
			
		get_pixel_color_tbblue(attribute,&ink,&paper);
			
    	for (bit=0;bit<8;bit++) {			
			color= ( byte_leido & 128 ? ink : paper ) ;

			int posx=x*8+bit; //Posicion pixel. Para clip window registers	
			if (si_timex_hires.v) posx /=2;

			//Tener en cuenta valor clip window
			
			//(W) 0x1A (26) => Clip Window ULA/LoRes
			if (posx>=clip_windows[TBBLUE_CLIP_WINDOW_ULA][0] && posx<=clip_windows[TBBLUE_CLIP_WINDOW_ULA][1] && scanline_copia>=clip_windows[TBBLUE_CLIP_WINDOW_ULA][2] && scanline_copia<=clip_windows[TBBLUE_CLIP_WINDOW_ULA][3]) {
				if (!tbblue_force_disable_layer_ula.v) {
					z80_int color_final=tbblue_get_palette_active_ula(color);

					//Ver si color resultante es el transparente de ula, y cambiarlo por el color transparente ficticio
					if (tbblue_si_transparent(color_final)) color_final=TBBLUE_SPRITE_TRANS_FICT;

					tbblue_layer_ula[posicion_array_layer]=color_final;
					if (si_timex_hires.v==0) tbblue_layer_ula[posicion_array_layer+1]=color_final; //doble de ancho

				}
			}

		
			posicion_array_layer++;
			if (si_timex_hires.v==0) posicion_array_layer++; //doble de ancho
        	byte_leido=byte_leido<<1;
				
      	}

		if (si_timex_hires.v) {
				if (x&1) {
					pos_no_rainbow_pix_x++;
					//direccion++;
				}
		}

		else {
			//direccion++;
			pos_no_rainbow_pix_x++;
		}


			
		pos_no_rainbow_pix_x %=32;		

	  }
	
}




void tbblue_do_ula_lores_overlay()
{


	//Render de capa ULA LORES
	//printf ("scan line de pantalla fisica (no border): %d\n",t_scanline_draw);

	//linea que se debe leer
	int scanline_copia=t_scanline_draw-screen_indice_inicio_pant;


	int color;

	/* modo lores
	(R/W) 0x15 (21) => Sprite and Layers system
  bit 7 - LoRes mode, 128 x 96 x 256 colours (1 = enabled)
  	*/

	  	

	z80_byte *lores_pointer;
	z80_byte posicion_x_lores_pointer;

	
	int linea_lores=scanline_copia;  
	//Sumamos offset y
	/*
	(R/W) 0x33 (51) => LoRes Offset Y
	bits 7-0 = Y Offset (0-191)(Reset to 0 after a reset)
	Being only 96 pixels, this allows the display to scroll in "half-pixels",
	at the same resolution and smoothness as Layer 2.
	*/
	linea_lores +=tbblue_registers[0x33];

	linea_lores=linea_lores % 192;
	//if (linea_lores>=192) linea_lores -=192;

	lores_pointer=get_lores_pointer(linea_lores/2);  //admite hasta y=95, dividimos entre 2 linea actual

	//Y scroll horizontal
	posicion_x_lores_pointer=tbblue_registers[0x32];
  		


	int posicion_array_layer=0;
	posicion_array_layer +=(screen_total_borde_izquierdo*border_enabled.v*2); //Doble de ancho


	int posx;
	z80_int color_final;

	for (posx=0;posx<256;posx++) {
				
		color=lores_pointer[posicion_x_lores_pointer/2];
		//tenemos indice color de paleta
		//transformar a color final segun paleta ula activa
		//color=tbblue_get_palette_active_ula(lorescolor);

		posicion_x_lores_pointer++; 
		//nota: dado que es una variable de 8 bits, automaticamente se trunca al pasar de 255 a 0, por tanto no hay que sacar el modulo de division con 256
		
		//Tener en cuenta valor clip window
		
		//(W) 0x1A (26) => Clip Window ULA/LoRes
		if (posx>=clip_windows[TBBLUE_CLIP_WINDOW_ULA][0] && posx<=clip_windows[TBBLUE_CLIP_WINDOW_ULA][1] && scanline_copia>=clip_windows[TBBLUE_CLIP_WINDOW_ULA][2] && scanline_copia<=clip_windows[TBBLUE_CLIP_WINDOW_ULA][3]) {
			if (!tbblue_force_disable_layer_ula.v) {
				color_final=tbblue_get_palette_active_ula(color);

				//Ver si color resultante es el transparente de ula, y cambiarlo por el color transparente ficticio
				if (tbblue_si_transparent(color_final)) color_final=TBBLUE_SPRITE_TRANS_FICT;

				tbblue_layer_ula[posicion_array_layer]=color_final;
				tbblue_layer_ula[posicion_array_layer+1]=color_final; //doble de ancho

			}
		}

		posicion_array_layer+=2; //doble de ancho
				
    }


}

//Guardar en buffer rainbow la linea actual. 
//Tener en cuenta que si border esta desactivado, la primera linea del buffer sera de display,
//en cambio, si border esta activado, la primera linea del buffer sera de border
void screen_store_scanline_rainbow_solo_display_tbblue(void)
{
	//Por si acaso en un futuro cambia ese valor
	if (TBBLUE_SPRITE_TRANS_FICT!=65535) cpu_panic("Changed transparent value. Can not do fast layer clear");

	//Tenemos que escribir en array de z80_int (2 bytes)
	int tamanyo_clear=TBBLUE_LAYERS_PIXEL_WIDTH*2;
	memset(tbblue_layer_ula,0xFF,tamanyo_clear);
	memset(tbblue_layer_layer2,0xFF,tamanyo_clear);
	memset(tbblue_layer_sprites,0xFF,tamanyo_clear);
	



	//int bordesupinf=0;

	int capalayer2=0;
	int capasprites=0;
	//int capatiles=0;

  	//En zona visible pantalla (no borde superior ni inferior)
  	if (t_scanline_draw>=screen_indice_inicio_pant && t_scanline_draw<screen_indice_fin_pant) {

			//int scanline_copia=t_scanline_draw-screen_indice_inicio_pant;


			int tbblue_lores=tbblue_registers[0x15] & 128;
			if (tbblue_lores) tbblue_do_ula_lores_overlay();
		  	else {
				if (tbblue_if_ula_is_enabled() ) {
				  tbblue_do_ula_standard_overlay();
				}
			}

		//Overlay de layer2
							//Capa layer2
				/*if (tbblue_is_active_layer2() && !tbblue_force_disable_layer_layer_two.v) {
					if (scanline_copia>=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][2] && scanline_copia<=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][3]) {
						capalayer2=1;
					
						tbblue_do_layer2_overlay();
						if (tbblue_reveal_layer_layer2.v) {
								tbblue_reveal_layer_draw(tbblue_layer_layer2);
						}
					}
				}*/

	}

	else {
		//bordesupinf=1;
	}

	//Aqui puede ser borde superior o inferior




		//Overlay de layer2
		//Capa layer2
				if (tbblue_is_active_layer2() && !tbblue_force_disable_layer_layer_two.v) {
                                        // TODO Hack for now as the logic below isn't enabling layer2 correctly
                                        capalayer2 = 1;

					int y_layer2=t_scanline_draw; //0..63 es border (8 no visibles);
					int border_no_visible=screen_indice_inicio_pant-TBBLUE_LAYER2_12_BORDER;


					int layer2_resolution=(tbblue_registers[112]>>4) & 3; 

					if (layer2_resolution>0) {
						y_layer2 -=border_no_visible;
					}
					else {
						y_layer2 -=screen_indice_inicio_pant;
					}

					int dibujar=0;


					if (layer2_resolution==0) {
						if (t_scanline_draw>=screen_indice_inicio_pant && t_scanline_draw<screen_indice_fin_pant) {
							if (y_layer2>=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][2] && y_layer2<=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][3]) {
								dibujar=1;
							}
						}
					}

					else if (y_layer2>=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][2] && y_layer2<=clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][3]) {
						dibujar=1;
					}

					if (dibujar) {
						capalayer2=1;
					
						tbblue_do_layer2_overlay(y_layer2);


					

						if (tbblue_reveal_layer_layer2.v) {
								tbblue_reveal_layer_draw(tbblue_layer_layer2);
						}
					}

								
				}

		
		//Capa de tiles. Mezclarla directamente a la capa de ula tbblue_layer_ula


	if ( tbblue_if_tilemap_enabled() && tbblue_force_disable_layer_tilemap.v==0) {
		int y_tile=t_scanline_draw; //0..63 es border (8 no visibles)
		int border_no_visible=screen_indice_inicio_pant-TBBLUE_TILES_BORDER;
		y_tile-=border_no_visible;

				/*
				The tilemap display surface extends 32 pixels around the central 256×192 display.
The origin of the clip window is the top left corner of this area 32 pixels to the left and 32 pixels above 
the central 256×192 display. The X coordinates are internally doubled to cover the full 320 pixel width of the surface.
 The clip window indicates the portion of the tilemap display that is non-transparent and its indicated extent is inclusive; 
 it will extend from X1*2 to X2*2+1 horizontally and from Y1 to Y2 vertically.
			*/

			//Tener en cuenta clip window
		if (y_tile>=clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][2] && y_tile<=clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][3]) {
			//capatiles=1;
			tbblue_do_tile_overlay(y_tile);
		}
	}


						if (tbblue_reveal_layer_ula.v) {
								tbblue_reveal_layer_draw(tbblue_layer_ula);
						}



	//capa sprites. Si clip window y corresponde:
	z80_byte sprites_over_border=tbblue_registers[21]&2;
	//Clip window on Sprites only work when the "over border bit" is disabled
	int mostrar_sprites=tbblue_registers[0x15] & 0x1;
	if (sprites_over_border==0) {
		int scanline_copia=t_scanline_draw-screen_indice_inicio_pant;
		if (scanline_copia<clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][2] || scanline_copia>clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][3]) mostrar_sprites=0;
	}

	
	if (mostrar_sprites && !tbblue_force_disable_layer_sprites.v) {
		capasprites=1;
		tbsprite_do_overlay();

						if (tbblue_reveal_layer_sprites.v) {
								tbblue_reveal_layer_draw(tbblue_layer_sprites);
						}

	}




  //Renderizamos las 3 capas buffer rainbow
	tbblue_render_layers_rainbow(capalayer2,capasprites);



}



void screen_tbblue_refresca_pantalla_comun_tbblue(int x,int y,unsigned int color)
{

        int dibujar=0;

        //if (x>255) dibujar=1;
        //else if (y>191) dibujar=1;
        if (scr_ver_si_refrescar_por_menu_activo(x/8,y/8)) dibujar=1;

        if (dibujar) {
		scr_putpixel_zoom(x,y,color);
                scr_putpixel_zoom(x,y+1,color);
                scr_putpixel_zoom(x+1,y,color);
                scr_putpixel_zoom(x+1,y+1,color);
        }
}


//Refresco pantalla sin rainbow para tbblue
void screen_tbblue_refresca_pantalla_comun(void)
{
        int x,y,bit;
        z80_int direccion,dir_atributo;
        z80_byte byte_leido;
        int color=0;
        int fila;
        //int zx,zy;

        z80_byte attribute,ink,paper,bright,flash,aux;


       z80_byte *screen=get_base_mem_pantalla();

        //printf ("dpy=%x ventana=%x gc=%x image=%x\n",dpy,ventana,gc,image);
        z80_byte x_hi;

        for (y=0;y<192;y++) {
                //direccion=16384 | devuelve_direccion_pantalla(0,y);

                //direccion=16384 | screen_addr_table[(y<<5)];
                direccion=screen_addr_table[(y<<5)];


                fila=y/8;
                dir_atributo=6144+(fila*32);
                for (x=0,x_hi=0;x<32;x++,x_hi +=8) {



                                byte_leido=screen[direccion];
                                attribute=screen[dir_atributo];


                                ink=attribute &7;
                                paper=(attribute>>3) &7;
											bright=(attribute) &64;
                                flash=(attribute)&128;
                                if (flash) {
                                        //intercambiar si conviene
                                        if (estado_parpadeo.v) {
                                                aux=paper;
                                                paper=ink;
                                                ink=aux;
                                        }
                                }

                                if (bright) {
                                        ink +=8;
                                        paper +=8;
                                }

                                for (bit=0;bit<8;bit++) {

                                        color= ( byte_leido & 128 ? ink : paper );

					//Por cada pixel, hacer *2s en ancho y alto.
					//Esto es muy simple dado que no soporta modo rainbow y solo el estandard 256x192
					screen_tbblue_refresca_pantalla_comun_tbblue((x_hi+bit)*2,y*2,color);
		

                                        byte_leido=byte_leido<<1;
                                }
                        

     
                        direccion++;
                        dir_atributo++;
                }

        }

}



void screen_tbblue_refresca_no_rainbow_border(void)
{
	int color;

	color=out_254 & 7;

	if (scr_refresca_sin_colores.v) color=7;

int x,y;



       //parte superior
        for (y=0;y<TBBLUE_TOP_BORDER;y++) {
                for (x=0;x<TBBLUE_DISPLAY_WIDTH*zoom_x+TBBLUE_LEFT_BORDER*2;x++) {
                                scr_putpixel(x,y,color);


                }
        }

        //parte inferior
        for (y=0;y<TBBLUE_TOP_BORDER;y++) {
                for (x=0;x<TBBLUE_DISPLAY_WIDTH*zoom_x+TBBLUE_LEFT_BORDER*2;x++) {
                                scr_putpixel(x,TBBLUE_TOP_BORDER+y+TBBLUE_DISPLAY_HEIGHT*zoom_y,color);


                }
        }


        //laterales
        for (y=0;y<TBBLUE_DISPLAY_HEIGHT*zoom_y;y++) {
                for (x=0;x<TBBLUE_LEFT_BORDER;x++) {
                        scr_putpixel(x,TBBLUE_TOP_BORDER+y,color);
                        scr_putpixel(TBBLUE_LEFT_BORDER+TBBLUE_DISPLAY_WIDTH*zoom_x+x,TBBLUE_TOP_BORDER+y,color);
                }

        }



}


//Refresco pantalla con rainbow. Nota. esto deberia ser una funcion comun y no tener diferentes para comun, prism, tbblue, etc
void screen_tbblue_refresca_rainbow(void)
{


	int ancho,alto;

	ancho=get_total_ancho_rainbow();
	alto=get_total_alto_rainbow();

	int x,y,bit;

	//margenes de zona interior de pantalla. Para overlay menu
	int margenx_izq=TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v;
	int margenx_der=TBBLUE_LEFT_BORDER_NO_ZOOM*border_enabled.v+TBBLUE_DISPLAY_WIDTH;
	int margeny_arr=TBBLUE_TOP_BORDER_NO_ZOOM*border_enabled.v;
	int margeny_aba=TBBLUE_BOTTOM_BORDER_NO_ZOOM*border_enabled.v+TBBLUE_DISPLAY_HEIGHT;

	z80_int color_pixel;
	z80_int *puntero;
        z80_int *even_puntero;

	even_puntero=rainbow_buffer;
	int dibujar;

	for (y=0;y<alto;y++) {

                puntero = even_puntero;

		for (x=0;x<ancho;x+=8) {
// TODO Can't see any problem with commenting out this at the moment
/*
			dibujar=1;

			//Ver si esa zona esta ocupada por texto de menu u overlay
			if (y>=margeny_arr && y<margeny_aba && x>=margenx_izq && x<margenx_der) {
				if (!scr_ver_si_refrescar_por_menu_activo( (x-margenx_izq)/8, (y-margeny_arr)/8) )
					dibujar=0;
			}

			if (dibujar==1) */ {
					for (bit=0;bit<8;bit++) {
						color_pixel=*puntero++;
						scr_putpixel_zoom_rainbow(x+bit,y,color_pixel);
					}
			}
//			else puntero+=8;

		}
		
                if (y & 1) even_puntero = puntero;
	}


}





void screen_tbblue_refresca_no_rainbow(void)
{
                //modo clasico. sin rainbow
                if (rainbow_enabled.v==0) {
                        if (border_enabled.v) {
                                //ver si hay que refrescar border
                                if (modificado_border.v)
                                {
                                        //scr_refresca_border();
																				screen_tbblue_refresca_no_rainbow_border();
                                        modificado_border.v=0;
                                }

                        }

                        screen_tbblue_refresca_pantalla_comun();
                }
}

