/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <string.h>
#include <stdio.h>

#include "autoselectoptions.h"
#include "debug.h"
#include "cpu.h"
#include "screen.h"
#include "ay38912.h"
#include "joystick.h"
#include "realjoystick.h"
#include "ulaplus.h"
#include "menu.h"
#include "utils.h"




//Para indicar que cinta/snapshot se ha detectado en la base de datos y se ha impreso en el footer el primer texto
int tape_options_set_first_message_counter=0;

//Para indicar que cinta/snapshot se ha detectado en la base de datos y se ha impreso en el footer el segundo texto
int tape_options_set_second_message_counter=0;

//Puntero a nombre del juego
char *mostrar_footer_game_name;

//Nombre del juego. donde se guarda el texto final
char texto_mostrar_footer_game_name[AUTOSELECTOPTIONS_MAX_FOOTER_LENGTH];

//texto mostrado en pantalla para primer mensaje.
//aunque en teoria deberia ser de 32, lo hacemos mayor porque se usa como buffer temporal y puede exceder 32
char mostrar_footer_first_message_mostrado[AUTOSELECTOPTIONS_MAX_FOOTER_LENGTH];

//texto entero para el primer mensaje
char mostrar_footer_first_message[AUTOSELECTOPTIONS_MAX_FOOTER_LENGTH];

//Indices de texto mostrado. Usados cuando hay que desplazar texto hacia la izquierda porque no cabe
int indice_first_message_mostrado,indice_second_message_mostrado;

//puntero a texto entero para el segundo mensaje
char *mostrar_footer_second_message;

//texto entero para el segundo mensaje
char texto_mostrar_footer_second_message[255];

//texto mostrado en pantalla para segundo mensaje.
//aunque en teoria deberia ser de 32, lo hacemos mayor porque se usa como buffer temporal y puede exceder 32
char mostrar_footer_second_message_mostrado[255];

void set_snaptape_filemachine_setreset(char *message,int machine)
{
                if (current_machine_type!=machine) {
                        debug_printf (VERBOSE_INFO,message);
                        current_machine_type=machine;
                        set_machine(NULL);
                        reset_cpu();
                }

}

//Auxiliar para obtener nombre e info del juego
//Separa las dos partes con un punto
//Si el juego tiene un . , se debe establecer manualmente mostrar_footer_game_name y mostrar_footer_game_info
void split_game_name_info(char *s)
{

	//split_game_name_info ("game with rainbow effects. Enabling Real Video");
	debug_printf (VERBOSE_INFO,"Detected %s",s);

	//Buscar hasta el primer punto
	int i=0;

	while (s[i]!=0 && s[i]!='.') {
		i++;
		//printf ("%d\n",i);
	}

	sprintf (texto_mostrar_footer_game_name,"%s",s);
	//Puntero apunta al buffer de texto
	mostrar_footer_game_name=texto_mostrar_footer_game_name;

	mostrar_footer_game_name[i]=0;

	//Hay info?
	if (s[i]!=0 && s[i+1]!=0) {
		//Si lo que hay es un espacio, avanzar a siguiente posicion
		if (s[i+1]==' ') i++;

		sprintf (texto_mostrar_footer_second_message,"%s",&s[i+1]);
		//Puntero apunta al buffer de texto
		mostrar_footer_second_message=texto_mostrar_footer_second_message;
	}

}

void put_footer_first_message(char *mensaje)
{

	//Indices de desplazamiento a cero
	indice_first_message_mostrado=indice_second_message_mostrado=0;

	//Por defecto dejar los contadores a cero

        strcpy(mostrar_footer_first_message,mensaje);
       

		//Texto mostrado
		sprintf (mostrar_footer_first_message_mostrado,"%s",mostrar_footer_first_message);

		//Cortar a 32

		autoselect_options_put_footer();

		
	
}

//Pone el footer segun lo que corresponda, si primer mensaje o segundo mensaje activo
void autoselect_options_put_footer(void)
{
}


void tape_options_corta_a_32(char *s)
{
	                int longitud_mensaje=strlen(s);
                        //Ver si longitud excede 32
                        if (longitud_mensaje>32) {
                                s[32]=0;
                        }

}

//Desplazar texto mostrado
void desplaza_texto(int *indice, char *cadena_entera, char *cadena_mostrada)
{

	int i=*indice;
	i++;

	*indice=i;
	//printf ("indice: %d\n",i);
	//Y copiamos 32 caracteres
	sprintf (cadena_mostrada,"%s",&cadena_entera[i]);
	cadena_mostrada[32]=0;
}


//Primer mensaje ya ha pasado 4 segundos en pantalla. Desplazar si no ha cabido entero o borrarlo
void delete_tape_options_set_first_message(void)
{
	//De momento borrar lo que haya
	menu_putstring_footer(0,2,"                                ",WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);


        //Hacer que el first message se desplace a la izquierda si no ha cabido entero
        int longitud_mostrada=strlen(mostrar_footer_first_message_mostrado);
        int longitud_real=strlen(&mostrar_footer_first_message[indice_first_message_mostrado]);
        if (longitud_real>longitud_mostrada) {
			desplaza_texto(&indice_first_message_mostrado,mostrar_footer_first_message,mostrar_footer_first_message_mostrado);

                        //Metemos contador a 1 segundo

			autoselect_options_put_footer();

			

                        return;
	}



	//No hay que desplazar primer mensaje. Mostrar segundo mensaje, si hay
	if (mostrar_footer_second_message!=NULL) {
		sprintf (mostrar_footer_second_message_mostrado,"%s",mostrar_footer_second_message);

		autoselect_options_put_footer();

	

	}

	//No hay segundo mensaje
	else {

		//Aunque para Z88 no se llama a funcion de set_snaptape_fileoptions, y no haria falta repintar zona footer Z88,
		//lo hacemos por si en un futuro para alguna eprom de Z88 se llama aqui
		menu_footer_bottom_line();
	}
}

//Segundo mensaje ya ha pasado 4 segundos en pantalla. Desplazar si no ha cabido entero o borrarlo
void delete_tape_options_set_second_message(void)
{
	//De momento borrar lo que haya
        menu_putstring_footer(0,2,"                                ",WINDOW_FOOTER_INK,WINDOW_FOOTER_PAPER);

	//Hacer que el second message se desplace a la izquierda si no ha cabido entero
	int longitud_mostrada=strlen(mostrar_footer_second_message_mostrado);

	debug_printf (VERBOSE_DEBUG,"second message: index: %d text: %s",indice_second_message_mostrado,&mostrar_footer_second_message[indice_second_message_mostrado]);

	int longitud_real=strlen(&mostrar_footer_second_message[indice_second_message_mostrado]);
	if (longitud_real>longitud_mostrada) {
			desplaza_texto(&indice_second_message_mostrado,mostrar_footer_second_message,mostrar_footer_second_message_mostrado);

			//Metemos contador a 1 segundo

			//Y mostrarlo
			autoselect_options_put_footer();



			return;
	}

	//Aunque para Z88 no se llama a funcion de set_snaptape_fileoptions, y no haria falta repintar zona footer Z88,
        //lo hacemos por si en un futuro para alguna eprom de Z88 se llama aqui
        menu_footer_bottom_line();
}
