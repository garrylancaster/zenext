/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "tbblue.h"
#include "mem128.h"
#include "debug.h"
#include "contend.h"
#include "utils.h"
#include "menu.h"
#include "divmmc.h"
#include "diviface.h"
#include "screen.h"

#include "timex.h"
#include "ula.h"
#include "audio.h"

#include "datagear.h"
#include "ay38912.h"
#include "multiface.h"
#include "uartbridge.h"
#include "chardevice.h"
#include "settings.h"
#include "joystick.h"


z80_byte target_nextreg;
z80_byte target_value;


void set_nextreg_02(z80_byte value)
{
        tbblue_registers[0x02] = (tbblue_registers[0x02] & 0x73)
                                | (value & 0x8c);
        // TODO Bit 7

        if (value & 0x02) {
                hard_reset_cpu();
        }
        else if (value & 0x01) {
                soft_reset_cpu();
        }
        else if (value & 0x08) {
                generate_nmi_multiface_tbblue();
        }
        else if (value & 0x04) {
                generate_nmi_drive();
        }
}


void set_nextreg_03(z80_byte value)
{
        z80_byte machine_type = tbblue_registers[3] & 0x07;

        // Write to 0x03 disables the bootrom.
        tbblue_bootrom.v=0;

        // Change timing if bit 7 is set, and user lock (bit 3) is not
        // set in current or new value.
        if (((value & 0x88) == 0x80)
                && ((tbblue_registers[3] & 0x08) == 0))
        {
                tbblue_registers[3] = (tbblue_registers[3] & 0x8f)
                                        | (value & 0x70);

                z80_byte timing = (value >> 4) & 0x07;

                switch (timing)
                {
                        case 0:
                        case 1:
                                // 48K
                                debug_printf (VERBOSE_INFO,"Apply config.timing. change:48k");
                                tbblue_change_timing(0);
                                break;

                        case 2: // 128K
                        default: // TODO other timings (+3, Pentagon)
                                debug_printf (VERBOSE_INFO,"Apply config.timing. change:128k");
                                tbblue_change_timing(1);
                                break;
                }
        }

        // Toggle user timing lock.
        tbblue_registers[3] ^= (value & 0x08);


        // Change machine type, in config mode only.
        if (machine_type==0)
        {
                tbblue_registers[3] = (tbblue_registers[3] & 0xfc)
                                        | (value & 0x03);
        }

        tbblue_touch_mmu01();
}


void set_nextreg_04(z80_byte value)
{
        tbblue_registers[0x04] = value & 0x7f;
        tbblue_touch_mmu01();
}


void set_nextreg_05(z80_byte value)
{
        if ((tbblue_registers[5] & 0x04) != (value & 0x04))
        {
                tbblue_splash_monitor_mode();
        }

        // TODO I/O mode
        tbblue_registers[0x05] = value;
}


void set_nextreg_06(z80_byte value)
{
        z80_byte last_register_6 = tbblue_registers[0x06];
        tbblue_registers[0x06] = value;

        if ( (last_register_6&8) != (value&8)) tbblue_set_emulator_setting_multiface();
}


void set_nextreg_07(z80_byte value)
{
        z80_byte last_register_7 = tbblue_registers[0x07];
        tbblue_registers[0x07] = value;

        if ( last_register_7 != value ) tbblue_set_emulator_setting_turbo();
}


void set_nextreg_08(z80_byte value)
{
        tbblue_registers[0x08] = value;

        // bit 7 = 128K paging enable
        if (value & 0x80)
        {
                // Disable paging lock.
                puerto_32765 &= 0xdf;
        }
        else
        {
                // Don't change paging lock, but value should reflect it.
                tbblue_registers[8] &= 0x7f;
                if ((puerto_32765 & 0x20) == 0)
                {
                        tbblue_registers[8] |= 0x80;
                }
        }

        //bit 6 = "1" to disable RAM contention. (0 after a reset)
        if (value&64) {
                if (contend_enabled.v) {
                        debug_printf (VERBOSE_DEBUG,"Disabling contention");
                contend_enabled.v=0;
                inicializa_tabla_contend();
                }
        }

        else {
                if (contend_enabled.v==0) {
                        debug_printf (VERBOSE_DEBUG,"Enabling contention");
                contend_enabled.v=1;
                inicializa_tabla_contend();
                }               

        }

        //bit 5 = Stereo mode (0 = ABC, 1 = ACB)(0 after a PoR or Hard-reset)
        //ay3_stereo_mode;
        //1=ACB Stereo (Canal A=Izq,Canal C=Centro,Canal B=Der)
    //2=ABC Stereo (Canal A=Izq,Canal B=Centro,Canal C=Der)       
        if (value&32) {
                //ACB
                ay3_stereo_mode=1;
                debug_printf (VERBOSE_DEBUG,"Setting ACB stereo");
        }
        else {
                //ABC
                ay3_stereo_mode=2;
                debug_printf (VERBOSE_DEBUG,"Setting ABC stereo");
        }

  
        //bit 4 = Enable internal speaker (1 = enabled)(1 after a PoR or Hard-reset)
        if (value&16) {
                beeper_enabled.v=1;
                debug_printf (VERBOSE_DEBUG,"Enabling beeper");
        }
        else {
                beeper_enabled.v=0;
                debug_printf (VERBOSE_DEBUG,"Disabling beeper");
        }

        //bit 3 = Enable Specdrum/Covox (1 = enabled)(0 after a PoR or Hard-reset)
        if (value&8) {
                audiodac_enabled.v=1;
                audiodac_selected_type=0;
                debug_printf (VERBOSE_DEBUG,"Enabling audiodac Specdrum");
        }
        else {
                audiodac_enabled.v=0;
                debug_printf (VERBOSE_DEBUG,"Disabling audiodac Specdrum");
        }


        //bit 2 = Enable Timex modes (1 = enabled)(0 after a PoR or Hard-reset)
        if (value&4) {

                /*
                Desactivamos esto, pues NextOS al arrancar activa modo timex, y por tanto, el real video
                Con real video activado, usa mucha mas cpu
                Quitando esto, arrancara NextOS sin forzar a activar modo timex ni real video y por tanto usara menos cpu
                Si alguien quiere modo timex y/o real video, que lo habilite a mano
                debug_printf (VERBOSE_DEBUG,"Enabling timex video");
                enable_timex_video();
                */
        }
        else {
                /*
                debug_printf (VERBOSE_DEBUG,"Disabling timex video");
                disable_timex_video();
                */
        }
        
        //bit 1 = Enable TurboSound (1 = enabled)(0 after a PoR or Hard-reset)
        if (value &2) set_total_ay_chips(3);
        else set_total_ay_chips(1);
}


void set_nextreg_09(z80_byte value)
{
        tbblue_registers[0x09] = value;

        if (value & 8) {   
                diviface_write_control_register(
                        diviface_read_control_register() & 0xbf);
        }
}


void set_nextreg_0a(z80_byte value)
{
        tbblue_registers[0x0a] = value;

        tbblue_set_emulator_setting_divmmc();
        // TODO other bits
}


void set_nextreg_11(z80_byte value)
{
        if ((tbblue_registers[0x11] & 0x07) != (value & 0x07))
        {
                tbblue_splash_monitor_mode();
        }

        tbblue_registers[0x11] = value & 0x07;
}


void set_nextreg_12(z80_byte value)
{
        tbblue_registers[0x12] = value;
        tbblue_touch_mmu012345();
}


void set_nextreg_13(z80_byte value)
{
        tbblue_registers[0x13] = value;
        tbblue_touch_mmu012345();
}


void set_nextreg_18(z80_byte value)
{
        clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][tbblue_get_clip_window_layer2_index()]=value;
        tbblue_inc_clip_window_layer2_index();
}


void set_nextreg_19(z80_byte value)
{
        clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][tbblue_get_clip_window_sprites_index()]=value;
        tbblue_inc_clip_window_sprites_index();
}


void set_nextreg_1a(z80_byte value)
{
        clip_windows[TBBLUE_CLIP_WINDOW_ULA][tbblue_get_clip_window_ula_index()]=value;
        tbblue_inc_clip_window_ula_index();
}


void set_nextreg_1b(z80_byte value)
{
        clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][tbblue_get_clip_window_tilemap_index()]=value;
        tbblue_inc_clip_window_tilemap_index();
}


void set_nextreg_1c(z80_byte value)
{
        if (value&1) tbblue_reset_clip_window_layer2_index();
        if (value&2) tbblue_reset_clip_window_sprites_index();
        if (value&4) tbblue_reset_clip_window_ula_index();
        if (value&8) tbblue_reset_clip_window_tilemap_index();
}


void set_nextreg_2d(z80_byte value)
{
        if (audiodac_enabled.v) audiodac_send_sample_value(value);
}


void set_nextreg_2f(z80_byte value)
{
        tbblue_registers[0x2f] = value & 0x03;
}


void set_nextreg_34(z80_byte value)
{
        tbblue_registers[0x34] = value & 0x7f;

        if (tbsprite_is_lockstep()) {
                tbblue_out_port_sprite_index(value);
        } else {
                tbsprite_nr_index_sprite=value%TBBLUE_MAX_SPRITES;
        }
}


void set_nextreg_spriteattrib(z80_byte value)
{
        int attribute_id = (target_nextreg - 0x35) & 0x7;   //0..4
        int sprite_id = tbsprite_is_lockstep() ? tbsprite_index_sprite : tbsprite_nr_index_sprite;

        tbsprite_sprites[sprite_id][attribute_id] = value;

        if (target_nextreg > 0x70)
        {
                if (tbsprite_is_lockstep()) {
                        tbsprite_increment_index_303b();
                } else {
                        ++tbsprite_nr_index_sprite;
                        tbsprite_nr_index_sprite%=TBBLUE_MAX_SPRITES;
                }
        }
}


void set_nextreg_40(z80_byte value)
{
        tbblue_registers[0x40] = value;
        tbblue_reset_palette_write_state();
}


void set_nextreg_41(z80_byte value)
{
        tbblue_write_palette_value_high8(value);
        tbblue_increment_palette_index();
}


void set_nextreg_44(z80_byte value)
{
        tbblue_write_palette_value_high8_low1(value);
}


void set_nextreg_4c(z80_byte value)
{
        tbblue_registers[0x4c] = value & 0x0f;
}


void set_nextreg_60(z80_byte value)
{
        tbblue_copper_write_data(value);
}


void set_nextreg_62(z80_byte value)
{
        tbblue_registers[0x62] = value;
        tbblue_copper_write_control_hi_byte(value);
}


void set_nextreg_63(z80_byte value)
{
        z80_byte last_value = tbblue_registers[0x63];
        tbblue_registers[0x63] = value;
        tbblue_copper_write_data_16b(last_value, value);
}


void set_nextreg_69(z80_byte value)
{
        tbblue_registers[0x69] = value;
        tbblue_sync_display1_reg_to_others(value);
}


void set_nextreg_6a(z80_byte value)
{
        tbblue_registers[0x6a] = value & 0x3f;
}


void set_nextreg_70(z80_byte value)
{
        tbblue_registers[0x70] = value & 0x3f;
}


void set_nextreg_71(z80_byte value)
{
        tbblue_registers[0x71] = value & 0x01;
}


void set_nextreg_8c(z80_byte value)
{
        tbblue_registers[0x8c] = value;
        tbblue_touch_mmu01();
}


void set_nextreg_8e(z80_byte value)
{
        tbblue_registers[0x8e] = value;
        z80_byte new_1ffd = puerto_8189;
        z80_byte new_7ffd = puerto_32765;

        // TODO
        // bit 7 = port 0xdffd bit 0         \  RAM

        if (value & 0x08)
        {
                // bit 3 = 1: change RAM bank to bits 6..4.
                new_7ffd &= 0xf8;
                new_7ffd |= (value >> 4) & 0x07;
        }

        if (value & 0x04)
        {
                // bit 2 = 1: set allRAM mode.
                // bit 1 = port 0x1ffd bit 2         \  all
                // bit 0 = port 0x1ffd bit 1         /  RAM
                new_1ffd &= 0xf8;
                new_1ffd |= ((value & 0x03) << 1) | 0x01;
        }
        else
        {
                // bit 2 = 0: set standard mode.
                // bit 1 = port 0x1ffd bit 2         \  ROM
                // bit 0 = port 0x7ffd bit 4         /  select
                new_1ffd &= 0xf8;
                new_1ffd |= (value & 0x02) << 1;

                new_7ffd &= 0xef;
                new_7ffd |= (value & 0x01) << 4;
        }

        tbblue_update_memory_ports(new_1ffd, new_7ffd, value & 0x08);
}


void set_nextreg_undefined(z80_byte value)
{
        // Prevent warning.
        target_value = value;
}

void set_nextreg_readonly(z80_byte value)
{
        // Prevent warning.
        target_value = value;
}

void set_nextreg_simple(z80_byte value)
{
        tbblue_registers[target_nextreg] = value;
}

void (*set_nextreg[])(z80_byte value) =
{
        set_nextreg_readonly,                   // 0x00
        set_nextreg_readonly,                   // 0x01
        set_nextreg_02,                         // 0x02
        set_nextreg_03,                         // 0x03
        set_nextreg_04,                         // 0x04
        set_nextreg_05,                         // 0x05
        set_nextreg_06,                         // 0x06
        set_nextreg_07,                         // 0x07
        set_nextreg_08,                         // 0x08
        set_nextreg_09,                         // 0x09
        set_nextreg_0a,                         // 0x0a
        set_nextreg_undefined,                  // 0x0b
        set_nextreg_undefined,                  // 0x0c
        set_nextreg_undefined,                  // 0x0d
        set_nextreg_readonly,                   // 0x0e
        set_nextreg_undefined,                  // 0x0f

        set_nextreg_undefined,                  // 0x10 TODO
        set_nextreg_11,                         // 0x11
        set_nextreg_12,                         // 0x12
        set_nextreg_13,                         // 0x13
        set_nextreg_simple,                     // 0x14
        set_nextreg_simple,                     // 0x15
        set_nextreg_simple,                     // 0x16
        set_nextreg_simple,                     // 0x17
        set_nextreg_18,                         // 0x18
        set_nextreg_19,                         // 0x19
        set_nextreg_1a,                         // 0x1a
        set_nextreg_1b,                         // 0x1b
        set_nextreg_1c,                         // 0x1c
        set_nextreg_undefined,                  // 0x1d
        set_nextreg_readonly,                   // 0x1e
        set_nextreg_readonly,                   // 0x1f

        set_nextreg_undefined,                  // 0x20
        set_nextreg_undefined,                  // 0x21
        set_nextreg_undefined,                  // 0x22 TODO
        set_nextreg_simple,                     // 0x23
        set_nextreg_undefined,                  // 0x24
        set_nextreg_undefined,                  // 0x25
        set_nextreg_simple,                     // 0x26
        set_nextreg_simple,                     // 0x27
        set_nextreg_undefined,                  // 0x28 TODO
        set_nextreg_undefined,                  // 0x29 TODO
        set_nextreg_undefined,                  // 0x2a TODO
        set_nextreg_undefined,                  // 0x2b TODO
        set_nextreg_undefined,                  // 0x2c TODO
        set_nextreg_2d,                         // 0x2d
        set_nextreg_undefined,                  // 0x2e TODO
        set_nextreg_2f,                         // 0x2f

        set_nextreg_simple,                     // 0x30
        set_nextreg_simple,                     // 0x31
        set_nextreg_simple,                     // 0x32
        set_nextreg_simple,                     // 0x33
        set_nextreg_34,                         // 0x34
        set_nextreg_spriteattrib,               // 0x35
        set_nextreg_spriteattrib,               // 0x36
        set_nextreg_spriteattrib,               // 0x37
        set_nextreg_spriteattrib,               // 0x38
        set_nextreg_spriteattrib,               // 0x39
        set_nextreg_undefined,                  // 0x3a
        set_nextreg_undefined,                  // 0x3b
        set_nextreg_undefined,                  // 0x3c
        set_nextreg_undefined,                  // 0x3d
        set_nextreg_undefined,                  // 0x3e
        set_nextreg_undefined,                  // 0x3f

        set_nextreg_40,                         // 0x40
        set_nextreg_41,                         // 0x41
        set_nextreg_simple,                     // 0x42
        set_nextreg_simple,                     // 0x43
        set_nextreg_44,                         // 0x44
        set_nextreg_undefined,                  // 0x45
        set_nextreg_undefined,                  // 0x46
        set_nextreg_undefined,                  // 0x47
        set_nextreg_undefined,                  // 0x48
        set_nextreg_undefined,                  // 0x49
        set_nextreg_simple,                     // 0x4a
        set_nextreg_simple,                     // 0x4b
        set_nextreg_4c,                         // 0x4c
        set_nextreg_undefined,                  // 0x4d
        set_nextreg_undefined,                  // 0x4e
        set_nextreg_undefined,                  // 0x4f

        tbblue_set_mmu0,                        // 0x50
        tbblue_set_mmu1,                        // 0x51
        tbblue_set_mmu2,                        // 0x52
        tbblue_set_mmu3,                        // 0x53
        tbblue_set_mmu4,                        // 0x54
        tbblue_set_mmu5,                        // 0x55
        tbblue_set_mmu6,                        // 0x56
        tbblue_set_mmu7,                        // 0x57
        set_nextreg_undefined,                  // 0x58
        set_nextreg_undefined,                  // 0x59
        set_nextreg_undefined,                  // 0x5a
        set_nextreg_undefined,                  // 0x5b
        set_nextreg_undefined,                  // 0x5c
        set_nextreg_undefined,                  // 0x5d
        set_nextreg_undefined,                  // 0x5e
        set_nextreg_undefined,                  // 0x5f

        set_nextreg_60,                         // 0x60
        set_nextreg_simple,                     // 0x61
        set_nextreg_62,                         // 0x62
        set_nextreg_63,                         // 0x63
        set_nextreg_simple,                     // 0x64
        set_nextreg_undefined,                  // 0x65
        set_nextreg_undefined,                  // 0x66
        set_nextreg_undefined,                  // 0x67
        set_nextreg_simple,                     // 0x68
        set_nextreg_69,                         // 0x69
        set_nextreg_6a,                         // 0x6a
        set_nextreg_simple,                     // 0x6b
        set_nextreg_simple,                     // 0x6c
        set_nextreg_undefined,                  // 0x6d
        set_nextreg_simple,                     // 0x6e
        set_nextreg_simple,                     // 0x6f

        set_nextreg_70,                         // 0x70
        set_nextreg_71,                         // 0x71
        set_nextreg_undefined,                  // 0x72
        set_nextreg_undefined,                  // 0x73
        set_nextreg_undefined,                  // 0x74
        set_nextreg_spriteattrib,               // 0x75
        set_nextreg_spriteattrib,               // 0x76
        set_nextreg_spriteattrib,               // 0x77
        set_nextreg_spriteattrib,               // 0x78
        set_nextreg_spriteattrib,               // 0x79
        set_nextreg_undefined,                  // 0x7a
        set_nextreg_undefined,                  // 0x7b
        set_nextreg_undefined,                  // 0x7c
        set_nextreg_undefined,                  // 0x7d
        set_nextreg_undefined,                  // 0x7e
        set_nextreg_simple,                     // 0x7f

        set_nextreg_simple,                     // 0x80 TODO
        set_nextreg_simple,                     // 0x81 TODO
        set_nextreg_simple,                     // 0x82 TODO
        set_nextreg_simple,                     // 0x83 TODO
        set_nextreg_simple,                     // 0x84 TODO
        set_nextreg_simple,                     // 0x85 TODO
        set_nextreg_simple,                     // 0x86 TODO
        set_nextreg_simple,                     // 0x87 TODO
        set_nextreg_simple,                     // 0x88 TODO
        set_nextreg_simple,                     // 0x89 TODO
        set_nextreg_simple,                     // 0x8a TODO
        set_nextreg_undefined,                  // 0x8b
        set_nextreg_8c,                         // 0x8c
        set_nextreg_undefined,                  // 0x8d
        set_nextreg_8e,                         // 0x8e
        set_nextreg_undefined,                  // 0x8f TODO

        set_nextreg_undefined,                  // 0x90 TODO
        set_nextreg_undefined,                  // 0x91 TODO
        set_nextreg_undefined,                  // 0x92 TODO
        set_nextreg_undefined,                  // 0x93 TODO
        set_nextreg_undefined,                  // 0x94
        set_nextreg_undefined,                  // 0x95
        set_nextreg_undefined,                  // 0x96
        set_nextreg_undefined,                  // 0x97
        set_nextreg_undefined,                  // 0x98 TODO
        set_nextreg_undefined,                  // 0x99 TODO
        set_nextreg_undefined,                  // 0x9a TODO
        set_nextreg_undefined,                  // 0x9b TODO
        set_nextreg_undefined,                  // 0x9c
        set_nextreg_undefined,                  // 0x9d
        set_nextreg_undefined,                  // 0x9e
        set_nextreg_undefined,                  // 0x9f

        set_nextreg_undefined,                  // 0xa0 TODO
        set_nextreg_undefined,                  // 0xa1
        set_nextreg_undefined,                  // 0xa2 TODO
        set_nextreg_undefined,                  // 0xa3 TODO
        set_nextreg_undefined,                  // 0xa4
        set_nextreg_undefined,                  // 0xa5
        set_nextreg_undefined,                  // 0xa6
        set_nextreg_undefined,                  // 0xa7
        set_nextreg_undefined,                  // 0xa8 TODO
        set_nextreg_undefined,                  // 0xa9 TODO
        set_nextreg_undefined,                  // 0xaa
        set_nextreg_undefined,                  // 0xab
        set_nextreg_undefined,                  // 0xac
        set_nextreg_undefined,                  // 0xad
        set_nextreg_undefined,                  // 0xae
        set_nextreg_undefined,                  // 0xaf

        set_nextreg_undefined,                  // 0xb0 TODO
        set_nextreg_undefined,                  // 0xb1 TODO
        set_nextreg_undefined,                  // 0xb2
        set_nextreg_undefined,                  // 0xb3
        set_nextreg_undefined,                  // 0xb4
        set_nextreg_undefined,                  // 0xb5
        set_nextreg_undefined,                  // 0xb6
        set_nextreg_undefined,                  // 0xb7
        set_nextreg_simple,                     // 0xb8
        set_nextreg_simple,                     // 0xb9
        set_nextreg_simple,                     // 0xba
        set_nextreg_simple,                     // 0xbb
        set_nextreg_undefined,                  // 0xbc
        set_nextreg_undefined,                  // 0xbd
        set_nextreg_undefined,                  // 0xbe
        set_nextreg_undefined,                  // 0xbf

        set_nextreg_simple,                     // 0xc0 TODO
        set_nextreg_undefined,                  // 0xc1
        set_nextreg_simple,                     // 0xc2
        set_nextreg_simple,                     // 0xc3
        set_nextreg_undefined,                  // 0xc4 TODO
        set_nextreg_undefined,                  // 0xc5 TODO
        set_nextreg_undefined,                  // 0xc6 TODO
        set_nextreg_undefined,                  // 0xc7
        set_nextreg_undefined,                  // 0xc8 TODO
        set_nextreg_undefined,                  // 0xc9 TODO
        set_nextreg_undefined,                  // 0xca TODO
        set_nextreg_undefined,                  // 0xcb TODO
        set_nextreg_undefined,                  // 0xcc TODO
        set_nextreg_undefined,                  // 0xcd TODO
        set_nextreg_undefined,                  // 0xce TODO
        set_nextreg_undefined,                  // 0xcf TODO

        set_nextreg_undefined,                  // 0xd0
        set_nextreg_undefined,                  // 0xd1
        set_nextreg_undefined,                  // 0xd2
        set_nextreg_undefined,                  // 0xd3
        set_nextreg_undefined,                  // 0xd4
        set_nextreg_undefined,                  // 0xd5
        set_nextreg_undefined,                  // 0xd6
        set_nextreg_undefined,                  // 0xd7
        set_nextreg_undefined,                  // 0xd8
        set_nextreg_undefined,                  // 0xd9
        set_nextreg_undefined,                  // 0xda
        set_nextreg_undefined,                  // 0xdb
        set_nextreg_undefined,                  // 0xdc
        set_nextreg_undefined,                  // 0xdd
        set_nextreg_undefined,                  // 0xde
        set_nextreg_undefined,                  // 0xdf

        set_nextreg_undefined,                  // 0xe0
        set_nextreg_undefined,                  // 0xe1
        set_nextreg_undefined,                  // 0xe2
        set_nextreg_undefined,                  // 0xe3
        set_nextreg_undefined,                  // 0xe4
        set_nextreg_undefined,                  // 0xe5
        set_nextreg_undefined,                  // 0xe6
        set_nextreg_undefined,                  // 0xe7
        set_nextreg_undefined,                  // 0xe8
        set_nextreg_undefined,                  // 0xe9
        set_nextreg_undefined,                  // 0xea
        set_nextreg_undefined,                  // 0xeb
        set_nextreg_undefined,                  // 0xec
        set_nextreg_undefined,                  // 0xed
        set_nextreg_undefined,                  // 0xee
        set_nextreg_undefined,                  // 0xef

        set_nextreg_undefined,                  // 0xf0
        set_nextreg_undefined,                  // 0xf1
        set_nextreg_undefined,                  // 0xf2
        set_nextreg_undefined,                  // 0xf3
        set_nextreg_undefined,                  // 0xf4
        set_nextreg_undefined,                  // 0xf5
        set_nextreg_undefined,                  // 0xf6
        set_nextreg_undefined,                  // 0xf7
        set_nextreg_undefined,                  // 0xf8
        set_nextreg_undefined,                  // 0xf9
        set_nextreg_undefined,                  // 0xfa
        set_nextreg_undefined,                  // 0xfb
        set_nextreg_undefined,                  // 0xfc
        set_nextreg_undefined,                  // 0xfd
        set_nextreg_undefined,                  // 0xfe
        set_nextreg_undefined,                  // 0xff
};


void tbblue_set_value_port(z80_byte value)
{
        target_nextreg = tbblue_last_register;
        set_nextreg[target_nextreg](value);
}


void tbblue_set_value_port_position(z80_byte index, z80_byte value)
{
        target_nextreg = index;
        set_nextreg[target_nextreg](value);
}


z80_byte get_nextreg_00(void)
{
        return tbblue_machine_id;
}


z80_byte get_nextreg_01(void)
{
        return (tbblue_core_current_version_major<<4 | tbblue_core_current_version_minor);
}


z80_byte get_nextreg_03(void)
{
        return (tbblue_registers[0x03] & 0x7f)
                | (tbblue_write_palette_state << 7);
}


z80_byte get_nextreg_07(void)
{
        return ( (tbblue_registers[7] &3) | ((tbblue_registers[7] &3)<<4) );
}


z80_byte get_nextreg_0e(void)
{
        return tbblue_core_current_version_subminor;
}


z80_byte get_nextreg_18(void)
{
        return clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][tbblue_get_clip_window_layer2_index()];
}


z80_byte get_nextreg_19(void)
{
        return clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][tbblue_get_clip_window_sprites_index()];
}


z80_byte get_nextreg_1a(void)
{
        return clip_windows[TBBLUE_CLIP_WINDOW_ULA][tbblue_get_clip_window_ula_index()];
}


z80_byte get_nextreg_1b(void)
{
        return clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][tbblue_get_clip_window_tilemap_index()];
}


z80_byte get_nextreg_1e(void)
{
        return (tbblue_get_raster_line() >> 8) & 0x01;
}


z80_byte get_nextreg_1f(void)
{
        return tbblue_get_raster_line() & 0xff;
}


z80_byte get_nextreg_28(void)
{
        return tbblue_write_palette_latched_8;
}


z80_byte get_nextreg_41(void)
{
        z80_int *paleta = tbblue_get_palette_rw(); 
        z80_byte indice_paleta = tbblue_registers[0x40];

        return (paleta[indice_paleta] >> 1) & 0xff;
}


z80_byte get_nextreg_44(void)
{
        // TODO Layer2 priority bit
        z80_int *paleta = tbblue_get_palette_rw(); 
        z80_byte indice_paleta = tbblue_registers[0x40];

        return paleta[indice_paleta] & 0x01;
}


z80_byte get_nextreg_8e(void)
{
        // bit 3 = 1
	z80_byte value = 0x08;

        // TODO
        // bit 7 = port 0xdffd bit 0         \  RAM

        // bits 6:4 = port 0x7ffd bits 2:0
        value |= (puerto_32765 & 0x07) << 4;

        if (puerto_8189 & 0x01)
        {
                // AllRAM mode:
                // bit 2 = port 0x1ffd bit 0 (1 for AllRAM)
                // bit 1 = port 0x1ffd bit 2
                // bit 0 = port 0x1ffd bit 1
                value |= 0x04 | ((puerto_8189 >> 1) & 0x03);
        }
        else
        {
                // Standard mode:
                // bit 2 = port 0x1ffd bit 0 (0 for standard)
                // bit 1 = port 0x1ffd bit 2
                // bit 0 = port 0x7ffd bit 4
                value |= ((puerto_8189 >> 1) & 0x02) |
                         ((puerto_32765 >> 4) & 0x01);
        }

        return value;
}


z80_byte get_nextreg_undefined(void)
{
        return 0x00;
}


z80_byte get_nextreg_writeonly(void)
{
        return 0x00;
}


z80_byte get_nextreg_simple(void)
{
        return tbblue_registers[target_nextreg];
}


z80_byte (*get_nextreg[])(void) =
{
        get_nextreg_00,                         // 0x00
        get_nextreg_01,                         // 0x01
        get_nextreg_simple,                     // 0x02
        get_nextreg_03,                         // 0x03
        get_nextreg_writeonly,                  // 0x04
        get_nextreg_simple,                     // 0x05
        get_nextreg_simple,                     // 0x06
        get_nextreg_07,                         // 0x07
        get_nextreg_simple,                     // 0x08
        get_nextreg_simple,                     // 0x09
        get_nextreg_simple,                     // 0x0a
        get_nextreg_undefined,                  // 0x0b
        get_nextreg_undefined,                  // 0x0c
        get_nextreg_undefined,                  // 0x0d
        get_nextreg_0e,                         // 0x0e
        get_nextreg_undefined,                  // 0x0f

        get_nextreg_undefined,                  // 0x10 TODO
        get_nextreg_simple,                     // 0x11
        get_nextreg_simple,                     // 0x12
        get_nextreg_simple,                     // 0x13
        get_nextreg_simple,                     // 0x14
        get_nextreg_simple,                     // 0x15
        get_nextreg_simple,                     // 0x16
        get_nextreg_simple,                     // 0x17
        get_nextreg_18,                         // 0x18
        get_nextreg_19,                         // 0x19
        get_nextreg_1a,                         // 0x1a
        get_nextreg_1b,                         // 0x1b
        get_nextreg_simple,                     // 0x1c
        get_nextreg_undefined,                  // 0x1d
        get_nextreg_1e,                         // 0x1e
        get_nextreg_1f,                         // 0x1f

        get_nextreg_undefined,                  // 0x20
        get_nextreg_undefined,                  // 0x21
        get_nextreg_simple,                     // 0x22
        get_nextreg_simple,                     // 0x23
        get_nextreg_undefined,                  // 0x24
        get_nextreg_undefined,                  // 0x25
        get_nextreg_simple,                     // 0x26
        get_nextreg_simple,                     // 0x27
        get_nextreg_28,                         // 0x28
        get_nextreg_writeonly,                  // 0x29
        get_nextreg_writeonly,                  // 0x2a
        get_nextreg_writeonly,                  // 0x2b
        get_nextreg_undefined,                  // 0x2c TODO
        get_nextreg_undefined,                  // 0x2d TODO
        get_nextreg_undefined,                  // 0x2e TODO
        get_nextreg_simple,                     // 0x2f

        get_nextreg_simple,                     // 0x30
        get_nextreg_simple,                     // 0x31
        get_nextreg_simple,                     // 0x32
        get_nextreg_simple,                     // 0x33
        get_nextreg_simple,                     // 0x34
        get_nextreg_writeonly,                  // 0x35
        get_nextreg_writeonly,                  // 0x36
        get_nextreg_writeonly,                  // 0x37
        get_nextreg_writeonly,                  // 0x38
        get_nextreg_writeonly,                  // 0x39
        get_nextreg_undefined,                  // 0x3a
        get_nextreg_undefined,                  // 0x3b
        get_nextreg_undefined,                  // 0x3c
        get_nextreg_undefined,                  // 0x3d
        get_nextreg_undefined,                  // 0x3e
        get_nextreg_undefined,                  // 0x3f

        get_nextreg_simple,                     // 0x40
        get_nextreg_41,                         // 0x41
        get_nextreg_simple,                     // 0x42
        get_nextreg_simple,                     // 0x43
        get_nextreg_44,                         // 0x44
        get_nextreg_undefined,                  // 0x45
        get_nextreg_undefined,                  // 0x46
        get_nextreg_undefined,                  // 0x47
        get_nextreg_undefined,                  // 0x48
        get_nextreg_undefined,                  // 0x49
        get_nextreg_simple,                     // 0x4a
        get_nextreg_simple,                     // 0x4b
        get_nextreg_simple,                     // 0x4c
        get_nextreg_undefined,                  // 0x4d
        get_nextreg_undefined,                  // 0x4e
        get_nextreg_undefined,                  // 0x4f

        get_nextreg_simple,                     // 0x50
        get_nextreg_simple,                     // 0x51
        get_nextreg_simple,                     // 0x52
        get_nextreg_simple,                     // 0x53
        get_nextreg_simple,                     // 0x54
        get_nextreg_simple,                     // 0x55
        get_nextreg_simple,                     // 0x56
        get_nextreg_simple,                     // 0x57
        get_nextreg_undefined,                  // 0x58
        get_nextreg_undefined,                  // 0x59
        get_nextreg_undefined,                  // 0x5a
        get_nextreg_undefined,                  // 0x5b
        get_nextreg_undefined,                  // 0x5c
        get_nextreg_undefined,                  // 0x5d
        get_nextreg_undefined,                  // 0x5e
        get_nextreg_undefined,                  // 0x5f

        get_nextreg_writeonly,                  // 0x60
        get_nextreg_simple,                     // 0x61
        get_nextreg_simple,                     // 0x62
        get_nextreg_writeonly,                  // 0x63
        get_nextreg_simple,                     // 0x64
        get_nextreg_undefined,                  // 0x65
        get_nextreg_undefined,                  // 0x66
        get_nextreg_undefined,                  // 0x67
        get_nextreg_simple,                     // 0x68
        get_nextreg_simple,                     // 0x69
        get_nextreg_simple,                     // 0x6a
        get_nextreg_simple,                     // 0x6b
        get_nextreg_simple,                     // 0x6c
        get_nextreg_undefined,                  // 0x6d
        get_nextreg_simple,                     // 0x6e
        get_nextreg_simple,                     // 0x6f

        get_nextreg_simple,                     // 0x70
        get_nextreg_simple,                     // 0x71
        get_nextreg_undefined,                  // 0x72
        get_nextreg_undefined,                  // 0x73
        get_nextreg_undefined,                  // 0x74
        get_nextreg_writeonly,                  // 0x75
        get_nextreg_writeonly,                  // 0x76
        get_nextreg_writeonly,                  // 0x77
        get_nextreg_writeonly,                  // 0x78
        get_nextreg_writeonly,                  // 0x79
        get_nextreg_undefined,                  // 0x7a
        get_nextreg_undefined,                  // 0x7b
        get_nextreg_undefined,                  // 0x7c
        get_nextreg_undefined,                  // 0x7d
        get_nextreg_undefined,                  // 0x7e
        get_nextreg_simple,                     // 0x7f

        get_nextreg_simple,                     // 0x80
        get_nextreg_simple,                     // 0x81
        get_nextreg_simple,                     // 0x82
        get_nextreg_simple,                     // 0x83
        get_nextreg_simple,                     // 0x84
        get_nextreg_simple,                     // 0x85
        get_nextreg_simple,                     // 0x86
        get_nextreg_simple,                     // 0x87
        get_nextreg_simple,                     // 0x88
        get_nextreg_simple,                     // 0x89
        get_nextreg_simple,                     // 0x8a
        get_nextreg_undefined,                  // 0x8b
        get_nextreg_simple,                     // 0x8c
        get_nextreg_undefined,                  // 0x8d
        get_nextreg_8e,                         // 0x8e
        get_nextreg_simple,                     // 0x8f

        get_nextreg_undefined,                  // 0x90 TODO
        get_nextreg_undefined,                  // 0x91 TODO
        get_nextreg_undefined,                  // 0x92 TODO
        get_nextreg_undefined,                  // 0x93 TODO
        get_nextreg_undefined,                  // 0x94
        get_nextreg_undefined,                  // 0x95
        get_nextreg_undefined,                  // 0x96
        get_nextreg_undefined,                  // 0x97
        get_nextreg_undefined,                  // 0x98 TODO
        get_nextreg_undefined,                  // 0x99 TODO
        get_nextreg_undefined,                  // 0x9a TODO
        get_nextreg_undefined,                  // 0x9b TODO
        get_nextreg_undefined,                  // 0x9c
        get_nextreg_undefined,                  // 0x9d
        get_nextreg_undefined,                  // 0x9e
        get_nextreg_undefined,                  // 0x9f

        get_nextreg_undefined,                  // 0xa0 TODO
        get_nextreg_undefined,                  // 0xa1
        get_nextreg_undefined,                  // 0xa2 TODO
        get_nextreg_undefined,                  // 0xa3 TODO
        get_nextreg_undefined,                  // 0xa4
        get_nextreg_undefined,                  // 0xa5
        get_nextreg_undefined,                  // 0xa6
        get_nextreg_undefined,                  // 0xa7
        get_nextreg_undefined,                  // 0xa8 TODO
        get_nextreg_undefined,                  // 0xa9 TODO
        get_nextreg_undefined,                  // 0xaa
        get_nextreg_undefined,                  // 0xab
        get_nextreg_undefined,                  // 0xac
        get_nextreg_undefined,                  // 0xad
        get_nextreg_undefined,                  // 0xae
        get_nextreg_undefined,                  // 0xaf

        get_nextreg_undefined,                  // 0xb0 TODO
        get_nextreg_undefined,                  // 0xb1 TODO
        get_nextreg_undefined,                  // 0xb2
        get_nextreg_undefined,                  // 0xb3
        get_nextreg_undefined,                  // 0xb4
        get_nextreg_undefined,                  // 0xb5
        get_nextreg_undefined,                  // 0xb6
        get_nextreg_undefined,                  // 0xb7
        get_nextreg_simple,                     // 0xb8
        get_nextreg_simple,                     // 0xb9
        get_nextreg_simple,                     // 0xba
        get_nextreg_simple,                     // 0xbb
        get_nextreg_undefined,                  // 0xbc
        get_nextreg_undefined,                  // 0xbd
        get_nextreg_undefined,                  // 0xbe
        get_nextreg_undefined,                  // 0xbf

        get_nextreg_simple,                     // 0xc0
        get_nextreg_undefined,                  // 0xc1
        get_nextreg_simple,                     // 0xc2
        get_nextreg_simple,                     // 0xc3
        get_nextreg_simple,                     // 0xc4
        get_nextreg_simple,                     // 0xc5
        get_nextreg_simple,                     // 0xc6
        get_nextreg_undefined,                  // 0xc7
        get_nextreg_undefined,                  // 0xc8 TODO
        get_nextreg_undefined,                  // 0xc9 TODO
        get_nextreg_undefined,                  // 0xca TODO
        get_nextreg_undefined,                  // 0xcb
        get_nextreg_simple,                     // 0xcc
        get_nextreg_simple,                     // 0xcd
        get_nextreg_simple,                     // 0xce
        get_nextreg_undefined,                  // 0xcf

        get_nextreg_undefined,                  // 0xd0
        get_nextreg_undefined,                  // 0xd1
        get_nextreg_undefined,                  // 0xd2
        get_nextreg_undefined,                  // 0xd3
        get_nextreg_undefined,                  // 0xd4
        get_nextreg_undefined,                  // 0xd5
        get_nextreg_undefined,                  // 0xd6
        get_nextreg_undefined,                  // 0xd7
        get_nextreg_undefined,                  // 0xd8
        get_nextreg_undefined,                  // 0xd9
        get_nextreg_undefined,                  // 0xda
        get_nextreg_undefined,                  // 0xdb
        get_nextreg_undefined,                  // 0xdc
        get_nextreg_undefined,                  // 0xdd
        get_nextreg_undefined,                  // 0xde
        get_nextreg_undefined,                  // 0xdf

        get_nextreg_undefined,                  // 0xe0
        get_nextreg_undefined,                  // 0xe1
        get_nextreg_undefined,                  // 0xe2
        get_nextreg_undefined,                  // 0xe3
        get_nextreg_undefined,                  // 0xe4
        get_nextreg_undefined,                  // 0xe5
        get_nextreg_undefined,                  // 0xe6
        get_nextreg_undefined,                  // 0xe7
        get_nextreg_undefined,                  // 0xe8
        get_nextreg_undefined,                  // 0xe9
        get_nextreg_undefined,                  // 0xea
        get_nextreg_undefined,                  // 0xeb
        get_nextreg_undefined,                  // 0xec
        get_nextreg_undefined,                  // 0xed
        get_nextreg_undefined,                  // 0xee
        get_nextreg_undefined,                  // 0xef

        get_nextreg_undefined,                  // 0xf0
        get_nextreg_undefined,                  // 0xf1
        get_nextreg_undefined,                  // 0xf2
        get_nextreg_undefined,                  // 0xf3
        get_nextreg_undefined,                  // 0xf4
        get_nextreg_undefined,                  // 0xf5
        get_nextreg_undefined,                  // 0xf6
        get_nextreg_undefined,                  // 0xf7
        get_nextreg_undefined,                  // 0xf8
        get_nextreg_undefined,                  // 0xf9
        get_nextreg_undefined,                  // 0xfa
        get_nextreg_undefined,                  // 0xfb
        get_nextreg_undefined,                  // 0xfc
        get_nextreg_undefined,                  // 0xfd
        get_nextreg_undefined,                  // 0xfe
        get_nextreg_undefined,                  // 0xff
};


z80_byte tbblue_get_value_port(void)
{
        target_nextreg = tbblue_last_register;
        return get_nextreg[target_nextreg]();
}


z80_byte tbblue_get_value_port_register(z80_byte index)
{
        target_nextreg = index;
        return get_nextreg[target_nextreg]();
}
