/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MENU_H
#define MENU_H

#include "menu_system.h"


extern int menu_inicio_opcion_seleccionada;
extern int display_settings_opcion_seleccionada;
extern int mem_breakpoints_opcion_seleccionada;
extern int breakpoints_opcion_seleccionada;
extern int cpu_transaction_log_opcion_seleccionada;
extern int tsconf_layer_settings_opcion_seleccionada;
extern int debug_tsconf_opcion_seleccionada;
extern int audio_settings_opcion_seleccionada;
extern int hardware_settings_opcion_seleccionada;
extern int keyboard_settings_opcion_seleccionada;
extern int hardware_memory_settings_opcion_seleccionada;
extern int debug_settings_opcion_seleccionada;
extern int hardware_advanced_opcion_seleccionada;
extern int hardware_printers_opcion_seleccionada;
extern int hardware_realjoystick_opcion_seleccionada;
extern int hardware_realjoystick_event_opcion_seleccionada;
extern int hardware_realjoystick_keys_opcion_seleccionada;
extern int find_opcion_seleccionada;
extern int find_bytes_opcion_seleccionada;
extern int find_lives_opcion_seleccionada;
extern int poke_opcion_seleccionada;
extern int hardware_redefine_keys_opcion_seleccionada;
extern int hardware_set_f_functions_opcion_seleccionada;
extern int hardware_set_f_func_action_opcion_seleccionada;
extern int ula_settings_opcion_seleccionada;
extern int settings_opcion_seleccionada;
extern int uartbridge_opcion_seleccionada;
extern int settings_audio_opcion_seleccionada;
extern int cpu_settings_opcion_seleccionada;
extern int cpu_stats_opcion_seleccionada;
extern int settings_display_opcion_seleccionada;
extern int settings_debug_opcion_seleccionada;
extern int menu_tbblue_hardware_id_opcion_seleccionada;
extern int menu_watches_opcion_seleccionada;
extern int debug_tsconf_dma_opcion_seleccionada;
extern int debug_pok_file_opcion_seleccionada;


extern void menu_display_settings(int);
extern void menu_cpu_transaction_log(int);
extern void menu_mem_breakpoints(int);
extern void menu_breakpoints(int);
extern void menu_tsconf_layer_settings(int);
extern void menu_debug_tsconf_tbblue_msx(int);
extern void menu_find_bytes(int);
extern void menu_find_lives(int);
extern void menu_find(int);
extern void menu_poke(int);
extern void menu_debug_settings(int);
extern void menu_uartbridge(int);
extern void menu_settings_audio(int);
extern void menu_cpu_settings(int);
extern void menu_settings_debug(int);
extern void menu_settings_display(int);
extern void menu_hardware_redefine_keys(int);
extern void menu_hardware_set_f_functions(int);
extern void menu_keyboard_settings(int);
extern void menu_hardware_realjoystick(int);
extern void menu_hardware_printers(int);
extern void menu_hardware_settings(int);
extern void menu_ula_advanced(int);
extern int menu_cond_realvideo(void);
extern void menu_ula_settings(int);
extern void menu_settings(int);
extern void menu_inicio_bucle_main(void);
extern void menu_inicio_bucle(void);
extern void menu_inicio_pre_retorno(void);

#endif
