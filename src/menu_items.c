/*
    ZEsarUX  ZX Second-Emulator And Released for UniX
    Copyright (C) 2013 Cesar Hernandez Bano

    This file is part of ZEsarUX.

    ZEsarUX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>
#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <errno.h>

#include "assemble.h"
#include "audio.h"
#include "ay38912.h"
#include "contend.h"
#include "datagear.h"
#include "debug.h"
#include "disassemble.h"
#include "joystick.h"
#include "menu_system.h"
#include "menu.h"
#include "menu_items.h"
#include "multiface.h"
#include "operaciones.h"
#include "printers.h"
#include "realjoystick.h"
#include "remote.h"
#include "screen.h"
#include "settings.h"
#include "tbblue.h"
#include "timer.h"
#include "uartbridge.h"
#include "ula.h"

#ifdef COMPILE_ALSA
#include "audioalsa.h"
#endif


#ifdef COMPILE_PCSPEAKER
#include "audiopcspeaker.h"
#endif

 
#if defined(__APPLE__)
	#include <sys/syslimits.h>

	#include <sys/resource.h>

#endif


#ifdef COMPILE_XWINDOWS
	#include "scrxwindows.h"
#endif


char binary_file_load[PATH_MAX]="";
char binary_file_save[PATH_MAX];
char zxprinter_bitmap_filename_buffer[PATH_MAX];
char zxprinter_ocr_filename_buffer[PATH_MAX];

menu_z80_moto_int view_sprites_direccion=0;
z80_int view_sprites_ancho_sprite=8;
z80_int view_sprites_alto_sprite=8*6;
int view_sprites_hardware=0;
z80_bit view_sprites_inverse={0};
int view_sprite_incremento=1;
int view_sprites_ppb=8;
int view_sprites_bpp=1;
int view_sprites_palette=0;
int view_sprites_scr_sprite=0;
int view_sprites_offset_palette=0;

zxvision_window *menu_debug_draw_sprites_window;

int view_sprites_bytes_por_linea=1;
int view_sprites_bytes_por_ventana=1;
int view_sprites_increment_cursor_vertical=1;

zxvision_window zxvision_window_view_sprites;

int menu_debug_show_memory_zones=0;
int menu_debug_memory_zone=-1;
menu_z80_moto_int menu_debug_memory_zone_size=65536;

  
int menu_debug_registers_current_view=1;
menu_z80_moto_int menu_debug_disassemble_last_ptr=0;
z80_bit menu_debug_disassemble_hexa_view={0};

z80_bit menu_debug_follow_pc={1};
menu_z80_moto_int menu_debug_memory_pointer=0;
int menu_debug_line_cursor=0;
char menu_debug_change_registers_last_reg[30]="";
char menu_debug_change_registers_last_val[30]="";
size_t menu_debug_registers_print_registers_longitud_opcode=0;
menu_z80_moto_int menu_debug_memory_pointer_last=0;
int menu_debug_registers_subview_type=0;
int menu_debug_hexdump_with_ascii_modo_ascii=0;
z80_bit menu_breakpoint_exception_pending_show={0};
int continuous_step=0;
zxvision_window *menu_watches_overlay_window;
zxvision_window zxvision_window_watches;
int menu_debug_continuous_speed=0;
int menu_debug_continuous_speed_step=0;
int menu_debug_registers_buffer_precursor[ANCHO_SCANLINE_CURSOR];
int menu_debug_registers_buffer_pre_x=-1; //posicion anterior del cursor
int menu_debug_registers_buffer_pre_y=-1;
zxvision_window zxvision_window_menu_debug_registers;
zxvision_window *menu_debug_dma_tsconf_zxuno_overlay_window;
zxvision_window *menu_debug_unnamed_console_overlay_window;
int menu_debug_unnamed_console_indicador_actividad_contador=0;
int menu_debug_unnamed_console_indicador_actividad_visible=0;
zxvision_window zxvision_window_unnamed_console;

menu_z80_moto_int menu_debug_hexdump_direccion=0;
int menu_hexdump_edit_position_x=0;
int menu_hexdump_edit_position_y=0;
int menu_hexdump_lineas_total=13;
int menu_hexdump_edit_mode=0;
const int menu_hexdump_bytes_por_linea=8;
int menu_debug_hexdump_cursor_en_zona_ascii=0;
int menu_find_bytes_empty=1;
unsigned char *menu_find_bytes_mem_pointer=NULL;
int menu_find_lives_state=0;
z80_int menu_find_lives_pointer=0;
int lives_to_find=3;
z80_byte ultimo_menu_find_lives_set=9;

int last_debug_poke_dir=16384; 
char aofilename_file[PATH_MAX];

menu_z80_moto_int load_binary_last_address=0;
menu_z80_moto_int load_binary_last_length=0;
menu_z80_moto_int save_binary_last_address=0;
menu_z80_moto_int save_binary_last_length=1;

int menu_debug_tsconf_tbblue_msx_videoregisters_valor_contador_segundo_anterior;
zxvision_window *menu_debug_tsconf_tbblue_msx_videoregisters_overlay_window;
zxvision_window menu_debug_tsconf_tbblue_msx_videoregisters_ventana;

int menu_tsconf_layer_valor_contador_segundo_anterior;
char *menu_tsconf_layer_aux_usedunused_used="In use";
char *menu_tsconf_layer_aux_usedunused_unused="Unused";
zxvision_window *menu_tsconf_layer_overlay_window;

int menu_debug_tsconf_tbblue_msx_spritenav_current_sprite=0;
zxvision_window *menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window;
zxvision_window zxvision_window_tsconf_tbblue_spritenav;

int menu_debug_tsconf_tbblue_msx_tilenav_current_tilelayer=0;
z80_bit menu_debug_tsconf_tbblue_msx_tilenav_showmap={0};
zxvision_window *menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window;
zxvision_window zxvision_window_tsconf_tbblue_tilenav;

int menu_info_joystick_last_button;
int menu_info_joystick_last_type;
int menu_info_joystick_last_value;
int menu_info_joystick_last_index;
int menu_info_joystick_last_raw_value;


defined_f_function defined_f_functions_array[MAX_F_FUNCTIONS]={
	{"Default",F_FUNCION_DEFAULT},
	{"Nothing",F_FUNCION_NOTHING},
	{"Reset",F_FUNCION_RESET},
	{"HardReset",F_FUNCION_HARDRESET},
	{"NMI",F_FUNCION_NMI},
	{"Drive",F_FUNCION_DRIVE},
	{"OpenMenu",F_FUNCION_OPENMENU},
	{"LoadBinary",F_FUNCION_LOADBINARY},
	{"SaveBinary",F_FUNCION_SAVEBINARY},
	{"DebugCPU",F_FUNCION_DEBUGCPU},
	{"ExitEmulator",F_FUNCION_EXITEMULATOR},
};


//Funciones de teclas F mapeadas. Desde F1 hasta F15
enum defined_f_function_ids defined_f_functions_keys_array[MAX_F_FUNCTIONS_KEYS]={
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT, //F5
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT, //F10
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT,
	F_FUNCION_DEFAULT //F15
};


void menu_tape_settings_trunc_name(char *orig,char *dest,int max)
{
	//printf ("max: %d\n",max);
	if (max<=0) {
		dest[0]=0;
		return;
	}
	//en maximo se incluye caracter 0 del final
	max--;

                if (orig!=0) {

                        int longitud=strlen(orig);
                        int indice=longitud-max;

			if (indice<0) indice=0;

                        strncpy(dest,&orig[indice],max);


			//printf ("copiamos %d max caracteres\n",max);

                        //si cadena es mayor, acabar con 0

			//en teoria max siempre es mayor de cero, pero por si acaso...
			if (max>0) dest[max]=0;

			//else printf ("max=%d\n",max);


			if (indice>0) dest[0]='<';

                }

             else {
                        strcpy(dest,"");
                }

}


void menu_view_screen(MENU_ITEM_PARAMETERS)
{
        menu_espera_no_tecla();

	//para que no se vea oscuro
	menu_set_menu_abierto(0);
	//modificado_border.v=1;

	menu_cls_refresh_emulated_screen();

	//menu_refresca_pantalla();
        menu_espera_tecla();
	menu_set_menu_abierto(1);
	menu_espera_no_tecla();
	modificado_border.v=1;
}


void menu_display_load_screen(MENU_ITEM_PARAMETERS)
{

char screen_load_file[PATH_MAX];

  char *filtros[2];

        filtros[0]="scr";
        filtros[1]=0;


        if (menu_filesel("Select Screen File",filtros,screen_load_file)==1) {
		load_screen(screen_load_file);
                salir_todos_menus=1;

        }

}


void menu_display_save_screen(MENU_ITEM_PARAMETERS)
{

	char screen_save_file[PATH_MAX];

	char *filtros[4];


		filtros[0]="scr";
		filtros[1]="pbm";
		filtros[2]="bmp";
		filtros[3]=0;


	if (menu_filesel("Select Screen File",filtros,screen_save_file)==1) {

		//Ver si archivo existe y preguntar
		struct stat buf_stat;

		if (stat(screen_save_file, &buf_stat)==0) {

			if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return;

		}

		save_screen(screen_save_file);



		salir_todos_menus=1;

	}

}


void menu_mem_breakpoints_edit(MENU_ITEM_PARAMETERS)
{


        int brkp_type,dir;

        char string_type[4];
        char string_dir[10];

        strcpy (string_dir,"0");

        menu_ventana_scanf("Address",string_dir,10);

        dir=parse_string_to_number(string_dir);

        if (dir<0 || dir>65535) {
                debug_printf (VERBOSE_ERR,"Invalid address %d",dir);
                return;
        }				

        strcpy (string_type,"0");

        menu_ventana_scanf("Type (1:RD,2:WR,3:RW)",string_type,4);

        brkp_type=parse_string_to_number(string_type);

        if (brkp_type<0 || brkp_type>255) {
                debug_printf (VERBOSE_ERR,"Invalid value %d",brkp_type);
                return;
        }

	debug_set_mem_breakpoint(dir,brkp_type);
	//mem_breakpoint_array[dir]=brkp_type;
	

}

void menu_mem_breakpoints_list(MENU_ITEM_PARAMETERS)
{

        //int index_find;
		int index_buffer;

        char results_buffer[MAX_TEXTO_GENERIC_MESSAGE];

        //margen suficiente para que quepa una linea
        //direccion+salto linea+codigo 0
        char buf_linea[33];

        index_buffer=0;

        int encontrados=0;

        int salir=0;

		int i;

        for (i=0;i<65536 && salir==0;i++) {
			z80_byte tipo=mem_breakpoint_array[i];
			if (tipo) {
				if (tipo<MAX_MEM_BREAKPOINT_TYPES) {
					sprintf (buf_linea,"%04XH : %s\n",i,mem_breakpoint_types_strings[tipo]);
				}
				else {
					sprintf (buf_linea,"%04XH : Unknown (%d)\n",i,tipo);
				}

				sprintf (&results_buffer[index_buffer],"%s\n",buf_linea);
                index_buffer +=strlen(buf_linea);
                encontrados++;
                

                //controlar maximo
                //33 bytes de margen
                if (index_buffer>MAX_TEXTO_GENERIC_MESSAGE-33) {
                        debug_printf (VERBOSE_ERR,"Too many results to show. Showing only the first %d",encontrados);
                        //forzar salir
                        salir=1;
                }
			}

        }

        results_buffer[index_buffer]=0;

        menu_generic_message("List Memory Breakpoints",results_buffer);
}

void menu_mem_breakpoints_clear(MENU_ITEM_PARAMETERS)
{
	if (menu_confirm_yesno("Clear Mem breakpoints")) {
		clear_mem_breakpoints();
		menu_generic_message("Clear Mem breakpoints","OK. All memory breakpoints cleared");
	}
}


void menu_clear_all_breakpoints(MENU_ITEM_PARAMETERS)
{
	if (menu_confirm_yesno("Clear breakpoints")) {
		init_breakpoints_table();
		menu_generic_message("Clear breakpoints","OK. All breakpoints cleared");
	}
}

void menu_debug_reset(MENU_ITEM_PARAMETERS)
{
        reset_cpu();

        salir_todos_menus=1;
}


void menu_debug_hard_reset(MENU_ITEM_PARAMETERS)
{
        hard_reset_cpu();

        salir_todos_menus=1;
}


void menu_debug_nmi_drive(MENU_ITEM_PARAMETERS)
{
        generate_nmi_drive();

        salir_todos_menus=1;
}


void menu_debug_nmi_multiface_tbblue(MENU_ITEM_PARAMETERS)
{
        generate_nmi_multiface_tbblue();

        salir_todos_menus=1;
}


void menu_cpu_transaction_log_enable(MENU_ITEM_PARAMETERS)
{
	if (cpu_transaction_log_enabled.v) {
		reset_cpu_core_transaction_log();
	}
	else {

		if (menu_confirm_yesno_texto("May use lot of disk","Sure?")==1)
			set_cpu_core_transaction_log();
	}
}

void menu_cpu_transaction_log_truncate(MENU_ITEM_PARAMETERS)
{
	if (menu_confirm_yesno("Truncate log file")) {
		transaction_log_truncate();
		menu_generic_message("Truncate log file","OK. Log truncated");
	}
}

void menu_cpu_transaction_log_file(MENU_ITEM_PARAMETERS)
{

	if (cpu_transaction_log_enabled.v) reset_cpu_core_transaction_log();

        char *filtros[2];

        filtros[0]="log";
        filtros[1]=0;


        if (menu_filesel("Select Log File",filtros,transaction_log_filename)==1) {
                //Ver si archivo existe y preguntar
		if (si_existe_archivo(transaction_log_filename)) {
                        if (menu_confirm_yesno_texto("File exists","Append?")==0) {
				transaction_log_filename[0]=0;
				return;
			}
                }

        }

	else transaction_log_filename[0]=0;

}


void menu_cpu_transaction_log_enable_address(MENU_ITEM_PARAMETERS)
{
	cpu_transaction_log_store_address.v ^=1;
}

void menu_cpu_transaction_log_enable_datetime(MENU_ITEM_PARAMETERS)
{
        cpu_transaction_log_store_datetime.v ^=1;
}


void menu_cpu_transaction_log_enable_tstates(MENU_ITEM_PARAMETERS)
{
        cpu_transaction_log_store_tstates.v ^=1;
}

void menu_cpu_transaction_log_enable_opcode(MENU_ITEM_PARAMETERS)
{
        cpu_transaction_log_store_opcode.v ^=1;
}

void menu_cpu_transaction_log_enable_registers(MENU_ITEM_PARAMETERS)
{
        cpu_transaction_log_store_registers.v ^=1;
}


void menu_cpu_transaction_log_enable_rotate(MENU_ITEM_PARAMETERS)
{
	cpu_transaction_log_rotate_enabled.v ^=1;	
}

void menu_cpu_transaction_log_rotate_number(MENU_ITEM_PARAMETERS)
{

        char string_number[4];

        sprintf (string_number,"%d",cpu_transaction_log_rotated_files);

        menu_ventana_scanf("Number of files",string_number,4);

        int numero=parse_string_to_number(string_number);

		if (transaction_log_set_rotate_number(numero)) {
			debug_printf (VERBOSE_ERR,"Invalid rotation number");			
		}


}


void menu_cpu_transaction_log_rotate_size(MENU_ITEM_PARAMETERS)
{


        char string_number[5];

        sprintf (string_number,"%d",cpu_transaction_log_rotate_size);

        menu_ventana_scanf("Size in MB (0=no rot)",string_number,5);

        int numero=parse_string_to_number(string_number);

		if (transaction_log_set_rotate_size(numero)) {
			debug_printf (VERBOSE_ERR,"Invalid rotation size");
		}


}

void menu_cpu_transaction_log_rotate_lines(MENU_ITEM_PARAMETERS)
{


        char string_number[11];

        sprintf (string_number,"%d",cpu_transaction_log_rotate_lines);

        menu_ventana_scanf("Lines (0=no rotate)",string_number,11);

        int numero=parse_string_to_number(string_number);

		if (transaction_log_set_rotate_lines(numero)) {
			debug_printf (VERBOSE_ERR,"Invalid rotation lines");
		}


}

void menu_cpu_transaction_log_ignore_rep_halt(MENU_ITEM_PARAMETERS)
{
	cpu_trans_log_ignore_repeated_halt.v ^=1;
}


void menu_debug_disassemble_export(int p)
{

	char string_address[10];
	sprintf (string_address,"%XH",p);


    menu_ventana_scanf("Start?",string_address,10);

	menu_z80_moto_int inicio=parse_string_to_number(string_address);

	menu_ventana_scanf("End?",string_address,10);
	menu_z80_moto_int final=parse_string_to_number(string_address);

	if (final<inicio){
		menu_warn_message("End address must be higher or equal than start address");
		return;
	}

	char file_save[PATH_MAX];	
	int ret=menu_ask_file_to_save("Destination file","asm",file_save);

	if (!ret) {
		menu_warn_message("Export cancelled");
		return;
	}


	FILE *ptr_asmfile;
    ptr_asmfile=fopen(file_save,"wb");
    if (!ptr_asmfile) {
		debug_printf (VERBOSE_ERR,"Unable to open asm file");
		return;
    }
                  
 
	char dumpassembler[65];


	int longitud_opcode;

	//ponemos un final de un numero maximo de instrucciones
	//sera igual al tamaño de la zona de memoria
	int limite_instrucciones=menu_debug_memory_zone_size;

	int instrucciones=0;

	for (;inicio<=final && instrucciones<limite_instrucciones;instrucciones++) {

		menu_debug_dissassemble_una_inst_sino_hexa(dumpassembler,inicio,&longitud_opcode,0);


		inicio +=longitud_opcode;
		debug_printf (VERBOSE_DEBUG,"Exporting asm: %s",dumpassembler);

		//Agregar salto de linea
		int longitud_linea=strlen(dumpassembler);
		dumpassembler[longitud_linea++]='\n';
		dumpassembler[longitud_linea]=0;
		fwrite(&dumpassembler,1,longitud_linea,ptr_asmfile);
		//zxvision_print_string_defaults_fillspc(&ventana,1,linea,dumpassembler);
	}	

	fclose(ptr_asmfile);

	menu_generic_message_splash("Export disassembly","Ok process finished");

}

void menu_debug_disassemble(MENU_ITEM_PARAMETERS)
{

	//printf ("Opening disassemble menu\n");
 	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

	zxvision_window ventana;

	int ancho_total=32-1;


	zxvision_new_window(&ventana,0,1,32,20,
							ancho_total,20-2,"Disassemble");

	//Permitir hotkeys desde raton
	ventana.can_mouse_send_hotkeys=1;

	zxvision_draw_window(&ventana);			

    //Inicializar info de tamanyo zona
	menu_debug_set_memory_zone_attr();



	z80_byte tecla;

    
    menu_z80_moto_int direccion=menu_debug_disassemble_last_ptr;
		

	do {
		int linea=0;

		int lineas_disass=0;
		const int lineas_total=15;

		char dumpassembler[65];

		int longitud_opcode;
		int longitud_opcode_primera_linea;

		menu_z80_moto_int dir=direccion;

		for (;lineas_disass<lineas_total;lineas_disass++,linea++) {

			//Formato de texto en buffer:
			//0123456789012345678901234567890
			//DDDD AABBCCDD OPCODE-----------
			//DDDD: Direccion
			//AABBCCDD: Volcado hexa

			//Metemos 30 espacios
		


			//menu_debug_dissassemble_una_instruccion(dumpassembler,dir,&longitud_opcode);
			menu_debug_dissassemble_una_inst_sino_hexa(dumpassembler,dir,&longitud_opcode,menu_debug_disassemble_hexa_view.v);


			if (lineas_disass==0) longitud_opcode_primera_linea=longitud_opcode;

			dir +=longitud_opcode;
			zxvision_print_string_defaults_fillspc(&ventana,1,linea,dumpassembler);
		}


	//Forzar a mostrar atajos
	z80_bit antes_menu_writing_inverse_color;
	antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
	menu_writing_inverse_color.v=1;



        zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"");

        zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"~~memptr ~~export");
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"~~togglehexa");

		zxvision_draw_window_contents(&ventana);


	//Restaurar comportamiento atajos
	menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;




		tecla=zxvision_common_getkey_refresh();				

                     
		int i;

        switch (tecla) {

			case 11:
				//arriba
				direccion=menu_debug_disassemble_subir(direccion);
			break;

			case 10:
				//abajo
				direccion +=longitud_opcode_primera_linea;
			break;

			//No llamamos a zxvision_handle_cursors_pgupdn para solo poder gestionar scroll ventana en horizontal,
			//el resto de teclas (cursores, pgup, dn etc) las gestionamos desde aqui de manera diferente

            //izquierda
            case 8:
				/*
				//Decir que se ha pulsado tecla para que no se relea
				menu_speech_tecla_pulsada=1;*/
				zxvision_handle_cursors_pgupdn(&ventana,tecla);
            break;

            //derecha
            case 9:
				/*
				//Decir que se ha pulsado tecla para que no se relea
				menu_speech_tecla_pulsada=1;*/
				zxvision_handle_cursors_pgupdn(&ventana,tecla);
			break;					

			case 24:
				//PgUp
				for (i=0;i<lineas_total;i++) direccion=menu_debug_disassemble_subir(direccion);
			break;

			case 25:
				//PgDn
				direccion=dir;
			break;

			case 'm':
				//Usamos misma funcion de menu_debug_hexdump_change_pointer
				direccion=menu_debug_hexdump_change_pointer(direccion);
				zxvision_draw_window(&ventana);
			break;

			case 'e':
				menu_debug_disassemble_export(direccion);
				zxvision_draw_window(&ventana);
			break;

			case 't':
				menu_debug_disassemble_hexa_view.v ^=1;
			break;

		}


	} while (tecla!=2); 


    cls_menu_overlay();
	zxvision_destroy_window(&ventana);		

 

}


void menu_debug_assemble(MENU_ITEM_PARAMETERS)
{

	//printf ("Opening disassemble menu\n");
 	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

	zxvision_window ventana;

	int ancho_total=32-1;


	zxvision_new_window(&ventana,0,1,32,20,
							ancho_total,20-2,"Assemble");
	zxvision_draw_window(&ventana);			

    //Inicializar info de tamanyo zona
	menu_debug_set_memory_zone_attr();


    
    menu_z80_moto_int direccion=menu_debug_disassemble_last_ptr;

	menu_z80_moto_int direccion_ensamblado=direccion;

	int salir=0;
	int lineas_ensambladas=0;
		

	do {
		int linea=0;

		int lineas_disass=0;
		const int lineas_total=15;

		char dumpassembler[65];

		int longitud_opcode;
		int longitud_opcode_primera_linea;

		menu_z80_moto_int dir=direccion;

		for (;lineas_disass<lineas_total;lineas_disass++,linea++) {

			//Formato de texto en buffer:
			//0123456789012345678901234567890
			//DDDD AABBCCDD OPCODE-----------
			//DDDD: Direccion
			//AABBCCDD: Volcado hexa

			//Metemos 30 espacios
		


			//menu_debug_dissassemble_una_instruccion(dumpassembler,dir,&longitud_opcode);
			menu_debug_dissassemble_una_inst_sino_hexa(dumpassembler,dir,&longitud_opcode,menu_debug_disassemble_hexa_view.v);


			if (lineas_disass==0) longitud_opcode_primera_linea=longitud_opcode;

			dir +=longitud_opcode;
			zxvision_print_string_defaults_fillspc(&ventana,1,linea,dumpassembler);
		}


		zxvision_draw_window_contents(&ventana);



		char string_opcode[256];
		string_opcode[0]=0;


		char texto_titulo[256];
		sprintf (texto_titulo,"Assemble at %XH",direccion_ensamblado);

    	menu_ventana_scanf(texto_titulo,string_opcode,256);
		zxvision_draw_window(&ventana);

		if (string_opcode[0]==0) salir=1;
		else {


				z80_byte destino_ensamblado[MAX_DESTINO_ENSAMBLADO];


				int longitud_destino=assemble_opcode(direccion_ensamblado,string_opcode,destino_ensamblado);

				if (longitud_destino==0) {
					menu_error_message("Error. Invalid opcode");
					//escribir_socket_format(misocket,"Error. Invalid opcode: %s\n",texto);
					salir=1;
				}

				else {
					menu_debug_set_memory_zone_attr();
					unsigned int direccion_escribir=direccion_ensamblado;
					int i;
					for (i=0;i<longitud_destino;i++) {
						menu_debug_write_mapped_byte(direccion_escribir++,destino_ensamblado[i]);
					}

				}

				direccion_ensamblado+=longitud_destino;

				lineas_ensambladas++;

				//Desensamblar desde la siguiente instruccion si conviene
				if (lineas_ensambladas>5) {
					direccion +=longitud_opcode_primera_linea;
				}

		}
	
	} while (!salir);


		

    cls_menu_overlay();
	zxvision_destroy_window(&ventana);		

 

}


menu_z80_moto_int menu_debug_disassemble_bajar(menu_z80_moto_int dir_inicial)
{
	//Bajar 1 opcode en el listado
	        char buffer[32];
        size_t longitud_opcode;

	debugger_disassemble(buffer,30,&longitud_opcode,dir_inicial);

	dir_inicial +=longitud_opcode;

	return dir_inicial;
}


menu_z80_moto_int menu_debug_disassemble_subir(menu_z80_moto_int dir_inicial)
{
	//Subir 1 opcode en el listado

	//Metodo:
	//Empezamos en direccion-10 (en QL: direccion-30)
	//inicializamos un puntero ficticio de direccion a 0, mientras que mantenemos la posicion de memoria de lectura inicial en direccion-10/30
	//Vamos leyendo opcodes. Cuando el puntero ficticio este >=10 (o 30), nuestra direccion final será la inicial - longitud opcode anterior

	char buffer[32];
	size_t longitud_opcode;

	menu_z80_moto_int dir;

	int decremento=10;



	dir=dir_inicial-decremento;

	dir=menu_debug_hexdump_adjusta_en_negativo(dir,1);

	//menu_z80_moto_int dir_anterior=dir;

	int puntero_ficticio=0;


	do {

		//dir_anterior=dir;

		debugger_disassemble(buffer,30,&longitud_opcode,dir);
		
		dir+=longitud_opcode;
		dir=adjust_address_memory_size(dir);
		puntero_ficticio+=longitud_opcode;

		//printf ("dir %X dir_anterior %X puntero_ficticio %d\n",dir,dir_anterior,puntero_ficticio);

		if (puntero_ficticio>=decremento) {
			return menu_debug_hexdump_adjusta_en_negativo(dir_inicial-longitud_opcode,1);
		}
		

	} while (1);


}

//Desensamblando usando un maximo de 64 caracteres
void menu_debug_dissassemble_una_inst_sino_hexa(char *dumpassembler,menu_z80_moto_int dir,int *longitud_final_opcode,int sino_hexa)
{

	char buf_temp_dir[65];
	char buf_temp_hexa[65];
	char buf_temp_opcode[65];

	size_t longitud_opcode;

	//int full_hexa_dump_motorola=1;

	//Direccion

	dir=adjust_address_memory_size(dir);


	//Texto direccion
	menu_debug_print_address_memory_zone(buf_temp_dir,dir);

	int longitud_direccion=MAX_LENGTH_ADDRESS_MEMORY_ZONE;

	//metemos espacio en 0 final
	dumpassembler[longitud_direccion]=' ';

	int max_longitud_volcado_hexa=8;

	//Hasta instrucciones de 8 bytes si se indica full dump
	//Si no, como maximo mostrara 4 bytes (longitud hexa=8)
	//El full dump solo aparece en menu disassemble, pero no en debug cpu


	//Texto opcode
	debugger_disassemble(buf_temp_opcode,64,&longitud_opcode,dir);


	//Texto volcado hexa
	//Primero meter espacios hasta limite 64
	int i;
	for (i=0;i<64;i++) {
		buf_temp_hexa[i]=' ';
	}

	buf_temp_hexa[i]=0;

	menu_debug_registers_dump_hex(buf_temp_hexa,dir,longitud_opcode);
	int longitud_texto_hex=longitud_opcode*2;
	//quitar el 0 final
	buf_temp_hexa[longitud_texto_hex]=' ';


	//agregar un espacio final para poder meter "+" en caso necesario, esto solo sucede en Motorola
		//Meter el 0 final donde diga el limite de volcado
		buf_temp_hexa[max_longitud_volcado_hexa]=0;

	//Si meter +
	if (longitud_texto_hex>max_longitud_volcado_hexa) {
		buf_temp_hexa[max_longitud_volcado_hexa]='+';
	}


	//Montar todo
	if (sino_hexa) {
		sprintf(dumpassembler,"%s %s %s",buf_temp_dir,buf_temp_hexa,buf_temp_opcode);
	}

	else {
		sprintf(dumpassembler,"%s %s",buf_temp_dir,buf_temp_opcode);
	}

	*longitud_final_opcode=longitud_opcode;

}


void menu_debug_dissassemble_una_instruccion(char *dumpassembler,menu_z80_moto_int dir,int *longitud_final_opcode)
{
	menu_debug_dissassemble_una_inst_sino_hexa(dumpassembler,dir,longitud_final_opcode,1);
}


void menu_debug_set_memory_zone_attr(void)
{

	int readwrite;

	if (menu_debug_show_memory_zones==0) {
		menu_debug_memory_zone_size=65536;
		return;
	}

	//Primero ver si zona actual no esta disponible, fallback a 0 que siempre esta
	 menu_debug_memory_zone_size=machine_get_memory_zone_attrib(menu_debug_memory_zone,&readwrite);
	if (!menu_debug_memory_zone_size) {
		//printf ("Zona no disponible. Fallback a memory mapped\n");
		menu_debug_set_memory_zone_mapped();
		//menu_debug_memory_zone=0;
		//menu_debug_memory_zone_size=machine_get_memory_zone_attrib(menu_debug_memory_zone,&readwrite);
	}
}

z80_byte menu_debug_get_mapped_byte(int direccion)
{

	//Mostrar memoria normal
	if (menu_debug_show_memory_zones==0) {
		//printf ("menu_debug_get_mapped_byte dir %04XH result %02XH\n",direccion,peek_byte_z80_moto(direccion));
		return peek_byte_z80_moto(direccion);
	}


	//Mostrar zonas mapeadas
	//printf ("menu_debug_get_mapped_byte 1\n");
	menu_debug_set_memory_zone_attr();

	//Aqui si se ha hecho fallback a mapped zone, recomprobar de nuevo
	if (menu_debug_show_memory_zones==0) {
		//printf ("menu_debug_get_mapped_byte dir %04XH result %02XH\n",direccion,peek_byte_z80_moto(direccion));
		//printf ("menu_debug_get_mapped_byte 1.5\n");
		return peek_byte_z80_moto(direccion);
	}	

	//printf ("menu_debug_get_mapped_byte 2\n");

	//printf ("menu_debug_get_mapped_byte menu_debug_memory_zone_size: %d\n",menu_debug_memory_zone_size);

	direccion=direccion % menu_debug_memory_zone_size;
	//printf ("menu_debug_get_mapped_byte 3\n");
	return *(machine_get_memory_zone_pointer(menu_debug_memory_zone,direccion));
	


}


void menu_debug_write_mapped_byte(int direccion,z80_byte valor)
{



	//Mostrar memoria normal
	if (menu_debug_show_memory_zones==0) {
		return poke_byte_z80_moto(direccion,valor);
	}


	//Mostrar zonas mapeadas
	menu_debug_set_memory_zone_attr();


	//Aqui si se ha hecho fallback a mapped zone, recomprobar de nuevo
	if (menu_debug_show_memory_zones==0) {
		return poke_byte_z80_moto(direccion,valor);
	}	

	direccion=direccion % menu_debug_memory_zone_size;
	*(machine_get_memory_zone_pointer(menu_debug_memory_zone,direccion))=valor;



}


menu_z80_moto_int adjust_address_memory_size(menu_z80_moto_int direccion)
{

	//Si modo mapeo normal
	if (menu_debug_show_memory_zones==0) {
		return adjust_address_space_cpu(direccion);
	}

	//Si zonas memoria mapeadas
	if (direccion>=menu_debug_memory_zone_size) {
		//printf ("ajustamos direccion %x a %x\n",direccion,menu_debug_memory_zone_size);
		direccion=direccion % menu_debug_memory_zone_size;
		//printf ("resultado ajustado: %x\n",direccion);
	}

	return direccion;
}


void menu_debug_set_memory_zone_mapped(void)
{
		menu_debug_memory_zone=-1;
		menu_debug_show_memory_zones=0;	
		menu_debug_memory_zone_size=65536;
}


//Retorna -1 si mapped memory. 0 o en adelante si otros. -2 si ESC
int menu_change_memory_zone_list_title(char *titulo)
{

        menu_item *array_menu_memory_zones;
        menu_item item_seleccionado;
        int retorno_menu;
		int menu_change_memory_zone_list_opcion_seleccionada=0;
        //do {

                char buffer_texto[40];

				

				menu_add_item_menu_inicial_format(&array_menu_memory_zones,MENU_OPCION_NORMAL,NULL,NULL,"Mapped memory");
				menu_add_item_menu_valor_opcion(array_menu_memory_zones,-1);

                int zone=-1;
				int i=1;
                do {

					zone++;
					zone=machine_get_next_available_memory_zone(zone);
					if (zone>=0) {
						machine_get_memory_zone_name(zone,buffer_texto);
						menu_add_item_menu_format(array_menu_memory_zones,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto);
						menu_add_item_menu_valor_opcion(array_menu_memory_zones,zone);

						if (menu_debug_memory_zone==zone) menu_change_memory_zone_list_opcion_seleccionada=i;

					}
					i++;
				} while (zone>=0);


                menu_add_item_menu(array_menu_memory_zones,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                //menu_add_item_menu(array_menu_hardware_set_f_func_action,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_memory_zones);

                retorno_menu=menu_dibuja_menu(&menu_change_memory_zone_list_opcion_seleccionada,&item_seleccionado,array_menu_memory_zones,titulo );

                


				if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
						//Cambiamos la zona
						int valor_opcion=item_seleccionado.valor_opcion;
						return valor_opcion;
                    
                }

	return -2;

}

int menu_change_memory_zone_list(void)
{
  return menu_change_memory_zone_list_title("Zones");
 }

void menu_set_memzone(int valor_opcion)
{
if (valor_opcion<0) {
		menu_debug_set_memory_zone_mapped();
	}
	else {
		menu_debug_show_memory_zones=1;
		menu_debug_memory_zone=valor_opcion;
	}	
}

void menu_debug_change_memory_zone(void)
{
	int valor_opcion=menu_change_memory_zone_list();
	if (valor_opcion==-2) return; //Pulsado ESC
	
	menu_set_memzone(valor_opcion);


	/*if (menu_debug_show_memory_zones==0) menu_debug_show_memory_zones=1;

	//Si se ha habilitado en el if anterior, entrara aqui
	if (menu_debug_show_memory_zones) {
		menu_debug_memory_zone++;
		menu_debug_memory_zone=machine_get_next_available_memory_zone(menu_debug_memory_zone);
		if (menu_debug_memory_zone<0)  {
			menu_debug_set_memory_zone_mapped();

		}
	}
	*/
}

void menu_debug_change_memory_zone_non_interactive(void)
{


	if (menu_debug_show_memory_zones==0) menu_debug_show_memory_zones=1;

	//Si se ha habilitado en el if anterior, entrara aqui
	if (menu_debug_show_memory_zones) {
		menu_debug_memory_zone++;
		menu_debug_memory_zone=machine_get_next_available_memory_zone(menu_debug_memory_zone);
		if (menu_debug_memory_zone<0)  {
			menu_debug_set_memory_zone_mapped();

		}
	}
	
}

void menu_debug_set_memory_zone(int zone)
{
	//Cambiar a zona memoria indicada
	int salir=0;

	//int zona_inicial=menu_debug_memory_zone;

	while (menu_debug_memory_zone!=zone && salir<2) {
		menu_debug_change_memory_zone_non_interactive();

		//Si ha pasado dos veces por la zona mapped, es que no existe dicha zona
		if (menu_debug_memory_zone<0) salir++;
	}
}

int menu_get_current_memory_zone_name_number(char *s)
{
	if (menu_debug_show_memory_zones==0) {
		strcpy(s,"Mapped memory");
		return -1;
	}

	machine_get_memory_zone_name(menu_debug_memory_zone,s);
	return menu_debug_memory_zone;
}


//Retorna el numero de digitos para representar un numero en hexadecimal
int menu_debug_get_total_digits_hexa(int valor)
{
	char temp_digitos[20];
	sprintf (temp_digitos,"%X",valor);
	return strlen(temp_digitos);
}

//Retorna el numero de digitos para representar un numero en decimal
int menu_debug_get_total_digits_dec(int valor)
{
	char temp_digitos[20];
	sprintf (temp_digitos,"%d",valor);
	return strlen(temp_digitos);
}

//Escribe una direccion en texto, en hexa, teniendo en cuenta zona memoria (rellenando espacios segun tamanyo zona)
void menu_debug_print_address_memory_zone(char *texto, menu_z80_moto_int address)
{
	//primero meter 6 espacios
	sprintf (texto,"      ");

	address=adjust_address_memory_size(address);
	//int longitud_direccion=MAX_LENGTH_ADDRESS_MEMORY_ZONE;

	//Obtener cuantos digitos hexa se necesitan
	//char temp_digitos[20];
	//sprintf (temp_digitos,"%X",menu_debug_memory_zone_size-1);
	//int digitos=strlen(temp_digitos);

	int digitos=menu_debug_get_total_digits_hexa(menu_debug_memory_zone_size-1);

	//Obtener posicion inicial a escribir direccion. Suponemos maximo 6
	int posicion_inicial_digitos=6-digitos;


	//Escribimos direccion
	sprintf (&texto[posicion_inicial_digitos],"%0*X",digitos,address);
}


//Retorna indice color asociado a la paleta indicada
int menu_debug_sprites_return_index_palette(int paleta, z80_byte color)
{
	switch (paleta) {
		case 0:
			return tbblue_palette_ula_first[color];
		break;

		case 1:
			return tbblue_palette_ula_second[color];
		break;			

		case 2:
			return tbblue_palette_layer2_first[color];
		break;

		case 3:
			return tbblue_palette_layer2_second[color];
		break;		

		case 4:
			return tbblue_palette_sprite_first[color];
		break;

		case 5:
			return tbblue_palette_sprite_second[color];
		break;

		case 6:
			return tbblue_palette_tilemap_first[color];
		break;		

		case 7:
			return tbblue_palette_tilemap_second[color];
		break;	
	}

	return color;
}

//Retorna valor de color asociado a la paleta actual mapeada
int menu_debug_sprites_return_color_palette(int paleta, z80_byte color)
{

	int index;

	index=menu_debug_sprites_return_index_palette(paleta, color);
	return RGB9_INDEX_FIRST_COLOR+index;
}


void menu_debug_sprites_change_palette(void)
{
	view_sprites_palette++;
	if (view_sprites_palette==MENU_TOTAL_MAPPED_PALETTES) view_sprites_palette=0;
}


void menu_debug_sprites_get_palette_name(int paleta, char *s)
{
	switch (paleta) {
		case 0:
			strcpy(s,"ULA 1");
		break;

		case 1:
			strcpy(s,"ULA 2");
		break;	

		case 2:
			strcpy(s,"Layer2 1");
		break;

		case 3:
			strcpy(s,"Layer2 2");
		break;	

		case 4:
			strcpy(s,"Sprites 1");
		break;

		case 5:
			strcpy(s,"Sprites 2");
		break;			

		case 6:
			strcpy(s,"Tilemap 1");
		break;

		case 7:
			strcpy(s,"Tilemap 2");
		break;	

		default:
			strcpy(s,"UNKNOWN");
		break;
	}
}


void menu_debug_sprites_change_bpp(void)
{
	view_sprites_bpp=view_sprites_bpp <<1;
	view_sprites_ppb=view_sprites_ppb >>1;

	if (view_sprites_bpp > 8) {
		view_sprites_bpp=4;
		view_sprites_ppb=2;
	}

	if (view_sprites_bpp < 4) {
		view_sprites_bpp=8;
		view_sprites_ppb=1;
	}
}


menu_z80_moto_int menu_debug_draw_sprites_get_pointer_offset(int direccion)
{

	menu_z80_moto_int puntero;

	puntero=direccion; //por defecto

	if (view_sprites_hardware) {

		{
			if (view_sprites_bpp==4) puntero=view_sprites_direccion*TBBLUE_SPRITE_4BPP_SIZE;
			else puntero=view_sprites_direccion*TBBLUE_SPRITE_8BPP_SIZE;
		}


	}


	return puntero;

}


z80_byte menu_debug_draw_sprites_get_byte(menu_z80_moto_int puntero)
{

	//printf ("menu_debug_draw_sprites_get_byte 1\n");
	z80_byte byte_leido;

					puntero=adjust_address_memory_size(puntero);

					//printf ("menu_debug_draw_sprites_get_byte 2\n");
				byte_leido=menu_debug_get_mapped_byte(puntero);
					//printf ("menu_debug_draw_sprites_get_byte 3\n");
				
				if (view_sprites_inverse.v) {
					byte_leido ^=255;
				}

	return byte_leido;
}

void menu_debug_draw_sprites(void)
{

	if (!zxvision_drawing_in_background) normal_overlay_texto_menu();


	menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech	


	//int sx=SPRITES_X+1;
	int sx=1;
	//int sy=SPRITES_Y+3;
	
	//Si es mas ancho, que ventana visible, mover coordenada x 1 posicion atrás
	//if (view_sprites_ancho_sprite/menu_char_width>=SPRITES_ANCHO-2) sx--;

	//Si es mas alto, mover coordenada sprite a 1, asi podemos mostrar sprites de hasta 192 de alto
	//if (view_sprites_alto_sprite>168) sy=1;

	//Si se pasa aun mas
	//if (view_sprites_alto_sprite>184) sy=0;

        int xorigen=sx*menu_char_width;
        
		int yorigen=16; //sy*8;


        int x,y,bit;
	z80_byte byte_leido;

	menu_z80_moto_int puntero;

	//puntero=view_sprites_direccion;

	//Ajustar valor puntero en caso de sprites tsconf por ejemplo
	puntero=menu_debug_draw_sprites_get_pointer_offset(view_sprites_direccion);


	int color;

	int finalx;

	menu_z80_moto_int puntero_inicio_linea;

	//int maximo_visible_x=32*menu_char_width;



		int tamanyo_linea=view_sprites_bytes_por_linea;

		for (y=0;y<view_sprites_alto_sprite;y++) {
			if (view_sprites_scr_sprite && y<192) {
				 puntero=view_sprites_direccion+screen_addr_table[(y<<5)];
			}

			puntero_inicio_linea=puntero;
			finalx=xorigen;
			for (x=0;x<view_sprites_ancho_sprite;) {
				//printf ("puntero: %d\n",puntero);
				puntero=adjust_address_memory_size(puntero);


				menu_z80_moto_int puntero_final;

				puntero_final=puntero;
				byte_leido=menu_debug_draw_sprites_get_byte(puntero_final);

				/*byte_leido=menu_debug_get_mapped_byte(puntero);

				

				//Si hay puntero a valores en rom como algunos juegos pseudo hires de zx81
				if (view_sprites_zx81_pseudohires.v) {
					int temp_inverse=0; //si se hace inverse derivado de juegos pseudo hires de zx81
					if (byte_leido&128) temp_inverse=1;

					z80_int temp_dir=reg_i*256+(8*(byte_leido&63));
					byte_leido=peek_byte_no_time(temp_dir);

					if (temp_inverse) byte_leido ^=255;
				}
	

				if (view_sprites_inverse.v) {
					byte_leido ^=255;
				}
				*/

				puntero +=view_sprite_incremento;

				int incx=0;

				for (bit=0;bit<8;bit+=view_sprites_bpp,incx++,finalx++,x++) {


				

					int dis=(8-(incx+1)*view_sprites_bpp);

					//printf ("incx: %d dis: %d\n",incx,dis);

					color=byte_leido >> dis;
					z80_byte mascara = 0;
					switch (view_sprites_bpp) {
						case 1:
							mascara=1;
						break;

						case 2:
							mascara=3;
						break;

						case 4:
							mascara=15;
						break;

						case 8:
							mascara=255;
						break;
					}

					color=color & mascara;

				//en 8ppb (1 bpp), primer color rotar 7, luego 6, luego 5, .... 0 (8-(pos actual+1)*bpp). mascara 1
				//en 4ppb (2 bpp), primer color rotar 6, luego 4, luego 2, luego 0 (8-(pos actual+1)*bpp). mascara 3
				//en 2ppb (4 bpp), primer color rotar 4, luego 0  (8-(pos actual+1)*bpp). mascara 15
				//en 1ppb (8 bpp), rotar 0 . mascara 255  (8-(pos actual+1)*bpp). mascara 255

				//Caso 1 bpp
				if (view_sprites_bpp==1) {
                                        if ( color ==0 ) color=ESTILO_GUI_PAPEL_NORMAL;
                                        else color=ESTILO_GUI_TINTA_NORMAL;
				}
				else {
					color=menu_debug_sprites_return_color_palette(view_sprites_palette,color+view_sprites_offset_palette);

				}

	            
				zxvision_putpixel(menu_debug_draw_sprites_window,finalx,yorigen+y,color);
			   }
			}

			puntero=puntero_inicio_linea;
			puntero +=tamanyo_linea;

		}
	

	zxvision_draw_window_contents(menu_debug_draw_sprites_window);

}

menu_z80_moto_int menu_debug_view_sprites_change_pointer(menu_z80_moto_int p)
{

       //restauramos modo normal de texto de menu, porque sino, tendriamos la ventana
        //del cambio de direccion, y encima los sprites
       set_menu_overlay_function(normal_overlay_texto_menu);


        char string_address[10];



				if (view_sprites_hardware) {
					sprintf(string_address,"%d",p&63);
					menu_ventana_scanf("Sprite?",string_address,3);
				}
				else {
					util_sprintf_address_hex(p,string_address);
        	menu_ventana_scanf("Address?",string_address,10);
				}



        p=parse_string_to_number(string_address);



        set_menu_overlay_function(menu_debug_draw_sprites);


        return p;

}





//Retorna 0 si ok
int menu_debug_view_sprites_save(menu_z80_moto_int direccion,int ancho, int alto, int ppb, int incremento)
{

	char file_save[PATH_MAX];

	char *filtros[3];

						 filtros[0]="pbm";
						 filtros[1]="c";
						 filtros[2]=0;

		int ret;

		 ret=menu_filesel("Select PBM/C File",filtros,file_save);

		 if (ret==1) {

 		 		//Ver si archivo existe y preguntar
			 if (si_existe_archivo(file_save)) {

	 				if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return 0;

 				}


				char string_height[4];
				sprintf(string_height,"%d",alto);
				menu_ventana_scanf("Height?",string_height,4);
				alto=parse_string_to_number(string_height);


				//Asignar buffer temporal
				int longitud=ancho*alto;
				z80_byte *buf_temp=malloc(longitud);
				if (buf_temp==NULL) {
					debug_printf(VERBOSE_ERR,"Error allocating temporary buffer");
				}

				//Copiar de memoria emulador ahi
				int i;
				for (i=0;i<longitud;i++) {
					//buf_temp[i]=peek_byte_z80_moto(direccion);
					buf_temp[i]=menu_debug_draw_sprites_get_byte(direccion);
					direccion +=incremento;
				}


				if (!util_compare_file_extension(file_save,"pbm")) {
					util_write_pbm_file(file_save,ancho,alto,ppb,buf_temp);
				}

				else if (!util_compare_file_extension(file_save,"c")) {
					util_write_sprite_c_file(file_save,ancho,alto,ppb,buf_temp);
				}

				else {
					//debug_printf (VERBOSE_ERR,"Error. Unkown sprite file format");
					return 1;
				}

				free(buf_temp);
		}

	return 0;

}


void menu_debug_sprites_get_parameters_hardware(void)
{
	if (view_sprites_hardware) {
		{
			//Parametros que alteramos segun sprite activo


                	view_sprites_ancho_sprite=16;

	                view_sprites_alto_sprite=16;



			//offset paleta
			view_sprites_offset_palette=0;

			view_sprites_increment_cursor_vertical=1; //saltar de 1 en 1






			//Permitir 4 u 8 bpp
			if (view_sprites_bpp!=8 && view_sprites_bpp!=4) {
				view_sprites_bpp=4;
				view_sprites_ppb=2;
			}


            view_sprites_bytes_por_linea=16/view_sprites_ppb;


			view_sprites_bytes_por_ventana=8; 


			//Cambiar a zona memoria 14. TBBlue sprites
			//while (menu_debug_memory_zone!=14) menu_debug_change_memory_zone_non_interactive();

			menu_debug_set_memory_zone(14);

			//paleta 11 tbblue
			//view_sprites_palette=11;


		}

	}

	else {
		view_sprites_bytes_por_linea=view_sprites_ancho_sprite/view_sprites_ppb;
		view_sprites_bytes_por_ventana=view_sprites_bytes_por_linea*view_sprites_alto_sprite;
		view_sprites_increment_cursor_vertical=view_sprites_bytes_por_linea;
	}
}

void menu_debug_view_sprites_textinfo(zxvision_window *ventana)
{
	int linea=0;
		char buffer_texto[100];

		//Antes de escribir, normalizar zona memoria
		menu_debug_set_memory_zone_attr();

		int restoancho=view_sprites_ancho_sprite % view_sprites_ppb;
		if (view_sprites_ancho_sprite-restoancho>0) view_sprites_ancho_sprite-=restoancho;

		//esto antes que menu_debug_sprites_get_parameters_hardware
		view_sprites_direccion=adjust_address_memory_size(view_sprites_direccion);

		menu_debug_sprites_get_parameters_hardware();



		char texto_memptr[50];

		if (view_sprites_hardware) {

			char texto_sprite[32];
			strcpy(texto_sprite,"Sprite");
			int max_sprites;
			{
				if (view_sprites_bpp==4) max_sprites=TBBLUE_MAX_PATTERNS*2;
				else max_sprites=TBBLUE_MAX_PATTERNS;

				strcpy(texto_sprite,"Pattern");
			}

			sprintf(texto_memptr,"%s: %3d",texto_sprite,view_sprites_direccion%max_sprites); //dos digitos, tsconf hace 85 y tbblue hace 64. suficiente
		}

		else {
            char buffer_direccion[MAX_LENGTH_ADDRESS_MEMORY_ZONE+1];
            menu_debug_print_address_memory_zone(buffer_direccion,view_sprites_direccion);
			sprintf(texto_memptr,"Memptr:%s",buffer_direccion);
		}


	


		sprintf (buffer_texto,"%s Size:%dX%d %dBPP",texto_memptr,view_sprites_ancho_sprite,view_sprites_alto_sprite,view_sprites_bpp);

		zxvision_print_string_defaults_fillspc(ventana,1,linea++,buffer_texto);



		linea=SPRITES_ALTO+3;

		char buffer_primera_linea[64]; //dar espacio de mas para poder alojar el ~de los atajos
		char buffer_segunda_linea[86];

		char buffer_tercera_linea[64];

		//Forzar a mostrar atajos
		z80_bit antes_menu_writing_inverse_color;
		antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
		menu_writing_inverse_color.v=1;

		char nombre_paleta[33];
		menu_debug_sprites_get_palette_name(view_sprites_palette,nombre_paleta);

		sprintf(buffer_tercera_linea,"Pa~~l.: %s. O~~ff:%d",nombre_paleta,view_sprites_offset_palette);


		char mensaje_texto_hardware[33];

		//por defecto
		mensaje_texto_hardware[0]=0;

			sprintf(mensaje_texto_hardware,"[%c] ~~hardware",(view_sprites_hardware ? 'X' : ' ') );

		char mensaje_texto_zx81_pseudohires[33];
		//por defecto
		mensaje_texto_zx81_pseudohires[0]=0;

		sprintf(buffer_primera_linea,"~~memptr In~~c+%d ~~o~~p~~q~~a:Size ~~bpp %s",
		view_sprite_incremento,
		(view_sprites_bpp==1 && !view_sprites_scr_sprite ? "~~save " : ""));

		sprintf(buffer_segunda_linea, "[%c] ~~inv [%c] Sc~~r %s%s",
					(view_sprites_inverse.v ? 'X' : ' '),
					(view_sprites_scr_sprite ? 'X' : ' '),
					mensaje_texto_hardware,mensaje_texto_zx81_pseudohires);


		zxvision_print_string_defaults_fillspc(ventana,1,linea++,buffer_primera_linea);
		zxvision_print_string_defaults_fillspc(ventana,1,linea++,buffer_segunda_linea);
		zxvision_print_string_defaults_fillspc(ventana,1,linea++,buffer_tercera_linea);

		//Mostrar zona memoria

		char textoshow[33];

		char memory_zone_text[64]; //espacio temporal mas grande por si acaso

		if (menu_debug_show_memory_zones==0) {
			sprintf (memory_zone_text,"Mem ~~zone (mapped memory)");
		}

		else {
			//printf ("Info zona %d\n",menu_debug_memory_zone);
			char buffer_name[MACHINE_MAX_MEMORY_ZONE_NAME_LENGHT+1];
			//int readwrite;
			machine_get_memory_zone_name(menu_debug_memory_zone,buffer_name);
			sprintf (memory_zone_text,"Mem ~~zone (%d %s)",menu_debug_memory_zone,buffer_name);
			//printf ("size: %X\n",menu_debug_memory_zone_size);
			//printf ("Despues zona %d\n",menu_debug_memory_zone);
		}

		//truncar texto a 32 por si acaso
		memory_zone_text[32]=0;


		zxvision_print_string_defaults_fillspc(ventana,1,linea++,memory_zone_text);

		sprintf (textoshow," Size: %d (%d KB)",menu_debug_memory_zone_size,menu_debug_memory_zone_size/1024);
		
		zxvision_print_string_defaults_fillspc(ventana,1,linea++,textoshow);

	

		//Restaurar comportamiento atajos
		menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;


}


void menu_debug_view_sprites(MENU_ITEM_PARAMETERS)
{

	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();



	zxvision_window *ventana;
	ventana=&zxvision_window_view_sprites;

    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
	zxvision_delete_window_if_exists(ventana);

	int x,y,ancho,alto;

	
    if (!util_find_window_geometry("sprites",&x,&y,&ancho,&alto)) {
        x=SPRITES_X;
        y=SPRITES_Y;
        ancho=SPRITES_ANCHO;
        alto=SPRITES_ALTO_VENTANA;
    }


	zxvision_new_window_nocheck_staticsize(ventana,x,y,ancho,alto,64,64+2,"Sprites");
	ventana->can_be_backgrounded=1;
	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"sprites");

	//Permitir hotkeys desde raton
	ventana->can_mouse_send_hotkeys=1;

	zxvision_draw_window(ventana);

	z80_byte tecla;


    //Cambiamos funcion overlay de texto de menu
    //Se establece a la de funcion de ver sprites
    set_menu_overlay_function(menu_debug_draw_sprites);

	menu_debug_draw_sprites_window=ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui	


       //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
       //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
       if (zxvision_currently_restoring_windows_on_start) {
               //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
               return;
       }	

	int redibujar_texto=1;	


    do {


		//Solo redibujar el texto cuando alguna tecla pulsada sea de las validas,
		//y no con mouse moviendo la ventana
		//porque usa mucha cpu y por ejemplo en maquina tsconf se clava si arrastramos ventana
		if (redibujar_texto) {
			//printf ("redibujamos texto\n");


			menu_debug_view_sprites_textinfo(ventana);

		
			//Si no esta multitarea, mostrar el texto que acabamos de escribir
	    	if (!menu_multitarea) {
				zxvision_draw_window_contents(ventana);
			}			
		}

	
		tecla=zxvision_common_getkey_refresh();
		//printf ("tecla: %d\n",tecla);

		if (tecla) redibujar_texto=1;

        switch (tecla) {

					case 8:
						//izquierda
						view_sprites_direccion--;
					break;

					case 9:
						//derecha
						view_sprites_direccion++;
					break;

                    case 11:
                        //arriba
                        view_sprites_direccion -=view_sprites_increment_cursor_vertical;
                    break;

                    case 10:
                        //abajo
                        view_sprites_direccion +=view_sprites_increment_cursor_vertical;
                    break;

                    case 24:
                        //PgUp
                        view_sprites_direccion -=view_sprites_bytes_por_ventana;
                    break;

                    case 25:
                        //PgDn
                        view_sprites_direccion +=view_sprites_bytes_por_ventana;
                    break;

                    case 'm':
                        view_sprites_direccion=menu_debug_view_sprites_change_pointer(view_sprites_direccion);
							zxvision_draw_window(ventana);
                    break;

					case 'b':
						menu_debug_sprites_change_bpp();
					break;

					case 'f':
						view_sprites_offset_palette++;
						if (view_sprites_offset_palette>=256) view_sprites_offset_palette=0;
					break;


					case 'l':
						menu_debug_sprites_change_palette();
					break;

					case 'h':
						 view_sprites_hardware ^=1;								
					break;

					case 'e':
					break;

					case 'i':
						view_sprites_inverse.v ^=1;
					break;

					case 'z':
							//restauramos modo normal de texto de menu, sino, el selector de zona se vera
								//con el sprite encima
						set_menu_overlay_function(normal_overlay_texto_menu);

						menu_debug_change_memory_zone();

						set_menu_overlay_function(menu_debug_draw_sprites);

						break;

					case 's':


						if (view_sprites_hardware) {

						}

						else {

							//Solo graba sprites de 1bpp (monocromos)
							if (view_sprites_bpp==1 && !view_sprites_scr_sprite) {
								//restauramos modo normal de texto de menu, sino, el selector de archivos se vera
								//con el sprite encima
								set_menu_overlay_function(normal_overlay_texto_menu);


								if (menu_debug_view_sprites_save(view_sprites_direccion,view_sprites_ancho_sprite,view_sprites_alto_sprite,view_sprites_ppb,view_sprite_incremento)) {
									menu_error_message("Unknown file format");
								}

								//cls_menu_overlay();

								//menu_debug_view_sprites_ventana();
								set_menu_overlay_function(menu_debug_draw_sprites);
								zxvision_draw_window(ventana);
							}

						}
					break;

					case 'o':
						if (view_sprites_ancho_sprite>view_sprites_ppb) view_sprites_ancho_sprite -=view_sprites_ppb;
					break;

					case 'p':

						if (view_sprites_ancho_sprite<512) view_sprites_ancho_sprite +=view_sprites_ppb;
					break;

                    case 'q':
                        if (view_sprites_alto_sprite>1) view_sprites_alto_sprite--;
                    break;

                    case 'a':
						if (view_sprites_alto_sprite<512)  view_sprites_alto_sprite++;
                    break;

					case 'c':
							if (view_sprite_incremento==1) view_sprite_incremento=2;
							else view_sprite_incremento=1;
					break;

					case 'r':
							view_sprites_scr_sprite ^=1;
					break;

					default:
						//no es tecla valida, no redibujar texto
						redibujar_texto=0;
					break;

		}


    } while (tecla!=2 && tecla!=3);



	zxvision_set_window_overlay_from_current(ventana);

    //restauramos modo normal de texto de menu
    set_menu_overlay_function(normal_overlay_texto_menu);

    cls_menu_overlay();

    	//Grabar geometria ventana

		util_add_window_geometry_compact(ventana);	


	if (tecla==3) {
		zxvision_message_put_window_background();
	}	

	else {



		zxvision_destroy_window(ventana);		
	}



}


int menu_breakpoints_cond(void)
{
	return debug_breakpoints_enabled.v;
}



void menu_breakpoints_conditions_set(MENU_ITEM_PARAMETERS)
{
        //printf ("linea: %d\n",breakpoints_opcion_seleccionada);

	//saltamos los breakpoints de registro pc y la primera linea
        //int breakpoint_index=breakpoints_opcion_seleccionada-MAX_BREAKPOINTS-1;

	//saltamos las primeras 2 lineas
	//int breakpoint_index=breakpoints_opcion_seleccionada-2;

	int breakpoint_index=valor_opcion;

  char string_texto[MAX_BREAKPOINT_CONDITION_LENGTH];

			exp_par_tokens_to_exp(debug_breakpoints_conditions_array_tokens[breakpoint_index],string_texto,MAX_PARSER_TOKENS_NUM);
			
			

  menu_ventana_scanf("Condition",string_texto,MAX_BREAKPOINT_CONDITION_LENGTH);

  debug_set_breakpoint(breakpoint_index,string_texto);

	//comprobar error
	if (if_pending_error_message) {
		menu_muestra_pending_error_message(); //Si se genera un error derivado del set breakpoint, mostrarlo y salir
		return;
	}


	sprintf (string_texto,"%s",debug_breakpoints_actions_array[breakpoint_index]);

  menu_ventana_scanf("Action? (enter=normal)",string_texto,MAX_BREAKPOINT_CONDITION_LENGTH);

  debug_set_breakpoint_action(breakpoint_index,string_texto);

}


void menu_breakpoints_condition_evaluate_new(MENU_ITEM_PARAMETERS)
{

        char string_texto[MAX_BREAKPOINT_CONDITION_LENGTH];
	string_texto[0]=0;

        menu_ventana_scanf("Expression",string_texto,MAX_BREAKPOINT_CONDITION_LENGTH);


        //menu_generic_message_format("Result","%s -> %s",string_texto,(result ? "True" : "False " ));


	//int exp_par_evaluate_expression(char *entrada,char *salida)
	char buffer_salida[256]; //mas que suficiente
	char string_detoken[MAX_BREAKPOINT_CONDITION_LENGTH];

	int result=exp_par_evaluate_expression(string_texto,buffer_salida,string_detoken);
	if (result==0) {
		menu_generic_message_format("Result","Parsed string: %s\nResult: %s",string_detoken,buffer_salida);		
	}

	else if (result==1) {
		menu_error_message(buffer_salida);
	}

	else {
		menu_generic_message_format("Error","%s parsed string: %s",buffer_salida,string_detoken);
	}

	
}




void menu_breakpoints_enable_disable(MENU_ITEM_PARAMETERS)
{
        if (debug_breakpoints_enabled.v==0) {
                debug_breakpoints_enabled.v=1;

		breakpoints_enable();
        }


        else {
                debug_breakpoints_enabled.v=0;

		breakpoints_disable();
        }

}


void menu_breakpoints_condition_enable_disable(MENU_ITEM_PARAMETERS)
{
	debug_breakpoints_conditions_toggle(valor_opcion);

}



void menu_debug_registers_dump_hex(char *texto,menu_z80_moto_int direccion,int longitud)
{

	z80_byte byte_leido;

	int puntero=0;

	for (;longitud>0;longitud--) {
		//direccion=adjust_address_space_cpu(direccion);
		direccion=adjust_address_memory_size(direccion);

			//byte_leido=peek_byte_z80_moto(direccion);
			byte_leido=menu_debug_get_mapped_byte(direccion);
			//printf ("dump hex: %X\n",direccion);
			direccion++;
		//}

		sprintf (&texto[puntero],"%02X",byte_leido);

		puntero+=2;

	}
}


void menu_debug_registers_dump_ascii(char *texto,menu_z80_moto_int direccion,int longitud,z80_byte valor_xor)
{

        z80_byte byte_leido;

        int puntero=0;
				//printf ("dir ascii: %d\n",direccion);

        for (;longitud>0;longitud--) {
							//direccion=adjust_address_space_cpu(direccion);
							direccion=adjust_address_memory_size(direccion);

									byte_leido=menu_debug_get_mapped_byte(direccion) ^ valor_xor;
									direccion++;



		if (byte_leido<32 || byte_leido>126) byte_leido='.';


                sprintf (&texto[puntero],"%c",byte_leido);

                puntero+=1;

        }
}

void menu_debug_get_memory_pages(char *s)
{
	debug_memory_segment segmentos[MAX_DEBUG_MEMORY_SEGMENTS];
        int total_segmentos=debug_get_memory_pages_extended(segmentos);

        int i;
        int longitud;
        int indice=0;

        for (i=0;i<total_segmentos;i++) { 
        	longitud=strlen(segmentos[i].shortname)+1;
        	sprintf(&s[indice],"%s ",segmentos[i].shortname);

        	indice +=longitud;

        }
		
}


int get_menu_debug_num_lineas_full(zxvision_window *w)
{
	//return 14;

	//24->14
	int lineas=w->visible_height-10;

	if (lineas<2) lineas=2;

	return lineas;
}


void menu_debug_change_registers(void)
{
	char string_registervalue[61]; //REG=VALUE

	menu_ventana_scanf("Register?",menu_debug_change_registers_last_reg,30);

	menu_ventana_scanf("Value?",menu_debug_change_registers_last_val,30);

	sprintf (string_registervalue,"%s=%s",menu_debug_change_registers_last_reg,menu_debug_change_registers_last_val);

	if (debug_change_register(string_registervalue)) {
		debug_printf(VERBOSE_ERR,"Error changing register");
        }
}



void menu_debug_registers_change_ptr(void)
{



        char string_address[10];


                                        util_sprintf_address_hex(menu_debug_memory_pointer,string_address);
                menu_ventana_scanf("Address?",string_address,10);

        menu_debug_memory_pointer=parse_string_to_number(string_address);


        return;

}


                                         //Muestra el registro que le corresponde para esta linea
void menu_debug_show_register_line(int linea,char *textoregistros)
{
        debug_memory_segment segmentos[MAX_DEBUG_MEMORY_SEGMENTS];
        int total_segmentos=debug_get_memory_pages_extended(segmentos);

	int offset_bloque;

	//Por defecto, cadena vacia
	textoregistros[0]=0;
	
	//para mostrar vector interrupcion
	char string_vector_int[10]="     ";
	if (im_mode==2) {
	
	
	z80_int temp_i;
z80_int puntero_int;
z80_byte dir_l,dir_h;

							temp_i=reg_i*256+255;
							dir_l=peek_byte_no_time(temp_i++);
							dir_h=peek_byte_no_time(temp_i);
							puntero_int=value_8_to_16(dir_h,dir_l);
							
	sprintf(string_vector_int,"@%04X",puntero_int);
	
	}

	switch (linea) {
		case 0:
			sprintf (textoregistros,"PC %04X",get_pc_register() );
		break;

		case 1:
			sprintf (textoregistros,"SP %04X",reg_sp);
		break;

		case 2:
			sprintf (textoregistros,"AF %02X%02X'%02X%02X",reg_a,Z80_FLAGS,reg_a_shadow,Z80_FLAGS_SHADOW);
		break;

		case 3:
			sprintf (textoregistros,"%c%c%c%c%c%c%c%c",DEBUG_STRING_FLAGS);
		break;		

		case 4:
			sprintf (textoregistros,"HL %04X'%02X%02X",HL,reg_h_shadow,reg_l_shadow);
		break;

		case 5:
			sprintf (textoregistros,"DE %04X'%02X%02X",DE,reg_d_shadow,reg_e_shadow);
		break;

		case 6:
			sprintf (textoregistros,"BC %04X'%02X%02X",BC,reg_b_shadow,reg_c_shadow);
		break;

		case 7:
			sprintf (textoregistros,"IX %04X",reg_ix);
		break;

		case 8:
			sprintf (textoregistros,"IY %04X",reg_iy);
		break;

		case 9:
			sprintf (textoregistros,"IR %02X%02X%s",reg_i,(reg_r&127)|(reg_r_bit7&128) , string_vector_int);
		break;

		case 10:
			sprintf (textoregistros,"IM%d IFF%c%c",im_mode,DEBUG_STRING_IFF12 );
		break;

		/*case 12:
		case 13:
			menu_debug_get_memory_pages(textopaginasmem);
			menu_util_cut_line_at_spaces(12,textopaginasmem,textopaginasmem_linea1,textopaginasmem_linea2);
			if (linea==12) sprintf (textoregistros,"%s",textopaginasmem_linea1 );
			if (linea==13) sprintf (textoregistros,"%s",textopaginasmem_linea2 );
		break;*/

		case 11:
		case 12:
		case 13:
		case 14:
			//Por defecto, cad
			//Mostrar en una linea, dos bloques de memoria mapeadas
			offset_bloque=linea-11;
			
			offset_bloque *=2; //2 bloques por cada linea
			//primer bloque
			if (offset_bloque<total_segmentos) {
				sprintf (textoregistros,"[%s]",segmentos[offset_bloque].shortname);
				offset_bloque++;

				//Segundo bloque
				if (offset_bloque<total_segmentos) {
					int longitud=strlen(textoregistros);
					sprintf (&textoregistros[longitud],"[%s]",segmentos[offset_bloque].shortname);
				}
			}
		break;
	}
}

//Numero de lineas del listado principal de la vista
int menu_debug_get_main_list_view(zxvision_window *w)
{
	int lineas=1;

    if (menu_debug_registers_current_view==3 || menu_debug_registers_current_view==5) lineas=9;
    if (menu_debug_registers_current_view==1 || menu_debug_registers_current_view==4 || menu_debug_registers_current_view==6) lineas=get_menu_debug_num_lineas_full(w);
	if (menu_debug_registers_current_view==8) lineas=get_menu_debug_num_lineas_full(w)-2;

	return lineas;
}

//Si vista actual tiene desensamblado u otros datos. En el primer de los casos, los movimientos de cursor se gestionan mediante saltos de opcodes
int menu_debug_view_has_disassemly(void)
{
	if (menu_debug_registers_current_view<=4) return 1;

	return 0;
}

menu_z80_moto_int menu_debug_disassemble_subir_veces(menu_z80_moto_int posicion,int veces)
{
        int i;
        for (i=0;i<veces;i++) {
                posicion=menu_debug_disassemble_subir(posicion);
        }
        return posicion;
}


menu_z80_moto_int menu_debug_register_decrement_half(menu_z80_moto_int posicion,zxvision_window *w)
{
	int i;
	for (i=0;i<get_menu_debug_num_lineas_full(w)/2;i++) {
		posicion=menu_debug_disassemble_subir(posicion);
	}
	return posicion;
}

 

int menu_debug_hexdump_change_pointer(int p)
{


        char string_address[10];

        sprintf (string_address,"%XH",p);


        //menu_ventana_scanf("Address? (in hex)",string_address,6);
        menu_ventana_scanf("Address?",string_address,10);

	//p=strtol(string_address, NULL, 16);
	p=parse_string_to_number(string_address);


	return p;

}


//Ajustar cuando se pulsa hacia arriba por debajo de direccion 0.
//Debe poner el puntero hacia el final de la zona de memoria
menu_z80_moto_int menu_debug_hexdump_adjusta_en_negativo(menu_z80_moto_int dir,int linesize)
{
	if (dir>=menu_debug_memory_zone_size) {
		dir=menu_debug_memory_zone_size-linesize;
	}
	//printf ("menu_debug_memory_zone_size %X\n",menu_debug_memory_zone_size);

	return dir;
}


void menu_debug_next_dis_show_hexa(void)
{
	menu_debug_registers_subview_type++;

	if (menu_debug_registers_subview_type==4) menu_debug_registers_subview_type=0;
}

void menu_debug_registers_adjust_ptr_on_follow(void)
{
	if (menu_debug_follow_pc.v) {
                menu_debug_memory_pointer=get_pc_register();
                //Si se esta mirando zona copper
                if (menu_debug_memory_zone==MEMORY_ZONE_NUM_TBBLUE_COPPER) {
                        menu_debug_memory_pointer=tbblue_copper_pc;
                }

        }
}


void menu_debug_registros_parte_derecha(int linea,char *buffer_linea,int columna_registros,int mostrar_separador)
{

    char buffer_registros[33];
    if (menu_debug_registers_subview_type!=3) {

            //Quitar el 0 del final
            int longitud=strlen(buffer_linea);
            buffer_linea[longitud]=32;

            //Muestra el registro que le corresponde para esta linea
            menu_debug_show_register_line(linea,buffer_registros);

            //Agregar registro que le corresponda. Columna 19 normalmente. Con el || del separador para quitar el color seleccionado
            if (mostrar_separador) sprintf(&buffer_linea[columna_registros],"||%s",buffer_registros);
            else sprintf(&buffer_linea[columna_registros],"%s",buffer_registros);
    }
}

int menu_debug_registers_print_registers(zxvision_window *w,int linea)
{
	//printf("linea: %d\n",linea);
	char textoregistros[80];

	char dumpmemoria[33];

	char dumpassembler[65];

	//size_t longitud_opcode;

	//menu_z80_moto_int copia_reg_pc;
	int i;

	menu_z80_moto_int menu_debug_memory_pointer_copia;

	//menu_debug_registers_adjust_ptr_on_follow();


	//Conservamos valor original y usamos uno de copia
	menu_debug_memory_pointer_copia=menu_debug_memory_pointer;

	char buffer_linea[MAX_ESCR_LINEA_OPCION_ZXVISION_LENGTH];	



	//Por defecto
	menu_debug_registers_print_registers_longitud_opcode=8; //Esto se hace para que en las vistas de solo hexadecimal, se mueva arriba/abajo de 8 en 8


		if (menu_debug_registers_current_view==7) {
			menu_debug_print_address_memory_zone(dumpassembler,menu_debug_memory_pointer_copia);

			int longitud_direccion=MAX_LENGTH_ADDRESS_MEMORY_ZONE;

			//metemos espacio en 0 final
			dumpassembler[longitud_direccion]=' ';


			//Assembler
			debugger_disassemble(&dumpassembler[longitud_direccion+1],17,&menu_debug_registers_print_registers_longitud_opcode,menu_debug_memory_pointer_copia);


			//debugger_disassemble(dumpassembler,32,&menu_debug_registers_print_registers_longitud_opcode,menu_debug_memory_pointer_copia );
                        menu_debug_memory_pointer_last=menu_debug_memory_pointer_copia+menu_debug_registers_print_registers_longitud_opcode;

                        //menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
			zxvision_print_string_defaults_fillspc(w,1,linea++,dumpassembler);

			sprintf (textoregistros,"TSTATES: %05d SCANL: %03dX%03d",t_estados,(t_estados % screen_testados_linea),t_scanline_draw);
			//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);
		}


		if (menu_debug_registers_current_view==2) {

			debugger_disassemble(dumpassembler,32,&menu_debug_registers_print_registers_longitud_opcode,menu_debug_memory_pointer_copia );
			menu_debug_memory_pointer_last=menu_debug_memory_pointer_copia+menu_debug_registers_print_registers_longitud_opcode;

			//menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
			zxvision_print_string_defaults_fillspc(w,1,linea++,dumpassembler);

			{
				//Z80
				menu_debug_registers_dump_hex(dumpmemoria,get_pc_register(),8);

				sprintf (textoregistros,"PC: %04X : %s",get_pc_register(),dumpmemoria);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);


				menu_debug_registers_dump_hex(dumpmemoria,reg_sp,8);
				sprintf (textoregistros,"SP: %04X : %s",reg_sp,dumpmemoria);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

				sprintf (textoregistros,"A: %02X F: %c%c%c%c%c%c%c%c",reg_a,DEBUG_STRING_FLAGS);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

				sprintf (textoregistros,"A':%02X F':%c%c%c%c%c%c%c%c",reg_a_shadow,DEBUG_STRING_FLAGS_SHADOW);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

				sprintf (textoregistros,"HL: %04X DE: %04X BC: %04X",HL,DE,BC);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

				sprintf (textoregistros,"HL':%04X DE':%04X BC':%04X",(reg_h_shadow<<8)|reg_l_shadow,(reg_d_shadow<<8)|reg_e_shadow,(reg_b_shadow<<8)|reg_c_shadow);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

				sprintf (textoregistros,"IX: %04X IY: %04X",reg_ix,reg_iy);
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

				char texto_nmi[10];
					texto_nmi[0]=0;

				sprintf (textoregistros,"R:%02X I:%02X IM%d IFF%c%c %s",
					(reg_r&127)|(reg_r_bit7&128),
					reg_i,
					im_mode,
					DEBUG_STRING_IFF12,
				
					texto_nmi);

				//01234567890123456789012345678901
				// R: 84 I: 1E DI IM1 NMI: Off
				// R: 84 I: 1E IFF1 IFF2 IM1 NMI: Off
				// R:84 I:1E IFF1 IFF2 IM1 NMI:Off

				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

			}


		}

		if (menu_debug_registers_current_view==4 || menu_debug_registers_current_view==3) {


			int longitud_op;
			

			int limite=menu_debug_get_main_list_view(w);

			for (i=0;i<limite;i++) {
				menu_debug_dissassemble_una_instruccion(dumpassembler,menu_debug_memory_pointer_copia,&longitud_op);
				//menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
				zxvision_print_string_defaults_fillspc(w,1,linea++,dumpassembler);
				menu_debug_memory_pointer_copia +=longitud_op;

				//Almacenar longitud del primer opcode mostrado
				if (i==0) menu_debug_registers_print_registers_longitud_opcode=longitud_op;
			}

			menu_debug_memory_pointer_last=menu_debug_memory_pointer_copia;


		}




        if (menu_debug_registers_current_view==1) {


				size_t longitud_op;
				int limite=get_menu_debug_num_lineas_full(w);


				//printf ("%d\n",w->visible_width);


				//A partir de que columna aparecen los registros a la derecha
				//dependera del tamaño de la ventana
				int columna_registros;


				columna_registros=w->visible_width-13;   //32-13


				//Revisar un minimo y maximo
				if (columna_registros<19) columna_registros=19;

				//20 caracteres dan mas que de sobra para el texto de registros
				if (columna_registros>MAX_ESCR_LINEA_OPCION_ZXVISION_LENGTH-20) columna_registros=MAX_ESCR_LINEA_OPCION_ZXVISION_LENGTH-20;



				//Mi valor ptr
				menu_z80_moto_int puntero_ptr_inicial=menu_debug_memory_pointer_copia;

				//Donde empieza la vista. Subir desde direccion actual, desensamblando "hacia atras" , tantas veces como posicion cursor actual
				menu_debug_memory_pointer_copia=menu_debug_disassemble_subir_veces(puntero_ptr_inicial,menu_debug_line_cursor);
         



				//Comportamiento de 1 caracter de margen a la izquierda en ventana 
				int antes_menu_escribe_linea_startx=menu_escribe_linea_startx;

				menu_escribe_linea_startx=0;
					
				//char buffer_linea[MAX_LINE_CPU_REGISTERS_LENGTH];
                for (i=0;i<limite;i++) {

					//Por si acaso
					//buffer_registros[0]=0;

					//Inicializamos linea a mostrar primero con espacios
					
					int j; 
					for (j=0;j<MAX_ESCR_LINEA_OPCION_ZXVISION_LENGTH;j++) buffer_linea[j]=32;

					int opcion_actual=-1;

					int opcion_activada=1;  

					//Si esta linea tiene el cursor
					if (i==menu_debug_line_cursor) {
						opcion_actual=linea;			
						menu_debug_memory_pointer_copia=puntero_ptr_inicial;
						//printf ("draw line is the current. pointer=%04XH\n",menu_debug_memory_pointer_copia);
					}

					menu_z80_moto_int puntero_dir=adjust_address_memory_size(menu_debug_memory_pointer_copia);

					int tiene_brk=0;
					int tiene_pc=0;

					//Si linea tiene breakpoint
					if (debug_return_brk_pc_dir_condition(puntero_dir)>=0) tiene_brk=1;

					//Si linea es donde esta el PC
					if (puntero_dir==get_pc_register() ) tiene_pc=1;

					if (tiene_pc) buffer_linea[0]='>';
					if (tiene_brk) {
						buffer_linea[0]='*';
						opcion_activada=0;
					}

					if (tiene_pc && tiene_brk) buffer_linea[0]='+'; //Cuando coinciden breakpoint y cursor

					

                    debugger_disassemble(dumpassembler,32,&longitud_op,menu_debug_memory_pointer_copia);

/*
//Si desensamblado en menu view registers muestra:
//0: lo normal. opcodes
//1: hexa
//2: ascii
//3: lo normal pero sin mostrar registros a la derecha
int menu_debug_registers_subview_type=0;

*/
//menu_debug_memory_pointer=adjust_address_memory_size(menu_debug_memory_pointer);


					//Si mostramos en vez de desensamblado, volcado hexa o ascii
					if (menu_debug_registers_subview_type==1)	menu_debug_registers_dump_hex(dumpassembler,puntero_dir,longitud_op);
					if (menu_debug_registers_subview_type==2)  menu_debug_registers_dump_ascii(dumpassembler,puntero_dir,longitud_op,0);
					//4 para direccion, fijo
					
					sprintf(&buffer_linea[1],"%04X %s",puntero_dir,dumpassembler);

					//Guardar las direcciones de cada linea
					//menu_debug_lines_addresses[i]=puntero_dir;


					//printf ("segundo menu_debug_registros_parte_derecha. i=%d columna=%d buffer_linea: [%s]\n",i,columna_registros,buffer_linea);
					menu_debug_registros_parte_derecha(i,buffer_linea,columna_registros,1);
					//printf ("despues\n");

					//printf ("buffer_linea: [%s]\n",buffer_linea);


					//zxvision_print_string_defaults_fillspc(w,1,linea,buffer_linea);

					//De los pocos usos de menu_escribe_linea_opcion_zxvision,
					//solo se usa en menus y aqui: para poder mostrar linea activada o en rojo

					menu_escribe_linea_opcion_zxvision(w,linea,opcion_actual,opcion_activada,buffer_linea);

					//menu_escribe_linea_opcion_zxvision(w,linea,opcion_actual,opcion_activada,"0123456789001234567890012345678900123456789001234567890");

					//printf ("despues menu_escribe_linea_opcion_zxvision. i=%d\n",i);

					linea++;


					menu_debug_memory_pointer_copia +=longitud_op;

					//Almacenar longitud del primer opcode mostrado
					if (i==0) menu_debug_registers_print_registers_longitud_opcode=longitud_op;
                }


				menu_debug_memory_pointer_last=menu_debug_memory_pointer_copia;


				//Vamos a ver si metemos una linea mas de la parte de la derecha extra, siempre que tenga contenido (primer caracter no espacio)
				//Esto sucede por ejemplo en tbblue, pues tiene 8 segmentos de memoria
				//Inicializamos a espacios
				int j;
				for (j=0;j<MAX_ESCR_LINEA_OPCION_ZXVISION_LENGTH;j++) buffer_linea[j]=32;


				//printf ("tercero menu_debug_registros_parte_derecha\n");
				menu_debug_registros_parte_derecha(i,buffer_linea,columna_registros,1);
				//printf ("despues\n");

				//primero borramos esa linea, por si cambiamos de subvista con M y hay "restos" ahi
				zxvision_print_string_defaults_fillspc(w,1,linea,"");

				//Si tiene contenido
				if (buffer_linea[columna_registros]!=' ' && buffer_linea[columna_registros]!=0) {
                                                //Agregamos linea perdiendo la linea en blanco de margen
						//menu_escribe_linea_opcion(linea,-1,1,buffer_linea);
						//zxvision_print_string_defaults_fillspc(w,1,linea,buffer_linea);

					//De los pocos usos de menu_escribe_linea_opcion_zxvision,
					//solo se usa en menus y dos veces en esta funcion
					//en este caso, es para poder procesar los caracteres "||"
					menu_escribe_linea_opcion_zxvision(w,linea,-1,1,buffer_linea);


				}

				linea++;

				menu_escribe_linea_startx=antes_menu_escribe_linea_startx;

			

				//Linea de stack
				//No mostrar stack en caso de scmp
					sprintf(buffer_linea,"(SP) ");

					int valores=5;
					debug_get_stack_values(valores,&buffer_linea[5]);
					//menu_escribe_linea_opcion(linea++,-1,1,buffer_linea);
					zxvision_print_string_defaults_fillspc(w,1,linea++,buffer_linea);
					//En caso de Z80 o SCMP meter linea vacia
					zxvision_print_string_defaults_fillspc(w,1,linea++,"");


        }

		if (menu_debug_registers_current_view==5 || menu_debug_registers_current_view==6) {

			//Hacer que texto ventana empiece pegado a la izquierda
			menu_escribe_linea_startx=0;

		
			int longitud_linea=8;
			

			int limite=menu_debug_get_main_list_view(w);

			for (i=0;i<limite;i++) {
					menu_debug_hexdump_with_ascii(dumpassembler,menu_debug_memory_pointer_copia,longitud_linea,0);
					//menu_debug_registers_dump_hex(dumpassembler,menu_debug_memory_pointer_copia,longitud_linea);
					//menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
					zxvision_print_string_defaults_fillspc(w,0,linea++,dumpassembler);
					menu_debug_memory_pointer_copia +=longitud_linea;
			}

			menu_debug_memory_pointer_last=menu_debug_memory_pointer_copia;


			//Restaurar comportamiento texto ventana
			menu_escribe_linea_startx=1;

		}

		//Aparecen otros registros y valores complementarios
		if (menu_debug_registers_current_view==2 || menu_debug_registers_current_view==3 || menu_debug_registers_current_view==5) {
            //Separador
        	sprintf (textoregistros," ");
            //menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);


			//
			// MEMPTR y T-Estados
			//
            sprintf (textoregistros,"MEMPTR: %04X TSTATES: %05d",memptr,t_estados);
            //menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);


			//
			// Mas T-Estados y parcial
			//

			char buffer_estadosparcial[32];
			/*int estadosparcial=debug_t_estados_parcial;
			

			if (estadosparcial>999999999) sprintf (buffer_estadosparcial,"%s","OVERFLOW");
			else sprintf (buffer_estadosparcial,"%09u",estadosparcial);*/

			debug_get_t_estados_parcial(buffer_estadosparcial);

            sprintf (textoregistros,"TSTATL: %03d TSTATP: %s",(t_estados % screen_testados_linea),buffer_estadosparcial );
            //menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

			//
			// FPS y Scanline
			//

	            sprintf (textoregistros,"SCANLINE: %03d FPS: %03d",t_scanline_draw,ultimo_fps);
            //menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);



			//
    	    // ULA
			//

			//no hacer autodeteccion de idle bus port, para que no se active por si solo
			z80_bit copia_autodetect_rainbow;
			copia_autodetect_rainbow.v=autodetect_rainbow.v;

			autodetect_rainbow.v=0;



			//
			//Puerto FE, Idle port y flash. cada uno para la maquina que lo soporte
			//Solo para Spectrum O Z88
			//
			{
				char feporttext[20];
				{
					sprintf (feporttext,"FE: %02X ",out_254_original_value);
				}

            	char flashtext[40];
            	{
	            	sprintf (flashtext,"FLASH: %d ",estado_parpadeo.v);
    	       	}


				char idleporttext[20];
					sprintf (idleporttext,"IDLEPORT: %02X",idle_bus_port(255) );

	            sprintf (textoregistros,"%s%s%s",feporttext,flashtext,idleporttext );

				autodetect_rainbow.v=copia_autodetect_rainbow.v;
    	        //menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);


			}


			//
			// Linea audio 
			//
                        sprintf (textoregistros,"AUDIO: BEEPER: %03d AY: %03d", value_beeper,da_output_ay() );
                        //menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);



				sprintf (textoregistros,"COPPER PC: %04XH CTRL: %02XH",tbblue_copper_pc,tbblue_copper_get_control_bits() );
				//menu_escribe_linea_opcion(linea++,-1,1,textoregistros);
				zxvision_print_string_defaults_fillspc(w,1,linea++,textoregistros);

			//
    		//Paginas memoria
			//
            char textopaginasmem[100];
			menu_debug_get_memory_pages(textopaginasmem);

			int max_longitud=31;
			//limitar a 31 por si acaso

    		//Si paging enabled o no, scr
    		char buffer_paging_state[32];
    		debug_get_paging_screen_state(buffer_paging_state);

    		//Si cabe, se escribe
    		int longitud_texto1=strlen(textopaginasmem);

    		//Lo escribo y ya lo limitará debajo a 31
			sprintf(&textopaginasmem[longitud_texto1]," %s",buffer_paging_state);


			textopaginasmem[max_longitud]=0;
    		//menu_escribe_linea_opcion(linea++,-1,1,textopaginasmem);
			zxvision_print_string_defaults_fillspc(w,1,linea++,textopaginasmem);


		}




	return linea;

}


int menu_debug_registers_get_height_ventana_vista(void)
{
	int alto_ventana;

        if (menu_debug_registers_current_view==7) {
                alto_ventana=5;
        }

        else if (menu_debug_registers_current_view==8) {
                alto_ventana=16;
        }		

        else {
                alto_ventana=24;
        }

	return alto_ventana;	
}

void menu_debug_registers_zxvision_ventana_set_height(zxvision_window *w)
{

	int alto_ventana=menu_debug_registers_get_height_ventana_vista();

    

	zxvision_set_visible_height(w,alto_ventana);
}

void menu_debug_registers_set_title(zxvision_window *w)
{
        char titulo[33];


        //menu_debug_registers_current_view

        //Por defecto
                                   //0123456789012345678901
        sprintf (titulo,"Debug CPU             V");

        if (menu_breakpoint_exception_pending_show.v==1 || menu_breakpoint_exception.v) {
                                           //0123456789012345678901
                sprintf (titulo,"Debug CPU (brk cond)  V");
                //printf ("breakpoint pending show\n");
        }
        else {
                                                                                        //0123456789012345678901
                if (cpu_step_mode.v) sprintf (titulo,"Debug CPU (step)      V");
                //printf ("no breakpoint pending show\n");
        }

        //Poner numero de vista siempre en posicion 23
        sprintf (&titulo[23],"%d",menu_debug_registers_current_view);

	strcpy(w->window_title,titulo);
}

void menu_debug_registers_ventana_common(zxvision_window *ventana)
{
	//Cambiar el alto visible segun la vista actual
	menu_debug_registers_zxvision_ventana_set_height(ventana);

	ventana->can_use_all_width=1; //Para poder usar la ultima columna de la derecha donde normalmente aparece linea scroll	
}

void menu_debug_registers_zxvision_ventana(zxvision_window *ventana)
{
/*
	

	*/

	int ancho_ventana;
	int alto_ventana;

	int xorigin,yorigin;


	if (!util_find_window_geometry("debugcpu",&xorigin,&yorigin,&ancho_ventana,&alto_ventana)) {
		xorigin=menu_origin_x();
		yorigin=0;
		ancho_ventana=32;
		alto_ventana=24;
	}


	//asignamos mismo ancho visible que ancho total para poder usar la ultima columna de la derecha, donde se suele poner scroll vertical
	zxvision_new_window_nocheck_staticsize(ventana,xorigin,yorigin,ancho_ventana,alto_ventana,ancho_ventana,alto_ventana-2,"Debug CPU");


	//Preservar ancho y alto anterior
	//menu_debug_registers_ventana_common(ventana);


	ventana->can_use_all_width=1; //Para poder usar la ultima columna de la derecha donde normalmente aparece linea scroll
	ventana->can_be_backgrounded=1;
	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"debugcpu");

	//Puede enviar hotkeys con raton
	ventana->can_mouse_send_hotkeys=1;


}



void menu_debug_registers_gestiona_breakpoint(void)
{
    menu_breakpoint_exception.v=0;
		menu_breakpoint_exception_pending_show.v=1;
    cpu_step_mode.v=1;

    //printf ("Reg pc: %d\n",reg_pc);
		continuous_step=0;

}


void menu_watches_overlay_mostrar_texto(void)
{
 int linea;

    linea=1; //Empezar justo en cada linea Result

    
  
				char buf_linea[32];

				//char string_detoken[MAX_BREAKPOINT_CONDITION_LENGTH];

				int i;

				for (i=0;i<DEBUG_MAX_WATCHES;i++) {
					
                        int error_code;

                        int resultado=exp_par_evaluate_token(debug_watches_array[i],MAX_PARSER_TOKENS_NUM,&error_code);
                        /* if (error_code) {
                                //printf ("%d\n",tokens[0].tipo);
                                menu_generic_message_format("Error","Error evaluating parsed string: %s\nResult: %d",
                                string_detoken,resultado);
                        }
                        else {
                                menu_generic_message_format("Result","Parsed string: %s\nResult: %d",
                                string_detoken,resultado);
                        }
						*/



	                sprintf (buf_linea,"  Result: %d",resultado); 
					zxvision_print_string_defaults_fillspc(menu_watches_overlay_window,1,linea,buf_linea);

					linea+=2;

								
				}

}



void menu_watches_overlay(void)
{

    if (!zxvision_drawing_in_background) normal_overlay_texto_menu();

 	menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech

 

		menu_watches_overlay_mostrar_texto();
		zxvision_draw_window_contents(menu_watches_overlay_window);

}



void menu_watches_edit(MENU_ITEM_PARAMETERS)
{
        int watch_index=valor_opcion;

  char string_texto[MAX_BREAKPOINT_CONDITION_LENGTH];

    exp_par_tokens_to_exp(debug_watches_array[watch_index],string_texto,MAX_PARSER_TOKENS_NUM);

  menu_ventana_scanf("Watch",string_texto,MAX_BREAKPOINT_CONDITION_LENGTH);

  debug_set_watch(watch_index,string_texto);

  menu_muestra_pending_error_message(); //Si se genera un error derivado del set watch, mostrarlo

}



//Esta funcion no se llama realmente desde una opcion de menu, y por tanto deberia ser con parametros (void),
//pero dado que está en el listado de zxvision_known_window_names_array, debe ser con este parámetro
void menu_watches(MENU_ITEM_PARAMETERS)
{


	
	//Watches normales

	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

    zxvision_window *ventana;
    ventana=&zxvision_window_watches;

    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
    zxvision_delete_window_if_exists(ventana);


    int xventana,yventana;
    int ancho_ventana,alto_ventana;	

	if (!util_find_window_geometry("watches",&xventana,&yventana,&ancho_ventana,&alto_ventana)) {

	 xventana=menu_origin_x();
	 yventana=1;

	 ancho_ventana=32;
	 alto_ventana=22;
	}



	zxvision_new_window(ventana,xventana,yventana,ancho_ventana,alto_ventana,ancho_ventana-1,alto_ventana-2,"Watches");
	ventana->can_be_backgrounded=1;	
	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"watches");

	zxvision_draw_window(ventana);		



    //Cambiamos funcion overlay de texto de menu
    set_menu_overlay_function(menu_watches_overlay);

	menu_watches_overlay_window=ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui	


	//Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
	//Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
	if (zxvision_currently_restoring_windows_on_start) {
			//printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
			return;
	}	

    menu_item *array_menu_watches_settings;
    menu_item item_seleccionado;
    int retorno_menu;						

    do {

		//Valido tanto para cuando multitarea es off y para que nada mas entrar aqui, se vea, sin tener que esperar el medio segundo 
		//que he definido en el overlay para que aparezca
		menu_watches_overlay_mostrar_texto();

        int lin=0;

		
		
		int i;

		char string_detoken[MAX_BREAKPOINT_CONDITION_LENGTH];

		menu_add_item_menu_inicial(&array_menu_watches_settings,"",MENU_OPCION_UNASSIGNED,NULL,NULL);
		char texto_expresion_shown[27];


		for (i=0;i<DEBUG_MAX_WATCHES;i++) {
			
			//Convertir token de watch a texto 
			if (debug_watches_array[i][0].tipo==TPT_FIN) {
				strcpy(string_detoken,"None");
			}
			else exp_par_tokens_to_exp(debug_watches_array[i],string_detoken,MAX_PARSER_TOKENS_NUM);

			//Limitar a 27 caracteres
			menu_tape_settings_trunc_name(string_detoken,texto_expresion_shown,27);

 			menu_add_item_menu_format(array_menu_watches_settings,MENU_OPCION_NORMAL,menu_watches_edit,NULL,"%2d: %s",i+1,texto_expresion_shown);

			//En que linea va
			menu_add_item_menu_tabulado(array_menu_watches_settings,1,lin);		

			//Indicamos el indice
			menu_add_item_menu_valor_opcion(array_menu_watches_settings,i);
		

			lin+=2;			
		}	
		
				

    retorno_menu=menu_dibuja_menu(&menu_watches_opcion_seleccionada,&item_seleccionado,array_menu_watches_settings,"Watches" );

	if (retorno_menu!=MENU_RETORNO_BACKGROUND) {

	//En caso de menus tabulados, es responsabilidad de este de borrar la ventana
        cls_menu_overlay();

				//Nombre de ventana solo aparece en el caso de stdout
                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");



									//restauramos modo normal de texto de menu para llamar al editor de watch
                                                                //con el sprite encima
                                    set_menu_overlay_function(normal_overlay_texto_menu);


                                      
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);

								set_menu_overlay_function(menu_watches_overlay);
								zxvision_clear_window_contents(ventana); //limpiar de texto anterior en linea de watch
								zxvision_draw_window(ventana);


				//En caso de menus tabulados, es responsabilidad de este de borrar la ventana
                                
                        }
                }
	}

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus && retorno_menu!=MENU_RETORNO_BACKGROUND);

	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	zxvision_set_window_overlay_from_current(ventana);

       //restauramos modo normal de texto de menu
       set_menu_overlay_function(normal_overlay_texto_menu);

	   cls_menu_overlay();

	util_add_window_geometry_compact(ventana);	   


	if (retorno_menu==MENU_RETORNO_BACKGROUND) {
        zxvision_message_put_window_background();
    }

    else {	

		//En caso de menus tabulados, es responsabilidad de este de liberar ventana
		zxvision_destroy_window(ventana);			   

	}


}


void menu_debug_registers_set_view(zxvision_window *ventana,int vista)
{

	zxvision_clear_window_contents(ventana);

	if (vista<1 || vista>8) vista=1;

	menu_debug_registers_current_view=vista;

	/*
	Dado que se cambia de vista, podemos estar en vista 7 , por ejemplo, que es pequeña, y el alto total es minimo,
	y si se cambiara a vista 1 por ejemplo, es una vista mayor pero el alto total no variaria y no se veria mas que las primeras 3 lineas
	Entonces, tenemos que destruir la ventana y volverla a crear
	 */

	

    cls_menu_overlay();

	

	int ventana_x=ventana->x;
	int ventana_y=ventana->y;
	int ventana_visible_width=ventana->visible_width;

	//El alto es el que calculamos segun la vista actual. x,y,ancho los dejamos tal cual estaban
	int ventana_visible_height=menu_debug_registers_get_height_ventana_vista();


	zxvision_destroy_window(ventana);

	//Cerrar la ventana y volverla a crear pero cambiando maximo alto

	//asignamos mismo ancho visible que ancho total para poder usar la ultima columna de la derecha, donde se suele poner scroll vertical
	zxvision_new_window(ventana,ventana_x,ventana_y,ventana_visible_width,ventana_visible_height,ventana_visible_width,ventana_visible_height-2,"Debug CPU");	

	menu_debug_registers_ventana_common(ventana);

}

void menu_debug_registers_splash_memory_zone(void)
{

	menu_debug_set_memory_zone_attr();

	char textofinal[200];
	char zone_name[MACHINE_MAX_MEMORY_ZONE_NAME_LENGHT+1];
	int zone=menu_get_current_memory_zone_name_number(zone_name);
	//machine_get_memory_zone_name(menu_debug_memory_zone,buffer_name);

	sprintf (textofinal,"Zone number: %d\nName: %s\nSize: %d (%d KB)", zone,zone_name,
		menu_debug_memory_zone_size,menu_debug_memory_zone_size/1024);

	menu_generic_message_splash("Memory Zone",textofinal);


}


//Actualmente nadie usa esta funcion. Para que queremos cambiar la zona (en un menu visible) y luego hacer splash?
//antes tenia sentido pues el cambio de zona de memoria no era con menu, simplemente saltaba a la siguiente
void menu_debug_change_memory_zone_splash(void)
{
	menu_debug_change_memory_zone();

	menu_debug_registers_splash_memory_zone();


}

void menu_debug_cpu_step_over(void)
{
  //Si apunta PC a instrucciones RET o JP, hacer un cpu-step
  if (si_cpu_step_over_jpret()) {
          debug_printf(VERBOSE_DEBUG,"Running only cpu-step as current opcode is JP or RET");
	  cpu_core_loop();
          return;
  }


  debug_cpu_step_over();


}


void menu_debug_cursor_up(void)
{


		if (menu_debug_line_cursor>0) {
			menu_debug_line_cursor--;
		}

                                        if (menu_debug_view_has_disassemly() ) { //Si vista con desensamblado
                                                menu_debug_memory_pointer=menu_debug_disassemble_subir(menu_debug_memory_pointer);
                                        }
                                        else {  //Vista solo hexa
                                                menu_debug_memory_pointer -=menu_debug_registers_print_registers_longitud_opcode;
                                        }
}


void menu_debug_cursor_down(zxvision_window *w)
{
		if (menu_debug_line_cursor<get_menu_debug_num_lineas_full(w)-1) {
			menu_debug_line_cursor++;
		}

                                        if (menu_debug_view_has_disassemly() ) { //Si vista con desensamblado
                                                menu_debug_memory_pointer=menu_debug_disassemble_bajar(menu_debug_memory_pointer);
                                        }
                                        else {  //Vista solo hexa
                                                menu_debug_memory_pointer +=menu_debug_registers_print_registers_longitud_opcode;
                                        }

}


void menu_debug_cursor_pgup(zxvision_window *w)
{

                                        int lineas=menu_debug_get_main_list_view(w);


                                        int i;
                                        for (i=0;i<lineas;i++) {
						menu_debug_cursor_up();
                                        }
}


void menu_debug_cursor_pgdn(zxvision_window *w)
{

                                        int lineas=menu_debug_get_main_list_view(w);


                                        int i;
                                        for (i=0;i<lineas;i++) {
                                                menu_debug_cursor_down(w);
                                        }

}


void menu_breakpoint_fired(char *s) 
{
/*
//Si mostrar aviso cuando se cumple un breakpoint
int debug_show_fired_breakpoints_type=0;
//0: siempre
//1: solo cuando condicion no es tipo "PC=XXXX"
//2: nunca
*/
	int mostrar=0;

	int es_pc_cond=debug_text_is_pc_condition(s);

	//printf ("es_pc_cond: %d\n",es_pc_cond);

	if (debug_show_fired_breakpoints_type==0) mostrar=1;
	if (debug_show_fired_breakpoints_type==1 && !es_pc_cond) mostrar=1;

	if (mostrar) {
		menu_generic_message_format("Breakpoint","Breakpoint fired: %s",catch_breakpoint_message);
	}

	//Forzar follow pc
	menu_debug_follow_pc.v=1;



}


void menu_debug_ret(void)
{
		reg_pc=pop_valor();
}

void menu_debug_toggle_breakpoint(void)
{
	//Buscar primero direccion que indica el cursor
	menu_z80_moto_int direccion_cursor;

	//direccion_cursor=menu_debug_lines_addresses[menu_debug_line_cursor];
	direccion_cursor=menu_debug_memory_pointer;

	debug_printf (VERBOSE_DEBUG,"Address on cursor: %X",direccion_cursor);

	//Si hay breakpoint ahi, quitarlo
	int posicion=debug_return_brk_pc_dir_condition(direccion_cursor);
	if (posicion>=0) {
		debug_printf (VERBOSE_DEBUG,"Clearing breakpoint at index %d",posicion);
		debug_clear_breakpoint(posicion);

	}

	//Si no, ponerlo
	else {

		char condicion[30];
		sprintf (condicion,"PC=%XH",direccion_cursor);

        if (debug_breakpoints_enabled.v==0) {
                debug_breakpoints_enabled.v=1;

                breakpoints_enable();
    	}
		debug_printf (VERBOSE_DEBUG,"Putting breakpoint [%s] at next free slot",condicion);

		debug_add_breakpoint_free(condicion,""); 
	}
}

void menu_debug_runto(void)
{
	//Buscar primero direccion que indica el cursor
	menu_z80_moto_int direccion_cursor;

	//direccion_cursor=menu_debug_lines_addresses[menu_debug_line_cursor];
	direccion_cursor=menu_debug_memory_pointer;

	debug_printf (VERBOSE_DEBUG,"Address on cursor: %X",direccion_cursor);

	//Si no hay breakpoint ahi, ponerlo
	int posicion=debug_return_brk_pc_dir_condition(direccion_cursor);
	if (posicion<0) {

		char condicion[30];
		sprintf (condicion,"PC=%XH",direccion_cursor);

        if (debug_breakpoints_enabled.v==0) {
                debug_breakpoints_enabled.v=1;

                breakpoints_enable();
    	}
		debug_printf (VERBOSE_DEBUG,"Putting breakpoint [%s] at next free slot",condicion);

		debug_add_breakpoint_free(condicion,""); 
	}

	//Y salir
}




void menu_debug_get_legend_short_long(char *destination_string,int ancho_visible,char *short_string,char *long_string)
{

	int longitud_largo=menu_calcular_ancho_string_item(long_string);

	//Texto mas largo cuando tenemos mas ancho

	//+2 por contar 1 espacio a la izquierda y otro a la derecha
	if (ancho_visible>=longitud_largo+2) strcpy(destination_string,long_string);


	else strcpy(destination_string,short_string);	
}


int menu_debug_registers_show_ptr_text(zxvision_window *w,int linea)
{

	debug_printf (VERBOSE_DEBUG,"Refreshing ptr");


	char buffer_mensaje[64];
	char buffer_mensaje_short[64];
	char buffer_mensaje_long[64];
                //Forzar a mostrar atajos
                z80_bit antes_menu_writing_inverse_color;
                antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
                menu_writing_inverse_color.v=1;


                                //Mostrar puntero direccion
                                menu_debug_memory_pointer=adjust_address_memory_size(menu_debug_memory_pointer);


				if (menu_debug_registers_current_view!=7 && menu_debug_registers_current_view!=8) {

                                char string_direccion[10];
                                menu_debug_print_address_memory_zone(string_direccion,menu_debug_memory_pointer);

				char maxima_vista='7';


								sprintf(buffer_mensaje_short,"P~~tr:%sH [%c] ~~FlwPC ~~1-~~%c:View",
                                        string_direccion,(menu_debug_follow_pc.v ? 'X' : ' '),maxima_vista );

								sprintf(buffer_mensaje_long,"Poin~~ter:%sH [%c] ~~FollowPC ~~1-~~%c:View",
                                        string_direccion,(menu_debug_follow_pc.v ? 'X' : ' '),maxima_vista );



								menu_debug_get_legend_short_long(buffer_mensaje,w->visible_width,buffer_mensaje_short,buffer_mensaje_long);


								//sprintf(buffer_mensaje,"P~~tr:%sH ~~FlwPC:%s ~~1-~~%c:View",
                                //        string_direccion,(menu_debug_follow_pc.v ? "Yes" : "No"),maxima_vista );

                                
				zxvision_print_string_defaults_fillspc(w,1,linea++,buffer_mensaje);

				}

	menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;

	return linea;
}

void menu_debug_switch_follow_pc(void)
{
	menu_debug_follow_pc.v ^=1;
	
	//if (follow_pc.v==0) menu_debug_memory_pointer=menu_debug_register_decrement_half(menu_debug_memory_pointer);
}


void menu_debug_get_legend(int linea,char *s,zxvision_window *w)
{

	int ancho_visible=w->visible_width;

	switch (linea) {

		//Primera linea
		case 0:

			//Modo step mode
			if (cpu_step_mode.v) {
				if (menu_debug_registers_current_view==1) {

					menu_debug_get_legend_short_long(s,ancho_visible,
							//01234567890123456789012345678901
							// StM DAsm En:Stp StOvr CntSt Md	
							"~~StM ~~D~~Asm ~~E~~n:Stp St~~Ovr ~~CntSt ~~Md",
								//          10        20        30        40        50        60
								//012345678901234567890123456789012345678901234567890123456789012
							//     StepMode DisAssemble Enter:Step StepOver ContinuosStep Mode

							"~~StepMode ~~Dis~~Assemble ~~E~~n~~t~~e~~r:Step Step~~Over ~~ContinousStep ~~Mode"
					
					); 

				
				}

				else {

					menu_debug_get_legend_short_long(s,ancho_visible,
							//01234567890123456789012345678901
							// StpM DAsm Ent:Stp Stovr ContSt
							"~~StpM ~~D~~Asm ~~E~~n~~t:Stp St~~Ovr ~~ContSt",
								//          10        20        30        40        50        60
								//012345678901234567890123456789012345678901234567890123456789012
							//     StepMode Disassemble Enter:Step StepOver ContinuosStep 

							"~~StepMode ~~Dis~~Assemble ~~E~~nter:Step Step~~Over ~~ContinousStep"
					
					);

				}
			}

			//Modo NO step mode
			else {
				if (menu_debug_registers_current_view==1) {

					menu_debug_get_legend_short_long(s,ancho_visible,

							//01234567890123456789012345678901
							// Stepmode Disassem Assem Mode
							"~~StepMode ~~Disassem ~~Assem ~~Mode",

							//012345678901234567890123456789012345678901234567890123456789012
							// StepMode Disassemble Assemble Mode
							"~~StepMode ~~Disassemble ~~Assemble ~~Mode"
					);



				}
				else {			
							//01234567890123456789012345678901
							// Stepmode Disassemble Assemble				
					sprintf(s,"~~StepMode ~~Disassemble ~~Assemble");
				}
			}
		break;


		//Segunda linea
		case 1:


			if (menu_debug_registers_current_view==1) {

				menu_debug_get_legend_short_long(s,ancho_visible,
							//01234567890123456789012345678901
							// Chr brk wtch Togl Run Runto Ret	
							  "Ch~~r ~~brk ~~wtch Tog~~l Ru~~n R~~unto R~~et",

							// Changeregisters breakpoints watch Toggle Run Runto Ret	
							//012345678901234567890123456789012345678901234567890123456789012
							  "Change~~registers ~~breakpoints ~~watches Togg~~le Ru~~n R~~unto R~~et"
				);
			}

			else {

				menu_debug_get_legend_short_long(s,ancho_visible,
							//01234567890123456789012345678901
							// changeReg Breakpoints Watches					
							  "Change~~reg ~~breakpoints ~~watches",

							// Changeregisters breakpoints watches
							//012345678901234567890123456789012345678901234567890123456789012
							  "Change~~registers ~~breakpoints ~~watches"

				);

			}
		break;


		//Tercera linea
		case 2:
                {
			char buffer_intermedio_short[128];
			char buffer_intermedio_long[128];

			if (cpu_step_mode.v) {

							//01234567890123456789012345678901
							// ClrTstPart Write VScr MemZn 99	
				sprintf (buffer_intermedio_short,"ClrTst~~Part Wr~~ite ~~VScr Mem~~Zm %d",menu_debug_memory_zone);
							//012345678901234567890123456789012345678901234567890123456789012
							// ClearTstatesPartial Write ViewScreen MemoryZone 99	
				sprintf (buffer_intermedio_long,"ClearTstates~~Partial Wr~~ite ~~ViewScreen Memory~~Zone %d",menu_debug_memory_zone);


				menu_debug_get_legend_short_long(s,ancho_visible,buffer_intermedio_short,buffer_intermedio_long);
			}
			else {
							//01234567890123456789012345678901
							// Clrtstpart Write MemZone 99
				sprintf (buffer_intermedio_short,"ClrTst~~Part Wr~~ite Mem~~Zone %d",menu_debug_memory_zone);

							//012345678901234567890123456789012345678901234567890123456789012
							// ClearTstatesPartial Write MemoryZone 99	
				sprintf (buffer_intermedio_long,"ClearTstates~~Partial Wr~~ite Memory~~Zone %d",menu_debug_memory_zone);

				menu_debug_get_legend_short_long(s,ancho_visible,buffer_intermedio_short,buffer_intermedio_long);

			}
                }
		break;
	}
}


void menu_debug_registers_next_cont_speed(void)
{
	menu_debug_continuous_speed++;
	if (menu_debug_continuous_speed==4) menu_debug_continuous_speed=0;
}

 

//Si borra el menu a cada pulsacion y muestra la pantalla de la maquina emulada debajo
void menu_debug_registers_if_cls(void)
{
                                
	//A cada pulsacion de tecla, mostramos la pantalla del ordenador emulado
	if (debug_settings_show_screen.v) {
		cls_menu_overlay();
		menu_refresca_pantalla();

		//Y forzar en este momento a mostrar pantalla
		//scr_refresca_pantalla_solo_driver();
		//printf ("refrescando pantalla\n");
	}


	if (cpu_step_mode.v==1 || menu_multitarea==0) {
		//printf ("Esperamos tecla NO cpu loop\n");
		menu_espera_no_tecla_no_cpu_loop();
	}
	else {
		//printf ("Esperamos tecla SI cpu loop\n");
		menu_espera_no_tecla();
	}


}


void menu_debug_cont_speed_progress(char *s)
{

	int max_position=19;
	//Meter caracteres con .
	int i;
	for (i=0;i<max_position;i++) s[i]='.';
	s[i]=0;

	//Meter tantas franjas > como velocidad
	i=menu_debug_continuous_speed_step;
	int caracteres=menu_debug_continuous_speed+1;

	while (caracteres>0) {
		s[i]='>';
		i++;
		if (i==max_position) i=0; //Si se sale por la derecha
		caracteres--;
	}

	menu_debug_continuous_speed_step++;
	if (menu_debug_continuous_speed_step==max_position) menu_debug_continuous_speed_step=0; //Si se sale por la derecha
}


void menu_debug_showscan_putpixel(z80_int *destino,int x,int y,int ancho,int color)
{

	screen_generic_putpixel_indexcolour(destino,x,y,ancho,color);	

}

void menu_debug_registers_show_scan_pos_putcursor(int x_inicial,int y)
{

	int ancho,alto;

	ancho=get_total_ancho_rainbow();
	alto=get_total_alto_rainbow();

    //rojo, amarillo, verde, cyan
    int colores_rainbow[]={2+8,6+8,4+8,5+8};

	int x;
    int indice_color=0;

	//printf ("inicial %d,%d\n",x_inicial,y);

	if (x_inicial<0 || y<0) return;

	//TBBlue tiene doble de alto. El ancho ya lo viene multiplicado por 2 al entrar aqui
	y *=2;		

	//Restauramos lo que habia en la posicion anterior del cursor
	if (menu_debug_registers_buffer_pre_x>=0 && menu_debug_registers_buffer_pre_y>=0) {
	        for (x=0;x<ANCHO_SCANLINE_CURSOR;x++) {
	            int x_final=menu_debug_registers_buffer_pre_x+x;


				if (x_final<ancho) {
					int color_antes=menu_debug_registers_buffer_precursor[x];
					menu_debug_showscan_putpixel(rainbow_buffer,x_final,menu_debug_registers_buffer_pre_y,ancho,color_antes);
				}
			}
	}



	menu_debug_registers_buffer_pre_x=x_inicial;
	menu_debug_registers_buffer_pre_y=y;


	if (x_inicial<0) return;

	for (x=0;x<ANCHO_SCANLINE_CURSOR;x++) {
		int x_final=x_inicial+x;


		//Guardamos lo que habia antes de poner el cursor
		if (x_final<ancho) {
			int color_anterior;

			//printf ("%d, %d\n",x_final,y);

			if (y>=0 && y<alto && x>=0 && x<ancho) {

				color_anterior=screen_generic_getpixel_indexcolour(rainbow_buffer,x_final,y,ancho);

				menu_debug_registers_buffer_precursor[x]=color_anterior;

				//Y ponemos pixel
			
	    		menu_debug_showscan_putpixel(rainbow_buffer,x_final,y,ancho,colores_rainbow[indice_color]);
			}
		}



		//Trozos de colores de 4 pixeles de ancho
		if (x>0 && (x%8)==0) {
			indice_color++;
			if (indice_color==4) indice_color=0;
		}


    }
}




void menu_debug_registers_show_scan_position(void)
{

	if (menu_debug_registers_if_showscan.v==0) return;

	if (rainbow_enabled.v) {
		//copiamos contenido linea y border a buffer rainbow
/*
//temp mostrar contenido buffer pixeles y atributos
printf ("pixeles y atributos:\n");
int i;
for (i=0;i<224*2/4;i++) printf ("%02X ",scanline_buffer[i]);
printf ("\n");
*/

			screen_store_scanline_rainbow_solo_border();
			screen_store_scanline_rainbow_solo_display();

		//Obtener posicion x e y e indicar posicion visualmente

		int si_salta_linea;
		int x,y;
		x=screen_get_x_coordinate_tstates(&si_salta_linea);

		y=screen_get_y_coordinate_tstates();

		//En caso de TBBLUE, doble de ancho

		x*=2;

		menu_debug_registers_show_scan_pos_putcursor(x,y+si_salta_linea);

				

	}
                                
}


int menu_debug_registers_print_legend(zxvision_window *w,int linea)
{



     if (menu_debug_registers_current_view!=7) {
		char buffer_mensaje[128];

				menu_debug_get_legend(0,buffer_mensaje,w);                                
				zxvision_print_string_defaults_fillspc(w,1,linea++,buffer_mensaje);

				menu_debug_get_legend(1,buffer_mensaje,w);                            
				zxvision_print_string_defaults_fillspc(w,1,linea++,buffer_mensaje);

				menu_debug_get_legend(2,buffer_mensaje,w);                                
				zxvision_print_string_defaults_fillspc(w,1,linea++,buffer_mensaje);

      }

	return linea;

}




int menu_debug_registers_get_line_legend(zxvision_window *w)
{

	if (menu_debug_registers_current_view!=8) return get_menu_debug_num_lineas_full(w)+5; //19;
	else return 11; //get_menu_debug_num_lineas_full(w)-3; //11;


}	


void menu_debug_registers_zxvision_save_size(zxvision_window *ventana,int *ventana_ancho_antes,int *ventana_alto_antes)
{

	//Guardar ancho y alto anterior para recrear la ventana si cambia
	*ventana_ancho_antes=ventana->visible_width;
	*ventana_alto_antes=ventana->visible_height;
}
	

void menu_debug_registers(MENU_ITEM_PARAMETERS)
{

	z80_byte acumulado;

	//ninguna tecla pulsada inicialmente
	acumulado=MENU_PUERTO_TECLADO_NINGUNA;

	int linea=0;

	z80_byte tecla;

	int valor_contador_segundo_anterior;

	valor_contador_segundo_anterior=contador_segundo;


	//Inicializar info de tamanyo zona
	menu_debug_set_memory_zone_attr();


	//Ver si hemos entrado desde un breakpoint
	if (menu_breakpoint_exception.v) menu_debug_registers_gestiona_breakpoint();

	else menu_espera_no_tecla();


	char buffer_mensaje[64];

	//Si no esta multitarea activa, modo por defecto es step to step
	if (menu_multitarea==0) cpu_step_mode.v=1;


	//zxvision_window ventana;
	zxvision_window *ventana;
	ventana=&zxvision_window_menu_debug_registers;

    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
    zxvision_delete_window_if_exists(ventana);


	menu_debug_registers_zxvision_ventana(ventana);


	//Guardar ancho y alto anterior para recrear la ventana si cambia
	int ventana_ancho_antes;
	int ventana_alto_antes;

	menu_debug_registers_zxvision_save_size(ventana,&ventana_ancho_antes,&ventana_alto_antes);

	menu_debug_registers_set_title(ventana);


        //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
        //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
        if (zxvision_currently_restoring_windows_on_start) {
                //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");

				//printf ("Overlay al finalizar desde inicio: %p\n",ventana->overlay_function);

                return;
        }	


	do {

		//Ver si ha cambiado tamanyo ventana para recrearla
		//Si no gestionamos esto, zxvision redimensiona por su cuenta el tamaño visible pero el tamaño total
		//requiere logicamente recrearla de nuevo
		if (ventana->visible_width!=ventana_ancho_antes || ventana->visible_height!=ventana_alto_antes) {
			debug_printf (VERBOSE_DEBUG,"Debug CPU window size has changed. Recreate it again");

			//Guardamos la geometria actual
			//util_add_window_geometry("debugcpu",ventana.x,ventana.y,ventana.visible_width,ventana.visible_height);
			util_add_window_geometry_compact(ventana);

			zxvision_destroy_window(ventana);

			//Al crearla, usara la geometria guardada antes
			menu_debug_registers_zxvision_ventana(ventana);
			menu_debug_registers_set_title(ventana);

			//Guardamos valores en variables para saber si cambia
			menu_debug_registers_zxvision_save_size(ventana,&ventana_ancho_antes,&ventana_alto_antes);
		}


		//Si es la vista 8, siempre esta en cpu step mode, y zona de memoria es la mapped
		if (menu_debug_registers_current_view==8) {
			cpu_step_mode.v=1;
			menu_debug_set_memory_zone_mapped();
		}

		//
		//Si no esta el modo step de la cpu
		//
		if (cpu_step_mode.v==0) {


			//Cuadrarlo cada 1/16 de segundo, justo lo mismo que el flash, asi
			//el valor de flash se ve coordinado
        	        //if ( (contador_segundo%(16*20)) == 0 || menu_multitarea==0) {
			if ( ((contador_segundo%(16*20)) == 0 && valor_contador_segundo_anterior!=contador_segundo ) || menu_multitarea==0) {
				//printf ("Refresco pantalla. contador_segundo=%d\n",contador_segundo);
				valor_contador_segundo_anterior=contador_segundo;


				menu_debug_registers_set_title(ventana);
				zxvision_draw_window(ventana);

				menu_debug_registers_adjust_ptr_on_follow();

                linea=0;
                linea=menu_debug_registers_show_ptr_text(ventana,linea);

                linea++;


                //Forzar a mostrar atajos
                z80_bit antes_menu_writing_inverse_color;
                antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
                menu_writing_inverse_color.v=1;

                        
				linea=menu_debug_registers_print_registers(ventana,linea);
				//linea=19;


				//En que linea aparece la leyenda
				linea=menu_debug_registers_get_line_legend(ventana);
				linea=menu_debug_registers_print_legend(ventana,linea);


				//Restaurar estado mostrar atajos
				menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;


				zxvision_draw_window_contents(ventana);

                if (menu_multitarea==0) menu_refresca_pantalla();


	        }

        	menu_cpu_core_loop();

			if (menu_breakpoint_exception.v) {
				//Si accion nula o menu o break
				if (debug_if_breakpoint_action_menu(catch_breakpoint_index)) {
				  menu_debug_registers_gestiona_breakpoint();
				  //Y redibujar ventana para reflejar breakpoint cond
				  //menu_debug_registers_ventana();
				}

				else {
					menu_breakpoint_exception.v=0;
					//Gestion acciones
					debug_run_action_breakpoint(debug_breakpoints_actions_array[catch_breakpoint_index]);
				}
			}



            acumulado=menu_da_todas_teclas();

	    	//si no hay multitarea, esperar tecla y salir
        	if (menu_multitarea==0) {
            	menu_espera_tecla();
               	acumulado=0;
	        }

			//Hay tecla pulsada
			if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) !=MENU_PUERTO_TECLADO_NINGUNA ) {
				//tecla=zxvision_common_getkey_refresh();
				tecla=zxvision_common_getkey_refresh_noesperanotec();

            	//Aqui suele llegar al mover raton-> se produce un evento pero no se pulsa tecla
                if (tecla==0) {
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

                else {
                    //printf ("tecla: %d\n",tecla);
                    //A cada pulsacion de tecla, mostramos la pantalla del ordenador emulado
                    menu_debug_registers_if_cls();
                    //menu_espera_no_tecla_no_cpu_loop();
                }




                if (tecla=='s') {
					cpu_step_mode.v=1;
					menu_debug_follow_pc.v=1; //se sigue pc
				}

				if (tecla=='z') {
					menu_debug_change_memory_zone();
				}


				if (tecla=='d') {
					menu_debug_disassemble_last_ptr=menu_debug_memory_pointer;
					menu_debug_disassemble(0);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}


				if (tecla=='a') {
					menu_debug_disassemble_last_ptr=menu_debug_memory_pointer;
					menu_debug_assemble(0);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}				

				if (tecla=='b') {
					menu_breakpoints(0);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}

				if (tecla=='m' && menu_debug_registers_current_view==1) {
                    menu_debug_next_dis_show_hexa();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

				if (tecla=='l' && menu_debug_registers_current_view==1) {
                    menu_debug_toggle_breakpoint();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}

				if (tecla=='u' && menu_debug_registers_current_view==1) {
					menu_debug_runto();
                    tecla=2; //Simular ESC
					salir_todos_menus=1;
                }			

				if (tecla=='n' && menu_debug_registers_current_view==1) {
					//run tal cual. como runto pero sin poner breakpoint
                    tecla=2; //Simular ESC
					salir_todos_menus=1;
                }										

				if (tecla=='w') {
					//La cerramos pues el envio de watches a background no funciona bien si hay otra ventana detras
					zxvision_destroy_window(ventana);
                    menu_watches(0);
					menu_debug_registers_zxvision_ventana(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

								
								
				if (tecla=='i') {
					last_debug_poke_dir=menu_debug_memory_pointer;
                    menu_debug_poke(0);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }								

                if (tecla=='p') {
						debug_t_estados_parcial=0;
                    	//Decimos que no hay tecla pulsada
                    	acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

				//Vista. Entre 1 y 6
				if (tecla>='1' && tecla<='8') {
					menu_debug_registers_set_view(ventana,tecla-'0');
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}

				if (tecla=='f') {
					menu_debug_switch_follow_pc();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}

				if (tecla=='t') {
					menu_debug_follow_pc.v=0; //se deja de seguir pc
					menu_debug_registers_change_ptr();
					//Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}

               	if (tecla=='r') {
					menu_debug_change_registers();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
            	}

				if (tecla==11) {
                    //arriba
					menu_debug_follow_pc.v=0; //se deja de seguir pc
					menu_debug_cursor_up();
					//Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

				if (tecla==10) {
                    //abajo
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
					menu_debug_cursor_down(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

				//24 pgup
                if (tecla==24) {
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
					menu_debug_cursor_pgup(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                }

				//25 pgwn
				if (tecla==25) {
					//PgDn
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
					menu_debug_cursor_pgdn(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
				}

				//Si tecla no es ESC o background, no salir
				if (tecla!=2 && tecla!=3) acumulado=MENU_PUERTO_TECLADO_NINGUNA;



			}

		}


		//
		//En modo Step mode
		//
		else {

			menu_debug_registers_set_title(ventana);
			zxvision_draw_window(ventana);

			menu_breakpoint_exception_pending_show.v=0;

			menu_debug_registers_adjust_ptr_on_follow();
	
   	        linea=0;
	        linea=menu_debug_registers_show_ptr_text(ventana,linea);

        	linea++;


			int si_ejecuta_una_instruccion=1;

            linea=menu_debug_registers_print_registers(ventana,linea);

			//linea=19;
			linea=menu_debug_registers_get_line_legend(ventana);

        	//Forzar a mostrar atajos
	        z80_bit antes_menu_writing_inverse_color;
	        antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
        	menu_writing_inverse_color.v=1;



			if (continuous_step==0) {
								//      01234567890123456789012345678901
				linea=menu_debug_registers_print_legend(ventana,linea);
																	// ~~1-~~5 View
			}
			else {
				//Mostrar progreso

				if (menu_debug_registers_current_view!=7) {
					char buffer_progreso[32];
					menu_debug_cont_speed_progress(buffer_progreso);
					sprintf (buffer_mensaje,"~~C: Speed %d %s",menu_debug_continuous_speed,buffer_progreso);
					zxvision_print_string_defaults_fillspc(ventana,1,linea++,buffer_mensaje);

					zxvision_print_string_defaults_fillspc(ventana,1,linea++,"Any other key: Stop cont step");
													  //0123456789012345678901234567890

					//si lento, avisar
					if (menu_debug_continuous_speed<=1) {
						zxvision_print_string_defaults_fillspc(ventana,1,linea++,"Note: Make long key presses");
					}
					else {
						zxvision_print_string_defaults_fillspc(ventana,1,linea++,"                         ");
					}

				}


				//Pausa
				//0= pausa de 0.5
				//1= pausa de 0.1
				//2= pausa de 0.02
				//3= sin pausa

				if (menu_debug_continuous_speed==0) usleep(500000); //0.5 segundo
				else if (menu_debug_continuous_speed==1) usleep(100000); //0.1 segundo
				else if (menu_debug_continuous_speed==2) usleep(20000); //0.02 segundo
			}


			//Restaurar estado mostrar atajos
			menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;

			//Actualizamos pantalla
			//zxvision_draw_window(&ventana);
			zxvision_draw_window_contents(ventana);
			menu_refresca_pantalla();


			//Esperamos tecla
			if (continuous_step==0)
			{ 
				menu_espera_tecla_no_cpu_loop();
					
				//No quiero que se llame a core loop si multitarea esta activo pero aqui estamos en cpu step
				int antes_menu_multitarea=menu_multitarea;
				menu_multitarea=0;
				//tecla=zxvision_common_getkey_refresh();
				tecla=zxvision_common_getkey_refresh_noesperanotec();
				menu_multitarea=antes_menu_multitarea;

				//Aqui suele llegar al mover raton-> se produce un evento pero no se pulsa tecla
				if (tecla==0) {
					acumulado=MENU_PUERTO_TECLADO_NINGUNA;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
					si_ejecuta_una_instruccion=0;
				}

				else {
					//printf ("tecla: %d\n",tecla);

					//A cada pulsacion de tecla, mostramos la pantalla del ordenador emulado
					menu_debug_registers_if_cls();
					//menu_espera_no_tecla_no_cpu_loop();
				}


				if (tecla=='c') {
					continuous_step=1;
				}

                if (tecla=='o') {
                    menu_debug_cpu_step_over();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }

				if (tecla=='d') {
					menu_debug_disassemble_last_ptr=menu_debug_memory_pointer;
					menu_debug_disassemble(0);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
				}


				if (tecla=='a') {
					menu_debug_disassemble_last_ptr=menu_debug_memory_pointer;
					menu_debug_assemble(0);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
				}		


				if (tecla=='z') {
					//Detener multitarea, porque si no, se input ejecutara opcodes de la cpu, al tener que leer el teclado
					int antes_menu_multitarea=menu_multitarea;
					menu_multitarea=0;

                    menu_debug_change_memory_zone();

                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;

					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
					si_ejecuta_una_instruccion=0;

					//Restaurar estado multitarea despues de menu_debug_registers_ventana, pues si hay algun error derivado
                    //de cambiar registros, se mostraria ventana de error, y se ejecutaria opcodes de la cpu, al tener que leer el teclado
					menu_multitarea=antes_menu_multitarea;

				}								

                if (tecla=='b') {
					//Detener multitarea, porque si no, se input ejecutara opcodes de la cpu, al tener que leer el teclado
					int antes_menu_multitarea=menu_multitarea;
					menu_multitarea=0;

                    menu_breakpoints(0);

                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;

					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
					si_ejecuta_una_instruccion=0;


                    //Restaurar estado multitarea despues de menu_debug_registers_ventana, pues si hay algun error derivado
                    //de cambiar registros, se mostraria ventana de error, y se ejecutaria opcodes de la cpu, al tener que leer el teclado
					menu_multitarea=antes_menu_multitarea;
					
                }

                if (tecla=='w') {
					//Detener multitarea, porque si no, se input ejecutara opcodes de la cpu, al tener que leer el teclado
					int antes_menu_multitarea=menu_multitarea;
					menu_multitarea=0;

					//La cerramos pues el envio de watches a background no funciona bien si hay otra ventana detras
					zxvision_destroy_window(ventana);
                    menu_watches(0);
					menu_debug_registers_zxvision_ventana(ventana);					

                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;

                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;

                    //Restaurar estado multitarea despues de menu_debug_registers_ventana, pues si hay algun error derivado
                    //de cambiar registros, se mostraria ventana de error, y se ejecutaria opcodes de la cpu, al tener que leer el teclado
					menu_multitarea=antes_menu_multitarea;
                }


                if (tecla=='i') {
                	//Detener multitarea, porque si no, se input ejecutara opcodes de la cpu, al tener que leer el teclado
					int antes_menu_multitarea=menu_multitarea;
					menu_multitarea=0;
				
					last_debug_poke_dir=menu_debug_memory_pointer;
                    menu_debug_poke(0);					

                	//Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;

                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;

                    //Restaurar estado multitarea despues de menu_debug_registers_ventana, pues si hay algun error derivado
                    //de cambiar registros, se mostraria ventana de error, y se ejecutaria opcodes de la cpu, al tener que leer el teclado
					menu_multitarea=antes_menu_multitarea;
                }


				if (tecla=='m' && menu_debug_registers_current_view==1) {
		            menu_debug_next_dis_show_hexa();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }



		        if (tecla=='l' && menu_debug_registers_current_view==1) {
                    menu_debug_toggle_breakpoint();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }

				//Ret
		        if (tecla=='e' && menu_debug_registers_current_view==1) {
                    menu_debug_ret();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }				
								
				if (tecla=='u' && menu_debug_registers_current_view==1) {
                    menu_debug_runto();
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
					salir_todos_menus=1;
					cpu_step_mode.v=0;
					acumulado=0; //teclas pulsadas
					//Con esto saldremos
                }	

				
				if (tecla=='n' && menu_debug_registers_current_view==1) {
					//run tal cual. como runto pero sin poner breakpoint
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
					salir_todos_menus=1;
					cpu_step_mode.v=0;
					acumulado=0; //teclas pulsadas
					//Con esto saldremos
                }					


                if (tecla=='p') {
						debug_t_estados_parcial=0;
                    	//Decimos que no hay tecla pulsada
                    	acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    	//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    	si_ejecuta_una_instruccion=0;
                }





				//Vista. Entre 1 y 8
				if (tecla>='1' && tecla<='8') {
                	menu_debug_registers_set_view(ventana,tecla-'0');
				    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
				}

				if (tecla=='f') {
					menu_debug_switch_follow_pc();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
				}

		        if (tecla=='t') {
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
					//Detener multitarea, porque si no, se input ejecutara opcodes de la cpu, al tener que leer el teclado
					int antes_menu_multitarea=menu_multitarea;
					menu_multitarea=0;
                    menu_debug_registers_change_ptr();

                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                                        
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;

                    //Restaurar estado multitarea despues de menu_debug_registers_ventana, pues si hay algun error derivado
                    //de cambiar registros, se mostraria ventana de error, y se ejecutaria opcodes de la cpu, al tener que leer el teclado
					menu_multitarea=antes_menu_multitarea;
                }



                if (tecla=='r') {
                	//Detener multitarea, porque si no, se input ejecutara opcodes de la cpu, al tener que leer el teclado
					int antes_menu_multitarea=menu_multitarea;
					menu_multitarea=0;

                    menu_debug_change_registers();

                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;

					//Restaurar estado multitarea despues de menu_debug_registers_ventana, pues si hay algun error derivado
					//de cambiar registros, se mostraria ventana de error, y se ejecutaria opcodes de la cpu, al tener que leer el teclado
					menu_multitarea=antes_menu_multitarea;
                }


			    if (tecla==11) {
                	//arriba
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
                    menu_debug_cursor_up();
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }

                if (tecla==10) {
                	//abajo
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
                    menu_debug_cursor_down(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                	si_ejecuta_una_instruccion=0;
                }

                //24 pgup
                if (tecla==24) {
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
                    menu_debug_cursor_pgup(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }
                
				//25 pgdn
                if (tecla==25) {
                    //PgDn
                    menu_debug_follow_pc.v=0; //se deja de seguir pc
                    menu_debug_cursor_pgdn(ventana);
                    //Decimos que no hay tecla pulsada
                    acumulado=MENU_PUERTO_TECLADO_NINGUNA;
                    //decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                }


				if (tecla=='v') {
					menu_espera_no_tecla_no_cpu_loop();
				    //para que no se vea oscuro
				    menu_set_menu_abierto(0);
					menu_cls_refresh_emulated_screen();
				    menu_espera_tecla_no_cpu_loop();
					menu_espera_no_tecla_no_cpu_loop();

					//vuelta a oscuro
				    menu_set_menu_abierto(1);

					menu_cls_refresh_emulated_screen();

					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
					si_ejecuta_una_instruccion=0;

					//Y redibujar ventana
					zxvision_draw_window(ventana);
				}

				if (tecla=='s') { 
					cpu_step_mode.v=0;
					//Decimos que no hay tecla pulsada
					acumulado=MENU_PUERTO_TECLADO_NINGUNA;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
					si_ejecuta_una_instruccion=0;
				}

				if (tecla==2) { //ESC
					cpu_step_mode.v=0;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                    acumulado=0; //teclas pulsadas
                    //Con esto saldremos

				}

				if (tecla==3) { //background
					cpu_step_mode.v=0;
					//decirle que despues de pulsar esta tecla no tiene que ejecutar siguiente instruccion
                    si_ejecuta_una_instruccion=0;
                    acumulado=0; //teclas pulsadas
                    //Con esto saldremos

				}				

				//Cualquier tecla no enter, no ejecuta instruccion
				if (tecla!=13) si_ejecuta_una_instruccion=0;

			}

			else {
				//Cualquier tecla Detiene el continuous loop excepto C
				//printf ("continuos loop\n");
				acumulado=menu_da_todas_teclas();
				if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) != MENU_PUERTO_TECLADO_NINGUNA) {

					//tecla=menu_get_pressed_key();
					tecla=zxvision_common_getkey_refresh();

					if (tecla=='c') {
						menu_debug_registers_next_cont_speed();
						tecla=0;
						menu_espera_no_tecla_no_cpu_loop();
					}

					//Si tecla no es 0->0 se suele producir al mover el raton.
					if (tecla!=0) {
						continuous_step=0;
						//printf ("cont step: %d\n",continuous_step);

            			//Decimos que no hay tecla pulsada
            			acumulado=MENU_PUERTO_TECLADO_NINGUNA;

						menu_espera_no_tecla_no_cpu_loop();
					}

				}

			}


			//1 instruccion cpu
			if (si_ejecuta_una_instruccion) {
				//printf ("ejecutando instruccion en step-to-step o continuous\n");
				debug_core_lanzado_inter.v=0;

				screen_force_refresh=1; //Para que no haga frameskip y almacene los pixeles/atributos en buffer rainbow


				cpu_core_loop();

				//Ver si se ha disparado interrupcion (nmi o maskable)
				//if (debug_core_lanzado_inter.v && debug_core_evitamos_inter.v) {
				if (debug_core_lanzado_inter.v && (remote_debug_settings&32)) {
					debug_run_until_return_interrupt();
				}


				menu_debug_registers_show_scan_position();
			}

			if (menu_breakpoint_exception.v) {
				//Si accion nula o menu o break
				if (debug_if_breakpoint_action_menu(catch_breakpoint_index)) {
					menu_debug_registers_gestiona_breakpoint();
				  	//Y redibujar ventana para reflejar breakpoint cond
					//menu_debug_registers_ventana();
				}

				else {
					menu_breakpoint_exception.v=0;
					//Gestion acciones
					debug_run_action_breakpoint(debug_breakpoints_actions_array[catch_breakpoint_index]);
				}
			}

		}

	//Hacer mientras step mode este activo o no haya tecla pulsada
	//printf ("acumulado %d cpu_ste_mode: %d\n",acumulado,cpu_step_mode.v);
    } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA || cpu_step_mode.v==1);
	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	//NO: dado que no tenemos overlay en esta ventana
	//zxvision_set_window_overlay_from_current(ventana);


    cls_menu_overlay();

	//util_add_window_geometry("debugcpu",ventana.x,ventana.y,ventana.visible_width,ventana.visible_height);
	util_add_window_geometry_compact(ventana);



	if (tecla==3) {
		//En este caso, dado que no hay overlay, borramos contenido de la ventana
		//para que el usuario no piense que se esta actualizando continuamente
		zxvision_cls(ventana);

		zxvision_message_put_window_background();
	}

	else {
		zxvision_destroy_window(ventana);		
 	}	


	//zxvision_destroy_window(ventana);

}


void menu_debug_dma_tsconf_zxuno_overlay(void)
{

    normal_overlay_texto_menu();

    int linea=0;
   

    
    	//mostrarlos siempre a cada refresco
    menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech

	char texto_dma[40];

	if (datagear_dma_emulation.v) {
		//NOTA: Si se activa datagear, no se vera si hay dma de tsconf o zxuno
		z80_int dma_port_a=value_8_to_16(datagear_port_a_start_addr_high,datagear_port_a_start_addr_low);
		z80_int dma_port_b=value_8_to_16(datagear_port_b_start_addr_high,datagear_port_b_start_addr_low);

		z80_int dma_len=value_8_to_16(datagear_block_length_high,datagear_block_length_low);	

		sprintf (texto_dma,"Port A:      %04XH",dma_port_a);
		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);

		sprintf (texto_dma,"Port B:      %04XH",dma_port_b);
		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);

		sprintf (texto_dma,"Length:      %5d",dma_len);
		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);

		if (datagear_wr0 & 4) sprintf (texto_dma,"Port A->B");
		else sprintf (texto_dma,"Port B->A");

		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);



		char access_type[20];

        if (datagear_wr1 & 8) sprintf (access_type,"I/O"); 
		else sprintf (access_type,"Memory");

		if ( (datagear_wr1 & 32) == 0 ) {
            if (datagear_wr1 & 16) sprintf (texto_dma,"Port A++. %s",access_type);
            else sprintf (texto_dma,"Port A--. %s",access_type);
        }
		else sprintf (texto_dma,"Port A fixed. %s",access_type);
		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);


        if (datagear_wr2 & 8) sprintf (access_type,"I/O"); 
		else sprintf (access_type,"Memory");

		if ( (datagear_wr2 & 32) == 0 ) {
            if (datagear_wr2 & 16) sprintf (texto_dma,"Port B++. %s",access_type);
            else sprintf (texto_dma,"Port B--. %s",access_type);
        }
		else sprintf (texto_dma,"Port B fixed. %s",access_type);
		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);	
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);

		//WR4. Bits D6 D5:
		//#       0   0 = Byte mode -> Do not use (Behaves like Continuous mode, Byte mode on Z80 DMA)
		//#       0   1 = Continuous mode
		//#       1   0 = Burst mode
		//#       1   1 = Do not use

		z80_byte modo_transferencia=(datagear_wr4>>5)&3;
		if (modo_transferencia==0) 		sprintf (texto_dma,"Mode: Byte mode");
		else if (modo_transferencia==1) sprintf (texto_dma,"Mode: Continuous");
		else if (modo_transferencia==2) sprintf (texto_dma,"Mode: Burst");
		else 							sprintf (texto_dma,"Mode: Do not use");

		//menu_escribe_linea_opcion(linea++,-1,1,texto_dma);	
		zxvision_print_string_defaults_fillspc(menu_debug_dma_tsconf_zxuno_overlay_window,1,linea++,texto_dma);



	}

	zxvision_draw_window_contents(menu_debug_dma_tsconf_zxuno_overlay_window);
}


void menu_debug_dma_tsconf_zxuno_disable(MENU_ITEM_PARAMETERS)
{
	if (datagear_dma_emulation.v) datagear_dma_is_disabled.v ^=1;
}


void menu_debug_dma_tsconf_zxuno(MENU_ITEM_PARAMETERS)
{
 	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

	char texto_ventana[33];
	//por defecto por si acaso
	strcpy(texto_ventana,"DMA");
	int alto_ventana=11;


	if (datagear_dma_emulation.v) strcpy(texto_ventana,"Datagear DMA");	


	//menu_dibuja_ventana(2,6,27,alto,texto_ventana);
	zxvision_window ventana;

	//int posicionx=menu_origin_x()+2;
	int ancho_ventana=27;
	int posicionx=menu_center_x()-ancho_ventana/2;

	int posiciony=menu_center_y()-alto_ventana/2;

	zxvision_new_window(&ventana,posicionx,posiciony,ancho_ventana,alto_ventana,
							ancho_ventana-1,alto_ventana-2,texto_ventana);
	zxvision_draw_window(&ventana);			



    //Cambiamos funcion overlay de texto de menu
	set_menu_overlay_function(menu_debug_dma_tsconf_zxuno_overlay);

	menu_debug_dma_tsconf_zxuno_overlay_window=&ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui			



	menu_item *array_menu_debug_dma_tsconf_zxuno;
        menu_item item_seleccionado;
        int retorno_menu;
        do {

        			
            //Hay que redibujar la ventana desde este bucle
            //menu_debug_dma_tsconf_zxuno_dibuja_ventana();

	

			int lin=8;
			int condicion_dma_disabled=1;

			if (datagear_dma_emulation.v) condicion_dma_disabled=datagear_dma_is_disabled.v;
		
				menu_add_item_menu_inicial_format(&array_menu_debug_dma_tsconf_zxuno,MENU_OPCION_NORMAL,menu_debug_dma_tsconf_zxuno_disable,NULL,"~~DMA: %s",
					(condicion_dma_disabled ? "Disabled" : "Enabled ") );  //Enabled acaba con espacio para borrar rastro de texto "Disabled"
				menu_add_item_menu_shortcut(array_menu_debug_dma_tsconf_zxuno,'d');
				menu_add_item_menu_ayuda(array_menu_debug_dma_tsconf_zxuno,"Disable DMA");
				menu_add_item_menu_tabulado(array_menu_debug_dma_tsconf_zxuno,1,lin);




		//Nombre de ventana solo aparece en el caso de stdout
                retorno_menu=menu_dibuja_menu(&debug_tsconf_dma_opcion_seleccionada,&item_seleccionado,array_menu_debug_dma_tsconf_zxuno,"TSConf DMA" );


	//En caso de menus tabulados, es responsabilidad de este de borrar la ventana
	cls_menu_overlay();
                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
				//En caso de menus tabulados, es responsabilidad de este de borrar la ventana
                                
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);



       //restauramos modo normal de texto de menu
       set_menu_overlay_function(normal_overlay_texto_menu);


        cls_menu_overlay();

	//En caso de menus tabulados, es responsabilidad de este de liberar ventana
	zxvision_destroy_window(&ventana);				

}


void menu_debug_ioports(MENU_ITEM_PARAMETERS)
{

	char stats_buffer[MAX_TEXTO_GENERIC_MESSAGE];


	debug_get_ioports(stats_buffer);

    char titulo_ventana[33];

    strcpy(titulo_ventana,"IO Ports");

  menu_generic_message(titulo_ventana,stats_buffer);

}


void menu_debug_unnamed_console_show_legend(zxvision_window *ventana)
{
        //Forzar a mostrar atajos
        z80_bit antes_menu_writing_inverse_color;
        antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
        menu_writing_inverse_color.v=1;		

        //Y linea leyenda
        char buffer_leyenda[32];
        sprintf(buffer_leyenda,"[%d] Verbose ~~level",verbose_level);
        zxvision_print_string_defaults_fillspc(ventana,1,0,buffer_leyenda);

        //Restaurar comportamiento atajos
        menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;     
}

void menu_debug_unnamed_console_overlay(void)
{

    if (!zxvision_drawing_in_background) normal_overlay_texto_menu();

    zxvision_window *ventana;

    ventana=menu_debug_unnamed_console_overlay_window;   


    //Revisar aqui tambien si no esta inicializado el puntero,
    //por si esta la ventana en background y a alguien le da por desactivar el debug console
    if (debug_unnamed_console_memory_pointer==NULL || debug_unnamed_console_enabled.v==0) {
        zxvision_print_string_defaults_fillspc(ventana,1,0,"Debug console is not enabled. Enable it on Settings->Debug");
        zxvision_draw_window_contents(ventana);
        return;
    }   


    int refrescar_borrado_contador=0;

    //Ver si hay que borrar contador actividad
    if (menu_debug_unnamed_console_indicador_actividad_visible>0) {
        menu_debug_unnamed_console_indicador_actividad_visible--;
        if (menu_debug_unnamed_console_indicador_actividad_visible==0) {
            //Borrar contador actividad
            //printf("---------BORRAR contador actividad\n");
            zxvision_print_string_defaults_fillspc(ventana,1,1,"");

            refrescar_borrado_contador=1;
        }
    }

    if (!debug_unnamed_console_modified) {
        //Si no hay mensajes, ver si hay que refrescar porque se ha borrado el contador de actividad
        if (refrescar_borrado_contador) {
            //printf("---Refrescar antes de salir sin escribir\n");
            zxvision_draw_window_contents(ventana);
        }
        return;
    }


    int x,y;
    char *puntero;

    puntero=debug_unnamed_console_memory_pointer;

    //DEBUG_UNNAMED_CONSOLE_WIDTH*DEBUG_UNNAMED_CONSOLE_HEIGHT
    for (y=0;y<DEBUG_UNNAMED_CONSOLE_HEIGHT;y++) {
        for (x=0;x<DEBUG_UNNAMED_CONSOLE_WIDTH;x++) {
            //printf("%c",*puntero);

            //Empieza en x+1 para dejar 1 caracter margen izquierda
            zxvision_print_char_defaults(ventana,x+1,y+2,*puntero);
            puntero++;
        }
        //printf("\n");
    }

    //Mostrar indicador actividad. Para que diga que hay mensajes nuevos
    //mantener durante 50 frames (1 segundo visible) despues de ultimo mensaje
    menu_debug_unnamed_console_indicador_actividad_visible=50;
    char *mensaje="|/-\\";

    int max=strlen(mensaje);
    char mensaje_dest[32];

    int pos=menu_debug_unnamed_console_indicador_actividad_contador % max;
    menu_debug_unnamed_console_indicador_actividad_contador++;

    sprintf(mensaje_dest,"[New messages %c]",mensaje[pos]);

    zxvision_print_string_defaults_fillspc(ventana,1,1,mensaje_dest);

    //Mostar la leyenda tambien aqui, para cuando refresca en segundo plano,
    //porque a veces se redibujan las ventanas pero solo se llama al overlay, y no a la funcion principal 
    menu_debug_unnamed_console_show_legend(ventana);

    zxvision_draw_window_contents(ventana);

    //Decir que no se ha modificado 
    debug_unnamed_console_modified=0;
}


void menu_debug_unnamed_console(MENU_ITEM_PARAMETERS)
{
    zxvision_window *ventana;
    ventana=&zxvision_window_unnamed_console;    

    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
    zxvision_delete_window_if_exists(ventana);    

    int x,y,ancho,alto;

    if (!util_find_window_geometry("debugconsole",&x,&y,&ancho,&alto)) {
        x=menu_origin_x();
        y=0;
        ancho=32;
        alto=18;
    }    

    //DEBUG_UNNAMED_CONSOLE_HEIGHT+2 porque hay dos lineas de leyenda superior
    //DEBUG_UNNAMED_CONSOLE_WIDTH+1 porque damos 1 espacio con margen por la izquierda
    zxvision_new_window(ventana,x,y,ancho,alto,DEBUG_UNNAMED_CONSOLE_WIDTH+1,DEBUG_UNNAMED_CONSOLE_HEIGHT+2,"Debug console");
  
    //Ajustar el scroll al maximo, para entrar y mostrar las ultimas lineas

    //Con esto llegara mas alla del limite
    //dado que debug_unnamed_console_current_y, si esta al maximo, es mas de lo que se puede bajar

    int linea_scroll=debug_unnamed_console_current_y;

    //-4 para asegurarnos que siempre vaya por debajo
    linea_scroll -=(alto-4);
    if (linea_scroll<0) linea_scroll=0;
    zxvision_set_offset_y_or_maximum(ventana,linea_scroll);

    

    ventana->can_be_backgrounded=1;
    ventana->upper_margin=2;
    //Permitir hotkeys desde raton
    ventana->can_mouse_send_hotkeys=1;	

    //indicar nombre del grabado de geometria
    strcpy(ventana->geometry_name,"debugconsole");    

    zxvision_draw_window(ventana);

    menu_debug_unnamed_console_overlay_window=ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui

                                                
    //Cambiamos funcion overlay de texto de menu
    //Se establece a la de funcion de onda + texto
    set_menu_overlay_function(menu_debug_unnamed_console_overlay);   

    //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
    //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
    if (zxvision_currently_restoring_windows_on_start) {
        //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
        return;
    }     

    z80_byte tecla;
    do {

        
        menu_debug_unnamed_console_show_legend(ventana);


        tecla=zxvision_common_getkey_refresh();
        zxvision_handle_cursors_pgupdn(ventana,tecla);

        if (tecla=='l') {
            menu_debug_verbose(0);
            debug_unnamed_console_modified=1;
        }

        //printf ("tecla: %d\n",tecla);
    } while (tecla!=2 && tecla!=3);

	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	//(siempre que esta funcion tenga overlay realmente)
	zxvision_set_window_overlay_from_current(ventana);    

    //restauramos modo normal de texto de menu
     set_menu_overlay_function(normal_overlay_texto_menu);

    
    cls_menu_overlay();

    //Grabar geometria ventana
    util_add_window_geometry_compact(ventana);    

	if (tecla==3) {
		//zxvision_ay_registers_overlay
		zxvision_message_put_window_background();
	}

	else {
		zxvision_destroy_window(ventana);		
 	}
}


void menu_debug_hexdump_with_ascii(char *dumpmemoria,menu_z80_moto_int dir_leida,int bytes_por_linea,z80_byte valor_xor)
{
	//dir_leida=adjust_address_space_cpu(dir_leida);

	menu_debug_set_memory_zone_attr();


	int longitud_direccion=MAX_LENGTH_ADDRESS_MEMORY_ZONE;

	menu_debug_print_address_memory_zone(dumpmemoria,dir_leida);

	

	//cambiamos el 0 final por un espacio
	dumpmemoria[longitud_direccion]=' ';

	menu_debug_registers_dump_hex(&dumpmemoria[longitud_direccion+1],dir_leida,bytes_por_linea);

	//01234567890123456789012345678901
	//000FFF ABCDABCDABCDABCD 12345678

	//metemos espacio
	int offset=longitud_direccion+1+bytes_por_linea*2;

	dumpmemoria[offset]=' ';
	//dumpmemoria[offset]='X';

	//Tener en cuenta el valor xor

	menu_debug_registers_dump_ascii(&dumpmemoria[offset+1],dir_leida,bytes_por_linea,valor_xor);

	//printf ("%s\n",dumpmemoria);
}


void menu_debug_hexdump_print_editcursor(zxvision_window *ventana,int x,int y,char caracter)
{


	//Inverso
	int papel=ESTILO_GUI_PAPEL_SELECCIONADO;
    int tinta=ESTILO_GUI_TINTA_SELECCIONADO;	

	//Si multitarea esta off, no se vera el parpadeo. Entonces cambiar el caracter por cursor '_'
	if (!menu_multitarea) caracter='_';

	//putchar_menu_overlay_parpadeo(x,y,caracter,tinta,papel,1);
	zxvision_print_char_simple(ventana,x,y,tinta,papel,1,caracter);

}

void menu_debug_hexdump_print_editcursor_nibble(zxvision_window *ventana,int x,int y,char caracter)
{


	//Inverso
	int papel=ESTILO_GUI_PAPEL_SELECCIONADO;
    int tinta=ESTILO_GUI_TINTA_SELECCIONADO;	

	//putchar_menu_overlay_parpadeo(x,y,caracter,tinta,papel,0);
	zxvision_print_char_simple(ventana,x,y,tinta,papel,0,caracter);

}

void menu_debug_hexdump_edit_cursor_izquierda(void)
{
	if (menu_hexdump_edit_position_x>0) {
		menu_hexdump_edit_position_x--;

		//Si en medio del espacio entre hexa y ascii
		if (menu_hexdump_edit_position_x==menu_hexdump_bytes_por_linea*2) menu_hexdump_edit_position_x--;
	}

	else {
		//Aparecer por la derecha
		menu_debug_hexdump_cursor_arriba();
		menu_hexdump_edit_position_x=menu_hexdump_bytes_por_linea*3;
	}

}

//escribiendo_memoria cursor indica que si estamos a la derecha de zona de edicion escribiendo,
//tiene que saltar a la zona izquierda de la zona ascii o hexa, al llegar a la derecha de dicha zona
void menu_debug_hexdump_edit_cursor_derecha(int escribiendo_memoria)
{

	//Hexdump. bytes_por_linea*2 espacio bytes_por_linea

	int ancho_linea=menu_hexdump_bytes_por_linea*3+1;

	if (menu_hexdump_edit_position_x<ancho_linea-1) {
		menu_hexdump_edit_position_x++;

		if (menu_hexdump_edit_position_x==menu_hexdump_bytes_por_linea*2) { //Fin zona derecha hexa
			if (escribiendo_memoria) {
				//Ponernos al inicio zona hexa de nuevo saltando siguiente linea
				menu_hexdump_edit_position_x=0;
				menu_debug_hexdump_cursor_abajo();
			}
			else {
				//Saltar a zona ascii
				menu_hexdump_edit_position_x++;
			}
		}
	}

	else {
		//Fin zona derecha ascii. 
		menu_debug_hexdump_cursor_abajo();

		if (escribiendo_memoria) {
			//Ponernos en el principio zona ascii
			menu_hexdump_edit_position_x=menu_hexdump_bytes_por_linea*2+1;
		}
		else {
			menu_hexdump_edit_position_x=0;
		}
	}

}

void menu_debug_hexdump_cursor_arriba(void)
{
	int alterar_ptr=0;
						//arriba
						if (menu_hexdump_edit_mode) {
							if (menu_hexdump_edit_position_y>0) menu_hexdump_edit_position_y--;
							else alterar_ptr=1;
						}

						else {
							alterar_ptr=1;
						}

						if (alterar_ptr) {
							menu_debug_hexdump_direccion -=menu_hexdump_bytes_por_linea;
							menu_debug_hexdump_direccion=menu_debug_hexdump_adjusta_en_negativo(menu_debug_hexdump_direccion,menu_hexdump_bytes_por_linea);
						}
}

void menu_debug_hexdump_cursor_abajo(void)
{
	int alterar_ptr=0;
						//abajo
						if (menu_hexdump_edit_mode) {
							if (menu_hexdump_edit_position_y<menu_hexdump_lineas_total-1) menu_hexdump_edit_position_y++;
							else alterar_ptr=1;
						}						
						else {
							alterar_ptr=1;
						}

						if (alterar_ptr) {
							menu_debug_hexdump_direccion +=menu_hexdump_bytes_por_linea;
						}
}

void menu_debug_hexdump_copy(void)
{


    char string_address[10];

    sprintf (string_address,"%XH",menu_debug_hexdump_direccion);
    menu_ventana_scanf("Source?",string_address,10);
	menu_z80_moto_int source=parse_string_to_number(string_address);

    sprintf (string_address,"%XH",source);
    menu_ventana_scanf("Destination?",string_address,10);
	menu_z80_moto_int destination=parse_string_to_number(string_address);
	
	int destzone=menu_change_memory_zone_list_title("Destination Zone");
	if (destzone==-2) return; //Pulsado ESC
	
	int origzone=menu_debug_memory_zone;
	

    strcpy (string_address,"1");
    menu_ventana_scanf("Length?",string_address,10);
	menu_z80_moto_int longitud=parse_string_to_number(string_address);	
	
	

	if (menu_confirm_yesno("Copy bytes")) {
		for (;longitud>0;source++,destination++,longitud--) {
			menu_set_memzone(origzone);
			//Antes de escribir o leer, normalizar zona memoria
			menu_debug_set_memory_zone_attr();
			source=adjust_address_memory_size(source);
			z80_byte valor=menu_debug_get_mapped_byte(source);
			
			
			menu_set_memzone(destzone);
			//Antes de escribir o leer, normalizar zona memoria
			menu_debug_set_memory_zone_attr();
			destination=adjust_address_memory_size(destination);
			menu_debug_write_mapped_byte(destination,valor);
		}
		
		//dejar la zona origen tal cual
		menu_set_memzone(origzone);
	}


}

void menu_debug_hexdump_aviso_edit_filezone(zxvision_window *w)
{
							menu_warn_message("Memory zone is File zone. Changes won't be saved to the file");
							//Volver a dibujar ventana, pues se ha borrado al aparecer el aviso
							//menu_debug_hexdump_ventana();	
	zxvision_draw_window(w);
}

void menu_debug_hexdump_info_subzones(void)
{
	
		int x=1;
		int y=1;
		int ancho=30;
		int alto=22;



        subzone_info *puntero;
        puntero=machine_get_memory_subzone_array(menu_debug_memory_zone,current_machine_type);
        if (puntero==NULL) return;

		zxvision_window ventana;

                zxvision_new_window(&ventana,x,y,ancho,alto,
                                                        64,alto-2,"Memory subzones");

                zxvision_draw_window(&ventana);		

        int i;

		char buffer_linea[64];
        for (i=0;puntero[i].nombre[0]!=0;i++) {

			//printf ("inicio: %d fin: %d texto: %s\n",puntero[i].inicio,puntero[i].fin,puntero[i].nombre);
			sprintf (buffer_linea,"%06X-%06X %s",puntero[i].inicio,puntero[i].fin,puntero[i].nombre);
			zxvision_print_string_defaults_fillspc(&ventana,1,i,buffer_linea);
			
		}

		zxvision_draw_window_contents(&ventana);

		zxvision_wait_until_esc(&ventana);

        cls_menu_overlay();

                zxvision_destroy_window(&ventana);					


}

void menu_debug_hexdump_crea_ventana(zxvision_window *ventana,int x,int y,int ancho,int alto)
{
	//asignamos mismo ancho visible que ancho total para poder usar la ultima columna de la derecha, donde se suele poner scroll vertical
	zxvision_new_window_nocheck_staticsize(ventana,x,y,ancho,alto,ancho,alto-2,"Hexadecimal Editor");

	//printf ("ancho: %d alto: %d\n",ancho,alto);

	ventana->can_use_all_width=1; //Para poder usar la ultima columna de la derecha donde normalmente aparece linea scroll

	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"hexeditor");

	//Permitir hotkeys desde raton
	ventana->can_mouse_send_hotkeys=1;	

	zxvision_draw_window(ventana);

}

menu_z80_moto_int menu_debug_hexdump_get_cursor_pointer(void)
{
							//Obtener direccion puntero
						menu_z80_moto_int direccion_cursor=menu_debug_hexdump_direccion;

						//int si_zona_hexa=0; //en zona hexa o ascii
						//if (menu_hexdump_edit_position_x<bytes_por_linea*2) si_zona_hexa=1;


						if (!menu_debug_hexdump_cursor_en_zona_ascii) {
							//Sumar x (cada dos, una posicion)
							direccion_cursor +=menu_hexdump_edit_position_x/2;
						}
						else {
							int indice_hasta_ascii=menu_hexdump_bytes_por_linea*2+1; //el hexa y el espacio
							direccion_cursor +=menu_hexdump_edit_position_x-indice_hasta_ascii;
						}

						//Sumar y. 
						direccion_cursor +=menu_hexdump_edit_position_y*menu_hexdump_bytes_por_linea;

						//Ajustar direccion a zona memoria
						direccion_cursor=adjust_address_memory_size(direccion_cursor);


						return direccion_cursor;
}

void menu_debug_hexdump(MENU_ITEM_PARAMETERS)
{
	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

	zxvision_window ventana;
	int xventana,yventana,ancho_ventana,alto_ventana;
	
	if (!util_find_window_geometry("hexeditor",&xventana,&yventana,&ancho_ventana,&alto_ventana)) {
		xventana=DEBUG_HEXDUMP_WINDOW_X;
		yventana=DEBUG_HEXDUMP_WINDOW_Y;
		ancho_ventana=DEBUG_HEXDUMP_WINDOW_ANCHO;
		alto_ventana=DEBUG_HEXDUMP_WINDOW_ALTO;
	}


	//asignamos mismo ancho visible que ancho total para poder usar la ultima columna de la derecha, donde se suele poner scroll vertical
	//zxvision_new_window_nocheck_staticsize(&ventana,x,y,ancho,alto,ancho,alto-2,"Hexadecimal Editor");
	menu_debug_hexdump_crea_ventana(&ventana,xventana,yventana,ancho_ventana,alto_ventana);

	//Nos guardamos alto y ancho anterior. Si el usuario redimensiona la ventana, la recreamos
	int alto_anterior=alto_ventana;
	int ancho_anterior=ancho_ventana;

	



	//ventana.can_use_all_width=1; //Para poder usar la ultima columna de la derecha donde normalmente aparece linea scroll

	//zxvision_draw_window(&ventana);


    z80_byte tecla;

	int salir=0;

	z80_byte valor_xor=0;



	menu_debug_hexdump_with_ascii_modo_ascii=0;


	int asked_about_writing_rom=0;

                //Guardar estado atajos
                z80_bit antes_menu_writing_inverse_color;
                antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;	



        do {

		//printf ("dibujamos ventana\n");
			//Alto 23. lineas 13
		menu_hexdump_lineas_total=ventana.visible_height-10;

			if (menu_hexdump_lineas_total<3) menu_hexdump_lineas_total=3;

			
			menu_debug_hexdump_cursor_en_zona_ascii=0;
			int editando_en_zona_ascii=0;


					//Si maquina no es QL, direccion siempre entre 0 y 65535
					//menu_debug_hexdump_direccion=adjust_address_space_cpu(menu_debug_hexdump_direccion);
					menu_debug_hexdump_direccion=adjust_address_memory_size(menu_debug_hexdump_direccion);


		int linea=0;

		int lineas_hex=0;



		int bytes_por_ventana=menu_hexdump_bytes_por_linea*menu_hexdump_lineas_total;

		char dumpmemoria[33];



		//Antes de escribir, normalizar zona memoria
		menu_debug_set_memory_zone_attr();

				char textoshow[33];

		sprintf (textoshow,"Showing %d bytes per page:",bytes_por_ventana);
        //menu_escribe_linea_opcion(linea++,-1,1,textoshow);
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,textoshow);

        //menu_escribe_linea_opcion(linea++,-1,1,"");
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"");


		//Hacer que texto ventana empiece pegado a la izquierda
		menu_escribe_linea_startx=0;

		//No mostrar caracteres especiales
		menu_disable_special_chars.v=1;

		//Donde esta el otro caracter que acompanya al nibble, en caso de cursor en zona hexa
		int menu_hexdump_edit_position_x_nibble=menu_hexdump_edit_position_x^1;

		
		if (menu_hexdump_edit_position_x>menu_hexdump_bytes_por_linea*2) menu_debug_hexdump_cursor_en_zona_ascii=1;


		if (menu_hexdump_edit_mode && menu_debug_hexdump_cursor_en_zona_ascii) editando_en_zona_ascii=1;		

		char nibble_char='X';	
		char nibble_char_cursor='X';	

		for (;lineas_hex<menu_hexdump_lineas_total;lineas_hex++,linea++) {

			menu_z80_moto_int dir_leida=menu_debug_hexdump_direccion+lineas_hex*menu_hexdump_bytes_por_linea;
			menu_debug_hexdump_direccion=adjust_address_memory_size(menu_debug_hexdump_direccion);

			menu_debug_hexdump_with_ascii(dumpmemoria,dir_leida,menu_hexdump_bytes_por_linea,valor_xor);

			zxvision_print_string_defaults_fillspc(&ventana,0,linea,dumpmemoria);

			//Meter el nibble_char si corresponde
			if (lineas_hex==menu_hexdump_edit_position_y) {
				nibble_char_cursor=dumpmemoria[7+menu_hexdump_edit_position_x];
				if (!editando_en_zona_ascii) nibble_char=dumpmemoria[7+menu_hexdump_edit_position_x_nibble];
			}
		}


		menu_escribe_linea_startx=1;

		//Volver a mostrar caracteres especiales
		menu_disable_special_chars.v=0;		



		//Mostrar cursor si en modo edicion
		
		//Al mostrar en cursor: si esta en parte ascii, hacer parpadear el caracter en esa zona, metiendo color de opcion seleccionada
		//Si esta en parte hexa, parpadeamos la parte del nibble que editamos, el otro nibble no parpadea. Ambos tienen color de opcion seleccionada
		//Si multitarea esta a off, no existe el parpadeo, y por tanto, para que se viera en que nibble edita, se mostrara el caracter _, logicamente
		//tapando el caracter de debajo
		//Para ver los caracteres de debajo, los asignamos antes, en el bucle que hace el volcado hexa, y lo guardo en las variables
		//nibble_char_cursor (que dice el caracter de debajo del cursor) y nibble_char (que dice el otro caracter que acompanya al nibble)
	
		
		if (menu_hexdump_edit_mode) {
			int xfinal=7+menu_hexdump_edit_position_x;
			int yfinal=2+menu_hexdump_edit_position_y;			

			menu_debug_hexdump_print_editcursor(&ventana,xfinal,yfinal,nibble_char_cursor);

			//Indicar nibble entero. En caso de edit hexa
			if (!editando_en_zona_ascii) {
				xfinal=7+menu_hexdump_edit_position_x_nibble;
				menu_debug_hexdump_print_editcursor_nibble(&ventana,xfinal,yfinal,nibble_char);
			}
		}


				//Forzar a mostrar atajos
                menu_writing_inverse_color.v=1;


//printf ("zone size: %x dir: %x\n",menu_debug_memory_zone_size,menu_debug_hexdump_direccion);

        //menu_escribe_linea_opcion(linea++,-1,1,"");
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"");

		char buffer_linea[64]; //Por si acaso, entre negritas y demas

		char buffer_char_type[20];

		char string_atajos[3]="~~"; 
		//Si esta en edit mode y en zona de ascii, no hay atajos



		if (editando_en_zona_ascii) string_atajos[0]=0;

		if (menu_debug_hexdump_with_ascii_modo_ascii==0) {
			sprintf (buffer_char_type,"ASCII");
		}

		else if (menu_debug_hexdump_with_ascii_modo_ascii==1) {
			sprintf (buffer_char_type,"ZX80");
		}

		else sprintf (buffer_char_type,"ZX81");



		//Si esta editando, mostrar puntero en leyenda de memptr
		char buffer_puntero[40];
		if (menu_hexdump_edit_mode) {
			menu_z80_moto_int direccion_cursor=menu_debug_hexdump_get_cursor_pointer();
			char buf_temp_pointer[32];

			menu_debug_print_address_memory_zone(buf_temp_pointer,direccion_cursor);
			sprintf(buffer_puntero," (%s)",buf_temp_pointer);
		}
		else {
			buffer_puntero[0]=0;
		}

		sprintf (buffer_linea,"%smemptr%s C%sopy",string_atajos,buffer_puntero,string_atajos);


		//menu_escribe_linea_opcion(linea++,-1,1,buffer_linea);
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_linea);

		sprintf (buffer_linea,"[%c] %sinvert [%c] Edi%st C%shar:%s",
			(valor_xor==0 ? ' ' : 'X'), 
			string_atajos,
			
			(menu_hexdump_edit_mode==0 ? ' ' : 'X' ),
			string_atajos,
			
			string_atajos,
			buffer_char_type
			);
		//menu_escribe_linea_opcion(linea++,-1,1,buffer_linea);
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_linea);


		char memory_zone_text[64]; //espacio temporal mas grande por si acaso
		if (menu_debug_show_memory_zones==0) {
			sprintf (memory_zone_text,"Mem %szone (mapped memory)",string_atajos);
		}
		else {
			//printf ("Info zona %d\n",menu_debug_memory_zone);
			char buffer_name[MACHINE_MAX_MEMORY_ZONE_NAME_LENGHT+1];
			//int readwrite;
			machine_get_memory_zone_name(menu_debug_memory_zone,buffer_name);
			sprintf (memory_zone_text,"Mem %szone (%d %s)",string_atajos,menu_debug_memory_zone,buffer_name);
			//printf ("size: %X\n",menu_debug_memory_zone_size);
			//printf ("Despues zona %d\n",menu_debug_memory_zone);
		}

		//truncar texto a 32 por si acaso
		memory_zone_text[32]=0;
		//menu_escribe_linea_opcion(linea++,-1,1,memory_zone_text);
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,memory_zone_text);

		sprintf (textoshow," Size: %d (%d KB)",menu_debug_memory_zone_size,menu_debug_memory_zone_size/1024);
		//menu_escribe_linea_opcion(linea++,-1,1,textoshow);
		zxvision_print_string_defaults_fillspc(&ventana,1,linea++,textoshow);


		char subzone_info[33];
		machine_get_memory_subzone_name(menu_debug_memory_zone,current_machine_type, menu_debug_hexdump_direccion, subzone_info);
		if (subzone_info[0]!=0) {
			sprintf(buffer_linea," S~~ubzone info: %s",subzone_info);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_linea);
		}
		else {
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"");
		}


		//Restaurar comportamiento atajos
		menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;

		zxvision_draw_window_contents(&ventana);
		//NOTA: este menu no acostumbra a refrescar rapido la ventana cuando la redimensionamos con el raton
		//es una razon facil: el volcado de hexa usa relativamente mucha cpu,
		//cada vez que redimensionamos ventana, se llama al bucle continuamente, usando mucha cpu y si esta el autoframeskip,
		//hace saltar frames
		

		tecla=zxvision_common_getkey_refresh();		


				//Aviso: hay que conseguir que las letras de accion no esten entre la a-f, porque asi,
				//podemos usar dichas letras para editar hexa

				switch (tecla) {

					case 11:
						menu_debug_hexdump_cursor_arriba();

					break;

					case 10:
						menu_debug_hexdump_cursor_abajo();

					break;

					case 8:
					case 12:
						//izquierda o delete
						if (menu_hexdump_edit_mode) {
							//if (menu_hexdump_edit_position_x>0) menu_hexdump_edit_position_x--;
							menu_debug_hexdump_edit_cursor_izquierda();
						}
					break;

					case 9:
						//derecha
						if (menu_hexdump_edit_mode) {
							menu_debug_hexdump_edit_cursor_derecha(0);
							//if (menu_hexdump_edit_position_x<(bytes_por_linea*2)-1) menu_hexdump_edit_position_x++;
						}
					break;					

					case 24:
						//PgUp
						menu_debug_hexdump_direccion -=bytes_por_ventana;
						menu_debug_hexdump_direccion=menu_debug_hexdump_adjusta_en_negativo(menu_debug_hexdump_direccion,bytes_por_ventana);
					break;

					case 25:
						//PgDn
						menu_debug_hexdump_direccion +=bytes_por_ventana;
					break;

					case 'm':
						if (!editando_en_zona_ascii)  {
							menu_debug_hexdump_direccion=menu_debug_hexdump_change_pointer(menu_debug_hexdump_direccion);
							//menu_debug_hexdump_ventana();
							zxvision_draw_window(&ventana);
							//Y resetear cursor edicion
							menu_hexdump_edit_position_x=0;
							menu_hexdump_edit_position_y=0;
						}
					break;

					case 'o':
						if (!editando_en_zona_ascii)  {
							menu_debug_hexdump_copy();
							//menu_debug_hexdump_ventana();
							zxvision_draw_window(&ventana);
						}
					break;					

					case 'h':
						if (!editando_en_zona_ascii)  {
							menu_debug_hexdump_with_ascii_modo_ascii++;
							if (menu_debug_hexdump_with_ascii_modo_ascii==3) menu_debug_hexdump_with_ascii_modo_ascii=0;
						}
					break;

					case 'i':
						if (!editando_en_zona_ascii) valor_xor ^= 255;
					break;

					case 't':
						if (!editando_en_zona_ascii) {
							menu_hexdump_edit_mode ^= 1;
							menu_espera_no_tecla();
							tecla=0; //para no enviar dicha tecla al editor
						}

						//Si zona de filemem
						if (menu_hexdump_edit_mode && menu_debug_memory_zone==MEMORY_ZONE_NUM_FILE_ZONE) {
							menu_debug_hexdump_aviso_edit_filezone(&ventana);				
						}
					break;	

					case 'u':
						//Ver info subzonas
						menu_debug_hexdump_info_subzones();
					break;				

					//case 'l':
					//	menu_debug_hex_shows_inves_low_ram.v ^=1;
					//break;

					case 'z':

						if (!editando_en_zona_ascii) {
							menu_debug_change_memory_zone();
							asked_about_writing_rom=0;
						}

						break;

					//Salir con ESC si no es modo edit
					case 2:
						if (menu_hexdump_edit_mode) {
							menu_hexdump_edit_mode=0;
						}
						else salir=1;
					break;

					//Enter tambien sale de modo edit
					case 13:
						if (menu_hexdump_edit_mode) menu_hexdump_edit_mode=0;
					break;



				}

				//Y ahora para el caso de edit_mode y pulsar tecla hexa o ascii segun la zona
				int editar_byte=0;
				if (menu_hexdump_edit_mode) {
					//Para la zona ascii
					if (menu_debug_hexdump_cursor_en_zona_ascii && tecla>=32 && tecla<=126) editar_byte=1; 

					//Para la zona hexa
					if (
						!menu_debug_hexdump_cursor_en_zona_ascii && 
						( (tecla>='0' && tecla<='9') || (tecla>='a' && tecla<='f') )
						) {
						editar_byte=1; 
					}
				}

				//Ver si vamos a editar en zona de rom
				if (editar_byte) {
					int attribute_zone;
					//Si zona por defecto mapped memory, asumimos lectura/escritura
					if (menu_debug_memory_zone==-1) attribute_zone=3;
					else machine_get_memory_zone_attrib(menu_debug_memory_zone, &attribute_zone);

					//printf ("Attrib zone %d asked %d\n",attribute_zone,asked_about_writing_rom);

					//Attrib: bit 0: read, bit 1: write
					//Si no tiene atributo de escritura y no se ha pedido antes si se quiere escribir en rom
					if ( (attribute_zone&2)==0 && !asked_about_writing_rom) {
						if (menu_confirm_yesno_texto("Memory zone is ROM","Sure you want to edit?")==0) {
							editar_byte=0;
						}
						else {
							asked_about_writing_rom=1;
						}

						//Volver a dibujar ventana, pues se ha borrado al pregutar confirmacion
						//menu_debug_hexdump_ventana();
						zxvision_draw_window(&ventana);
					}

 
				}


				//asked_about_writing_rom

				if (editar_byte) {
						menu_z80_moto_int direccion_cursor=menu_debug_hexdump_get_cursor_pointer();
				

						//TODO: ver si se sale de tamanyo zona memoria

						//printf ("Direccion edicion: %X\n",direccion_cursor);

						//Obtenemos byte en esa posicion
						z80_byte valor_leido=menu_debug_get_mapped_byte(direccion_cursor);


						//Estamos en zona hexa o ascii

						if (!menu_debug_hexdump_cursor_en_zona_ascii) {
							//printf ("Zona hexa\n");
							//Zona hexa

							//Obtener valor nibble
							z80_byte valor_nibble;

							if (tecla>='0' && tecla<='9') valor_nibble=tecla-'0';
							else valor_nibble=tecla-'a'+10;

							//Ver si par o impar
							if ( (menu_hexdump_edit_position_x %2) ==0) {
								//par. alterar nibble alto
								valor_leido=(valor_leido&0x0F) | (valor_nibble<<4);
							}

							else {
								//impar. alterar nibble bajo
								valor_leido=(valor_leido&0xF0) | valor_nibble;
							}
						}

						else {
							//printf ("Zona ascii\n");
							valor_leido=tecla;
						}



						//Escribimos valor
						menu_debug_write_mapped_byte(direccion_cursor,valor_leido);

						//Y mover cursor a la derecha
						menu_debug_hexdump_edit_cursor_derecha(1);

						//Si se llega a detecha de hexa o ascii, saltar linea

					
				}

		//Si ha cambiado el alto
		alto_ventana=ventana.visible_height;
		ancho_ventana=ventana.visible_width;
		xventana=ventana.x;
		yventana=ventana.y;
		if (alto_ventana!=alto_anterior || ancho_ventana!=ancho_anterior) {
			//printf ("recrear ventana\n");
			//Recrear ventana
			//Cancelamos edicion si estaba ahi
			editando_en_zona_ascii=0;
			menu_hexdump_edit_mode=0;
			menu_hexdump_edit_position_x=0;
			menu_hexdump_edit_position_y=0;

			zxvision_destroy_window(&ventana);
			menu_debug_hexdump_crea_ventana(&ventana,xventana,yventana,ancho_ventana,alto_ventana);
			alto_anterior=alto_ventana;
			ancho_anterior=ancho_ventana;
		}
			


        } while (salir==0);

	cls_menu_overlay();

	//util_add_window_geometry("hexeditor",ventana.x,ventana.y,ventana.visible_width,ventana.visible_height);
	util_add_window_geometry_compact(&ventana);

	zxvision_destroy_window(&ventana);
	

}


void menu_debug_view_basic(MENU_ITEM_PARAMETERS)
{

	char results_buffer[MAX_TEXTO_GENERIC_MESSAGE];
	debug_view_basic(results_buffer);

  menu_generic_message_format("View Basic","%s",results_buffer);
}


int get_efectivo_tamanyo_find_buffer(void)
{
	return 65536;
}


void menu_find_bytes_clear_results_process(void)
{
        //inicializamos array
        int i;

        for (i=0;i<get_efectivo_tamanyo_find_buffer();i++) menu_find_bytes_mem_pointer[i]=0;

        menu_find_bytes_empty=1;

}

void menu_find_bytes_clear_results(MENU_ITEM_PARAMETERS)
{
        menu_find_bytes_clear_results_process();

        menu_generic_message("Clear Results","OK. Results cleared");
}

void menu_find_bytes_view_results(MENU_ITEM_PARAMETERS)
{

        int index_find,index_buffer;

        char results_buffer[MAX_TEXTO_GENERIC_MESSAGE];

        //margen suficiente para que quepa una linea
        //direccion+salto linea+codigo 0
        char buf_linea[9];

        index_buffer=0;

        int encontrados=0;

        int salir=0;

        for (index_find=0;index_find<get_efectivo_tamanyo_find_buffer() && salir==0;index_find++) {
                if (menu_find_bytes_mem_pointer[index_find]) {
                        sprintf (buf_linea,"%d\n",index_find);
                        sprintf (&results_buffer[index_buffer],"%s\n",buf_linea);
                        index_buffer +=strlen(buf_linea);
                        encontrados++;
                }

                //controlar maximo
                //20 bytes de margen
                if (index_buffer>MAX_TEXTO_GENERIC_MESSAGE-20) {
                        debug_printf (VERBOSE_ERR,"Too many results to show. Showing only the first %d",encontrados);
                        //forzar salir
                        salir=1;
                }

        }

        results_buffer[index_buffer]=0;

        menu_generic_message("View Results",results_buffer);
}

//Busqueda desde direccion indicada
int menu_find_bytes_process_from(z80_byte byte_to_find,int inicio)
{
	int dir;
	int total_items_found=0;
	int final_find=get_efectivo_tamanyo_find_buffer();

	//Busqueda con array no inicializado
	if (menu_find_bytes_empty) {

					debug_printf (VERBOSE_INFO,"Starting Search with no previous results");


					//asumimos que no va a encontrar nada
					menu_find_bytes_empty=1;

					for (dir=inicio;dir<final_find;dir++) {
									if (peek_byte_z80_moto(dir)==byte_to_find) {
													menu_find_bytes_mem_pointer[dir]=1;

													//al menos hay un resultado
													menu_find_bytes_empty=0;

													//incrementamos contador de resultados para mostrar al final
													total_items_found++;
									}
					}

	}

	else {
					//Busqueda con array ya con contenido
					//examinar solo las direcciones que indique el array

					debug_printf (VERBOSE_INFO,"Starting Search using previous results");

					//asumimos que no va a encontrar nada
					menu_find_bytes_empty=1;

					int i;
					for (i=0;i<final_find;i++) {
									if (menu_find_bytes_mem_pointer[i]) {
													//Ver el contenido de esa direccion
													if (peek_byte_z80_moto(i)==byte_to_find) {
																	//al menos hay un resultado
																	menu_find_bytes_empty=0;
																	//incrementamos contador de resultados para mostrar al final
																	total_items_found++;
													}
													else {
																	//el byte ya no esta en esa direccion
																	menu_find_bytes_mem_pointer[i]=0;
													}
									}
					}
	}

	return total_items_found;

}

//Busqueda desde direccion 0
int menu_find_bytes_process(z80_byte byte_to_find)
{
	return menu_find_bytes_process_from(byte_to_find,0);
}

void menu_find_bytes_find(MENU_ITEM_PARAMETERS)
{

        //Buscar en la memoria direccionable (0...65535) si se encuentra el byte
        z80_byte byte_to_find;


        char string_find[4];

        sprintf (string_find,"0");

        menu_ventana_scanf("Find byte",string_find,4);

        int valor_find=parse_string_to_number(string_find);

        if (valor_find<0 || valor_find>255) {
                debug_printf (VERBOSE_ERR,"Invalid value %d",valor_find);
                return;
        }


        byte_to_find=valor_find;


        int total_items_found;

				total_items_found=menu_find_bytes_process(byte_to_find);

        menu_generic_message_format("Find","Total addresses found: %d",total_items_found);


}



// TODO
#define MEM_LIMIT (1024*256)

void menu_find_bytes_alloc_if_needed(void)
{
	//Si puntero memoria no esta asignado, asignarlo, o si hemos cambiado de tipo de maquina
	if (menu_find_bytes_mem_pointer==NULL) {

					//65536 elementos del array, cada uno de tamanyo unsigned char
					//total_tamanyo_find_buffer=get_total_tamanyo_find_buffer();

					menu_find_bytes_mem_pointer=malloc(MEM_LIMIT);
					if (menu_find_bytes_mem_pointer==NULL) cpu_panic("Error allocating find array");

					//inicializamos array
					int i;
					for (i=0;i<get_efectivo_tamanyo_find_buffer();i++) menu_find_bytes_mem_pointer[i]=0;
	}
}


void menu_find_lives_restart(MENU_ITEM_PARAMETERS)
{
	menu_find_lives_state=0;
}

void menu_find_lives_initial(MENU_ITEM_PARAMETERS)
{
	//Limpiar resultados
	menu_find_bytes_alloc_if_needed();
	if (menu_find_lives_state==0) menu_find_bytes_clear_results_process();

	//Suponemos que la segunda vez habra perdido al menos 1 vida
	if (menu_find_lives_state==1 && lives_to_find>0) lives_to_find--;

	//Pedir vidas actuales
	//Buscar en la memoria direccionable (0...65535) si se encuentra el byte


	char string_find[4];

	sprintf (string_find,"%d",lives_to_find);

	menu_ventana_scanf("Current lives",string_find,4);

	int valor_find=parse_string_to_number(string_find);

	if (valor_find<0 || valor_find>255) {
					debug_printf (VERBOSE_ERR,"Invalid value %d",valor_find);
					return;
	}


	lives_to_find=valor_find;


	int total_items_found;

	//Empezar a buscar desde la 16384. No tiene sentido buscar desde la rom
	total_items_found=menu_find_bytes_process_from(lives_to_find,16384);

	//menu_generic_message_format("Find","Total addresses found: %d",total_items_found);

	//Si estamos en estado 0
	if (menu_find_lives_state==0) {
		if (total_items_found==0) {
			 menu_generic_message("Find lives","Sorry, no lives counter found");
		}
		else {
			menu_generic_message("Find lives","Ok. Continue playing game and come back when you lose a life");
			menu_find_lives_state=1;
		}
	}

	//Si estamos en estado 1
	else if (menu_find_lives_state==1) {
		if (total_items_found==0) {
			menu_generic_message("Find lives","Sorry, I haven't found any addresses. The process has been restarted");
			menu_find_lives_state=0;
		}
		else if (total_items_found!=1) {
			 menu_generic_message("Find lives","Sorry, no unique address found. You may want to try again losing another live or maybe manually find it on the Find bytes menu");
		}
		else {

			//Buscar la direccion
			int index_find;

			int salir=0;

			for (index_find=0;index_find<get_efectivo_tamanyo_find_buffer() && salir==0;index_find++) {
							if (menu_find_bytes_mem_pointer[index_find]) {
											menu_find_lives_pointer=index_find;
											salir=0;
							}
			}


			menu_find_lives_state=2;

			menu_generic_message("Find lives","Great. Memory pointer found");
		}
	}



	//Buscar bytes

}


void menu_find_lives_set(MENU_ITEM_PARAMETERS)
{


	char string_lives[4];

	sprintf (string_lives,"%d",ultimo_menu_find_lives_set);

	menu_ventana_scanf("Lives?",string_lives,4);

	int valor_lives=parse_string_to_number(string_lives);

	if (valor_lives<0 || valor_lives>255) {
					debug_printf (VERBOSE_ERR,"Invalid value %d",valor_lives);
					return;
	}

	ultimo_menu_find_lives_set=valor_lives;

	poke_byte_z80_moto(menu_find_lives_pointer,valor_lives);

}


void menu_debug_poke(MENU_ITEM_PARAMETERS)
{

        int valor_poke,dir,veces;

        char string_poke[4];
        char string_dir[10];
	char string_veces[6];

        sprintf (string_dir,"%XH",last_debug_poke_dir);

        menu_ventana_scanf("Address",string_dir,10);

        dir=parse_string_to_number(string_dir);

				last_debug_poke_dir=dir;

        sprintf (string_poke,"0");

        menu_ventana_scanf("Poke Value",string_poke,4);

        valor_poke=parse_string_to_number(string_poke);

        if (valor_poke<0 || valor_poke>255) {
                debug_printf (VERBOSE_ERR,"Invalid value %d",valor_poke);
                return;
        }


	sprintf (string_veces,"1");

	menu_ventana_scanf("How many bytes?",string_veces,6);

	veces=parse_string_to_number(string_veces);

	if (veces<1 || veces>65536) {
                debug_printf (VERBOSE_ERR,"Invalid quantity %d",veces);
		return;
	}


	for (;veces;veces--,dir++) {

	        //poke_byte_no_time(dir,valor_poke);
		//poke_byte_z80_moto(dir,valor_poke);
		menu_debug_write_mapped_byte(dir,valor_poke);

	}

}


void menu_debug_poke_pok_file(MENU_ITEM_PARAMETERS)
{

        char *filtros[2];

        filtros[0]="pok";
        filtros[1]=0;

	char pokfile[PATH_MAX];

        int ret;

        ret=menu_filesel("Select POK File",filtros,pokfile);

	
	//contenido
	//MAX_LINEAS_POK_FILE es maximo de lineas de pok file
	//normalmente la tabla de pokes sera menor que el numero de lineas en el archivo .pok
	//struct s_pokfile tabla_pokes[MAX_LINEAS_POK_FILE];
	struct s_pokfile *tabla_pokes;
	tabla_pokes=malloc(sizeof(struct s_pokfile)*MAX_LINEAS_POK_FILE);	

	//punteros
	//struct s_pokfile *punteros_pokes[MAX_LINEAS_POK_FILE];
	struct s_pokfile **punteros_pokes;
	punteros_pokes=malloc(sizeof(struct s_pokfile *)*MAX_LINEAS_POK_FILE);


	if (tabla_pokes==NULL || punteros_pokes==NULL) cpu_panic("Can not allocate memory for poke table");

	int i;
	for (i=0;i<MAX_LINEAS_POK_FILE;i++) punteros_pokes[i]=&tabla_pokes[i];


        if (ret==1) {

                //cls_menu_overlay();
		int total=util_parse_pok_file(pokfile,punteros_pokes);

		if (total<1) {
			debug_printf (VERBOSE_ERR,"Error parsing POK file");
			return;
		}

		int j;
		for (j=0;j<total;j++) {
			debug_printf (VERBOSE_DEBUG,"menu poke index %d text %s bank %d address %d value %d value_orig %d",
				punteros_pokes[j]->indice_accion,
				punteros_pokes[j]->texto,
				punteros_pokes[j]->banco,
				punteros_pokes[j]->direccion,
				punteros_pokes[j]->valor,
				punteros_pokes[j]->valor_orig);
		}


		//Meter cada poke en un menu




        menu_item *array_menu_debug_pok_file;
        menu_item item_seleccionado;
        int retorno_menu;
	//Resetear siempre ultima linea = 0
	debug_pok_file_opcion_seleccionada=0;

	//temporal para mostrar todos los caracteres 0-255
	//int temp_conta=1;

        do {



		//Meter primer item de menu
		//truncar texto a 28 caracteres si excede de eso
		if (strlen(punteros_pokes[0]->texto)>28) punteros_pokes[0]->texto[28]=0;
                menu_add_item_menu_inicial_format(&array_menu_debug_pok_file,MENU_OPCION_NORMAL,NULL,NULL,"%s", punteros_pokes[0]->texto);


		//Luego recorrer array de pokes y cuando el numero de poke se incrementa, agregar
		int poke_anterior=0;

		int total_elementos=1;

		for (j=1;j<total;j++) {
			if (punteros_pokes[j]->indice_accion!=poke_anterior) {

				//temp para mostrar todos los caracteres 0-255
				//int kk;
				//for (kk=0;kk<strlen(punteros_pokes[j]->texto);kk++) {
				//	punteros_pokes[j]->texto[kk]=temp_conta++;
				//	if (temp_conta==256) temp_conta=1;
				//}

				poke_anterior=punteros_pokes[j]->indice_accion;
				//truncar texto a 28 caracteres si excede de eso
				if (strlen(punteros_pokes[j]->texto)>28) punteros_pokes[j]->texto[28]=0;
				menu_add_item_menu_format(array_menu_debug_pok_file,MENU_OPCION_NORMAL,NULL,NULL,"%s", punteros_pokes[j]->texto);

				total_elementos++;
				if (total_elementos==20) {
					debug_printf (VERBOSE_DEBUG,"Too many pokes to show on Window. Showing only first 20");
					menu_warn_message("Too many pokes to show on Window. Showing only first 20");
					break;
				}


			}
		}



                menu_add_item_menu(array_menu_debug_pok_file,"",MENU_OPCION_SEPARADOR,NULL,NULL);


                //menu_add_item_menu(array_menu_debug_pok_file,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_debug_pok_file);

                retorno_menu=menu_dibuja_menu(&debug_pok_file_opcion_seleccionada,&item_seleccionado,array_menu_debug_pok_file,"Select Poke" );

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion

			//Hacer poke sabiendo la linea seleccionada. Desde ahi, ejecutar todos los pokes de dicha accion
			debug_printf (VERBOSE_DEBUG,"Doing poke/s from line %d",debug_pok_file_opcion_seleccionada);

			z80_byte banco;
			z80_int direccion;
			z80_byte valor;

			//buscar indice_accion
			int result_poke=0;
			for (j=0;j<total && result_poke==0;j++) {

				debug_printf (VERBOSE_DEBUG,"index %d looking %d current %d",j,debug_pok_file_opcion_seleccionada,punteros_pokes[j]->indice_accion);

				if (punteros_pokes[j]->indice_accion==debug_pok_file_opcion_seleccionada) {
					banco=punteros_pokes[j]->banco;
					direccion=punteros_pokes[j]->direccion;
					valor=punteros_pokes[j]->valor;
					debug_printf (VERBOSE_DEBUG,"Doing poke bank %d address %d value %d",banco,direccion,valor);
					result_poke=util_poke(banco,direccion,valor);
				}


                      

			}
			if (result_poke==0) menu_generic_message("Poke","OK. Poke applied");
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);




        }

	free(tabla_pokes);
	free(punteros_pokes);

}


void menu_debug_load_binary(MENU_ITEM_PARAMETERS)
{

	char *filtros[2];

	filtros[0]="";
	filtros[1]=0;


	//guardamos directorio actual
	char directorio_actual[PATH_MAX];
	getcwd(directorio_actual,PATH_MAX);

	int ret;

	//Obtenemos ultimo directorio visitado
	if (binary_file_load[0]!=0) {
		char directorio[PATH_MAX];
		util_get_dir(binary_file_load,directorio);
		//printf ("strlen directorio: %d directorio: %s\n",strlen(directorio),directorio);

		//cambiamos a ese directorio, siempre que no sea nulo
		if (directorio[0]!=0) {
				debug_printf (VERBOSE_INFO,"Changing to last directory: %s",directorio);
				menu_filesel_chdir(directorio);
		}
	}

	ret=menu_filesel("Select File to Load",filtros,binary_file_load);

	//volvemos a directorio inicial
	menu_filesel_chdir(directorio_actual);

	if (ret==1) {

		cls_menu_overlay();

		menu_debug_change_memory_zone();


		char string_direccion[10];
		sprintf (string_direccion,"%XH",load_binary_last_address);
		menu_ventana_scanf("Address: ",string_direccion,10);
		int valor_leido_direccion=parse_string_to_number(string_direccion);
		load_binary_last_address=valor_leido_direccion;

		cls_menu_overlay();

		char string_longitud[8];
		sprintf (string_longitud,"%d",load_binary_last_length);
		menu_ventana_scanf("Length: 0 - all",string_longitud,8);
		int valor_leido_longitud=parse_string_to_number(string_longitud);
		load_binary_last_length=valor_leido_longitud;

		load_binary_file(binary_file_load,valor_leido_direccion,valor_leido_longitud);

		//Y salimos de todos los menus
		salir_todos_menus=1;


    }

}


void menu_debug_save_binary(MENU_ITEM_PARAMETERS)
{

	char *filtros[2];

	filtros[0]="";
	filtros[1]=0;


	//guardamos directorio actual
	char directorio_actual[PATH_MAX];
	getcwd(directorio_actual,PATH_MAX);

	int ret;

	//Obtenemos ultimo directorio visitado
	if (binary_file_save[0]!=0) {
		char directorio[PATH_MAX];
		util_get_dir(binary_file_save,directorio);
		//printf ("strlen directorio: %d directorio: %s\n",strlen(directorio),directorio);

		//cambiamos a ese directorio, siempre que no sea nulo
		if (directorio[0]!=0) {
				debug_printf (VERBOSE_INFO,"Changing to last directory: %s",directorio);
				menu_filesel_chdir(directorio);
		}
	}

	ret=menu_filesel("Select File to Save",filtros,binary_file_save);

	//volvemos a directorio inicial
	menu_filesel_chdir(directorio_actual);


    if (ret==1) {

		//Ver si archivo existe y preguntar

      	if (si_existe_archivo(binary_file_save)) {

			if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return;

		}

		cls_menu_overlay();

		menu_debug_change_memory_zone();

		char string_direccion[10];
		sprintf (string_direccion,"%XH",save_binary_last_address);
		menu_ventana_scanf("Address: ",string_direccion,10);
		int valor_leido_direccion=parse_string_to_number(string_direccion);
		save_binary_last_address=valor_leido_direccion;

		cls_menu_overlay();
		char string_longitud[8];
		sprintf (string_longitud,"%d",save_binary_last_length);
		menu_ventana_scanf("Length: 0 - all",string_longitud,8);
		int valor_leido_longitud=parse_string_to_number(string_longitud);
		save_binary_last_length=valor_leido_longitud;						


		save_binary_file(binary_file_save,valor_leido_direccion,valor_leido_longitud);
		
		
		//Y salimos de todos los menus
        salir_todos_menus=1;

    }
}


void menu_debug_tsconf_tbblue_msx_videoregisters_overlay(void)
{
	if (!zxvision_drawing_in_background) normal_overlay_texto_menu();


	zxvision_window *ventana;

	ventana=menu_debug_tsconf_tbblue_msx_videoregisters_overlay_window;	

//esto hara ejecutar esto 2 veces por segundo
			if ( ((contador_segundo%500) == 0 && menu_debug_tsconf_tbblue_msx_videoregisters_valor_contador_segundo_anterior!=contador_segundo) || menu_multitarea==0) {
											menu_debug_tsconf_tbblue_msx_videoregisters_valor_contador_segundo_anterior=contador_segundo;
				//printf ("Refrescando. contador_segundo=%d\n",contador_segundo);

				int linea=0;
				//int opcode;
				//int sumatotal;


	char texto_buffer[80];

	char texto_buffer2[64];

	//Empezar con espacio
    texto_buffer[0]=' ';				


				{

					//menu_escribe_linea_opcion(linea++,-1,1,"ULA Video mode:");		
					zxvision_print_string_defaults(ventana,1,linea++,"ULA Video mode:");

					//menu_escribe_linea_opcion(linea++,-1,1,get_spectrum_ula_string_video_mode() );
					zxvision_print_string_defaults(ventana,1,linea++,get_spectrum_ula_string_video_mode() );

					linea++;

					//menu_escribe_linea_opcion(linea++,-1,1,"Palette format:");
					//zxvision_print_string_defaults(&ventana,1,linea++,"Palette:");

					tbblue_get_string_palette_format(texto_buffer2);
					sprintf (texto_buffer,"Palette: %s",texto_buffer2);
					
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					linea++;

					/*
					(R/W) 0x12 (18) => Layer 2 RAM page
 bits 7-6 = Reserved, must be 0
 bits 5-0 = SRAM page (point to page 8 after a Reset)

(R/W) 0x13 (19) => Layer 2 RAM shadow page
 bits 7-6 = Reserved, must be 0
 bits 5-0 = SRAM page (point to page 11 after a Reset)
					*/

				//tbblue_get_offset_start_layer2_reg

					sprintf (texto_buffer,"Layer 2 mode:   %s",tbblue_get_layer2_mode_name() );
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					sprintf (texto_buffer,"Layer 2 addr:        %06XH",tbblue_get_offset_start_layer2_reg(tbblue_registers[18]) );
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					sprintf (texto_buffer,"Layer 2 shadow addr: %06XH",tbblue_get_offset_start_layer2_reg(tbblue_registers[19]) );					
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					sprintf (texto_buffer,"Tilemap base addr:     %02X00H",0x40+tbblue_get_offset_start_tilemap() );					
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					sprintf (texto_buffer,"Tile definitions addr: %02X00H",0x40+tbblue_get_offset_start_tiledef() );					
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);					

					sprintf (texto_buffer,"Tile width: %d columns",tbblue_get_tilemap_width() );					
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					sprintf (texto_buffer,"Tile bpp: %d", (tbblue_tiles_are_monocrome() ? 1 : 4)  );
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);							

					/*
					z80_byte clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][4];
z80_byte clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][4];
z80_byte clip_windows[TBBLUE_CLIP_WINDOW_ULA][4];
z80_byte clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][4];
					*/

					linea++;
					sprintf (texto_buffer,"Clip Windows:");
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					sprintf (texto_buffer,"Layer2:  X=%3d-%3d Y=%3d-%3d",
					clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][0],clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][1],clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][2],clip_windows[TBBLUE_CLIP_WINDOW_LAYER2][3]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);


					sprintf (texto_buffer,"Sprites: X=%3d-%3d Y=%3d-%3d",
					clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][0],clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][1],clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][2],clip_windows[TBBLUE_CLIP_WINDOW_SPRITES][3]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);


					sprintf (texto_buffer,"ULA:     X=%3d-%3d Y=%3d-%3d",
					clip_windows[TBBLUE_CLIP_WINDOW_ULA][0],clip_windows[TBBLUE_CLIP_WINDOW_ULA][1],clip_windows[TBBLUE_CLIP_WINDOW_ULA][2],clip_windows[TBBLUE_CLIP_WINDOW_ULA][3]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);


					sprintf (texto_buffer,"Tilemap: X=%3d-%3d Y=%3d-%3d",
					clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][0]*2,clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][1]*2+1,clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][2],clip_windows[TBBLUE_CLIP_WINDOW_TILEMAP][3]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);



					linea++;
					sprintf (texto_buffer,"Offset Windows:");
					//menu_escribe_linea_opcion(linea++,-1,1,texto_buffer);	
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);


					int off_layer2_x=tbblue_registers[22] + (tbblue_registers[113]&1)*256;
					sprintf (texto_buffer,"Layer2:     X=%4d  Y=%3d",off_layer2_x,tbblue_registers[23]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);


					sprintf (texto_buffer,"ULA/LoRes:  X=%4d  Y=%3d",tbblue_registers[50],tbblue_registers[51]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);

					//Offset X puede llegar hasta 1023. Por tanto 4 cifras. El resto X solo 3 cifras, pero los dejamos a 4 para que formato quede igual en pantalla
					sprintf (texto_buffer,"Tilemap:    X=%4d  Y=%3d",tbblue_registers[48]+256*(tbblue_registers[47]&3),tbblue_registers[49]);
					zxvision_print_string_defaults(ventana,1,linea++,texto_buffer);					

				}



				zxvision_draw_window_contents(ventana);	
			}
}



void menu_debug_tsconf_tbblue_msx_videoregisters(MENU_ITEM_PARAMETERS)
{

	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();

	zxvision_window *ventana;
		
	ventana=&menu_debug_tsconf_tbblue_msx_videoregisters_ventana;	

	//IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
	//si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
	//la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
	zxvision_delete_window_if_exists(ventana);		

	int xventana,yventana;
	int ancho_ventana,alto_ventana;

	if (!util_find_window_geometry("videoinfo",&xventana,&yventana,&ancho_ventana,&alto_ventana)) {	

		ancho_ventana=32;
		xventana=menu_center_x()-ancho_ventana/2;

			//yventana=0;
			alto_ventana=24;

		yventana=menu_center_y()-alto_ventana/2;

	}



	zxvision_new_window(ventana,xventana,yventana,ancho_ventana,alto_ventana,
						ancho_ventana-1,alto_ventana-2,"Video Info");	

	ventana->can_be_backgrounded=1;	
	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"videoinfo");											

	zxvision_draw_window(ventana);


	menu_debug_tsconf_tbblue_msx_videoregisters_overlay_window=ventana;



	//Cambiamos funcion overlay de texto de menu
	//Se establece a la de funcion de onda + texto
	set_menu_overlay_function(menu_debug_tsconf_tbblue_msx_videoregisters_overlay);


       //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
       //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
       if (zxvision_currently_restoring_windows_on_start) {
               //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
               return;
       }	

	z80_byte tecla;

	do {
		tecla=zxvision_common_getkey_refresh();		
		zxvision_handle_cursors_pgupdn(ventana,tecla);
		//printf ("tecla: %d\n",tecla);
	} while (tecla!=2 && tecla!=3);				

	//Gestionar salir con tecla background
 
	menu_espera_no_tecla(); //Si no, se va al menu anterior.
	//En AY Piano por ejemplo esto no pasa aunque el estilo del menu es el mismo...

	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	zxvision_set_window_overlay_from_current(ventana);	

    //restauramos modo normal de texto de menu
	set_menu_overlay_function(normal_overlay_texto_menu);


    cls_menu_overlay();	
	util_add_window_geometry_compact(ventana);

	if (tecla==3) {
		zxvision_message_put_window_background();
	}

	else {	


		zxvision_destroy_window(ventana);

	}

}


char *menu_tsconf_layer_aux_usedunused(int value)
{
	if (value) return menu_tsconf_layer_aux_usedunused_used;
	else return menu_tsconf_layer_aux_usedunused_unused;
}


void menu_tsconf_layer_overlay_mostrar_texto(void)
{
 int linea;

    linea=0;

    
        //mostrarlos siempre a cada refresco

                char texto_layer[33];

				{
	                sprintf (texto_layer,"ULA:       %s",menu_tsconf_layer_aux_usedunused(tbblue_if_ula_is_enabled()) ); 
    	            //menu_escribe_linea_opcion(linea,-1,1,texto_layer);
					zxvision_print_string_defaults_fillspc(menu_tsconf_layer_overlay_window,1,linea,texto_layer);
					linea +=3;

	                sprintf (texto_layer,"Tiles:     %s",menu_tsconf_layer_aux_usedunused(tbblue_if_tilemap_enabled()) ); 
    	            //menu_escribe_linea_opcion(linea,-1,1,texto_layer);
					zxvision_print_string_defaults_fillspc(menu_tsconf_layer_overlay_window,1,linea,texto_layer);
					linea +=3;			

					zxvision_print_string_defaults(menu_tsconf_layer_overlay_window,1,linea,"ULA&Tiles:");
					linea +=2;									

                	sprintf (texto_layer,"Sprites:   %s",menu_tsconf_layer_aux_usedunused(tbblue_if_sprites_enabled() ));
                	//menu_escribe_linea_opcion(linea,-1,1,texto_layer);	
					zxvision_print_string_defaults_fillspc(menu_tsconf_layer_overlay_window,1,linea,texto_layer);
					linea +=3;		

					sprintf (texto_layer,"Layer 2:   %s",menu_tsconf_layer_aux_usedunused(tbblue_is_active_layer2() ) );
    	            //menu_escribe_linea_opcion(linea,-1,1,texto_layer);
					zxvision_print_string_defaults_fillspc(menu_tsconf_layer_overlay_window,1,linea,texto_layer);
					linea +=3;						


					//Layer priorities

					z80_byte prio=tbblue_get_layers_priorities();
					sprintf (texto_layer,"Priorities: (%d)",prio);
					//menu_escribe_linea_opcion(linea++,-1,1,texto_layer);
					zxvision_print_string_defaults_fillspc(menu_tsconf_layer_overlay_window,1,linea++,texto_layer);

				
					int i;
					for (i=0;i<3;i++) {
						char nombre_capa[32];
						strcpy(nombre_capa,tbblue_get_string_layer_prio(i,prio) );
						//if (strcmp(nombre_capa,"ULA&Tiles")) strcpy(nombre_capa,"  ULA  "); //meter espacios para centrarlo
						//las otras capas son "Sprites" y "Layer 2" y ocupan lo mismo

													//     Sprites
													//    ULA&Tiles
						if (i!=2) strcpy (texto_layer,"|---------------|");
						else      strcpy (texto_layer,"v---------------v");

						//Centrar el nombre de capa
						int longitud_medio=strlen(nombre_capa)/2;
						int medio=strlen(texto_layer)/2;
						int pos=medio-longitud_medio;
						if (pos<0) pos=0;

						//Meter texto centrado y quitar 0 del final
						strcpy(&texto_layer[pos],nombre_capa);

						int final=strlen(texto_layer);
						texto_layer[final]='-';

						//menu_escribe_linea_opcion(linea++,-1,1,texto_layer);
						zxvision_print_string_defaults_fillspc(menu_tsconf_layer_overlay_window,1,linea++,texto_layer);

					}
				
				}		



}


void menu_tsconf_layer_overlay(void)
{

    normal_overlay_texto_menu();

 
    //esto hara ejecutar esto 2 veces por segundo
    if ( ((contador_segundo%500) == 0 && menu_tsconf_layer_valor_contador_segundo_anterior!=contador_segundo) || menu_multitarea==0) {

        menu_tsconf_layer_valor_contador_segundo_anterior=contador_segundo;
        //printf ("Refrescando. contador_segundo=%d\n",contador_segundo);
       

		menu_tsconf_layer_overlay_mostrar_texto();
		zxvision_draw_window_contents(menu_tsconf_layer_overlay_window);

    }
}


void menu_tbblue_layer_settings_sprites(MENU_ITEM_PARAMETERS)
{
	tbblue_force_disable_layer_sprites.v ^=1;
}

void menu_tbblue_layer_settings_ula(MENU_ITEM_PARAMETERS)
{
	tbblue_force_disable_layer_ula.v ^=1;
}

void menu_tbblue_layer_settings_tilemap(MENU_ITEM_PARAMETERS)
{
	tbblue_force_disable_layer_tilemap.v ^=1;
}

void menu_tbblue_layer_settings_layer_two(MENU_ITEM_PARAMETERS)
{
	tbblue_force_disable_layer_layer_two.v ^=1;
}

void menu_tbblue_layer_reveal_ula(MENU_ITEM_PARAMETERS)
{
	tbblue_reveal_layer_ula.v ^=1;
}

void menu_tbblue_layer_reveal_layer2(MENU_ITEM_PARAMETERS)
{
	tbblue_reveal_layer_layer2.v ^=1;
}

void menu_tbblue_layer_reveal_sprites(MENU_ITEM_PARAMETERS)
{
	tbblue_reveal_layer_sprites.v ^=1;
}


int menu_debug_tsconf_tbblue_msx_spritenav_get_total_sprites(void)
{
	int limite;

	limite=TBBLUE_MAX_SPRITES;

	return limite;
}


int menu_debug_tsconf_tbblue_msx_spritenav_get_total_height_win(void)
{

	return menu_debug_tsconf_tbblue_msx_spritenav_get_total_sprites()*3;

}

//Muestra lista de sprites
void menu_debug_tsconf_tbblue_msx_spritenav_lista_sprites(void)
{

	char dumpmemoria[64];

	int linea_color;
	int limite;

	int linea=0;


	limite=menu_debug_tsconf_tbblue_msx_spritenav_get_total_sprites();

	//z80_byte tbsprite_sprites[TBBLUE_MAX_SPRITES][4];
	/*
	1st: X position (bits 7-0).
2nd: Y position (0-255).
3rd: bits 7-4 is palette offset, bit 3 is X mirror, bit 2 is Y mirror, bit 1 is the rotate flag and bit 0 is X MSB.
4th: bit 7 is the visible flag, bit 6 is reserved, bits 5-0 is Name (pattern index, 0-63).
*/

	int current_sprite;


		for (linea_color=0;linea_color<limite;linea_color++) {					

			current_sprite=menu_debug_tsconf_tbblue_msx_spritenav_current_sprite+linea_color;

			{
					//z80_byte tbsprite_sprites[TBBLUE_MAX_SPRITES][4];
	/*
	1st: X position (bits 7-0).
2nd: Y position (0-255).
3rd: bits 7-4 is palette offset, bit 3 is X mirror, bit 2 is Y mirror, bit 1 is the rotate flag and bit 0 is X MSB.
4th: bit 7 is the visible flag, bit 6 is reserved, bits 5-0 is Name (pattern index, 0-63).
*/

				z80_int x=tbsprite_sprites[current_sprite][0]; //
				z80_byte y=tbsprite_sprites[current_sprite][1];  //

				z80_byte byte_3=tbsprite_sprites[current_sprite][2];
				z80_byte paloff=byte_3 & 0xF0; //
				z80_byte mirror_x=byte_3 & 8; //
				z80_byte mirror_y=byte_3 & 4; //
				z80_byte rotate=byte_3 & 2; //
				z80_byte msb_x=byte_3 &1; //

				x +=msb_x*256;

				z80_byte byte_4=tbsprite_sprites[current_sprite][3];
				z80_byte byte_5=tbsprite_sprites[current_sprite][4];
				z80_byte visible=byte_4 & 128; //
				z80_byte pattern=byte_4 & 63; //

				int sprite_es_4bpp=0;
				int offset_4bpp_N6=0;

				char buf_subindex_4_bit[10];

				int zoom_x=1;
				int zoom_y=1;

				if (byte_4 & 64) {
					//Pattern de 5 bytes
					if (byte_5 & 128) sprite_es_4bpp=1;
					if (byte_5 & 64) offset_4bpp_N6=1;

					z80_byte zoom_x_value=(byte_5>>3)&3;
					if (zoom_x_value) zoom_x <<=zoom_x_value;

					z80_byte zoom_y_value=(byte_5>>1)&3;
					if (zoom_y_value) zoom_y <<=zoom_y_value;


					//TODO: Y8

				}	

				if (sprite_es_4bpp) {
					sprintf(buf_subindex_4_bit,":%d",offset_4bpp_N6);
				}	
				else {
					strcpy(buf_subindex_4_bit,"  ");
				}			
								//012345678901234567890123456789012
								//123:1 X: 123 Y: 123 MIRX MIRY ROT
				sprintf (dumpmemoria,"%03d%s X: %3d Y: %3d %s %s %s",current_sprite,buf_subindex_4_bit,x,y,
						(mirror_x ? "MX" : "  "),(mirror_y ? "MY" : "  "),(rotate ? "ROT" : "   ")
				);				
				zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window,1,linea++,dumpmemoria);


				sprintf (dumpmemoria," Pattn: %2d Palof: %3d Vis: %s"
					,pattern,paloff, (visible ? "Yes" : "No ") );
				zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window,1,linea++,dumpmemoria);


				sprintf(dumpmemoria," %dbpp ZX: %d ZY: %d",(sprite_es_4bpp ? 4 : 8) ,zoom_x,zoom_y);
				zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window,1,linea++,dumpmemoria);				

				
			}
					
		}

	zxvision_draw_window_contents(menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window); 


}

void menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites(void)
{



				/*menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech


				//Mostrar lista sprites
				menu_debug_tsconf_tbblue_msx_spritenav_lista_sprites();

				//Esto tiene que estar despues de escribir la lista de sprites, para que se refresque y se vea
				//Si estuviese antes, al mover el cursor hacia abajo dejándolo pulsado, el texto no se vería hasta que no se soltase la tecla
				normal_overlay_texto_menu();*/


				menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech
				if (!zxvision_drawing_in_background) normal_overlay_texto_menu();
				menu_debug_tsconf_tbblue_msx_spritenav_lista_sprites();



}

void menu_debug_tsconf_tbblue_msx_spritenav(MENU_ITEM_PARAMETERS)
{
	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();

	
	//zxvision_window ventana;
    zxvision_window *ventana;
    ventana=&zxvision_window_tsconf_tbblue_spritenav;	

	//IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
	//si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
	//la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
	zxvision_delete_window_if_exists(ventana);	

	int xventana,yventana,ancho_ventana,alto_ventana;

	if (!util_find_window_geometry("tsconftbbluespritenav",&xventana,&yventana,&ancho_ventana,&alto_ventana)) {
		xventana=TSCONF_SPRITENAV_WINDOW_X;
		yventana=TSCONF_SPRITENAV_WINDOW_Y;
		ancho_ventana=TSCONF_SPRITENAV_WINDOW_ANCHO;
		alto_ventana=TSCONF_SPRITENAV_WINDOW_ALTO;
	}


	zxvision_new_window(ventana,xventana,yventana,ancho_ventana,alto_ventana,
							TSCONF_SPRITENAV_WINDOW_ANCHO-1,menu_debug_tsconf_tbblue_msx_spritenav_get_total_height_win(),"Sprite navigator");

	ventana->can_be_backgrounded=1;
	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"tsconftbbluespritenav");

	zxvision_draw_window(ventana);		

    set_menu_overlay_function(menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites);

	menu_debug_tsconf_tbblue_msx_spritenav_draw_sprites_window=ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui


       //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
       //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
       if (zxvision_currently_restoring_windows_on_start) {
               //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
               return;
       }	


	z80_byte tecla;

		//Si no esta multitarea, hacer un refresco inicial para que aparezca el contenido de la ventana sin tener que pulsar una tecla
		//dado que luego funciona como overlay, el overlay se aplica despues de hacer el render
		//esto solo es necesario para ventanas que usan overlay
	    if (!menu_multitarea) {
			//printf ("refresca pantalla inicial\n");
			menu_refresca_pantalla();
		}			

	
    do {
    	menu_speech_tecla_pulsada=0; //Que envie a speech
   		tecla=zxvision_common_getkey_refresh();
		zxvision_handle_cursors_pgupdn(ventana,tecla);
	} while (tecla!=2 && tecla!=3);  

	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	zxvision_set_window_overlay_from_current(ventana);	

	//restauramos modo normal de texto de menu
    set_menu_overlay_function(normal_overlay_texto_menu);		

    cls_menu_overlay();

	util_add_window_geometry_compact(ventana);

	if (tecla==3) {
		zxvision_message_put_window_background();
	}

	else {

		zxvision_destroy_window(ventana);
	}

}


char menu_debug_tsconf_tbblue_msx_tiles_retorna_visualchar(int tnum)
{
	//Hacer un conjunto de 64 caracteres. Mismo set de caracteres que para Base64. Por que? Por que si :)
			   //0123456789012345678901234567890123456789012345678901234567890123
	char *caracter_list="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	int index=tnum % 64;
	return caracter_list[index];
}

int menu_debug_tsconf_tbblue_msx_tilenav_total_vert(void)
{

	int limite_vertical;

	{ //TBBLUE
		limite_vertical=tbblue_get_tilemap_width()*32;

		if (menu_debug_tsconf_tbblue_msx_tilenav_showmap.v) limite_vertical=32;	
	}

	return limite_vertical;
}

//Muestra lista de tiles
void menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles(void)
{

	//Suficientemente grande para almacenar regla superior en modo visual
	//80 + 3 espacios izquierda + 0 final	
#define DEBUG_TILENAV_TEXTO_LINEA 84
	char dumpmemoria[DEBUG_TILENAV_TEXTO_LINEA]; 

	
	//int limite;

	int linea=0;
	//limite=DEBUG_TSCONF_TILENAV_MAX_TILES;

	int current_tile;

	z80_byte *puntero_tilemap;
	z80_byte *puntero_tilemap_orig;

	{  //TBBLUE
		//puntero_tilemap=tbblue_ram_mem_table[5]+tbblue_get_offset_start_tilemap();
			//Siempre saldra de ram 5
		puntero_tilemap=tbblue_ram_memory_pages[5*2]+(256*tbblue_get_offset_start_tilemap());	
		//printf ("%XH\n",tbblue_get_offset_start_tilemap() );

	}

	z80_byte tbblue_tilemap_control;
	int tilemap_width;


	int tbblue_bytes_per_tile=2;

	{
					/*
					(R/W) 0x6B (107) => Tilemap Control
  bit 7    = 1 to enable the tilemap
  bit 6    = 0 for 40x32, 1 for 80x32
  bit 5    = 1 to eliminate the attribute entry in the tilemap
  bit 4    = palette select
  bits 3-0 = Reserved set to 0
					*/
					tbblue_tilemap_control=tbblue_registers[107];

					if (tbblue_tilemap_control&32) tbblue_bytes_per_tile=1;

					tilemap_width=tbblue_get_tilemap_width();

	}

	puntero_tilemap_orig=puntero_tilemap;

	int limite_vertical=menu_debug_tsconf_tbblue_msx_tilenav_total_vert();


	int offset_vertical=0;

	if (menu_debug_tsconf_tbblue_msx_tilenav_showmap.v) {
		{ //TBBLUE
			if (tilemap_width==40) {
				             //0123456789012345678901234567890123456789012345678901234567890123
		strcpy(dumpmemoria,"   0    5    10   15   20   25   30   35                                           ");
			}
			else {
				             //01234567890123456789012345678901234567890123456789012345678901234567890123456789
		strcpy(dumpmemoria,"   0    5    10   15   20   25   30   35   40   45   50   55   60   65   70   75   ");
			}

		}

		//Indicar codigo 0 de final
		//dumpmemoria[current_tile_x+TSCONF_TILENAV_TILES_HORIZ_PER_WINDOW+3]=0;  //3 espacios al inicio

		//menu_escribe_linea_opcion(linea++,-1,1,&dumpmemoria[current_tile_x]); //Mostrar regla superior
		zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window,1,0,dumpmemoria);
	}
	else {
		//Aumentarlo en cuanto al offset que estamos (si modo lista)

		int offset_y=menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window->offset_y;
		

		offset_vertical=offset_y/2;
		linea=offset_vertical*2;



		limite_vertical=offset_vertical+((24-2)/2)+1; //El maximo que cabe en pantalla, +1 para cuando se baja 1 posicion con cursor

	}

	//linea destino es +3, pues las tres primeras son de leyenda
	linea +=3;	



		for (;offset_vertical<limite_vertical;offset_vertical++) {

				//texto linea inicializarlo siempre con espacios
				//#define DEBUG_TILENAV_TEXTO_LINEA 84
				//char dumpmemoria[DEBUG_TILENAV_TEXTO_LINEA]; 
				int i;
				for (i=0;i<DEBUG_TILENAV_TEXTO_LINEA-1;i++) {
					dumpmemoria[i]=' ';
				}

				//Y 0 del final
				dumpmemoria[i]=0;			

			int repetir_ancho=1;
			int mapa_tile_x=3;
			if (menu_debug_tsconf_tbblue_msx_tilenav_showmap.v==0) {
				//Modo lista tiles
				current_tile=offset_vertical;
			}

			else {
				//Modo mapa tiles
				{ //TBBLUE
					current_tile=offset_vertical*tilemap_width;
					repetir_ancho=tilemap_width;

					//poner regla vertical
					int linea_tile=current_tile/tilemap_width;
					if ( (linea_tile%5)==0) sprintf (dumpmemoria,"%2d ",linea_tile);
					else sprintf (dumpmemoria,"   ");				
				}
			}

			//printf ("linea: %3d current tile: %10d puntero: %10d\n",linea_color,current_tile,puntero_tilemap-tsconf_ram_mem_table[0]-tsconf_return_tilemappage()	);

			do {



				{

					int y=current_tile/tilemap_width;
					int x=current_tile%tilemap_width; 

					int offset=(tilemap_width*tbblue_bytes_per_tile*y)+(x*tbblue_bytes_per_tile);
					/*
					 bits 15-12 : palette offset
  bit     11 : x mirror
  bit     10 : y mirror
  bit      9 : rotate
  bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
  bits   7-0 : tile number
					*/

					int xmirror,ymirror,rotate;
					z80_byte tpal;

					z80_byte byte_first;
					z80_byte byte_second;

					byte_first=puntero_tilemap[offset];
					byte_second=puntero_tilemap[offset+1];					

					int tnum=byte_first;
					int ula_over_tilemap=0;

					z80_byte tbblue_default_tilemap_attr=tbblue_registers[108];

					if (tbblue_bytes_per_tile==1) {
						/*
						(R/W) 0x6C (108) => Default Tilemap Attribute
  bits 7-4 = Palette Offset
  bit 3    = X mirror
  bit 2    = Y mirror
  bit 1    = Rotate
  bit 0    = ULA over tilemap
             (bit 8 of tile id if the ULA is disabled)	
			 			*/
					 	tpal=(tbblue_default_tilemap_attr>>4)&15;
						xmirror=(tbblue_default_tilemap_attr>>3)&1;
						ymirror=(tbblue_default_tilemap_attr>>2)&1;
						rotate=(tbblue_default_tilemap_attr>>1)&1;

						if (tbblue_if_ula_is_enabled() ) {
						/*
						108
						  bit 0    = ULA over tilemap
             (bit 8 of tile id if the ULA is disabled)
						*/							
							ula_over_tilemap=tbblue_default_tilemap_attr &1;
						}

						else {
							tnum |=(tbblue_default_tilemap_attr&1)<<8; // bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
						}
						
					}

					else {
						/*
							
					 bits 15-12 : palette offset
  bit     11 : x mirror
  bit     10 : y mirror
  bit      9 : rotate
  bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
					*/	
					 	tpal=(byte_second>>4)&15;
						xmirror=(byte_second>>3)&1;
						ymirror=(byte_second>>2)&1;
						rotate=(byte_second>>1)&1;
						//ula_over_tilemap=byte_second &1;

					if (tbblue_if_ula_is_enabled() ) {
						/*
						  bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
						*/							
							ula_over_tilemap=byte_second &1;
						}

						else {
							tnum |=(byte_second&1)<<8; // bit      8 : ULA over tilemap (if the ula is disabled, bit 8 of tile number)
						}


					}

					//printf ("tnum: %d\n",tnum);


					if (menu_debug_tsconf_tbblue_msx_tilenav_showmap.v==0) {
						//Modo lista tiles
						sprintf (dumpmemoria,"X: %3d Y: %3d                   ",x,y);

						zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window,1,linea++,dumpmemoria);

						sprintf (dumpmemoria," Tile: %3d %s %s %s %s P:%2d ",tnum,
							(xmirror ? "MX" : "  "),(ymirror ? "MY": "  "),
							(rotate ? "R" : " "),(ula_over_tilemap ? "U": " "),
							tpal );

						zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window,1,linea++,dumpmemoria);
					}
					else {
						//Modo mapa tiles
						int caracter_final;

						if (tnum==0) {
							caracter_final=' '; 
						}
						else {
							caracter_final=menu_debug_tsconf_tbblue_msx_tiles_retorna_visualchar(tnum);
						}

						dumpmemoria[mapa_tile_x++]=caracter_final;
					}					


				}

				current_tile++;

				repetir_ancho--;
			} while (repetir_ancho);

			if (menu_debug_tsconf_tbblue_msx_tilenav_showmap.v) {
				zxvision_print_string_defaults(menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window,1,linea++,dumpmemoria);
				puntero_tilemap=puntero_tilemap_orig;
			}
					
		}



	//return linea;

	zxvision_draw_window_contents(menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window); 
}

void menu_debug_tsconf_tbblue_msx_tilenav_draw_tiles(void)
{
/*
				menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech


				//Mostrar lista tiles
				menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles();

				//Esto tiene que estar despues de escribir la lista de tiles, para que se refresque y se vea
				//Si estuviese antes, al mover el cursor hacia abajo dejándolo pulsado, el texto no se vería hasta que no se soltase la tecla
				normal_overlay_texto_menu();
				*/



				menu_speech_tecla_pulsada=1; //Si no, envia continuamente todo ese texto a speech
				if (!zxvision_drawing_in_background) normal_overlay_texto_menu();
				menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles();				

}



void menu_debug_tsconf_tbblue_msx_tilenav_new_window(zxvision_window *ventana)
{

		char titulo[33];
		
		char linea_leyenda[64];
		sprintf (titulo,"Tile Navigator");

        //Forzar a mostrar atajos
        z80_bit antes_menu_writing_inverse_color;
        antes_menu_writing_inverse_color.v=menu_writing_inverse_color.v;
        menu_writing_inverse_color.v=1;		

		int total_height=menu_debug_tsconf_tbblue_msx_tilenav_total_vert();
		int total_width=31;

		char texto_layer[32];

		//En caso de tbblue y msx, solo hay una capa
		texto_layer[0]=0;

		if (menu_debug_tsconf_tbblue_msx_tilenav_showmap.v) {
			sprintf (linea_leyenda,"~~Mode: Visual %s",texto_layer);

			{
				//TBBLUE
				total_width=tbblue_get_tilemap_width()+4;
			}

		}

		else {
			sprintf (linea_leyenda,"~~Mode: List %s",texto_layer);
			total_height*=2;
		}

		//tres mas para ubicar las lineas de leyenda
		total_height+=3;


		int xventana,yventana,ancho_ventana,alto_ventana;

		if (!util_find_window_geometry("tsconftbbluetilenav",&xventana,&yventana,&ancho_ventana,&alto_ventana)) {
			xventana=TSCONF_TILENAV_WINDOW_X;
			yventana=TSCONF_TILENAV_WINDOW_Y;
			ancho_ventana=TSCONF_TILENAV_WINDOW_ANCHO;
			alto_ventana=TSCONF_TILENAV_WINDOW_ALTO;
		}

		zxvision_new_window(ventana,xventana,yventana,
							ancho_ventana,alto_ventana,
							total_width,total_height,titulo);


		//Establecer leyenda en la parte de abajo
		ventana->lower_margin=2;
		//Texto sera el de la primera linea
		ventana->upper_margin=1;

		ventana->can_be_backgrounded=1;
		//indicar nombre del grabado de geometria
		strcpy(ventana->geometry_name,"tsconftbbluetilenav");

		//Permitir hotkeys desde raton
		ventana->can_mouse_send_hotkeys=1;			
		
		//Leyenda inferior
		//zxvision_print_string_defaults_fillspc(ventana,1,1,"-----");
		zxvision_print_string_defaults_fillspc(ventana,1,2,linea_leyenda);

		zxvision_draw_window(ventana);	

        //Restaurar comportamiento atajos
        menu_writing_inverse_color.v=antes_menu_writing_inverse_color.v;
		//Nota: los atajos se "pintan" en la memoria de la ventana ya con el color inverso
		//por tanto con meter al principio la variable de inverse_color es suficiente
		//y no hay que activar inverse color cada vez que se redibuja ventana,
		//pues al redibujar ventana está leyendo el contenido de la memoria de la ventana, y ahí ya está con color inverso		

}


void menu_debug_tsconf_tbblue_msx_save_geometry(zxvision_window *ventana)
{
	util_add_window_geometry_compact(ventana);
}


void menu_debug_tsconf_tbblue_msx_tilenav(MENU_ITEM_PARAMETERS)
{

	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();

	
	zxvision_window *ventana;
	ventana=&zxvision_window_tsconf_tbblue_tilenav;

    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
    zxvision_delete_window_if_exists(ventana);	

	menu_debug_tsconf_tbblue_msx_tilenav_new_window(ventana);

	set_menu_overlay_function(menu_debug_tsconf_tbblue_msx_tilenav_draw_tiles);

	menu_debug_tsconf_tbblue_msx_tilenav_lista_tiles_window=ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui


       //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
       //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
       if (zxvision_currently_restoring_windows_on_start) {
               //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
               return;
       }	


	z80_byte tecla;

	//Si no esta multitarea, hacer un refresco inicial para que aparezca el contenido de la ventana sin tener que pulsar una tecla
	//dado que luego funciona como overlay, el overlay se aplica despues de hacer el render
	//esto solo es necesario para ventanas que usan overlay
	if (!menu_multitarea) {
		//printf ("refresca pantalla inicial\n");
		menu_refresca_pantalla();
	}				


	do {
    	menu_speech_tecla_pulsada=0; //Que envie a speech

		tecla=zxvision_common_getkey_refresh();				

        
		switch (tecla) {

			case 'l':
			break;

			case 'm':
				menu_debug_tsconf_tbblue_msx_save_geometry(ventana);
				zxvision_destroy_window(ventana);		
				menu_debug_tsconf_tbblue_msx_tilenav_showmap.v ^=1;
				menu_debug_tsconf_tbblue_msx_tilenav_new_window(ventana);

			break;


			default:
				zxvision_handle_cursors_pgupdn(ventana,tecla);
			break;
		}		

		

	} while (tecla!=2 && tecla!=3); 

	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	zxvision_set_window_overlay_from_current(ventana);	

	//restauramos modo normal de texto de menu
    set_menu_overlay_function(normal_overlay_texto_menu);		

    cls_menu_overlay();

    //Grabar geometria ventana. Usamos funcion auxiliar pues la llamamos tambien al cambiar de modo y layer
	menu_debug_tsconf_tbblue_msx_save_geometry(ventana);


    if (tecla==3) {
		zxvision_message_put_window_background();
	}

	else {

		zxvision_destroy_window(ventana);
	}



}


void menu_uartbridge_file(MENU_ITEM_PARAMETERS)
{
	uartbridge_disable();

        char *filtros[2];

        filtros[0]="";
        filtros[1]=0;


        if (menu_filesel("Select Device File",filtros,uartbridge_name)==1) {
                if (!si_existe_archivo(uartbridge_name)) {
                        menu_error_message("File does not exist");
                        uartbridge_name[0]=0;
                        return;
                }


        }
        //Sale con ESC
        else {
                //Quitar nombre
                uartbridge_name[0]=0;


        }

}


void menu_uartbridge_enable(MENU_ITEM_PARAMETERS)
{
	if (uartbridge_enabled.v) uartbridge_disable();
	else uartbridge_enable();
}


int menu_uartbridge_cond(void)
{
	if (uartbridge_name[0]==0) return 0;

	else return 1;

}


int menu_uartbridge_speed_cond(void)
{
	if (uartbridge_enabled.v) return 0;

	else return 1;

}


void menu_uartbridge_speed(MENU_ITEM_PARAMETERS)
{
	if (uartbridge_speed==CHDEV_SPEED_115200) uartbridge_speed=CHDEV_SPEED_DEFAULT;
	else uartbridge_speed++;
}


void menu_audio_beep_filter_on_rom_save(MENU_ITEM_PARAMETERS)
{
	output_beep_filter_on_rom_save.v ^=1;
}


void menu_audio_beep_alter_volume(MENU_ITEM_PARAMETERS)
{
	output_beep_filter_alter_volume.v ^=1;
}


void menu_audio_beep_volume(MENU_ITEM_PARAMETERS)
{

        char string_vol[6];

        sprintf (string_vol,"%d",output_beep_filter_volume);


        menu_ventana_scanf("Volume (0-127)",string_vol,4);

        int v=parse_string_to_number(string_vol);

        if (v>127 || v<0) {
                debug_printf (VERBOSE_ERR,"Invalid volume value");
                return;
        }

        output_beep_filter_volume=v;
}

void menu_audio_beeper (MENU_ITEM_PARAMETERS)
{
	beeper_enabled.v ^=1;
}

void menu_audio_beeper_real (MENU_ITEM_PARAMETERS)
{
	beeper_real_enabled ^=1;
}

void menu_audio_volume(MENU_ITEM_PARAMETERS)
{
	menu_ventana_scanf_numero_enhanced("Volume in %",&audiovolume,4,+20,0,100,0);
}

void menu_aofile_insert(MENU_ITEM_PARAMETERS)
{

	if (aofile_inserted.v==0) {
		init_aofile();

		//Si todo ha ido bien
		if (aofile_inserted.v) {
			menu_generic_message_format("File information","%s\n%s\n\n%s",
			last_message_helper_aofile_vofile_file_format,last_message_helper_aofile_vofile_bytes_minute_audio,last_message_helper_aofile_vofile_util);
		}

	}

        else if (aofile_inserted.v==1) {
                close_aofile();
        }

}

int menu_aofile_cond(void)
{
	if (aofilename!=NULL) return 1;
	else return 0;
}

void menu_aofile(MENU_ITEM_PARAMETERS)
{

	aofile_inserted.v=0;


        char *filtros[3];

#ifdef USE_SNDFILE
        filtros[0]="rwa";
        filtros[1]="wav";
        filtros[2]=0;
#else
        filtros[0]="rwa";
        filtros[1]=0;
#endif


        if (menu_filesel("Select Audio File",filtros,aofilename_file)==1) {

       	        if (si_existe_archivo(aofilename_file)) {

               	        if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) {
				aofilename=NULL;
				return;
			}

       	        }

                aofilename=aofilename_file;
        }

	else {
		aofilename=NULL;
	}
}


void menu_pcspeaker_wait_time(MENU_ITEM_PARAMETERS)
{


        char string_time[3];

        sprintf (string_time,"%d",audiopcspeaker_tiempo_espera);

        menu_ventana_scanf("Wait time (0-64)",string_time,3);

        int valor_tiempo=parse_string_to_number(string_time);

        if (valor_tiempo<0 || valor_tiempo>64) {
                                        debug_printf (VERBOSE_ERR,"Invalid value %d",valor_tiempo);
                                        return;
        }

        audiopcspeaker_tiempo_espera=valor_tiempo;
}


void menu_pcspeaker_auto_calibrate_wait_time(MENU_ITEM_PARAMETERS)
{
//Esta funcion solo disponible si se compila audiopcspeaker
#ifdef COMPILE_PCSPEAKER
	audiopcspeaker_calibrate_tiempo_espera();
#endif
	salir_todos_menus=1;
}


void menu_cpu_type(MENU_ITEM_PARAMETERS)
{
	z80_cpu_current_type++;
	if (z80_cpu_current_type>=TOTAL_Z80_CPU_TYPES) z80_cpu_current_type=0;
}


void menu_debug_registers_console(MENU_ITEM_PARAMETERS) {
	debug_registers ^=1;
}


void menu_debug_shows_invalid_opcode(MENU_ITEM_PARAMETERS)
{
	debug_shows_invalid_opcode.v ^=1;
}


void menu_debug_configuration_stepover(MENU_ITEM_PARAMETERS)
{
	//debug_core_evitamos_inter.v ^=1;
	remote_debug_settings ^=32;
}


void menu_debug_settings_show_screen(MENU_ITEM_PARAMETERS)
{
	debug_settings_show_screen.v ^=1;
}


void menu_debug_settings_show_scanline(MENU_ITEM_PARAMETERS)
{
	menu_debug_registers_if_showscan.v ^=1;
}


void menu_debug_verbose(MENU_ITEM_PARAMETERS)
{
	verbose_level++;
	if (verbose_level>4) verbose_level=0;
}


void menu_debug_unnamed_console_enable(MENU_ITEM_PARAMETERS)
{

    if (debug_unnamed_console_enabled.v) {
        debug_unnamed_console_end();
        debug_unnamed_console_enabled.v=0;
    }

    else {
        debug_unnamed_console_enabled.v=1;
        debug_unnamed_console_init();
    }

}


void menu_debug_verbose_always_console(MENU_ITEM_PARAMETERS)
{
	debug_always_show_messages_in_console.v ^=1;
}


void menu_debug_configuration_remoteproto(MENU_ITEM_PARAMETERS)
{
	if (remote_protocol_enabled.v) {
		end_remote_protocol();
		remote_protocol_enabled.v=0;
	}

	else {
		enable_and_init_remote_protocol();
	}
}


void menu_debug_configuration_remoteproto_port(MENU_ITEM_PARAMETERS)
{
	char string_port[6];
	int port;

	sprintf (string_port,"%d",remote_protocol_port);

	menu_ventana_scanf("Port",string_port,6);

	if (string_port[0]==0) return;

	else {
			port=parse_string_to_number(string_port);

			if (port<1 || port>65535) {
								debug_printf (VERBOSE_ERR,"Invalid port %d",port);
								return;
			}


			end_remote_protocol();
			remote_protocol_port=port;
			init_remote_protocol();
	}

}


void menu_hardware_debug_port(MENU_ITEM_PARAMETERS)
{
	hardware_debug_port.v ^=1;
}


void menu_zesarux_zxi_hardware_debug_file(MENU_ITEM_PARAMETERS)
{

	char *filtros[2];

    filtros[0]="";
    filtros[1]=0;


    if (menu_filesel("Select Debug File",filtros,zesarux_zxi_hardware_debug_file)==1) {
    	//Ver si archivo existe y preguntar
		if (si_existe_archivo(zesarux_zxi_hardware_debug_file)) {
            if (menu_confirm_yesno_texto("File exists","Append?")==0) {
				zesarux_zxi_hardware_debug_file[0]=0;
				return;
			}
        }

    }

	else zesarux_zxi_hardware_debug_file[0]=0;

}


void menu_breakpoints_condition_behaviour(MENU_ITEM_PARAMETERS)
{
	debug_breakpoints_cond_behaviour.v ^=1;
}


void menu_debug_settings_show_fired_breakpoint(MENU_ITEM_PARAMETERS)
{
	debug_show_fired_breakpoints_type++;
	if (debug_show_fired_breakpoints_type==3) debug_show_fired_breakpoints_type=0;
}


void menu_display_rainbow(MENU_ITEM_PARAMETERS)
{
	if (rainbow_enabled.v==0) enable_rainbow();
	else disable_rainbow();


}


void menu_display_tbblue_store_scanlines(MENU_ITEM_PARAMETERS)
{
	tbblue_store_scanlines.v ^=1;
}


void menu_display_tbblue_store_scanlines_border(MENU_ITEM_PARAMETERS)
{
	tbblue_store_scanlines_border.v ^=1;
}


void menu_hardware_joystick(MENU_ITEM_PARAMETERS)
{
	joystick_cycle_next_type();
}


void menu_hardware_autofire(MENU_ITEM_PARAMETERS)
{
	if (joystick_autofire_frequency==0) joystick_autofire_frequency=1;
	else if (joystick_autofire_frequency==1) joystick_autofire_frequency=2;
	else if (joystick_autofire_frequency==2) joystick_autofire_frequency=5;
	else if (joystick_autofire_frequency==5) joystick_autofire_frequency=10;
	else if (joystick_autofire_frequency==10) joystick_autofire_frequency=25;
	else if (joystick_autofire_frequency==25) joystick_autofire_frequency=50;

	else joystick_autofire_frequency=0;
}


void menu_hardware_kempston_mouse(MENU_ITEM_PARAMETERS)
{
	kempston_mouse_emulation.v ^=1;
}


void menu_hardware_kempston_mouse_sensibilidad(MENU_ITEM_PARAMETERS)
{
	//kempston_mouse_factor_sensibilidad++;
	//if (kempston_mouse_factor_sensibilidad==6) kempston_mouse_factor_sensibilidad=1;
	char titulo_ventana[33];
	sprintf (titulo_ventana,"Sensitivity (1-%d)",MAX_KMOUSE_SENSITIVITY);

	menu_hardware_advanced_input_value(1,MAX_KMOUSE_SENSITIVITY,titulo_ventana,&kempston_mouse_factor_sensibilidad);
}


void menu_hardware_datagear_dma(MENU_ITEM_PARAMETERS)
{
	if (datagear_dma_emulation.v) datagear_dma_disable();
	else datagear_dma_enable();
}


void menu_hardware_tbblue_core_version(MENU_ITEM_PARAMETERS)
{
	char string_value[4];

	int valor;

	sprintf (string_value,"%d",tbblue_core_current_version_major);
	//Entre 0 y 255
	menu_ventana_scanf("Major",string_value,4);
	valor=parse_string_to_number(string_value);
	if (valor<0 || valor>255) {
		debug_printf (VERBOSE_ERR,"Invalid value");
		return;
	}

	tbblue_core_current_version_major=valor;


	sprintf (string_value,"%d",tbblue_core_current_version_minor);
	//Entre 0 y 15
	menu_ventana_scanf("Minor",string_value,3);
	valor=parse_string_to_number(string_value);
	if (valor<0 || valor>15) {
		debug_printf (VERBOSE_ERR,"Invalid value");
		return;
	}

	tbblue_core_current_version_minor=valor;

	sprintf (string_value,"%d",tbblue_core_current_version_subminor);
	//Entre 0 y 15
	menu_ventana_scanf("Subminor",string_value,3);
	valor=parse_string_to_number(string_value);
	if (valor<0 || valor>15) {
		debug_printf (VERBOSE_ERR,"Invalid value");
		return;
	}
	
	tbblue_core_current_version_subminor=valor;	


}


void menu_tbblue_rtc_traps(MENU_ITEM_PARAMETERS)
{
	tbblue_use_rtc_traps ^=1;
}


void menu_hardware_keyboard_issue(MENU_ITEM_PARAMETERS)
{
	keyboard_issue2.v^=1;
}


void menu_hardware_redefine_keys_set_keys(MENU_ITEM_PARAMETERS)
{
        z80_byte tecla_original,tecla_redefinida;
	tecla_original=lista_teclas_redefinidas[hardware_redefine_keys_opcion_seleccionada].tecla_original;
	tecla_redefinida=lista_teclas_redefinidas[hardware_redefine_keys_opcion_seleccionada].tecla_redefinida;

        char buffer_caracter_original[2];
	char buffer_caracter_redefinida[2];

	if (tecla_original==0) {
		buffer_caracter_original[0]=0;
		buffer_caracter_redefinida[0]=0;
	}
	else {
		buffer_caracter_original[0]=(tecla_original>=32 && tecla_original <=127 ? tecla_original : '?');
		buffer_caracter_redefinida[0]=(tecla_redefinida>=32 && tecla_redefinida <=127 ? tecla_redefinida : '?');
	}

        buffer_caracter_original[1]=0;
        buffer_caracter_redefinida[1]=0;

        menu_ventana_scanf("Original key",buffer_caracter_original,2);
        tecla_original=buffer_caracter_original[0];

	if (tecla_original==0) {
		lista_teclas_redefinidas[hardware_redefine_keys_opcion_seleccionada].tecla_original=0;
		return;
	}

        menu_ventana_scanf("Destination key",buffer_caracter_redefinida,2);
        tecla_redefinida=buffer_caracter_redefinida[0];

	if (tecla_redefinida==0) {
                lista_teclas_redefinidas[hardware_redefine_keys_opcion_seleccionada].tecla_original=0;
                return;
        }

	lista_teclas_redefinidas[hardware_redefine_keys_opcion_seleccionada].tecla_original=tecla_original;
	lista_teclas_redefinidas[hardware_redefine_keys_opcion_seleccionada].tecla_redefinida=tecla_redefinida;
}


void menu_hardware_set_f_func_action(MENU_ITEM_PARAMETERS)
{
        hardware_set_f_func_action_opcion_seleccionada=defined_f_functions_keys_array[valor_opcion];


        menu_item *array_menu_hardware_set_f_func_action;
        menu_item item_seleccionado;
        int retorno_menu;
        //do {

                char buffer_texto[40];

                int i;
                for (i=0;i<MAX_F_FUNCTIONS;i++) {

                  //enum defined_f_function_ids accion=defined_f_functions_keys_array[i];

                  sprintf (buffer_texto,"%s",defined_f_functions_array[i].texto_funcion);


                        if (i==0) menu_add_item_menu_inicial_format(&array_menu_hardware_set_f_func_action,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto);
                        else menu_add_item_menu_format(array_menu_hardware_set_f_func_action,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto);

												}


                menu_add_item_menu(array_menu_hardware_set_f_func_action,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                //menu_add_item_menu(array_menu_hardware_set_f_func_action,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_set_f_func_action);

                retorno_menu=menu_dibuja_menu(&hardware_set_f_func_action_opcion_seleccionada,&item_seleccionado,array_menu_hardware_set_f_func_action,"Set F keys" );

								if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {

												//Si se pulsa Enter
												int indice=hardware_set_f_func_action_opcion_seleccionada;
												defined_f_functions_keys_array[valor_opcion]=indice;

												
                }
}


int menu_hardware_realjoystick_cond(void)
{
	return realjoystick_present.v;
}


void menu_hardware_realjoystick_event_button(MENU_ITEM_PARAMETERS)
{



    menu_simple_ventana("Redefine event","Please press the button/axis");
	menu_refresca_pantalla();

	//Para xwindows hace falta esto, sino no refresca
	 scr_actualiza_tablas_teclado();

        //redefinir evento
        if (!realjoystick_redefine_event(hardware_realjoystick_event_opcion_seleccionada)) {
		//se ha salido con tecla. ver si es ESC
		if ((puerto_especial1&1)==0) {
			//desasignar evento
			realjoystick_events_array[hardware_realjoystick_event_opcion_seleccionada].asignado.v=0;
		}
	}

        cls_menu_overlay();
}


//Retorna <0 si salir con ESC
int menu_joystick_event_list(void)
{
      
        menu_item *array_menu_joystick_event_list;
        menu_item item_seleccionado;
        int retorno_menu;

        int joystick_event_list_opcion_seleccionada=0;
       

                char buffer_texto[40];

                int i;
                for (i=0;i<MAX_EVENTS_JOYSTICK;i++) {

                  //enum defined_f_function_ids accion=defined_f_functions_keys_array[i];

                  sprintf (buffer_texto,"%s",realjoystick_event_names[i]);


                    if (i==0) menu_add_item_menu_inicial_format(&array_menu_joystick_event_list,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto);
                     else menu_add_item_menu_format(array_menu_joystick_event_list,MENU_OPCION_NORMAL,NULL,NULL,buffer_texto);

				}



                menu_add_item_menu(array_menu_joystick_event_list,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                menu_add_ESC_item(array_menu_joystick_event_list);

                retorno_menu=menu_dibuja_menu(&joystick_event_list_opcion_seleccionada,&item_seleccionado,array_menu_joystick_event_list,"Select event" );

                


								if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {

												//Si se pulsa Enter
												return joystick_event_list_opcion_seleccionada;
			
                                }

                                else return -1;

}



void menu_hardware_realjoystick_keys_button(MENU_ITEM_PARAMETERS)
{


	//int menu_simple_two_choices(char *texto_ventana,char *texto_interior,char *opcion1,char *opcion2)
	int tipo=menu_simple_two_choices("Selection type","You want to set by","Button","Event");

	if (tipo==0) return; //ESC


        z80_byte caracter;

        char buffer_caracter[2];
        buffer_caracter[0]=0;

        menu_ventana_scanf("Please write the key",buffer_caracter,2);


        caracter=buffer_caracter[0];

        if (caracter==0) {
		//desasignamos
		realjoystick_keys_array[hardware_realjoystick_keys_opcion_seleccionada].asignado.v=0;
		return;
	}



	cls_menu_overlay();

	if (tipo==1) { //Definir por boton

			//Uso una simple ventana dado que por zxvision_window no puedo dejarla ahi de fondo y leer el joystick
			//TODO: probar esto porque quiza si se puede...
        	menu_simple_ventana("Redefine key","Please press the button/axis");
        	menu_refresca_pantalla();

        	//Para xwindows hace falta esto, sino no refresca
        	scr_actualiza_tablas_teclado();

        	//redefinir boton a tecla
        	if (!realjoystick_redefine_key(hardware_realjoystick_keys_opcion_seleccionada,caracter)) {
			//se ha salido con tecla. ver si es ESC
        	        if ((puerto_especial1&1)==0) {
                	        //desasignar evento
				realjoystick_keys_array[hardware_realjoystick_keys_opcion_seleccionada].asignado.v=0;
                	}
        	}
        }

        if (tipo==2) { //Definir por evento
        	int evento=menu_joystick_event_list();
        	 realjoystick_copy_event_button_key(evento,hardware_realjoystick_keys_opcion_seleccionada,caracter);
        	//printf ("evento: %d\n",evento);
        }


	//Ya que estamos usando una simple_ventana en vez de zxwindow_vision
	cls_menu_overlay();
	menu_refresca_pantalla();

}

void menu_print_text_axis(char *buffer,int button_type,int button_number)
{

	char buffer_axis[2];

	if (button_type==0) sprintf (buffer_axis,"%s","");
                                        //este sprintf se hace asi para evitar warnings al compilar

	if (button_type<0) sprintf (buffer_axis,"-");
	if (button_type>0) sprintf (buffer_axis,"+");

	sprintf(buffer,"%s%d",buffer_axis,button_number);

}


void menu_hardware_realjoystick_clear_keys(MENU_ITEM_PARAMETERS)
{
        if (menu_confirm_yesno_texto("Clear list","Sure?")==1) {
                realjoystick_clear_keys_array();
        }
}


void menu_hardware_realjoystick_keys(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_realjoystick_keys;
        menu_item item_seleccionado;
        int retorno_menu;
        do {

                char buffer_texto[40];
                char buffer_texto_boton[10];

                int i;
                for (i=0;i<MAX_KEYS_JOYSTICK;i++) {
                        if (realjoystick_keys_array[i].asignado.v) {
				menu_print_text_axis(buffer_texto_boton,realjoystick_keys_array[i].button_type,realjoystick_keys_array[i].button);

				z80_byte c=realjoystick_keys_array[i].caracter;
				if (c>=32 && c<=126) sprintf (buffer_texto,"Button %s sends [%c]",buffer_texto_boton,c);
				else sprintf (buffer_texto,"Button %s sends [(%d)]",buffer_texto_boton,c);
                        }

                        else {
                                sprintf(buffer_texto,"Unused entry");
			}



                        if (i==0) menu_add_item_menu_inicial_format(&array_menu_hardware_realjoystick_keys,MENU_OPCION_NORMAL,menu_hardware_realjoystick_keys_button,NULL,buffer_texto);
                        else menu_add_item_menu_format(array_menu_hardware_realjoystick_keys,MENU_OPCION_NORMAL,menu_hardware_realjoystick_keys_button,NULL,buffer_texto);


                        menu_add_item_menu_tooltip(array_menu_hardware_realjoystick_keys,"Redefine the button");
                        menu_add_item_menu_ayuda(array_menu_hardware_realjoystick_keys,"Indicates which key on the Spectrum keyboard is sent when "
						"pressed the button/axis on the real joystick");
                }

                menu_add_item_menu(array_menu_hardware_realjoystick_keys,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_item_menu_format(array_menu_hardware_realjoystick_keys,MENU_OPCION_NORMAL,menu_hardware_realjoystick_clear_keys,NULL,"Clear list");


                menu_add_item_menu(array_menu_hardware_realjoystick_keys,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                //menu_add_item_menu(array_menu_hardware_realjoystick_keys,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_realjoystick_keys);

                retorno_menu=menu_dibuja_menu(&hardware_realjoystick_keys_opcion_seleccionada,&item_seleccionado,array_menu_hardware_realjoystick_keys,"Joystick to keys" );

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                                
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}



void menu_hardware_realjoystick_clear_events(MENU_ITEM_PARAMETERS)
{
	if (menu_confirm_yesno_texto("Clear list","Sure?")==1) {
		realjoystick_clear_events_array();
	}
}


void menu_hardware_realjoystick_event(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_hardware_realjoystick_event;
        menu_item item_seleccionado;
        int retorno_menu;
        do {

                char buffer_texto[40];
                char buffer_texto_boton[10];

                int i;
                for (i=0;i<MAX_EVENTS_JOYSTICK;i++) {
                        if (realjoystick_events_array[i].asignado.v) {
				menu_print_text_axis(buffer_texto_boton,realjoystick_events_array[i].button_type,realjoystick_events_array[i].button);
                        }

                        else {
                                sprintf(buffer_texto_boton,"None");
                        }

                        sprintf (buffer_texto,"Button for %s [%s]",realjoystick_event_names[i],buffer_texto_boton);


                        if (i==0) menu_add_item_menu_inicial_format(&array_menu_hardware_realjoystick_event,MENU_OPCION_NORMAL,menu_hardware_realjoystick_event_button,NULL,buffer_texto);
                        else menu_add_item_menu_format(array_menu_hardware_realjoystick_event,MENU_OPCION_NORMAL,menu_hardware_realjoystick_event_button,NULL,buffer_texto);


                        menu_add_item_menu_tooltip(array_menu_hardware_realjoystick_event,"Redefine the action");
                        menu_add_item_menu_ayuda(array_menu_hardware_realjoystick_event,"Redefine the action");
                }

                menu_add_item_menu(array_menu_hardware_realjoystick_event,"",MENU_OPCION_SEPARADOR,NULL,NULL);
		menu_add_item_menu_format(array_menu_hardware_realjoystick_event,MENU_OPCION_NORMAL,menu_hardware_realjoystick_clear_events,NULL,"Clear list");


                menu_add_item_menu(array_menu_hardware_realjoystick_event,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                //menu_add_item_menu(array_menu_hardware_realjoystick_event,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_hardware_realjoystick_event);

                retorno_menu=menu_dibuja_menu(&hardware_realjoystick_event_opcion_seleccionada,&item_seleccionado,array_menu_hardware_realjoystick_event,"Joystick to events" );

                

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                                
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}


void menu_hardware_realjoystick_test_reset_last_values(void)
{
	menu_info_joystick_last_button=-1;
	menu_info_joystick_last_type=-1;
	menu_info_joystick_last_value=-1;
	menu_info_joystick_last_index=-1;
	menu_info_joystick_last_raw_value=-1;
}


void menu_hardware_realjoystick_test_fill_bars(int valor,char *string,int limite_barras)
{
	//Limitar valor entre -32767 y 32767
	if (valor>32767) valor=32767;
	if (valor<-32767) valor=-32767;

	//Cuantas barras hay que hacer
	int barras=(valor*limite_barras)/32767;

	if (barras<0) barras=-barras;

	//String inicial
	int i;
	for (i=0;i<limite_barras;i++) {
		string[i]='-';
		string[i+limite_barras+1]='-';
	}

	//Medio
	string[i]='|';

	//Final
	string[i+limite_barras+1]=0;


	//Y ahora llenar hacia la izquierda o derecha
	int signo=+1;
	if (valor<0) signo=-1;
	int indice=limite_barras+signo; //Nos posicionamos a la derecha o izquierda de la barra central
	for (;barras;barras--) {
		string[indice]='=';
		indice +=signo;
	}

}


void menu_hardware_realjoystick_test(MENU_ITEM_PARAMETERS)
{

	menu_espera_no_tecla();
    

	zxvision_window ventana;

	int alto_ventana=REALJOYSTICK_TEST_ALTO;
	int ancho_ventana=REALJOYSTICK_TEST_ANCHO;	
	int x_ventana=menu_center_x()-ancho_ventana/2; 
	int y_ventana=menu_center_y()-alto_ventana/2; 	

	zxvision_new_window(&ventana,x_ventana,y_ventana,ancho_ventana,alto_ventana,
							ancho_ventana-1,alto_ventana-2,"Joystick Information");
	zxvision_draw_window(&ventana);			


	z80_byte acumulado;



	int valor_contador_segundo_anterior;

	valor_contador_segundo_anterior=contador_segundo;

	menu_hardware_realjoystick_test_reset_last_values();

	int salir_por_boton=0;


	do {

		menu_cpu_core_loop();
                acumulado=menu_da_todas_teclas();

		//si no hay multitarea, pausar
		if (menu_multitarea==0) {
			usleep(20000); //20 ms
		}


		//Si es evento de salir, forzar el mostrar la info y luego salir
		if (menu_info_joystick_last_button>=0 && menu_info_joystick_last_index==REALJOYSTICK_EVENT_ESC_MENU) {
			//printf ("Salir por boton\n");
			salir_por_boton=1;
		}

        if ( ((contador_segundo%50) == 0 && valor_contador_segundo_anterior!=contador_segundo) || menu_multitarea==0 || salir_por_boton) {
            valor_contador_segundo_anterior=contador_segundo;
			//printf ("Refrescando. contador_segundo=%d\n",contador_segundo);
			if (menu_multitarea==0) menu_refresca_pantalla();


			char buffer_texto_medio[60];

			int linea=0;
			//int menu_info_joystick_last_button,menu_info_joystick_last_type,menu_info_joystick_last_value,menu_info_joystick_last_index;
			//menu_escribe_linea_opcion(linea++,-1,1,"Last joystick button/axis:");
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"Last joystick button/axis:");
			linea++;

		

			//printf ("nuevo evento test joystick\n");

			char buffer_type[40];
#define LONGITUD_BARRAS 6
			char fill_bars[(LONGITUD_BARRAS*2)+2];
			fill_bars[0]=0;
			if (menu_info_joystick_last_type==REALJOYSTICK_INPUT_EVENT_BUTTON) {
				strcpy(buffer_type,"Button");
			}
			else if (menu_info_joystick_last_type==REALJOYSTICK_INPUT_EVENT_AXIS) {
				strcpy(buffer_type,"Axis");
				menu_hardware_realjoystick_test_fill_bars(menu_info_joystick_last_raw_value,fill_bars,LONGITUD_BARRAS);
			}
			else strcpy(buffer_type,"Unknown");

		
			if (menu_info_joystick_last_button<0) strcpy(buffer_texto_medio,"Button: None");
			else sprintf (buffer_texto_medio,"Button: %d",menu_info_joystick_last_button);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);

			if (menu_info_joystick_last_type<0) strcpy(buffer_texto_medio,"Type: None");
			else sprintf (buffer_texto_medio,"Type: %d (%s)",menu_info_joystick_last_type,buffer_type);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);



			char buffer_event[40];
			if (menu_info_joystick_last_index>=0 && menu_info_joystick_last_index<MAX_EVENTS_JOYSTICK) {
				strcpy(buffer_event,realjoystick_event_names[menu_info_joystick_last_index]);
			}
			else {
				strcpy(buffer_event,"None");
			}


			sprintf (buffer_texto_medio,"Value: %6d %s",menu_info_joystick_last_raw_value,fill_bars);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);

			sprintf (buffer_texto_medio,"Index: %d Event: %s",menu_info_joystick_last_index,buffer_event);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);

			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"");	

			sprintf (buffer_texto_medio,"Driver: %s",realjoystick_driver_name);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);	

			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,"Name:");	
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,realjoystick_joy_name);	

			sprintf (buffer_texto_medio,"Total buttons: %d",realjoystick_total_buttons);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);	

			sprintf (buffer_texto_medio,"Total axis: %d",realjoystick_total_axes);
			zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);	

 			if (!realjoystick_is_linux_native() ) {
				sprintf (buffer_texto_medio,"Autocalibrate value: %d",realjoystick_autocalibrate_value);
				zxvision_print_string_defaults_fillspc(&ventana,1,linea++,buffer_texto_medio);			
			}



			//realjoystick_ultimo_indice=-1;
			//menu_hardware_realjoystick_test_reset_last_values();
			//menu_info_joystick_last_button=-1;
			//menu_info_joystick_last_type=-1;
			//menu_info_joystick_last_value=-1;
			//menu_info_joystick_last_index=-1;
			//menu_info_joystick_last_raw_value=-1;

			zxvision_draw_window_contents(&ventana);


			
        }

		

        //Hay tecla pulsada
            if ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) !=MENU_PUERTO_TECLADO_NINGUNA ) {
                                int tecla=menu_get_pressed_key();

                                                                                                                                
				//Si tecla no es ESC, no salir
                                
				if (tecla!=2) {
					acumulado = MENU_PUERTO_TECLADO_NINGUNA;
				}


				//Si ha salido por boton de joystick, esperar evento
				if (salir_por_boton) {
                       			if (menu_multitarea==0) menu_refresca_pantalla();
					menu_espera_no_tecla();
				}
				
                                                                        
			}


        } while ( (acumulado & MENU_PUERTO_TECLADO_NINGUNA) ==MENU_PUERTO_TECLADO_NINGUNA);


	cls_menu_overlay();
	zxvision_destroy_window(&ventana);

}


void menu_hardware_realjoystick_autocalibrate(MENU_ITEM_PARAMETERS)
{
    char string_calibrate[6];
	int valor;


    sprintf (string_calibrate,"%d",realjoystick_autocalibrate_value);

	menu_ventana_scanf("Autocalibrate value",string_calibrate,6);

	valor=parse_string_to_number(string_calibrate);

	
	if (valor<0 || valor>32000) {
		debug_printf (VERBOSE_ERR,"Value out of range. Minimum: 0 Maximum: 32000");
        return;
    }

	realjoystick_autocalibrate_value=valor;


}

void menu_hardware_realjoystick_set_defaults(MENU_ITEM_PARAMETERS)
{
	if (menu_confirm_yesno_texto("Set to defaults","Sure?")==1) {
        realjoystick_new_set_default_functions();
		menu_generic_message("Set to defaults","OK. Events and keys tables set to default values");
    }
}


void menu_hardware_realjoystick_native(MENU_ITEM_PARAMETERS)
{
	no_native_linux_realjoystick.v ^=1;
	menu_generic_message("Linux native driver","OK. You must reopen ZEsarUX to apply this setting");
}


void menu_hardware_printers_zxprinter_enable(MENU_ITEM_PARAMETERS)
{
	zxprinter_enabled.v ^=1;

	if (zxprinter_enabled.v==0) {
	        close_zxprinter_bitmap_file();
        	close_zxprinter_ocr_file();
	}

}


int menu_hardware_zxprinter_cond(void)
{
	return zxprinter_enabled.v;
}


void menu_hardware_zxprinter_bitmapfile(MENU_ITEM_PARAMETERS)
{
	close_zxprinter_bitmap_file();

        char *filtros[3];

        filtros[0]="txt";
        filtros[1]="pbm";
        filtros[2]=0;


        if (menu_filesel("Select Bitmap File",filtros,zxprinter_bitmap_filename_buffer)==1) {
                //Ver si archivo existe y preguntar
                struct stat buf_stat;

                if (stat(zxprinter_bitmap_filename_buffer, &buf_stat)==0) {

                        if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return;

                }

                zxprinter_bitmap_filename=zxprinter_bitmap_filename_buffer;

		zxprinter_file_bitmap_init();


        }
}


void menu_hardware_zxprinter_ocrfile(MENU_ITEM_PARAMETERS)
{
        close_zxprinter_ocr_file();

        char *filtros[2];

        filtros[0]="txt";
        filtros[1]=0;


        if (menu_filesel("Select OCR File",filtros,zxprinter_ocr_filename_buffer)==1) {
                //Ver si archivo existe y preguntar
                struct stat buf_stat;

                if (stat(zxprinter_ocr_filename_buffer, &buf_stat)==0) {

                        if (menu_confirm_yesno_texto("File exists","Overwrite?")==0) return;

                }

                zxprinter_ocr_filename=zxprinter_ocr_filename_buffer;

                zxprinter_file_ocr_init();


        }
}


void menu_hardware_tbblue_ram(MENU_ITEM_PARAMETERS)
{
        if (tbblue_extra_512kb_blocks == 3) tbblue_extra_512kb_blocks = 1;
        else tbblue_extra_512kb_blocks = 3;
}


//Retorna 0 si ok
//Retorna -1 si no hay cambio de variable
//Modifica valor de variable
int menu_hardware_advanced_input_value(int minimum,int maximum,char *texto,int *variable)
{
	
	int variable_copia;
	variable_copia=*variable;

	menu_ventana_scanf_numero_enhanced(texto,&variable_copia,4,+1,minimum,maximum,1);

	if (variable_copia==(*variable)) {
		//printf("no hay cambios\n");
		return -1;
	}

	else {
		*variable=variable_copia;
		return 0;
	}
	/*

	int valor;

        char string_value[4];

        sprintf (string_value,"%d",*variable);


        menu_ventana_scanf(texto,string_value,4);

        valor=parse_string_to_number(string_value);

	if (valor<minimum || valor>maximum) {
		debug_printf (VERBOSE_ERR,"Value out of range. Minimum: %d Maximum: %d",minimum,maximum);
		return -1;
	}

	*variable=valor;
	return 0;
	*/


}

void menu_hardware_advanced_reload_display(void)
{

		screen_testados_linea=screen_total_borde_izquierdo/2+128+screen_total_borde_derecho/2+screen_invisible_borde_derecho/2;

	        //Recalcular algunos valores cacheados
	        recalcular_get_total_ancho_rainbow();
        	recalcular_get_total_alto_rainbow();

                screen_set_video_params_indices();
                inicializa_tabla_contend();

                init_rainbow();
                init_cache_putpixel();
}



void menu_hardware_advanced_hidden_top_border(MENU_ITEM_PARAMETERS)
{
	int max,min;
	min=7;
	max=16;

	if (menu_hardware_advanced_input_value(min,max,"Hidden top Border",&screen_invisible_borde_superior)==0) {
		menu_hardware_advanced_reload_display();
	}
}

void menu_hardware_advanced_visible_top_border(MENU_ITEM_PARAMETERS)
{

	int max,min;
	min=32;
	max=56;

        if (menu_hardware_advanced_input_value(min,max,"Visible top Border",&screen_borde_superior)==0) {
                menu_hardware_advanced_reload_display();
        }
}

void menu_hardware_advanced_visible_bottom_border(MENU_ITEM_PARAMETERS)
{
	int max,min;
	min=48;
	max=56;


        if (menu_hardware_advanced_input_value(min,max,"Visible bottom Border",&screen_total_borde_inferior)==0) {
                menu_hardware_advanced_reload_display();
        }
}

void menu_hardware_advanced_borde_izquierdo(MENU_ITEM_PARAMETERS)
{

	int valor_pixeles;

	valor_pixeles=screen_total_borde_izquierdo/2;

        int max,min;
	min=12;
	max=24;

	if (menu_hardware_advanced_input_value(min,max,"Left Border TLength",&valor_pixeles)==0) {
		screen_total_borde_izquierdo=valor_pixeles*2;
		 menu_hardware_advanced_reload_display();
        }
}

void menu_hardware_advanced_borde_derecho(MENU_ITEM_PARAMETERS)
{

        int valor_pixeles;

	valor_pixeles=screen_total_borde_derecho/2;

        int max,min;
	min=12;
        max=24;

        if (menu_hardware_advanced_input_value(min,max,"Right Border TLength",&valor_pixeles)==0) {
                screen_total_borde_derecho=valor_pixeles*2;
                 menu_hardware_advanced_reload_display();
        }
}

void menu_hardware_advanced_hidden_borde_derecho(MENU_ITEM_PARAMETERS)
{

        int valor_pixeles;

	valor_pixeles=screen_invisible_borde_derecho/2;

        int max,min;
	min=24;
	max=52;


        if (menu_hardware_advanced_input_value(min,max,"Right Hidden B.TLength",&valor_pixeles)==0) {
                screen_invisible_borde_derecho=valor_pixeles*2;
                 menu_hardware_advanced_reload_display();
        }
}


void menu_ula_late_timings(MENU_ITEM_PARAMETERS)
{
	ula_late_timings.v ^=1;
	inicializa_tabla_contend();
}


void menu_ula_contend(MENU_ITEM_PARAMETERS)
{
	contend_enabled.v ^=1;

	inicializa_tabla_contend();

}


void menu_ula_im2_slow(MENU_ITEM_PARAMETERS)
{
        ula_im2_slow.v ^=1;
}


void menu_ula_pentagon_timing(MENU_ITEM_PARAMETERS)
{
	if (pentagon_timing.v) {
		contend_enabled.v=1;
		ula_disable_pentagon_timing();
	}

	else {
		contend_enabled.v=0;
		ula_enable_pentagon_timing();
	}


}


void menu_exit_emulator(MENU_ITEM_PARAMETERS)
{

	menu_reset_counters_tecla_repeticion();

	int salir=0;

	//Si quickexit, no preguntar
	if (quickexit.v) salir=1;

	else salir=menu_confirm_yesno("Exit ZEsarUX");

                        if (salir) {

				//menu_footer=0;

                                cls_menu_overlay();

                                reset_menu_overlay_function();
                                menu_abierto=0;


                                end_emulator();

                        }
                        cls_menu_overlay();
}


void menu_principal_salir_emulador(MENU_ITEM_PARAMETERS)
{
	menu_exit_emulator(0);	
}



#ifdef TIMESENSORS_ENABLED

void menu_debug_timesensors(MENU_ITEM_PARAMETERS)
{
	int i;  

	char timesensors_buffer[MAX_TEXTO_GENERIC_MESSAGE];

	char buf_linea[64];

    int index_buffer=0;

    for (i=0;i<10;i++) {
		long media=TIMESENSOR_ENTRY_MEDIATIME(i);
        sprintf (buf_linea,"ID: %02d average: %ld us (%ld ms)\n",i,media,media/1000);
        sprintf (&timesensors_buffer[index_buffer],"%s",buf_linea); index_buffer +=strlen(buf_linea);

		long max=TIMESENSOR_ENTRY_MAXTIME(i);
        sprintf (buf_linea,"ID: %02d max: %ld us (%ld ms)\n",i,max,max/1000);
        sprintf (&timesensors_buffer[index_buffer],"%s",buf_linea); index_buffer +=strlen(buf_linea);		
	}

	menu_generic_message("Sensors",timesensors_buffer);


}

void menu_debug_timesensors_enable(MENU_ITEM_PARAMETERS)
{
	timesensors_started ^=1;

}

void menu_debug_timesensors_init(MENU_ITEM_PARAMETERS)
{
	TIMESENSOR_INIT();
}

#endif


#ifdef EMULATE_CPU_STATS

int cpu_stats_valor_contador_segundo_anterior;
zxvision_window *menu_debug_cpu_resumen_stats_overlay_window;
zxvision_window menu_debug_cpu_resumen_stats_ventana;

void menu_debug_cpu_stats_clear_disassemble_array(void)
{
	int i;

	for (i=0;i<DISASSEMBLE_ARRAY_LENGTH;i++) disassemble_array[i]=0;
}

void menu_debug_cpu_stats_diss_no_print(z80_byte index,z80_byte opcode,char *dumpassembler)
{

        size_t longitud_opcode;

        disassemble_array[index]=opcode;

        debugger_disassemble_array(dumpassembler,31,&longitud_opcode,0);
}


void menu_debug_cpu_stats_diss_complete_no_print (z80_byte opcode,char *buffer,z80_byte preffix1,z80_byte preffix2)
{

	//Sin prefijo
	if (preffix1==0) {
                        menu_debug_cpu_stats_clear_disassemble_array();
                        menu_debug_cpu_stats_diss_no_print(0,opcode,buffer);
	}

	//Con 1 solo prefijo
	else if (preffix2==0) {
                        menu_debug_cpu_stats_clear_disassemble_array();
                        disassemble_array[0]=preffix1;
                        menu_debug_cpu_stats_diss_no_print(1,opcode,buffer);
	}

	//Con 2 prefijos (DD/FD + CB)
	else {
                        //Opcode
                        menu_debug_cpu_stats_clear_disassemble_array();
                        disassemble_array[0]=preffix1;
                        disassemble_array[1]=preffix2;
                        disassemble_array[2]=0;
                        menu_debug_cpu_stats_diss_no_print(3,opcode,buffer);
	}
}



void menu_cpu_full_stats(unsigned int *stats_table,char *title,z80_byte preffix1,z80_byte preffix2)
{

	int index_op,index_buffer;
	unsigned int counter;

	char stats_buffer[MAX_TEXTO_GENERIC_MESSAGE];

	char dumpassembler[32];

	//margen suficiente para que quepa una linea y un contador int de 32 bits...
	//aunque si pasa el ancho de linea, la rutina de generic_message lo troceara
	char buf_linea[64];

	index_buffer=0;

	for (index_op=0;index_op<256;index_op++) {
		counter=util_stats_get_counter(stats_table,index_op);

		menu_debug_cpu_stats_diss_complete_no_print(index_op,dumpassembler,preffix1,preffix2);

		sprintf (buf_linea,"%02X %-16s: %u \n",index_op,dumpassembler,counter);
		//16 ocupa la instruccion mas larga: LD B,RLC (IX+dd)

		sprintf (&stats_buffer[index_buffer],"%s\n",buf_linea);
		//sprintf (&stats_buffer[index_buffer],"%02X: %11d\n",index_op,counter);
		//index_buffer +=16;
		index_buffer +=strlen(buf_linea);
	}

	stats_buffer[index_buffer]=0;

	menu_generic_message(title,stats_buffer);

}

void menu_cpu_full_stats_codsinpr(MENU_ITEM_PARAMETERS)
{
	menu_cpu_full_stats(stats_codsinpr,"Full statistic no preffix",0,0);
}

void menu_cpu_full_stats_codpred(MENU_ITEM_PARAMETERS)
{
        menu_cpu_full_stats(stats_codpred,"Full statistic pref ED",0xED,0);
}

void menu_cpu_full_stats_codprcb(MENU_ITEM_PARAMETERS)
{
        menu_cpu_full_stats(stats_codprcb,"Full statistic pref CB",0xCB,0);
}

void menu_cpu_full_stats_codprdd(MENU_ITEM_PARAMETERS)
{
        menu_cpu_full_stats(stats_codprdd,"Full statistic pref DD",0xDD,0);
}

void menu_cpu_full_stats_codprfd(MENU_ITEM_PARAMETERS)
{
        menu_cpu_full_stats(stats_codprfd,"Full statistic pref FD",0xFD,0);
}

void menu_cpu_full_stats_codprddcb(MENU_ITEM_PARAMETERS)
{
        menu_cpu_full_stats(stats_codprddcb,"Full statistic pref DDCB",0xDD,0xCB);
}

void menu_cpu_full_stats_codprfdcb(MENU_ITEM_PARAMETERS)
{
        menu_cpu_full_stats(stats_codprfdcb,"Full statistic pref FDCB",0xFD,0xCB);
}


void menu_cpu_full_stats_clear(MENU_ITEM_PARAMETERS)
{
	util_stats_init();

	menu_generic_message_splash("Clear CPU statistics","OK. Statistics cleared");
}


void menu_debug_cpu_stats(MENU_ITEM_PARAMETERS)
{
        menu_item *array_menu_cpu_stats;
        menu_item item_seleccionado;
        int retorno_menu;
        do {
                menu_add_item_menu_inicial_format(&array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_debug_cpu_resumen_stats,NULL,"Compact Statistics");
                menu_add_item_menu_tooltip(array_menu_cpu_stats,"Shows Compact CPU Statistics");
                menu_add_item_menu_ayuda(array_menu_cpu_stats,"Shows the most used opcode for every type: without preffix, with ED preffix, "
					"etc. CPU Statistics are reset when changing machine or resetting CPU.");


                menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codsinpr,NULL,"Full Statistics No pref");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codpred,NULL,"Full Statistics Pref ED");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codprcb,NULL,"Full Statistics Pref CB");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codprdd,NULL,"Full Statistics Pref DD");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codprfd,NULL,"Full Statistics Pref FD");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codprddcb,NULL,"Full Statistics Pref DDCB");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_codprfdcb,NULL,"Full Statistics Pref FDCB");
		menu_add_item_menu_format(array_menu_cpu_stats,MENU_OPCION_NORMAL,menu_cpu_full_stats_clear,NULL,"Clear Statistics");


                menu_add_item_menu(array_menu_cpu_stats,"",MENU_OPCION_SEPARADOR,NULL,NULL);
                //menu_add_item_menu(array_menu_cpu_stats,"ESC Back",MENU_OPCION_NORMAL|MENU_OPCION_ESC,NULL,NULL);
                menu_add_ESC_item(array_menu_cpu_stats);

                retorno_menu=menu_dibuja_menu(&cpu_stats_opcion_seleccionada,&item_seleccionado,array_menu_cpu_stats,"CPU Statistics" );
		

                if ((item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu>=0) {
                        //llamamos por valor de funcion
                        if (item_seleccionado.menu_funcion!=NULL) {
                                //printf ("actuamos por funcion\n");
                                item_seleccionado.menu_funcion(item_seleccionado.valor_opcion);
                                
                        }
                }

        } while ( (item_seleccionado.tipo_opcion&MENU_OPCION_ESC)==0 && retorno_menu!=MENU_RETORNO_ESC && !salir_todos_menus);
}

void menu_debug_cpu_resumen_stats_overlay(void)
{
	if (!zxvision_drawing_in_background) normal_overlay_texto_menu();

	    char textostats[32];
	zxvision_window *ventana;

	ventana=menu_debug_cpu_resumen_stats_overlay_window;


        char dumpassembler[32];

        //Empezar con espacio
        dumpassembler[0]=' ';

				//int valor_contador_segundo_anterior;



		//z80_byte tecla;

		//printf ("%d %d\n",contador_segundo,cpu_stats_valor_contador_segundo_anterior);
     

			//esto hara ejecutar esto 2 veces por segundo
			if ( ((contador_segundo%500) == 0 && cpu_stats_valor_contador_segundo_anterior!=contador_segundo) || menu_multitarea==0) {
											cpu_stats_valor_contador_segundo_anterior=contador_segundo;
				//printf ("Refrescando. contador_segundo=%d\n",contador_segundo);

			int linea=0;
                        int opcode;

			unsigned int sumatotal; 
                        sumatotal=util_stats_sum_all_counters();
                    	sprintf (textostats,"Total opcodes run: %u",sumatotal);
						//menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);
                        


						//menu_escribe_linea_opcion(linea++,-1,1,"Most used op. for each preffix");
						zxvision_print_string_defaults(ventana,1,linea++,"Most used op. for each preffix");

                        opcode=util_stats_find_max_counter(stats_codsinpr);
                        sprintf (textostats,"Op nopref:    %02XH: %u",opcode,util_stats_get_counter(stats_codsinpr,opcode) );
						//menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);
                        

                        //Opcode
						menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],0,0);
						//menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);
						



                        opcode=util_stats_find_max_counter(stats_codpred);
                        sprintf (textostats,"Op pref ED:   %02XH: %u",opcode,util_stats_get_counter(stats_codpred,opcode) );
						//menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);
                        

                        //Opcode
                        menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],237,0);
						//menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);
                        

	
                        opcode=util_stats_find_max_counter(stats_codprcb);
                        sprintf (textostats,"Op pref CB:   %02XH: %u",opcode,util_stats_get_counter(stats_codprcb,opcode) );
						//menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);


                        //Opcode
                        menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],203,0);
						//menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);




                        opcode=util_stats_find_max_counter(stats_codprdd);
                        sprintf (textostats,"Op pref DD:   %02XH: %u",opcode,util_stats_get_counter(stats_codprdd,opcode) );
                        //menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);

                        //Opcode
                        menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],221,0);
                        //menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);


                        opcode=util_stats_find_max_counter(stats_codprfd);
                        sprintf (textostats,"Op pref FD:   %02XH: %u",opcode,util_stats_get_counter(stats_codprfd,opcode) );
                        //menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);

                        //Opcode
                        menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],253,0);
                        //menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);


                        opcode=util_stats_find_max_counter(stats_codprddcb);
                        sprintf (textostats,"Op pref DDCB: %02XH: %u",opcode,util_stats_get_counter(stats_codprddcb,opcode) );
                        //menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);

                        //Opcode
                        menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],221,203);
                        //menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);



                        opcode=util_stats_find_max_counter(stats_codprfdcb);
                        sprintf (textostats,"Op pref FDCB: %02XH: %u",opcode,util_stats_get_counter(stats_codprfdcb,opcode) );
                        //menu_escribe_linea_opcion(linea++,-1,1,textostats);
						zxvision_print_string_defaults(ventana,1,linea++,textostats);

                        //Opcode
                        menu_debug_cpu_stats_diss_complete_no_print(opcode,&dumpassembler[1],253,203);
                        //menu_escribe_linea_opcion(linea++,-1,1,dumpassembler);
						zxvision_print_string_defaults(ventana,1,linea++,dumpassembler);


						//zxvision_draw_window_contents(ventana);


                }

			//Siempre hará el dibujado de contenido para evitar que cuando esta en background, otra ventana por debajo escriba algo,
			//y entonces como esta no redibuja siempre, al no escribir encima, se sobreescribe este contenido con el de otra ventana
			//En ventanas que no escriben siempre su contenido, siempre deberia estar zxvision_draw_window_contents que lo haga siempre
			zxvision_draw_window_contents(ventana);



}


void menu_debug_cpu_resumen_stats(MENU_ITEM_PARAMETERS)
{
	menu_espera_no_tecla();
	menu_reset_counters_tecla_repeticion();		

		zxvision_window *ventana;
		
		ventana=&menu_debug_cpu_resumen_stats_ventana;

    //IMPORTANTE! no crear ventana si ya existe. Esto hay que hacerlo en todas las ventanas que permiten background.
    //si no se hiciera, se crearia la misma ventana, y en la lista de ventanas activas , al redibujarse,
    //la primera ventana repetida apuntaria a la segunda, que es el mismo puntero, y redibujaria la misma, y se quedaria en bucle colgado
    zxvision_delete_window_if_exists(ventana);

		
	int x,y,ancho,alto;

	if (!util_find_window_geometry("cpucompactstatistics",&x,&y,&ancho,&alto)) {
		x=menu_origin_x();
		y=1;
		ancho=32;
		alto=18;
	}		

	//int originx=menu_origin_x();

	zxvision_new_window(ventana,x,y,ancho,alto,
							ancho-1,alto-2,"CPU Compact Statistics");




	ventana->can_be_backgrounded=1;	
	zxvision_draw_window(ventana);
	//indicar nombre del grabado de geometria
	strcpy(ventana->geometry_name,"cpucompactstatistics");

	
		


		menu_debug_cpu_resumen_stats_overlay_window=ventana; //Decimos que el overlay lo hace sobre la ventana que tenemos aqui

						cpu_stats_valor_contador_segundo_anterior=contador_segundo;

        //Cambiamos funcion overlay de texto de menu
        //Se establece a la de funcion de onda + texto
        set_menu_overlay_function(menu_debug_cpu_resumen_stats_overlay);

       //Toda ventana que este listada en zxvision_known_window_names_array debe permitir poder salir desde aqui
       //Se sale despues de haber inicializado overlay y de cualquier otra variable que necesite el overlay
       if (zxvision_currently_restoring_windows_on_start) {
               //printf ("Saliendo de ventana ya que la estamos restaurando en startup\n");
               return;
       }	

	

	z80_byte tecla;

	do {
		tecla=zxvision_common_getkey_refresh();		
		zxvision_handle_cursors_pgupdn(ventana,tecla);
		//printf ("tecla: %d\n",tecla);
	} while (tecla!=2 && tecla!=3);				

	//Gestionar salir con tecla background
 
	menu_espera_no_tecla(); //Si no, se va al menu anterior.
	//En AY Piano por ejemplo esto no pasa aunque el estilo del menu es el mismo...

	//Antes de restaurar funcion overlay, guardarla en estructura ventana, por si nos vamos a background
	zxvision_set_window_overlay_from_current(ventana);	

    //restauramos modo normal de texto de menu
     set_menu_overlay_function(normal_overlay_texto_menu);


    cls_menu_overlay();	

	//Grabar geometria ventana
	util_add_window_geometry_compact(ventana);		


	if (tecla==3) {
		zxvision_message_put_window_background();
	}

	else {
		zxvision_destroy_window(ventana);		
 	}
}

#endif

