#!/bin/bash


INSTALLPREFIX=`cat compileoptions.h |grep INSTALL_PREFIX|cut -d '"' -f2`


cat > install.sh << _EOF
#!/bin/bash

echo "Installing ZEsarUX under $INSTALLPREFIX ..."

mkdir -p $INSTALLPREFIX
mkdir -p $INSTALLPREFIX/bin
mkdir -p $INSTALLPREFIX/share/zesarux/

COMMONFILES="ACKNOWLEDGEMENTS LICENSE LICENSES_info licenses Changelog README INSTALL INSTALLWINDOWS *.rom tbblue.mmc zesarux.xcf"


cp -a \$COMMONFILES $INSTALLPREFIX/share/zesarux/
cp zesarux $INSTALLPREFIX/bin/


# Default permissions for files: read only
find $INSTALLPREFIX/share/zesarux/ -type f -print0| xargs -0 chmod 444

echo "Install done"

_EOF


chmod 755 install.sh

